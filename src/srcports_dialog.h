/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_SRCPORTSDIALOG_H_
#define _YANG_SRCPORTSDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/statline.h>
#endif

#include "config.h"
#include "theme.h"

enum {
	ID_DOSBLOODSWCHECKBOX = 1000,
	ID_LOCATEDOSBLOODSW,
	ID_DOSBLOODRGCHECKBOX,
	ID_LOCATEDOSBLOODRG,
	ID_DOSBLOODPPCHECKBOX,
	ID_LOCATEDOSBLOODPP,
	ID_DOSBLOODOUCHECKBOX,
	ID_LOCATEDOSBLOODOU,
	ID_SELECTBLOODMAPSFOLDER,

	ID_DOSDESCENTCHECKBOX,
	ID_LOCATEDOSDESCENT,
	ID_D1XREBIRTHCHECKBOX,
	ID_LOCATED1XREBIRTH,
	ID_SELECTDESCENTMAPSFOLDER,

	ID_DOSDESCENT2CHECKBOX,
	ID_LOCATEDOSDESCENT2,
	ID_D2XREBIRTHCHECKBOX,
	ID_LOCATED2XREBIRTH,
	ID_SELECTDESCENT2MAPSFOLDER,

	ID_DOSDUKESWCHECKBOX,
	ID_LOCATEDOSDUKESW,
	ID_DOSDUKERGCHECKBOX,
	ID_LOCATEDOSDUKERG,
	ID_DOSDUKEAECHECKBOX,
	ID_LOCATEDOSDUKEAE,
	ID_DUKE3DWCHECKBOX,
	ID_LOCATEDUKE3DW,
	ID_EDUKE32CHECKBOX,
	ID_LOCATEEDUKE32,
	ID_NDUKECHECKBOX,
	ID_LOCATENDUKE,
	ID_HDUKECHECKBOX,
	ID_LOCATEHDUKE,
	ID_XDUKECHECKBOX,
	ID_LOCATEXDUKE,
	ID_SELECTDUKEMAPSFOLDER,

	ID_DOSSWSWCHECKBOX,
	ID_LOCATEDOSSWSW,
	ID_DOSSWRGCHECKBOX,
	ID_LOCATEDOSSWRG,
	ID_VOIDSWCHECKBOX,
	ID_LOCATEVOIDSW,
	ID_SWPCHECKBOX,
	ID_LOCATESWP,
	ID_SELECTSWMAPSFOLDER,

	ID_SELECTCUSTOMPROFILE,
	ID_ADDNEWCUSTOMPROFILE,
	ID_DELETECUSTOMPROFILE,
	ID_CUSTOMSPONLYCHECKBOX,
	ID_SPEXECUTABLEINPUTTEXT,
	ID_LOCATECUSTOMSP,
	ID_CUSTOMSAMEBINCHECKBOX,
	ID_MPEXECUTABLEINPUTTEXT,
	ID_LOCATECUSTOMMP,
	ID_CUSTOMNOCDRADIOBTN,
	ID_CUSTOMCDLOCATIONRADIOBTN,
	ID_CDLOCATIONINPUTTEXT,
	ID_SELECTCUSTOMCDLOCATION,
	ID_CUSTOMCDIMAGERADIOBTN,
	ID_CDIMAGEINPUTTEXT,
	ID_LOCATECUSTOMCDIMAGE,
	ID_INGAMESUPPORTCHECKBOX,
	ID_NETBIOSCHECKBOX,
	ID_NETBIOSINPUTTEXT,
	ID_LOCATENETBIOS,
	ID_EXTRAARGSINPUTTEXT,
	ID_EXTRAHOSTARGSINPUTTEXT,
#if EXTRA_ARGS_FOR_ALL
	ID_EXTRAALLARGSINPUTTEXT,
#endif

	ID_DOSBOXCHECKBOX,
	ID_LOCATEDOSBOX,
	ID_DOSBOXCONFCHECKBOX,
	ID_LOCATEDOSBOXCONF,
	ID_DOSBOXNOCDRADIOBTN,
	ID_DOSBOXCDLOCATIONRADIOBTN,
	ID_SELECTDOSBOXCDLOCATION,
	ID_DOSBOXCDIMAGERADIOBTN,
	ID_LOCATEDOSBOXCDIMAGE,
	ID_BMOUSECHECKBOX,
	ID_LOCATEBMOUSE,

#ifndef __WXMSW__
	ID_WINECHECKBOX,
	ID_LOCATEWINE,
#endif
};

class SRCPortsDialog : public YANGDialog
{
public:
	SRCPortsDialog();
	~SRCPortsDialog();
	void OnLocateFile(wxCommandEvent& event);
	void OnSelectDir(wxCommandEvent& event);
	void OnCheckBoxClick(wxCommandEvent& event);
	void OnRadioBtnClick(wxCommandEvent& event);
//	void OnSelectCustomProfile(wxCommandEvent& event);
	void SelectCustomProfile();
	void OnUpdateTextInput(wxCommandEvent& event);
	void OnAddNewProfile(wxCommandEvent& event);
	void OnDeleteProfile(wxCommandEvent& event);

//	void OnApply(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);
	void OnOk(wxCommandEvent& event);
	bool ApplySettings();

private:
	YANGNotebook* m_srcportsnotebook;

	YANGPanel* m_srcportbloodpanel;
	YANGCheckBox* m_dosbloodswavailcheckBox;
	YANGTextCtrl* m_dosbloodswbintextCtrl;
	YANGButton* m_dosbloodswlocatebutton;
	YANGCheckBox* m_dosbloodrgavailcheckBox;
	YANGTextCtrl* m_dosbloodrgbintextCtrl;
	YANGButton* m_dosbloodrglocatebutton;
	YANGCheckBox* m_dosbloodppavailcheckBox;
	YANGTextCtrl* m_dosbloodppbintextCtrl;
	YANGButton* m_dosbloodpplocatebutton;
	YANGCheckBox* m_dosbloodouavailcheckBox;
	YANGTextCtrl* m_dosbloodoubintextCtrl;
	YANGButton* m_dosbloodoulocatebutton;
	wxStaticLine* m_prefbloodstaticline;
	wxStaticText* m_bloodmapsstaticText;
	YANGTextCtrl* m_bloodmapstextCtrl;
	YANGButton* m_bloodmapsselectbutton;

	YANGPanel* m_srcportdescentpanel;
	YANGCheckBox* m_dosdescentavailcheckBox;
	YANGTextCtrl* m_dosdescentbintextCtrl;
	YANGButton* m_dosdescentlocatebutton;
	YANGCheckBox* m_d1xrebirthavailcheckBox;
	YANGTextCtrl* m_d1xrebirthbintextCtrl;
	YANGButton* m_d1xrebirthlocatebutton;
	wxStaticLine* m_prefdescentstaticline;
	wxStaticText* m_descentmapsstaticText;
	YANGTextCtrl* m_descentmapstextCtrl;
	YANGButton* m_descentmapsselectbutton;

	YANGPanel* m_srcportdescent2panel;
	YANGCheckBox* m_dosdescent2availcheckBox;
	YANGTextCtrl* m_dosdescent2bintextCtrl;
	YANGButton* m_dosdescent2locatebutton;
	YANGCheckBox* m_d2xrebirthavailcheckBox;
	YANGTextCtrl* m_d2xrebirthbintextCtrl;
	YANGButton* m_d2xrebirthlocatebutton;
	wxStaticLine* m_prefdescent2staticline;
	wxStaticText* m_descent2mapsstaticText;
	YANGTextCtrl* m_descent2mapstextCtrl;
	YANGButton* m_descent2mapsselectbutton;

	YANGPanel* m_srcportduke3dpanel;
	YANGCheckBox* m_dosdukeswavailcheckBox;
	YANGTextCtrl* m_dosdukeswbintextCtrl;
	YANGButton* m_dosdukeswlocatebutton;
	YANGCheckBox* m_dosdukergavailcheckBox;
	YANGTextCtrl* m_dosdukergbintextCtrl;
	YANGButton* m_dosdukerglocatebutton;
	YANGCheckBox* m_dosdukeaeavailcheckBox;
	YANGTextCtrl* m_dosdukeaebintextCtrl;
	YANGButton* m_dosdukeaelocatebutton;
	YANGCheckBox* m_duke3dwavailcheckBox;
	YANGTextCtrl* m_duke3dwbintextCtrl;
	YANGButton* m_duke3dwlocatebutton;
	YANGCheckBox* m_eduke32availcheckBox;
	YANGTextCtrl* m_eduke32bintextCtrl;
	YANGButton* m_eduke32locatebutton;
	YANGCheckBox* m_ndukeavailcheckBox;
	YANGTextCtrl* m_ndukebintextCtrl;
	YANGButton* m_ndukelocatebutton;
	YANGCheckBox* m_hdukeavailcheckBox;
	YANGTextCtrl* m_hdukebintextCtrl;
	YANGButton* m_hdukelocatebutton;
	YANGCheckBox* m_xdukeavailcheckBox;
	YANGTextCtrl* m_xdukebintextCtrl;
	YANGButton* m_xdukelocatebutton;
	wxStaticLine* m_prefduke3dstaticline;
	wxStaticText* m_duke3dmapsstaticText;
	YANGTextCtrl* m_duke3dmapstextCtrl;
	YANGButton* m_duke3dmapsselectbutton;

	YANGPanel* m_srcportswpanel;
	YANGCheckBox* m_dosswswavailcheckBox;
	YANGTextCtrl* m_dosswswbintextCtrl;
	YANGButton* m_dosswswlocatebutton;
	YANGCheckBox* m_dosswrgavailcheckBox;
	YANGTextCtrl* m_dosswrgbintextCtrl;
	YANGButton* m_dosswrglocatebutton;
	YANGCheckBox* m_voidswavailcheckBox;
	YANGTextCtrl* m_voidswbintextCtrl;
	YANGButton* m_voidswlocatebutton;
	YANGCheckBox* m_swpavailcheckBox;
	YANGTextCtrl* m_swpbintextCtrl;
	YANGButton* m_swplocatebutton;
	wxStaticLine* m_prefswstaticline;
	wxStaticText* m_swmapsstaticText;
	YANGTextCtrl* m_swmapstextCtrl;
	YANGButton* m_swmapsselectbutton;

	YANGPanel* m_srcportcustompanel;
	wxStaticText* m_customprofilestaticText;
	YANGComboBox* m_customprofilechoice;
	YANGBitmapButton* m_customaddnewbutton;
	YANGBitmapButton* m_customdeletebutton;
	wxStaticLine* m_customstaticline;
	YANGCheckBox* m_customsponlycheckBox;
	wxStaticText* m_customspbinstaticText;
	YANGTextCtrl* m_customspbintextCtrl;
	YANGButton* m_customspbinlocatebutton;
	YANGCheckBox* m_customsamebincheckBox;
	wxStaticText* m_custommpbinstaticText;
	YANGTextCtrl* m_custommpbintextCtrl;
	YANGButton* m_custommpbinlocatebutton;
	YANGRadioButton* m_customnocdradioBtn;
	YANGRadioButton* m_customcdlocationradioBtn;
	YANGTextCtrl* m_customcdlocationtextCtrl;
	YANGButton* m_customcdlocationselectbutton;
	YANGRadioButton* m_customcdimageradioBtn;
	YANGTextCtrl* m_customcdimagetextCtrl;
	YANGButton* m_customcdimagelocatebutton;
	YANGCheckBox* m_customingamesupportcheckBox;
	YANGCheckBox* m_customnetbioscheckBox;
	YANGTextCtrl* m_customnetbiostextCtrl;
	YANGButton* m_customnetbioslocatebutton;
	wxStaticText* m_customextraargsstaticText;
	YANGTextCtrl* m_customextraargstextCtrl;
	wxStaticText* m_customextrahostargsstaticText;
	YANGTextCtrl* m_customextrahostargstextCtrl;
#if EXTRA_ARGS_FOR_ALL
	wxStaticText* m_customextraallargsstaticText;
	YANGTextCtrl* m_customextraallargstextCtrl;
#endif

	YANGPanel* m_srcportdosboxpanel;
	YANGCheckBox* m_dosboxavailcheckBox;
	YANGTextCtrl* m_dosboxbintextCtrl;
	YANGButton* m_dosboxlocatebutton;
	YANGCheckBox* m_dosboxconfcheckBox;
	YANGTextCtrl* m_dosboxconftextCtrl;
	YANGButton* m_dosboxconflocatebutton;
	YANGRadioButton* m_dosboxnocdradioBtn;
	YANGRadioButton* m_dosboxcdlocationradioBtn;
	YANGTextCtrl* m_dosboxcdlocationtextCtrl;
	YANGButton* m_dosboxcdlocationselectbutton;
	YANGRadioButton* m_dosboxcdimageradioBtn;
	YANGTextCtrl* m_dosboxcdimagetextCtrl;
	YANGButton* m_dosboxcdimagelocatebutton;
	YANGHyperlinkCtrl* m_dosboxhyperlink;
	wxStaticLine* m_dosboxstaticline;
	YANGCheckBox* m_dosboxbmousecheckBox;
	YANGTextCtrl* m_dosboxbmousetextCtrl;
	YANGButton* m_dosboxbmouselocatebutton;
#ifndef __WXMSW__
	YANGPanel* m_srcportwinepanel;
	YANGCheckBox* m_usewinecheckBox;
	YANGTextCtrl* m_winebintextCtrl;
	wxStaticText* m_winestaticText;
	YANGButton* m_winelocatebutton;
#endif
	wxStdDialogButtonSizer* m_srcportssdbSizer;
	YANGButton* m_srcportssdbSizerOK;
	YANGButton* m_srcportssdbSizerCancel;

	int custom_profile_index;

	custom_profile_t *custom_profile_list_temp, *custom_profile_current_temp;

	DECLARE_EVENT_TABLE()
};

#endif

