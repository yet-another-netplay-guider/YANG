Yet Another Netplay Guider (YANG)
=================================

This source tree covers sources for YANG, a multiplayer frontend
that had its initial release as a single-player launcher on April 26 2008.
The master server software, introduced on February 7 2009, is also included.

This codebase was prepared for the 15th anniversary of YANG's first release.
It was made available as a git repository on April 25 2023
(April 26 in other time zones).

Expect no technical support for getting YANG or its master server working.
Use at your own risk.

See a list of problems under "Known issues" further below.

The GeoIP directory
-------------------

The directory named "GeoIP" has a modification of GeoIP 1.4.6,
a library used for mapping IP addresses to countries. The modifications
are mostly code removals, albeit YANG can also function even with them
(at least on Linux).

Terms of use
------------

YANG and the YANG master server are both available for use under the terms
of the 3-Clause BSD License. See the relevant sources under `src` for details.

The modified GeoIP library is available under the terms of the
GNU Lesser General Public License v2.1+. See GeoIP/lgpl-2.1.txt for details.

The code under `src/getosdesc.cpp`, consisting of
a modified `wxGetOsVersion` function, is available under
the terms of the wxWindows License. See wxwindows.txt for details.

GeoIP.dat, consisting of data rather than sources, is available under
the terms of the OpenDataLicense. See open-data-license.txt for details.

Additional sources (like `src/crc_32.cpp`) may be available for use
under their own terms.

Note that the aforementioned terms of the 3-Clause BSD License also hold
to earlier versions of YANG and YANG master server sources from 2008-2023.

Building working binaries
-------------------------

See COMPILING.md.

Known issues
------------

- Password-protection of rooms was disabled in compile-time. Reason is that
when enabled, the passwords are sent as-is, with no encryption at all.
- UPnP was similarly disabled. The existing implementation was found
to be less stable while using wxGTK. On a side-note, UPnP is also
less useful behind CGNATs.
- The codebase is knowingly incompatible with wxGTK 3.0.3 and later.
This isn't a problem in wxGTK, but rather in YANG. Sockets were mistakenly
set as `wxSOCKET_BLOCK` while expecting UI events to arrive.
- Retrieving data from a master server was additionally found to be broken
while using wxMSW 3.1.3 or 3.2.0 for the client.
- By design, adding support for a new game, or even just a single source port,
can feel very far from "easy". There are multiple definitions and functions
that will have to be modified across differing files,
including switch statements which aren't that small. This isn't
like the JSON files introduced by NukemNet, making it much less
difficult to extend its support of games and source ports.
- There are other potential code maintenance problems.
- As an alternative to reporting the OS with wxGetOsDescription, a modification
of an older version of this function is present. It was initially prepared by
TURRICAN in 2009. The modified function might require periodic updates for
newer operating systems, and wxGetOsDescription itself may similarly depend on
newer versions of wxWidgets for more relevant descriptions. TURRICAN's version
is used for now, at least for (possibly older versions of) Windows and macOS.
- When using YANG for the first time, the default choices of column widths
for tables (e.g., servers list) might be nonoptimal. The original code was
maybe functional with wxWidgets 2.8, but couldn't be used as-is with the
3.0 series. Hopefully, after applying a few modifications,
it's still mostly usable with 3.0.
- Depending on how was wxWidgets built, you might occasionally get a wx-side
pop-up hinting about an error from the way YANG was coded. These pop-ups would
probably not be reproduced with release builds of wx2.8. Each of these might
be related either to a missed adaptation while porting to wx3.0, an old bug
in YANG which wasn't detected with wx2.8, or the lack of
use of debug builds of wxWidgets in the past.
- Chances are more problems exist in the codebase.

Some history
------------

YANG was started by NY00123 in 2008 as a GUI frontend for the 1990s titles
of Duke Nukem 3D and Shadow Warrior, while not having enough experience with
writing GUI programs (and non-GUI programs).

While the very first version, 0.1, was just a single-player launcher,
it didn't take long for version 0.2 to arrive. This version introduced
multiplayer support, letting ones create chat rooms for arranging games.
Later additions are mentioned here further below.

The idea for YANG was inspired by other launchers being written beforehand.
One is Addfaz's Dukester X, similarly letting ones create chat rooms for
establishing online games of Duke Nukem 3D. There was also Dukonnector,
created by NukemNet's author, aaBlueDragon, more than 15 years
before starting any work on NukemNet. Dukonnector supported
Duke Nukem 3D, Shadow Warrior, Blood and Redneck Rampage,
and would also let its users manually configure other DOS games.
It further supported the creation of user accounts.

Albeit YANG itself wasn't inspired from it in the same manner, another program
to mention is Kali, which used to be available as a commercial service.
Kali made it possible to play multiple DOS games making use of the
IPX protocol using IPX emulation. It also supported chat rooms for the games.

At the time, people were using Dukester X for playing Duke3D with source ports;
Mostly Rancidmeat's `duke3d_w32`, and later, the `duke3d_w32`-based `xDuke`.
Kali could also be used for playing a DOS version of Duke3D online.
Unfortunately, Dukonnector didn't gain traction, even after implementing
backwards compatibility with Dukester X. One possible reason was the
familiar "chicken and egg" problem, i.e., people wouldn't move
to Dukonnector because they were around in Dukester X.
Another one was the need to install .NET Framework 2.0.

Dukester X was a VB app, while Dukonnector was written using VB.NET.
On the other hand, YANG was written using C++ and the wxWidgets library.
This library was chosen so YANG would be cross-platform.

If NY00123 were still using Windows at the time,
most chances are that YANG would had never been born.

YANG was technically not NY00123's first attempt to write a launcher with
wxWidgets, either. There was an earlier single player launcher released,
shortly known as `bg_launcher`. Other attempts, not publicly
known until recently and also not really functional, were `Gamecon`
(looking similar to a UI clone of Dukonnector) and `CrossMeeting`.
It's not impossible that earlier code of at least one of
these attempts eventually found itself in YANG.

Back to YANG itself, version 0.2 and other follow-up versions maybe supported
multiplayer chat-rooms, but they lacked an important feature of Dukester X
and Dukonnector at the time. Basically, the chat rooms could not be
advertised on any public-facing server list of YANG.

This was changed in early 2009 with version 0.45, introducing server
advertising, along with a separate master server program. Replica, who
was hosting a master server for Dukester X at the time, agreed to do the same
for YANG. Version 0.45 was followed by other few follow-up releases shortly
afterwards, but what mattered the most was the server advertising feature.

Albeit interest was shown by players, Dukester X still had its userbase.
But something else coincidentally occurred shortly afterwards. The list
of Dukester X's master servers used to be retrieved from a central server,
hosted at the time by Lycos. By the beginning of March 2009,
though, Lycos web hosting was shut down. As a side-effect,
Dukester X server advertising became nonfunctional.

Dukester X 1.6 was released about a month later, resolving this issue
and listing other few improvements in the change log. Before its release,
though, users of Dukester X had already been migrating to YANG.

More coding
-----------

After the migration to YANG began, at least a few more individuals
(even if only about 2) were inspecting the codebase.

One of these was TURRICAN. As previously done for the `xDuke` source port,
TURRICAN's intention was to submit extensions to YANG so users of it
would benefit from them. Among other things, this covered new columns
showing estimated values for each user's ping and flux, as well as
one's country (via the GeoIP library) and operating system.

Further added by TURRICAN later were support for Blood, Descent and Descent 2,
and also for other, manually-setup DOS games. Additionally, the TC/MOD concept
was rewritten. Instead of manually selecting specific CON and/or GRP files,
a TC/MOD profile could be created, listing the desired files. Also supported
here were DEF files, along with URLs linking to the mods for other users.

Another individual to mention was Poda, initially going through
the YANG sources in order to make a build compatible with Windows 98.
Since it felt at the time that YANG was lacking in terms of updates,
Poda decided to start a fork of YANG, known as Meltdown.

It was mostly developed separately from YANG, while independently
implementing features like columns for users' countries and estimated pings,
and adding support for Blood and Redneck Rampage.
Among other additions and modifications, the chat service was a good example.
It was modified while taking clear inspirations from Kali,
and a global chatroom was added.

Another example of a modification was in user maps support, letting a user
select a map from a database. User account login was additionally implemented.

Another modified YANG client, named Duke Matcher, was further made available.

There's one very clear fact to learn; Especially (but not only) from
the creation of Meltdown, but also from countless other examples
(e.g., `duke3d_w32` -> `xDuke`, and the various other programs' forks).
Basically, if any program becomes open-source, this naturally means that
other people can make modifications of it; And if the original program
would connect to a central server list, then so could the modified version.
It maybe didn't seem to occur with Dukonnector at the time, even if
open-sourced in 2006, but this was clearly the case with YANG before mid-2009.

The 2020 version
----------------

Fast-forward to 2019, YANG was still not open source, but access to the
sources was granted to StrikerTheHedgefox. Apart from finally creating
a git repository for YANG, at least a few additions were implemented
by Striker. Examples are the ability to select PK3 files for mods,
as well as the addition of portable mode via a file (which can be empty)
named `yang_portable`.

Unfortunately, due to the lack of experience and mistakes previously done
in 2008-2009, it was often difficult (or at least nontrivial) to extend YANG,
even if just for adding support for one more source port. Therefore,
this work of Striker was left behind for some time.

And yet, this work eventually found itself in version 0.95. The following
occurred about 8 months after Striker's last git commit. Basically, Replica,
who used to host the website and master server, accidentally realized that
YANG v0.91 had an exploit. When hosting a chat room, in addition to the
field "Extra args for host only", there was also "Extra args for everyone",
letting the host add custom command-line arguments to be used for all
players (in case YANG didn't have built-in support for a specific option).
Problem was, by using the pipe character `|`, it could be made to execute
an arbitrary EXE file already installed on Windows systems (say notepad.exe).
In fact, using a variation of this idea and with the ability to send any EXE
under the disguise of a MAP file, it was possible to rename the MAP to an
EXE and then make the clients execute it, under the host's control.

Thus, version 0.95 was released with the removal of "Extra args for everyone".
It was also a one-time chance to cover Striker's updates and migrate to the
wxWidgets 3.0 series (from 2.8), along with having additional modifications.

The open-source release of 2023
-------------------------------

This release was based on v0.95 while having a few other changes,
along with general preparations. A few features were disabled in the
codebase, albeit they can still be enabled in compile-time with macros.
