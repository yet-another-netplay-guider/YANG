/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#endif

#include "master_roomstable.h"

MasterRoomsTable::MasterRoomsTable(int max_rooms, int max_rooms_per_ip)
{
	m_rooms = new MasterRoom[max_rooms];
	m_iptorooms_hash = new IPStrToNumOfRoomsHash(4*max_rooms);
	m_max_num_of_rooms = max_rooms;
	m_max_num_of_rooms_per_ip = max_rooms_per_ip;
	m_num_of_rooms = 0;
	for (size_t l_loop_var = 0; l_loop_var < SOURCEPORT_NUMOFPORTS; l_loop_var++)
		m_room_counts_by_port[l_loop_var] = 0;
}

MasterRoomsTable::~MasterRoomsTable()
{
	delete m_iptorooms_hash;
	delete [] m_rooms;
}

int MasterRoomsTable::Count(const wxString& address)
{
	int l_result = 0, l_loop_var;
	wxCriticalSectionLocker l_locker(m_critical_section);
	for (l_loop_var = 0; l_loop_var < m_num_of_rooms; l_loop_var++)
		if (m_rooms[l_loop_var].info.address == address)
			l_result++;
	return l_result;
}

//bool MasterRoomsTable::TryAddOrUpdate(RoomInfo& room_info)
bool MasterRoomsTable::TryAdd(MasterRoom& room)
{
	wxCriticalSectionLocker l_locker(m_critical_section);
	if (m_num_of_rooms == m_max_num_of_rooms)
		return false;
	wxIPV4address l_addr;
	room.info.sock->GetPeer(l_addr);

	if (m_iptorooms_hash->count(l_addr.IPAddress()))
	{
		if ((*m_iptorooms_hash)[l_addr.IPAddress()] == m_max_num_of_rooms_per_ip)
			return false;
		(*m_iptorooms_hash)[l_addr.IPAddress()]++;
	}
	else
		(*m_iptorooms_hash)[l_addr.IPAddress()] = 1;
	
	//room.info.num_players = room.nicknames.GetCount();
	m_rooms[m_num_of_rooms] = room;
	m_room_counts_by_port[room.info.srcport]++;
	m_num_of_rooms++;
	return true;
	/*int l_loop_var;
	for (l_loop_var = 0; l_loop_var < m_num_of_rooms; l_loop_var++)
	if (room_info.sock == m_rooms[l_loop_var].info.sock)
	break;
	if (l_loop_var == m_num_of_rooms)
	{
	if (l_loop_var == m_max_num_of_rooms)
	return false;
	m_num_of_rooms++;
	//  m_rooms[l_loop_var].nicknames = NULL;
	}
	m_rooms[l_loop_var].info = room_info;
	//m_rooms[l_loop_var] = room;
	return true;*/
}

void MasterRoomsTable::UpdateInfo(RoomInfo& room_info)
{
	wxCriticalSectionLocker l_locker(m_critical_section);
	int l_loop_var;
	wxSocketBase* l_sock = room_info.sock;
	for (l_loop_var = 0; l_loop_var < m_num_of_rooms; l_loop_var++)
		if (m_rooms[l_loop_var].info.sock == l_sock)
		{
			//    room_info.num_players = m_rooms[l_loop_var].nicknames.GetCount();
			m_room_counts_by_port[m_rooms[l_loop_var].info.srcport]--;
			m_room_counts_by_port[room_info.srcport]++;
			m_rooms[l_loop_var].info = room_info;
			break;
		}
}

void MasterRoomsTable::UpdatePlayers(wxSocketBase* sock, const wxArrayString& players)
{
	wxCriticalSectionLocker l_locker(m_critical_section);
	int l_loop_var;
	for (l_loop_var = 0; l_loop_var < m_num_of_rooms; l_loop_var++)
		if (m_rooms[l_loop_var].info.sock == sock)
		{
			m_rooms[l_loop_var].players = players;

			//    m_rooms[l_loop_var].info.num_players = nicknames.GetCount();
			break;
		}
}

/*
void MasterRoomsTable::AddNickname(wxSocketBase* sock, const wxString& nickname)
{
wxCriticalSectionLocker l_locker(m_critical_section);
int l_loop_var;
for (l_loop_var = 0; l_loop_var < m_num_of_rooms; l_loop_var++)
if ((m_rooms[l_loop_var].info.sock == sock) &&
(m_rooms[l_loop_var].nicknames.GetCount() < m_rooms[l_loop_var].info.max_num_players))
{
m_rooms[l_loop_var].nicknames.Add(nickname);
//    m_rooms[l_loop_var].info.num_players++;
break;
}
}

void MasterRoomsTable::RemNickname(wxSocketBase* sock, const wxString& nickname)
{
wxCriticalSectionLocker l_locker(m_critical_section);
int l_loop_var = 0;
for (l_loop_var = 0; l_loop_var < m_num_of_rooms; l_loop_var++)
if (m_rooms[l_loop_var].info.sock == sock)
{
m_rooms[l_loop_var].nicknames.Remove(nickname);
//    m_rooms[l_loop_var].info.num_players--;
break;
}
}
*/
bool MasterRoomsTable::IsFull()
{
	//wxCriticalSectionLocker l_locker(m_critical_section);
	return (m_num_of_rooms == m_max_num_of_rooms);
}

bool MasterRoomsTable::TryGet(int index, MasterRoom& room)
{
	wxCriticalSectionLocker l_locker(m_critical_section);
	if (index < m_num_of_rooms)
	{
		room = m_rooms[index];
		return true;
	}
	return false;
}

void MasterRoomsTable::Delete(wxSocketBase* sock)
{
	int l_loop_var;
	wxCriticalSectionLocker l_locker(m_critical_section);
	for (l_loop_var = 0; l_loop_var < m_num_of_rooms; l_loop_var++)
		if (m_rooms[l_loop_var].info.sock == sock)
		{
			m_num_of_rooms--;
			m_room_counts_by_port[m_rooms[l_loop_var].info.srcport]--;
			if (l_loop_var < m_num_of_rooms) // A simple move of one array element.
				m_rooms[l_loop_var] = m_rooms[m_num_of_rooms];

			wxIPV4address l_addr;
			sock->GetPeer(l_addr);
			if ((*m_iptorooms_hash)[l_addr.IPAddress()] == 1)
				m_iptorooms_hash->erase(l_addr.IPAddress());
			else
				(*m_iptorooms_hash)[l_addr.IPAddress()]--;

			break;
		}
}

size_t MasterRoomsTable::GetCountByPort(SourcePortType srcport)
{
	return m_room_counts_by_port[srcport];
}
