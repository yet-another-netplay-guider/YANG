/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#endif

#include "master_clientstable.h"

MasterClientsTable::MasterClientsTable(int max_clients)
{
	m_clients = new MasterClient[max_clients];
	m_max_num_of_clients = max_clients;
	m_num_of_clients = 0;
}

MasterClientsTable::~MasterClientsTable()
{
	delete [] m_clients;
}

int MasterClientsTable::Count(const wxString& address)
{
	int l_result = 0, l_loop_var;
	wxCriticalSectionLocker l_locker(m_critical_section);
	for (l_loop_var = 0; l_loop_var < m_num_of_clients; l_loop_var++)
		if (m_clients[l_loop_var].address == address)
			l_result++;
	return l_result;
}

bool MasterClientsTable::TryAdd(wxSocketBase* sock)
{
	wxIPV4address l_addr;
	wxCriticalSectionLocker l_locker(m_critical_section);
	if (m_num_of_clients < m_max_num_of_clients)
	{
		m_clients[m_num_of_clients].sock = sock;
		sock->GetPeer(l_addr);
		m_clients[m_num_of_clients].address = l_addr.IPAddress();
		m_num_of_clients++;
		return true;
	}
	return false;
}

void MasterClientsTable::Delete(wxSocketBase* sock)
{
	int l_loop_var;
	wxCriticalSectionLocker l_locker(m_critical_section);
	for (l_loop_var = 0; l_loop_var < m_num_of_clients; l_loop_var++)
		if (m_clients[l_loop_var].sock == sock)
		{
			m_num_of_clients--;
			if (l_loop_var < m_num_of_clients) // A simple move of one array element.
				m_clients[l_loop_var] = m_clients[m_num_of_clients];
			break;
		}
}
