/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_MAINFRAME_H_
#define _YANG_MAINFRAME_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#include <wx/imaglist.h>
#include <wx/timer.h>
#endif

#include "comm_txt.h"
#include "pingstopwatch.h"
#include "theme.h"
#include "config.h"
#include "common.h"

#define ID_OPENSPDIALOG 1000
#define ID_OPENMPDIALOG 1001
#define ID_OPENMANUALJOINDIALOG 1002
#define ID_CHANGETHEME 1003
#define ID_OPENSRCPORTSETTINGS 1004
#define ID_OPENTCMODSETTINGS 1005
#define ID_OPENNETWORKSETTINGS 1006
//#define ID_OPENLOOKANDFEEL 1007
#define ID_OPENADVANCEDSETTINGS 1008
#define ID_OPENFAQ 1009
#define ID_CHECKFORUPDATES 1010
#define ID_ALWAYSCHECKFORUPDATES 1011
#define ID_VIEWYANGNETTERMS 1012
#define ID_VISITWEBSITE 1013
#define ID_FILTERTREE 1014
#define ID_SERVERLIST 1015
#define ID_SERVERLISTAUTOCHECK 1016
/*#define ID_MANUALJOINADD 1017
#define ID_MANUALJOINLIST 1018
#define ID_MANUALJOINREM 1019*/
#define ID_GETLISTSOCKET 1020
#define ID_GETPING 1021
#define ID_GETLISTTIMER 1022
#define ID_GETPINGTIMER 1023

#define COL_PRIVATE 0
#define COL_LOCATION 1
#define COL_ROOM_NAME 2
#define COL_PING 3
#define COL_FLUX 4
#define COL_GAME 5
#define COL_SOURCE_PORT 6
#define COL_PLAYERS 7
#define COL_MAP 8
#define COL_TC_MOD 9
#define COL_GAME_TYPE 10
#define COL_SKILL 11
#define COL_INGAME 12

#define IMG_LOCK 0
#define IMG_BLANK 1

#define IMG_NUMBER 2

#define PLAYERCOL_LOCATION 0
#define PLAYERCOL_NICKNAME 1
#define PLAYERCOL_OS 2

class MainFrame : public YANGFrame
{
public:
	MainFrame(wxWindow* parent);
	~MainFrame();
	//void RefreshListCtrlSize();
	//void OnResizeWindow(wxSizeEvent& event);
	//void OnMaximize(wxMaximizeEvent& event);
	void OnChangeTheme(wxCommandEvent& event);
	void OnLoadSPDialog(wxCommandEvent& event);
	void OnLoadMPDialog(wxCommandEvent& event);
	void OnLoadManualJoinDialog(wxCommandEvent& event);
	void OnLoadSRCPortsDialog(wxCommandEvent& event);
	void OnLoadTCsMODsDialog(wxCommandEvent& event);
	void OnLoadNetworkDialog(wxCommandEvent& event);
	//void OnLoadLookAndFeelDialog(wxCommandEvent& event);
	void OnLoadAdvancedDialog(wxCommandEvent& event);
	void OnViewYANGTerms(wxCommandEvent& event);
	void OnVisitWebsite(wxCommandEvent& event);
	void OnOpenFAQ(wxCommandEvent& event);
	void OnCheckForUpdates(wxCommandEvent& event);
	void OnAutoCheckForUpdates(wxCommandEvent& event);
	void OnAbout(wxCommandEvent& event);
	void OnQuit(wxCommandEvent& event);
	void OnCloseWindow(wxCloseEvent& event);
	void OnServerListAutoCheck(wxCommandEvent& event);
	void Join();
	void OnJoin(wxCommandEvent& event);
	void OnFilterSelect(wxTreeEvent& event);
	void OnHostActivate(wxListEvent& event);
	void OnHostSelect(wxListEvent& event);
	void OnAddHost(wxCommandEvent& event);
	void OnRemHost(wxCommandEvent& event);
	//void OnManualJoinHostSelect(wxCommandEvent& event);
	void Connect();
	void TryNextServer();
	void RefreshList();
	void PingServer();
	void OnGetList(wxCommandEvent& event);
	void OnSocketEvent(wxSocketEvent& event);
	void OnPingEvent(wxSocketEvent& event);
	void OnTimer(wxTimerEvent& event);
	void OnPingTimer(wxTimerEvent& event);

	void ShowMainFrame();
	void HideMainFrame();

private:
	wxWindow* m_parent;

	wxMenuBar* m_Mainmenubar;
	//YANGMenuBar* m_Mainmenubar;
	wxMenu* m_Actionmenu;
	wxMenu* m_Settingsmenu;
	wxMenu* m_Helpmenu;
#if CHECK_FOR_UPDATES
	wxMenuItem* m_checkforupdatesonstartupmenuItem; // Has a CheckBox.
#endif
	YANGPanel* m_mainpanel;
	YANGTreeCtrl* m_filtertreeCtrl;
	YANGListCtrl* m_serverlistCtrl;
	YANGListCtrl* m_playerlistCtrl;
	//YANGListBox* m_playerlistBox;
	YANGButton* m_getlistbutton;
	wxStaticText* m_serverliststaticText;
	YANGCheckBox* m_autolookupcheckBox;
	/*wxStaticText* m_manualjoinnamestaticText;
	wxStaticText* m_manualjoinhostaddrstaticText;
	wxStaticText* m_manualjoinportnumstaticText;
	YANGTextCtrl* m_manualjoinnametextCtrl;
	YANGTextCtrl* m_manualjoinhostaddrtextCtrl;
	wxStaticText* m_manualjoinseparatorstaticText;
	YANGTextCtrl* m_manualjoinportnumtextCtrl;
	YANGButton* m_manualjoinaddbutton;
	YANGChoice* m_manualjoinchoice;
	YANGButton* m_manualjoinrembutton;
	YANGButton* m_joinbutton;*/
	wxStatusBar* m_MainstatusBar;
	//YANGStatusBar* m_MainstatusBar;
	wxImageList *m_ImageList, *m_playerImageList;

	void StartPinging();
	void SaveUIControlSizes();

	bool m_check_default_lists, m_wait_for_nicks;
	size_t m_curr_room, m_room_num, /*m_last_server, */m_server_in_curr_list, m_servers_to_count;
	wxTimer* m_serverlist_timer;
	wxTimer* m_ping_timer;
	wxSocketClient* m_client;

	size_t m_ping_times, m_ping_id;
	long m_min_ping, m_max_ping;
	PingStopWatch ping;

	ArrayOfStringArrays m_players;
	wxArrayString m_addrs, m_numplayers_limits;
	wxArrayLong m_portnums;

	char m_short_buffer[MAX_COMM_TEXT_LENGTH];
	wxString m_long_buffer;

	wxArrayString m_games_name, m_ports_name, m_games_code, m_ports_code;

	wxTreeItemId m_filtertree_current_selection;

	size_t first_time_filter_select;

	int game_ranges[SOURCEPORT_NUMOFPORTS], sourceport_ranges[GAME_NUMOFGAMES+1];

	typedef struct YANG_games_s
	{
		wxTreeItemId Id;
		long num_of_rooms;
	} YANG_games_t;

	YANG_games_t m_games[GAME_NUMOFGAMES];

	typedef struct YANG_ports_s
	{
		wxTreeItemId Id;
		long num_of_rooms;
	} YANG_ports_t;

	YANG_ports_t m_ports[SOURCEPORT_NUMOFPORTS];

	DECLARE_EVENT_TABLE()
};

#endif
