/**************************************************************************

Copyright 2011-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/filename.h>
#endif

#include "tcmodselect_dialog.h"
#include "yang.h"
#include "yang.xpm"

BEGIN_EVENT_TABLE(TCMODSelectDialog, YANGDialog)

EVT_BUTTON(wxID_OK, TCMODSelectDialog::OnOk)
EVT_BUTTON(wxID_CANCEL, TCMODSelectDialog::OnCancel)

EVT_CHOICE(ID_TCMODSELECTPROFILE, TCMODSelectDialog::OnSelectCustomProfile)

END_EVENT_TABLE()

TCMODSelectDialog::TCMODSelectDialog( wxWindow* parent, GameType game, const wxString& modname, const wxString& modconfile, const wxString& moddeffile, bool showmodurl, const wxString& modurl, const wxArrayString& modfiles ) : YANGDialog( parent, wxID_ANY, wxT("Select a TC/MOD profile"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE )
{
	wxBoxSizer* TCMODSelectDialogbSizer = new wxBoxSizer( wxVERTICAL );

	m_TCMODSelectpanel = new YANGPanel( this , wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* TCMODSelectpanelbSizer = new wxBoxSizer( wxVERTICAL );

	wxGridBagSizer* TCMODSelectpanelgbSizer = new wxGridBagSizer( 0, 0 );
	TCMODSelectpanelgbSizer->SetFlexibleDirection( wxBOTH );
	TCMODSelectpanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_TCMODSelectroomnamestaticText = new wxStaticText( m_TCMODSelectpanel, wxID_ANY, wxT("Select one of your configured TC/MOD profiles,\nmatching the following TC/MOD profile,\nthat the host has selected on his side:"), wxDefaultPosition, wxDefaultSize, 0 );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelectroomnamestaticText, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	if (showmodurl)
	{
		m_TCMODSelecthyperlink = new YANGHyperlinkCtrl( m_TCMODSelectpanel, wxID_ANY, modname, modurl, wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE );
		TCMODSelectpanelgbSizer->Add( m_TCMODSelecthyperlink, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL, 5 );
	}
	else
	{
		m_TCMODSelectstaticText = new wxStaticText( m_TCMODSelectpanel, wxID_ANY, modname, wxDefaultPosition, wxDefaultSize, 0 );
		TCMODSelectpanelgbSizer->Add( m_TCMODSelectstaticText, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	}

	m_TCMODSelectprofilestaticText = new wxStaticText( m_TCMODSelectpanel, wxID_ANY, wxT("TC/MOD profile:"), wxDefaultPosition, wxDefaultSize, 0 );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelectprofilestaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_TCMODSelectprofilechoice = new YANGChoice( m_TCMODSelectpanel, ID_TCMODSELECTPROFILE, wxDefaultPosition, wxDLG_UNIT(this, wxSize(160, -1)));
	TCMODSelectpanelgbSizer->Add( m_TCMODSelectprofilechoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_TCMODSelectstaticline = new wxStaticLine( m_TCMODSelectpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelectstaticline, wxGBPosition( 3, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_TCMODSelecthostmodfilesstaticText = new wxStaticText( m_TCMODSelectpanel, wxID_ANY, wxT("Host TC/MOD config:"), wxDefaultPosition, wxDefaultSize, 0 );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelecthostmodfilesstaticText, wxGBPosition( 4, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_TCMODSelectclientmodfilesstaticText = new wxStaticText( m_TCMODSelectpanel, wxID_ANY, wxT("Your TC/MOD config:"), wxDefaultPosition, wxDefaultSize, 0 );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelectclientmodfilesstaticText, wxGBPosition( 4, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_TCMODSelectmodfilesstaticText = new wxStaticText( m_TCMODSelectpanel, wxID_ANY, wxT("TC/MOD files:"), wxDefaultPosition, wxDefaultSize, 0 );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelectmodfilesstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_TCMODSelecthostmodfileslistCtrl = new YANGListCtrl( m_TCMODSelectpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, 120)), wxLC_REPORT|wxLC_NO_HEADER );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelecthostmodfileslistCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_TCMODSelectclientmodfileslistCtrl = new YANGListCtrl( m_TCMODSelectpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, 120)), wxLC_REPORT|wxLC_NO_HEADER );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelectclientmodfileslistCtrl, wxGBPosition( 5, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	if (game == GAME_DN3D)
	{
		m_TCMODSelectconfilestaticText = new wxStaticText( m_TCMODSelectpanel, wxID_ANY, wxT("Main CON file:"), wxDefaultPosition, wxDefaultSize, 0 );
		TCMODSelectpanelgbSizer->Add( m_TCMODSelectconfilestaticText, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_TCMODSelecthostconfiletextCtrl = new YANGTextCtrl( m_TCMODSelectpanel, wxID_ANY, modconfile, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, -1)), wxTE_READONLY );
		TCMODSelectpanelgbSizer->Add( m_TCMODSelecthostconfiletextCtrl, wxGBPosition( 6, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

		m_TCMODSelectclientconfiletextCtrl = new YANGTextCtrl( m_TCMODSelectpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, -1)), wxTE_READONLY );
		TCMODSelectpanelgbSizer->Add( m_TCMODSelectclientconfiletextCtrl, wxGBPosition( 6, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );
	}

	m_TCMODSelectdeffilestaticText = new wxStaticText( m_TCMODSelectpanel, wxID_ANY, wxT("Main DEF file:"), wxDefaultPosition, wxDefaultSize, 0 );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelectdeffilestaticText, wxGBPosition( game == GAME_DN3D ? 7 : 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_TCMODSelecthostdeffiletextCtrl = new YANGTextCtrl( m_TCMODSelectpanel, wxID_ANY, moddeffile, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, -1)), wxTE_READONLY );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelecthostdeffiletextCtrl, wxGBPosition( game == GAME_DN3D ? 7 : 6, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_TCMODSelectclientdeffiletextCtrl = new YANGTextCtrl( m_TCMODSelectpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, -1)), wxTE_READONLY );
	TCMODSelectpanelgbSizer->Add( m_TCMODSelectclientdeffiletextCtrl, wxGBPosition( game == GAME_DN3D ? 7 : 6, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	TCMODSelectpanelbSizer->Add( TCMODSelectpanelgbSizer, 1, wxEXPAND, 0 );

	m_TCMODSelectsdbSizer = new wxStdDialogButtonSizer();
	m_TCMODSelectsdbSizerOK = new YANGButton( m_TCMODSelectpanel, wxID_OK );
	m_TCMODSelectsdbSizer->AddButton( m_TCMODSelectsdbSizerOK );
	m_TCMODSelectsdbSizerCancel = new YANGButton( m_TCMODSelectpanel, wxID_CANCEL );
	m_TCMODSelectsdbSizer->AddButton( m_TCMODSelectsdbSizerCancel );
	m_TCMODSelectsdbSizer->Realize();
	TCMODSelectpanelbSizer->Add( m_TCMODSelectsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxTOP | wxBOTTOM, 5 );

	m_TCMODSelectpanel->SetSizer( TCMODSelectpanelbSizer );
	m_TCMODSelectpanel->Layout();
	TCMODSelectpanelbSizer->Fit( m_TCMODSelectpanel );
	TCMODSelectDialogbSizer->Add( m_TCMODSelectpanel, 1, wxEXPAND | wxALL, 0 );

	SetSizer( TCMODSelectDialogbSizer );
	Layout();
	TCMODSelectDialogbSizer->Fit( this );

	Centre();
	SetIcon(wxIcon(yang_xpm));
	m_game = game;
	m_modfiles = modfiles;


	switch (game)
	{
		case GAME_DN3D:
			g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

			while (g_configuration->dn3dtcmod_profile_current)
			{
				m_TCMODSelectprofilechoice->Append(g_configuration->dn3dtcmod_profile_current->profilename);

				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;
			}

			m_TCMODSelectprofilechoice->SetStringSelection(g_configuration->gamelaunch_dn3dtcmodselect_profilename);

			if (m_TCMODSelectprofilechoice->GetSelection() == wxNOT_FOUND)
			{
				m_TCMODSelectprofilechoice->SetSelection(0);

				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;
			}
			else
			{
				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

				while (g_configuration->dn3dtcmod_profile_current->profilename != g_configuration->gamelaunch_dn3dtcmodselect_profilename)
					g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;
			}

			break;

		case GAME_SW:
			g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

			while (g_configuration->swtcmod_profile_current)
			{
				m_TCMODSelectprofilechoice->Append(g_configuration->swtcmod_profile_current->profilename);

				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;
			}

			m_TCMODSelectprofilechoice->SetStringSelection(g_configuration->gamelaunch_swtcmodselect_profilename);

			if (m_TCMODSelectprofilechoice->GetSelection() == wxNOT_FOUND)
			{
				m_TCMODSelectprofilechoice->SetSelection(0);

				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;
			}
			else
			{
				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

				while (g_configuration->swtcmod_profile_current->profilename != g_configuration->gamelaunch_swtcmodselect_profilename)
					g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;
			}

			break;
		default: ;
	}

	UpdateFilesList();
}

void TCMODSelectDialog::OnSelectCustomProfile(wxCommandEvent& WXUNUSED(event))
{
	wxString l_temp_str = m_TCMODSelectprofilechoice->GetStringSelection();


	switch (m_game)
	{
		case GAME_DN3D:
			g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

			while (g_configuration->dn3dtcmod_profile_current->profilename != l_temp_str)
				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;

			break;

		case GAME_SW:
			g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

			while (g_configuration->swtcmod_profile_current->profilename != l_temp_str)
				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;

			break;
		default: ;
	}

	UpdateFilesList();
}

void TCMODSelectDialog::UpdateFilesList()
{
	wxArrayString l_modfiles;
	size_t l_loop_var, l_num_of_items, l_num_of_files;


	switch (m_game)
	{
		case GAME_DN3D:
			l_num_of_files = g_configuration->dn3dtcmod_profile_current->modfiles.GetCount();

			for (l_loop_var = 0; l_loop_var < l_num_of_files; l_loop_var++)
				l_modfiles.Add(((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName());

			m_TCMODSelectclientconfiletextCtrl->SetValue(g_configuration->dn3dtcmod_profile_current->confile);

			if (!m_TCMODSelecthostconfiletextCtrl->GetValue().IsSameAs(g_configuration->dn3dtcmod_profile_current->confile, false))
			{
				m_TCMODSelecthostconfiletextCtrl->SetForegroundColour(*wxRED);
				m_TCMODSelectclientconfiletextCtrl->SetForegroundColour(*wxRED);
			}
			else if (g_configuration->theme_is_dark)
			{
				m_TCMODSelecthostconfiletextCtrl->SetForegroundColour(*wxGREEN);
				m_TCMODSelectclientconfiletextCtrl->SetForegroundColour(*wxGREEN);
			}
			else
			{
				m_TCMODSelecthostconfiletextCtrl->SetForegroundColour(*wxBLACK);
				m_TCMODSelectclientconfiletextCtrl->SetForegroundColour(*wxBLACK);
			}

			m_TCMODSelecthostconfiletextCtrl->Refresh();
			m_TCMODSelectclientconfiletextCtrl->Refresh();

			m_TCMODSelectclientdeffiletextCtrl->SetValue(g_configuration->dn3dtcmod_profile_current->deffile);

			if (!m_TCMODSelecthostdeffiletextCtrl->GetValue().IsSameAs(g_configuration->dn3dtcmod_profile_current->deffile, false))
			{
				m_TCMODSelecthostdeffiletextCtrl->SetForegroundColour(*wxRED);
				m_TCMODSelectclientdeffiletextCtrl->SetForegroundColour(*wxRED);
			}
			else if (g_configuration->theme_is_dark)
			{
				m_TCMODSelecthostdeffiletextCtrl->SetForegroundColour(*wxGREEN);
				m_TCMODSelectclientdeffiletextCtrl->SetForegroundColour(*wxGREEN);
			}
			else
			{
				m_TCMODSelecthostdeffiletextCtrl->SetForegroundColour(*wxBLACK);
				m_TCMODSelectclientdeffiletextCtrl->SetForegroundColour(*wxBLACK);
			}

			m_TCMODSelecthostdeffiletextCtrl->Refresh();
			m_TCMODSelectclientdeffiletextCtrl->Refresh();

			break;

		case GAME_SW:
			l_num_of_files = g_configuration->swtcmod_profile_current->modfiles.GetCount();

			for (l_loop_var = 0; l_loop_var < l_num_of_files; l_loop_var++)
				l_modfiles.Add(((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName());

			m_TCMODSelectclientdeffiletextCtrl->SetValue(g_configuration->swtcmod_profile_current->deffile);

			if (!m_TCMODSelecthostdeffiletextCtrl->GetValue().IsSameAs(g_configuration->swtcmod_profile_current->deffile, false))
			{
				m_TCMODSelecthostdeffiletextCtrl->SetForegroundColour(*wxRED);
				m_TCMODSelectclientdeffiletextCtrl->SetForegroundColour(*wxRED);
			}
			else if (g_configuration->theme_is_dark)
			{
				m_TCMODSelecthostdeffiletextCtrl->SetForegroundColour(*wxGREEN);
				m_TCMODSelectclientdeffiletextCtrl->SetForegroundColour(*wxGREEN);
			}
			else
			{
				m_TCMODSelecthostdeffiletextCtrl->SetForegroundColour(*wxBLACK);
				m_TCMODSelectclientdeffiletextCtrl->SetForegroundColour(*wxBLACK);
			}

			m_TCMODSelecthostdeffiletextCtrl->Refresh();
			m_TCMODSelectclientdeffiletextCtrl->Refresh();

			break;
		default: ;
	}


	l_num_of_items = m_modfiles.GetCount();

	m_TCMODSelecthostmodfileslistCtrl->ClearAll();
	m_TCMODSelecthostmodfileslistCtrl->InsertColumn(0, wxEmptyString);

	for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
	{
		m_TCMODSelecthostmodfileslistCtrl->InsertItem(l_loop_var, m_modfiles[l_loop_var]);

		if (l_modfiles.Index(m_modfiles[l_loop_var], false) == wxNOT_FOUND)
			m_TCMODSelecthostmodfileslistCtrl->SetItemTextColour(l_loop_var, *wxRED);
		else if (g_configuration->theme_is_dark)
			m_TCMODSelecthostmodfileslistCtrl->SetItemTextColour(l_loop_var, *wxGREEN);
	}

	m_TCMODSelecthostmodfileslistCtrl->SetColumnWidth(0, wxLIST_AUTOSIZE);


	m_TCMODSelectclientmodfileslistCtrl->ClearAll();
	m_TCMODSelectclientmodfileslistCtrl->InsertColumn(0, wxEmptyString);

	for (l_loop_var = 0; l_loop_var < l_num_of_files; l_loop_var++)
	{
		m_TCMODSelectclientmodfileslistCtrl->InsertItem(l_loop_var, l_modfiles[l_loop_var]);

		if (m_modfiles.Index(l_modfiles[l_loop_var], false) == wxNOT_FOUND)
			m_TCMODSelectclientmodfileslistCtrl->SetItemTextColour(l_loop_var, *wxBLUE);
		else if (g_configuration->theme_is_dark)
			m_TCMODSelectclientmodfileslistCtrl->SetItemTextColour(l_loop_var, *wxGREEN);
	}

	m_TCMODSelectclientmodfileslistCtrl->SetColumnWidth(0, wxLIST_AUTOSIZE);
}

void TCMODSelectDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	switch (m_game)
	{
		case GAME_DN3D: g_configuration->gamelaunch_dn3dtcmodselect_profilename = m_TCMODSelectprofilechoice->GetStringSelection(); break;
		case GAME_SW:   g_configuration->gamelaunch_swtcmodselect_profilename = m_TCMODSelectprofilechoice->GetStringSelection(); break;
		default: ;
	}

	g_configuration->Save();

	tcmodprofile_selected = true;

	EndModal(0);
}

void TCMODSelectDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	tcmodprofile_selected = false;

	EndModal(0);
}
