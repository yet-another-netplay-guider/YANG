/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_COLOREDCLASSES_H_
#define _YANG_COLOREDCLASSES_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/frame.h>
#include <wx/dialog.h>
#include <wx/panel.h>
#include <wx/listbook.h>
#include <wx/notebook.h>
#include <wx/menu.h>	
#include <wx/statusbr.h>
#include <wx/button.h>
#include <wx/bmpbuttn.h>
#include <wx/tglbtn.h>
#include <wx/stattext.h>
#include <wx/checkbox.h>
#include <wx/radiobut.h>
#include <wx/textctrl.h>
#include <wx/hyperlink.h>
#include <wx/listbox.h>
#include <wx/choice.h>
#include <wx/combobox.h>
#include <wx/listctrl.h>
#include <wx/treectrl.h>
#include <wx/statline.h>

#include <wx/platform.h>
#endif

//void SetStatusBarThemeColour(wxStatusBar* status_bar);

class YANGFrame: public wxFrame
{
public:
	YANGFrame();
	YANGFrame(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE, const wxString& name = wxT("frame"));
	//wxStatusBar* CreateStatusBar(int number = 1, long style = wxST_SIZEGRIP | wxFULL_REPAINT_ON_RESIZE, wxWindowID id = 0, const wxString& name = wxT("statusBar"));
	//wxStatusBar* OnCreateStatusBar(int number, long style, wxWindowID id, const wxString& name);
	void SetThemeColour();
};

class YANGDialog: public wxDialog
{
public:
	YANGDialog();
	YANGDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE, const wxString& name = wxT("dialogBox"));
	void SetThemeColour();
};

class YANGPanel: public wxPanel
{
public:
	YANGPanel();
	YANGPanel(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxTAB_TRAVERSAL, const wxString& name = wxT("panel"));
	void SetThemeColour();
};

class YANGListbook: public wxListbook
{
public:
	YANGListbook();
	YANGListbook(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxString& name = wxEmptyString);
	void SetThemeColour();
};

class YANGNotebook: public wxNotebook
{
public:
	YANGNotebook();
	YANGNotebook(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxString& name = wxNotebookNameStr);
	void SetThemeColour();
};

#if (defined __WXMAC__) || (defined __WXCOCOA__)
#define YANGGeneralizedbook YANGListbook
#else
#define YANGGeneralizedbook YANGNotebook
#endif

/*
class YANGMenuBar: public wxMenuBar
{
public:
YANGMenuBar(long style = 0);
YANGMenuBar(size_t n, wxMenu* menus[], const wxString titles[], long style = 0);
void SetThemeColour();
};
*/
class YANGButton: public wxButton
{
public:
	YANGButton();
	YANGButton(wxWindow *parent, wxWindowID id,
		const wxString& label = wxEmptyString,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize, long style = 0,
		const wxValidator& validator = wxDefaultValidator,
		const wxString& name = wxButtonNameStr);
	void SetThemeColour();
};

class YANGBitmapButton: public wxBitmapButton
{
public:
	YANGBitmapButton();
	YANGBitmapButton(wxWindow *parent, wxWindowID id,
		const wxBitmap& bitmap,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize, long style = wxBU_AUTODRAW,
		const wxValidator& validator = wxDefaultValidator,
		const wxString& name = wxT("button"));
	void SetThemeColour();
};

class YANGToggleButton: public wxToggleButton
{
public:
	YANGToggleButton();
	YANGToggleButton(wxWindow* parent, wxWindowID id, const wxString& label,
		const wxPoint& pos = wxDefaultPosition,
		const wxSize& size = wxDefaultSize, long style = 0,
		const wxValidator& val = wxDefaultValidator,
		const wxString& name = wxT("checkBox"));
	void SetThemeColour();
};

class YANGCheckBox: public wxCheckBox
{
public:
	YANGCheckBox();
	YANGCheckBox(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& val = wxDefaultValidator, const wxString& name = wxT("checkBox"));
	void SetThemeColour();
};

class YANGRadioButton: public wxRadioButton
{
public:
	YANGRadioButton();
	YANGRadioButton(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("radioButton"));
	void SetThemeColour();
};

class YANGTextCtrl: public wxTextCtrl
{
public:
	YANGTextCtrl();
	YANGTextCtrl(wxWindow* parent, wxWindowID id, const wxString& value = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxTextCtrlNameStr);
	void SetThemeColour();
};

class YANGHyperlinkCtrl: public wxHyperlinkCtrl
{
public:
	YANGHyperlinkCtrl();
	YANGHyperlinkCtrl(wxWindow* parent, wxWindowID id, const wxString & label, const wxString & url, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHL_DEFAULT_STYLE, const wxString& name = wxT("hyperlink"));
	void SetThemeColour();
};

class YANGListBox: public wxListBox
{
public:
	YANGListBox();
	YANGListBox(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("listBox"));
	YANGListBox(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("listBox"));
	void SetThemeColour();
};

class YANGChoice: public wxChoice
{
public:
	YANGChoice();
	YANGChoice(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("choice"));
	YANGChoice(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("choice"));
	void SetThemeColour();
};

class YANGComboBox: public wxComboBox
{
public:
	YANGComboBox();
	YANGComboBox(wxWindow* parent, wxWindowID id, const wxString& value = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, int n = 0, const wxString choices[] = NULL, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("comboBox"));
	YANGComboBox(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("comboBox"));
	void SetThemeColour();
};

class YANGListCtrl: public wxListCtrl
{
public:
	YANGListCtrl();
	YANGListCtrl(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxLC_ICON, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxListCtrlNameStr);
	void SetThemeColour();
};

class YANGTreeCtrl: public wxTreeCtrl
{
public:
	YANGTreeCtrl();
	YANGTreeCtrl(wxWindow* parent, wxWindowID id, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxTR_HAS_BUTTONS, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxT("treeCtrl"));
	void SetThemeColour();
};

class YANGStaticLine: public wxStaticLine
{
public:
	YANGStaticLine();
	YANGStaticLine(wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxLI_HORIZONTAL, const wxString& name = wxT("staticLine"));
	void SetThemeColour();
};

#endif
