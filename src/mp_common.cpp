/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/string.h>
#endif

#include "mp_common.h"

RoomPlayersTable::RoomPlayersTable()
{
	num_players = 0;
}

bool RoomPlayersTable::Add(const wxString& nickname,
						   const wxString& ingameip, long game_port_number)
{
	if (num_players < MAX_NUM_PLAYERS)
	{
		players[num_players].nickname = nickname;
		players[num_players].ingameip = ingameip;
		players[num_players].game_port_number = game_port_number;
		num_players++;
		return true;
	}
	return false;
}

bool RoomPlayersTable::DelByIndex(size_t index)
{
	if (index < num_players)
	{
		index++;
		while (index < num_players)
		{
			players[index-1] = players[index];
			index++;
		}
		num_players--;
		return true;
	}
	return false;
}

size_t RoomPlayersTable::FindIndexByNick(const wxString& nickname)
{
	size_t l_loop_var = 0;
	while (l_loop_var < num_players)
	{
		if (players[l_loop_var].nickname == nickname)
			return l_loop_var;
		l_loop_var++;
	}
	return l_loop_var; // An indication for not finding.
}

// For the host only, at least for now.

ExtendedRoomPlayersTable::ExtendedRoomPlayersTable() : RoomPlayersTable()
{
}

bool ExtendedRoomPlayersTable::Add(const wxString& nickname,
								   const wxString& ingameip, long game_port_number,
								   const wxString& os_description, bool ready)
{
	if (num_players < MAX_NUM_PLAYERS)
	{
		players[num_players].nickname = nickname;
		players[num_players].ingameip = ingameip;
		players[num_players].game_port_number = game_port_number;
		players[num_players].os_description = os_description;
		players[num_players].ready = ready;
		requests[num_players].transferrequest = TRANSFERREQUEST_NONE;
		//  requests[num_players].awaiting_download = false;
		num_players++;
		return true;
	}
	return false;
}

bool ExtendedRoomPlayersTable::DelByIndex(size_t index)
{
	if (index < num_players)
	{
		index++;
		while (index < num_players)
		{
			players[index-1] = players[index];
			requests[index-1] = requests[index];
			index++;
		}
		num_players--;
		return true;
	}
	return false;
}

/*
ExtendedRoomPlayersTable::ExtendedRoomPlayersTable()
{
num_players = 0;
}

bool ExtendedRoomPlayersTable::Add(const wxString& nickname,
const wxString& ingameip, long game_port_number)
{
if (num_players < MAX_NUM_PLAYERS)
{
players[num_players].nickname = nickname;
players[num_players].ingameip = ingameip;
players[num_players].game_port_number = game_port_number;
num_players++;
return true;
}
return false;
}

bool ExtendedRoomPlayersTable::DelByIndex(size_t index)
{
if (index < num_players)
{
index++;
while (index < num_players)
{
players[index-1] = players[index];
index++;
}
num_players--;
return true;
}
return false;
}

size_t ExtendedRoomPlayersTable::FindIndexByNick(const wxString& nickname)
{
size_t l_loop_var = 0;
while (l_loop_var < num_players)
{
if (players[l_loop_var].nickname == nickname)
return l_loop_var;
l_loop_var++;
}
return l_loop_var; // An indication for not finding.
}
*/

// For the host only as well.
HostRoomClientsTable::HostRoomClientsTable()
{
	num_clients = 0;
}

bool HostRoomClientsTable::Add(wxSocketBase* sock,
							   const wxString& peerdetectedip,
							   //                             const wxString& selfdetectedip,
							   wxStopWatch* stopwatch)
{
	if (num_clients <= MAX_NUM_CONNECTED_CLIENTS) // <= and not <
		// One more for file-transfer.
	{
		clients[num_clients].sock = sock;
		clients[num_clients].peerdetectedip = peerdetectedip;
		//  clients[num_clients].selfdetectedip = selfdetectedip;
		clients[num_clients].stopwatch = stopwatch;
		num_clients++;
		return true;
	}
	return false;
}

bool HostRoomClientsTable::PutInRoom(size_t index, size_t inroom_num_clients)
{
	HostRoomClientData l_temp_client;
	//size_t l_loop_var;
	if ((index >= num_clients) || (inroom_num_clients >= num_clients))
		return false;
	// UPDATE: Timers are not used anymore, but for old reference...

	// Because timers are used, we must "move" the rest of the clients
	// which are not in-room by 1.
	//l_temp_client = clients[inroom_num_clients];
	l_temp_client = clients[index];
	//for (l_loop_var = index; l_loop_var > inroom_num_clients; l_loop_var--)
	//  clients[l_loop_var] = clients[l_loop_var-1];
	clients[inroom_num_clients] = l_temp_client;
	clients[index] = l_temp_client;
	return true;
}

bool HostRoomClientsTable::DelBySock(wxSocketBase* sock)
{
	size_t l_loop_var = 0;
	while (l_loop_var < num_clients)
		if (clients[l_loop_var].sock == sock)
		{
			l_loop_var++;
			while (l_loop_var < num_clients)
			{
				clients[l_loop_var-1] = clients[l_loop_var];
				l_loop_var++;
			}
			num_clients--;
			return true;
		}
		else
			l_loop_var++;
	return false;
}

bool HostRoomClientsTable::DelByIndex(size_t index)
{
	if (index < num_clients)
	{
		index++;
		while (index < num_clients)
		{
			clients[index-1] = clients[index];
			index++;
		}
		num_clients--;
		return true;
	}
	return false;
}

size_t HostRoomClientsTable::FindIndexBySock(wxSocketBase* sock)
{
	size_t l_loop_var = 0;
	while (l_loop_var < num_clients)
	{
		if (clients[l_loop_var].sock == sock)
			return l_loop_var;
		l_loop_var++;
	}
	return l_loop_var; // An indication for not finding.
}
