/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/
#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/string.h>
#include <wx/filename.h>
#include <wx/textfile.h>
#include <wx/toplevel.h>
#endif

#include "config.h"
#include "mp_common.h"
#include "yang.h"

void RestoreDefaultSounds(wxString& snd_join, wxString& snd_leave,
						  wxString& snd_send, wxString& snd_receive, wxString& snd_error)
{
#ifndef __WXMSW__
	if (wxFileExists(*g_launcher_resources_path + wxT("snddata/join.wav")))
		snd_join = *g_launcher_resources_path + wxT("snddata/join.wav");
	if (wxFileExists(*g_launcher_resources_path + wxT("snddata/leave.wav")))
		snd_leave = *g_launcher_resources_path + wxT("snddata/leave.wav");
	if (wxFileExists(*g_launcher_resources_path + wxT("snddata/send.wav")))
		snd_send = *g_launcher_resources_path + wxT("snddata/send.wav");
	if (wxFileExists(*g_launcher_resources_path + wxT("snddata/receive.wav")))
		snd_receive = *g_launcher_resources_path + wxT("snddata/receive.wav");
	if (wxFileExists(*g_launcher_resources_path + wxT("snddata/error.wav")))
		snd_error = *g_launcher_resources_path + wxT("snddata/error.wav");
#endif
	if (wxFileExists(wxT("snddata/join.wav")))
		snd_join = wxT("snddata/join.wav");
	if (wxFileExists(wxT("snddata/leave.wav")))
		snd_leave = wxT("snddata/leave.wav");
	if (wxFileExists(wxT("snddata/send.wav")))
		snd_send = wxT("snddata/send.wav");
	if (wxFileExists(wxT("snddata/receive.wav")))
		snd_receive = wxT("snddata/receive.wav");
	if (wxFileExists(wxT("snddata/error.wav")))
		snd_error = wxT("snddata/error.wav");
}

LauncherConfig::LauncherConfig()
{
	wxString l_temp_str;
	//size_t l_loop_var;

#if ASK_TO_ACCEPT_LICENSE
	// Is license agreement accepted?
	is_license_accepted = false;
#endif

	cfg_ver = 0;
	// Visual appearance
	theme_is_dark = use_custom_chat_text_layout = use_custom_chat_text_colors = false;
	chat_font_size = CUSTOM_FONT_DEFAULT_SIZE;
	chat_textcolor_red = chat_textcolor_green = chat_textcolor_blue = 0;
	chat_backcolor_red = chat_backcolor_green = chat_backcolor_blue = 255;
	/*for (size_t l_loop_var = 0; l_loop_var < COLORDIALOG_NUM_CUSTOM_COLORS; l_loop_var++)
	custcolor_red[l_loop_var] = custcolor_green[l_loop_var] = custcolor_blue[l_loop_var] = 127;*/

#if CHECK_FOR_UPDATES
	check_for_updates_on_startup = true;
#else
	check_for_updates_on_startup = false;
#endif

	gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODSW;
	gamelaunch_descent_source_port = SOURCEPORT_DOSDESCENT;
	gamelaunch_descent2_source_port = SOURCEPORT_DOSDESCENT2;
	gamelaunch_dn3d_source_port = SOURCEPORT_DOSDUKESW;
	gamelaunch_sw_source_port = SOURCEPORT_DOSSWSW;

	// May change depending on which ports are configured to be available.
	gamelaunch_source_port = SOURCEPORT_EDUKE32;
	gamelaunch_blood_use_crypticpassage = false;
	gamelaunch_blood_skill = BLOOD_DEFAULT_SKILL;
	gamelaunch_dn3d_skill = DN3D_DEFAULT_SKILL;
	gamelaunch_sw_skill = SW_DEFAULT_SKILL;
	gamelaunch_dn3d_skill_in_sp = DN3D_DEFAULT_SKILL_IN_SP;
	gamelaunch_sw_skill_in_sp = SW_DEFAULT_SKILL_IN_SP;
	// All gamelaunch-related strings should be initialized as an empty string by default.
	gamelaunch_bloodlvl = BLOOD_DEFAULT_LEVEL;
	gamelaunch_bloodlvl_cp = BLOOD_DEFAULT_LEVEL;
	gamelaunch_descentlvl = DESCENT_DEFAULT_LEVEL;
	gamelaunch_descent2lvl = DESCENT2_DEFAULT_LEVEL;
	gamelaunch_dn3dlvl = DN3D_DEFAULT_LEVEL;
	gamelaunch_swlvl = SW_DEFAULT_LEVEL;
	gamelaunch_blood_numofplayers = DEFAULT_NUM_PLAYERS;
	gamelaunch_descent_numofplayers = DEFAULT_NUM_PLAYERS;
	gamelaunch_descent2_numofplayers = DEFAULT_NUM_PLAYERS;
	gamelaunch_dn3d_numofplayers = DEFAULT_NUM_PLAYERS;
	gamelaunch_sw_numofplayers = DEFAULT_NUM_PLAYERS;
	gamelaunch_custom_numofplayers = DEFAULT_NUM_PLAYERS;
	gamelaunch_numoffakeplayers = 0;
	gamelaunch_enable_bot_ai = false;
	gamelaunch_bloodgametype_in_sp = (BloodMPGameT_Type)0;
	gamelaunch_dn3dgametype_in_sp = (DN3DMPGameT_Type)0;
	gamelaunch_bloodgametype = BLOODMPGAMETYPE_BB;
	gamelaunch_descentgametype = DESCENTMPGAMETYPE_ANARCHY;
	gamelaunch_descent2gametype = DESCENT2MPGAMETYPE_ANARCHY;
	gamelaunch_dn3dgametype = DN3DMPGAMETYPE_DMSPAWN;
	gamelaunch_swgametype = SWMPGAMETYPE_WBSPAWN;
	gamelaunch_dn3dspawn = DN3DSPAWN_NONE;
	gamelaunch_blood_monster = BLOODMPMONSTERTYPE_NONE;
	gamelaunch_blood_weapon = BLOODMPWEAPONTYPE_PERMANENT;
	gamelaunch_blood_item = BLOODMPITEMTYPE_RESPAWN;
	gamelaunch_blood_respawn = BLOODMPRESPAWNTYPE_RANDOM;
	gamelaunch_blood_packetmode = PACKETMODE_AUTO;
	gamelaunch_duke3dw_connectiontype = CONNECTIONTYPE_PEER2PEER;
	gamelaunch_eduke32_connectiontype = CONNECTIONTYPE_MASTERSLAVE;
	gamelaunch_custom_use_nullmodem = false;
	gamelaunch_blood_maxping = DEFAULT_MAXPING;
	gamelaunch_descent_maxping = DEFAULT_MAXPING;
	gamelaunch_descent2_maxping = DEFAULT_MAXPING;
	gamelaunch_dn3d_maxping = DEFAULT_MAXPING;
	gamelaunch_sw_maxping = DEFAULT_MAXPING;
	gamelaunch_custom_maxping = DEFAULT_MAXPING;
	gamelaunch_custom_rxdelay = DEFAULT_RXDELAY;
	gamelaunch_custom_use_profilename = true;
	gamelaunch_show_modurl = gamelaunch_advertise = gamelaunch_skip_settings = false;
	gamelaunch_dn3d_playercolnum = gamelaunch_sw_playercolnum = 0;
	gamelaunch_use_password = false;
	//last_serverlist_in_cycle = 0;
	// Many other strings should be empty by default as well.
	// But only if we want.
	//gamelaunch_enable_natfree =
	have_dosbloodsw = have_dosbloodrg = have_dosbloodpp = have_dosbloodou
		= have_dosdescent = have_d1xrebirth = have_dosdescent2 = have_d2xrebirth
		= have_dosdukesw = have_dosdukerg = have_dosdukeae = have_duke3dw = have_eduke32 = have_nduke = have_hduke = have_xduke
		= have_dosswsw = have_dosswrg = have_voidsw = have_swp = have_dosbox = dosbox_use_conf = have_bmouse
#ifndef __WXMSW__
		= have_wine
#endif
		= false;

	custom_profile_list = NULL;
	dn3dtcmod_profile_list = NULL;
	swtcmod_profile_list = NULL;

	dosbox_cdmount = CDROMMOUNT_NONE;
	blood_maps_dir = *g_launcher_user_profile_path + wxT("bloodmaps");
	descent_maps_dir = *g_launcher_user_profile_path + wxT("descentmaps");
	descent2_maps_dir = *g_launcher_user_profile_path + wxT("descent2maps");
	dn3d_maps_dir = *g_launcher_user_profile_path + wxT("dn3dmaps");
	sw_maps_dir = *g_launcher_user_profile_path + wxT("swmaps");
	duke3dw_userpath_use = nduke_userpath_use = hduke_userpath_use = false;
#ifdef __WXMSW__
	eduke32_userpath_use = voidsw_userpath_use = false;
	l_temp_str = wxGetenv(wxT("APPDATA"));
	eduke32_userpath = l_temp_str + wxT("\\EDuke32 Settings");
	duke3dw_userpath = l_temp_str + wxT("\\Duke3dw");
	voidsw_userpath = l_temp_str + wxT("\\VoidSW");

#else
	if (wxFileExists(wxT("/usr/bin/wine")))
		wine_exec = wxT("/usr/bin/wine");
	else if (wxFileExists(wxT("/usr/local/bin/wine")))
		wine_exec = wxT("/usr/local/bin/wine");
	l_temp_str = wxGetenv(wxT("HOME"));
	eduke32_userpath_use = voidsw_userpath_use = true;
#if (defined __WXMAC__) || (defined __WXCOCOA__)
	if (wxFileExists(wxT("/Applications/DOSBox.app/Contents/MacOS/DOSBox")))
		dosbox_exec = wxT("/Applications/DOSBox.app/Contents/MacOS/DOSBox");
	dosbox_conf = l_temp_str + wxT("/Library/Preferences/DOSBox Preferences");
//	eduke32_userpath = l_temp_str + wxT("/Library/Application Support/EDuke32");
//	duke3dw_userpath = l_temp_str + wxT("/Library/Preferences/Duke3dw");
//	nduke_userpath = l_temp_str + wxT("/Library/Application Support/nDuke");
//	hduke_userpath = l_temp_str + wxT("/Library/Application Support/hDuke");
//	voidsw_userpath = l_temp_str + wxT("/Library/Preferences/VoidSW");
	terminal_fullcmd = wxT("open");
#else
	if (wxFileExists(wxT("/usr/bin/eduke32")))
		eduke32_exec = wxT("/usr/bin/eduke32");
	else if (wxFileExists(wxT("/usr/games/eduke32")))
		eduke32_exec = wxT("/usr/games/eduke32");
	else if (wxFileExists(wxT("/usr/local/bin/eduke32")))
		eduke32_exec = wxT("/usr/local/bin/eduke32");
	if (wxFileExists(wxT("/usr/bin/voidsw")))
		voidsw_exec = wxT("/usr/bin/voidsw");
	else if (wxFileExists(wxT("/usr/games/voidsw")))
		voidsw_exec = wxT("/usr/games/voidsw");
	else if (wxFileExists(wxT("/usr/local/bin/voidsw")))
		voidsw_exec = wxT("/usr/local/bin/voidsw");
	if (wxFileExists(wxT("/usr/bin/dosbox")))
		dosbox_exec = wxT("/usr/bin/dosbox");
	else if (wxFileExists(wxT("/usr/games/dosbox")))
		dosbox_exec = wxT("/usr/games/dosbox");
	else if (wxFileExists(wxT("/usr/local/bin/dosbox")))
		dosbox_exec = wxT("/usr/local/bin/dosbox");
	if (wxDirExists(wxT("/media/cdrom")))
		dosbox_cdrom_location = wxT("/media/cdrom");
	else if (wxDirExists(wxT("/media/cdrom0")))
		dosbox_cdrom_location = wxT("/media/cdrom0");
	else if (wxDirExists(wxT("/mnt/cdrom")))
		dosbox_cdrom_location = wxT("/mnt/cdrom");
	else if (wxDirExists(wxT("/mnt/cdrom0")))
		dosbox_cdrom_location = wxT("/mnt/cdrom0");
	/*	if (wxFileExists(wxT("/dev/cdrom")))
	dosbox_cdrom_image = wxT("/dev/cdrom");
	else if (wxFileExists(wxT("/dev/cdrw")))
	dosbox_cdrom_image = wxT("/dev/cdrw");
	else if (wxFileExists(wxT("/dev/cdrecorder")))
	dosbox_cdrom_image = wxT("/dev/cdrecorder");
	else if (wxFileExists(wxT("/dev/dvd")))
	dosbox_cdrom_image = wxT("/dev/dvd");
	else if (wxFileExists(wxT("/dev/dvdrom")))
	dosbox_cdrom_image = wxT("/dev/dvdrom");
	else if (wxFileExists(wxT("/dev/dvdrw")))
	dosbox_cdrom_image = wxT("/dev/dvdrw");
	else if (wxFileExists(wxT("/dev/dvdrecorder")))
	dosbox_cdrom_image = wxT("/dev/dvdrecorder");
	else if (wxFileExists(wxT("/dev/dvdram")))
	dosbox_cdrom_image = wxT("/dev/dvdram");*/
	// Terminal - Empty as well. Well, at least for now.
#endif
	eduke32_userpath = l_temp_str + wxT("/.config/eduke32");
	duke3dw_userpath = l_temp_str + wxT("/.config/duke3dw");
	nduke_userpath = l_temp_str + wxT("/.config/nduke");
	hduke_userpath = l_temp_str + wxT("/.config/hduke");
	voidsw_userpath = l_temp_str + wxT("/.config/voidsw");
#endif

	if (yang_portable)
	{
		wxString exec;
		exec = wxString(".") + wxFILE_SEP_PATH + wxT("sourceports") + wxFILE_SEP_PATH + wxT("eduke32") + wxFILE_SEP_PATH + wxT("eduke32-oldmp")
#ifdef __WXMSW__
		       + wxT(".exe")
#endif
		;
		if (wxFileExists(exec))
		{
			eduke32_exec = exec;
			have_eduke32 = true;
		}
		exec = wxString(".") + wxFILE_SEP_PATH + wxT("sourceports") + wxFILE_SEP_PATH + wxT("xduke_variants") + wxFILE_SEP_PATH + wxT("nduke.exe");
		if (wxFileExists(exec))
		{
			nduke_exec = exec;
#ifdef __WXMSW__
			have_nduke = true;
#endif
		}
		exec = wxString(".") + wxFILE_SEP_PATH + wxT("sourceports") + wxFILE_SEP_PATH + wxT("xduke_variants") + wxFILE_SEP_PATH + wxT("hduke.exe");
		if (wxFileExists(exec))
		{
			hduke_exec = exec;
#ifdef __WXMSW__
			have_hduke = true;
#endif
		}
		exec = wxString(".") + wxFILE_SEP_PATH + wxT("sourceports") + wxFILE_SEP_PATH + wxT("xduke_variants") + wxFILE_SEP_PATH + wxT("duke3d_w32.exe");
		if (wxFileExists(exec))
		{
			xduke_exec = exec;
#ifdef __WXMSW__
			have_xduke = true;
#endif
		}
	}

	banlist_filepath = *g_launcher_user_profile_path + wxT("banlist.txt");
	// Overriden browser string - Ditto.
	override_browser = false;
	enable_localip_optimization = true;
#if ((defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__))
	override_soundcmd = false;
#elif (wxUSE_LIBSDL == 1)
	override_soundcmd = false;
#else
	// PulseAudio?
	if (wxFileExists(wxT("/usr/bin/paplay")))
	{
		playsound_cmd = wxT("/usr/bin/paplay");
		override_soundcmd = true;
	}
	else if (wxFileExists(wxT("/usr/local/bin/paplay")))
	{
		playsound_cmd = wxT("/usr/local/bin/paplay");
		override_soundcmd = true;
	}
	// ALSA?
	else if (wxFileExists(wxT("/usr/bin/aplay")))
	{
		playsound_cmd = wxT("/usr/bin/aplay");
		override_soundcmd = true;
	}
	else if (wxFileExists(wxT("/usr/local/bin/aplay")))
	{
		playsound_cmd = wxT("/usr/local/bin/aplay");
		override_soundcmd = true;
	}
	// ESD?
	else if (wxFileExists(wxT("/usr/bin/esdplay")))
	{
		playsound_cmd = wxT("/usr/bin/esdplay");
		override_soundcmd = true;
	}
	else if (wxFileExists(wxT("/usr/local/bin/esdplay")))
	{
		playsound_cmd = wxT("/usr/local/bin/esdplay");
		override_soundcmd = true;
	}
	else
		override_soundcmd = false;
#endif
	nickname = wxT("Build-game player");
	game_nickname = wxT("BuildGamer");

	mute_sounds_while_ingame = true;
	RestoreDefaultSounds(snd_join_file, snd_leave_file,
		snd_send_file, snd_receive_file, snd_error_file);
	play_snd_join = play_snd_leave = play_snd_send = play_snd_receive = play_snd_error = false;
	/*play_snd_join = (snd_join_file);
	play_snd_leave = (snd_leave_file);
	play_snd_send = (snd_send_file);
	play_snd_receive = (snd_receive_file);
	play_snd_error = (snd_error_file);*/

	game_port_number = DEFAULT_GAME_PORT_NUM;
	server_port_number = DEFAULT_SERVER_PORT_NUM;

	host_autoaccept_downloads = false;
	host_accept_wakeup = true;
	lookup_on_startup = false;

	show_timestamps = false;

	game_filter_tree = wxT("all");

#if 0
//	int winWidth;
//	wxDisplaySize(&winWidth, NULL);

#define SCALE_WIDTH(w) ((w) * 5)

	main_col_private_len = SCALE_WIDTH(17);
	main_col_loc_len = SCALE_WIDTH(15);
	main_col_roomname_len = SCALE_WIDTH(83);
	main_col_ping_len = SCALE_WIDTH(24);
	main_col_flux_len = SCALE_WIDTH(24);
	main_col_game_len = SCALE_WIDTH(60);
	main_col_srcport_len = SCALE_WIDTH(65);
	main_col_players_len = SCALE_WIDTH(34);
	main_col_map_len = SCALE_WIDTH(94);
	main_col_tcmod_len = SCALE_WIDTH(37);
	main_col_gametype_len = SCALE_WIDTH(72);
	main_col_skill_len = SCALE_WIDTH(59);
	main_col_ingame_len = SCALE_WIDTH(36);

	mainplayer_col_loc_len = SCALE_WIDTH(15);
	mainplayer_col_nick_len = SCALE_WIDTH(52);
	mainplayer_col_ip_len = SCALE_WIDTH(64);
	mainplayer_col_os_len = SCALE_WIDTH(111);

	host_col_ready_len = SCALE_WIDTH(23);
	host_col_loc_len = SCALE_WIDTH(15);
	host_col_nick_len = SCALE_WIDTH(52);
	host_col_ping_len = SCALE_WIDTH(24);
	host_col_flux_len = SCALE_WIDTH(24);
	host_col_os_len = SCALE_WIDTH(111);
	host_col_detectedip_len = SCALE_WIDTH(64);
	host_col_ingameip_len = SCALE_WIDTH(87);
	host_col_requests_len = SCALE_WIDTH(40);
	host_col_filenames_len = SCALE_WIDTH(49);
	host_col_filesizes_len = SCALE_WIDTH(38);

	host_col_settings_len = SCALE_WIDTH(48);
	host_col_value_len = SCALE_WIDTH(94);

	client_col_ready_len = SCALE_WIDTH(23);
	client_col_loc_len = SCALE_WIDTH(15);
	client_col_nick_len = SCALE_WIDTH(52);
	client_col_ping_len = SCALE_WIDTH(24);
	client_col_flux_len = SCALE_WIDTH(24);
	client_col_os_len = SCALE_WIDTH(111);
	client_col_detectedip_len = SCALE_WIDTH(64);
	client_col_ingameip_len = SCALE_WIDTH(87);

	client_col_settings_len = SCALE_WIDTH(48);
	client_col_value_len = SCALE_WIDTH(94);

#undef SCALE_WIDTH

#else
	// This is bad, but - probably by chance - using just wxFrame was needed in 2.8
	wxFrame* l_temp_win = new MainFrame(NULL);
//	wxFrame* l_temp_win = new wxFrame();

	main_col_private_len = (wxDLG_UNIT(l_temp_win, wxSize(17, -1))).GetWidth();
	main_col_loc_len = (wxDLG_UNIT(l_temp_win, wxSize(15, -1))).GetWidth();
	main_col_roomname_len = (wxDLG_UNIT(l_temp_win, wxSize(83, -1))).GetWidth();
	main_col_ping_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
	main_col_flux_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
	main_col_game_len = (wxDLG_UNIT(l_temp_win, wxSize(60, -1))).GetWidth();
	main_col_srcport_len = (wxDLG_UNIT(l_temp_win, wxSize(65, -1))).GetWidth();
	main_col_players_len = (wxDLG_UNIT(l_temp_win, wxSize(34, -1))).GetWidth();
	main_col_map_len = (wxDLG_UNIT(l_temp_win, wxSize(94, -1))).GetWidth();
	main_col_tcmod_len = (wxDLG_UNIT(l_temp_win, wxSize(37, -1))).GetWidth();
	main_col_gametype_len = (wxDLG_UNIT(l_temp_win, wxSize(72, -1))).GetWidth();
	main_col_skill_len = (wxDLG_UNIT(l_temp_win, wxSize(59, -1))).GetWidth();
	main_col_ingame_len = (wxDLG_UNIT(l_temp_win, wxSize(36, -1))).GetWidth();

	mainplayer_col_loc_len = (wxDLG_UNIT(l_temp_win, wxSize(15, -1))).GetWidth();
	mainplayer_col_nick_len = (wxDLG_UNIT(l_temp_win, wxSize(52, -1))).GetWidth();
	mainplayer_col_ip_len = (wxDLG_UNIT(l_temp_win, wxSize(64, -1))).GetWidth();
	mainplayer_col_os_len = (wxDLG_UNIT(l_temp_win, wxSize(111, -1))).GetWidth();

	host_col_ready_len = (wxDLG_UNIT(l_temp_win, wxSize(23, -1))).GetWidth();
	host_col_loc_len = (wxDLG_UNIT(l_temp_win, wxSize(15, -1))).GetWidth();
	host_col_nick_len = (wxDLG_UNIT(l_temp_win, wxSize(52, -1))).GetWidth();
	host_col_ping_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
	host_col_flux_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
	host_col_os_len = (wxDLG_UNIT(l_temp_win, wxSize(111, -1))).GetWidth();
	host_col_detectedip_len = (wxDLG_UNIT(l_temp_win, wxSize(64, -1))).GetWidth();
	host_col_ingameip_len = (wxDLG_UNIT(l_temp_win, wxSize(87, -1))).GetWidth();
	host_col_requests_len = (wxDLG_UNIT(l_temp_win, wxSize(40, -1))).GetWidth();
	host_col_filenames_len = (wxDLG_UNIT(l_temp_win, wxSize(49, -1))).GetWidth();
	host_col_filesizes_len = (wxDLG_UNIT(l_temp_win, wxSize(38, -1))).GetWidth();

	host_col_settings_len = (wxDLG_UNIT(l_temp_win, wxSize(48, -1))).GetWidth();
	host_col_value_len = (wxDLG_UNIT(l_temp_win, wxSize(94, -1))).GetWidth();

	client_col_ready_len = (wxDLG_UNIT(l_temp_win, wxSize(23, -1))).GetWidth();
	client_col_loc_len = (wxDLG_UNIT(l_temp_win, wxSize(15, -1))).GetWidth();
	client_col_nick_len = (wxDLG_UNIT(l_temp_win, wxSize(52, -1))).GetWidth();
	client_col_ping_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
	client_col_flux_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
	client_col_os_len = (wxDLG_UNIT(l_temp_win, wxSize(111, -1))).GetWidth();
	client_col_detectedip_len = (wxDLG_UNIT(l_temp_win, wxSize(64, -1))).GetWidth();
	client_col_ingameip_len = (wxDLG_UNIT(l_temp_win, wxSize(87, -1))).GetWidth();

	client_col_settings_len = (wxDLG_UNIT(l_temp_win, wxSize(48, -1))).GetWidth();
	client_col_value_len = (wxDLG_UNIT(l_temp_win, wxSize(94, -1))).GetWidth();

	l_temp_win->Destroy();
#endif

	main_win_width = main_win_height = host_win_width = host_win_height
		= client_win_width = client_win_height = wxDefaultCoord;

	main_is_maximized = host_is_maximized = client_is_maximized = false;

#ifdef ENABLE_UPNP
	enable_upnp = false;
#endif
}

bool l_parseConfigString(wxTextFile& settingsfile, const wxString& key, wxString& value)
{
	size_t l_loop_var = settingsfile.GetLineCount();
	while (l_loop_var > 0)
	{
		l_loop_var--;
		if (!settingsfile[l_loop_var].Find(key + wxT(" = ")))
		{
			value = settingsfile[l_loop_var].Mid(key.Len()+3);
			return true;
		}
	}
	return false;
}

bool l_parseConfigBool(wxTextFile& settingsfile, const wxString& key, bool& value)
{
	wxString l_strvalue;
	if (!l_parseConfigString(settingsfile, key, l_strvalue))
		return false;
	if (l_strvalue == wxT("1"))
		value = 1;
	else if (l_strvalue == wxT("0"))
		value = 0;
	else
		return false;
	return true;
}

bool l_parseConfigLong(wxTextFile& settingsfile, const wxString& key, long& value)
{
	wxString l_strvalue;
	if (!l_parseConfigString(settingsfile, key, l_strvalue))
		return false;
	return (l_strvalue.ToLong(&value));
}

bool l_parseConfigULong(wxTextFile& settingsfile, const wxString& key, unsigned long& value)
{
	wxString l_strvalue;
	if (!l_parseConfigString(settingsfile, key, l_strvalue))
		return false;
	return (l_strvalue.ToULong(&value));
}

bool l_parseConfigInt(wxTextFile& settingsfile, const wxString& key, int& value)
{
	long l_longvalue;
	if (!l_parseConfigLong(settingsfile, key, l_longvalue))
		return false;
	value = (int)l_longvalue;
	return true;
}

bool l_parseConfigUChar(wxTextFile& settingsfile, const wxString& key, unsigned char& value)
{
	unsigned long l_ulongvalue;
	if (!l_parseConfigULong(settingsfile, key, l_ulongvalue))
		return false;
	value = (unsigned char)l_ulongvalue;
	return true;
}

bool LauncherConfig::Load()
{
	wxTextFile l_settingsfile;
	wxString l_temp_str, l_nickname, l_hostaddr;
	int l_temp_int1, l_temp_int2;
	long l_temp_long, l_temp_length;
	unsigned long l_temp_ulong;
	wxString l_temp_str1, l_temp_str2;
	int l_loop_var;

	if (wxFile::Access(YANG_CONFIG_FULLFILEPATH, wxFile::read))
	{
		l_settingsfile.Open(YANG_CONFIG_FULLFILEPATH);

		// Configuration version number
		l_parseConfigULong(l_settingsfile, wxT("cfg_ver"), cfg_ver);
#if ASK_TO_ACCEPT_LICENSE
		// Is license agreement accepted?
		l_parseConfigBool(l_settingsfile, wxT("license_accepted"), is_license_accepted);
#endif
		// Visual appearance
#if !((defined __WXMAC__) || (defined __WXCOCOA__))
		l_parseConfigBool(l_settingsfile, wxT("theme_is_dark"), theme_is_dark);
#endif      
		l_parseConfigBool(l_settingsfile, wxT("use_custom_chat_text_layout"), use_custom_chat_text_layout);
		l_parseConfigBool(l_settingsfile, wxT("use_custom_chat_text_colors"), use_custom_chat_text_colors);
		l_parseConfigString(l_settingsfile, wxT("chat_font"), chat_font);

		if (l_parseConfigString(l_settingsfile, wxT("chat_font_size"), l_temp_str))
		{
			if (l_temp_str.ToULong(&l_temp_ulong))
			{
				chat_font_size = (int)l_temp_ulong;
				if (chat_font_size < 0)
					chat_font_size = CUSTOM_FONT_DEFAULT_SIZE;
			}
		}

		l_parseConfigUChar(l_settingsfile, wxT("chat_textcolor_red"), chat_textcolor_red);
		l_parseConfigUChar(l_settingsfile, wxT("chat_textcolor_green"), chat_textcolor_green);
		l_parseConfigUChar(l_settingsfile, wxT("chat_textcolor_blue"), chat_textcolor_blue);
		l_parseConfigUChar(l_settingsfile, wxT("chat_backcolor_red"), chat_backcolor_red);
		l_parseConfigUChar(l_settingsfile, wxT("chat_backcolor_green"), chat_backcolor_green);
		l_parseConfigUChar(l_settingsfile, wxT("chat_backcolor_blue"), chat_backcolor_blue);
		/*    else
		for (l_temp_long1 = 0; l_temp_long1 < COLORDIALOG_NUM_CUSTOM_COLORS; l_temp_long1++)
		{
		l_temp_str1 = wxString::Format(wxT("custcolor_red%d = "), l_temp_long1);
		if (!temp_string.Find(l_temp_str1))
		{
		if (temp_string.Mid(l_temp_str1.Len()).ToULong(&l_temp_ulong))
		custcolor_red[l_temp_long1] = (unsigned char)l_temp_ulong;
		break;
		}
		l_temp_str1 = wxString::Format(wxT("custcolor_green%d = "), l_temp_long1);
		if (!temp_string.Find(l_temp_str1))
		{
		if (temp_string.Mid(l_temp_str1.Len()).ToULong(&l_temp_ulong))
		custcolor_green[l_temp_long1] = (unsigned char)l_temp_ulong;
		break;
		}
		l_temp_str1 = wxString::Format(wxT("custcolor_blue%d = "), l_temp_long1);
		if (!temp_string.Find(l_temp_str1))
		{
		if (temp_string.Mid(l_temp_str1.Len()).ToULong(&l_temp_ulong))
		custcolor_blue[l_temp_long1] = (unsigned char)l_temp_ulong;
		break;
		}
		}*/

#if CHECK_FOR_UPDATES
		// Check for updates on startup
		l_parseConfigBool(l_settingsfile, wxT("check_for_updates_on_startup"), check_for_updates_on_startup);
#endif
		// Single Player / Host settings
		l_parseConfigString(l_settingsfile, wxT("blood_source_port"), l_temp_str);
		if (l_temp_str == wxT("dosbloodsw"))
			gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODSW;
		else if (l_temp_str == wxT("dosbloodrg"))
			gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODRG;
		else if (l_temp_str == wxT("dosbloodpp"))
			gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODPP;
		else if (l_temp_str == wxT("dosbloodou"))
			gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODOU;

		l_parseConfigBool(l_settingsfile, wxT("blood_use_crypticpassage"), gamelaunch_blood_use_crypticpassage);

		l_parseConfigString(l_settingsfile, wxT("descent_source_port"), l_temp_str);
		if (l_temp_str == wxT("dosdescent"))
			gamelaunch_descent_source_port = SOURCEPORT_DOSDESCENT;
		else if (l_temp_str == wxT("d1xrebirth"))
			gamelaunch_descent_source_port = SOURCEPORT_D1XREBIRTH;

		l_parseConfigString(l_settingsfile, wxT("descent2_source_port"), l_temp_str);
		if (l_temp_str == wxT("dosdescent2"))
			gamelaunch_descent2_source_port = SOURCEPORT_DOSDESCENT2;
		else if (l_temp_str == wxT("d2xrebirth"))
			gamelaunch_descent2_source_port = SOURCEPORT_D2XREBIRTH;

		l_parseConfigString(l_settingsfile, wxT("dn3d_source_port"), l_temp_str);
		if (l_temp_str == wxT("dosdukesw"))
			gamelaunch_dn3d_source_port = SOURCEPORT_DOSDUKESW;
		else if (l_temp_str == wxT("dosdukerg"))
			gamelaunch_dn3d_source_port = SOURCEPORT_DOSDUKERG;
		else if (l_temp_str == wxT("dosdukeae"))
			gamelaunch_dn3d_source_port = SOURCEPORT_DOSDUKEAE;
		else if (l_temp_str == wxT("duke3dw"))
			gamelaunch_dn3d_source_port = SOURCEPORT_DUKE3DW;
		else if (l_temp_str == wxT("eduke32"))
			gamelaunch_dn3d_source_port = SOURCEPORT_EDUKE32;
		else if (l_temp_str == wxT("nduke"))
			gamelaunch_dn3d_source_port = SOURCEPORT_NDUKE;
		else if (l_temp_str == wxT("hduke"))
			gamelaunch_dn3d_source_port = SOURCEPORT_HDUKE;
		else if (l_temp_str == wxT("xduke"))
			gamelaunch_dn3d_source_port = SOURCEPORT_XDUKE;

		l_parseConfigString(l_settingsfile, wxT("sw_source_port"), l_temp_str);
		if (l_temp_str == wxT("dosswsw"))
			gamelaunch_sw_source_port = SOURCEPORT_DOSSWSW;
		else if (l_temp_str == wxT("dosswrg"))
			gamelaunch_sw_source_port = SOURCEPORT_DOSSWRG;
		else if (l_temp_str == wxT("voidsw"))
			gamelaunch_sw_source_port = SOURCEPORT_VOIDSW;
		else if (l_temp_str == wxT("swp"))
			gamelaunch_sw_source_port = SOURCEPORT_SWP;

		l_parseConfigString(l_settingsfile, wxT("source_port"), l_temp_str);
		if (l_temp_str == wxT("dosbloodsw"))
			gamelaunch_source_port = SOURCEPORT_DOSBLOODSW;
		else if (l_temp_str == wxT("dosbloodrg"))
			gamelaunch_source_port = SOURCEPORT_DOSBLOODRG;
		else if (l_temp_str == wxT("dosbloodpp"))
			gamelaunch_source_port = SOURCEPORT_DOSBLOODPP;
		else if (l_temp_str == wxT("dosbloodou"))
			gamelaunch_source_port = SOURCEPORT_DOSBLOODOU;
		else if (l_temp_str == wxT("dosdescent"))
			gamelaunch_source_port = SOURCEPORT_DOSDESCENT;
		else if (l_temp_str == wxT("d1xrebirth"))
			gamelaunch_source_port = SOURCEPORT_D1XREBIRTH;
		else if (l_temp_str == wxT("dosdescent2"))
			gamelaunch_source_port = SOURCEPORT_DOSDESCENT2;
		else if (l_temp_str == wxT("d2xrebirth"))
			gamelaunch_source_port = SOURCEPORT_D2XREBIRTH;
		else if (l_temp_str == wxT("dosdukesw"))
			gamelaunch_source_port = SOURCEPORT_DOSDUKESW;
		else if (l_temp_str == wxT("dosdukerg"))
			gamelaunch_source_port = SOURCEPORT_DOSDUKERG;
		else if (l_temp_str == wxT("dosdukeae"))
			gamelaunch_source_port = SOURCEPORT_DOSDUKEAE;
		else if (l_temp_str == wxT("duke3dw"))
			gamelaunch_source_port = SOURCEPORT_DUKE3DW;
		else if (l_temp_str == wxT("eduke32"))
			gamelaunch_source_port = SOURCEPORT_EDUKE32;
		else if (l_temp_str == wxT("nduke"))
			gamelaunch_source_port = SOURCEPORT_NDUKE;
		else if (l_temp_str == wxT("hduke"))
			gamelaunch_source_port = SOURCEPORT_HDUKE;
		else if (l_temp_str == wxT("xduke"))
			gamelaunch_source_port = SOURCEPORT_XDUKE;
		else if (l_temp_str == wxT("dosswsw"))
			gamelaunch_source_port = SOURCEPORT_DOSSWSW;
		else if (l_temp_str == wxT("dosswrg"))
			gamelaunch_source_port = SOURCEPORT_DOSSWRG;
		else if (l_temp_str == wxT("voidsw"))
			gamelaunch_source_port = SOURCEPORT_VOIDSW;
		else if (l_temp_str == wxT("swp"))
			gamelaunch_source_port = SOURCEPORT_SWP;
		else if (l_temp_str == wxT("custom"))
			gamelaunch_source_port = SOURCEPORT_CUSTOM;

		l_parseConfigString(l_settingsfile, wxT("custom_profilename_in_sp"), gamelaunch_custom_profilename_in_sp);
		l_parseConfigString(l_settingsfile, wxT("custom_profilename"), gamelaunch_custom_profilename);
		l_parseConfigString(l_settingsfile, wxT("customselect_profilename"), gamelaunch_customselect_profilename);
		l_parseConfigString(l_settingsfile, wxT("dn3dtcmod_profilename"), gamelaunch_dn3dtcmod_profilename);
		l_parseConfigString(l_settingsfile, wxT("dn3dtcmodselect_profilename"), gamelaunch_dn3dtcmodselect_profilename);
		l_parseConfigString(l_settingsfile, wxT("swtcmod_profilename"), gamelaunch_swtcmod_profilename);
		l_parseConfigString(l_settingsfile, wxT("swtcmodselect_profilename"), gamelaunch_swtcmodselect_profilename);

		l_parseConfigLong(l_settingsfile, wxT("blood_skill"), gamelaunch_blood_skill);
		l_parseConfigLong(l_settingsfile, wxT("dn3d_skill"), gamelaunch_dn3d_skill);
		l_parseConfigLong(l_settingsfile, wxT("sw_skill"), gamelaunch_sw_skill);
		l_parseConfigLong(l_settingsfile, wxT("dn3d_skill_in_sp"), gamelaunch_dn3d_skill_in_sp);
		l_parseConfigLong(l_settingsfile, wxT("sw_skill_in_sp"), gamelaunch_sw_skill_in_sp);
		l_parseConfigString(l_settingsfile, wxT("blood_args_in_sp"), gamelaunch_blood_args_in_sp);
		l_parseConfigString(l_settingsfile, wxT("descent_args_in_sp"), gamelaunch_descent_args_in_sp);
		l_parseConfigString(l_settingsfile, wxT("descent2_args_in_sp"), gamelaunch_descent2_args_in_sp);
		l_parseConfigString(l_settingsfile, wxT("dn3d_args_in_sp"), gamelaunch_dn3d_args_in_sp);
		l_parseConfigString(l_settingsfile, wxT("sw_args_in_sp"), gamelaunch_sw_args_in_sp);
		l_parseConfigLong(l_settingsfile, wxT("blood_level"), gamelaunch_bloodlvl);
		l_parseConfigLong(l_settingsfile, wxT("blood_level_cp"), gamelaunch_bloodlvl_cp);
		l_parseConfigLong(l_settingsfile, wxT("descent_level"), gamelaunch_descentlvl);
		l_parseConfigLong(l_settingsfile, wxT("descent2_level"), gamelaunch_descent2lvl);
		l_parseConfigLong(l_settingsfile, wxT("dn3d_level"), gamelaunch_dn3dlvl);
		l_parseConfigLong(l_settingsfile, wxT("sw_level"), gamelaunch_swlvl);
		l_parseConfigString(l_settingsfile, wxT("blood_usermap"), gamelaunch_bloodusermap);
		l_parseConfigString(l_settingsfile, wxT("descent_usermap"), gamelaunch_descentusermap);
		l_parseConfigString(l_settingsfile, wxT("descent2_usermap"), gamelaunch_descent2usermap);
		l_parseConfigString(l_settingsfile, wxT("dn3d_usermap"), gamelaunch_dn3dusermap);
		l_parseConfigString(l_settingsfile, wxT("sw_usermap"), gamelaunch_swusermap);
		l_parseConfigString(l_settingsfile, wxT("blood_room_name"), gamelaunch_blood_roomname);
		l_parseConfigString(l_settingsfile, wxT("descent_room_name"), gamelaunch_descent_roomname);
		l_parseConfigString(l_settingsfile, wxT("descent2_room_name"), gamelaunch_descent2_roomname);
		l_parseConfigString(l_settingsfile, wxT("dn3d_room_name"), gamelaunch_dn3d_roomname);
		l_parseConfigString(l_settingsfile, wxT("sw_room_name"), gamelaunch_sw_roomname);
		l_parseConfigString(l_settingsfile, wxT("custom_room_name"), gamelaunch_custom_roomname);
		l_parseConfigLong(l_settingsfile, wxT("blood_num_of_players"), gamelaunch_blood_numofplayers);
		l_parseConfigLong(l_settingsfile, wxT("descent_num_of_players"), gamelaunch_descent_numofplayers);
		l_parseConfigLong(l_settingsfile, wxT("descent2_num_of_players"), gamelaunch_descent2_numofplayers);
		l_parseConfigLong(l_settingsfile, wxT("dn3d_num_of_players"), gamelaunch_dn3d_numofplayers);
		l_parseConfigLong(l_settingsfile, wxT("sw_num_of_players"), gamelaunch_sw_numofplayers);
		l_parseConfigLong(l_settingsfile, wxT("custom_num_of_players"), gamelaunch_custom_numofplayers);
		l_parseConfigLong(l_settingsfile, wxT("num_of_fakeplayers"), gamelaunch_numoffakeplayers);
		l_parseConfigBool(l_settingsfile, wxT("enable_bot_ai"), gamelaunch_enable_bot_ai);

		l_parseConfigString(l_settingsfile, wxT("blood_mpgametype_in_sp"), l_temp_str);
		if (l_temp_str == wxT("single"))
			gamelaunch_bloodgametype_in_sp = (BloodMPGameT_Type)0;
		else if (l_temp_str == wxT("coop"))
			gamelaunch_bloodgametype_in_sp = BLOODMPGAMETYPE_COOP;
		else if (l_temp_str == wxT("bb"))
			gamelaunch_bloodgametype_in_sp = BLOODMPGAMETYPE_BB;
		else if (l_temp_str == wxT("teams"))
			gamelaunch_bloodgametype_in_sp = BLOODMPGAMETYPE_TEAMS;

		l_parseConfigString(l_settingsfile, wxT("dn3d_mpgametype_in_sp"), l_temp_str);
		if (l_temp_str == wxT("single"))
			gamelaunch_dn3dgametype_in_sp = (DN3DMPGameT_Type)0;
		else if (l_temp_str == wxT("dm_spawn"))
			gamelaunch_dn3dgametype_in_sp = DN3DMPGAMETYPE_DMSPAWN;
		else if (l_temp_str == wxT("coop"))
			gamelaunch_dn3dgametype_in_sp = DN3DMPGAMETYPE_COOP;
		else if (l_temp_str == wxT("dm_nospawn"))
			gamelaunch_dn3dgametype_in_sp = DN3DMPGAMETYPE_DMNOSPAWN;
		else if (l_temp_str == wxT("tdm_spawn"))
			gamelaunch_dn3dgametype_in_sp = DN3DMPGAMETYPE_TDMSPAWN;
		else if (l_temp_str == wxT("tdm_nospawn"))
			gamelaunch_dn3dgametype_in_sp = DN3DMPGAMETYPE_TDMNOSPAWN;

		l_parseConfigString(l_settingsfile, wxT("blood_mpgametype"), l_temp_str);
		if (l_temp_str == wxT("coop"))
			gamelaunch_bloodgametype = BLOODMPGAMETYPE_COOP;
		else if (l_temp_str == wxT("bb"))
			gamelaunch_bloodgametype = BLOODMPGAMETYPE_BB;
		else if (l_temp_str == wxT("teams"))
			gamelaunch_bloodgametype = BLOODMPGAMETYPE_TEAMS;

		l_parseConfigString(l_settingsfile, wxT("descent_mpgametype"), l_temp_str);
		if (l_temp_str == wxT("anarchy"))
			gamelaunch_descentgametype = DESCENTMPGAMETYPE_ANARCHY;
		else if (l_temp_str == wxT("team_anarchy"))
			gamelaunch_descentgametype = DESCENTMPGAMETYPE_TEAM_ANARCHY;
		else if (l_temp_str == wxT("robo_anarchy"))
			gamelaunch_descentgametype = DESCENTMPGAMETYPE_ROBO_ANARCHY;
		else if (l_temp_str == wxT("cooperative"))
			gamelaunch_descentgametype = DESCENTMPGAMETYPE_COOPERATIVE;

		l_parseConfigString(l_settingsfile, wxT("descent2_mpgametype"), l_temp_str);
		if (l_temp_str == wxT("anarchy"))
			gamelaunch_descent2gametype = DESCENT2MPGAMETYPE_ANARCHY;
		else if (l_temp_str == wxT("team_anarchy"))
			gamelaunch_descent2gametype = DESCENT2MPGAMETYPE_TEAM_ANARCHY;
		else if (l_temp_str == wxT("robo_anarchy"))
			gamelaunch_descent2gametype = DESCENT2MPGAMETYPE_ROBO_ANARCHY;
		else if (l_temp_str == wxT("cooperative"))
			gamelaunch_descent2gametype = DESCENT2MPGAMETYPE_COOPERATIVE;
		else if (l_temp_str == wxT("capture_the_flag"))
			gamelaunch_descent2gametype = DESCENT2MPGAMETYPE_CAPTURE_THE_FLAG;
		else if (l_temp_str == wxT("hoard"))
			gamelaunch_descent2gametype = DESCENT2MPGAMETYPE_HOARD;
		else if (l_temp_str == wxT("team_hoard"))
			gamelaunch_descent2gametype = DESCENT2MPGAMETYPE_TEAM_HOARD;

		l_parseConfigString(l_settingsfile, wxT("dn3d_mpgametype"), l_temp_str);
		if (l_temp_str == wxT("dm_spawn"))
			gamelaunch_dn3dgametype = DN3DMPGAMETYPE_DMSPAWN;
		else if (l_temp_str == wxT("coop"))
			gamelaunch_dn3dgametype = DN3DMPGAMETYPE_COOP;
		else if (l_temp_str == wxT("dm_nospawn"))
			gamelaunch_dn3dgametype = DN3DMPGAMETYPE_DMNOSPAWN;
		else if (l_temp_str == wxT("tdm_spawn"))
			gamelaunch_dn3dgametype = DN3DMPGAMETYPE_TDMSPAWN;
		else if (l_temp_str == wxT("tdm_nospawn"))
			gamelaunch_dn3dgametype = DN3DMPGAMETYPE_TDMNOSPAWN;

		l_parseConfigString(l_settingsfile, wxT("sw_mpgametype"), l_temp_str);
		if (l_temp_str == wxT("wb_spawn"))
			gamelaunch_swgametype = SWMPGAMETYPE_WBSPAWN;
		else if (l_temp_str == wxT("wb_nospawn"))
			gamelaunch_swgametype = SWMPGAMETYPE_WBNOSPAWN;
		else if (l_temp_str == wxT("coop"))
			gamelaunch_swgametype = SWMPGAMETYPE_COOP;

		l_parseConfigString(l_settingsfile, wxT("dn3d_spawntype"), l_temp_str);
		if (l_temp_str == wxT("none"))
			gamelaunch_dn3dspawn = DN3DSPAWN_NONE;
		else if (l_temp_str == wxT("monsters"))
			gamelaunch_dn3dspawn = DN3DSPAWN_MONSTERS;
		else if (l_temp_str == wxT("items"))
			gamelaunch_dn3dspawn = DN3DSPAWN_ITEMS;
		else if (l_temp_str == wxT("inventory"))
			gamelaunch_dn3dspawn = DN3DSPAWN_INVENTORY;
		else if (l_temp_str == wxT("all"))
			gamelaunch_dn3dspawn = DN3DSPAWN_ALL;

		l_parseConfigString(l_settingsfile, wxT("blood_mpmonstertype"), l_temp_str);
		if (l_temp_str == wxT("none"))
			gamelaunch_blood_monster = BLOODMPMONSTERTYPE_NONE;
		else if (l_temp_str == wxT("enabled"))
			gamelaunch_blood_monster = BLOODMPMONSTERTYPE_ENABLED;
		else if (l_temp_str == wxT("respawn"))
			gamelaunch_blood_monster = BLOODMPMONSTERTYPE_RESPAWN;

		l_parseConfigString(l_settingsfile, wxT("blood_mpweapontype"), l_temp_str);
		if (l_temp_str == wxT("norespawn"))
			gamelaunch_blood_weapon = BLOODMPWEAPONTYPE_NORESPAWN;
		else if (l_temp_str == wxT("permanent"))
			gamelaunch_blood_weapon = BLOODMPWEAPONTYPE_PERMANENT;
		else if (l_temp_str == wxT("respawn"))
			gamelaunch_blood_weapon = BLOODMPWEAPONTYPE_RESPAWN;
		else if (l_temp_str == wxT("markers"))
			gamelaunch_blood_weapon = BLOODMPWEAPONTYPE_MARKERS;

		l_parseConfigString(l_settingsfile, wxT("blood_mpitemtype"), l_temp_str);
		if (l_temp_str == wxT("norespawn"))
			gamelaunch_blood_item = BLOODMPITEMTYPE_NORESPAWN;
		else if (l_temp_str == wxT("respawn"))
			gamelaunch_blood_item = BLOODMPITEMTYPE_RESPAWN;
		else if (l_temp_str == wxT("markers"))
			gamelaunch_blood_item = BLOODMPITEMTYPE_MARKERS;

		l_parseConfigString(l_settingsfile, wxT("blood_mprespawntype"), l_temp_str);
		if (l_temp_str == wxT("random"))
			gamelaunch_blood_respawn = BLOODMPRESPAWNTYPE_RANDOM;
		else if (l_temp_str == wxT("weapons"))
			gamelaunch_blood_respawn = BLOODMPRESPAWNTYPE_WEAPONS;
		else if (l_temp_str == wxT("enemies"))
			gamelaunch_blood_respawn = BLOODMPRESPAWNTYPE_ENEMIES;

		l_parseConfigString(l_settingsfile, wxT("blood_packetmode"), l_temp_str);
		if (l_temp_str == wxT("auto"))
			gamelaunch_blood_packetmode = PACKETMODE_AUTO;
		else if (l_temp_str == wxT("broadcast"))
			gamelaunch_blood_packetmode = PACKETMODE_BROADCAST;
		else if (l_temp_str == wxT("masterslave"))
			gamelaunch_blood_packetmode = PACKETMODE_MASTERSLAVE;

		l_parseConfigString(l_settingsfile, wxT("duke3dw_connectiontype"), l_temp_str);
		if (l_temp_str == wxT("peer2peer"))
			gamelaunch_duke3dw_connectiontype = CONNECTIONTYPE_PEER2PEER;
		else if (l_temp_str == wxT("masterslave"))
			gamelaunch_duke3dw_connectiontype = CONNECTIONTYPE_MASTERSLAVE;

		l_parseConfigString(l_settingsfile, wxT("eduke32_connectiontype"), l_temp_str);
		if (l_temp_str == wxT("peer2peer"))
			gamelaunch_eduke32_connectiontype = CONNECTIONTYPE_PEER2PEER;
		else if (l_temp_str == wxT("masterslave"))
			gamelaunch_eduke32_connectiontype = CONNECTIONTYPE_MASTERSLAVE;
		else if (l_temp_str == wxT("serverclient"))
			gamelaunch_eduke32_connectiontype = CONNECTIONTYPE_SERVERCLIENT;

		l_parseConfigBool(l_settingsfile, wxT("custom_use_nullmodem"), gamelaunch_custom_use_nullmodem);

		l_parseConfigString(l_settingsfile, wxT("blood_args_for_host"), gamelaunch_blood_args_for_host);
		l_parseConfigString(l_settingsfile, wxT("descent_args_for_host"), gamelaunch_descent_args_for_host);
		l_parseConfigString(l_settingsfile, wxT("descent2_args_for_host"), gamelaunch_descent2_args_for_host);
		l_parseConfigString(l_settingsfile, wxT("dn3d_args_for_host"), gamelaunch_dn3d_args_for_host);
		l_parseConfigString(l_settingsfile, wxT("sw_args_for_host"), gamelaunch_sw_args_for_host);
		l_parseConfigString(l_settingsfile, wxT("blood_args_for_all"), gamelaunch_blood_args_for_all);
		l_parseConfigString(l_settingsfile, wxT("descent_args_for_all"), gamelaunch_descent_args_for_all);
		l_parseConfigString(l_settingsfile, wxT("descent2_args_for_all"), gamelaunch_descent2_args_for_all);
		l_parseConfigString(l_settingsfile, wxT("dn3d_args_for_all"), gamelaunch_dn3d_args_for_all);
		l_parseConfigString(l_settingsfile, wxT("sw_args_for_all"), gamelaunch_sw_args_for_all);
		l_parseConfigLong(l_settingsfile, wxT("blood_maxping"), gamelaunch_blood_maxping);
		l_parseConfigLong(l_settingsfile, wxT("descent_maxping"), gamelaunch_descent_maxping);
		l_parseConfigLong(l_settingsfile, wxT("descent2_maxping"), gamelaunch_descent2_maxping);
		l_parseConfigLong(l_settingsfile, wxT("dn3d_maxping"), gamelaunch_dn3d_maxping);
		l_parseConfigLong(l_settingsfile, wxT("sw_maxping"), gamelaunch_sw_maxping);
		l_parseConfigLong(l_settingsfile, wxT("custom_maxping"), gamelaunch_custom_maxping);
		l_parseConfigLong(l_settingsfile, wxT("custom_rxdelay"), gamelaunch_custom_rxdelay);
		l_parseConfigBool(l_settingsfile, wxT("custom_use_profilename"), gamelaunch_custom_use_profilename);
		l_parseConfigString(l_settingsfile, wxT("mod_name"), gamelaunch_modname);
		l_parseConfigBool(l_settingsfile, wxT("show_mod_url"), gamelaunch_show_modurl);
		l_parseConfigString(l_settingsfile, wxT("mod_url"), gamelaunch_modurl);
		l_parseConfigBool(l_settingsfile, wxT("advertise"), gamelaunch_advertise);
		l_parseConfigBool(l_settingsfile, wxT("skip_settings"), gamelaunch_skip_settings);
		l_parseConfigLong(l_settingsfile, wxT("dn3d_playercolnum"), gamelaunch_dn3d_playercolnum);
		l_parseConfigLong(l_settingsfile, wxT("sw_playercolnum"), gamelaunch_sw_playercolnum);
#if USE_PASSWORD
		l_parseConfigBool(l_settingsfile, wxT("use_password"), gamelaunch_use_password);
		l_parseConfigString(l_settingsfile, wxT("password"), gamelaunch_password);
#endif
		l_parseConfigBool(l_settingsfile, wxT("show_timestamps"), show_timestamps);
		/*    else
		if (!temp_string.Find(wxT("last_serverlist_in_cycle = ")))
		{
		if (!temp_string.Mid(27).ToULong(&last_serverlist_in_cycle))
		last_serverlist_in_cycle = 0;
		}
		else
		if (!temp_string.Find(wxT("enable_natfree = 1")))
		gamelaunch_enable_natfree = true;
		else
		if (!temp_string.Find(wxT("enable_natfree = 0")))
		gamelaunch_enable_natfree = false;*/
		// Source ports settings - Blood
		//    else

		// Source ports settings - Blood
		l_parseConfigBool(l_settingsfile, wxT("dosbloodsw"), have_dosbloodsw);
		l_parseConfigBool(l_settingsfile, wxT("dosbloodrg"), have_dosbloodrg);
		l_parseConfigBool(l_settingsfile, wxT("dosbloodpp"), have_dosbloodpp);
		l_parseConfigBool(l_settingsfile, wxT("dosbloodou"), have_dosbloodou);
		l_parseConfigString(l_settingsfile, wxT("dosbloodsw_exec"), dosbloodsw_exec);
		l_parseConfigString(l_settingsfile, wxT("dosbloodrg_exec"), dosbloodrg_exec);
		l_parseConfigString(l_settingsfile, wxT("dosbloodpp_exec"), dosbloodpp_exec);
		l_parseConfigString(l_settingsfile, wxT("dosbloodou_exec"), dosbloodou_exec);
		l_parseConfigString(l_settingsfile, wxT("blood_maps_dir"), blood_maps_dir);
		// Source ports settings - Descent
		l_parseConfigBool(l_settingsfile, wxT("dosdescent"), have_dosdescent);
		l_parseConfigBool(l_settingsfile, wxT("d1xrebirth"), have_d1xrebirth);
		l_parseConfigString(l_settingsfile, wxT("dosdescent_exec"), dosdescent_exec);
		l_parseConfigString(l_settingsfile, wxT("d1xrebirth_exec"), d1xrebirth_exec);
		l_parseConfigString(l_settingsfile, wxT("descent_maps_dir"), descent_maps_dir);
		// Source ports settings - Descent 2
		l_parseConfigBool(l_settingsfile, wxT("dosdescent2"), have_dosdescent2);
		l_parseConfigBool(l_settingsfile, wxT("d2xrebirth"), have_d2xrebirth);
		l_parseConfigString(l_settingsfile, wxT("dosdescent2_exec"), dosdescent2_exec);
		l_parseConfigString(l_settingsfile, wxT("d2xrebirth_exec"), d2xrebirth_exec);
		l_parseConfigString(l_settingsfile, wxT("descent2_maps_dir"), descent2_maps_dir);
		// Source ports settings - Duke Nukem 3D
		l_parseConfigBool(l_settingsfile, wxT("dosdukesw"), have_dosdukesw);
		l_parseConfigBool(l_settingsfile, wxT("dosdukerg"), have_dosdukerg);
		l_parseConfigBool(l_settingsfile, wxT("dosdukeae"), have_dosdukeae);
		l_parseConfigBool(l_settingsfile, wxT("duke3dw"), have_duke3dw);
		l_parseConfigBool(l_settingsfile, wxT("eduke32"), have_eduke32);
		l_parseConfigBool(l_settingsfile, wxT("nduke"), have_nduke);
		l_parseConfigBool(l_settingsfile, wxT("hduke"), have_hduke);
		l_parseConfigBool(l_settingsfile, wxT("xduke"), have_xduke);
		l_parseConfigString(l_settingsfile, wxT("dosdukesw_exec"), dosdukesw_exec);
		l_parseConfigString(l_settingsfile, wxT("dosdukerg_exec"), dosdukerg_exec);
		l_parseConfigString(l_settingsfile, wxT("dosdukeae_exec"), dosdukeae_exec);
		l_parseConfigString(l_settingsfile, wxT("duke3dw_exec"), duke3dw_exec);
		l_parseConfigString(l_settingsfile, wxT("eduke32_exec"), eduke32_exec);
		l_parseConfigString(l_settingsfile, wxT("nduke_exec"), nduke_exec);
		l_parseConfigString(l_settingsfile, wxT("hduke_exec"), hduke_exec);
		l_parseConfigString(l_settingsfile, wxT("xduke_exec"), xduke_exec);
		l_parseConfigString(l_settingsfile, wxT("dn3d_maps_dir"), dn3d_maps_dir);
		// Source ports settings - Shadow Warrior
		l_parseConfigBool(l_settingsfile, wxT("dosswsw"), have_dosswsw);
		l_parseConfigBool(l_settingsfile, wxT("dosswrg"), have_dosswrg);
		l_parseConfigBool(l_settingsfile, wxT("voidsw"), have_voidsw);
		l_parseConfigBool(l_settingsfile, wxT("swp"), have_swp);
		l_parseConfigString(l_settingsfile, wxT("dosswsw_exec"), dosswsw_exec);
		l_parseConfigString(l_settingsfile, wxT("dosswrg_exec"), dosswrg_exec);
		l_parseConfigString(l_settingsfile, wxT("voidsw_exec"), voidsw_exec);
		l_parseConfigString(l_settingsfile, wxT("swp_exec"), swp_exec);
		l_parseConfigString(l_settingsfile, wxT("sw_maps_dir"), sw_maps_dir);
		// DOSBox settings
		l_parseConfigBool(l_settingsfile, wxT("dosbox"), have_dosbox);
		l_parseConfigBool(l_settingsfile, wxT("dosbox_use_conf"), dosbox_use_conf);
		l_parseConfigBool(l_settingsfile, wxT("bmouse"), have_bmouse);

		l_parseConfigString(l_settingsfile, wxT("dosbox_cdrom_mount"), l_temp_str);
		if (l_temp_str == wxT("none"))
			dosbox_cdmount = CDROMMOUNT_NONE;
		else if (l_temp_str == wxT("dir"))
			dosbox_cdmount = CDROMMOUNT_DIR;
		else if (l_temp_str == wxT("image"))
			dosbox_cdmount = CDROMMOUNT_IMG;

		l_parseConfigString(l_settingsfile, wxT("dosbox_exec"), dosbox_exec);
		l_parseConfigString(l_settingsfile, wxT("dosbox_conf"), dosbox_conf);
		l_parseConfigString(l_settingsfile, wxT("dosbox_cdrom_location"), dosbox_cdrom_location);
		l_parseConfigString(l_settingsfile, wxT("dosbox_cdrom_image"), dosbox_cdrom_image);
		l_parseConfigString(l_settingsfile, wxT("bmouse_exec"), bmouse_exec);
#ifndef __WXMSW__
		// Windows compatibility (e.g. Wine) settings
		l_parseConfigBool(l_settingsfile, wxT("wine"), have_wine);
		l_parseConfigString(l_settingsfile, wxT("wine_exec"), wine_exec);
#endif
		// Multiplayer and network settings
		l_parseConfigBool(l_settingsfile, wxT("play_snd_join"), play_snd_join);
		l_parseConfigBool(l_settingsfile, wxT("play_snd_leave"), play_snd_leave);
		l_parseConfigBool(l_settingsfile, wxT("play_snd_send"), play_snd_send);
		l_parseConfigBool(l_settingsfile, wxT("play_snd_receive"), play_snd_receive);
		l_parseConfigBool(l_settingsfile, wxT("play_snd_error"), play_snd_error);
		l_parseConfigBool(l_settingsfile, wxT("mute_sounds_while_ingame"), mute_sounds_while_ingame);
		l_parseConfigString(l_settingsfile, wxT("nickname"), nickname);
		l_parseConfigString(l_settingsfile, wxT("game_nickname"), game_nickname);
		l_parseConfigString(l_settingsfile, wxT("snd_join_file"), snd_join_file);
		l_parseConfigString(l_settingsfile, wxT("snd_leave_file"), snd_leave_file);
		l_parseConfigString(l_settingsfile, wxT("snd_send_file"), snd_send_file);
		l_parseConfigString(l_settingsfile, wxT("snd_receive_file"), snd_receive_file);
		l_parseConfigString(l_settingsfile, wxT("snd_error_file"), snd_error_file);
		l_parseConfigLong(l_settingsfile, wxT("game_port_number"), game_port_number);

		l_parseConfigLong(l_settingsfile, wxT("server_port_number"), server_port_number);
		// Port number 8000 should never be used. It causes issues,
		// for certain people who try to *join* a server hosted on port 8000.
		if (server_port_number == 8000)
			server_port_number = DEFAULT_SERVER_PORT_NUM;
#ifdef ENABLE_UPNP
		l_parseConfigBool(l_settingsfile, wxT("enable_upnp"), enable_upnp);
#endif
		// Advanced options
		l_parseConfigBool(l_settingsfile, wxT("duke3dw_userpath_use"), duke3dw_userpath_use);
		l_parseConfigBool(l_settingsfile, wxT("eduke32_userpath_use"), eduke32_userpath_use);
		l_parseConfigBool(l_settingsfile, wxT("nduke_userpath_use"), nduke_userpath_use);
		l_parseConfigBool(l_settingsfile, wxT("hduke_userpath_use"), hduke_userpath_use);
		l_parseConfigBool(l_settingsfile, wxT("voidsw_userpath_use"), voidsw_userpath_use);
		l_parseConfigString(l_settingsfile, wxT("duke3dw_userpath"), duke3dw_userpath);
		l_parseConfigString(l_settingsfile, wxT("eduke32_userpath"), eduke32_userpath);
		l_parseConfigString(l_settingsfile, wxT("nduke_userpath"), nduke_userpath);
		l_parseConfigString(l_settingsfile, wxT("hduke_userpath"), hduke_userpath);
		l_parseConfigString(l_settingsfile, wxT("voidsw_userpath"), voidsw_userpath);
		l_parseConfigString(l_settingsfile, wxT("banlist_filepath"), banlist_filepath);
		l_parseConfigBool(l_settingsfile, wxT("override_browser"), override_browser);
		l_parseConfigString(l_settingsfile, wxT("browser_exec"), browser_exec);
		l_parseConfigBool(l_settingsfile, wxT("override_soundcmd"), override_soundcmd);
		l_parseConfigString(l_settingsfile, wxT("playsound_cmd"), playsound_cmd);
		l_parseConfigBool(l_settingsfile, wxT("enable_localip_optimization"), enable_localip_optimization);
#ifndef __WXMSW__
		l_parseConfigString(l_settingsfile, wxT("terminal_fullcmd"), terminal_fullcmd);
#endif
		// Host in-room options
		l_parseConfigBool(l_settingsfile, wxT("host_autoaccept_downloads"), host_autoaccept_downloads);
		l_parseConfigBool(l_settingsfile, wxT("host_accept_wakeup"), host_accept_wakeup);
		l_parseConfigBool(l_settingsfile, wxT("lookup_on_startup"), lookup_on_startup);
		// Windows UI controls sizes.
		l_parseConfigString(l_settingsfile, wxT("game_filter_tree"), game_filter_tree);
		l_parseConfigInt(l_settingsfile, wxT("main_col_private_len"), main_col_private_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_loc_len"), main_col_loc_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_roomname_len"), main_col_roomname_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_ping_len"), main_col_ping_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_flux_len"), main_col_flux_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_game_len"), main_col_game_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_srcport_len"), main_col_srcport_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_players_len"), main_col_players_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_map_len"), main_col_map_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_tcmod_len"), main_col_tcmod_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_gametype_len"), main_col_gametype_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_skill_len"), main_col_skill_len);
		l_parseConfigInt(l_settingsfile, wxT("main_col_ingame_len"), main_col_ingame_len);
		l_parseConfigInt(l_settingsfile, wxT("mainplayer_col_loc_len"), mainplayer_col_loc_len);
		l_parseConfigInt(l_settingsfile, wxT("mainplayer_col_nick_len"), mainplayer_col_nick_len);
		l_parseConfigInt(l_settingsfile, wxT("mainplayer_col_ip_len"), mainplayer_col_ip_len);
		l_parseConfigInt(l_settingsfile, wxT("mainplayer_col_os_len"), mainplayer_col_os_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_ready_len"), host_col_ready_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_loc_len"), host_col_loc_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_nick_len"), host_col_nick_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_ping_len"), host_col_ping_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_flux_len"), host_col_flux_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_os_len"), host_col_os_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_detectedip_len"), host_col_detectedip_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_ingameip_len"), host_col_ingameip_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_requests_len"), host_col_requests_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_filenames_len"), host_col_filenames_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_filesizes_len"), host_col_filesizes_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_settings_len"), host_col_settings_len);
		l_parseConfigInt(l_settingsfile, wxT("host_col_value_len"), host_col_value_len);
		l_parseConfigInt(l_settingsfile, wxT("client_col_ready_len"), client_col_ready_len);
		l_parseConfigInt(l_settingsfile, wxT("client_col_loc_len"), client_col_loc_len);
		l_parseConfigInt(l_settingsfile, wxT("client_col_nick_len"), client_col_nick_len);
		l_parseConfigInt(l_settingsfile, wxT("client_col_ping_len"), client_col_ping_len);
		l_parseConfigInt(l_settingsfile, wxT("client_col_flux_len"), client_col_flux_len);
		l_parseConfigInt(l_settingsfile, wxT("client_col_os_len"), client_col_os_len);
		l_parseConfigInt(l_settingsfile, wxT("client_col_detectedip_len"), client_col_detectedip_len);
		l_parseConfigInt(l_settingsfile, wxT("client_col_ingameip_len"), client_col_ingameip_len);
		l_parseConfigInt(l_settingsfile, wxT("client_col_settings_len"), client_col_settings_len);
		l_parseConfigInt(l_settingsfile, wxT("client_col_value_len"), client_col_value_len);
		l_parseConfigInt(l_settingsfile, wxT("main_win_width"), main_win_width);
		l_parseConfigInt(l_settingsfile, wxT("main_win_height"), main_win_height);
		l_parseConfigInt(l_settingsfile, wxT("host_win_width"), host_win_width);
		l_parseConfigInt(l_settingsfile, wxT("host_win_height"), host_win_height);
		l_parseConfigInt(l_settingsfile, wxT("client_win_width"), client_win_width);
		l_parseConfigInt(l_settingsfile, wxT("client_win_height"), client_win_height);
		l_parseConfigBool(l_settingsfile, wxT("main_is_maximized"), main_is_maximized);
		l_parseConfigBool(l_settingsfile, wxT("host_is_maximized"), host_is_maximized);
		l_parseConfigBool(l_settingsfile, wxT("client_is_maximized"), client_is_maximized);

		// Extra server lists.
		if (l_parseConfigString(l_settingsfile, wxT("extra_serverlists"), l_temp_str))
		{
			l_temp_int1 = 0;
			l_temp_int2 = 0;
			l_temp_length = l_temp_str.Len();
			while (l_temp_int1 < l_temp_length)
			{
				if (l_temp_str[l_temp_int1] != wxT('"'))
				{
					do
					{
						l_temp_int1++;
					}
					while ((l_temp_int1 < l_temp_length)
						&& (l_temp_str[l_temp_int1] != wxT('"')));
				}

				if (l_temp_int1 < l_temp_length)
				{
					l_hostaddr = wxEmptyString;
					l_temp_int1++;
					while ((l_temp_int1 < l_temp_length)
						&& (l_temp_str[l_temp_int1] != wxT(':')))
					{
						if (l_temp_str[l_temp_int1] == wxT('\\'))
						{
							l_temp_int1++;
							if (l_temp_int1 < l_temp_length)
							{
								l_hostaddr << l_temp_str[l_temp_int1];
								l_temp_int1++;
							}
						}
						else
						{
							l_hostaddr << l_temp_str[l_temp_int1];
							l_temp_int1++;
						}
					}

					if (l_temp_int1 < l_temp_length)
					{
						l_temp_int2 = l_temp_int1 + 1;
						while ((l_temp_int2 < l_temp_length)
							&& (l_temp_str[l_temp_int2] != wxT('"')))
							l_temp_int2++;
						if (l_temp_int2 < l_temp_length)
						{
							extraservers_addrs.Add(l_hostaddr);
							if ((l_temp_str.Mid(l_temp_int1+1, l_temp_int2-l_temp_int1-1)).ToLong(&l_temp_long))
								extraservers_portnums.Add(l_temp_long);
							else
								extraservers_portnums.Add(0);
						}
						l_temp_int1 = l_temp_int2 + 1;
					}
				}
			}
		}

		// Aliases, IP addresses and port numbers for manual joining.
		if (l_parseConfigString(l_settingsfile, wxT("manualjoin_items"), l_temp_str))
		{
			l_temp_int1 = 0;
			l_temp_int2 = 0;
			l_temp_length = l_temp_str.Len();
			while (l_temp_int1 < l_temp_length)
			{
				if (l_temp_str[l_temp_int1] != wxT('"'))
				{
					do
					{
						l_temp_int1++;
					}
					while ((l_temp_int1 < l_temp_length)
						&& (l_temp_str[l_temp_int1] != wxT('"')));
				}

				if (l_temp_int1 < l_temp_length)
				{
					l_nickname = wxEmptyString;
					l_temp_int1++;
					while ((l_temp_int1 < l_temp_length)
						&& (l_temp_str[l_temp_int1] != wxT(':')))
					{
						if (l_temp_str[l_temp_int1] == wxT('\\'))
						{
							l_temp_int1++;
							if (l_temp_int1 < l_temp_length)
							{
								l_nickname << l_temp_str[l_temp_int1];
								l_temp_int1++;
							}
						}
						else
						{
							l_nickname << l_temp_str[l_temp_int1];
							l_temp_int1++;
						}
					}

					if (l_temp_int1 < l_temp_length)
					{
						l_hostaddr = wxEmptyString;
						l_temp_int1++;
						while ((l_temp_int1 < l_temp_length)
							&& (l_temp_str[l_temp_int1] != wxT(':')))
						{
							if (l_temp_str[l_temp_int1] == wxT('\\'))
							{
								l_temp_int1++;
								if (l_temp_int1 < l_temp_length)
								{
									l_hostaddr << l_temp_str[l_temp_int1];
									l_temp_int1++;
								}
							}
							else
							{
								l_hostaddr << l_temp_str[l_temp_int1];
								l_temp_int1++;
							}
						}

						if (l_temp_int1 < l_temp_length)
						{
							l_temp_int2 = l_temp_int1 + 1;
							while ((l_temp_int2 < l_temp_length)
								&& (l_temp_str[l_temp_int2] != wxT('"')))
								l_temp_int2++;
							if (l_temp_int2 < l_temp_length)
							{
								manualjoin_names.Add(l_nickname);
								manualjoin_addrs.Add(l_hostaddr);
								if ((l_temp_str.Mid(l_temp_int1+1, l_temp_int2-l_temp_int1-1)).ToLong(&l_temp_long))
									manualjoin_portnums.Add(l_temp_long);
								else
									manualjoin_portnums.Add(server_port_number);
							}
							l_temp_int1 = l_temp_int2 + 1;
						}
					}
				}
			}
		}


		have_custom_mpprofile = false;

		l_loop_var = 1;

		while (l_parseConfigString(l_settingsfile, wxString::Format(wxT("custom_profile_%d_profilename"), l_loop_var), l_temp_str))
		{
			if (!custom_profile_list)
			{
				custom_profile_list = new custom_profile_t;
				custom_profile_list->previous = NULL;
			}
			else
			{
				custom_profile_list->next = new custom_profile_t;
				custom_profile_list->next->previous = custom_profile_list;
				custom_profile_list = custom_profile_list->next;
			}

			custom_profile_list->next = NULL;

			if (!l_temp_str.Trim(false).IsEmpty())
				custom_profile_list->profilename = l_temp_str;
			else
				custom_profile_list->profilename = wxString::Format(wxT("Custom DOS game profile %d"), l_loop_var);

			l_parseConfigBool(l_settingsfile, wxString::Format(wxT("custom_profile_%d_spgameonly"), l_loop_var), custom_profile_list->spgameonly);
			l_parseConfigString(l_settingsfile, wxString::Format(wxT("custom_profile_%d_spexecutable"), l_loop_var), custom_profile_list->spexecutable);

			if (!custom_profile_list->spgameonly)
			{
				l_parseConfigBool(l_settingsfile, wxString::Format(wxT("custom_profile_%d_sameexecutable"), l_loop_var), custom_profile_list->sameexecutable);

				if (!custom_profile_list->sameexecutable)
					l_parseConfigString(l_settingsfile, wxString::Format(wxT("custom_profile_%d_mpexecutable"), l_loop_var), custom_profile_list->mpexecutable);
				else
					custom_profile_list->mpexecutable = custom_profile_list->spexecutable;
			}
			else
				custom_profile_list->sameexecutable = false;

			l_parseConfigString(l_settingsfile, wxString::Format(wxT("custom_profile_%d_cdmount"), l_loop_var), l_temp_str);

			if (l_temp_str == wxT("none"))
				custom_profile_list->cdmount = CDROMMOUNT_NONE;
			else if (l_temp_str == wxT("dir"))
			{
				custom_profile_list->cdmount = CDROMMOUNT_DIR;
				l_parseConfigString(l_settingsfile, wxString::Format(wxT("custom_profile_%d_cdlocation"), l_loop_var), custom_profile_list->cdlocation);
			}
			else if (l_temp_str == wxT("image"))
			{
				custom_profile_list->cdmount = CDROMMOUNT_IMG;
				l_parseConfigString(l_settingsfile, wxString::Format(wxT("custom_profile_%d_cdimage"), l_loop_var), custom_profile_list->cdimage);
			}

			if (!custom_profile_list->spgameonly)
			{
				l_parseConfigBool(l_settingsfile, wxString::Format(wxT("custom_profile_%d_ingamesupport"), l_loop_var), custom_profile_list->ingamesupport);
				l_parseConfigBool(l_settingsfile, wxString::Format(wxT("custom_profile_%d_usenetbios"), l_loop_var), custom_profile_list->usenetbios);

				if (custom_profile_list->usenetbios)
					l_parseConfigString(l_settingsfile, wxString::Format(wxT("custom_profile_%d_netbiospath"), l_loop_var), custom_profile_list->netbiospath);
			}
			else
			{
				custom_profile_list->ingamesupport = false;
				custom_profile_list->usenetbios = false;
			}

			l_parseConfigString(l_settingsfile, wxString::Format(wxT("custom_profile_%d_extraargs"), l_loop_var), custom_profile_list->extraargs);

			if (!custom_profile_list->spgameonly)
			{
				l_parseConfigString(l_settingsfile, wxString::Format(wxT("custom_profile_%d_extrahostargs"), l_loop_var), custom_profile_list->extrahostargs);
				l_parseConfigString(l_settingsfile, wxString::Format(wxT("custom_profile_%d_extraallargs"), l_loop_var), custom_profile_list->extraallargs);
			}

			if (!custom_profile_list->spgameonly)
				have_custom_mpprofile = true;

			l_loop_var++;
		}

		if (custom_profile_list)
			while (custom_profile_list->previous)
				custom_profile_list = custom_profile_list->previous;


		l_loop_var = 1;

		while (l_parseConfigString(l_settingsfile, wxString::Format(wxT("dn3dtcmod_profile_%d_profilename"), l_loop_var), l_temp_str))
		{
			if (!dn3dtcmod_profile_list)
			{
				dn3dtcmod_profile_list = new dn3dtcmod_profile_t;
				dn3dtcmod_profile_list->previous = NULL;
			}
			else
			{
				dn3dtcmod_profile_list->next = new dn3dtcmod_profile_t;
				dn3dtcmod_profile_list->next->previous = dn3dtcmod_profile_list;
				dn3dtcmod_profile_list = dn3dtcmod_profile_list->next;
			}

			dn3dtcmod_profile_list->next = NULL;

			if (!l_temp_str.Trim(false).IsEmpty())
				dn3dtcmod_profile_list->profilename = l_temp_str;
			else
				dn3dtcmod_profile_list->profilename = wxString::Format(wxT("Duke Nukem 3D TC/MOD profile %d"), l_loop_var);

			if (l_parseConfigString(l_settingsfile, wxString::Format(wxT("dn3dtcmod_profile_%d_modfiles"), l_loop_var), l_temp_str))
			{
				l_temp_int1 = 0;
				l_temp_int2 = 0;
				l_temp_length = l_temp_str.Len();
				while (l_temp_int1 < l_temp_length)
				{
					if (l_temp_str[l_temp_int1] != wxT('"'))
					{
						do
						{
							l_temp_int1++;
						}
						while ((l_temp_int1 < l_temp_length)
							&& (l_temp_str[l_temp_int1] != wxT('"')));
					}

					if (l_temp_int1 < l_temp_length)
					{
						l_temp_int2 = l_temp_int1 + 1;
						while ((l_temp_int2 < l_temp_length)
							&& (l_temp_str[l_temp_int2] != wxT('"')))
							l_temp_int2++;
						if (l_temp_int2 < l_temp_length)
							dn3dtcmod_profile_list->modfiles.Add(l_temp_str.Mid(l_temp_int1+1, l_temp_int2-l_temp_int1-1));
						l_temp_int1 = l_temp_int2 + 1;
					}
				}
			}

			if (!dn3dtcmod_profile_list->modfiles.IsEmpty())
			{
				l_parseConfigString(l_settingsfile, wxString::Format(wxT("dn3dtcmod_profile_%d_confile"), l_loop_var), dn3dtcmod_profile_list->confile);

				l_parseConfigString(l_settingsfile, wxString::Format(wxT("dn3dtcmod_profile_%d_deffile"), l_loop_var), dn3dtcmod_profile_list->deffile);
			}

			l_parseConfigBool(l_settingsfile, wxString::Format(wxT("dn3dtcmod_profile_%d_usemodurl"), l_loop_var), dn3dtcmod_profile_list->usemodurl);

			if (dn3dtcmod_profile_list->usemodurl)
				l_parseConfigString(l_settingsfile, wxString::Format(wxT("dn3dtcmod_profile_%d_modurl"), l_loop_var), dn3dtcmod_profile_list->modurl);

			l_loop_var++;
		}

		if (dn3dtcmod_profile_list)
			while (dn3dtcmod_profile_list->previous)
				dn3dtcmod_profile_list = dn3dtcmod_profile_list->previous;


		l_loop_var = 1;

		while (l_parseConfigString(l_settingsfile, wxString::Format(wxT("swtcmod_profile_%d_profilename"), l_loop_var), l_temp_str))
		{
			if (!swtcmod_profile_list)
			{
				swtcmod_profile_list = new swtcmod_profile_t;
				swtcmod_profile_list->previous = NULL;
			}
			else
			{
				swtcmod_profile_list->next = new swtcmod_profile_t;
				swtcmod_profile_list->next->previous = swtcmod_profile_list;
				swtcmod_profile_list = swtcmod_profile_list->next;
			}

			swtcmod_profile_list->next = NULL;

			if (!l_temp_str.Trim(false).IsEmpty())
				swtcmod_profile_list->profilename = l_temp_str;
			else
				swtcmod_profile_list->profilename = wxString::Format(wxT("Shadow Warrior TC/MOD profile %d"), l_loop_var);

			if (l_parseConfigString(l_settingsfile, wxString::Format(wxT("swtcmod_profile_%d_modfiles"), l_loop_var), l_temp_str))
			{
				l_temp_int1 = 0;
				l_temp_int2 = 0;
				l_temp_length = l_temp_str.Len();
				while (l_temp_int1 < l_temp_length)
				{
					if (l_temp_str[l_temp_int1] != wxT('"'))
					{
						do
						{
							l_temp_int1++;
						}
						while ((l_temp_int1 < l_temp_length)
							&& (l_temp_str[l_temp_int1] != wxT('"')));
					}

					if (l_temp_int1 < l_temp_length)
					{
						l_temp_int2 = l_temp_int1 + 1;
						while ((l_temp_int2 < l_temp_length)
							&& (l_temp_str[l_temp_int2] != wxT('"')))
							l_temp_int2++;
						if (l_temp_int2 < l_temp_length)
							swtcmod_profile_list->modfiles.Add(l_temp_str.Mid(l_temp_int1+1, l_temp_int2-l_temp_int1-1));
						l_temp_int1 = l_temp_int2 + 1;
					}
				}
			}

			if (!swtcmod_profile_list->modfiles.IsEmpty())
				l_parseConfigString(l_settingsfile, wxString::Format(wxT("swtcmod_profile_%d_deffile"), l_loop_var), swtcmod_profile_list->deffile);

			l_parseConfigBool(l_settingsfile, wxString::Format(wxT("swtcmod_profile_%d_usemodurl"), l_loop_var), swtcmod_profile_list->usemodurl);

			if (swtcmod_profile_list->usemodurl)
				l_parseConfigString(l_settingsfile, wxString::Format(wxT("swtcmod_profile_%d_modurl"), l_loop_var), swtcmod_profile_list->modurl);

			l_loop_var++;
		}

		if (swtcmod_profile_list)
			while (swtcmod_profile_list->previous)
				swtcmod_profile_list = swtcmod_profile_list->previous;


		l_settingsfile.Close();
		// Latest checks for all cases of invalid values (e.g. a skill of -2)

		if ((gamelaunch_blood_skill > BLOOD_MAX_SKILLNUM) || (gamelaunch_blood_skill < 0))
			gamelaunch_blood_skill = BLOOD_DEFAULT_SKILL;

		if ((gamelaunch_dn3d_skill > DN3D_MAX_SKILLNUM) || (gamelaunch_dn3d_skill < 0))
			gamelaunch_dn3d_skill = DN3D_DEFAULT_SKILL;

		if ((gamelaunch_sw_skill > SW_MAX_SKILLNUM) || (gamelaunch_sw_skill < 0))
			gamelaunch_sw_skill = SW_DEFAULT_SKILL;

		if ((gamelaunch_dn3d_skill_in_sp > DN3D_MAX_SKILLNUM) || (gamelaunch_dn3d_skill_in_sp < 0))
			gamelaunch_dn3d_skill_in_sp = DN3D_DEFAULT_SKILL_IN_SP;

		if ((gamelaunch_sw_skill_in_sp > SW_MAX_SKILLNUM) || (gamelaunch_sw_skill_in_sp < 0))
			gamelaunch_sw_skill_in_sp = SW_DEFAULT_SKILL_IN_SP;

		if ((gamelaunch_bloodlvl < 0) ||
			((gamelaunch_bloodlvl > 0) && (gamelaunch_bloodlvl <= 100)) ||
			((gamelaunch_bloodlvl > 108) && (gamelaunch_bloodlvl <= 200)) ||
			((gamelaunch_bloodlvl > 209) && (gamelaunch_bloodlvl <= 300)) ||
			((gamelaunch_bloodlvl > 308) && (gamelaunch_bloodlvl <= 400)) ||
			((gamelaunch_bloodlvl > 409) && (gamelaunch_bloodlvl <= 500)) ||
			((gamelaunch_bloodlvl > 511) && (gamelaunch_bloodlvl <= 600)) ||
			(gamelaunch_bloodlvl > 609))
			gamelaunch_bloodlvl = BLOOD_DEFAULT_LEVEL;

		if ((gamelaunch_bloodlvl_cp < 0) ||
			((gamelaunch_bloodlvl_cp > 0) && (gamelaunch_bloodlvl <= 100)) ||
			((gamelaunch_bloodlvl_cp > 110) && (gamelaunch_bloodlvl <= 200)) ||
			(gamelaunch_bloodlvl_cp > 204))
			gamelaunch_bloodlvl_cp = BLOOD_DEFAULT_LEVEL;

		if ((gamelaunch_descentlvl < 0) ||
			(gamelaunch_descentlvl > 1))
			gamelaunch_descentlvl = DESCENT_DEFAULT_LEVEL;

		if ((gamelaunch_descent2lvl < 0) ||
			(gamelaunch_descent2lvl > 3))
			gamelaunch_descent2lvl = DESCENT2_DEFAULT_LEVEL;

		if ((gamelaunch_dn3dlvl < 0) ||
			((gamelaunch_dn3dlvl > 0) && (gamelaunch_dn3dlvl <= 100)) ||
			((gamelaunch_dn3dlvl > 107) && (gamelaunch_dn3dlvl <= 200)) ||
			((gamelaunch_dn3dlvl > 211) && (gamelaunch_dn3dlvl <= 300)) ||
			((gamelaunch_dn3dlvl > 311) && (gamelaunch_dn3dlvl <= 400)) ||
			(gamelaunch_dn3dlvl > 411))
			gamelaunch_dn3dlvl = DN3D_DEFAULT_LEVEL;

		if ((gamelaunch_swlvl < 0) ||
			(gamelaunch_swlvl > 28))
			gamelaunch_swlvl = SW_DEFAULT_LEVEL;

		if (!gamelaunch_bloodusermap.IsEmpty())
			if (!wxFileExists(blood_maps_dir + wxFILE_SEP_PATH + gamelaunch_bloodusermap))
				gamelaunch_bloodusermap = wxEmptyString;

		if (!gamelaunch_descentusermap.IsEmpty())
			if (!wxFileExists(descent_maps_dir + wxFILE_SEP_PATH + gamelaunch_descentusermap))
				gamelaunch_descentusermap = wxEmptyString;

		if (!gamelaunch_descent2usermap.IsEmpty())
			if (!wxFileExists(descent2_maps_dir + wxFILE_SEP_PATH + gamelaunch_descent2usermap))
				gamelaunch_descent2usermap = wxEmptyString;

		if (!gamelaunch_dn3dusermap.IsEmpty())
			if (!wxFileExists(dn3d_maps_dir + wxFILE_SEP_PATH + gamelaunch_dn3dusermap))
				gamelaunch_dn3dusermap = wxEmptyString;

		if (!gamelaunch_swusermap.IsEmpty())
			if (!wxFileExists(sw_maps_dir + wxFILE_SEP_PATH + gamelaunch_swusermap))
				gamelaunch_swusermap = wxEmptyString;

		if ((gamelaunch_dn3d_playercolnum < 0) || (gamelaunch_dn3d_playercolnum >= DN3D_NUM_OF_PLAYER_COLORS))
			gamelaunch_dn3d_playercolnum = 0;

		if ((gamelaunch_sw_playercolnum < 0) || (gamelaunch_sw_playercolnum >= SW_NUM_OF_PLAYER_COLORS))
			gamelaunch_sw_playercolnum = 0;

		if (gamelaunch_use_password && gamelaunch_password.IsEmpty())
			gamelaunch_use_password = false; // Password can't be empty.

		return true;
	}
	return false;
}

#if (!((defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__)))
void LauncherConfig::DetectTerminal()
{
	if (wxFileExists(wxT("/usr/bin/gnome-terminal")))
		terminal_fullcmd = wxT("/usr/bin/gnome-terminal -x");
	else if (wxFileExists(wxT("/usr/bin/konsole")))
		terminal_fullcmd = wxT("/usr/bin/konsole -e");
	else if (wxFileExists(wxT("/usr/bin/xfce4-terminal")))
		terminal_fullcmd = wxT("/usr/bin/xfce4-terminal -x");
	else if (wxFileExists(wxT("/usr/bin/mlterm")))
		terminal_fullcmd = wxT("/usr/bin/mlterm -e");
	else if (wxFileExists(wxT("/usr/bin/xterm")))
		terminal_fullcmd = wxT("/usr/bin/xterm -e");
	else if (wxFileExists(wxT("/usr/bin/rxvt")))
		terminal_fullcmd = wxT("/usr/bin/rxvt -e");
	else if (wxFileExists(wxT("/usr/local/bin/gnome-terminal")))
		terminal_fullcmd = wxT("/usr/local/bin/gnome-terminal -x");
	else if (wxFileExists(wxT("/usr/local/bin/konsole")))
		terminal_fullcmd = wxT("/usr/local/bin/konsole -e");
	else if (wxFileExists(wxT("/usr/local/bin/xfce4-terminal")))
		terminal_fullcmd = wxT("/usr/local/bin/xfce4-terminal -x");
	else if (wxFileExists(wxT("/usr/local/bin/mlterm")))
		terminal_fullcmd = wxT("/usr/local/bin/mlterm -e");
	else if (wxFileExists(wxT("/usr/local/bin/xterm")))
		terminal_fullcmd = wxT("/usr/local/bin/xterm -e");
	else if (wxFileExists(wxT("/usr/local/bin/rxvt")))
		terminal_fullcmd = wxT("/usr/local/bin/rxvt -e");
	else if (wxFileExists(wxT("/usr/X11R6/bin/gnome-terminal")))
		terminal_fullcmd = wxT("/usr/X11R6/bin/gnome-terminal -x");
	else if (wxFileExists(wxT("/usr/X11R6/bin/konsole")))
		terminal_fullcmd = wxT("/usr/X11R6/bin/konsole -e");
	else if (wxFileExists(wxT("/usr/X11R6/bin/xfce4-terminal")))
		terminal_fullcmd = wxT("/usr/X11R6/bin/xfce4-terminal -x");
	else if (wxFileExists(wxT("/usr/X11R6/bin/mlterm")))
		terminal_fullcmd = wxT("/usr/X11R6/bin/mlterm -e");
	else if (wxFileExists(wxT("/usr/X11R6/bin/xterm")))
		terminal_fullcmd = wxT("/usr/X11R6/bin/xterm -e");
	else if (wxFileExists(wxT("/usr/X11R6/bin/rxvt")))
		terminal_fullcmd = wxT("/usr/X11R6/bin/rxvt -e");
	else if (wxFileExists(wxT("/usr/local/X11R6/bin/gnome-terminal")))
		terminal_fullcmd = wxT("/usr/local/X11R6/bin/gnome-terminal -x");
	else if (wxFileExists(wxT("/usr/local/X11R6/bin/konsole")))
		terminal_fullcmd = wxT("/usr/local/X11R6/bin/konsole -e");
	else if (wxFileExists(wxT("/usr/local/X11R6/bin/xfce4-terminal")))
		terminal_fullcmd = wxT("/usr/local/X11R6/bin/xfce4-terminal -x");
	else if (wxFileExists(wxT("/usr/local/X11R6/bin/mlterm")))
		terminal_fullcmd = wxT("/usr/local/X11R6/bin/mlterm -e");
	else if (wxFileExists(wxT("/usr/local/X11R6/bin/xterm")))
		terminal_fullcmd = wxT("/usr/local/X11R6/bin/xterm -e");
	else if (wxFileExists(wxT("/usr/local/X11R6/bin/rxvt")))
		terminal_fullcmd = wxT("/usr/local/X11R6/bin/rxvt -e");
	else if (wxFileExists(wxT("/usr/X11/bin/gnome-terminal")))
		terminal_fullcmd = wxT("/usr/X11/bin/gnome-terminal -x");
	else if (wxFileExists(wxT("/usr/X11/bin/konsole")))
		terminal_fullcmd = wxT("/usr/X11/bin/konsole -e");
	else if (wxFileExists(wxT("/usr/X11/bin/xfce4-terminal")))
		terminal_fullcmd = wxT("/usr/X11/bin/xfce4-terminal -x");
	else if (wxFileExists(wxT("/usr/X11/bin/mlterm")))
		terminal_fullcmd = wxT("/usr/X11/bin/mlterm -e");
	else if (wxFileExists(wxT("/usr/X11/bin/xterm")))
		terminal_fullcmd = wxT("/usr/X11/bin/xterm -e");
	else if (wxFileExists(wxT("/usr/X11/bin/rxvt")))
		terminal_fullcmd = wxT("/usr/X11/bin/rxvt -e");
	else if (wxFileExists(wxT("/usr/local/X11/bin/gnome-terminal")))
		terminal_fullcmd = wxT("/usr/local/X11/bin/gnome-terminal -x");
	else if (wxFileExists(wxT("/usr/local/X11/bin/konsole")))
		terminal_fullcmd = wxT("/usr/local/X11/bin/konsole -e");
	else if (wxFileExists(wxT("/usr/local/X11/bin/xfce4-terminal")))
		terminal_fullcmd = wxT("/usr/local/X11/bin/xfce4-terminal -x");
	else if (wxFileExists(wxT("/usr/local/X11/bin/mlterm")))
		terminal_fullcmd = wxT("/usr/local/X11/bin/mlterm -e");
	else if (wxFileExists(wxT("/usr/local/X11/bin/xterm")))
		terminal_fullcmd = wxT("/usr/local/X11/bin/xterm -e");
	else if (wxFileExists(wxT("/usr/local/X11/bin/rxvt")))
		terminal_fullcmd = wxT("/usr/local/X11/bin/rxvt -e");
}
#endif

bool LauncherConfig::Save()
{
	if (!wxDirExists(*g_launcher_user_profile_path))
		if (!wxFileName::Mkdir(*g_launcher_user_profile_path, 0777, wxPATH_MKDIR_FULL))
			return false;
	wxTextFile settings_file;
	wxString l_str_line, l_str_curr;
	size_t l_loop_var, l_loop_var2, l_num_of_items, l_str_length;
	// Future compatibility with wxWidgets v3.00.
#ifdef wxUSE_UNICODE_UTF8
	wxUniChar l_curr_char;
#else
	wxChar l_curr_char;
#endif
	if (((!wxFileExists(YANG_CONFIG_FULLFILEPATH)) || (wxRemoveFile(YANG_CONFIG_FULLFILEPATH)))
		&& (settings_file.Create(YANG_CONFIG_FULLFILEPATH)))
	{
		settings_file.AddLine(wxT("# This is the YANG configuration file."));
		// Configuration version number
		settings_file.AddLine(wxString::Format(wxT("cfg_ver = %d"), CONFIG_DEFAULT_VERSION));
		// Visual appearance
		settings_file.AddLine(wxString::Format(wxT("theme_is_dark = %d"), theme_is_dark));
		settings_file.AddLine(wxString::Format(wxT("use_custom_chat_text_layout = %d"), use_custom_chat_text_layout));
		settings_file.AddLine(wxString::Format(wxT("use_custom_chat_text_colors = %d"), use_custom_chat_text_colors));
		settings_file.AddLine(wxT("chat_font = ") + chat_font);
		settings_file.AddLine(wxString::Format(wxT("chat_font_size = %d"), chat_font_size));
		settings_file.AddLine(wxString::Format(wxT("chat_textcolor_red = %d"), chat_textcolor_red));
		settings_file.AddLine(wxString::Format(wxT("chat_textcolor_green = %d"), chat_textcolor_green));
		settings_file.AddLine(wxString::Format(wxT("chat_textcolor_blue = %d"), chat_textcolor_blue));
		settings_file.AddLine(wxString::Format(wxT("chat_backcolor_red = %d"), chat_backcolor_red));
		settings_file.AddLine(wxString::Format(wxT("chat_backcolor_green = %d"), chat_backcolor_green));
		settings_file.AddLine(wxString::Format(wxT("chat_backcolor_blue = %d"), chat_backcolor_blue));

		/*  for (l_loop_var2 = 0; l_loop_var2 < COLORDIALOG_NUM_CUSTOM_COLORS; l_loop_var2++)
		{
		settings_file.AddLine(wxString::Format(wxT("custcolor_red%d = %d"), l_loop_var2, custcolor_red[l_loop_var2]));
		settings_file.AddLine(wxString::Format(wxT("custcolor_green%d = %d"), l_loop_var2, custcolor_green[l_loop_var2]));
		settings_file.AddLine(wxString::Format(wxT("custcolor_blue%d = %d"), l_loop_var2, custcolor_blue[l_loop_var2]));
		}*/

#if ASK_TO_ACCEPT_LICENSE
		// Is license agreement accepted?
		settings_file.AddLine(wxString::Format(wxT("license_accepted = %d"), is_license_accepted));
#endif
#if CHECK_FOR_UPDATES
		// Check for updates on startup
		settings_file.AddLine(wxString::Format(wxT("check_for_updates_on_startup = %d"), check_for_updates_on_startup));
#endif
		// Single Player / Host settings
		switch (gamelaunch_blood_source_port)
		{
			case SOURCEPORT_DOSBLOODSW:  settings_file.AddLine(wxT("blood_source_port = dosbloodsw")); break;
			case SOURCEPORT_DOSBLOODRG:  settings_file.AddLine(wxT("blood_source_port = dosbloodrg")); break;
			case SOURCEPORT_DOSBLOODPP:  settings_file.AddLine(wxT("blood_source_port = dosbloodpp")); break;
			case SOURCEPORT_DOSBLOODOU:  settings_file.AddLine(wxT("blood_source_port = dosbloodou")); break;
			default: ;
		}

		settings_file.AddLine(wxString::Format(wxT("blood_use_crypticpassage = %d"), gamelaunch_blood_use_crypticpassage));

		switch (gamelaunch_descent_source_port)
		{
			case SOURCEPORT_DOSDESCENT:  settings_file.AddLine(wxT("descent_source_port = dosdescent")); break;
			case SOURCEPORT_D1XREBIRTH:  settings_file.AddLine(wxT("descent_source_port = d1xrebirth")); break;
			default: ;
		}

		switch (gamelaunch_descent2_source_port)
		{
			case SOURCEPORT_DOSDESCENT2: settings_file.AddLine(wxT("descent2_source_port = dosdescent2")); break;
			case SOURCEPORT_D2XREBIRTH:  settings_file.AddLine(wxT("descent2_source_port = d2xrebirth")); break;
			default: ;
		}

		switch (gamelaunch_dn3d_source_port)
		{
			case SOURCEPORT_DOSDUKESW:   settings_file.AddLine(wxT("dn3d_source_port = dosdukesw")); break;
			case SOURCEPORT_DOSDUKERG:   settings_file.AddLine(wxT("dn3d_source_port = dosdukerg")); break;
			case SOURCEPORT_DOSDUKEAE:   settings_file.AddLine(wxT("dn3d_source_port = dosdukeae")); break;
			case SOURCEPORT_DUKE3DW:     settings_file.AddLine(wxT("dn3d_source_port = duke3dw")); break;
			case SOURCEPORT_EDUKE32:     settings_file.AddLine(wxT("dn3d_source_port = eduke32")); break;
			case SOURCEPORT_NDUKE:       settings_file.AddLine(wxT("dn3d_source_port = nduke")); break;
			case SOURCEPORT_HDUKE:       settings_file.AddLine(wxT("dn3d_source_port = hduke")); break;
			case SOURCEPORT_XDUKE:       settings_file.AddLine(wxT("dn3d_source_port = xduke")); break;
			default: ;
		}

		switch (gamelaunch_sw_source_port)
		{
			case SOURCEPORT_DOSSWSW:     settings_file.AddLine(wxT("sw_source_port = dosswsw")); break;
			case SOURCEPORT_DOSSWRG:     settings_file.AddLine(wxT("sw_source_port = dosswrg")); break;
			case SOURCEPORT_VOIDSW:      settings_file.AddLine(wxT("sw_source_port = voidsw")); break;
			case SOURCEPORT_SWP:         settings_file.AddLine(wxT("sw_source_port = swp")); break;
			default: ;
		}

		switch (gamelaunch_source_port)
		{
			case SOURCEPORT_DOSBLOODSW:  settings_file.AddLine(wxT("source_port = dosbloodsw")); break;
			case SOURCEPORT_DOSBLOODRG:  settings_file.AddLine(wxT("source_port = dosbloodrg")); break;
			case SOURCEPORT_DOSBLOODPP:  settings_file.AddLine(wxT("source_port = dosbloodpp")); break;
			case SOURCEPORT_DOSBLOODOU:  settings_file.AddLine(wxT("source_port = dosbloodou")); break;
			case SOURCEPORT_DOSDESCENT:  settings_file.AddLine(wxT("source_port = dosdescent")); break;
			case SOURCEPORT_D1XREBIRTH:  settings_file.AddLine(wxT("source_port = d1xrebirth")); break;
			case SOURCEPORT_DOSDESCENT2: settings_file.AddLine(wxT("source_port = dosdescent2")); break;
			case SOURCEPORT_D2XREBIRTH:  settings_file.AddLine(wxT("source_port = d2xrebirth")); break;
			case SOURCEPORT_DOSDUKESW:   settings_file.AddLine(wxT("source_port = dosdukesw")); break;
			case SOURCEPORT_DOSDUKERG:   settings_file.AddLine(wxT("source_port = dosdukerg")); break;
			case SOURCEPORT_DOSDUKEAE:   settings_file.AddLine(wxT("source_port = dosdukeae")); break;
			case SOURCEPORT_DUKE3DW:     settings_file.AddLine(wxT("source_port = duke3dw")); break;
			case SOURCEPORT_EDUKE32:     settings_file.AddLine(wxT("source_port = eduke32")); break;
			case SOURCEPORT_NDUKE:       settings_file.AddLine(wxT("source_port = nduke")); break;
			case SOURCEPORT_HDUKE:       settings_file.AddLine(wxT("source_port = hduke")); break;
			case SOURCEPORT_XDUKE:       settings_file.AddLine(wxT("source_port = xduke")); break;
			case SOURCEPORT_DOSSWSW:     settings_file.AddLine(wxT("source_port = dosswsw")); break;
			case SOURCEPORT_DOSSWRG:     settings_file.AddLine(wxT("source_port = dosswrg")); break;
			case SOURCEPORT_VOIDSW:      settings_file.AddLine(wxT("source_port = voidsw")); break;
			case SOURCEPORT_SWP:         settings_file.AddLine(wxT("source_port = swp")); break;
			case SOURCEPORT_CUSTOM:      settings_file.AddLine(wxT("source_port = custom")); break;
			default: ;
		}

		settings_file.AddLine(wxT("custom_profilename_in_sp = ") + gamelaunch_custom_profilename_in_sp);
		settings_file.AddLine(wxT("custom_profilename = ") + gamelaunch_custom_profilename);
		settings_file.AddLine(wxT("customselect_profilename = ") + gamelaunch_customselect_profilename);
		settings_file.AddLine(wxT("dn3dtcmod_profilename = ") + gamelaunch_dn3dtcmod_profilename);
		settings_file.AddLine(wxT("dn3dtcmodselect_profilename = ") + gamelaunch_dn3dtcmodselect_profilename);
		settings_file.AddLine(wxT("swtcmod_profilename = ") + gamelaunch_swtcmod_profilename);
		settings_file.AddLine(wxT("swtcmodselect_profilename = ") + gamelaunch_swtcmodselect_profilename);

		settings_file.AddLine(wxString::Format(wxT("blood_skill = %ld"), gamelaunch_blood_skill));
		settings_file.AddLine(wxString::Format(wxT("dn3d_skill = %ld"), gamelaunch_dn3d_skill));
		settings_file.AddLine(wxString::Format(wxT("sw_skill = %ld"), gamelaunch_sw_skill));
		settings_file.AddLine(wxString::Format(wxT("dn3d_skill_in_sp = %ld"), gamelaunch_dn3d_skill_in_sp));
		settings_file.AddLine(wxString::Format(wxT("sw_skill_in_sp = %ld"), gamelaunch_sw_skill_in_sp));
		settings_file.AddLine(wxT("blood_args_in_sp = ") + gamelaunch_blood_args_in_sp);
		settings_file.AddLine(wxT("descent_args_in_sp = ") + gamelaunch_descent_args_in_sp);
		settings_file.AddLine(wxT("descent2_args_in_sp = ") + gamelaunch_descent2_args_in_sp);
		settings_file.AddLine(wxT("dn3d_args_in_sp = ") + gamelaunch_dn3d_args_in_sp);
		settings_file.AddLine(wxT("sw_args_in_sp = ") + gamelaunch_sw_args_in_sp);
		settings_file.AddLine(wxString::Format(wxT("blood_level = %ld"), gamelaunch_bloodlvl));
		settings_file.AddLine(wxString::Format(wxT("blood_level_cp = %ld"), gamelaunch_bloodlvl_cp));
		settings_file.AddLine(wxString::Format(wxT("descent_level = %ld"), gamelaunch_descentlvl));
		settings_file.AddLine(wxString::Format(wxT("descent2_level = %ld"), gamelaunch_descent2lvl));
		settings_file.AddLine(wxString::Format(wxT("dn3d_level = %ld"), gamelaunch_dn3dlvl));
		settings_file.AddLine(wxString::Format(wxT("sw_level = %ld"), gamelaunch_swlvl));
		settings_file.AddLine(wxT("blood_usermap = ") + gamelaunch_bloodusermap);
		settings_file.AddLine(wxT("descent_usermap = ") + gamelaunch_descentusermap);
		settings_file.AddLine(wxT("descent2_usermap = ") + gamelaunch_descent2usermap);
		settings_file.AddLine(wxT("dn3d_usermap = ") + gamelaunch_dn3dusermap);
		settings_file.AddLine(wxT("sw_usermap = ") + gamelaunch_swusermap);

		settings_file.AddLine(wxT("blood_room_name = ") + gamelaunch_blood_roomname);
		settings_file.AddLine(wxT("descent_room_name = ") + gamelaunch_descent_roomname);
		settings_file.AddLine(wxT("descent2_room_name = ") + gamelaunch_descent2_roomname);
		settings_file.AddLine(wxT("dn3d_room_name = ") + gamelaunch_dn3d_roomname);
		settings_file.AddLine(wxT("sw_room_name = ") + gamelaunch_sw_roomname);
		settings_file.AddLine(wxT("custom_room_name = ") + gamelaunch_custom_roomname);
		settings_file.AddLine(wxString::Format(wxT("blood_num_of_players = %ld"), gamelaunch_blood_numofplayers));
		settings_file.AddLine(wxString::Format(wxT("descent_num_of_players = %ld"), gamelaunch_descent_numofplayers));
		settings_file.AddLine(wxString::Format(wxT("descent2_num_of_players = %ld"), gamelaunch_descent2_numofplayers));
		settings_file.AddLine(wxString::Format(wxT("dn3d_num_of_players = %ld"), gamelaunch_dn3d_numofplayers));
		settings_file.AddLine(wxString::Format(wxT("sw_num_of_players = %ld"), gamelaunch_sw_numofplayers));
		settings_file.AddLine(wxString::Format(wxT("custom_num_of_players = %ld"), gamelaunch_custom_numofplayers));
		settings_file.AddLine(wxString::Format(wxT("num_of_fakeplayers = %ld"), gamelaunch_numoffakeplayers));
		settings_file.AddLine(wxString::Format(wxT("enable_bot_ai = %d"), gamelaunch_enable_bot_ai));

		switch (gamelaunch_bloodgametype_in_sp)
		{
			case (BloodMPGameT_Type)0:  settings_file.AddLine(wxT("blood_mpgametype_in_sp = single")); break;
			case BLOODMPGAMETYPE_COOP:  settings_file.AddLine(wxT("blood_mpgametype_in_sp = coop")); break;
			case BLOODMPGAMETYPE_BB:    settings_file.AddLine(wxT("blood_mpgametype_in_sp = bb")); break;
			case BLOODMPGAMETYPE_TEAMS: settings_file.AddLine(wxT("blood_mpgametype_in_sp = teams")); break;
			default: ;
		}
		switch (gamelaunch_dn3dgametype_in_sp)
		{
			case (DN3DMPGameT_Type)0:       settings_file.AddLine(wxT("dn3d_mpgametype_in_sp = single")); break;
			case DN3DMPGAMETYPE_DMSPAWN:    settings_file.AddLine(wxT("dn3d_mpgametype_in_sp = dm_spawn")); break;
			case DN3DMPGAMETYPE_COOP:       settings_file.AddLine(wxT("dn3d_mpgametype_in_sp = coop")); break;
			case DN3DMPGAMETYPE_DMNOSPAWN:  settings_file.AddLine(wxT("dn3d_mpgametype_in_sp = dm_nospawn")); break;
			case DN3DMPGAMETYPE_TDMSPAWN:   settings_file.AddLine(wxT("dn3d_mpgametype_in_sp = tdm_spawn")); break;
			case DN3DMPGAMETYPE_TDMNOSPAWN: settings_file.AddLine(wxT("dn3d_mpgametype_in_sp = tdm_nospawn")); break;
			default: ;
		}

		switch (gamelaunch_bloodgametype)
		{
			case BLOODMPGAMETYPE_COOP:  settings_file.AddLine(wxT("blood_mpgametype = coop")); break;
			case BLOODMPGAMETYPE_BB:    settings_file.AddLine(wxT("blood_mpgametype = bb")); break;
			case BLOODMPGAMETYPE_TEAMS: settings_file.AddLine(wxT("blood_mpgametype = teams")); break;
			default: ;
		}
		switch (gamelaunch_descentgametype)
		{
			case DESCENTMPGAMETYPE_ANARCHY:      settings_file.AddLine(wxT("descent_mpgametype = anarchy")); break;
			case DESCENTMPGAMETYPE_TEAM_ANARCHY: settings_file.AddLine(wxT("descent_mpgametype = team_anarchy")); break;
			case DESCENTMPGAMETYPE_ROBO_ANARCHY: settings_file.AddLine(wxT("descent_mpgametype = robo_anarchy")); break;
			case DESCENTMPGAMETYPE_COOPERATIVE:  settings_file.AddLine(wxT("descent_mpgametype = cooperative")); break;
			default: ;
		}
		switch (gamelaunch_descent2gametype)
		{
			case DESCENT2MPGAMETYPE_ANARCHY:          settings_file.AddLine(wxT("descent2_mpgametype = anarchy")); break;
			case DESCENT2MPGAMETYPE_TEAM_ANARCHY:     settings_file.AddLine(wxT("descent2_mpgametype = team_anarchy")); break;
			case DESCENT2MPGAMETYPE_ROBO_ANARCHY:     settings_file.AddLine(wxT("descent2_mpgametype = robo_anarchy")); break;
			case DESCENT2MPGAMETYPE_COOPERATIVE:      settings_file.AddLine(wxT("descent2_mpgametype = cooperative")); break;
			case DESCENT2MPGAMETYPE_CAPTURE_THE_FLAG: settings_file.AddLine(wxT("descent2_mpgametype = capture_the_flag")); break;
			case DESCENT2MPGAMETYPE_HOARD:            settings_file.AddLine(wxT("descent2_mpgametype = hoard")); break;
			case DESCENT2MPGAMETYPE_TEAM_HOARD:       settings_file.AddLine(wxT("descent2_mpgametype = team_hoard")); break;
			default: ;
		}
		switch (gamelaunch_dn3dgametype)
		{
			case DN3DMPGAMETYPE_DMSPAWN:    settings_file.AddLine(wxT("dn3d_mpgametype = dm_spawn")); break;
			case DN3DMPGAMETYPE_COOP:       settings_file.AddLine(wxT("dn3d_mpgametype = coop")); break;
			case DN3DMPGAMETYPE_DMNOSPAWN:  settings_file.AddLine(wxT("dn3d_mpgametype = dm_nospawn")); break;
			case DN3DMPGAMETYPE_TDMSPAWN:   settings_file.AddLine(wxT("dn3d_mpgametype = tdm_spawn")); break;
			case DN3DMPGAMETYPE_TDMNOSPAWN: settings_file.AddLine(wxT("dn3d_mpgametype = tdm_nospawn")); break;
			default: ;
		}
		switch (gamelaunch_swgametype)
		{
			case SWMPGAMETYPE_WBSPAWN:   settings_file.AddLine(wxT("sw_mpgametype = wb_spawn")); break;
			case SWMPGAMETYPE_WBNOSPAWN: settings_file.AddLine(wxT("sw_mpgametype = wb_nospawn")); break;
			case SWMPGAMETYPE_COOP:      settings_file.AddLine(wxT("sw_mpgametype = coop")); break;
			default: ;
		}
		switch (gamelaunch_dn3dspawn)
		{
			case DN3DSPAWN_NONE:      settings_file.AddLine(wxT("dn3d_spawntype = none")); break;
			case DN3DSPAWN_MONSTERS:  settings_file.AddLine(wxT("dn3d_spawntype = monsters")); break;
			case DN3DSPAWN_ITEMS:     settings_file.AddLine(wxT("dn3d_spawntype = items")); break;
			case DN3DSPAWN_INVENTORY: settings_file.AddLine(wxT("dn3d_spawntype = inventory")); break;
			case DN3DSPAWN_ALL:       settings_file.AddLine(wxT("dn3d_spawntype = all")); break;
			default: ;
		}
		switch (gamelaunch_blood_monster)
		{
			case BLOODMPMONSTERTYPE_NONE:    settings_file.AddLine(wxT("blood_mpmonstertype = none")); break;
			case BLOODMPMONSTERTYPE_ENABLED: settings_file.AddLine(wxT("blood_mpmonstertype = enabled")); break;
			case BLOODMPMONSTERTYPE_RESPAWN: settings_file.AddLine(wxT("blood_mpmonstertype = respawn")); break;
			default: ;
		}
		switch (gamelaunch_blood_weapon)
		{
			case BLOODMPWEAPONTYPE_NORESPAWN: settings_file.AddLine(wxT("blood_mpweapontype = norespawn")); break;
			case BLOODMPWEAPONTYPE_PERMANENT: settings_file.AddLine(wxT("blood_mpweapontype = permanent")); break;
			case BLOODMPWEAPONTYPE_RESPAWN:   settings_file.AddLine(wxT("blood_mpweapontype = respawn")); break;
			case BLOODMPWEAPONTYPE_MARKERS:   settings_file.AddLine(wxT("blood_mpweapontype = markers")); break;
			default: ;
		}
		switch (gamelaunch_blood_item)
		{
			case BLOODMPITEMTYPE_NORESPAWN: settings_file.AddLine(wxT("blood_mpitemtype = norespawn")); break;
			case BLOODMPITEMTYPE_RESPAWN:   settings_file.AddLine(wxT("blood_mpitemtype = respawn")); break;
			case BLOODMPITEMTYPE_MARKERS:   settings_file.AddLine(wxT("blood_mpitemtype = markers")); break;
			default: ;
		}
		switch (gamelaunch_blood_respawn)
		{
			case BLOODMPRESPAWNTYPE_RANDOM:  settings_file.AddLine(wxT("blood_mprespawntype = random")); break;
			case BLOODMPRESPAWNTYPE_WEAPONS: settings_file.AddLine(wxT("blood_mprespawntype = weapons")); break;
			case BLOODMPRESPAWNTYPE_ENEMIES: settings_file.AddLine(wxT("blood_mprespawntype = enemies")); break;
			default: ;
		}

		switch (gamelaunch_blood_packetmode)
		{
			case PACKETMODE_AUTO:        settings_file.AddLine(wxT("blood_packetmode = auto")); break;
			case PACKETMODE_BROADCAST:   settings_file.AddLine(wxT("blood_packetmode = broadcast")); break;
			case PACKETMODE_MASTERSLAVE: settings_file.AddLine(wxT("blood_packetmode = masterslave")); break;
			default: ;
		}

		switch (gamelaunch_duke3dw_connectiontype)
		{
			case CONNECTIONTYPE_PEER2PEER:   settings_file.AddLine(wxT("duke3dw_connectiontype = peer2peer")); break;
			case CONNECTIONTYPE_MASTERSLAVE: settings_file.AddLine(wxT("duke3dw_connectiontype = masterslave")); break;
			default: ;
		}

		switch (gamelaunch_eduke32_connectiontype)
		{
			case CONNECTIONTYPE_PEER2PEER:    settings_file.AddLine(wxT("eduke32_connectiontype = peer2peer")); break;
			case CONNECTIONTYPE_MASTERSLAVE:  settings_file.AddLine(wxT("eduke32_connectiontype = masterslave")); break;
			case CONNECTIONTYPE_SERVERCLIENT: settings_file.AddLine(wxT("eduke32_connectiontype = serverclient")); break;
			default: ;
		}

		settings_file.AddLine(wxString::Format(wxT("custom_use_nullmodem = %d"), gamelaunch_custom_use_nullmodem));

		settings_file.AddLine(wxT("blood_args_for_host = ") + gamelaunch_blood_args_for_host);
		settings_file.AddLine(wxT("descent_args_for_host = ") + gamelaunch_descent_args_for_host);
		settings_file.AddLine(wxT("descent2_args_for_host = ") + gamelaunch_descent2_args_for_host);
		settings_file.AddLine(wxT("dn3d_args_for_host = ") + gamelaunch_dn3d_args_for_host);
		settings_file.AddLine(wxT("sw_args_for_host = ") + gamelaunch_sw_args_for_host);
		settings_file.AddLine(wxT("blood_args_for_all = ") + gamelaunch_blood_args_for_all);
		settings_file.AddLine(wxT("descent_args_for_all = ") + gamelaunch_descent_args_for_all);
		settings_file.AddLine(wxT("descent2_args_for_all = ") + gamelaunch_descent2_args_for_all);
		settings_file.AddLine(wxT("dn3d_args_for_all = ") + gamelaunch_dn3d_args_for_all);
		settings_file.AddLine(wxT("sw_args_for_all = ") + gamelaunch_sw_args_for_all);
		settings_file.AddLine(wxString::Format(wxT("blood_maxping = %ld"), gamelaunch_blood_maxping));
		settings_file.AddLine(wxString::Format(wxT("descent_maxping = %ld"), gamelaunch_descent_maxping));
		settings_file.AddLine(wxString::Format(wxT("descent2_maxping = %ld"), gamelaunch_descent2_maxping));
		settings_file.AddLine(wxString::Format(wxT("dn3d_maxping = %ld"), gamelaunch_dn3d_maxping));
		settings_file.AddLine(wxString::Format(wxT("sw_maxping = %ld"), gamelaunch_sw_maxping));
		settings_file.AddLine(wxString::Format(wxT("custom_maxping = %ld"), gamelaunch_custom_maxping));
		settings_file.AddLine(wxString::Format(wxT("custom_rxdelay = %ld"), gamelaunch_custom_rxdelay));
		settings_file.AddLine(wxString::Format(wxT("custom_use_profilename = %d"), gamelaunch_custom_use_profilename));
		settings_file.AddLine(wxT("mod_name = ") + gamelaunch_modname);
		settings_file.AddLine(wxString::Format(wxT("show_mod_url = %d"), gamelaunch_show_modurl));
		settings_file.AddLine(wxT("mod_url = ") + gamelaunch_modurl);
		settings_file.AddLine(wxString::Format(wxT("advertise = %d"), gamelaunch_advertise));
		settings_file.AddLine(wxString::Format(wxT("skip_settings = %d"), gamelaunch_skip_settings));
		settings_file.AddLine(wxString::Format(wxT("dn3d_playercolnum = %ld"), gamelaunch_dn3d_playercolnum));
		settings_file.AddLine(wxString::Format(wxT("sw_playercolnum = %ld"), gamelaunch_sw_playercolnum));
#if USE_PASSWORD
		settings_file.AddLine(wxString::Format(wxT("use_password = %d"), gamelaunch_use_password));
		settings_file.AddLine(wxT("password = ") + gamelaunch_password);
#endif
		settings_file.AddLine(wxString::Format(wxT("show_timestamps = %d"), show_timestamps));
		//  settings_file.AddLine(wxString::Format(wxT("last_serverlist_in_cycle = %lu"), last_serverlist_in_cycle));
		//  settings_file.AddLine(wxString::Format(wxT("enable_natfree = %d"), gamelaunch_enable_natfree));

		// Source ports settings: Blood
		settings_file.AddLine(wxString::Format(wxT("dosbloodsw = %d"), have_dosbloodsw));
		settings_file.AddLine(wxT("dosbloodsw_exec = ") + dosbloodsw_exec);
		settings_file.AddLine(wxString::Format(wxT("dosbloodrg = %d"), have_dosbloodrg));
		settings_file.AddLine(wxT("dosbloodrg_exec = ") + dosbloodrg_exec);
		settings_file.AddLine(wxString::Format(wxT("dosbloodpp = %d"), have_dosbloodpp));
		settings_file.AddLine(wxT("dosbloodpp_exec = ") + dosbloodpp_exec);
		settings_file.AddLine(wxString::Format(wxT("dosbloodou = %d"), have_dosbloodou));
		settings_file.AddLine(wxT("dosbloodou_exec = ") + dosbloodou_exec);
		settings_file.AddLine(wxT("blood_maps_dir = ") + blood_maps_dir);
		// Source ports settings: Descent
		settings_file.AddLine(wxString::Format(wxT("dosdescent = %d"), have_dosdescent));
		settings_file.AddLine(wxT("dosdescent_exec = ") + dosdescent_exec);
		settings_file.AddLine(wxString::Format(wxT("d1xrebirth = %d"), have_d1xrebirth));
		settings_file.AddLine(wxT("d1xrebirth_exec = ") + d1xrebirth_exec);
		settings_file.AddLine(wxT("descent_maps_dir = ") + descent_maps_dir);
		// Source ports settings: Descent 2
		settings_file.AddLine(wxString::Format(wxT("dosdescent2 = %d"), have_dosdescent2));
		settings_file.AddLine(wxT("dosdescent2_exec = ") + dosdescent2_exec);
		settings_file.AddLine(wxString::Format(wxT("d2xrebirth = %d"), have_d2xrebirth));
		settings_file.AddLine(wxT("d2xrebirth_exec = ") + d2xrebirth_exec);
		settings_file.AddLine(wxT("descent2_maps_dir = ") + descent2_maps_dir);
		// Source ports settings: Duke Nukem 3D
		settings_file.AddLine(wxString::Format(wxT("dosdukesw = %d"), have_dosdukesw));
		settings_file.AddLine(wxT("dosdukesw_exec = ") + dosdukesw_exec);
		settings_file.AddLine(wxString::Format(wxT("dosdukerg = %d"), have_dosdukerg));
		settings_file.AddLine(wxT("dosdukerg_exec = ") + dosdukerg_exec);
		settings_file.AddLine(wxString::Format(wxT("dosdukeae = %d"), have_dosdukeae));
		settings_file.AddLine(wxT("dosdukeae_exec = ") + dosdukeae_exec);
		settings_file.AddLine(wxString::Format(wxT("duke3dw = %d"), have_duke3dw));
		settings_file.AddLine(wxT("duke3dw_exec = ") + duke3dw_exec);
		settings_file.AddLine(wxString::Format(wxT("eduke32 = %d"), have_eduke32));
		settings_file.AddLine(wxT("eduke32_exec = ") + eduke32_exec);
		settings_file.AddLine(wxString::Format(wxT("nduke = %d"), have_nduke));
		settings_file.AddLine(wxT("nduke_exec = ") + nduke_exec);
		settings_file.AddLine(wxString::Format(wxT("hduke = %d"), have_hduke));
		settings_file.AddLine(wxT("hduke_exec = ") + hduke_exec);
		settings_file.AddLine(wxString::Format(wxT("xduke = %d"), have_xduke));
		settings_file.AddLine(wxT("xduke_exec = ") + xduke_exec);
		settings_file.AddLine(wxT("dn3d_maps_dir = ") + dn3d_maps_dir);
		// Source ports settings: Shadow Warrior
		settings_file.AddLine(wxString::Format(wxT("dosswsw = %d"), have_dosswsw));
		settings_file.AddLine(wxT("dosswsw_exec = ") + dosswsw_exec);
		settings_file.AddLine(wxString::Format(wxT("dosswrg = %d"), have_dosswrg));
		settings_file.AddLine(wxT("dosswrg_exec = ") + dosswrg_exec);
		settings_file.AddLine(wxString::Format(wxT("voidsw = %d"), have_voidsw));
		settings_file.AddLine(wxT("voidsw_exec = ") + voidsw_exec);
		settings_file.AddLine(wxString::Format(wxT("swp = %d"), have_swp));
		settings_file.AddLine(wxT("swp_exec = ") + swp_exec);
		settings_file.AddLine(wxT("sw_maps_dir = ") + sw_maps_dir);
		// DOSBox settings
		settings_file.AddLine(wxString::Format(wxT("dosbox = %d"), have_dosbox));
		settings_file.AddLine(wxString::Format(wxT("dosbox_use_conf = %d"), dosbox_use_conf));
		settings_file.AddLine(wxString::Format(wxT("bmouse = %d"), have_bmouse));
		switch (dosbox_cdmount)
		{
			case CDROMMOUNT_NONE: settings_file.AddLine(wxT("dosbox_cdrom_mount = none")); break;
			case CDROMMOUNT_DIR:  settings_file.AddLine(wxT("dosbox_cdrom_mount = dir")); break;
			case CDROMMOUNT_IMG:  settings_file.AddLine(wxT("dosbox_cdrom_mount = image")); break;
			default: ;
		}
		settings_file.AddLine(wxT("dosbox_exec = ") + dosbox_exec);
		settings_file.AddLine(wxT("dosbox_conf = ") + dosbox_conf);
		settings_file.AddLine(wxT("dosbox_cdrom_location = ") + dosbox_cdrom_location);
		settings_file.AddLine(wxT("dosbox_cdrom_image = ") + dosbox_cdrom_image);
		settings_file.AddLine(wxT("bmouse_exec = ") + bmouse_exec);
#ifndef __WXMSW__
		// Windows compatibility (e.g. Wine) settings
		settings_file.AddLine(wxString::Format(wxT("wine = %d"), have_wine));
		settings_file.AddLine(wxT("wine_exec = ") + wine_exec);
#endif
		// Multiplayer and network settings
		settings_file.AddLine(wxString::Format(wxT("play_snd_join = %d"), play_snd_join));
		settings_file.AddLine(wxT("snd_join_file = ") + snd_join_file);
		settings_file.AddLine(wxString::Format(wxT("play_snd_leave = %d"), play_snd_leave));
		settings_file.AddLine(wxT("snd_leave_file = ") + snd_leave_file);
		settings_file.AddLine(wxString::Format(wxT("play_snd_send = %d"), play_snd_send));
		settings_file.AddLine(wxT("snd_send_file = ") + snd_send_file);
		settings_file.AddLine(wxString::Format(wxT("play_snd_receive = %d"), play_snd_receive));
		settings_file.AddLine(wxT("snd_receive_file = ") + snd_receive_file);
		settings_file.AddLine(wxString::Format(wxT("play_snd_error = %d"), play_snd_error));
		settings_file.AddLine(wxT("snd_error_file = ") + snd_error_file);
		settings_file.AddLine(wxString::Format(wxT("mute_sounds_while_ingame = %d"), mute_sounds_while_ingame));
		settings_file.AddLine(wxT("nickname = ") + nickname);
		settings_file.AddLine(wxT("game_nickname = ") + game_nickname);
		settings_file.AddLine(wxString::Format(wxT("game_port_number = %ld"), game_port_number));
		settings_file.AddLine(wxString::Format(wxT("server_port_number = %ld"), server_port_number));
#ifdef ENABLE_UPNP
		settings_file.AddLine(wxString::Format(wxT("enable_upnp = %d"), enable_upnp));
#endif
		// Advanced options
		settings_file.AddLine(wxString::Format(wxT("duke3dw_userpath_use = %d"), duke3dw_userpath_use));
		settings_file.AddLine(wxT("duke3dw_userpath = ") + duke3dw_userpath);
		settings_file.AddLine(wxString::Format(wxT("eduke32_userpath_use = %d"), eduke32_userpath_use));
		settings_file.AddLine(wxT("eduke32_userpath = ") + eduke32_userpath);
		settings_file.AddLine(wxString::Format(wxT("nduke_userpath_use = %d"), nduke_userpath_use));
		settings_file.AddLine(wxT("nduke_userpath = ") + nduke_userpath);
		settings_file.AddLine(wxString::Format(wxT("hduke_userpath_use = %d"), hduke_userpath_use));
		settings_file.AddLine(wxT("hduke_userpath = ") + hduke_userpath);
		settings_file.AddLine(wxString::Format(wxT("voidsw_userpath_use = %d"), voidsw_userpath_use));
		settings_file.AddLine(wxT("voidsw_userpath = ") + voidsw_userpath);
		settings_file.AddLine(wxT("banlist_filepath = ") + banlist_filepath);
		settings_file.AddLine(wxString::Format(wxT("override_browser = %d"), override_browser));
		settings_file.AddLine(wxT("browser_exec = ") + browser_exec);
		settings_file.AddLine(wxString::Format(wxT("override_soundcmd = %d"), override_soundcmd));
		settings_file.AddLine(wxT("playsound_cmd = ") + playsound_cmd);
		settings_file.AddLine(wxString::Format(wxT("enable_localip_optimization = %d"), enable_localip_optimization));
#ifndef __WXMSW__
		settings_file.AddLine(wxT("terminal_fullcmd = ") + terminal_fullcmd);
#endif
		settings_file.AddLine(wxString::Format(wxT("host_autoaccept_downloads = %d"), host_autoaccept_downloads));
		// Extra server lists.
		l_num_of_items = extraservers_portnums.GetCount();
		l_str_line = wxT("extra_serverlists =");
		for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
		{
			l_str_line << wxT(" \"");
			l_str_curr = extraservers_addrs[l_loop_var];
			l_str_length = l_str_curr.Len();
			for (l_loop_var2 = 0; l_loop_var2 < l_str_length; l_loop_var2++)
			{
				l_curr_char = l_str_curr[l_loop_var2];
				if ((l_curr_char == wxT(':')) ||
					(l_curr_char == wxT('"')) || (l_curr_char == wxT('\\')))
					l_str_line << wxT('\\');
				l_str_line << l_curr_char;
			}
			l_str_line << wxString::Format(wxT(":%d\""), extraservers_portnums[l_loop_var]);
		}
		settings_file.AddLine(l_str_line);
		// Windows UI controls sizes.
		settings_file.AddLine(wxT("game_filter_tree = ") + game_filter_tree);
		settings_file.AddLine(wxString::Format(wxT("main_col_private_len = %d"), main_col_private_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_loc_len = %d"), main_col_loc_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_roomname_len = %d"), main_col_roomname_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_ping_len = %d"), main_col_ping_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_flux_len = %d"), main_col_flux_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_game_len = %d"), main_col_game_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_srcport_len = %d"), main_col_srcport_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_players_len = %d"), main_col_players_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_map_len = %d"), main_col_map_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_tcmod_len = %d"), main_col_tcmod_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_gametype_len = %d"), main_col_gametype_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_skill_len = %d"), main_col_skill_len));
		settings_file.AddLine(wxString::Format(wxT("main_col_ingame_len = %d"), main_col_ingame_len));

		settings_file.AddLine(wxString::Format(wxT("mainplayer_col_loc_len = %d"), mainplayer_col_loc_len));
		settings_file.AddLine(wxString::Format(wxT("mainplayer_col_nick_len = %d"), mainplayer_col_nick_len));
		settings_file.AddLine(wxString::Format(wxT("mainplayer_col_ip_len = %d"), mainplayer_col_ip_len));
		settings_file.AddLine(wxString::Format(wxT("mainplayer_col_os_len = %d"), mainplayer_col_os_len));

		settings_file.AddLine(wxString::Format(wxT("host_col_ready_len = %d"), host_col_ready_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_loc_len = %d"), host_col_loc_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_nick_len = %d"), host_col_nick_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_ping_len = %d"), host_col_ping_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_flux_len = %d"), host_col_flux_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_os_len = %d"), host_col_os_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_detectedip_len = %d"), host_col_detectedip_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_ingameip_len = %d"), host_col_ingameip_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_requests_len = %d"), host_col_requests_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_filenames_len = %d"), host_col_filenames_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_filesizes_len = %d"), host_col_filesizes_len));

		settings_file.AddLine(wxString::Format(wxT("host_col_settings_len = %d"), host_col_settings_len));
		settings_file.AddLine(wxString::Format(wxT("host_col_value_len = %d"), host_col_value_len));

		settings_file.AddLine(wxString::Format(wxT("client_col_ready_len = %d"), client_col_ready_len));
		settings_file.AddLine(wxString::Format(wxT("client_col_loc_len = %d"), client_col_loc_len));
		settings_file.AddLine(wxString::Format(wxT("client_col_nick_len = %d"), client_col_nick_len));
		settings_file.AddLine(wxString::Format(wxT("client_col_ping_len = %d"), client_col_ping_len));
		settings_file.AddLine(wxString::Format(wxT("client_col_flux_len = %d"), client_col_flux_len));
		settings_file.AddLine(wxString::Format(wxT("client_col_os_len = %d"), client_col_os_len));
		settings_file.AddLine(wxString::Format(wxT("client_col_detectedip_len = %d"), client_col_detectedip_len));
		settings_file.AddLine(wxString::Format(wxT("client_col_ingameip_len = %d"), client_col_ingameip_len));

		settings_file.AddLine(wxString::Format(wxT("client_col_settings_len = %d"), client_col_settings_len));
		settings_file.AddLine(wxString::Format(wxT("client_col_value_len = %d"), client_col_value_len));

		settings_file.AddLine(wxString::Format(wxT("main_win_width = %d"), main_win_width));
		settings_file.AddLine(wxString::Format(wxT("main_win_height = %d"), main_win_height));
		settings_file.AddLine(wxString::Format(wxT("host_win_width = %d"), host_win_width));
		settings_file.AddLine(wxString::Format(wxT("host_win_height = %d"), host_win_height));
		settings_file.AddLine(wxString::Format(wxT("client_win_width = %d"), client_win_width));
		settings_file.AddLine(wxString::Format(wxT("client_win_height = %d"), client_win_height));

		settings_file.AddLine(wxString::Format(wxT("main_is_maximized = %d"), main_is_maximized));
		settings_file.AddLine(wxString::Format(wxT("host_is_maximized = %d"), host_is_maximized));
		settings_file.AddLine(wxString::Format(wxT("client_is_maximized = %d"), client_is_maximized));

		// Main window: Retrieve rooms on startup, and stuff for manual joining.
		settings_file.AddLine(wxString::Format(wxT("lookup_on_startup = %d"), lookup_on_startup));

		l_num_of_items = manualjoin_portnums.GetCount();
		l_str_line = wxT("manualjoin_items =");
		for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
		{
			l_str_line << wxT(" \"");
			l_str_curr = manualjoin_names[l_loop_var];
			l_str_length = l_str_curr.Len();
			for (l_loop_var2 = 0; l_loop_var2 < l_str_length; l_loop_var2++)
			{
				l_curr_char = l_str_curr[l_loop_var2];
				if ((l_curr_char == wxT(':')) ||
					(l_curr_char == wxT('"')) || (l_curr_char == wxT('\\')))
					l_str_line << wxT('\\');
				l_str_line << l_curr_char;
			}
			l_str_line << wxT(':');
			l_str_curr = manualjoin_addrs[l_loop_var];
			l_str_length = l_str_curr.Len();
			for (l_loop_var2 = 0; l_loop_var2 < l_str_length; l_loop_var2++)
			{
				l_curr_char = l_str_curr[l_loop_var2];
				if ((l_curr_char == wxT(':')) ||
					(l_curr_char == wxT('"')) || (l_curr_char == wxT('\\')))
					l_str_line << wxT('\\');
				l_str_line << l_curr_char;
			}
			l_str_line << wxString::Format(wxT(":%d\""), (int)manualjoin_portnums[l_loop_var]);
		}
		settings_file.AddLine(l_str_line);


		have_custom_mpprofile = false;

		l_loop_var = 1;

		custom_profile_current = custom_profile_list;

		while (custom_profile_current)
		{
			settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_profilename = "), (int)l_loop_var) + custom_profile_current->profilename);
			settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_spgameonly = %d"), (int)l_loop_var, custom_profile_current->spgameonly));
			settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_spexecutable = "), (int)l_loop_var) + custom_profile_current->spexecutable);

			if (!custom_profile_current->spgameonly)
			{
				settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_sameexecutable = %d"), (int)l_loop_var, custom_profile_current->sameexecutable));

				if (!custom_profile_current->sameexecutable)
					settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_mpexecutable = "), (int)l_loop_var) + custom_profile_current->mpexecutable);
			}

			switch (custom_profile_current->cdmount)
			{
				case CDROMMOUNT_NONE:
					settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_cdmount = "), (int)l_loop_var) + wxT("none"));
					break;
				case CDROMMOUNT_DIR:
					settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_cdmount = "), (int)l_loop_var) + wxT("dir"));
					settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_cdlocation = "), (int)l_loop_var) + custom_profile_current->cdlocation);
					break;
				case CDROMMOUNT_IMG:
					settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_cdmount = "), (int)l_loop_var) + wxT("image"));
					settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_cdimage = "), (int)l_loop_var) + custom_profile_current->cdimage);
					break;
				default: ;
			}

			if (!custom_profile_current->spgameonly)
			{
				settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_ingamesupport = %d"), (int)l_loop_var, custom_profile_current->ingamesupport));
				settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_usenetbios = %d"), (int)l_loop_var, custom_profile_current->usenetbios));

				if (custom_profile_current->usenetbios)
					settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_netbiospath = "), (int)l_loop_var) + custom_profile_current->netbiospath);
			}

			if (!custom_profile_current->extraargs.IsEmpty())
				settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_extraargs = "), (int)l_loop_var) + custom_profile_current->extraargs);

			if (!custom_profile_current->spgameonly)
			{
				if (!custom_profile_current->extrahostargs.IsEmpty())
					settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_extrahostargs = "), (int)l_loop_var) + custom_profile_current->extrahostargs);

				if (!custom_profile_current->extraallargs.IsEmpty())
					settings_file.AddLine(wxString::Format(wxT("custom_profile_%d_extraallargs = "), (int)l_loop_var) + custom_profile_current->extraallargs);
			}

			if (!custom_profile_current->spgameonly)
				have_custom_mpprofile = true;

			l_loop_var++;

			custom_profile_current = custom_profile_current->next;
		}


		l_loop_var = 1;

		dn3dtcmod_profile_current = dn3dtcmod_profile_list;

		while (dn3dtcmod_profile_current)
		{
			settings_file.AddLine(wxString::Format(wxT("dn3dtcmod_profile_%d_profilename = "), l_loop_var) + dn3dtcmod_profile_current->profilename);

			l_num_of_items = dn3dtcmod_profile_current->modfiles.GetCount();
			l_str_line = wxString::Format(wxT("dn3dtcmod_profile_%d_modfiles = "), l_loop_var);
			for (l_loop_var2 = 0; l_loop_var2 < l_num_of_items; l_loop_var2++)
				l_str_line << wxT(" \"") << dn3dtcmod_profile_current->modfiles[l_loop_var2] << wxT('"');
			settings_file.AddLine(l_str_line);

			if (!dn3dtcmod_profile_current->confile.IsEmpty())
				settings_file.AddLine(wxString::Format(wxT("dn3dtcmod_profile_%d_confile = "), l_loop_var) + dn3dtcmod_profile_current->confile);

			if (!dn3dtcmod_profile_current->deffile.IsEmpty())
				settings_file.AddLine(wxString::Format(wxT("dn3dtcmod_profile_%d_deffile = "), l_loop_var) + dn3dtcmod_profile_current->deffile);

			settings_file.AddLine(wxString::Format(wxT("dn3dtcmod_profile_%d_usemodurl = %d"), l_loop_var, dn3dtcmod_profile_current->usemodurl));

			if (dn3dtcmod_profile_current->usemodurl)
				settings_file.AddLine(wxString::Format(wxT("dn3dtcmod_profile_%d_modurl = "), l_loop_var) + dn3dtcmod_profile_current->modurl);

			l_loop_var++;

			dn3dtcmod_profile_current = dn3dtcmod_profile_current->next;
		}


		l_loop_var = 1;

		swtcmod_profile_current = swtcmod_profile_list;

		while (swtcmod_profile_current)
		{
			settings_file.AddLine(wxString::Format(wxT("swtcmod_profile_%d_profilename = "), l_loop_var) + swtcmod_profile_current->profilename);

			l_num_of_items = swtcmod_profile_current->modfiles.GetCount();
			l_str_line = wxString::Format(wxT("swtcmod_profile_%d_modfiles = "), l_loop_var);
			for (l_loop_var2 = 0; l_loop_var2 < l_num_of_items; l_loop_var2++)
				l_str_line << wxT(" \"") << swtcmod_profile_current->modfiles[l_loop_var2] << wxT('"');
			settings_file.AddLine(l_str_line);

			if (!swtcmod_profile_current->deffile.IsEmpty())
				settings_file.AddLine(wxString::Format(wxT("swtcmod_profile_%d_deffile = "), l_loop_var) + swtcmod_profile_current->deffile);

			settings_file.AddLine(wxString::Format(wxT("swtcmod_profile_%d_usemodurl = %d"), l_loop_var, swtcmod_profile_current->usemodurl));

			if (swtcmod_profile_current->usemodurl)
				settings_file.AddLine(wxString::Format(wxT("swtcmod_profile_%d_modurl = "), l_loop_var) + swtcmod_profile_current->modurl);

			l_loop_var++;

			swtcmod_profile_current = swtcmod_profile_current->next;
		}


		settings_file.Write();
		settings_file.Close();
		return true;
	}
	return false;
}
/*
bool LauncherConfig::Remove()
{
	return wxRemoveFile(YANG_CONFIG_FULLFILEPATH);
}
*/
