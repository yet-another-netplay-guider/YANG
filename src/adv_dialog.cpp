/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/colordlg.h>
#include <wx/dirdlg.h>
#include <wx/filedlg.h>
#include <wx/gbsizer.h>
#endif

#include "theme.h"

#include "adv_dialog.h"
#include "main_frame.h"
#include "config.h"
#include "yang.h"
#include "yang.xpm"

BEGIN_EVENT_TABLE(AdvDialog, YANGDialog)

EVT_BUTTON(wxID_OK, AdvDialog::OnOk)
//EVT_BUTTON(wxID_APPLY, AdvDialog::OnApply)
EVT_BUTTON(wxID_CANCEL, AdvDialog::OnCancel)

EVT_BUTTON(ID_SELECTEDUKE32USERPATH, AdvDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTDUKE3DWUSERPATH, AdvDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTNDUKEUSERPATH, AdvDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTHDUKEUSERPATH, AdvDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTVOIDSWUSERPATH, AdvDialog::OnSelectDir)

EVT_BUTTON(ID_SELECTBANLISTFILE, AdvDialog::OnLocateBanListTextFile)

EVT_BUTTON(ID_LOCATEBROWSER, AdvDialog::OnLocateExec)

EVT_CHECKBOX(ID_EDUKE32USERPATHCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DUKE3DWUSERPATHCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_NDUKEUSERPATHCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_HDUKEUSERPATHCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_VOIDSWUSERPATHCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_BROWSERCHECKBOX, AdvDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SOUNDCMDCHECKBOX, AdvDialog::OnCheckBoxClick)

EVT_BUTTON(ID_SERVERLISTADD, AdvDialog::OnServerListAdd)
EVT_BUTTON(ID_SERVERLISTREM, AdvDialog::OnServerListRem)

END_EVENT_TABLE()

AdvDialog::AdvDialog() : YANGDialog(NULL, wxID_ANY, wxT("Advanced settings"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* AdvDialogSizer = new wxBoxSizer( wxVERTICAL );

	m_advancednotebook = new YANGNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_advpanel = new YANGPanel( m_advancednotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* advpanelbSizer = new wxBoxSizer( wxVERTICAL );

	wxGridBagSizer* advpanelgbSizer = new wxGridBagSizer( 0, 0 );
	advpanelgbSizer->SetFlexibleDirection( wxBOTH );
	advpanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	int i = -1;

	m_userpathstaticText = new wxStaticText( m_advpanel, wxID_ANY, wxT("On multi-user systems it may be required to specify user folders\nwhere it's possible to put temporary files (e.g. MAP files).\nNOTE THAT THE SOURCE PORT MUST SUPPORT THAT\nUSING THE SELECTED FOLDER \"OUT OF THE BOX\"."), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_userpathstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_eduke32userpathcheckBox = new YANGCheckBox( m_advpanel, ID_EDUKE32USERPATHCHECKBOX, SRCPORTNAME_EDUKE32, wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_eduke32userpathcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_eduke32userpathtextCtrl = new YANGTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_eduke32userpathtextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_eduke32userpathbutton = new YANGButton( m_advpanel, ID_SELECTEDUKE32USERPATH, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_eduke32userpathbutton, wxGBPosition( i, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_duke3dwuserpathcheckBox = new YANGCheckBox( m_advpanel, ID_DUKE3DWUSERPATHCHECKBOX, SRCPORTNAME_DUKE3DW, wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_duke3dwuserpathcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_duke3dwuserpathtextCtrl = new YANGTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_duke3dwuserpathtextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_duke3dwuserpathbutton = new YANGButton( m_advpanel, ID_SELECTDUKE3DWUSERPATH, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_duke3dwuserpathbutton, wxGBPosition( i, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_ndukeuserpathcheckBox = new YANGCheckBox( m_advpanel, ID_NDUKEUSERPATHCHECKBOX, SRCPORTNAME_NDUKE, wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_ndukeuserpathcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_ndukeuserpathtextCtrl = new YANGTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_ndukeuserpathtextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_ndukeuserpathbutton = new YANGButton( m_advpanel, ID_SELECTNDUKEUSERPATH, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_ndukeuserpathbutton, wxGBPosition( i, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hdukeuserpathcheckBox = new YANGCheckBox( m_advpanel, ID_HDUKEUSERPATHCHECKBOX, SRCPORTNAME_HDUKE, wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_hdukeuserpathcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hdukeuserpathtextCtrl = new YANGTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_hdukeuserpathtextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_hdukeuserpathbutton = new YANGButton( m_advpanel, ID_SELECTHDUKEUSERPATH, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_hdukeuserpathbutton, wxGBPosition( i, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_voidswuserpathcheckBox = new YANGCheckBox( m_advpanel, ID_VOIDSWUSERPATHCHECKBOX, SRCPORTNAME_VOIDSW, wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_voidswuserpathcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_voidswuserpathtextCtrl = new YANGTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_voidswuserpathtextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_voidswuserpathbutton = new YANGButton( m_advpanel, ID_SELECTVOIDSWUSERPATH, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_voidswuserpathbutton, wxGBPosition( i, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_banliststaticText = new wxStaticText( m_advpanel, wxID_ANY, wxT("Banlist file and location:"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_banliststaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_banlisttextCtrl = new YANGTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_banlisttextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_banlistbutton = new YANGButton( m_advpanel, ID_SELECTBANLISTFILE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_banlistbutton, wxGBPosition( i, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_overridebrowsercheckBox = new YANGCheckBox( m_advpanel, ID_BROWSERCHECKBOX, wxT("Override default browser for opening URLs:"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_overridebrowsercheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_browsertextCtrl = new YANGTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_browsertextCtrl, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_browserlocatebutton = new YANGButton( m_advpanel, ID_LOCATEBROWSER, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_browserlocatebutton, wxGBPosition( i, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_overridesoundcheckBox = new YANGCheckBox( m_advpanel, ID_SOUNDCMDCHECKBOX, wxT("Override sound playback using the following command:"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_overridesoundcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundcmdtextCtrl = new YANGTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_soundcmdtextCtrl, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_localipoptimizecheckBox = new YANGCheckBox( m_advpanel, wxID_ANY, wxT("In your hosted room, local players will connect to each other using their local IPs."), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_localipoptimizecheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
#ifndef __WXMSW__
	m_terminalstaticText = new wxStaticText( m_advpanel, wxID_ANY, wxT("When launching a game from a script, open a terminal \nusing the following command (may include any argument).\nExamples: gnome-terminal -x, konsole -e, open"), wxDefaultPosition, wxDefaultSize, 0 );
	advpanelgbSizer->Add( m_terminalstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_terminaltextCtrl = new YANGTextCtrl( m_advpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	advpanelgbSizer->Add( m_terminaltextCtrl, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
#endif

	advpanelgbSizer->AddGrowableCol( 1 );
	
	advpanelbSizer->Add( advpanelgbSizer, 1, wxEXPAND, 0 );

	m_advpanel->SetSizer( advpanelbSizer );
	m_advpanel->Layout();
	advpanelbSizer->Fit( m_advpanel );
	m_advancednotebook->AddPage( m_advpanel, wxT("General"), true );
	m_extraserverspanel = new YANGPanel( m_advancednotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* extraserverbSizer = new wxBoxSizer( wxVERTICAL );

	wxFlexGridSizer* extraserverfgSizer = new wxFlexGridSizer( 2, 4, 0, 0 );
	extraserverfgSizer->AddGrowableCol( 0 );
	extraserverfgSizer->SetFlexibleDirection( wxBOTH );
	extraserverfgSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_extraserveraddrstaticText = new wxStaticText( m_extraserverspanel, wxID_ANY, wxT("Hostname or IP"), wxDefaultPosition, wxDefaultSize, 0 );
	extraserverfgSizer->Add( m_extraserveraddrstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	extraserverfgSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_extraserverportnumstaticText = new wxStaticText( m_extraserverspanel, wxID_ANY, wxT("Port Number"), wxDefaultPosition, wxDefaultSize, 0 );
	extraserverfgSizer->Add( m_extraserverportnumstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	extraserverfgSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_extraserverhostaddrtextCtrl = new YANGTextCtrl( m_extraserverspanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	extraserverfgSizer->Add( m_extraserverhostaddrtextCtrl, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_extraserverseparatorstaticText = new wxStaticText( m_extraserverspanel, wxID_ANY, wxT(":"), wxDefaultPosition, wxDefaultSize, 0 );
	extraserverfgSizer->Add( m_extraserverseparatorstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_extraserverportnumtextCtrl = new YANGTextCtrl( m_extraserverspanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	extraserverfgSizer->Add( m_extraserverportnumtextCtrl, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_extraserveraddbutton = new YANGButton( m_extraserverspanel, ID_SERVERLISTADD, wxT("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	extraserverfgSizer->Add( m_extraserveraddbutton, 0, wxALL|wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	extraserverbSizer->Add( extraserverfgSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* extraserverslistbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_extraserverlistBox = new YANGListBox( m_extraserverspanel, wxID_ANY, wxDefaultPosition, (wxDLG_UNIT(this, wxSize(-1, 80)))); 
	extraserverslistbSizer->Add( m_extraserverlistBox, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_extraserverrembutton = new YANGButton( m_extraserverspanel, ID_SERVERLISTREM, wxT("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	extraserverslistbSizer->Add( m_extraserverrembutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	extraserverbSizer->Add( extraserverslistbSizer, 0, wxEXPAND, 5 );


	extraserverbSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_extraserverspanel->SetSizer( extraserverbSizer );
	m_extraserverspanel->Layout();
	extraserverbSizer->Fit( m_extraserverspanel );
	m_advancednotebook->AddPage( m_extraserverspanel, wxT("Extra server lists"), false );

	AdvDialogSizer->Add( m_advancednotebook, 1, wxEXPAND | wxALL, 5 );

	m_advsdbSizer = new wxStdDialogButtonSizer();
	m_advsdbSizerOK = new YANGButton( this, wxID_OK );
	m_advsdbSizer->AddButton( m_advsdbSizerOK );
	m_advsdbSizerCancel = new YANGButton( this, wxID_CANCEL );
	m_advsdbSizer->AddButton( m_advsdbSizerCancel );
	m_advsdbSizer->Realize();
	AdvDialogSizer->Add( m_advsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxBOTTOM, 5 );

	this->SetSizer( AdvDialogSizer );
	this->Layout();
	AdvDialogSizer->Fit( this );

	Centre();
	SetIcon(wxIcon(yang_xpm));

	m_eduke32userpathcheckBox->SetValue(g_configuration->eduke32_userpath_use);
	m_eduke32userpathtextCtrl->SetValue(g_configuration->eduke32_userpath);
	m_eduke32userpathtextCtrl->Enable(g_configuration->eduke32_userpath_use);
	m_eduke32userpathbutton->Enable(g_configuration->eduke32_userpath_use);

	m_duke3dwuserpathcheckBox->SetValue(g_configuration->duke3dw_userpath_use);
	m_duke3dwuserpathtextCtrl->SetValue(g_configuration->duke3dw_userpath);
	m_duke3dwuserpathtextCtrl->Enable(g_configuration->duke3dw_userpath_use);
	m_duke3dwuserpathbutton->Enable(g_configuration->duke3dw_userpath_use);

	m_ndukeuserpathcheckBox->SetValue(g_configuration->nduke_userpath_use);
	m_ndukeuserpathtextCtrl->SetValue(g_configuration->nduke_userpath);
	m_ndukeuserpathtextCtrl->Enable(g_configuration->nduke_userpath_use);
	m_ndukeuserpathbutton->Enable(g_configuration->nduke_userpath_use);

	m_hdukeuserpathcheckBox->SetValue(g_configuration->hduke_userpath_use);
	m_hdukeuserpathtextCtrl->SetValue(g_configuration->hduke_userpath);
	m_hdukeuserpathtextCtrl->Enable(g_configuration->hduke_userpath_use);
	m_hdukeuserpathbutton->Enable(g_configuration->hduke_userpath_use);

	m_voidswuserpathcheckBox->SetValue(g_configuration->voidsw_userpath_use);
	m_voidswuserpathtextCtrl->SetValue(g_configuration->voidsw_userpath);
	m_voidswuserpathtextCtrl->Enable(g_configuration->voidsw_userpath_use);
	m_voidswuserpathbutton->Enable(g_configuration->voidsw_userpath_use);

	m_banlisttextCtrl->SetValue(g_configuration->banlist_filepath);
#ifndef __WXMSW__
	m_terminaltextCtrl->SetValue(g_configuration->terminal_fullcmd);
#endif
	m_overridebrowsercheckBox->SetValue(g_configuration->override_browser);
	m_browsertextCtrl->SetValue(g_configuration->browser_exec);
	m_browsertextCtrl->Enable(g_configuration->override_browser);
	m_browserlocatebutton->Enable(g_configuration->override_browser);

	m_overridesoundcheckBox->SetValue(g_configuration->override_soundcmd);
	m_soundcmdtextCtrl->SetValue(g_configuration->playsound_cmd);
	m_soundcmdtextCtrl->Enable(g_configuration->override_soundcmd);

	m_localipoptimizecheckBox->SetValue(g_configuration->enable_localip_optimization);

	m_extra_addrs = g_configuration->extraservers_addrs;
	m_extra_portnums = g_configuration->extraservers_portnums;
	size_t l_num_of_items = m_extra_addrs.GetCount();
	for (size_t l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
		m_extraserverlistBox->Append(m_extra_addrs[l_loop_var] + wxString::Format(wxT(":%d"), m_extra_portnums[l_loop_var]));
}

AdvDialog::~AdvDialog()
{
	if (!g_main_frame)
		g_main_frame = new MainFrame(NULL);
	g_main_frame->ShowMainFrame();
	//g_adv_dialog = NULL;
}

void AdvDialog::OnSelectDir(wxCommandEvent& event)
{
	wxDirDialog l_dialog(NULL, wxT("Select a directory"), wxEmptyString,
		wxDD_DEFAULT_STYLE|wxDD_DIR_MUST_EXIST);
	if (l_dialog.ShowModal() == wxID_OK)
	{
		wxString l_path = l_dialog.GetPath();
		switch (event.GetId())
		{
			case ID_SELECTEDUKE32USERPATH: m_eduke32userpathtextCtrl->SetValue(l_path); break;
			case ID_SELECTDUKE3DWUSERPATH: m_duke3dwuserpathtextCtrl->SetValue(l_path); break;
			case ID_SELECTNDUKEUSERPATH:   m_ndukeuserpathtextCtrl->SetValue(l_path); break;
			case ID_SELECTHDUKEUSERPATH:   m_hdukeuserpathtextCtrl->SetValue(l_path); break;
			case ID_SELECTVOIDSWUSERPATH:  m_voidswuserpathtextCtrl->SetValue(l_path); break;
			default: ;
		}
	}
}

void AdvDialog::OnLocateBanListTextFile(wxCommandEvent& WXUNUSED(event))
{
	wxFileDialog l_dialog(NULL, wxT("Choose path and file name"), wxEmptyString, wxEmptyString,
		wxT("TXT files (*.txt)|*.txt|All files|*"), wxFD_OPEN);
	if (l_dialog.ShowModal() == wxID_OK)
		m_banlisttextCtrl->SetValue(l_dialog.GetPath());
}

void AdvDialog::OnLocateExec(wxCommandEvent& WXUNUSED(event))
{
	wxFileDialog l_dialog(NULL, wxT("Select a file"), wxEmptyString, wxEmptyString,
		wxT("All files|*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);
	if (l_dialog.ShowModal() == wxID_OK)
		m_browsertextCtrl->SetValue(l_dialog.GetPath());
}

void AdvDialog::OnCheckBoxClick(wxCommandEvent& event)
{
	bool l_selection = event.IsChecked();

	switch (event.GetId())
	{
		case ID_EDUKE32USERPATHCHECKBOX:
			m_eduke32userpathtextCtrl->Enable(l_selection);
			m_eduke32userpathbutton->Enable(l_selection);
			break;
		case ID_DUKE3DWUSERPATHCHECKBOX:
			m_duke3dwuserpathtextCtrl->Enable(l_selection);
			m_duke3dwuserpathbutton->Enable(l_selection);
			break;
		case ID_NDUKEUSERPATHCHECKBOX:
			m_ndukeuserpathtextCtrl->Enable(l_selection);
			m_ndukeuserpathbutton->Enable(l_selection);
			break;
		case ID_HDUKEUSERPATHCHECKBOX:
			m_hdukeuserpathtextCtrl->Enable(l_selection);
			m_hdukeuserpathbutton->Enable(l_selection);
			break;
		case ID_VOIDSWUSERPATHCHECKBOX:
			m_voidswuserpathtextCtrl->Enable(l_selection);
			m_voidswuserpathbutton->Enable(l_selection);
			break;
		case ID_BROWSERCHECKBOX:
			m_browsertextCtrl->Enable(l_selection);
			m_browserlocatebutton->Enable(l_selection);
			break;
		case ID_SOUNDCMDCHECKBOX:
			m_soundcmdtextCtrl->Enable(l_selection);
			break;
		default: ;
	}
}

void AdvDialog::OnServerListAdd(wxCommandEvent& WXUNUSED(event))
{
	long l_temp_num;
	m_extra_addrs.Add(m_extraserverhostaddrtextCtrl->GetValue());
	if (!((m_extraserverportnumtextCtrl->GetValue()).ToLong(&l_temp_num)))
		l_temp_num = 0;
	m_extra_portnums.Add(l_temp_num);
	l_temp_num = m_extra_addrs.GetCount()-1;
	m_extraserverlistBox->Append(m_extra_addrs[l_temp_num] + wxString::Format(wxT(":%d"), m_extra_portnums[l_temp_num]));
}

void AdvDialog::OnServerListRem(wxCommandEvent& WXUNUSED(event))
{
	int l_selection = m_extraserverlistBox->GetSelection();
	if (l_selection >= 0)
	{
		m_extra_addrs.RemoveAt(l_selection);
		m_extra_portnums.RemoveAt(l_selection);
		m_extraserverlistBox->Delete(l_selection);
		m_extraserverlistBox->SetSelection(l_selection);
	}
}
/*
void AdvDialog::OnApply(wxCommandEvent& WXUNUSED(event))
{
ApplySettings();
}
*/
void AdvDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	ApplySettings();
	Destroy();
}

void AdvDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	Destroy();
}

void AdvDialog::ApplySettings()
{
	//wxArrayString l_extra_servers = m_extraserverlistBox->GetStrings();
	g_configuration->eduke32_userpath_use = m_eduke32userpathcheckBox->GetValue();
	g_configuration->eduke32_userpath = m_eduke32userpathtextCtrl->GetValue();
	g_configuration->duke3dw_userpath_use = m_duke3dwuserpathcheckBox->GetValue();
	g_configuration->duke3dw_userpath = m_duke3dwuserpathtextCtrl->GetValue();
	g_configuration->nduke_userpath_use = m_ndukeuserpathcheckBox->GetValue();
	g_configuration->nduke_userpath = m_ndukeuserpathtextCtrl->GetValue();
	g_configuration->hduke_userpath_use = m_hdukeuserpathcheckBox->GetValue();
	g_configuration->hduke_userpath = m_hdukeuserpathtextCtrl->GetValue();
	g_configuration->voidsw_userpath_use = m_voidswuserpathcheckBox->GetValue();
	g_configuration->voidsw_userpath = m_voidswuserpathtextCtrl->GetValue();
	g_configuration->banlist_filepath = m_banlisttextCtrl->GetValue();
	g_configuration->override_browser = m_overridebrowsercheckBox->GetValue();
	g_configuration->browser_exec = m_browsertextCtrl->GetValue();
	g_configuration->override_soundcmd = m_overridesoundcheckBox->GetValue();
	g_configuration->playsound_cmd = m_soundcmdtextCtrl->GetValue();
	g_configuration->enable_localip_optimization = m_localipoptimizecheckBox->GetValue();
#ifndef __WXMSW__
	g_configuration->terminal_fullcmd = m_terminaltextCtrl->GetValue();
#endif
	g_configuration->extraservers_addrs = m_extra_addrs;
	g_configuration->extraservers_portnums = m_extra_portnums;
	g_configuration->Save();
}
