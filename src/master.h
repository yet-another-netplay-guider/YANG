/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANGMASTER_H_
#define _YANGMASTER_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
//#include <wx/app.h>
#include <wx/socket.h>
#include <wx/stopwatch.h>
#endif

#define MASTER_DEFAULT_PORT_NUM 8500
#define MASTER_DEFAULT_MAX_CLIENTS 64
#define MASTER_DEFAULT_MAX_CONNECTIONS_PER_IP 8
#define MASTER_DEFAULT_MAX_ROOMS 16
#define MASTER_DEFAULT_MAX_ROOMS_PER_IP 2
#define MASTER_DEFAULT_BANLIST_REFRESH_TIME 0

class BanListTimerThread : public wxThread
{
public:
	BanListTimerThread(long refresh_time);
	virtual void *Entry();
private:
	wxStopWatch m_stopwatch;
	long m_refresh_time;
};

class ClientThread : public wxThread
{
public:
	ClientThread(wxSocketBase* sock, const wxString& address);
	virtual void *Entry();
private:
	wxSocketBase *m_sock;
	wxString m_addr;
};

class ServerThread : public wxThread
{
public:
	ServerThread(wxSocketServer* server);
	virtual void *Entry();
private:
	wxSocketServer *m_server;
};
/*
class YANGMASTERApp : public wxApp
{
public:
// Called on application startup
virtual bool OnInit();
};
*/
#endif
