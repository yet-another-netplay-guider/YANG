Preface
-------

To begin with, as described in README.md, expect no technical support
for getting YANG or its master server working. You're under your own.

Additionally, there are multiple technical issues and/or limitations.
See "Known issues" under README.md for details.

The wxWidgets library
---------------------

wxWidgets is a cross-platform GUI library. There are ports of it
to different platforms and environments, like wxMSW for Windows.

Generally speaking, you'll want to use wxWidgets 3.0.2, and no
later version. wxMSW 3.0.5 might work, but 3.1.3 and 3.2.0 couldn't
properly connect to a master server or two. If you make a wxGTK build
or another non-Windows build, better use 3.0.2 and no newer release,
for an reason given under README.md's "Known issues".

Can Visual Studio be used?
--------------------------

The included msvc dir might let you navigate the source code,
but you probably won't be able to make a Windows binary with it.
You should use MinGW-w64 with the included makefile (GNUmakefile) instead.

Building on Windows (MSYS2 MinGW 32-bit)
----------------------------------------

The following instructions were originally written by Striker, and later got
adapted for use with wxWidgets 3.0.2 instead of 3.1.3. As of writing this,
they have not been tested since Striker stopped working on YANG's codebase.

* Before you start, make sure you have MSYS2 installed.

* Run MSYS2 MinGW 32-bit, and make sure you have the proper toolchains installed. You can do that by calling these commands:

`pacman -Syuu`
`pacman -S --needed --noconfirm mingw-w64-i686-toolchain mingw-w64-x86_64-toolchain make`

* Download wxWidgets 3.0.2: [link](https://github.com/wxWidgets/wxWidgets/releases/download/v3.0.2/wxWidgets-3.0.2.zip)
* Extract the contents of wxWidgets-3.0.2.zip to <repository root>/wxWidgets/wxWidgets-3.0.2

* In the MSYS2 MinGW 32-bit console, navigate to the folder that contains your cloned YANG repo, then type

`./make_all_wxmsw.sh`

This should compile wxWidgets and prepare it for use with YANG.

* After this is done, type:

`make -IGeoIP -LGeoIP/win32 WXCONFIG=wxWidgets/wxMSW-static/bin/wx-config`

YANG and the YANG Master server should start compiling now.

* If you wish to enable debugging symbols, try:

`make -IGeoIP -LGeoIP/win32 WXCONFIG=wxWidgets/wxMSW-static/bin/wx-config DEBUG=1`

* If you want to use a different version of wxWidgets for any reason,
repeat as above with the matching version, and also change `WXVER`
as used in `make_all_wxmsw.h`. 3.0.2 should otherwise still be
considered the version that YANG is most compatible with.

Building on Linux
-----------------

* If possible, use your Linux distribution's package manager in order
to install development files for wxGTK2/wxGTK3 version 3.0.2.
wxWidgets v3.0.0-3.0.1 might or might not be usable.
Also install development files for GeoIP v1.4.6 or
any other compatible version.

* Afterwards, while in the YANG source tree, type:

`make`

* If you wish to enable debugging symbols, try:

`make DEBUG=1`

* If you want to manually build and install wxGTK (2 or 3) version 3.0.2,
follow the aforementioned instructions for building a Windows executable,
while adjusting them for building wxGTK2/3 instead of wxMSW
(and not using MSYS2 or MinGW).

* You can also make use of a local build of the modified GeoIP 1.4.6 sources,
with bits of removed code, as found under the subdir named GeoIP.
GeoIP/lib32 and GeoIP/lib64 should have matching GeoIP builds,
but you might want to make your own one.

Features which are disabled by default
--------------------------------------

There are certain features which were disabled,
albeit they can still be enabled using compile-time macros.
- One is an experimental UPnP toggle, which was found to be unstable,
at least with wxGTK. It can be enabled from the makefile
by setting `ENABLE_UPNP` to 1.
- Other such features are password-protected rooms and the option to set
"Extra args for everyone". These were disabled due to security concerns.
The former was implemented with no encryption, and the latter includes
an exploit. Details of the exploit are given in the README description
for the 2020 release, i.e., v0.95. These features can't be enabled from the
makefile like UPnP, but they're controlled by macros in a similar manner.
Consider stopping to think before enabling any of these features,
especially "Extra args for everyone".
- There are other code blocks and definitions which are disabled by default
in a similar manner, like "Check for updates".

What to do if the reported operating system of a user is wrong
--------------------------------------------------------------

YANG can use either wxWidgets' own wxGetOsDescription function,
or a modification of an older version of wxGetOsDescription. The relevant
code can be found under `src/getosdesc.*`. If wxGetOsDescription itself
is used as-is, the reported OS may depend on the version of wxWidgets
in use. If the alternative implementation is used then it might require
an update. Finally, other factors (e.g., a Windows manifest file)
might also impact the reported OS.

Additional notes
----------------

Generally speaking, you may change the value of variable WXCONFIG within
GNUmakefile as needed. This can be done via an additional argument to GNU make.

Short of `make_all_wxmsw.sh`, the various `make*.sh` scripts
are probably not as useful as they used to be. Generally speaking,
they should have been assumed to not even be used for YANG v0.95
(at least in part), and may fail to work either way.

* `make_mingw.sh` is a helper script, originally created for
making Windows builds of YANG with MinGW.
* `make_all_wxgtk.sh` was used for making multiple builds of wxGTK 2.8.
* `make_all_wx.sh` would call other scripts, in turn making builds
of wxMSW and wxGTK, 2.8 and 2.9 altogether.
* `make_all.sh` was used for making multiple builds of YANG for release.
It would also prepare a source code package.
* `make_geoip_package.sh` was used for creating a package
with the modified GeoIP sources to be distributed separately.
