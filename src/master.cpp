/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/
/*
#ifdef wxUSE_GUI
#undef wxUSE_GUI
#endif
*/
#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
//#include <wx/app.h>
#include <wx/init.h>
#include <wx/string.h>
#include <wx/stdpaths.h>
#include <wx/utils.h>
#if wxCHECK_VERSION(2, 9, 0)
#include <wx/wxcrtvararg.h>
#endif
#endif

#include "master.h"
#include "master_config.h"
#include "master_banlist.h"
#include "master_clientstable.h"
#include "master_roomstable.h"
#include "comm_txt.h"
//#define g_SendCStringToSocket(socket, str) socket->Write(str, strlen(str))
#include "common.h"

#define MASTER_CONFIG_FILENAME wxT("master.cfg")
#define MASTER_BANLIST_FILENAME wxT("banlist.txt")
// NOTE THAT IF THE CHARACTER ':' IS TO BE EMBEDDED IN THE FOLLOWING STRING,
// THEN "\\:" (which is \:) SHOULD BE WRITTEN HERE, AND NOT JUST ":".
// REASON IS THE COLON IS USED AS A SPECIAL CHARACTER.
#define l_Send_Msg_Incompatible_Ver(sock) g_SendCStringToSocket(sock, "2:error:Your version of the launcher is of an incompatible version with this server list.:")

MasterConfig* g_master_configuration;
MasterBanList* g_master_banlist;
MasterClientsTable* g_master_clientstable;
MasterRoomsTable* g_master_roomstable;
wxArrayString *g_banlist_fileslist;
//wxCriticalSection *g_criticalsection;
bool g_server_is_up;
long g_num_of_clients, g_num_of_rooms;

// Give wxWidgets the means to create a YANGMASTERApp object
//IMPLEMENT_APP(YANGMASTERApp)

BanListTimerThread::BanListTimerThread(long refresh_time)
{
	m_refresh_time = 1000*refresh_time;
}

void* BanListTimerThread::Entry()
{
	m_stopwatch.Start();
	while (g_server_is_up)
	{
		wxSleep(1);
		if (m_stopwatch.Time() >= m_refresh_time)
		{
			m_stopwatch.Start();
			g_master_banlist->Load(*g_banlist_fileslist);
		}
	}
	return NULL;
}

// The returned array string has any IP address encrypted,
// ready to be sent for a YANG user (not in a room).
wxArrayString g_GetRoomInfo(RoomInfo& room_info, wxString& peer_ip)
{
	wxArrayString l_str_list;
	l_str_list.Add(wxT("info"));
	// If both are the same, maybe it's a LAN (or mixed Internet-LAN) game
	// advertised. Some network setups (e.g. routers) don't let one connect
	// to a computer on a local network, using the external (Internet) IP.
	if (peer_ip == room_info.address)
		l_str_list.Add(g_EncryptForCommText(room_info.localip, COMM_KEY_IPLIST_FROM_MASTER_TO_MAINWIN));
	else
		l_str_list.Add(g_EncryptForCommText(room_info.address, COMM_KEY_IPLIST_FROM_MASTER_TO_MAINWIN));
	// For detection of country.
	l_str_list.Add(g_EncryptForCommText(room_info.address, COMM_KEY_IPLIST_FROM_MASTER_TO_MAINWIN));

	l_str_list.Add(wxString::Format(wxT("%ld"), (long)room_info.port_num));
	l_str_list.Add(room_info.netver);
	if (room_info.password_protected)
		l_str_list.Add(wxT("yes"));
	else
		l_str_list.Add(wxT("no"));

	switch (room_info.game)
	{
		case GAME_BLOOD:
			l_str_list.Add(GAMECODE_BLOOD);
			l_str_list.Add(room_info.roomname);
			//  l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.num_players));
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.max_num_players));
			if (room_info.in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			switch (room_info.srcport)
			{
				case SOURCEPORT_DOSBLOODSW: l_str_list.Add(SRCPORTCODE_DOSBLOODSW); break;
				case SOURCEPORT_DOSBLOODRG: l_str_list.Add(SRCPORTCODE_DOSBLOODRG); break;
				case SOURCEPORT_DOSBLOODPP: l_str_list.Add(SRCPORTCODE_DOSBLOODPP); break;
				case SOURCEPORT_DOSBLOODOU: l_str_list.Add(SRCPORTCODE_DOSBLOODOU); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.skill));
			switch (room_info.bloodgametype)
			{
				case BLOODMPGAMETYPE_COOP:  l_str_list.Add(wxT("coop")); break;
				case BLOODMPGAMETYPE_BB:    l_str_list.Add(wxT("bb")); break;
				case BLOODMPGAMETYPE_TEAMS: l_str_list.Add(wxT("teams")); break;
				default: ;
			}

			break;

		case GAME_DESCENT:
			l_str_list.Add(GAMECODE_DESCENT);
			l_str_list.Add(room_info.roomname);
			//  l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.num_players));
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.max_num_players));
			if (room_info.in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			switch (room_info.srcport)
			{
				case SOURCEPORT_DOSDESCENT: l_str_list.Add(SRCPORTCODE_DOSDESCENT); break;
				case SOURCEPORT_D1XREBIRTH: l_str_list.Add(SRCPORTCODE_D1XREBIRTH); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.skill));
			switch (room_info.descentgametype)
			{
				case DESCENTMPGAMETYPE_ANARCHY:      l_str_list.Add(wxT("anarchy")); break;
				case DESCENTMPGAMETYPE_TEAM_ANARCHY: l_str_list.Add(wxT("team_anarchy")); break;
				case DESCENTMPGAMETYPE_ROBO_ANARCHY: l_str_list.Add(wxT("robo_anarchy")); break;
				case DESCENTMPGAMETYPE_COOPERATIVE:  l_str_list.Add(wxT("cooperative")); break;
				default: ;
			}

			break;

		case GAME_DESCENT2:
			l_str_list.Add(GAMECODE_DESCENT2);
			l_str_list.Add(room_info.roomname);
			//  l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.num_players));
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.max_num_players));
			if (room_info.in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			switch (room_info.srcport)
			{
				case SOURCEPORT_DOSDESCENT2: l_str_list.Add(SRCPORTCODE_DOSDESCENT2); break;
				case SOURCEPORT_D2XREBIRTH:  l_str_list.Add(SRCPORTCODE_D2XREBIRTH); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.skill));
			switch (room_info.descent2gametype)
			{
				case DESCENT2MPGAMETYPE_ANARCHY:          l_str_list.Add(wxT("anarchy")); break;
				case DESCENT2MPGAMETYPE_TEAM_ANARCHY:     l_str_list.Add(wxT("team_anarchy")); break;
				case DESCENT2MPGAMETYPE_ROBO_ANARCHY:     l_str_list.Add(wxT("robo_anarchy")); break;
				case DESCENT2MPGAMETYPE_COOPERATIVE:      l_str_list.Add(wxT("cooperative")); break;
				case DESCENT2MPGAMETYPE_CAPTURE_THE_FLAG: l_str_list.Add(wxT("capture_the_flag")); break;
				case DESCENT2MPGAMETYPE_HOARD:            l_str_list.Add(wxT("hoard")); break;
				case DESCENT2MPGAMETYPE_TEAM_HOARD:       l_str_list.Add(wxT("team_hoard")); break;
				default: ;
			}

			break;

		case GAME_DN3D:
			l_str_list.Add(GAMECODE_DN3D);
			l_str_list.Add(room_info.roomname);
			//  l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.num_players));
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.max_num_players));
			if (room_info.in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			switch (room_info.srcport)
			{
				case SOURCEPORT_DOSDUKESW: l_str_list.Add(SRCPORTCODE_DOSDUKESW); break;
				case SOURCEPORT_DOSDUKERG: l_str_list.Add(SRCPORTCODE_DOSDUKERG); break;
				case SOURCEPORT_DOSDUKEAE: l_str_list.Add(SRCPORTCODE_DOSDUKEAE); break;
				case SOURCEPORT_DUKE3DW:   l_str_list.Add(SRCPORTCODE_DUKE3DW); break;
				case SOURCEPORT_EDUKE32:   l_str_list.Add(SRCPORTCODE_EDUKE32); break;
				case SOURCEPORT_NDUKE:     l_str_list.Add(SRCPORTCODE_NDUKE); break;
				case SOURCEPORT_HDUKE:     l_str_list.Add(SRCPORTCODE_HDUKE); break;
				case SOURCEPORT_XDUKE:     l_str_list.Add(SRCPORTCODE_XDUKE); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.skill));
			switch (room_info.dn3dgametype)
			{
				case DN3DMPGAMETYPE_DMSPAWN:    l_str_list.Add(wxT("dmspawn")); break;
				case DN3DMPGAMETYPE_COOP:       l_str_list.Add(wxT("coop")); break;
				case DN3DMPGAMETYPE_DMNOSPAWN:  l_str_list.Add(wxT("dmnospawn")); break;
				case DN3DMPGAMETYPE_TDMSPAWN:   l_str_list.Add(wxT("tdmspawn")); break;
				case DN3DMPGAMETYPE_TDMNOSPAWN: l_str_list.Add(wxT("tdmnospawn")); break;
				default: ;
			}
			//  if (((room_info.srcport == SOURCEPORT_EDUKE32) || (room_info.srcport == SOURCEPORT_DUKE3DW))
			//       && room_info.usemasterslave)
			//  if (room_info.usemasterslave)
			//    l_str_list.Add(wxT("masterslave"));

			break;

		case GAME_SW:
			l_str_list.Add(GAMECODE_SW);
			l_str_list.Add(room_info.roomname);
			//  l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.num_players));
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.max_num_players));
			if (room_info.in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			switch (room_info.srcport)
			{
				case SOURCEPORT_DOSSWSW: l_str_list.Add(SRCPORTCODE_DOSSWSW); break;
				case SOURCEPORT_DOSSWRG: l_str_list.Add(SRCPORTCODE_DOSSWRG); break;
				case SOURCEPORT_VOIDSW:  l_str_list.Add(SRCPORTCODE_VOIDSW); break;
				case SOURCEPORT_SWP:     l_str_list.Add(SRCPORTCODE_SWP); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.skill));
			switch (room_info.swgametype)
			{
				case SWMPGAMETYPE_WBSPAWN:   l_str_list.Add(wxT("wbspawn")); break;
				case SWMPGAMETYPE_WBNOSPAWN: l_str_list.Add(wxT("wbnospawn")); break;
				case SWMPGAMETYPE_COOP:      l_str_list.Add(wxT("coop")); break;
				default: ;
			}

			break;

		case GAME_CUSTOM:
			l_str_list.Add(GAMECODE_CUSTOM);
			l_str_list.Add(room_info.roomname);
			//  l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.num_players));
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.max_num_players));
			if (room_info.in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			l_str_list.Add(SRCPORTCODE_CUSTOM);
			l_str_list.Add(wxEmptyString);
			l_str_list.Add(wxEmptyString);

			break;
		default: ;
	}

	if (room_info.launch_usermap)
	{
		l_str_list.Add(wxT("usermap"));
		l_str_list.Add(room_info.usermap);
	}
	else
	{
		l_str_list.Add(wxT("epimap"));
		l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)room_info.epimap_num));
	}

	l_str_list.Add(room_info.modname);

	if (room_info.use_crypticpassage)
		l_str_list.Add(wxT("yes"));
	else
		l_str_list.Add(wxT("no"));

	return l_str_list;
}

#define isSocketConnected(sock) (sock->IsConnected())

void g_SendRoomsList(wxSocketBase* sock, GameType game, SourcePortType srcport)
{
	int l_loop_var, l_loop_var2, l_num_of_player_data_entries, l_buff_length;
	MasterRoom l_room;
	char l_buffer[MAX_COMM_TEXT_LENGTH], *l_buff_ptr;
	wxArrayString l_str_list;
	wxIPV4address l_addr;
	wxString l_str_addr;
	sock->GetPeer(l_addr);
	l_str_addr = l_addr.IPAddress();
	l_str_list.Add(wxT("countbyport"));
	for (l_loop_var = 0; l_loop_var < SOURCEPORT_NUMOFPORTS; l_loop_var++)
		l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)g_master_roomstable->GetCountByPort((SourcePortType)l_loop_var)));
	l_buff_length = g_ConvertStringListToCommText(l_str_list, l_buffer);
	l_buff_ptr = l_buffer;
	while ((g_server_is_up) && isSocketConnected(sock) && (l_buff_length > 0))
	{
		sock->Write(l_buff_ptr, l_buff_length);
		l_buff_length -= sock->LastCount();
		l_buff_ptr += sock->LastCount();
	}
	l_loop_var = 0;
	while ((g_server_is_up) && isSocketConnected(sock) && (g_master_roomstable->TryGet(l_loop_var, l_room)))
	{
		if (((game == GAME_NUMOFGAMES) || (l_room.info.game == game)) &&
			((srcport == SOURCEPORT_NUMOFPORTS) || (l_room.info.srcport == srcport)))
		{
			l_str_list = g_GetRoomInfo(l_room.info, l_str_addr);
			l_buff_length = g_ConvertStringListToCommText(l_str_list, l_buffer);
			l_buff_ptr = l_buffer;
			while ((g_server_is_up) && isSocketConnected(sock) && (l_buff_length > 0))
			{
				sock->Write(l_buff_ptr, l_buff_length);
				l_buff_length -= sock->LastCount();
				l_buff_ptr += sock->LastCount();
			}
			if ((g_server_is_up) && isSocketConnected(sock))
			{
				l_loop_var2 = 0;
				l_num_of_player_data_entries = l_room.players.GetCount();
				while ((g_server_is_up) && isSocketConnected(sock) && (l_loop_var2 < l_num_of_player_data_entries))
				{
					l_str_list.Empty();
					l_str_list.Add(wxT("player"));
					l_str_list.Add(l_room.players[l_loop_var2]); // Nickname
					l_loop_var2++;
					if (l_loop_var2 < l_num_of_player_data_entries)
					{
						l_str_list.Add(g_EncryptForCommText(l_room.players[l_loop_var2], COMM_KEY_IPLIST_FROM_MASTER_TO_MAINWIN)); // IP Address
						l_loop_var2++;
						if (l_loop_var2 < l_num_of_player_data_entries)
						{
							l_str_list.Add(l_room.players[l_loop_var2]); // Operating System
							l_loop_var2++;
							l_buff_length = g_ConvertStringListToCommText(l_str_list, l_buffer);
							l_buff_ptr = l_buffer;
							while ((g_server_is_up) && isSocketConnected(sock) && (l_buff_length > 0))
							{
								sock->Write(l_buff_ptr, l_buff_length);
								l_buff_length -= sock->LastCount();
								l_buff_ptr += sock->LastCount();
							}
						}
					}
				}
			}
		}
		l_loop_var++;
	}
}

// str_list is obtained as-is from a connected peer (host of a room),
// with the IPs being encrypted.
void g_SetRoomInfo(wxSocketBase* sock, wxArrayString str_list,
				   MasterRoom& room, bool is_new_room)
{
	wxIPV4address l_addr;
	sock->GetPeer(l_addr);
	wxString l_temp_str;
	size_t l_index;

	if (is_new_room)
	{
		room.info.sock = sock;
		room.info.address = l_addr.IPAddress();
		room.info.netver = str_list[2];
		room.info.localip = g_DecryptFromCommText(str_list[3], COMM_KEY_IPLIST_FROM_HOST_TO_MASTER);
		str_list[4].ToLong(&room.info.port_num);
		l_index = 5;
	}
	else
		l_index = 1;

	room.info.password_protected = (str_list[l_index] == wxT("yes"));

	if (str_list[l_index+1] == GAMECODE_BLOOD)
	{
		room.info.game = GAME_BLOOD;

		l_temp_str = str_list[l_index+5];
		if (l_temp_str == SRCPORTCODE_DOSBLOODSW)
			room.info.srcport = SOURCEPORT_DOSBLOODSW;
		else if (l_temp_str == SRCPORTCODE_DOSBLOODRG)
			room.info.srcport = SOURCEPORT_DOSBLOODRG;
		else if (l_temp_str == SRCPORTCODE_DOSBLOODPP)
			room.info.srcport = SOURCEPORT_DOSBLOODPP;
		else
			room.info.srcport = SOURCEPORT_DOSBLOODOU;

		l_temp_str = str_list[l_index+7];
		if (l_temp_str == wxT("coop"))
			room.info.bloodgametype = BLOODMPGAMETYPE_COOP;
		else if (l_temp_str == wxT("bb"))
			room.info.bloodgametype = BLOODMPGAMETYPE_BB;
		else
			room.info.bloodgametype = BLOODMPGAMETYPE_TEAMS;
	}
	else if (str_list[l_index+1] == GAMECODE_DESCENT)
	{
		room.info.game = GAME_DESCENT;

		l_temp_str = str_list[l_index+5];
		if (l_temp_str == SRCPORTCODE_DOSDESCENT)
			room.info.srcport = SOURCEPORT_DOSDESCENT;
		else
			room.info.srcport = SOURCEPORT_D1XREBIRTH;

		l_temp_str = str_list[l_index+7];
		if (l_temp_str == wxT("anarchy"))
			room.info.descentgametype = DESCENTMPGAMETYPE_ANARCHY;
		else if (l_temp_str == wxT("team_anarchy"))
			room.info.descentgametype = DESCENTMPGAMETYPE_TEAM_ANARCHY;
		else if (l_temp_str == wxT("robo_anarchy"))
			room.info.descentgametype = DESCENTMPGAMETYPE_ROBO_ANARCHY;
		else
			room.info.descentgametype = DESCENTMPGAMETYPE_COOPERATIVE;
	}
	else if (str_list[l_index+1] == GAMECODE_DESCENT2)
	{
		room.info.game = GAME_DESCENT2;

		l_temp_str = str_list[l_index+5];
		if (l_temp_str == SRCPORTCODE_DOSDESCENT2)
			room.info.srcport = SOURCEPORT_DOSDESCENT2;
		else
			room.info.srcport = SOURCEPORT_D2XREBIRTH;

		l_temp_str = str_list[l_index+7];
		if (l_temp_str == wxT("anarchy"))
			room.info.descent2gametype = DESCENT2MPGAMETYPE_ANARCHY;
		else if (l_temp_str == wxT("team_anarchy"))
			room.info.descent2gametype = DESCENT2MPGAMETYPE_TEAM_ANARCHY;
		else if (l_temp_str == wxT("robo_anarchy"))
			room.info.descent2gametype = DESCENT2MPGAMETYPE_ROBO_ANARCHY;
		else if (l_temp_str == wxT("cooperative"))
			room.info.descent2gametype = DESCENT2MPGAMETYPE_COOPERATIVE;
		else if (l_temp_str == wxT("capture_the_flag"))
			room.info.descent2gametype = DESCENT2MPGAMETYPE_CAPTURE_THE_FLAG;
		else if (l_temp_str == wxT("hoard"))
			room.info.descent2gametype = DESCENT2MPGAMETYPE_HOARD;
		else
			room.info.descent2gametype = DESCENT2MPGAMETYPE_TEAM_HOARD;
	}
	else if (str_list[l_index+1] == GAMECODE_DN3D)
	{
		room.info.game = GAME_DN3D;

		l_temp_str = str_list[l_index+5];
		if (l_temp_str == SRCPORTCODE_DOSDUKESW)
			room.info.srcport = SOURCEPORT_DOSDUKESW;
		else if (l_temp_str == SRCPORTCODE_DOSDUKERG)
			room.info.srcport = SOURCEPORT_DOSDUKERG;
		else if (l_temp_str == SRCPORTCODE_DOSDUKEAE)
			room.info.srcport = SOURCEPORT_DOSDUKEAE;
		else if (l_temp_str == SRCPORTCODE_DUKE3DW)
			room.info.srcport = SOURCEPORT_DUKE3DW;
		else if (l_temp_str == SRCPORTCODE_EDUKE32)
			room.info.srcport = SOURCEPORT_EDUKE32;
		else if (l_temp_str == SRCPORTCODE_NDUKE)
			room.info.srcport = SOURCEPORT_NDUKE;
		else if (l_temp_str == SRCPORTCODE_HDUKE)
			room.info.srcport = SOURCEPORT_HDUKE;
		else
			room.info.srcport = SOURCEPORT_XDUKE;

		l_temp_str = str_list[l_index+7];
		if (l_temp_str == wxT("dmspawn"))
			room.info.dn3dgametype = DN3DMPGAMETYPE_DMSPAWN;
		else if (l_temp_str == wxT("coop"))
			room.info.dn3dgametype = DN3DMPGAMETYPE_COOP;
		else if (l_temp_str == wxT("dmnospawn"))
			room.info.dn3dgametype = DN3DMPGAMETYPE_DMNOSPAWN;
		else if (l_temp_str == wxT("tdmspawn"))
			room.info.dn3dgametype = DN3DMPGAMETYPE_TDMSPAWN;
		else
			room.info.dn3dgametype = DN3DMPGAMETYPE_TDMNOSPAWN;

		//  room.info.usemasterslave =
		//    (((room.info.srcport == SOURCEPORT_EDUKE32) || (room.info.srcport == SOURCEPORT_DUKE3DW))
		//     && (str_list.GetCount() == (l_index+12)) && (str_list[l_index+11] == wxT("masterslave")));
	}
	else if (str_list[l_index+1] == GAMECODE_SW)
	{
		room.info.game = GAME_SW;

		l_temp_str = str_list[l_index+5];
		if (l_temp_str == SRCPORTCODE_DOSSWSW)
			room.info.srcport = SOURCEPORT_DOSSWSW;
		else if (l_temp_str == SRCPORTCODE_DOSSWRG)
			room.info.srcport = SOURCEPORT_DOSSWRG;
		else if (l_temp_str == SRCPORTCODE_VOIDSW)
			room.info.srcport = SOURCEPORT_VOIDSW;
		else
			room.info.srcport = SOURCEPORT_SWP;

		l_temp_str = str_list[l_index+7];
		if (l_temp_str == wxT("wbspawn"))
			room.info.swgametype = SWMPGAMETYPE_WBSPAWN;
		else if (l_temp_str == wxT("wbnospawn"))
			room.info.swgametype = SWMPGAMETYPE_WBNOSPAWN;
		else
			room.info.swgametype = SWMPGAMETYPE_COOP;
	}
	else if (str_list[l_index+1] == GAMECODE_CUSTOM)
	{
		room.info.game = GAME_CUSTOM;

		room.info.srcport = SOURCEPORT_CUSTOM;
	}

	room.info.launch_usermap = (str_list[l_index+8] == wxT("usermap"));
	if (room.info.launch_usermap)
		room.info.usermap = str_list[l_index+9];
	else
		str_list[l_index+9].ToULong(&room.info.epimap_num);

	room.info.modname = str_list[l_index+10];

	room.info.roomname = str_list[l_index+2];
	str_list[l_index+3].ToULong(&room.info.max_num_players);
	if (room.info.max_num_players > MAX_NUM_PLAYERS)
		room.info.max_num_players = MAX_NUM_PLAYERS;
	room.info.in_game = (str_list[l_index+4] == wxT("yes"));
	str_list[l_index+6].ToULong(&room.info.skill);

	room.info.use_crypticpassage = (str_list[l_index+11] == wxT("yes"));
}

ClientThread::ClientThread(wxSocketBase* sock, const wxString& address)// : wxThread(wxTHREAD_JOINABLE)
{
	m_sock = sock;
	m_addr = address;
}

void* ClientThread::Entry()
{
	bool l_is_room_advertised = false, l_continue_loops = true;
	char l_short_buffer[MAX_COMM_TEXT_LENGTH];
	size_t /*l_num_of_read_attempts, */l_input_data_size, l_num_of_items;
	long l_temp_num;
	size_t l_loop_var;
	MasterRoom l_room;
	wxArrayString l_str_list;
	wxString l_long_buffer;
	wxSocketClient* l_client_sock;
	wxIPV4address l_addr;
	wxStopWatch l_stopwatch;
	l_stopwatch.Start(0);
	while (g_server_is_up && l_continue_loops && isSocketConnected(m_sock)
		&& (l_stopwatch.Time() < 240000) && (l_long_buffer.Len() <= 4*MAX_COMM_TEXT_LENGTH))
	{
		m_sock->Read(l_short_buffer, MAX_COMM_TEXT_LENGTH);
		l_input_data_size = m_sock->LastCount();
		if (l_input_data_size>0)
		{
			/*    l_num_of_read_attempts = 0;
			do
			{*/
			/*      if (m_sock->Error())
			{
			AddMessage(wxString::Format(wxT("* A communication error has occured while trying to fetch data: %d"), (int)l_sock->LastError()));
			if (m_allow_sounds && g_configuration->play_snd_error)
			g_PlaySound(g_configuration->snd_error_file);
			break;
			}*/
			/*      if (l_input_data_size < MAX_COMM_TEXT_LENGTH)
			l_num_of_read_attempts = MAX_COMM_READ_ATTEMPTS;
			else
			l_num_of_read_attempts++;*/
			l_long_buffer << wxString(l_short_buffer, wxConvUTF8, l_input_data_size);
			l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
			while (g_server_is_up && l_continue_loops && l_num_of_items)
			{
				if (l_num_of_items == 1)
				{
					// NOTE: Currently unused; Might
					// help while debugging, though?
					if (l_str_list[0] == wxT("getip"))
					{
						l_str_list.Empty();
						l_str_list.Add(wxT("ipaddress"));
						l_str_list.Add(g_EncryptForCommText(m_addr, COMM_KEY_IPQUERY_FROM_MASTER));
						g_SendStringListToSocket(m_sock, l_str_list);
						l_continue_loops = false;
						m_sock->Close();
					}
					else if (l_str_list[0] == wxT("disconnect"))
					{
						l_continue_loops = false;
						m_sock->Close();
					}
					// BACKWARD COMPATIBILITY: Notify users of outdated clients.
					else if (l_str_list[0] == wxT("getroomlist"))
					{
						l_Send_Msg_Incompatible_Ver(m_sock);
						l_continue_loops = false;
						m_sock->Close();
					}
					/*
					else if ((l_is_room_advertised) && (l_str_list[0] == wxT("stopadvertise")))
					{
					g_master_roomstable->Delete(m_sock);
					l_is_room_advertised = false;
					}*/
				}
				else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("getroomlist")))
				{
					if (l_str_list[1] != YANGMASTER_STR_NET_VERSION)
						l_Send_Msg_Incompatible_Ver(m_sock);
					else
						g_SendRoomsList(m_sock, GAME_NUMOFGAMES, SOURCEPORT_NUMOFPORTS);
					l_continue_loops = false;
					m_sock->Close();
				}
				else if ((l_num_of_items == 3) && (l_str_list[0] == wxT("getroomlist")))
				{
					if (l_str_list[1] != YANGMASTER_STR_NET_VERSION)
						l_Send_Msg_Incompatible_Ver(m_sock);
					else if (l_str_list[2] == GAMECODE_BLOOD)
						g_SendRoomsList(m_sock, GAME_BLOOD, SOURCEPORT_NUMOFPORTS);
					else if (l_str_list[2] == GAMECODE_DESCENT)
						g_SendRoomsList(m_sock, GAME_DESCENT, SOURCEPORT_NUMOFPORTS);
					else if (l_str_list[2] == GAMECODE_DESCENT2)
						g_SendRoomsList(m_sock, GAME_DESCENT2, SOURCEPORT_NUMOFPORTS);
					else if (l_str_list[2] == GAMECODE_DN3D)
						g_SendRoomsList(m_sock, GAME_DN3D, SOURCEPORT_NUMOFPORTS);
					else if (l_str_list[2] == GAMECODE_SW)
						g_SendRoomsList(m_sock, GAME_SW, SOURCEPORT_NUMOFPORTS);
					else if (l_str_list[2] == GAMECODE_CUSTOM)
						g_SendRoomsList(m_sock, GAME_CUSTOM, SOURCEPORT_NUMOFPORTS);
					else if (l_str_list[2] == SRCPORTCODE_DOSBLOODSW)
						g_SendRoomsList(m_sock, GAME_BLOOD, SOURCEPORT_DOSBLOODSW);
					else if (l_str_list[2] == SRCPORTCODE_DOSBLOODRG)
						g_SendRoomsList(m_sock, GAME_BLOOD, SOURCEPORT_DOSBLOODRG);
					else if (l_str_list[2] == SRCPORTCODE_DOSBLOODPP)
						g_SendRoomsList(m_sock, GAME_BLOOD, SOURCEPORT_DOSBLOODPP);
					else if (l_str_list[2] == SRCPORTCODE_DOSBLOODOU)
						g_SendRoomsList(m_sock, GAME_BLOOD, SOURCEPORT_DOSBLOODOU);
					else if (l_str_list[2] == SRCPORTCODE_DOSDESCENT)
						g_SendRoomsList(m_sock, GAME_DESCENT, SOURCEPORT_DOSDESCENT);
					else if (l_str_list[2] == SRCPORTCODE_D1XREBIRTH)
						g_SendRoomsList(m_sock, GAME_DESCENT, SOURCEPORT_D1XREBIRTH);
					else if (l_str_list[2] == SRCPORTCODE_DOSDESCENT2)
						g_SendRoomsList(m_sock, GAME_DESCENT2, SOURCEPORT_DOSDESCENT2);
					else if (l_str_list[2] == SRCPORTCODE_D2XREBIRTH)
						g_SendRoomsList(m_sock, GAME_DESCENT2, SOURCEPORT_D2XREBIRTH);
					else if (l_str_list[2] == SRCPORTCODE_DOSDUKESW)
						g_SendRoomsList(m_sock, GAME_DN3D, SOURCEPORT_DOSDUKESW);
					else if (l_str_list[2] == SRCPORTCODE_DOSDUKERG)
						g_SendRoomsList(m_sock, GAME_DN3D, SOURCEPORT_DOSDUKERG);
					else if (l_str_list[2] == SRCPORTCODE_DOSDUKEAE)
						g_SendRoomsList(m_sock, GAME_DN3D, SOURCEPORT_DOSDUKEAE);
					else if (l_str_list[2] == SRCPORTCODE_DUKE3DW)
						g_SendRoomsList(m_sock, GAME_DN3D, SOURCEPORT_DUKE3DW);
					else if (l_str_list[2] == SRCPORTCODE_EDUKE32)
						g_SendRoomsList(m_sock, GAME_DN3D, SOURCEPORT_EDUKE32);
					else if (l_str_list[2] == SRCPORTCODE_NDUKE)
						g_SendRoomsList(m_sock, GAME_DN3D, SOURCEPORT_NDUKE);
					else if (l_str_list[2] == SRCPORTCODE_HDUKE)
						g_SendRoomsList(m_sock, GAME_DN3D, SOURCEPORT_HDUKE);
					else if (l_str_list[2] == SRCPORTCODE_XDUKE)
						g_SendRoomsList(m_sock, GAME_DN3D, SOURCEPORT_XDUKE);
					else if (l_str_list[2] == SRCPORTCODE_DOSSWSW)
						g_SendRoomsList(m_sock, GAME_SW, SOURCEPORT_DOSSWSW);
					else if (l_str_list[2] == SRCPORTCODE_DOSSWRG)
						g_SendRoomsList(m_sock, GAME_SW, SOURCEPORT_DOSSWRG);
					else if (l_str_list[2] == SRCPORTCODE_VOIDSW)
						g_SendRoomsList(m_sock, GAME_SW, SOURCEPORT_VOIDSW);
					else if (l_str_list[2] == SRCPORTCODE_SWP)
						g_SendRoomsList(m_sock, GAME_SW, SOURCEPORT_SWP);
					l_continue_loops = false;
					m_sock->Close();
				}
				else if ((l_num_of_items == 13) &&
					(l_str_list[0] == wxT("updateinfo")) && (l_is_room_advertised))
				{
					g_SetRoomInfo(m_sock, l_str_list, l_room, false);
					g_master_roomstable->UpdateInfo(l_room.info);
				}
				else if ((l_num_of_items >= 17) &&
					(l_str_list[0] == wxT("advertise")) && (!l_is_room_advertised))
				{
					if (g_master_roomstable->IsFull())
						g_SendCStringToSocket(m_sock, "2:error:Server list is currently full.:");
					else if (l_str_list[1] != YANGMASTER_STR_NET_VERSION)
						l_Send_Msg_Incompatible_Ver(m_sock);
					else if (!l_str_list[4].ToLong(&l_temp_num))
						g_SendCStringToSocket(m_sock, "2:error:An invalid port number has been sent.:");
					else
					{
						m_sock->GetPeer(l_addr);
						l_addr.Service(l_temp_num);
						l_client_sock = new wxSocketClient();
						l_client_sock->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
						l_client_sock->SetTimeout(20);
						l_client_sock->Notify(false);
						if (l_client_sock->Connect(l_addr, true))
						{
							l_client_sock->Destroy();
							if ((g_server_is_up) && isSocketConnected(m_sock))
							{
								g_SetRoomInfo(m_sock, l_str_list, l_room, true);
								//                      l_num_of_read_attempts = 0;
								l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
								l_stopwatch.Start(0);
								while (g_server_is_up && isSocketConnected(m_sock)
									&& (!l_num_of_items) && (l_stopwatch.Time() < 30000))
								{
									m_sock->Read(l_short_buffer, MAX_COMM_TEXT_LENGTH);
									l_input_data_size = m_sock->LastCount();
									/*                        if (m_sock->Error())
									{
									AddMessage(wxString::Format(wxT("* A communication error has occured while trying to fetch data: %d"), (int)l_sock->LastError()));
									if (m_allow_sounds && g_configuration->play_snd_error)
									g_PlaySound(g_configuration->snd_error_file);
									break;
									}*/
									/*                        if (l_input_data_size < MAX_COMM_TEXT_LENGTH)
									l_num_of_read_attempts = MAX_COMM_READ_ATTEMPTS;
									else
									l_num_of_read_attempts++;*/
									if (l_input_data_size > 0)
									{
										l_long_buffer << wxString(l_short_buffer, wxConvUTF8, l_input_data_size);
										l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
									}
								}
								if ((g_server_is_up) && isSocketConnected(m_sock) && (l_str_list[0] == wxT("players")) && (l_str_list.GetCount() % 3 == 1))
								{
									l_str_list.RemoveAt(0);
									// For each player we have: Nickname, IP Address, Operating System

									// Decrypt IP addresses
									for (l_loop_var = 1; l_loop_var < l_str_list.GetCount(); l_loop_var += 3)
										l_str_list[l_loop_var] = g_DecryptFromCommText(l_str_list[l_loop_var], COMM_KEY_IPLIST_FROM_HOST_TO_MASTER);
									l_room.players = l_str_list;

									// TODO: This is repeated later (in another check for "players").
									// Maybe someone is globally banned, so check this.
									l_str_list.Empty();
									l_str_list.Add(wxT("bannedaddrs"));
									l_loop_var = 3;
									while (l_loop_var < l_room.players.GetCount())
										if (g_master_banlist->IsBanned(l_room.players[l_loop_var+1]))
										{
											l_str_list.Add(g_EncryptForCommText(l_room.players[l_loop_var+1], COMM_KEY_IPLIST_FROM_MASTER_TO_HOST));
											l_room.players.RemoveAt(l_loop_var);
											l_room.players.RemoveAt(l_loop_var);
											l_room.players.RemoveAt(l_loop_var);
										}
										else
											l_loop_var += 3;
									if (l_str_list.GetCount() > 1)
										g_SendStringListToSocket(m_sock, l_str_list);

									l_is_room_advertised = g_master_roomstable->TryAdd(l_room);
									if (l_is_room_advertised)
									{
										l_str_list.Empty();
										l_str_list.Add(wxT("advertised"));
										l_str_list.Add(g_EncryptForCommText(l_room.info.address, COMM_KEY_IPLIST_FROM_MASTER_TO_HOST));
										//                          l_str_list.Add(g_EncryptForCommText(l_addr.IPAddress(), COMM_KEY_IPLIST_FROM_MASTER_TO_HOST));
										if (g_master_configuration->send_msg_on_advertise)
											l_str_list.Add(g_master_configuration->message);
										g_SendStringListToSocket(m_sock, l_str_list);
									}
									else
										g_SendCStringToSocket(m_sock, "2:error:Server list is currently full.:");
								}
								else
								{
									l_continue_loops = false;
									m_sock->Close();
								}
							}
						}
						else
						{
							l_client_sock->Destroy();
							l_str_list.Empty();
							l_str_list.Add(wxT("error"));
							l_str_list.Add(wxString::Format(wxT("A test of connecting to your room has failed. It may just be a slow connection.\nIt's also possible that TCP port number %ld is not forwarded in your network."), (long)l_temp_num));
							g_SendStringListToSocket(m_sock, l_str_list);
						}
					}
				}
				else if ((l_num_of_items >= 2) && l_is_room_advertised && (l_str_list[0] == wxT("players")) && (l_str_list.GetCount() % 3 == 1))
				{
					l_str_list.RemoveAt(0);
					// For each player we have: Nickname, IP Address, Operating System

					// Decrypt IP addresses
					for (l_loop_var = 1; l_loop_var < l_str_list.GetCount(); l_loop_var += 3)
						l_str_list[l_loop_var] = g_DecryptFromCommText(l_str_list[l_loop_var], COMM_KEY_IPLIST_FROM_HOST_TO_MASTER);

					l_room.players = l_str_list;

					// TODO: This is repeated earlier (in another check for "players").
					// Maybe someone is globally banned, so check this.
					// TODO: Actually, here, we may not need to do the check,
					// if minor(?) modifications are applied to the host_frame code.
					l_str_list.Empty();
					l_str_list.Add(wxT("bannedaddrs"));
					l_loop_var = 3;
					while (l_loop_var < l_room.players.GetCount())
						if (g_master_banlist->IsBanned(l_room.players[l_loop_var+1]))
						{
							l_str_list.Add(g_EncryptForCommText(l_room.players[l_loop_var+1], COMM_KEY_IPLIST_FROM_MASTER_TO_HOST));
							l_room.players.RemoveAt(l_loop_var);
							l_room.players.RemoveAt(l_loop_var);
							l_room.players.RemoveAt(l_loop_var);
						}
						else
							l_loop_var += 3;
					if (l_str_list.GetCount() > 1)
						g_SendStringListToSocket(m_sock, l_str_list);

					g_master_roomstable->UpdatePlayers(m_sock, l_room.players);
				}
				else if ((l_num_of_items == 2) && l_is_room_advertised && (l_str_list[0] == wxT("globanquery"))) // Someone has joined and might be globally banned, so lets check this
				{
					l_str_list[1] = g_DecryptFromCommText(l_str_list[1], COMM_KEY_IPLIST_FROM_HOST_TO_MASTER);
					if (g_master_banlist->IsBanned(l_str_list[1]))
						l_str_list[0] = wxT("bannedaddrs");
					else
						l_str_list[0] = wxT("unbannedaddrs");
					l_str_list[1] = g_EncryptForCommText(l_str_list[1], COMM_KEY_IPLIST_FROM_MASTER_TO_HOST);
					g_SendStringListToSocket(m_sock, l_str_list);
				}
				l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
			}
			/*      m_sock->Read(l_short_buffer, MAX_COMM_TEXT_LENGTH);
			l_input_data_size = m_sock->LastCount();
			}
			while ((g_server_is_up) && l_continue_loops && (l_num_of_read_attempts < MAX_COMM_READ_ATTEMPTS));*/
			l_stopwatch.Start(0);
		}
		wxSleep(1);
	}
	if (g_server_is_up) // If not, everything will be destroyed anyway.
	{
		if (l_is_room_advertised)
			g_master_roomstable->Delete(m_sock);
		g_master_clientstable->Delete(m_sock);
	}
	m_sock->Destroy();
	return NULL;
}

ServerThread::ServerThread(wxSocketServer* server) : wxThread(wxTHREAD_JOINABLE)
{
	m_server = server;
}

void* ServerThread::Entry()
{
	wxSocketBase* l_sock;
	wxIPV4address l_addr;
	wxString l_addr_string;
	ClientThread* l_thread;
	while (g_server_is_up)
	{
		l_sock = m_server->Accept(false);
		if (l_sock != NULL)
		{
			l_sock->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
			l_sock->SetTimeout(2);
			l_sock->GetPeer(l_addr);
			l_addr_string = l_addr.IPAddress();
			if (g_master_banlist->IsBanned(l_addr_string))
			{
				g_SendCStringToSocket(l_sock, "2:error:Your address is banned from this server list. It's possible you've cheated in a game.:");
				delete l_sock;
			}
			else
			{
				if ((g_master_configuration->max_connections_per_ip) &&
					(g_master_clientstable->Count(l_addr_string) >= g_master_configuration->max_connections_per_ip))
				{
					g_SendCStringToSocket(l_sock, "2:error:Too many connections from your address.:");
					delete l_sock;
				}
				else if (g_master_clientstable->TryAdd(l_sock))
				{
					l_thread = new ClientThread(l_sock, l_addr_string);
					if (l_thread->Create() == wxTHREAD_NO_ERROR)
						l_thread->Run();
					else
					{
						wxFprintf(stderr, wxT("FATAL ERROR: Couldn't create thread for a new client."));
						l_thread->Delete();
						delete l_sock;
					}
				}
				else
				{
					g_SendCStringToSocket(l_sock, "2:error:The server list has exceeded its capacity. Please try again later.:");
					delete l_sock;
				}
			}
		}
		else
			wxSleep(1);
	}
	//while (g_num_of_clients) {};
	delete m_server;
	return NULL;
}

//bool YANGMASTERApp::OnInit()
int main()

{
	ServerThread *l_server_thread;
	BanListTimerThread *l_banlist_timer_thread;
	char l_inputkey;
	wxString *l_userdatapath;
	wxIPV4address *l_addr;
	// Used for getting user-profile dir.
	//SetAppName(wxT("yang"));
	wxInitializer l_initializer;
	if ( !l_initializer )
	{
		fprintf(stderr, "ERROR: Couldn't initialize wxWidgets. Aborting.\n");
		return 1;
	}
	g_master_configuration = new MasterConfig;
	g_master_banlist = new MasterBanList;
	g_banlist_fileslist = new wxArrayString;
	//g_master_configuration->Load();
	// We'd like to use wxStandardPaths::Get()).GetUserDataDir(),
	// but that'd require us to call wxApp:SetAppName(wxT("yang")) at first.
	// That would mean to implement a wxApp-based class, and then
	// depend on the wx core library, which is not a part of wxBase.

	// So in order to avoid that, we can get folders manually.

	//g_master_configuration->Load(wxString((wxStandardPaths::Get()).GetUserDataDir()) + wxFILE_SEP_PATH + MASTER_CONFIG_FILENAME);
	l_userdatapath = new wxString;
#ifdef __WXMSW__
	if (wxGetEnv(wxT("APPDATA"), l_userdatapath))
	{
		*l_userdatapath << wxT("\\yang\\");
		//  g_master_configuration->Load(*l_userdatapath + wxT("\\yang\\") + MASTER_CONFIG_FILENAME);
#else
	if (wxGetEnv(wxT("HOME"), l_userdatapath))
	{
#if (defined __WXMAC__) || (defined __WXCOCOA__)
		*l_userdatapath << wxT("/Library/Application Support/yang/");
		//  g_master_configuration->Load(*l_userdatapath + wxT("/Library/Application Support/yang/") + MASTER_CONFIG_FILENAME);
#else
		*l_userdatapath << wxT("/.yang/");
		//  g_master_configuration->Load(*l_userdatapath + wxT("/.yang/") + MASTER_CONFIG_FILENAME);
#endif
#endif
		g_master_configuration->Load(*l_userdatapath + MASTER_CONFIG_FILENAME);
		g_banlist_fileslist->Add(*l_userdatapath + MASTER_BANLIST_FILENAME);
	}
	g_banlist_fileslist->Add(MASTER_BANLIST_FILENAME);
	g_master_banlist->Load(*g_banlist_fileslist);

	g_master_configuration->Load(MASTER_CONFIG_FILENAME);

	g_master_clientstable = new MasterClientsTable(g_master_configuration->max_clients);
	g_master_roomstable = new MasterRoomsTable(g_master_configuration->max_rooms, g_master_configuration->max_rooms_per_ip);

	wxSocketBase::Initialize();

	l_addr = new wxIPV4address;

	l_addr->Service(g_master_configuration->port_num);

	wxSocketServer *m_server = new wxSocketServer(*l_addr);

	if (!m_server->Ok())
	{
		delete m_server;
		wxFprintf(stderr, wxT("ERROR: Can't start listening master server on port number %d; Aborting.\nIn case you've just hosted this server on the same port,\nit may take about a minute until you can rehost.\n"), g_master_configuration->port_num);
		//  return false;
		//  wxUninitialize();
		return 1;
	}
	m_server->SetFlags(wxSOCKET_REUSEADDR);
	m_server->Notify(false);

	g_server_is_up = true;
	g_num_of_clients = 0;
	g_num_of_rooms = 0;

	l_server_thread = new ServerThread(m_server);
	if (l_server_thread->Create() != wxTHREAD_NO_ERROR)
	{
		l_server_thread->Delete();
		delete m_server;
		wxFprintf(stderr, wxT("ERROR: Can't create initial server thread; Aborting.\n"));
		//  return false;
		//  wxUninitialize();
		return 1;
	}

	if (g_master_configuration->banlist_refresh_time)
	{
		l_banlist_timer_thread = new BanListTimerThread(g_master_configuration->banlist_refresh_time);
		if (l_banlist_timer_thread->Create() != wxTHREAD_NO_ERROR)
		{
			l_banlist_timer_thread->Delete();
			l_server_thread->Delete();
			delete m_server;
			wxFprintf(stderr, wxT("ERROR: Can't create banlist refresh thread; Aborting.\n"));
			//    return false;
			//    wxUninitialize();
			return 1;
		}
		l_banlist_timer_thread->Run();
	}
	else
		l_banlist_timer_thread = NULL;

	l_server_thread->Run();

	wxPrintf(wxT("Master server is listening on port number %d.\nMaximum possible amount of connected clients: %d.\nMaximum possible amount of connections from the same IP: %d.\nMax possible amount of rooms: %d.\nMax possible amount of rooms per IP: %d.\nBanlist refresh time in seconds (if any): %d.\n"),
		g_master_configuration->port_num, g_master_configuration->max_clients, g_master_configuration->max_connections_per_ip,
		g_master_configuration->max_rooms, g_master_configuration->max_rooms_per_ip, g_master_configuration->banlist_refresh_time);
	if (g_master_configuration->send_msg_on_advertise)
		wxPrintf(wxT("\nMessage to send when a room is advertised:\n") + g_master_configuration->message);
	else
		wxPrintf(wxT("No special message will be sent when a room is advertised."));
	wxPrintf(wxT("\n\nPress 'b' and then enter to refresh the ban list.\nPress '4' and then enter to shut down the master server.\n"));

	g_num_of_clients = 0;
	l_inputkey = '0';
	do
	{
		if ((l_inputkey == 'b') || (l_inputkey == 'B'))
		{
			g_master_banlist->Load(*g_banlist_fileslist);
			wxPrintf(wxT("Ban List has been refreshed.\nPress '4' and then enter to shut down the master server.\n"));
		}
		l_inputkey = getchar();
	}
	while (l_inputkey != '4');
	wxPrintf(wxT("Shutting down...\n"));
	g_server_is_up = false;
	if (l_banlist_timer_thread != NULL)
	{
		l_banlist_timer_thread->Wait();
		delete l_banlist_timer_thread;
	}
	l_server_thread->Wait();
	delete l_server_thread;
	//return false;
	//wxUninitialize();
	return 0;
}
