/**************************************************************************

Copyright 2009-2023 - NY00123
Copyright 2009-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/dialog.h>
#endif

#include "outdatedexe_dialog.h"
#include "yang.h"

BEGIN_EVENT_TABLE(OutdatedExeDialog, wxDialog)
EVT_BUTTON(wxID_YES, OutdatedExeDialog::OnDownload1)
EVT_BUTTON(wxID_NO, OutdatedExeDialog::OnDownload2)
EVT_BUTTON(wxID_CANCEL, OutdatedExeDialog::OnClose)
END_EVENT_TABLE()

OutdatedExeDialog::OutdatedExeDialog(wxWindow* parent, const wxString& contents, const wxString& title,
									 const wxString& patch, const wxString& extrapatch,
									 const wxString& patch_filename, const wxString& extrapatch_filename,
									 bool have_extrapatch)
									 : wxDialog( parent, wxID_ANY, title, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE )
{
	wxBoxSizer* OutdatedExebSizer = new wxBoxSizer( wxVERTICAL );

	m_OutdatedExepanel = new wxPanel( this , wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* OutdatedExepanelbSizer = new wxBoxSizer( wxVERTICAL );

	m_outdatedexecontents = new wxStaticText( m_OutdatedExepanel, wxID_ANY, contents );
	OutdatedExepanelbSizer->Add( m_outdatedexecontents, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	wxBoxSizer* ButtonsbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_download1button = new wxButton( m_OutdatedExepanel, wxID_YES, wxT("Download ") + patch_filename );
	ButtonsbSizer->Add( m_download1button, 0, wxALL, 10 );

	if (have_extrapatch)
	{
		m_download2button = new wxButton( m_OutdatedExepanel, wxID_NO, wxT("Download ") + extrapatch_filename );
		ButtonsbSizer->Add( m_download2button, 0, wxALL, 10 );

	}

	m_closebutton = new wxButton( m_OutdatedExepanel, wxID_CANCEL, wxT("Close") );
	ButtonsbSizer->Add( m_closebutton, 0, wxALL, 10 );

	OutdatedExepanelbSizer->Add( ButtonsbSizer, 0, wxALIGN_CENTER_HORIZONTAL );

	m_OutdatedExepanel->SetSizer( OutdatedExepanelbSizer );
	m_OutdatedExepanel->Layout();
	//	OutdatedExepanelbSizer->Fit( m_OutdatedExepanel );
	OutdatedExepanelbSizer->SetSizeHints( m_OutdatedExepanel );
	OutdatedExebSizer->Add( m_OutdatedExepanel, 1, wxEXPAND, 0 );

	SetSizer( OutdatedExebSizer );
	Layout();
	//	OutdatedExebSizer->Fit( this );
	OutdatedExebSizer->SetSizeHints(this);

	Centre();
	m_patch = patch;
	m_extrapatch = extrapatch;
}

void OutdatedExeDialog::OnDownload1(wxCommandEvent& WXUNUSED(event))
{
	g_OpenURL(m_patch);
}

void OutdatedExeDialog::OnDownload2(wxCommandEvent& WXUNUSED(event))
{
	g_OpenURL(m_extrapatch);
}

void OutdatedExeDialog::OnClose(wxCommandEvent& WXUNUSED(event))
{
	EndModal(0);
}
