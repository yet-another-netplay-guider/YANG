/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/filename.h>
#include <wx/gbsizer.h>
#include <wx/log.h>
#include <wx/msgdlg.h>
//#include <wx/socket.h>
#include <wx/statbox.h>
#include <wx/mstream.h>
#include <wx/image.h>
#endif

#ifdef __WXMSW__
#include <windows.h>
#include <wx/msw/winundef.h>
#endif

#include "main_frame.h"
#include "sp_dialog.h"
#include "mp_dialog.h"
#include "manualjoin_dialog.h"
#include "srcports_dialog.h"
#include "comm_txt.h"
#include "yang.h"
#include "theme.h"
#include "yang.xpm"
#include "unknown.xpm"
#include "countryflags.h"
#include "yang_images.h"

BEGIN_EVENT_TABLE(MainFrame, YANGFrame)
//EVT_SIZE(MainFrame::OnResizeWindow)
//EVT_MAXIMIZE(MainFrame::OnMaximize)

EVT_MENU(ID_OPENSPDIALOG, MainFrame::OnLoadSPDialog)
EVT_MENU(ID_OPENMPDIALOG, MainFrame::OnLoadMPDialog)
EVT_MENU(ID_OPENMANUALJOINDIALOG, MainFrame::OnLoadManualJoinDialog)
EVT_MENU(ID_CHANGETHEME, MainFrame::OnChangeTheme)
EVT_MENU(ID_OPENSRCPORTSETTINGS, MainFrame::OnLoadSRCPortsDialog)
EVT_MENU(ID_OPENTCMODSETTINGS, MainFrame::OnLoadTCsMODsDialog)
EVT_MENU(ID_OPENNETWORKSETTINGS, MainFrame::OnLoadNetworkDialog)
//EVT_MENU(ID_OPENLOOKANDFEEL, MainFrame::OnLoadLookAndFeelDialog)
EVT_MENU(ID_OPENADVANCEDSETTINGS, MainFrame::OnLoadAdvancedDialog)
EVT_MENU(wxID_ABOUT, MainFrame::OnAbout)
#if CHECK_FOR_UPDATES
EVT_MENU(ID_CHECKFORUPDATES, MainFrame::OnCheckForUpdates)
EVT_MENU(ID_ALWAYSCHECKFORUPDATES, MainFrame::OnAutoCheckForUpdates)
#endif
#if VIEW_YANG_NET_TERMS
EVT_MENU(ID_VIEWYANGNETTERMS, MainFrame::OnViewYANGTerms)
#endif
#if VISIT_WEBSITE
EVT_MENU(ID_VISITWEBSITE, MainFrame::OnVisitWebsite)
#endif
#if OPEN_FAQ
EVT_MENU(ID_OPENFAQ, MainFrame::OnOpenFAQ)
#endif
EVT_MENU(wxID_EXIT, MainFrame::OnQuit)
EVT_CLOSE(MainFrame::OnCloseWindow)
EVT_CHECKBOX(ID_SERVERLISTAUTOCHECK, MainFrame::OnServerListAutoCheck)
EVT_BUTTON(wxID_REFRESH, MainFrame::OnGetList)
EVT_MENU(wxID_REFRESH, MainFrame::OnGetList)
//EVT_BUTTON(wxID_OK, MainFrame::OnJoin)
EVT_TREE_SEL_CHANGED(ID_FILTERTREE, MainFrame::OnFilterSelect)
EVT_LIST_ITEM_SELECTED(ID_SERVERLIST, MainFrame::OnHostSelect)
EVT_LIST_ITEM_ACTIVATED(ID_SERVERLIST, MainFrame::OnHostActivate)
/*EVT_BUTTON(ID_MANUALJOINADD, MainFrame::OnAddHost)
EVT_BUTTON(ID_MANUALJOINREM, MainFrame::OnRemHost)
EVT_CHOICE(ID_MANUALJOINLIST, MainFrame::OnManualJoinHostSelect)*/
//EVT_TEXT_ENTER(wxID_OK, MainFrame::OnJoin)
EVT_SOCKET(ID_GETLISTSOCKET, MainFrame::OnSocketEvent)
EVT_SOCKET(ID_GETPING, MainFrame::OnPingEvent)
EVT_TIMER(ID_GETLISTTIMER, MainFrame::OnTimer)
EVT_TIMER(ID_GETPINGTIMER, MainFrame::OnPingTimer)
END_EVENT_TABLE()

MainFrame::MainFrame(wxWindow* parent) : YANGFrame(parent, wxID_ANY,
#if (defined __WXMAC__) || (defined __WXCOCOA__)
												   wxT("YANG"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE|wxFRAME_NO_TASKBAR)
#else
												   wxT("YANG"))
#endif
{
	if (g_configuration)
		Maximize(g_configuration->main_is_maximized);

	m_parent = parent;
	if (!parent)
	{
		m_Mainmenubar = new wxMenuBar();
		m_Actionmenu = new wxMenu();

		wxMenuItem* m_SPmenuItem = new wxMenuItem( m_Actionmenu, ID_OPENSPDIALOG, wxString( wxT("&Single Player") ) , wxEmptyString, wxITEM_NORMAL );
		m_Actionmenu->Append( m_SPmenuItem );

		wxMenuItem* m_HostmenuItem = new wxMenuItem( m_Actionmenu, ID_OPENMPDIALOG, wxString( wxT("&Create a room") ) , wxEmptyString, wxITEM_NORMAL );
		m_Actionmenu->Append( m_HostmenuItem );

		wxMenuItem* m_ManualjoinmenuItem = new wxMenuItem( m_Actionmenu, ID_OPENMANUALJOINDIALOG, wxString( wxT("&Join a room manually") ) , wxEmptyString, wxITEM_NORMAL );
		m_Actionmenu->Append( m_ManualjoinmenuItem );
#if !((defined __WXMAC__) || (defined __WXCOCOA__))
		wxMenuItem* m_ThememenuItem = new wxMenuItem( m_Actionmenu, ID_CHANGETHEME, wxString( wxT("Change &theme") ) , wxEmptyString, wxITEM_NORMAL );
		m_Actionmenu->Append( m_ThememenuItem );
#endif
		wxMenuItem* m_QuitmenuItem = new wxMenuItem( m_Actionmenu, wxID_EXIT, wxString( wxT("&Quit") ) + wxT('\t') + wxT("Ctrl-Q"), wxEmptyString, wxITEM_NORMAL );
		m_Actionmenu->Append( m_QuitmenuItem );

		m_Mainmenubar->Append( m_Actionmenu, wxT("&Action") );

		m_Settingsmenu = new wxMenu();

		wxMenuItem* m_SRCPortsmenuItem = new wxMenuItem( m_Settingsmenu, ID_OPENSRCPORTSETTINGS, wxString( wxT("&Source ports") ) , wxEmptyString, wxITEM_NORMAL );
		m_Settingsmenu->Append( m_SRCPortsmenuItem );

		wxMenuItem* m_TCsMODsmenuItem = new wxMenuItem( m_Settingsmenu, ID_OPENTCMODSETTINGS, wxString( wxT("&TCs and MODs") ) , wxEmptyString, wxITEM_NORMAL );
		m_Settingsmenu->Append( m_TCsMODsmenuItem );

		wxMenuItem* m_NetworkingmenuItem = new wxMenuItem( m_Settingsmenu, ID_OPENNETWORKSETTINGS, wxString( wxT("&Multiplayer and networking") ) , wxEmptyString, wxITEM_NORMAL );
		m_Settingsmenu->Append( m_NetworkingmenuItem );

		/*		wxMenuItem* m_LookAndFeelmenuItem;
		m_LookAndFeelmenuItem = new wxMenuItem( m_Settingsmenu, ID_OPENLOOKANDFEEL, wxString( wxT("Look and Feel") ) , wxEmptyString, wxITEM_NORMAL );
		m_Settingsmenu->Append( m_LookAndFeelmenuItem );*/

		wxMenuItem* m_AdvancedmenuItem = new wxMenuItem( m_Settingsmenu, ID_OPENADVANCEDSETTINGS, wxString( wxT("&Advanced") ) , wxEmptyString, wxITEM_NORMAL );
		m_Settingsmenu->Append( m_AdvancedmenuItem );

		m_Mainmenubar->Append( m_Settingsmenu, wxT("&Settings") );

		m_Helpmenu = new wxMenu();

#if OPEN_FAQ
		wxMenuItem* m_faqmenuItem = new wxMenuItem( m_Helpmenu, ID_OPENFAQ, wxString( wxT("&Frequently Asked Questions") ) , wxEmptyString, wxITEM_NORMAL );
		m_Helpmenu->Append( m_faqmenuItem );
#endif

		wxMenuItem* m_AboutmenuItem = new wxMenuItem( m_Helpmenu, wxID_ABOUT, wxString( wxT("&About") ) , wxEmptyString, wxITEM_NORMAL );
		m_Helpmenu->Append( m_AboutmenuItem );

#if CHECK_FOR_UPDATES
		wxMenuItem* m_checkforupdatesmenuItem = new wxMenuItem( m_Helpmenu, ID_CHECKFORUPDATES, wxString( wxT("&Check for updates") ) , wxEmptyString, wxITEM_NORMAL );
		m_Helpmenu->Append( m_checkforupdatesmenuItem );

		m_checkforupdatesonstartupmenuItem = new wxMenuItem( m_Helpmenu, ID_ALWAYSCHECKFORUPDATES, wxString( wxT("Check for updates on &startup") ) , wxEmptyString, wxITEM_CHECK );
		m_Helpmenu->Append( m_checkforupdatesonstartupmenuItem );
		if (g_configuration)
			m_checkforupdatesonstartupmenuItem->Check(g_configuration->check_for_updates_on_startup);
#endif

#if VIEW_YANG_NET_TERMS
		wxMenuItem* m_visitwebsitemenuItem = new wxMenuItem( m_Helpmenu, ID_VIEWYANGNETTERMS, wxString( wxT("View the YANG network &terms") ) , wxEmptyString, wxITEM_NORMAL );
		m_Helpmenu->Append( m_visitwebsitemenuItem );
#endif

#if VISIT_WEBSITE
//		wxMenuItem* m_visitwebsitemenuItem = new wxMenuItem( m_Helpmenu, ID_VISITWEBSITE, wxString( wxT("&Visit NY00123's website") ) , wxEmptyString, wxITEM_NORMAL );
		wxMenuItem* m_viewyangnettermsmenuItem = new wxMenuItem( m_Helpmenu, ID_VISITWEBSITE, wxString( wxT("&Visit the YANG web page") ) , wxEmptyString, wxITEM_NORMAL );
		m_Helpmenu->Append( m_viewyangnettermsmenuItem );
#endif

		m_Mainmenubar->Append( m_Helpmenu, wxT("&Help") );
#if (!((defined __WXMAC__) || (defined __WXCOCOA__)))
		SetMenuBar( m_Mainmenubar );
#endif
	}

	wxBoxSizer* MainFramebSizer = new wxBoxSizer( wxVERTICAL );

	m_mainpanel = new YANGPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* mainpanelbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_filtertreeCtrl = new YANGTreeCtrl( m_mainpanel, ID_FILTERTREE, wxDefaultPosition, wxDLG_UNIT(this, wxSize(112, -1)));

	m_games_name.Add(GAMENAME_BLOOD);
	m_games_name.Add(GAMENAME_DESCENT);
	m_games_name.Add(GAMENAME_DESCENT2);
	m_games_name.Add(GAMENAME_DN3D);
	m_games_name.Add(GAMENAME_SW);
	m_games_name.Add(GAMENAME_CUSTOM);

	m_ports_name.Add(SRCPORTNAME_DOSBLOODSW);
	m_ports_name.Add(SRCPORTNAME_DOSBLOODRG);
	m_ports_name.Add(SRCPORTNAME_DOSBLOODPP);
	m_ports_name.Add(SRCPORTNAME_DOSBLOODOU);
	m_ports_name.Add(SRCPORTNAME_DOSDESCENT);
	m_ports_name.Add(SRCPORTNAME_D1XREBIRTH);
	m_ports_name.Add(SRCPORTNAME_DOSDESCENT2);
	m_ports_name.Add(SRCPORTNAME_D2XREBIRTH);
	m_ports_name.Add(SRCPORTNAME_DOSDUKESW);
	m_ports_name.Add(SRCPORTNAME_DOSDUKERG);
	m_ports_name.Add(SRCPORTNAME_DOSDUKEAE);
	m_ports_name.Add(SRCPORTNAME_DUKE3DW);
	m_ports_name.Add(SRCPORTNAME_EDUKE32);
	m_ports_name.Add(SRCPORTNAME_NDUKE);
	m_ports_name.Add(SRCPORTNAME_HDUKE);
	m_ports_name.Add(SRCPORTNAME_XDUKE);
	m_ports_name.Add(SRCPORTNAME_DOSSWSW);
	m_ports_name.Add(SRCPORTNAME_DOSSWRG);
	m_ports_name.Add(SRCPORTNAME_VOIDSW);
	m_ports_name.Add(SRCPORTNAME_SWP);

	m_games_code.Add(GAMECODE_BLOOD);
	m_games_code.Add(GAMECODE_DESCENT);
	m_games_code.Add(GAMECODE_DESCENT2);
	m_games_code.Add(GAMECODE_DN3D);
	m_games_code.Add(GAMECODE_SW);
	m_games_code.Add(GAMECODE_CUSTOM);

	m_ports_code.Add(SRCPORTCODE_DOSBLOODSW);
	m_ports_code.Add(SRCPORTCODE_DOSBLOODRG);
	m_ports_code.Add(SRCPORTCODE_DOSBLOODPP);
	m_ports_code.Add(SRCPORTCODE_DOSBLOODOU);
	m_ports_code.Add(SRCPORTCODE_DOSDESCENT);
	m_ports_code.Add(SRCPORTCODE_D1XREBIRTH);
	m_ports_code.Add(SRCPORTCODE_DOSDESCENT2);
	m_ports_code.Add(SRCPORTCODE_D2XREBIRTH);
	m_ports_code.Add(SRCPORTCODE_DOSDUKESW);
	m_ports_code.Add(SRCPORTCODE_DOSDUKERG);
	m_ports_code.Add(SRCPORTCODE_DOSDUKEAE);
	m_ports_code.Add(SRCPORTCODE_DUKE3DW);
	m_ports_code.Add(SRCPORTCODE_EDUKE32);
	m_ports_code.Add(SRCPORTCODE_NDUKE);
	m_ports_code.Add(SRCPORTCODE_HDUKE);
	m_ports_code.Add(SRCPORTCODE_XDUKE);
	m_ports_code.Add(SRCPORTCODE_DOSSWSW);
	m_ports_code.Add(SRCPORTCODE_DOSSWRG);
	m_ports_code.Add(SRCPORTCODE_VOIDSW);
	m_ports_code.Add(SRCPORTCODE_SWP);
	m_ports_code.Add(SRCPORTCODE_CUSTOM);

	int l_loop_var, l_loop_var2;

	sourceport_ranges[0] = SOURCEPORT_FIRSTBLOODPORT;
	sourceport_ranges[1] = SOURCEPORT_FIRSTDESCENTPORT;
	sourceport_ranges[2] = SOURCEPORT_FIRSTDESCENT2PORT;
	sourceport_ranges[3] = SOURCEPORT_FIRSTDUKEPORT;
	sourceport_ranges[4] = SOURCEPORT_FIRSTSWPORT;
	sourceport_ranges[5] = SOURCEPORT_CUSTOM;
	sourceport_ranges[6] = SOURCEPORT_NUMOFPORTS;

	m_filtertreeCtrl->AddRoot(wxT("All Games (0)"));

	for (l_loop_var = 0; l_loop_var < GAME_NUMOFGAMES; l_loop_var++)
	{
		m_games[l_loop_var].Id = m_filtertreeCtrl->AppendItem(m_filtertreeCtrl->GetRootItem(), m_games_name[l_loop_var] + wxT(" (0)"));

		for (l_loop_var2 = sourceport_ranges[l_loop_var]; l_loop_var2 < sourceport_ranges[l_loop_var+1]; l_loop_var2++)
		{
			if (l_loop_var2 != SOURCEPORT_CUSTOM)
				m_ports[l_loop_var2].Id = m_filtertreeCtrl->AppendItem(m_games[l_loop_var].Id, m_ports_name[l_loop_var2] + wxT(" (0)"));
			else
				m_ports[l_loop_var2].Id = NULL;

			game_ranges[l_loop_var2] = l_loop_var;
		}
	}

	mainpanelbSizer->Add( m_filtertreeCtrl, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* serverlistbSizer;
	serverlistbSizer = new wxBoxSizer( wxVERTICAL );

	m_serverlistCtrl = new YANGListCtrl( m_mainpanel, ID_SERVERLIST, wxDefaultPosition, wxDLG_UNIT(this, wxSize(200, 160)), wxLC_REPORT|wxLC_SINGLE_SEL );

	m_ImageList = new wxImageList(16, 16);
	m_serverlistCtrl->AssignImageList(m_ImageList, wxIMAGE_LIST_SMALL);

	if (!g_configuration)
		return;

	wxListItem itemCol;
	itemCol.SetText(wxT("Private"));
	//	itemCol.SetAlign(wxLIST_FORMAT_LEFT);
	//	itemCol.SetImage(-1);
	m_serverlistCtrl->InsertColumn(COL_PRIVATE, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_PRIVATE, g_configuration->main_col_private_len);
	itemCol.SetText(wxT("Location"));
	m_serverlistCtrl->InsertColumn(COL_LOCATION, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_LOCATION, g_configuration->main_col_loc_len);
	itemCol.SetText(wxT("Room Name"));
	m_serverlistCtrl->InsertColumn(COL_ROOM_NAME, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_ROOM_NAME, g_configuration->main_col_roomname_len);
	itemCol.SetText(wxT("Ping"));
	m_serverlistCtrl->InsertColumn(COL_PING, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_PING, g_configuration->main_col_ping_len);
	itemCol.SetText(wxT("Flux"));
	m_serverlistCtrl->InsertColumn(COL_FLUX, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_FLUX, g_configuration->main_col_flux_len);
	itemCol.SetText(wxT("Game"));
	m_serverlistCtrl->InsertColumn(COL_GAME, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_GAME, g_configuration->main_col_game_len);
	itemCol.SetText(wxT("Source Port"));
	m_serverlistCtrl->InsertColumn(COL_SOURCE_PORT, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_SOURCE_PORT, g_configuration->main_col_srcport_len);
	itemCol.SetText(wxT("Players"));
	m_serverlistCtrl->InsertColumn(COL_PLAYERS, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_PLAYERS, g_configuration->main_col_players_len);
	itemCol.SetText(wxT("Map"));
	m_serverlistCtrl->InsertColumn(COL_MAP, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_MAP, g_configuration->main_col_map_len);
	itemCol.SetText(wxT("TC/MOD"));
	m_serverlistCtrl->InsertColumn(COL_TC_MOD, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_TC_MOD, g_configuration->main_col_tcmod_len);
	itemCol.SetText(wxT("Game Type"));
	m_serverlistCtrl->InsertColumn(COL_GAME_TYPE, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_GAME_TYPE, g_configuration->main_col_gametype_len);
	itemCol.SetText(wxT("Skill"));
	m_serverlistCtrl->InsertColumn(COL_SKILL, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_SKILL, g_configuration->main_col_skill_len);
	itemCol.SetText(wxT("In game"));
	m_serverlistCtrl->InsertColumn(COL_INGAME, itemCol);
	m_serverlistCtrl->SetColumnWidth(COL_INGAME, g_configuration->main_col_ingame_len);
	//	RefreshListCtrlSize();

	serverlistbSizer->Add( m_serverlistCtrl, 1, wxTOP|wxBOTTOM|wxRIGHT|wxEXPAND, 5 );

	wxBoxSizer* serverlistcontrolbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_getlistbutton = new YANGButton( m_mainpanel, wxID_REFRESH, wxT("Get list of rooms"), wxDefaultPosition, wxDefaultSize, 0 );
	serverlistcontrolbSizer->Add( m_getlistbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	if (!m_parent)
	{
		m_serverliststaticText = new wxStaticText( m_mainpanel, wxID_ANY, wxT("Double-click a selection (in the top table of rooms, not the one of players) to join."), wxDefaultPosition, wxDefaultSize, 0 );
		serverlistcontrolbSizer->Add( m_serverliststaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	}

	//	m_joinbutton = new YANGButton( m_mainpanel, wxID_OK, wxT("Join"), wxDefaultPosition, wxDefaultSize, 0 );
	//	serverlistcontrolbSizer->Add( m_joinbutton, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );


	serverlistcontrolbSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_autolookupcheckBox = new YANGCheckBox( m_mainpanel, ID_SERVERLISTAUTOCHECK, wxT("Look for rooms on startup."), wxDefaultPosition, wxDefaultSize, 0 );
	serverlistcontrolbSizer->Add( m_autolookupcheckBox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	serverlistbSizer->Add( serverlistcontrolbSizer, 0, wxEXPAND, 5 );

	/*	m_playerlistBox = new YANGListBox( m_mainpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(56, 80)), 0, NULL, 0 ); 
	serverlistbSizer->Add( m_playerlistBox, 2, wxALL|wxEXPAND, 5 );*/
	m_playerlistCtrl = new YANGListCtrl( m_mainpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(-1, 98)), wxLC_REPORT );

	m_playerImageList = new wxImageList(16, 16);
	m_playerlistCtrl->AssignImageList(m_playerImageList, wxIMAGE_LIST_SMALL);

	itemCol.SetText(wxT("Location"));
	m_playerlistCtrl->InsertColumn(PLAYERCOL_LOCATION, itemCol);
	m_playerlistCtrl->SetColumnWidth(PLAYERCOL_LOCATION, g_configuration->mainplayer_col_loc_len);
	itemCol.SetText(wxT("Nickname"));
	m_playerlistCtrl->InsertColumn(PLAYERCOL_NICKNAME, itemCol);
	m_playerlistCtrl->SetColumnWidth(PLAYERCOL_NICKNAME, g_configuration->mainplayer_col_nick_len);
	itemCol.SetText(wxT("Operating System"));
	m_playerlistCtrl->InsertColumn(PLAYERCOL_OS, itemCol);
	m_playerlistCtrl->SetColumnWidth(PLAYERCOL_OS, g_configuration->mainplayer_col_os_len);

	serverlistbSizer->Add( m_playerlistCtrl, 0, wxTOP|wxBOTTOM|wxRIGHT|wxEXPAND, 5 );

	mainpanelbSizer->Add( serverlistbSizer, 1, wxEXPAND );
#if 0	
	/****** WORKAROUND: Why should the button be created here, so with dark theme it isn't fully grey? ******/
	m_joinbutton = new YANGButton( m_mainpanel, wxID_OK, wxT("Join"), wxDefaultPosition, wxDefaultSize, 0 );

	wxStaticBoxSizer* ManualJoinsbSizer = new wxStaticBoxSizer( new wxStaticBox( m_mainpanel, wxID_ANY, wxT("Manual Join") ), wxVERTICAL );

	wxFlexGridSizer* ManualJoinfgSizer = new wxFlexGridSizer( 2, 5, 0, 0 );
	ManualJoinfgSizer->AddGrowableCol( 0 );
	ManualJoinfgSizer->AddGrowableCol( 1 );
	ManualJoinfgSizer->AddGrowableCol( 4 );
	ManualJoinfgSizer->SetFlexibleDirection( wxBOTH );
	ManualJoinfgSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_manualjoinnamestaticText = new wxStaticText( m_mainpanel, wxID_ANY, wxT("Name"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinfgSizer->Add( m_manualjoinnamestaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_manualjoinhostaddrstaticText = new wxStaticText( m_mainpanel, wxID_ANY, wxT("Hostname or IP"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinfgSizer->Add( m_manualjoinhostaddrstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	ManualJoinfgSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_manualjoinportnumstaticText = new wxStaticText( m_mainpanel, wxID_ANY, wxT("Port Number"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinfgSizer->Add( m_manualjoinportnumstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );


	ManualJoinfgSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_manualjoinnametextCtrl = new YANGTextCtrl( m_mainpanel, wxID_OK, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	ManualJoinfgSizer->Add( m_manualjoinnametextCtrl, 0, wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_manualjoinhostaddrtextCtrl = new YANGTextCtrl( m_mainpanel, wxID_OK, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	ManualJoinfgSizer->Add( m_manualjoinhostaddrtextCtrl, 0, wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_manualjoinseparatorstaticText = new wxStaticText( m_mainpanel, wxID_ANY, wxT(":"), wxDefaultPosition, wxDefaultSize, 0 );
	m_manualjoinseparatorstaticText->Wrap( -1 );
	ManualJoinfgSizer->Add( m_manualjoinseparatorstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_manualjoinportnumtextCtrl = new YANGTextCtrl( m_mainpanel, wxID_OK, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER );
	ManualJoinfgSizer->Add( m_manualjoinportnumtextCtrl, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_manualjoinaddbutton = new YANGButton( m_mainpanel, ID_MANUALJOINADD, wxT("Add and save"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinfgSizer->Add( m_manualjoinaddbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );

	ManualJoinsbSizer->Add( ManualJoinfgSizer, 0, wxEXPAND, 5 );
	//	mainpanelbSizer->Add( ManualJoinfgSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* ManualJoinlistbSizer = new wxBoxSizer( wxHORIZONTAL );

	//	wxArrayString m_manualjoinchoiceChoices;
	m_manualjoinchoice = new YANGChoice( m_mainpanel, ID_MANUALJOINLIST, wxDefaultPosition, wxDefaultSize/*, m_manualjoinchoiceChoices, 0 */);
	//	m_manualjoinchoice->SetSelection( 0 );
	ManualJoinlistbSizer->Add( m_manualjoinchoice, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_manualjoinrembutton = new YANGButton( m_mainpanel, ID_MANUALJOINREM, wxT("Remove permanently"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinlistbSizer->Add( m_manualjoinrembutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	ManualJoinsbSizer->Add( ManualJoinlistbSizer, 0, wxEXPAND, 5 );
	//	mainpanelbSizer->Add( ManualJoinlistbSizer, 0, wxEXPAND, 5 );

	/****** WORKAROUND: Why should the button be created before, so with dark theme it isn't fully grey? ******/
	//	m_joinbutton = new YANGButton( m_mainpanel, wxID_OK, wxT("Join"), wxDefaultPosition, wxDefaultSize, 0 );
	ManualJoinsbSizer->Add( m_joinbutton, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	mainpanelbSizer->Add( ManualJoinsbSizer, 0, wxEXPAND, 5 );
#endif	
	m_mainpanel->SetSizer( mainpanelbSizer );
	m_mainpanel->Layout();
	//	mainpanelbSizer->Fit( m_mainpanel );
	MainFramebSizer->Add( m_mainpanel, 1, wxEXPAND | wxALL, 0 );

	SetSizer( MainFramebSizer );
	Layout();
	m_MainstatusBar = CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );
	//	MainFramebSizer->Fit( this );
	MainFramebSizer->SetSizeHints(this);

	SetIcon(wxIcon(yang_xpm));
	SetSize(g_configuration->main_win_width, g_configuration->main_win_height);
	Centre();

	m_autolookupcheckBox->SetValue(g_configuration->lookup_on_startup);
	/*size_t l_num_of_items = g_configuration->manualjoin_portnums.GetCount();
	for (size_t l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
	m_manualjoinchoice->Append(g_configuration->manualjoin_names[l_loop_var] +
	wxT(' ') + g_configuration->manualjoin_addrs[l_loop_var] +
	wxString::Format(wxT(":%d"), (int)g_configuration->manualjoin_portnums[l_loop_var]));*/
	m_client = NULL;
	m_serverlist_timer = new wxTimer(this, ID_GETLISTTIMER);
	m_ping_timer = new wxTimer(this, ID_GETPINGTIMER);

	wxAcceleratorEntry l_entries[1];
	l_entries[0].Set(wxACCEL_NORMAL, WXK_F5, wxID_REFRESH);
	wxAcceleratorTable l_accel(4, l_entries);
	SetAcceleratorTable(l_accel);


	m_filtertreeCtrl->ExpandAll();

	// This is done to avoid triggering the refresh of the rooms list,
	// by "tree selection events" not actually triggered by the user.

	// Unfortunately, current behavior seems to be platform-specific.
	// It looks like wxTreeCtrl::ExpandAll() triggers the event on Windows,
	// but not on Mac and Linux.
#ifdef __WXMSW__
	first_time_filter_select = 1;
#else
	first_time_filter_select = 0;
#endif
	if (g_configuration->game_filter_tree != wxT("all"))
	{
		first_time_filter_select++;

		for (l_loop_var = 0; l_loop_var < GAME_NUMOFGAMES; l_loop_var++)
			if (g_configuration->game_filter_tree == m_games_code[l_loop_var])
				m_filtertreeCtrl->SelectItem(m_games[l_loop_var].Id);

		for (l_loop_var = 0; l_loop_var < SOURCEPORT_NUMOFPORTS; l_loop_var++)
			if (g_configuration->game_filter_tree == m_ports_code[l_loop_var])
				m_filtertreeCtrl->SelectItem(m_ports[l_loop_var].Id);
	}
}

MainFrame::~MainFrame()
{
	if (m_parent)
	{
		if (m_client)
		{
			if (m_client->IsConnected()) // If not yet connected, no timeout is set => hang!
				g_SendCStringToSocket(m_client, "1:disconnect:");
			m_client->Destroy();
			m_client = NULL;
		}
		SaveUIControlSizes();
		m_serverlist_timer->Stop();
		m_ping_timer->Stop();
	}
}
/*
void MainFrame::RefreshListCtrlSize()
{
int l_width;
if (m_serverlistCtrl->GetItemCount())
l_width = wxLIST_AUTOSIZE;
else
l_width = wxLIST_AUTOSIZE_USEHEADER;
m_serverlistCtrl->SetColumnWidth(0, l_width);
m_serverlistCtrl->SetColumnWidth(1, l_width);
m_serverlistCtrl->SetColumnWidth(2, l_width);
m_serverlistCtrl->SetColumnWidth(3, l_width);
m_serverlistCtrl->SetColumnWidth(4, l_width);
m_serverlistCtrl->SetColumnWidth(5, l_width);
m_serverlistCtrl->SetColumnWidth(6, l_width);
m_serverlistCtrl->SetColumnWidth(7, l_width);
m_serverlistCtrl->SetColumnWidth(8, l_width);
m_serverlistCtrl->Refresh();
}

void MainFrame::OnResizeWindow(wxSizeEvent& event)
{
wxSize l_size = event.GetSize();
if (!IsMaximized())
{
g_configuration->main_win_width = l_size.GetWidth();
g_configuration->main_win_height = l_size.GetHeight();
}
}

void MainFrame::OnMaximize(wxMaximizeEvent& WXUNUSED(event))
{
if (IsMaximized())
GetSize(&g_configuration->main_win_width, &g_configuration->main_win_height);
}
*/
void MainFrame::OnChangeTheme(wxCommandEvent& WXUNUSED(event))
{
	//int l_item_count = m_serverlistCtrl->GetItemCount(), l_loop_var;
	//wxArrayLong l_array_items;
	//wxColour l_colour;
	g_configuration->theme_is_dark = !g_configuration->theme_is_dark;
	SaveUIControlSizes();
	//g_configuration->Save();

	g_main_frame = new MainFrame(NULL);
	HideMainFrame();
	g_main_frame->ShowMainFrame();
	Close();

	// According to the experience, wxStaticText objects are updated automatically.
	/*m_Mainmenubar->SetThemeColour();
	m_mainpanel->SetThemeColour();

	for (l_loop_var = 0; l_loop_var < l_item_count; l_loop_var++)
	{
	l_colour = m_serverlistCtrl->GetItemTextColour(l_loop_var);
	if (l_colour == *wxRED)
	l_array_items.Add(l_loop_var);
	}
	m_serverlistCtrl->SetThemeColour();
	l_loop_var = 0;
	while ((l_loop_var < l_item_count) && (l_array_items.GetCount()))
	{
	if (l_loop_var == l_array_items[0])
	{
	l_array_items.RemoveAt(0);
	m_serverlistCtrl->SetItemTextColour(l_loop_var, *wxRED);
	}
	else
	if (g_configuration->theme_is_dark)
	m_serverlistCtrl->SetItemTextColour(l_loop_var, *wxGREEN);
	l_loop_var++;
	} // Even if there's nothing marked with red colour, we're probably not done.
	while (l_loop_var < l_item_count)
	{
	if (g_configuration->theme_is_dark)
	m_serverlistCtrl->SetItemTextColour(l_loop_var, *wxGREEN);
	l_loop_var++;
	}

	m_playerlistBox->SetThemeColour();
	m_getlistbutton->SetThemeColour();
	m_joinbutton->SetThemeColour();
	m_autolookupcheckBox->SetThemeColour();
	m_manualjoinnametextCtrl->SetThemeColour();
	m_manualjoinhostaddrtextCtrl->SetThemeColour();
	m_manualjoinportnumtextCtrl->SetThemeColour();
	m_manualjoinaddbutton->SetThemeColour();
	m_manualjoinchoice->SetThemeColour();
	m_manualjoinrembutton->SetThemeColour();
	SetStatusBarThemeColour(m_MainstatusBar);*/
}

void MainFrame::OnLoadSPDialog(wxCommandEvent& WXUNUSED(event))
{
	if (HAVE_BLOOD || HAVE_DESCENT || HAVE_DESCENT2 || HAVE_DN3D || HAVE_SW || HAVE_CUSTOM)
	{
		//  if (!g_sp_dialog)
		//  {
		HideMainFrame();
		g_sp_dialog = new SPDialog;
		g_sp_dialog->Show(true);
		g_sp_dialog->SetFocus();
		//  g_sp_dialog->Raise();
		//  }
		//  else
		//    g_sp_dialog->SetFocus();
	}
	else
		wxMessageBox(wxT("You should configure a source port\nso you can use one to launch the game,\naccessing Settings --> Source ports."),
		wxT("There are no configured source ports."), wxOK|wxICON_INFORMATION);
}

void MainFrame::OnLoadMPDialog(wxCommandEvent& WXUNUSED(event))
{
	GameType l_game;
	bool l_use_crypticpassage = false;
	wxString l_modname;
	wxArrayString l_modfiles;
	wxString l_modconfile;
	wxString l_moddeffile;
	bool l_showmodurl = false;
	wxString l_modurl;
	size_t l_episode_map = 0;
	wxString l_user_map;
	bool l_launch_usermap;
	size_t l_skill = 0;
	size_t l_connectiontype = 0;
	bool l_allow_ingame_joining = false;
	wxString l_roomname;
	size_t l_max_num_players;
	size_t l_maxping;
	size_t l_rxdelay = DEFAULT_RXDELAY;
	wxString l_args_for_host;
	wxString l_args_for_all;

	size_t l_loop_var, l_num_of_items;
	bool l_start_server = (g_configuration->gamelaunch_skip_settings);

	if (l_start_server)
	{
		if (((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODSW) && g_configuration->have_dosbloodsw && g_configuration->have_dosbox) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODRG) && g_configuration->have_dosbloodrg && g_configuration->have_dosbox) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODPP) && g_configuration->have_dosbloodpp && g_configuration->have_dosbox) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODOU) && g_configuration->have_dosbloodou && g_configuration->have_dosbox))
		{
			l_game = GAME_BLOOD;

			l_use_crypticpassage = (((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODRG && wxFileExists(((wxFileName)g_configuration->dosbloodrg_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))) ||
									 (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODPP && wxFileExists(((wxFileName)g_configuration->dosbloodpp_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))) ||
									 (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODOU && wxFileExists(((wxFileName)g_configuration->dosbloodou_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE")))) &&
									 g_configuration->gamelaunch_blood_use_crypticpassage);

			l_skill = g_configuration->gamelaunch_blood_skill;

			if (!l_use_crypticpassage)
				l_episode_map = g_configuration->gamelaunch_bloodlvl;
			else
				l_episode_map = g_configuration->gamelaunch_bloodlvl_cp;

			l_user_map = g_configuration->gamelaunch_bloodusermap;
			l_roomname = g_configuration->gamelaunch_blood_roomname;
			l_max_num_players = g_configuration->gamelaunch_blood_numofplayers;
			l_connectiontype = g_configuration->gamelaunch_blood_packetmode;
			l_maxping = g_configuration->gamelaunch_blood_maxping;
			l_args_for_host = g_configuration->gamelaunch_blood_args_for_host;
#if EXTRA_ARGS_FOR_ALL
			l_args_for_all = g_configuration->gamelaunch_blood_args_for_all;
#endif
		}
		else if (((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDESCENT) && g_configuration->have_dosdescent && g_configuration->have_dosbox) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_D1XREBIRTH) && g_configuration->have_d1xrebirth))
		{
			l_game = GAME_DESCENT;
			l_skill = 0;
			l_episode_map = g_configuration->gamelaunch_descentlvl;
			l_user_map = g_configuration->gamelaunch_descentusermap;
			l_roomname = g_configuration->gamelaunch_descent_roomname;
			l_max_num_players = g_configuration->gamelaunch_descent_numofplayers;
			l_maxping = g_configuration->gamelaunch_descent_maxping;
			l_args_for_host = g_configuration->gamelaunch_descent_args_for_host;
#if EXTRA_ARGS_FOR_ALL
			l_args_for_all = g_configuration->gamelaunch_descent_args_for_all;
#endif
		}
		else if (((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDESCENT2) && g_configuration->have_dosdescent2 && g_configuration->have_dosbox) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_D2XREBIRTH) && g_configuration->have_d2xrebirth))
		{
			l_game = GAME_DESCENT2;
			l_skill = 0;
			l_episode_map = g_configuration->gamelaunch_descent2lvl;
			l_user_map = g_configuration->gamelaunch_descent2usermap;
			l_roomname = g_configuration->gamelaunch_descent2_roomname;
			l_max_num_players = g_configuration->gamelaunch_descent2_numofplayers;
			l_maxping = g_configuration->gamelaunch_descent2_maxping;
			l_args_for_host = g_configuration->gamelaunch_descent2_args_for_host;
#if EXTRA_ARGS_FOR_ALL
			l_args_for_all = g_configuration->gamelaunch_descent2_args_for_all;
#endif
		}
		else if (((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKESW) && g_configuration->have_dosdukesw && g_configuration->have_dosbox) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKERG) && g_configuration->have_dosdukerg && g_configuration->have_dosbox) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKEAE) && g_configuration->have_dosdukeae && g_configuration->have_dosbox) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_DUKE3DW) && g_configuration->have_duke3dw) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_EDUKE32) && g_configuration->have_eduke32) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_NDUKE) && g_configuration->have_nduke) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_HDUKE) && g_configuration->have_hduke) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_XDUKE) && g_configuration->have_xduke))
		{
			l_game = GAME_DN3D;

			if (!g_configuration->gamelaunch_dn3dtcmod_profilename.IsEmpty() && g_configuration->dn3dtcmod_profile_list)
			{
				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

				while (g_configuration->dn3dtcmod_profile_current->profilename != g_configuration->gamelaunch_dn3dtcmod_profilename && g_configuration->dn3dtcmod_profile_current->next)
					g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;

				if (g_configuration->dn3dtcmod_profile_current->profilename != g_configuration->gamelaunch_dn3dtcmod_profilename)
				{
					g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

					g_configuration->gamelaunch_dn3dtcmod_profilename = g_configuration->dn3dtcmod_profile_current->profilename;
				}

				l_modname = g_configuration->dn3dtcmod_profile_current->profilename;

				l_num_of_items = g_configuration->dn3dtcmod_profile_current->modfiles.GetCount();

				for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
					l_modfiles.Add(((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName());

				l_modconfile = g_configuration->dn3dtcmod_profile_current->confile;

				l_moddeffile = g_configuration->dn3dtcmod_profile_current->deffile;

				l_showmodurl = g_configuration->dn3dtcmod_profile_current->usemodurl;
				l_modurl = g_configuration->dn3dtcmod_profile_current->modurl;
			}

			l_skill = g_configuration->gamelaunch_dn3d_skill;
			l_episode_map = g_configuration->gamelaunch_dn3dlvl;
			l_user_map = g_configuration->gamelaunch_dn3dusermap;
			l_roomname = g_configuration->gamelaunch_dn3d_roomname;
			l_max_num_players = g_configuration->gamelaunch_dn3d_numofplayers;

			if (g_configuration->gamelaunch_source_port == SOURCEPORT_DUKE3DW)
				l_connectiontype = g_configuration->gamelaunch_duke3dw_connectiontype;
			else if (g_configuration->gamelaunch_source_port == SOURCEPORT_EDUKE32)
				l_connectiontype = g_configuration->gamelaunch_eduke32_connectiontype;

			l_maxping = g_configuration->gamelaunch_dn3d_maxping;
			l_args_for_host = g_configuration->gamelaunch_dn3d_args_for_host;
#if EXTRA_ARGS_FOR_ALL
			l_args_for_all = g_configuration->gamelaunch_dn3d_args_for_all;
#endif
		}
		else if (((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSSWSW) && g_configuration->have_dosswsw && g_configuration->have_dosbox) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_DOSSWRG) && g_configuration->have_dosswrg && g_configuration->have_dosbox) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_VOIDSW) && g_configuration->have_voidsw) ||
			((g_configuration->gamelaunch_source_port == SOURCEPORT_SWP) && g_configuration->have_swp))
		{
			l_game = GAME_SW;

			if (!g_configuration->gamelaunch_swtcmod_profilename.IsEmpty() && g_configuration->swtcmod_profile_list)
			{
				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

				while (g_configuration->swtcmod_profile_current->profilename != g_configuration->gamelaunch_swtcmod_profilename && g_configuration->swtcmod_profile_current->next)
					g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;

				if (g_configuration->swtcmod_profile_current->profilename != g_configuration->gamelaunch_swtcmod_profilename)
				{
					g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

					g_configuration->gamelaunch_swtcmod_profilename = g_configuration->swtcmod_profile_current->profilename;
				}

				l_modname = g_configuration->swtcmod_profile_current->profilename;

				l_num_of_items = g_configuration->swtcmod_profile_current->modfiles.GetCount();

				for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
					l_modfiles.Add(((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName());

				l_moddeffile = g_configuration->swtcmod_profile_current->deffile;

				l_showmodurl = g_configuration->swtcmod_profile_current->usemodurl;
				l_modurl = g_configuration->swtcmod_profile_current->modurl;
			}

			l_skill = g_configuration->gamelaunch_sw_skill;
			l_episode_map = g_configuration->gamelaunch_swlvl;
			l_user_map = g_configuration->gamelaunch_swusermap;
			l_roomname = g_configuration->gamelaunch_sw_roomname;
			l_max_num_players = g_configuration->gamelaunch_sw_numofplayers;
			l_maxping = g_configuration->gamelaunch_sw_maxping;
			l_args_for_host = g_configuration->gamelaunch_sw_args_for_host;
#if EXTRA_ARGS_FOR_ALL
			l_args_for_all = g_configuration->gamelaunch_sw_args_for_all;
#endif

			l_connectiontype =
				(g_configuration->gamelaunch_source_port == SOURCEPORT_VOIDSW) ?
				CONNECTIONTYPE_MASTERSLAVE : CONNECTIONTYPE_PEER2PEER;
		}
		else if ((g_configuration->gamelaunch_source_port == SOURCEPORT_CUSTOM) && HAVE_CUSTOM && g_configuration->have_custom_mpprofile)
		{
			g_configuration->custom_profile_current = g_configuration->custom_profile_list;

			while ((g_configuration->custom_profile_current->profilename != g_configuration->gamelaunch_custom_profilename || g_configuration->custom_profile_current->spgameonly) && g_configuration->custom_profile_current->next)
				g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;

			if (g_configuration->custom_profile_current->profilename != g_configuration->gamelaunch_custom_profilename)
			{
				g_configuration->custom_profile_current = g_configuration->custom_profile_list;

				while (g_configuration->custom_profile_current->spgameonly)
					g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;

				g_configuration->gamelaunch_custom_profilename = g_configuration->custom_profile_current->profilename;
			}

			l_game = GAME_CUSTOM;

			if (g_configuration->gamelaunch_custom_use_profilename)
				l_roomname = g_configuration->custom_profile_current->profilename;
			else
				l_roomname = g_configuration->gamelaunch_custom_roomname;

			if (g_configuration->gamelaunch_custom_use_nullmodem)
				l_max_num_players = 2;
			else
				l_max_num_players = g_configuration->gamelaunch_custom_numofplayers;

			l_connectiontype = g_configuration->gamelaunch_custom_use_nullmodem;
			l_maxping = g_configuration->gamelaunch_custom_maxping;
			l_rxdelay = g_configuration->gamelaunch_custom_rxdelay;
			l_args_for_host = g_configuration->custom_profile_current->extrahostargs;
#if EXTRA_ARGS_FOR_ALL
			l_args_for_all = g_configuration->custom_profile_current->extraallargs;
#endif
			l_allow_ingame_joining = g_configuration->custom_profile_current->ingamesupport;
		}
		else
			l_start_server = false;

		if (l_start_server)
		{
			l_launch_usermap = (!l_user_map.IsEmpty());

			g_host_frame = new HostedRoomFrame;
			if (g_host_frame->StartServer())
			{
				HideMainFrame();
#ifdef ENABLE_UPNP
				if (g_configuration->enable_upnp)
				{
					g_UPNP_Forward(g_configuration->game_port_number, false);
					g_UPNP_Forward(g_configuration->server_port_number, true);
				}
#endif
				g_host_frame->Show(true);
				g_host_frame->UpdateGameSettings(
					g_configuration->gamelaunch_use_password,
					g_configuration->gamelaunch_password,
					l_game,
					g_configuration->gamelaunch_source_port,
					l_use_crypticpassage,
					l_roomname,
					l_max_num_players,
					false,
					wxEmptyString,
					l_skill,
					g_configuration->gamelaunch_bloodgametype,
					g_configuration->gamelaunch_descentgametype,
					g_configuration->gamelaunch_descent2gametype,
					g_configuration->gamelaunch_dn3dgametype,
					g_configuration->gamelaunch_swgametype,
					g_configuration->gamelaunch_dn3dspawn,
					g_configuration->gamelaunch_blood_monster,
					g_configuration->gamelaunch_blood_weapon,
					g_configuration->gamelaunch_blood_item,
					g_configuration->gamelaunch_blood_respawn,
					l_args_for_host,
					l_args_for_all,
					l_maxping,
					l_rxdelay,
					l_launch_usermap,
					l_episode_map,
					l_user_map,
					l_modfiles,
					l_modname,
					l_modconfile,
					l_moddeffile,
					l_showmodurl,
					l_modurl,
					g_configuration->gamelaunch_advertise,
					l_connectiontype,
					l_allow_ingame_joining);
				g_host_frame->FocusFrame();
				//      g_host_frame->Raise();
			}
			else
			{
				wxMessageBox(wxString::Format(wxT("ERROR: Couldn't start server on port %d for an unknown reason.\nPossible reasons:\n* The configured server port number is already in use by another listener.\n* The server has disconnected a client by itself. At least in that case you might have to wait for about a minute before you can rehost on the same port.\n* Somehow a disconnection hasn't been done properly."), (int)g_configuration->server_port_number),
					wxT("Couldn't start listening"), wxOK|wxICON_EXCLAMATION);
				g_host_frame->Destroy();
				g_host_frame = NULL;
			}
		}
	}
	if (!l_start_server)
	{
		if (HAVE_BLOOD || HAVE_DESCENT || HAVE_DESCENT2 || HAVE_DN3D || HAVE_SW || (HAVE_CUSTOM && g_configuration->have_custom_mpprofile))
		{
			//    if (!g_mp_dialog)
			//    {
			HideMainFrame();
			g_mp_dialog = new MPDialog();
			g_mp_dialog->Show(true);
			g_mp_dialog->SetFocus();
			//    g_mp_dialog->Raise();
			//    }
			//    else
			//    g_mp_dialog->SetFocus();
		}
		else
			wxMessageBox(wxT("You should configure a source port\nso you can use one to launch the game,\naccessing Settings --> Source ports."),
			wxT("There are no configured source ports."), wxOK|wxICON_INFORMATION);
	}
}

void MainFrame::OnLoadManualJoinDialog(wxCommandEvent& WXUNUSED(event))
{
	if (HAVE_BLOOD || HAVE_DESCENT || HAVE_DESCENT2 || HAVE_DN3D || HAVE_SW || (HAVE_CUSTOM && g_configuration->have_custom_mpprofile))
	{
		//  if (g_manualjoin_dialog) // Shouldn't happen but for the case it does...
		//    g_manualjoin_dialog->SetFocus();
		//  else
		//  {
		//    g_manualjoin_dialog = new ManualJoinDialog;
		//    g_manualjoin_dialog->Show(true);
		//  }
		HideMainFrame();
		g_manualjoin_dialog = new ManualJoinDialog;
		g_manualjoin_dialog->Show(true);
		g_manualjoin_dialog->SetFocus();
		//  g_manualjoin_dialog->Raise();
	}
	else
		wxMessageBox(wxT("You should configure a source port\nso you can use one to launch the game,\naccessing Settings --> Source ports."),
		wxT("There are no configured source ports."), wxOK|wxICON_INFORMATION);
}

void MainFrame::OnLoadSRCPortsDialog(wxCommandEvent& WXUNUSED(event))
{
	//if (!g_srcports_dialog)
	//{
	HideMainFrame();
	g_srcports_dialog = new SRCPortsDialog;
	g_srcports_dialog->Show(true);
	g_srcports_dialog->SetFocus();
	//g_srcports_dialog->Raise();
	//}
	//else
	//  g_srcports_dialog->SetFocus();
}

void MainFrame::OnLoadTCsMODsDialog(wxCommandEvent& WXUNUSED(event))
{
	HideMainFrame();
	g_tcsmods_dialog = new TCsMODsDialog;
	g_tcsmods_dialog->Show(true);
	g_tcsmods_dialog->SetFocus();
}

void MainFrame::OnLoadNetworkDialog(wxCommandEvent& WXUNUSED(event))
{
	//if (!g_network_dialog)
	//{
	HideMainFrame();
	g_network_dialog = new NetworkDialog;
	g_network_dialog->Show(true);
	g_network_dialog->SetFocus();
	//g_network_dialog->Raise();
	//}
	//else
	//  g_network_dialog->SetFocus();
}
/*
void MainFrame::OnLoadLookAndFeelDialog(wxCommandEvent& WXUNUSED(event))
{
//if (!g_lookandfeel_dialog)
//{
HideMainFrame();
g_lookandfeel_dialog = new LookAndFeelDialog;
g_lookandfeel_dialog->Show(true);
g_lookandfeel_dialog->SetFocus();
//g_network_dialog->Raise();
//}
//else
//  g_network_dialog->SetFocus();
}
*/
void MainFrame::OnLoadAdvancedDialog(wxCommandEvent& WXUNUSED(event))
{
	//if (!g_adv_dialog)
	//{
	HideMainFrame();
	g_adv_dialog = new AdvDialog;
	g_adv_dialog->Show(true);
	g_adv_dialog->SetFocus();
	//g_adv_dialog->Raise();
	//}
	//else
	//  g_adv_dialog->SetFocus();
}

void MainFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
//	wxMessageBox((wxString)wxT("YANG (Yet Another Netplay Guider) v") + YANG_STR_VERSION + wxT(" by NY00123\nHighly inspired by Dukonnector, made by aaBlueDragon\nName suggestion with the help of ftugrul\nSeveral code contributions thanks to Turrican\nCompilation and testing for FreeBSD thanks to Olivier/Zozo\nCompilation and testing for Mac OS X thanks to Dopefish7590 and rhoenie\nOther people who helped in some way"), wxT("About YANG"), wxOK|wxICON_INFORMATION);
	wxMessageBox((wxString)
		wxT("YANG (Yet Another Netplay Guider) v") + YANG_STR_VERSION + wxT(", created by NY00123\n\n"
			"Used to be maintained by Turrican, and later in the past, also by StrikerTheHedgefox.\n\n"
			"The website and a master server had been hosted by Replica.\n\n"
			"Highly inspired by Dukonnector, a front-end created by aaBlueDragon, who was also briefly hosting a YANG master server.\n\n"
			"Name suggestion with the help of ftugrul.\n\n"
			"Compilation and testing for FreeBSD thanks to Olivier/Zozo.\n\n"
			"Compilation and testing for Mac OS X thanks to Dopefish7590, rhoenie and Turrican.\n\n"
			"Also, thanks to all other people who helped in some way!"), wxT("About YANG"), wxOK|wxICON_INFORMATION);
}

#if CHECK_FOR_UPDATES
void MainFrame::OnCheckForUpdates(wxCommandEvent& WXUNUSED(event))
{
	Disable();
	g_CheckForUpdates();
	Enable();
	//Raise();
	m_filtertreeCtrl->SetFocus();
}

void MainFrame::OnAutoCheckForUpdates(wxCommandEvent& WXUNUSED(event))
{
	g_configuration->check_for_updates_on_startup = !g_configuration->check_for_updates_on_startup;
	m_checkforupdatesonstartupmenuItem->Check(g_configuration->check_for_updates_on_startup);
	g_configuration->Save();
	/****** WORKAROUND: Why do we need to do this? ******/
	SetStatusText(wxEmptyString);
}
#endif

#if VIEW_YANG_NET_TERMS
void MainFrame::OnViewYANGTerms(wxCommandEvent& WXUNUSED(event))
{
	g_OpenURL(WEBSITE_LICENSE_TERMS);
}
#endif

#if VISIT_WEBSITE
void MainFrame::OnVisitWebsite(wxCommandEvent& WXUNUSED(event))
{
	g_OpenURL(WEBSITE_ADDRESS);
}
#endif

#if OPEN_FAQ
void MainFrame::OnOpenFAQ(wxCommandEvent& WXUNUSED(event))
{
//	g_OpenURL(WEBSITE_ADDRESS + wxT("files/yang/yangfaq/yangfaq.html"));
	g_OpenURL(WEBSITE_ADDRESS + wxT("yangfaq.html"));
}
#endif

void MainFrame::SaveUIControlSizes()
{
	g_configuration->main_col_private_len = m_serverlistCtrl->GetColumnWidth(COL_PRIVATE);
	g_configuration->main_col_loc_len = m_serverlistCtrl->GetColumnWidth(COL_LOCATION);
	g_configuration->main_col_roomname_len = m_serverlistCtrl->GetColumnWidth(COL_ROOM_NAME);
	g_configuration->main_col_ping_len = m_serverlistCtrl->GetColumnWidth(COL_PING);
	g_configuration->main_col_flux_len = m_serverlistCtrl->GetColumnWidth(COL_FLUX);
	g_configuration->main_col_game_len = m_serverlistCtrl->GetColumnWidth(COL_GAME);
	g_configuration->main_col_srcport_len = m_serverlistCtrl->GetColumnWidth(COL_SOURCE_PORT);
	g_configuration->main_col_players_len = m_serverlistCtrl->GetColumnWidth(COL_PLAYERS);
	g_configuration->main_col_map_len = m_serverlistCtrl->GetColumnWidth(COL_MAP);
	g_configuration->main_col_tcmod_len = m_serverlistCtrl->GetColumnWidth(COL_TC_MOD);
	g_configuration->main_col_gametype_len = m_serverlistCtrl->GetColumnWidth(COL_GAME_TYPE);
	g_configuration->main_col_skill_len = m_serverlistCtrl->GetColumnWidth(COL_SKILL);
	g_configuration->main_col_ingame_len = m_serverlistCtrl->GetColumnWidth(COL_INGAME);
	g_configuration->mainplayer_col_loc_len = m_playerlistCtrl->GetColumnWidth(PLAYERCOL_LOCATION);
	g_configuration->mainplayer_col_nick_len = m_playerlistCtrl->GetColumnWidth(PLAYERCOL_NICKNAME);
	g_configuration->mainplayer_col_os_len = m_playerlistCtrl->GetColumnWidth(PLAYERCOL_OS);
	g_configuration->main_is_maximized = IsMaximized();
	Hide();
	/*Maximize(false);
	Layout();*/
	if (!g_configuration->main_is_maximized)
		GetSize(&g_configuration->main_win_width, &g_configuration->main_win_height);
	g_configuration->Save();
}

void MainFrame::OnCloseWindow(wxCloseEvent& WXUNUSED(event))
{
	if (m_client)
	{
		if (m_client->IsConnected()) // If not yet connected, no timeout is set => hang!
			g_SendCStringToSocket(m_client, "1:disconnect:");
		m_client->Destroy();
		m_client = NULL;
	}
	// Otherwise, it's a sign we're changing theme and so already
	// saved sizes before (done in OnChangeTheme).
	if ((this == g_main_frame) || m_parent)
		SaveUIControlSizes();
	if (m_parent)
	{
		m_serverlist_timer->Stop();
		m_ping_timer->Stop();
	}
	else
		Destroy();
}

void MainFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
	Close();
}

void MainFrame::OnServerListAutoCheck(wxCommandEvent& WXUNUSED(event))
{
	g_configuration->lookup_on_startup = m_autolookupcheckBox->GetValue();
	g_configuration->Save();
}

void MainFrame::OnFilterSelect(wxTreeEvent& WXUNUSED(event))
{
	size_t l_loop_var;
	if (!first_time_filter_select)
	{
		if (m_filtertreeCtrl->GetSelection() == m_filtertreeCtrl->GetRootItem())
			g_configuration->game_filter_tree = wxT("all");
		else
		{
			for (l_loop_var = 0; l_loop_var < GAME_NUMOFGAMES; l_loop_var++)
				if (m_filtertreeCtrl->GetSelection() == m_games[l_loop_var].Id)
					g_configuration->game_filter_tree = m_games_code[l_loop_var];

			for (l_loop_var = 0; l_loop_var < SOURCEPORT_NUMOFPORTS; l_loop_var++)
				if (m_filtertreeCtrl->GetSelection() == m_ports[l_loop_var].Id)
					g_configuration->game_filter_tree = m_ports_code[l_loop_var];
		}
		g_configuration->Save();
		RefreshList();
	}
	else
		first_time_filter_select--;
}

void MainFrame::OnHostSelect(wxListEvent& event)
{
	// Thanks to the help of the following Wiki entry:
	// http://wiki.wxwidgets.org/WxListCtrl

	long l_index = event.GetIndex();
	size_t l_loop_var, l_num_entries;
	m_playerlistCtrl->DeleteAllItems();
	m_playerImageList->RemoveAll();
	// If haven't received all data or nothing is actually selected, avoid crash!
	if ((l_index < 0) || (m_players.GetCount() <= (unsigned)l_index))
		return;
	l_num_entries = m_players[l_index].GetCount();

	const char * returnedCountry;

	for (l_loop_var = 0; l_loop_var < l_num_entries; l_loop_var += 3)
	{
		// Need to add icon before inserting item
		if (g_geoip_db != NULL)
		{
			returnedCountry = GeoIP_country_code_by_addr(g_geoip_db,m_players[l_index][l_loop_var+1].mb_str(wxConvUTF8));
			if (returnedCountry != NULL)
				m_playerImageList->Add(getFlag(returnedCountry));
			else
				m_playerImageList->Add(wxIcon(unknown));
		}

		m_playerlistCtrl->InsertItem(l_loop_var/3, l_loop_var/3);
		m_playerlistCtrl->SetItem(l_loop_var/3, PLAYERCOL_NICKNAME, m_players[l_index][l_loop_var]);
		if (g_configuration->theme_is_dark)
			m_playerlistCtrl->SetItemTextColour(l_loop_var/3, *wxGREEN);

		if (g_geoip_db != NULL)
			m_playerlistCtrl->SetItem(l_loop_var/3, PLAYERCOL_LOCATION, wxString(GeoIP_country_name_by_addr(g_geoip_db,m_players[l_index][l_loop_var+1].mb_str(wxConvUTF8)), wxConvUTF8));

		m_playerlistCtrl->SetItem(l_loop_var/3, PLAYERCOL_OS, m_players[l_index][l_loop_var+2]);
	}
}

void MainFrame::OnHostActivate(wxListEvent& WXUNUSED(event))
{
	if (!m_parent)
		Join();
}
/*
void MainFrame::OnJoin(wxCommandEvent& WXUNUSED(event))
{
Join();
}
*/
void MainFrame::Join()
{
	//long l_port_num;
	long l_index = m_serverlistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
	// If haven't received all data or nothing is actually selected, avoid crash!
	if ((l_index < 0) || (m_addrs.GetCount() <= (unsigned)l_index))
		return;
	wxString l_addr = m_addrs[l_index];
	//wxString l_addr = m_manualjoinhostaddrtextCtrl->GetValue();
	if (l_addr.IsEmpty())
		return;
	//if ((m_manualjoinportnumtextCtrl->GetValue()).ToLong(&l_port_num))
	//{
	g_client_frame = new ClientRoomFrame;
	Disable();
	Update();
	//  g_client_frame->ConnectToServer(l_addr, l_port_num);
	//  if (g_client_frame->ConnectToServer(l_addr, l_port_num))
	if (g_client_frame->ConnectToServer(l_addr, m_portnums[l_index]))
	{
		g_client_frame->Show(true);
		g_client_frame->FocusFrame();
		//    g_client_frame->SetFocus();
		//    g_client_frame->Raise();
		HideMainFrame();
		Enable();
	}
	else
	{
		Enable();
		wxMessageBox(wxT("Connection establishment has failed."), wxEmptyString, wxOK|wxICON_INFORMATION);
		g_client_frame->Destroy();
		SetFocus();
	}
	//}
	//else
	//  wxMessageBox(wxT("The given port number seems to be invalid.\nIs it really a number?"), wxT("Invalid port number"), wxOK|wxICON_INFORMATION);
}
/*
void MainFrame::OnAddHost(wxCommandEvent& WXUNUSED(event))
{
long l_temp_num;
g_configuration->manualjoin_names.Add(m_manualjoinnametextCtrl->GetValue());
g_configuration->manualjoin_addrs.Add(m_manualjoinhostaddrtextCtrl->GetValue());
if (!((m_manualjoinportnumtextCtrl->GetValue()).ToLong(&l_temp_num)))
l_temp_num = g_configuration->server_port_number;
g_configuration->manualjoin_portnums.Add(l_temp_num);
{
m_manualjoinchoice->Append(m_manualjoinnametextCtrl->GetValue() +
wxT(' ') + m_manualjoinhostaddrtextCtrl->GetValue() +
wxString::Format(wxT(":%d"), (int)l_temp_num));
m_manualjoinchoice->SetSelection(m_manualjoinchoice->GetCount()-1);
}
g_configuration->Save();
}

void MainFrame::OnRemHost(wxCommandEvent& WXUNUSED(event))
{
int l_selection = m_manualjoinchoice->GetSelection();
if (l_selection >= 0)
{
g_configuration->manualjoin_names.RemoveAt(l_selection);
g_configuration->manualjoin_addrs.RemoveAt(l_selection);
g_configuration->manualjoin_portnums.RemoveAt(l_selection);
m_manualjoinchoice->Delete(l_selection);
m_manualjoinchoice->SetSelection(l_selection);
g_configuration->Save();
}
}

void MainFrame::OnManualJoinHostSelect(wxCommandEvent& event)
{
int l_selection = event.GetSelection();
m_manualjoinnametextCtrl->SetValue(g_configuration->manualjoin_names[l_selection]);
m_manualjoinhostaddrtextCtrl->SetValue(g_configuration->manualjoin_addrs[l_selection]);
m_manualjoinportnumtextCtrl->SetValue(wxString::Format(wxT("%d"),(int)g_configuration->manualjoin_portnums[l_selection]));
}
*/
void MainFrame::OnSocketEvent(wxSocketEvent& event)
{
	size_t l_num_of_items, l_loop_var, l_num_of_read_attempts;
	unsigned long l_temp_num;
	wxArrayString l_str_list;
	wxSocketBase* l_sock = event.GetSocket();
	//wxListItem l_list_item;
	const char * returnedCountry;

	if (l_sock == m_client)
	{
		switch (event.GetSocketEvent())
		{
			case wxSOCKET_CONNECTION:
				m_client->SetTimeout(1);
				SetStatusText(wxT("Getting list of rooms..."));
				l_str_list.Add(wxT("getroomlist"));
				l_str_list.Add(YANGMASTER_STR_NET_VERSION);

				if (m_filtertree_current_selection != m_filtertreeCtrl->GetRootItem())
				{
					for (l_loop_var = 0; l_loop_var < GAME_NUMOFGAMES; l_loop_var++)
						if (m_filtertree_current_selection == m_games[l_loop_var].Id)
							l_str_list.Add(m_games_code[l_loop_var]);

					for (l_loop_var = 0; l_loop_var < SOURCEPORT_NUMOFPORTS; l_loop_var++)
						if (m_filtertree_current_selection == m_ports[l_loop_var].Id)
							l_str_list.Add(m_ports_code[l_loop_var]);
				}

				g_SendStringListToSocket(m_client, l_str_list);
				m_serverlist_timer->Start(5000, true);
				m_long_buffer = wxEmptyString;
				break;
			case wxSOCKET_INPUT:
				l_num_of_read_attempts = 0;
				do
				{
					m_client->Read(m_short_buffer, MAX_COMM_TEXT_LENGTH);
					/*        if (l_sock->Error())
					{
					AddMessage(wxString::Format(wxT("* A communication error has occured while trying to fetch data: %d"), (int)l_sock->LastError()));
					if (m_allow_sounds && g_configuration->play_snd_error)
					g_PlaySound(g_configuration->snd_error_file);
					break;
					}*/
					l_temp_num = l_sock->LastCount();
					if (l_temp_num < MAX_COMM_TEXT_LENGTH)
						l_num_of_read_attempts = MAX_COMM_READ_ATTEMPTS;
					else
						l_num_of_read_attempts++;
					m_long_buffer << wxString(m_short_buffer, wxConvUTF8, l_temp_num);
					l_num_of_items = g_ConvertStringBufToStringList(m_long_buffer, l_str_list);
					while (l_num_of_items)
					{
						if ((l_num_of_items == 2) && (l_str_list[0] == wxT("error")))
						{
							m_serverlist_timer->Stop();
							g_SendCStringToSocket(m_client, "1:disconnect:");
							m_client->Destroy();
							m_client = NULL;
							if (wxMessageBox(wxT("Error from server list:\n") + l_str_list[1]
							+ wxT("\n\nWould you like to check the rest of the server lists (if any)?"),
								wxT("Error from master server"), wxYES_NO|wxICON_EXCLAMATION) == wxYES)
								TryNextServer();            
							else
								StartPinging();
						}
						else if ((l_num_of_items == SOURCEPORT_NUMOFPORTS+1) && (l_str_list[0] == wxT("countbyport")))
						{
							for (l_loop_var = 0; l_loop_var < SOURCEPORT_NUMOFPORTS; l_loop_var++)
							{
								l_str_list[l_loop_var+1].ToULong(&l_temp_num);

								m_ports[l_loop_var].num_of_rooms += l_temp_num;

								m_games[game_ranges[l_loop_var]].num_of_rooms += l_temp_num;
							}
						}
						else if ((l_num_of_items == 4) && (l_str_list[0] == wxT("player")))
						{
							m_curr_room--;
							// Decrypt IP Address
							l_str_list[2] = g_DecryptFromCommText(l_str_list[2], COMM_KEY_IPLIST_FROM_MASTER_TO_MAINWIN); 

							m_players[m_curr_room].Add(l_str_list[1]); // Nickname
							m_players[m_curr_room].Add(l_str_list[2]); // IP Address (used for country flag)
							m_players[m_curr_room].Add(l_str_list[3]); // Operating System
							m_serverlistCtrl->SetItem(m_curr_room, COL_PLAYERS,
								wxString::Format(wxT("%d/"), (int)m_players[m_curr_room].GetCount()/3)
								+ m_numplayers_limits[m_curr_room]);
							//              RefreshListCtrlSize();
							m_curr_room++;
						}
						else if ((l_num_of_items == 17) && (l_str_list[0] == wxT("info")))
						{
							// Decrypt IP Addresses
							l_str_list[1] = g_DecryptFromCommText(l_str_list[1], COMM_KEY_IPLIST_FROM_MASTER_TO_MAINWIN); 
							l_str_list[2] = g_DecryptFromCommText(l_str_list[2], COMM_KEY_IPLIST_FROM_MASTER_TO_MAINWIN); 

							// TO BE ADDED: l_str_list[4] tells "yes" if password protected, "no" otherwise.
							// For this version it should always tell "no".
							m_addrs.Add(l_str_list[1]);
							l_str_list[3].ToULong(&l_temp_num);
							m_portnums.Add(l_temp_num);
							m_players.Add(wxArrayString());
							//                l_list_item.SetColumn(0);
							//                l_list_item.SetTextColour(*wxGREEN);
							//                l_list_item.SetText(l_str_list[7]);
							//                m_serverlistCtrl->InsertItem(l_list_item);

							m_serverlistCtrl->InsertItem(m_curr_room, l_str_list[5] == wxT("yes") ? IMG_LOCK : IMG_BLANK);

							if (g_geoip_db != NULL)
							{
								returnedCountry = GeoIP_country_code_by_addr(g_geoip_db,l_str_list[2].mb_str(wxConvUTF8));
								if (returnedCountry != NULL)
									m_ImageList->Add(getFlag(returnedCountry));
								else
									m_ImageList->Add(wxIcon(unknown));

								m_serverlistCtrl->SetItem(m_curr_room, COL_LOCATION, wxString(GeoIP_country_name_by_addr(g_geoip_db,l_str_list[2].mb_str(wxConvUTF8)), wxConvUTF8), m_curr_room+IMG_NUMBER);
							}

							m_serverlistCtrl->SetItem(m_curr_room, COL_ROOM_NAME, l_str_list[7]);
							if (l_str_list[4] != YANG_STR_NET_VERSION)
								m_serverlistCtrl->SetItemTextColour(m_curr_room, *wxRED);
							else if (g_configuration->theme_is_dark)
								m_serverlistCtrl->SetItemTextColour(m_curr_room, *wxGREEN);
							m_serverlistCtrl->SetItem(m_curr_room, COL_PLAYERS, wxT("0/") + l_str_list[8]);
							m_numplayers_limits.Add(l_str_list[8]);
							if (l_str_list[9] == wxT("yes"))
								m_serverlistCtrl->SetItem(m_curr_room, COL_INGAME, wxT("Yes"));
							else
								m_serverlistCtrl->SetItem(m_curr_room, COL_INGAME, wxT("No"));
							if (l_str_list[6] == GAMECODE_BLOOD)
							{
								m_serverlistCtrl->SetItem(m_curr_room, COL_GAME, GAMENAME_BLOOD);
								if (l_str_list[10] == SRCPORTCODE_DOSBLOODSW)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSBLOODSW);
								else if (l_str_list[10] == SRCPORTCODE_DOSBLOODRG)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSBLOODRG);
								else if (l_str_list[10] == SRCPORTCODE_DOSBLOODPP)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSBLOODPP);
								else
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSBLOODOU);
								l_str_list[11].ToULong(&l_temp_num);
								if (l_temp_num > BLOOD_MAX_SKILLNUM)
									l_temp_num = 0;
								m_serverlistCtrl->SetItem(m_curr_room, COL_SKILL, (*g_blood_skill_list)[l_temp_num]);
								if (l_str_list[12] == wxT("coop"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_blood_gametype_list)[BLOODMPGAMETYPE_COOP-1]);
								else if (l_str_list[12] == wxT("bb"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_blood_gametype_list)[BLOODMPGAMETYPE_BB-1]);
								else
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_blood_gametype_list)[BLOODMPGAMETYPE_TEAMS-1]);
								if (l_str_list[13] == wxT("usermap"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, l_str_list[14]);
								else
								{
									l_str_list[14].ToULong(&l_temp_num);

									if (l_str_list[16] == wxT("no"))
										m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, (*g_blood_level_list)[g_GetBloodLevelSelectionByNum(l_temp_num)]);
									else
										m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, (*g_blood_cp_level_list)[g_GetBloodCPLevelSelectionByNum(l_temp_num)]);
								}

								if (l_str_list[16] == wxT("yes"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_TC_MOD, wxT("Cryptic Passage"));
								else
									m_serverlistCtrl->SetItem(m_curr_room, COL_TC_MOD, l_str_list[15]);
							}
							else if (l_str_list[6] == GAMECODE_DESCENT)
							{
								m_serverlistCtrl->SetItem(m_curr_room, COL_GAME, GAMENAME_DESCENT);
								if (l_str_list[10] == SRCPORTCODE_DOSDESCENT)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSDESCENT);
								else
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_D1XREBIRTH);

								if (l_str_list[12] == wxT("anarchy"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent_gametype_list)[DESCENTMPGAMETYPE_ANARCHY]);
								else if (l_str_list[12] == wxT("team_anarchy"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent_gametype_list)[DESCENTMPGAMETYPE_TEAM_ANARCHY]);
								else if (l_str_list[12] == wxT("robo_anarchy"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent_gametype_list)[DESCENTMPGAMETYPE_ROBO_ANARCHY]);
								else
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent_gametype_list)[DESCENTMPGAMETYPE_COOPERATIVE]);
								if (l_str_list[13] == wxT("usermap"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, l_str_list[14]);
								else
								{
									l_str_list[14].ToULong(&l_temp_num);
									m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, (*g_descent_level_list)[l_temp_num]);
								}
							}
							else if (l_str_list[6] == GAMECODE_DESCENT2)
							{
								m_serverlistCtrl->SetItem(m_curr_room, COL_GAME, GAMENAME_DESCENT2);
								if (l_str_list[10] == SRCPORTCODE_DOSDESCENT2)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSDESCENT2);
								else
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_D2XREBIRTH);

								if (l_str_list[12] == wxT("anarchy"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent2_gametype_list)[DESCENT2MPGAMETYPE_ANARCHY]);
								else if (l_str_list[12] == wxT("team_anarchy"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent2_gametype_list)[DESCENT2MPGAMETYPE_TEAM_ANARCHY]);
								else if (l_str_list[12] == wxT("robo_anarchy"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent2_gametype_list)[DESCENT2MPGAMETYPE_ROBO_ANARCHY]);
								else if (l_str_list[12] == wxT("cooperative"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent2_gametype_list)[DESCENT2MPGAMETYPE_COOPERATIVE]);
								else if (l_str_list[12] == wxT("capture_the_flag"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent2_gametype_list)[DESCENT2MPGAMETYPE_CAPTURE_THE_FLAG]);
								else if (l_str_list[12] == wxT("hoard"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent2_gametype_list)[DESCENT2MPGAMETYPE_HOARD]);
								else
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_descent2_gametype_list)[DESCENT2MPGAMETYPE_TEAM_HOARD]);
								if (l_str_list[13] == wxT("usermap"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, l_str_list[14]);
								else
								{
									l_str_list[14].ToULong(&l_temp_num);
									m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, (*g_descent2_level_list)[l_temp_num]);
								}
							}
							else if (l_str_list[6] == GAMECODE_DN3D)
							{
								m_serverlistCtrl->SetItem(m_curr_room, COL_GAME, GAMENAME_DN3D);
								if (l_str_list[10] == SRCPORTCODE_DOSDUKESW)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSDUKESW);
								else if (l_str_list[10] == SRCPORTCODE_DOSDUKERG)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSDUKERG);
								else if (l_str_list[10] == SRCPORTCODE_DOSDUKEAE)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSDUKEAE);
								else if (l_str_list[10] == SRCPORTCODE_DUKE3DW)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DUKE3DW);
								else if (l_str_list[10] == SRCPORTCODE_EDUKE32)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_EDUKE32);
								else if (l_str_list[10] == SRCPORTCODE_NDUKE)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_NDUKE);
								else if (l_str_list[10] == SRCPORTCODE_HDUKE)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_HDUKE);
								else
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_XDUKE);
								l_str_list[11].ToULong(&l_temp_num);
								if (l_temp_num > DN3D_MAX_SKILLNUM)
									l_temp_num = 0;
								m_serverlistCtrl->SetItem(m_curr_room, COL_SKILL, (*g_dn3d_skill_list)[l_temp_num]);
								if (l_str_list[12] == wxT("dmspawn"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_dn3d_gametype_list)[DN3DMPGAMETYPE_DMSPAWN-1]);
								else if (l_str_list[12] == wxT("coop"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_dn3d_gametype_list)[DN3DMPGAMETYPE_COOP-1]);
								else if (l_str_list[12] == wxT("dmnospawn"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_dn3d_gametype_list)[DN3DMPGAMETYPE_DMNOSPAWN-1]);
								else if (l_str_list[12] == wxT("tdmspawn"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_dn3d_gametype_list)[DN3DMPGAMETYPE_TDMSPAWN-1]);
								else
									m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_dn3d_gametype_list)[DN3DMPGAMETYPE_TDMNOSPAWN-1]);
								if (l_str_list[13] == wxT("usermap"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, l_str_list[14]);
								else
								{
									l_str_list[14].ToULong(&l_temp_num);
									m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, (*g_dn3d_level_list)[g_GetDN3DLevelSelectionByNum(l_temp_num)]);
								}
								m_serverlistCtrl->SetItem(m_curr_room, COL_TC_MOD, l_str_list[15]);
							}
							else if (l_str_list[6] == GAMECODE_SW)
							{
								m_serverlistCtrl->SetItem(m_curr_room, COL_GAME, GAMENAME_SW);
								if (l_str_list[10] == SRCPORTCODE_DOSSWSW)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSSWSW);
								else if (l_str_list[10] == SRCPORTCODE_DOSSWRG)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_DOSSWRG);
								else if (l_str_list[10] == SRCPORTCODE_VOIDSW)
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_VOIDSW);
								else
									m_serverlistCtrl->SetItem(m_curr_room, COL_SOURCE_PORT, SRCPORTNAME_SWP);
								l_str_list[11].ToULong(&l_temp_num);
								if (l_temp_num > SW_MAX_SKILLNUM)
									l_temp_num = 0;
								m_serverlistCtrl->SetItem(m_curr_room, COL_SKILL, (*g_sw_skill_list)[l_temp_num]);

								if (l_str_list[10] == SRCPORTCODE_SWP)
								{
									if (l_str_list[12] == wxT("wbspawn"))
										m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_sw_gametype_list)[SWMPGAMETYPE_WBSPAWN]);
									else if (l_str_list[12] == wxT("wbnospawn"))
										m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_sw_gametype_list)[SWMPGAMETYPE_WBNOSPAWN]);
									else
										m_serverlistCtrl->SetItem(m_curr_room, COL_GAME_TYPE, (*g_sw_gametype_list)[SWMPGAMETYPE_COOP]);
								}

								if (l_str_list[13] == wxT("usermap"))
									m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, l_str_list[14]);
								else
								{
									l_str_list[14].ToULong(&l_temp_num);
									m_serverlistCtrl->SetItem(m_curr_room, COL_MAP, (*g_sw_level_list)[g_GetSWLevelSelectionByNum(l_temp_num)]);
								}
								m_serverlistCtrl->SetItem(m_curr_room, COL_TC_MOD, l_str_list[15]);
							}
							else if (l_str_list[6] == GAMECODE_CUSTOM)
							{
								m_serverlistCtrl->SetItem(m_curr_room, COL_GAME, GAMENAME_CUSTOM);
							}
							//                RefreshListCtrlSize();
							m_curr_room++;
							m_serverlist_timer->Start(5000, true);
						}
						l_num_of_items = g_ConvertStringBufToStringList(m_long_buffer, l_str_list);
					}
				}
				while (l_num_of_read_attempts < MAX_COMM_READ_ATTEMPTS);
				break;
			case wxSOCKET_LOST:
				m_serverlist_timer->Stop();
				TryNextServer();

				/*      if (m_curr_room)
				{
				SetStatusText(wxT("Pinging list of rooms..."));
				PingServer();
				m_room_num++;
				}
				else
				m_getlistbutton->Enable();*/
			default: ;
		}
	}
}

void MainFrame::OnPingEvent(wxSocketEvent& event)
{
	size_t l_num_of_items;
	wxArrayString l_str_list, l_str_ping;
	wxSocketBase* l_sock = event.GetSocket();
	//wxListItem l_list_item;
	if (l_sock == m_client)
	{
		switch (event.GetSocketEvent())
		{
			case wxSOCKET_CONNECTION:
				m_client->SetTimeout(1);
				m_ping_id = rand();
				l_str_ping.Empty();
				l_str_ping.Add(wxT("pingquery"));
				l_str_ping.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
				g_SendStringListToSocket(m_client, l_str_ping);
				m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
				ping.Start( 0 );
				m_long_buffer = wxEmptyString;
				break;
			case wxSOCKET_INPUT:
				m_client->Read(m_short_buffer, MAX_COMM_TEXT_LENGTH);
				m_long_buffer << wxString(m_short_buffer, wxConvUTF8, m_client->LastCount());
				l_num_of_items = g_ConvertStringBufToStringList(m_long_buffer, l_str_list);

				if ((l_num_of_items == 2) && (l_str_list[0] == wxT("pingreply")) && (l_str_list[1] == wxString::Format(wxT("%d"), (int)m_ping_id)))
				{
					ping.Pause();
					m_ping_timer->Stop();

					if (ping.Time() < m_min_ping)
						m_min_ping = ping.Time();

					if (ping.Time() > m_max_ping)
						m_max_ping = ping.Time();

					m_ping_times++;
				}

				if (m_ping_times < 4)
				{
					m_ping_id = rand();
					l_str_ping.Empty();
					l_str_ping.Add(wxT("pingquery"));
					l_str_ping.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
					g_SendStringListToSocket(m_client, l_str_ping);
					m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
					ping.Start( 0 );
				}
				else
				{
					m_serverlistCtrl->SetItem(m_room_num -1, COL_PING, wxString::Format(wxT("%d"), (int)(m_max_ping < 999 ? m_max_ping : 999)));

					if (m_max_ping < 999)
						m_serverlistCtrl->SetItem(m_room_num -1, COL_FLUX, wxString::Format(wxT("%c"), 177) + wxString::Format(wxT("%d"), (int)(m_max_ping-m_min_ping <= 99 ? m_max_ping-m_min_ping : 99)));

#if (defined __WXMAC__) || (defined __WXCOCOA__)
					m_serverlistCtrl->RefreshItem(m_room_num -1);
#endif
					m_client->Destroy();
					m_client = NULL;

					if (m_room_num < m_curr_room)
					{
						PingServer();
						m_room_num++;
					}
					else
					{
						SetStatusText(wxEmptyString);
						m_getlistbutton->Enable();
					}
				}
				break;
			case wxSOCKET_LOST:
				m_ping_timer->Stop();
				m_serverlistCtrl->SetItem(m_room_num -1, COL_PING, wxT("999"));
#if (defined __WXMAC__) || (defined __WXCOCOA__)
				m_serverlistCtrl->RefreshItem(m_room_num -1);
#endif
				m_client->Destroy();
				m_client = NULL;

				if (m_room_num < m_curr_room)
				{
					PingServer();
					m_room_num++;
				}
				else
				{
					SetStatusText(wxEmptyString);
					m_getlistbutton->Enable();
				}
			default: ;
		}
	}
}

void MainFrame::OnTimer(wxTimerEvent& WXUNUSED(event))
{
	if ((m_client != NULL) && (m_servers_to_count)) // A few optimizations, and
		TryNextServer(); // possible race conditions (although not catching all cases).
	else
		m_getlistbutton->Enable(); // Just in case of some race condition.
}

void MainFrame::OnPingTimer(wxTimerEvent& WXUNUSED(event))
{
	if ((m_curr_room > 0) && (m_client != NULL)) // POSSIBLE RACE CONDITIONS;
	{                                            // Like, check we haven't just restarted something.
		m_serverlistCtrl->SetItem(m_room_num -1, COL_PING, wxT("999"));
#if (defined __WXMAC__) || (defined __WXCOCOA__)
		m_serverlistCtrl->RefreshItem(m_room_num -1);
#endif
		m_client->Destroy();
		m_client = NULL;

		if (m_room_num < m_curr_room)
		{
			PingServer();
			m_room_num++;
		}
		else
		{
			SetStatusText(wxEmptyString);
			m_getlistbutton->Enable();
		}
	}
}

void MainFrame::Connect()
{
	wxIPV4address l_addr;
	m_client = new wxSocketClient;
	if (m_check_default_lists)
	{
		l_addr.Hostname((*g_serverlists_addrs)[m_server_in_curr_list]);
		l_addr.Service((*g_serverlists_portnums)[m_server_in_curr_list]);
		SetStatusText(wxString::Format(wxT("Connecting to master server %lu..."),
			(unsigned long)(m_server_in_curr_list+1+g_configuration->extraservers_portnums.GetCount())));
	}
	else
	{
		l_addr.Hostname(g_configuration->extraservers_addrs[m_server_in_curr_list]);
		l_addr.Service(g_configuration->extraservers_portnums[m_server_in_curr_list]);
		SetStatusText(wxString::Format(wxT("Connecting to master server %lu..."), (unsigned long)(m_server_in_curr_list+1)));
	}
	m_client->SetTimeout(3);
	m_client->SetEventHandler(*this, ID_GETLISTSOCKET);
	m_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
	m_client->SetNotify(wxSOCKET_CONNECTION_FLAG |
		wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
	m_client->Notify(true);

	m_client->Connect(l_addr, false);
	m_serverlist_timer->Start(5000, true);
}

void MainFrame::StartPinging()
{
	size_t l_loop_var, l_total_num_of_rooms = 0;

	for (l_loop_var = 0; l_loop_var < SOURCEPORT_NUMOFPORTS; l_loop_var++)
	{
		if (l_loop_var != SOURCEPORT_CUSTOM)
			m_filtertreeCtrl->SetItemText(m_ports[l_loop_var].Id, m_ports_name[l_loop_var] + wxString::Format(wxT(" (%ld)"), (long)m_ports[l_loop_var].num_of_rooms));

		l_total_num_of_rooms += m_ports[l_loop_var].num_of_rooms;
	}

	for (l_loop_var = 0; l_loop_var < GAME_NUMOFGAMES; l_loop_var++)
		m_filtertreeCtrl->SetItemText(m_games[l_loop_var].Id, m_games_name[l_loop_var] + wxString::Format(wxT(" (%ld)"), (long)m_games[l_loop_var].num_of_rooms));

	m_filtertreeCtrl->SetItemText(m_filtertreeCtrl->GetRootItem(), wxString::Format(wxT("All Games (%lu)"), (unsigned long)l_total_num_of_rooms));

	if (m_curr_room)
	{
		SetStatusText(wxT("Pinging list of rooms..."));
		PingServer();
		m_room_num++;
	}
	else
	{
		SetStatusText(wxEmptyString);
		m_client = NULL;
		m_getlistbutton->Enable();
	}
}

void MainFrame::TryNextServer()
{
	if (m_client != NULL) // For the case of an error message popped-up before.
	{
		if (m_client->IsConnected()) // If not yet connected, no timeout is set => hang!
			g_SendCStringToSocket(m_client, "1:disconnect:");
		m_client->Destroy();
	}
	m_serverlist_timer->Stop();
	if (m_check_default_lists)
	{
		m_server_in_curr_list++;
		if (m_server_in_curr_list < m_servers_to_count)
			Connect();
		else
			StartPinging();
	}
	else
	{
		m_server_in_curr_list++;
		if (m_server_in_curr_list < m_servers_to_count)
			Connect();
		else
		{
			m_servers_to_count = g_serverlists_portnums->GetCount();
			if (m_servers_to_count)
			{
				m_server_in_curr_list = 0;
				m_check_default_lists = true;
				Connect();
			}
			else
				StartPinging();
		}
	}
}

void MainFrame::RefreshList()
{
	size_t l_loop_var;

	m_filtertree_current_selection = m_filtertreeCtrl->GetSelection();

	for (l_loop_var = 0; l_loop_var < GAME_NUMOFGAMES; l_loop_var++)
		m_games[l_loop_var].num_of_rooms = 0;

	for (l_loop_var = 0; l_loop_var < SOURCEPORT_NUMOFPORTS; l_loop_var++)
		m_ports[l_loop_var].num_of_rooms = 0;

	m_getlistbutton->Disable();
	m_serverlistCtrl->DeleteAllItems();
	m_ImageList->RemoveAll();
	wxMemoryInputStream istream_lock(lock_png, sizeof lock_png);
	m_ImageList->Add(wxImage(istream_lock, wxBITMAP_TYPE_PNG));
	m_ImageList->Add(wxIcon(unknown));
	//RefreshListCtrlSize();
	m_playerlistCtrl->DeleteAllItems();
	//m_playerlistBox->Clear();
	m_players.Empty();
	m_addrs.Empty();
	m_numplayers_limits.Empty();
	m_portnums.Empty();
	m_ping_timer->Stop();
	if (m_client)
	{
		if (m_client->IsConnected()) // If not yet connected, no timeout is set => hang!
			g_SendCStringToSocket(m_client, "1:disconnect:");
		m_client->Destroy();
	}
	m_server_in_curr_list = m_curr_room = m_room_num/* = m_last_server*/ = 0;
	m_servers_to_count = g_configuration->extraservers_portnums.GetCount();
	m_check_default_lists = (!m_servers_to_count);
	if (m_check_default_lists)
	{
		m_servers_to_count = g_serverlists_portnums->GetCount();
		if (m_servers_to_count)
			Connect();
		else
		{
			wxLogError(wxT("ERROR: The list of master servers seems to be empty."));
			SetStatusText(wxEmptyString);
			m_getlistbutton->Enable();
		}
	}
	else
		Connect();
}

void MainFrame::PingServer()
{
	wxIPV4address l_addr;
	l_addr.Hostname(m_addrs[m_room_num]);
	l_addr.Service(m_portnums[m_room_num]);

	m_ping_times = m_max_ping = 0;
	m_min_ping = 999;

	m_client = new wxSocketClient();

	m_client->SetEventHandler(*this, ID_GETPING);
	m_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
	m_client->SetNotify(wxSOCKET_CONNECTION_FLAG | wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
	m_client->Notify(true);
	m_client->Connect(l_addr, false);
	m_ping_timer->Start(2000, wxTIMER_ONE_SHOT);
}

void MainFrame::OnGetList(wxCommandEvent& WXUNUSED(event))
{
	RefreshList();
}

void MainFrame::ShowMainFrame()
{
#if (defined __WXMAC__) || (defined __WXCOCOA__)
	SetMenuBar(m_Mainmenubar);
#endif
	//m_manualjoinportnumtextCtrl->SetValue(wxString::Format(wxT("%d"), (int)g_configuration->server_port_number));
	//m_manualjoinchoice->SetSelection(0);
	Show(true);
	//Raise();
	m_filtertreeCtrl->SetFocus();
	//m_getlistbutton->SetFocus();
	//m_joinbutton->SetFocus();
	if (g_configuration->lookup_on_startup)
		RefreshList();
	else
		m_getlistbutton->Enable(); // If it has been disabled before.
}

void MainFrame::HideMainFrame()
{
#if (defined __WXMAC__) || (defined __WXCOCOA__)
	SetMenuBar(g_empty_menubar);
#endif
	Hide();
	//m_getlistbutton->Enable();
	if (m_client)
	{
		if (m_client->IsConnected()) // If not yet connected, no timeout is set => hang!
			g_SendCStringToSocket(m_client, "1:disconnect:");
		m_client->Destroy();
	}
	m_client = NULL;
	//m_manualjoinnametextCtrl->Clear();
	//m_manualjoinhostaddrtextCtrl->Clear();
	m_serverlistCtrl->DeleteAllItems();
	//RefreshListCtrlSize();
	m_playerlistCtrl->DeleteAllItems();
	//m_playerlistBox->Clear();
	SetStatusText(wxEmptyString);
	m_ping_timer->Stop();
	m_serverlist_timer->Stop();
}
