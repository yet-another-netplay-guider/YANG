/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/filename.h>
#include <wx/msgdlg.h>
#include <wx/textfile.h>
#include <wx/gbsizer.h>
#endif

#include "sp_dialog.h"
#include "config.h"
#include "mp_common.h"
#include "yang.h"
#include "theme.h"
#include "yang.xpm"

BEGIN_EVENT_TABLE(SPDialog, YANGDialog)

EVT_BUTTON(wxID_OK, SPDialog::OnOk)
EVT_BUTTON(wxID_CANCEL, SPDialog::OnCancel)

EVT_CHOICE(ID_SPBLOODSELECTSRCPORT, SPDialog::OnSrcPortSelect)
EVT_CHOICE(ID_SPDESCENTSELECTSRCPORT, SPDialog::OnSrcPortSelect)
EVT_CHOICE(ID_SPDESCENT2SELECTSRCPORT, SPDialog::OnSrcPortSelect)
EVT_CHOICE(ID_SPDN3DSELECTSRCPORT, SPDialog::OnSrcPortSelect)
EVT_CHOICE(ID_SPSWSELECTSRCPORT, SPDialog::OnSrcPortSelect)

EVT_CHOICE(ID_SPBLOODSELECTGAMETYPE, SPDialog::OnGameTypeSelect)
EVT_CHOICE(ID_SPDN3DSELECTGAMETYPE, SPDialog::OnGameTypeSelect)

EVT_CHOICE(ID_SPSELECTCUSTOMPROFILE, SPDialog::OnSelectCustomProfile)

EVT_CHECKBOX(ID_SPDN3DPLAYDEMO, SPDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SPSWPLAYDEMO, SPDialog::OnCheckBoxClick)

EVT_CHECKBOX(ID_SPBLOODRECORDDEMO, SPDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SPDN3DRECORDDEMO, SPDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SPSWRECORDDEMO, SPDialog::OnCheckBoxClick)

EVT_CHECKBOX(ID_SPBLOODUSECRYPTICPASSAGE, SPDialog::OnCheckBoxClick)

EVT_RADIOBUTTON(ID_SPBLOODUSEEPIMAP, SPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_SPDESCENTUSEEPIMAP, SPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_SPDESCENT2USEEPIMAP, SPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_SPDN3DUSEEPIMAP, SPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_SPSWUSEEPIMAP, SPDialog::OnRadioBtnClick)

EVT_RADIOBUTTON(ID_SPBLOODUSEUSERMAP, SPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_SPDESCENTUSEUSERMAP, SPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_SPDESCENT2USEUSERMAP, SPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_SPDN3DUSEUSERMAP, SPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_SPSWUSEUSERMAP, SPDialog::OnRadioBtnClick)

END_EVENT_TABLE()

SPDialog::SPDialog() : YANGDialog(NULL, wxID_ANY, wxT("Launch a Single Player game"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* SPDialogbSizer = new wxBoxSizer( wxVERTICAL );

	m_spgeneralizedbook = new YANGGeneralizedbook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL|wxNB_MULTILINE );

	if (HAVE_BLOOD)
	{
		m_spbloodpanel = new YANGPanel( m_spgeneralizedbook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* spbloodpanelbSizer = new wxBoxSizer( wxVERTICAL );

		wxGridBagSizer* spbloodpanelgbSizer = new wxGridBagSizer( 0, 0 );
		spbloodpanelgbSizer->SetFlexibleDirection( wxBOTH );
		spbloodpanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_spbloodsourceportstaticText = new wxStaticText( m_spbloodpanel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
		spbloodpanelgbSizer->Add( m_spbloodsourceportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spbloodsourceportchoice = new YANGChoice( m_spbloodpanel, ID_SPBLOODSELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spbloodpanelgbSizer->Add( m_spbloodsourceportchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spbloodusecrypticpassagecheckBox = new YANGCheckBox( m_spbloodpanel, ID_SPBLOODUSECRYPTICPASSAGE, wxT("Use the Cryptic Passage add-on"), wxDefaultPosition, wxDefaultSize, 0 );
		spbloodpanelgbSizer->Add( m_spbloodusecrypticpassagecheckBox, wxGBPosition( 1, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spbloodepimapradioBtn = new YANGRadioButton( m_spbloodpanel, ID_SPBLOODUSEEPIMAP, wxT("Original map:"), wxDefaultPosition, wxDefaultSize, 0 );
		spbloodpanelgbSizer->Add( m_spbloodepimapradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spbloodepimapchoice = new YANGChoice( m_spbloodpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spbloodpanelgbSizer->Add( m_spbloodepimapchoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spbloodusermapradioBtn = new YANGRadioButton( m_spbloodpanel, ID_SPBLOODUSEUSERMAP, wxT("User map:"), wxDefaultPosition, wxDefaultSize, 0 );
		spbloodpanelgbSizer->Add( m_spbloodusermapradioBtn, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spbloodusermapchoice = new YANGChoice( m_spbloodpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spbloodpanelgbSizer->Add( m_spbloodusermapchoice, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spbloodgametypestaticText = new wxStaticText( m_spbloodpanel, wxID_ANY, wxT("Game type (SP / fake MP):"), wxDefaultPosition, wxDefaultSize, 0 );
		spbloodpanelgbSizer->Add( m_spbloodgametypestaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spbloodgametypechoice = new YANGChoice( m_spbloodpanel, ID_SPBLOODSELECTGAMETYPE, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spbloodpanelgbSizer->Add( m_spbloodgametypechoice, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spbloodrecorddemocheckBox = new YANGCheckBox( m_spbloodpanel, ID_SPBLOODRECORDDEMO, wxT("Record demo"), wxDefaultPosition, wxDefaultSize, 0 );
		spbloodpanelgbSizer->Add( m_spbloodrecorddemocheckBox, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spbloodrecorddemotextCtrl = new YANGTextCtrl( m_spbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spbloodpanelgbSizer->Add( m_spbloodrecorddemotextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spbloodextraargsstaticText = new wxStaticText( m_spbloodpanel, wxID_ANY, wxT("Extra arguments:"), wxDefaultPosition, wxDefaultSize, 0 );
		spbloodpanelgbSizer->Add( m_spbloodextraargsstaticText, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spbloodextraargstextCtrl = new YANGTextCtrl( m_spbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spbloodpanelgbSizer->Add( m_spbloodextraargstextCtrl, wxGBPosition( 6, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		spbloodpanelbSizer->Add( spbloodpanelgbSizer, 1, wxEXPAND, 0 );

		m_spbloodpanel->SetSizer( spbloodpanelbSizer );
		m_spbloodpanel->Layout();
		spbloodpanelbSizer->Fit( m_spbloodpanel );
		m_spgeneralizedbook->AddPage( m_spbloodpanel, GAMENAME_BLOOD );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODSW ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODRG ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODPP ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODOU)
		{
			m_spgeneralizedbook->ChangeSelection(m_spgeneralizedbook->GetPageCount()-1);
		}
	}


	if (HAVE_DESCENT)
	{
		m_spdescentpanel = new YANGPanel( m_spgeneralizedbook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* spdescentpanelbSizer = new wxBoxSizer( wxVERTICAL );

		wxGridBagSizer* spdescentpanelgbSizer = new wxGridBagSizer( 0, 0 );
		spdescentpanelgbSizer->SetFlexibleDirection( wxBOTH );
		spdescentpanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_spdescentsourceportstaticText = new wxStaticText( m_spdescentpanel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdescentpanelgbSizer->Add( m_spdescentsourceportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdescentsourceportchoice = new YANGChoice( m_spdescentpanel, ID_SPDESCENTSELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdescentpanelgbSizer->Add( m_spdescentsourceportchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spdescentepimapradioBtn = new YANGRadioButton( m_spdescentpanel, ID_SPDESCENTUSEEPIMAP, wxT("Original mission:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdescentpanelgbSizer->Add( m_spdescentepimapradioBtn, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdescentepimapchoice = new YANGChoice( m_spdescentpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdescentpanelgbSizer->Add( m_spdescentepimapchoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spdescentusermapradioBtn = new YANGRadioButton( m_spdescentpanel, ID_SPDESCENTUSEUSERMAP, wxT("User mission:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdescentpanelgbSizer->Add( m_spdescentusermapradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdescentusermapchoice = new YANGChoice( m_spdescentpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdescentpanelgbSizer->Add( m_spdescentusermapchoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdescentextraargsstaticText = new wxStaticText( m_spdescentpanel, wxID_ANY, wxT("Extra arguments:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdescentpanelgbSizer->Add( m_spdescentextraargsstaticText, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdescentextraargstextCtrl = new YANGTextCtrl( m_spdescentpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdescentpanelgbSizer->Add( m_spdescentextraargstextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		spdescentpanelbSizer->Add( spdescentpanelgbSizer, 1, wxEXPAND, 0 );

		m_spdescentpanel->SetSizer( spdescentpanelbSizer );
		m_spdescentpanel->Layout();
		spdescentpanelbSizer->Fit( m_spdescentpanel );
		m_spgeneralizedbook->AddPage( m_spdescentpanel, GAMENAME_DESCENT );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDESCENT ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_D1XREBIRTH)
		{
			m_spgeneralizedbook->ChangeSelection(m_spgeneralizedbook->GetPageCount()-1);
		}
	}


	if (HAVE_DESCENT2)
	{
		m_spdescent2panel = new YANGPanel( m_spgeneralizedbook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* spdescent2panelbSizer = new wxBoxSizer( wxVERTICAL );

		wxGridBagSizer* spdescent2panelgbSizer = new wxGridBagSizer( 0, 0 );
		spdescent2panelgbSizer->SetFlexibleDirection( wxBOTH );
		spdescent2panelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_spdescent2sourceportstaticText = new wxStaticText( m_spdescent2panel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdescent2panelgbSizer->Add( m_spdescent2sourceportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdescent2sourceportchoice = new YANGChoice( m_spdescent2panel, ID_SPDESCENT2SELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdescent2panelgbSizer->Add( m_spdescent2sourceportchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spdescent2epimapradioBtn = new YANGRadioButton( m_spdescent2panel, ID_SPDESCENT2USEEPIMAP, wxT("Original mission:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdescent2panelgbSizer->Add( m_spdescent2epimapradioBtn, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdescent2epimapchoice = new YANGChoice( m_spdescent2panel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdescent2panelgbSizer->Add( m_spdescent2epimapchoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spdescent2usermapradioBtn = new YANGRadioButton( m_spdescent2panel, ID_SPDESCENT2USEUSERMAP, wxT("User mission:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdescent2panelgbSizer->Add( m_spdescent2usermapradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdescent2usermapchoice = new YANGChoice( m_spdescent2panel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdescent2panelgbSizer->Add( m_spdescent2usermapchoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdescent2extraargsstaticText = new wxStaticText( m_spdescent2panel, wxID_ANY, wxT("Extra arguments:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdescent2panelgbSizer->Add( m_spdescent2extraargsstaticText, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdescent2extraargstextCtrl = new YANGTextCtrl( m_spdescent2panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdescent2panelgbSizer->Add( m_spdescent2extraargstextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		spdescent2panelbSizer->Add( spdescent2panelgbSizer, 1, wxEXPAND, 0 );

		m_spdescent2panel->SetSizer( spdescent2panelbSizer );
		m_spdescent2panel->Layout();
		spdescent2panelbSizer->Fit( m_spdescent2panel );
		m_spgeneralizedbook->AddPage( m_spdescent2panel, GAMENAME_DESCENT2 );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDESCENT2 ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_D2XREBIRTH)
		{
			m_spgeneralizedbook->ChangeSelection(m_spgeneralizedbook->GetPageCount()-1);
		}
	}


	if (HAVE_DN3D)
	{
		m_spdn3dpanel = new YANGPanel( m_spgeneralizedbook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* spdn3dpanelbSizer = new wxBoxSizer( wxVERTICAL );

		wxGridBagSizer* spdn3dpanelgbSizer = new wxGridBagSizer( 0, 0 );
		spdn3dpanelgbSizer->SetFlexibleDirection( wxBOTH );
		spdn3dpanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_spdn3dsourceportstaticText = new wxStaticText( m_spdn3dpanel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3dsourceportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dsourceportchoice = new YANGChoice( m_spdn3dpanel, ID_SPDN3DSELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdn3dpanelgbSizer->Add( m_spdn3dsourceportchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spdn3dtcmodprofilestaticText = new wxStaticText( m_spdn3dpanel, wxID_ANY, wxT("TC/MOD:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3dtcmodprofilestaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dtcmodprofilechoice = new YANGChoice( m_spdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdn3dpanelgbSizer->Add( m_spdn3dtcmodprofilechoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spdn3depimapradioBtn = new YANGRadioButton( m_spdn3dpanel, ID_SPDN3DUSEEPIMAP, wxT("Original map:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3depimapradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3depimapchoice = new YANGChoice( m_spdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdn3dpanelgbSizer->Add( m_spdn3depimapchoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spdn3dusermapradioBtn = new YANGRadioButton( m_spdn3dpanel, ID_SPDN3DUSEUSERMAP, wxT("User map:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3dusermapradioBtn, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dusermapchoice = new YANGChoice( m_spdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdn3dpanelgbSizer->Add( m_spdn3dusermapchoice, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dgametypestaticText = new wxStaticText( m_spdn3dpanel, wxID_ANY, wxT("Game type (SP / fake MP):"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3dgametypestaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dgametypechoice = new YANGChoice( m_spdn3dpanel, ID_SPDN3DSELECTGAMETYPE, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdn3dpanelgbSizer->Add( m_spdn3dgametypechoice, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spdn3dfakeplayersstaticText = new wxStaticText( m_spdn3dpanel, wxID_ANY, wxT("Amount of fake players:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3dfakeplayersstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dfakeplayerschoice = new YANGChoice( m_spdn3dpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
		spdn3dpanelgbSizer->Add( m_spdn3dfakeplayerschoice, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dbotscheckBox = new YANGCheckBox( m_spdn3dpanel, wxID_ANY, wxT("Enable player bot AI"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3dbotscheckBox, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dskillstaticText = new wxStaticText( m_spdn3dpanel, wxID_ANY, wxT("Skill (monsters):"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3dskillstaticText, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dskillchoice = new YANGChoice( m_spdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdn3dpanelgbSizer->Add( m_spdn3dskillchoice, wxGBPosition( 7, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spdn3dspawnstaticText = new wxStaticText( m_spdn3dpanel, wxID_ANY, wxT("Spawn:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3dspawnstaticText, wxGBPosition( 8, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dspawnchoice = new YANGChoice( m_spdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdn3dpanelgbSizer->Add( m_spdn3dspawnchoice, wxGBPosition( 8, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dplaydemocheckBox = new YANGCheckBox( m_spdn3dpanel, ID_SPDN3DPLAYDEMO, wxT("Play demo"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3dplaydemocheckBox, wxGBPosition( 9, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dplaydemochoice = new YANGChoice( m_spdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdn3dpanelgbSizer->Add( m_spdn3dplaydemochoice, wxGBPosition( 9, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3drecorddemocheckBox = new YANGCheckBox( m_spdn3dpanel, ID_SPDN3DRECORDDEMO, wxT("Record demo"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3drecorddemocheckBox, wxGBPosition( 10, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dextraargsstaticText = new wxStaticText( m_spdn3dpanel, wxID_ANY, wxT("Extra arguments:"), wxDefaultPosition, wxDefaultSize, 0 );
		spdn3dpanelgbSizer->Add( m_spdn3dextraargsstaticText, wxGBPosition( 11, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spdn3dextraargstextCtrl = new YANGTextCtrl( m_spdn3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spdn3dpanelgbSizer->Add( m_spdn3dextraargstextCtrl, wxGBPosition( 11, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		spdn3dpanelbSizer->Add( spdn3dpanelgbSizer, 1, wxEXPAND, 0 );

		m_spdn3dpanel->SetSizer( spdn3dpanelbSizer );
		m_spdn3dpanel->Layout();
		spdn3dpanelbSizer->Fit( m_spdn3dpanel );
		m_spgeneralizedbook->AddPage( m_spdn3dpanel, GAMENAME_DN3D );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKESW ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKERG ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKEAE ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DUKE3DW ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_EDUKE32 ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_NDUKE ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_HDUKE ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_XDUKE)
		{
			m_spgeneralizedbook->ChangeSelection(m_spgeneralizedbook->GetPageCount()-1);
		}
	}


	if (HAVE_SW)
	{
		m_spswpanel = new YANGPanel( m_spgeneralizedbook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* spswpanelbSizer = new wxBoxSizer( wxVERTICAL );

		wxGridBagSizer* spswpanelgbSizer = new wxGridBagSizer( 0, 0 );
		spswpanelgbSizer->SetFlexibleDirection( wxBOTH );
		spswpanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_spswsourceportstaticText = new wxStaticText( m_spswpanel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
		spswpanelgbSizer->Add( m_spswsourceportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswsourceportchoice = new YANGChoice( m_spswpanel, ID_SPSWSELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spswpanelgbSizer->Add( m_spswsourceportchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spswtcmodprofilestaticText = new wxStaticText( m_spswpanel, wxID_ANY, wxT("TC/MOD:"), wxDefaultPosition, wxDefaultSize, 0 );
		spswpanelgbSizer->Add( m_spswtcmodprofilestaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswtcmodprofilechoice = new YANGChoice( m_spswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spswpanelgbSizer->Add( m_spswtcmodprofilechoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spswepimapradioBtn = new YANGRadioButton( m_spswpanel, ID_SPSWUSEEPIMAP, wxT("Original map:"), wxDefaultPosition, wxDefaultSize, 0 );
		spswpanelgbSizer->Add( m_spswepimapradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswepimapchoice = new YANGChoice( m_spswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spswpanelgbSizer->Add( m_spswepimapchoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spswusermapradioBtn = new YANGRadioButton( m_spswpanel, ID_SPSWUSEUSERMAP, wxT("User map:"), wxDefaultPosition, wxDefaultSize, 0 );
		spswpanelgbSizer->Add( m_spswusermapradioBtn, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswusermapchoice = new YANGChoice( m_spswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spswpanelgbSizer->Add( m_spswusermapchoice, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswskillstaticText = new wxStaticText( m_spswpanel, wxID_ANY, wxT("Skill (monsters):"), wxDefaultPosition, wxDefaultSize, 0 );
		spswpanelgbSizer->Add( m_spswskillstaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswskillchoice = new YANGChoice( m_spswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spswpanelgbSizer->Add( m_spswskillchoice, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spswplaydemocheckBox = new YANGCheckBox( m_spswpanel, ID_SPSWPLAYDEMO, wxT("Play demo"), wxDefaultPosition, wxDefaultSize, 0 );
		spswpanelgbSizer->Add( m_spswplaydemocheckBox, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswplaydemochoice = new YANGChoice( m_spswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spswpanelgbSizer->Add( m_spswplaydemochoice, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswrecorddemocheckBox = new YANGCheckBox( m_spswpanel, ID_SPSWRECORDDEMO, wxT("Record demo"), wxDefaultPosition, wxDefaultSize, 0 );
		spswpanelgbSizer->Add( m_spswrecorddemocheckBox, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswrecorddemotextCtrl = new YANGTextCtrl( m_spswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spswpanelgbSizer->Add( m_spswrecorddemotextCtrl, wxGBPosition( 6, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswextraargsstaticText = new wxStaticText( m_spswpanel, wxID_ANY, wxT("Extra arguments:"), wxDefaultPosition, wxDefaultSize, 0 );
		spswpanelgbSizer->Add( m_spswextraargsstaticText, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spswextraargstextCtrl = new YANGTextCtrl( m_spswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spswpanelgbSizer->Add( m_spswextraargstextCtrl, wxGBPosition( 7, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		spswpanelbSizer->Add( spswpanelgbSizer, 1, wxEXPAND, 0 );

		m_spswpanel->SetSizer( spswpanelbSizer );
		m_spswpanel->Layout();
		spswpanelbSizer->Fit( m_spswpanel );
		m_spgeneralizedbook->AddPage( m_spswpanel, GAMENAME_SW );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSSWSW ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSSWRG ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_VOIDSW ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_SWP)
		{
			m_spgeneralizedbook->ChangeSelection(m_spgeneralizedbook->GetPageCount()-1);
		}
	}


	if (HAVE_CUSTOM)
	{
		m_spcustompanel = new YANGPanel( m_spgeneralizedbook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* spcustompanelbSizer = new wxBoxSizer( wxVERTICAL );

		wxGridBagSizer* spcustompanelgbSizer = new wxGridBagSizer( 0, 0 );
		spcustompanelgbSizer->SetFlexibleDirection( wxBOTH );
		spcustompanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_spcustomprofilestaticText = new wxStaticText( m_spcustompanel, wxID_ANY, wxT("Profile:"), wxDefaultPosition, wxDefaultSize, 0 );
		spcustompanelgbSizer->Add( m_spcustomprofilestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spcustomprofilechoice = new YANGChoice( m_spcustompanel, ID_SPSELECTCUSTOMPROFILE, wxDefaultPosition, wxDLG_UNIT(this, wxSize(160, -1)));
		spcustompanelgbSizer->Add( m_spcustomprofilechoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_spcustomsummarystaticText = new wxStaticText( m_spcustompanel, wxID_ANY, wxT("Summary:"), wxDefaultPosition, wxDefaultSize, 0 );
		spcustompanelgbSizer->Add( m_spcustomsummarystaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spcustomsummarytextCtrl = new YANGTextCtrl( m_spcustompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(-1, 140)), wxTE_MULTILINE|wxTE_READONLY );
		spcustompanelgbSizer->Add( m_spcustomsummarytextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_spcustomextraargsstaticText = new wxStaticText( m_spcustompanel, wxID_ANY, wxT("Extra arguments:"), wxDefaultPosition, wxDefaultSize, 0 );
		spcustompanelgbSizer->Add( m_spcustomextraargsstaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_spcustomextraargstextCtrl = new YANGTextCtrl( m_spcustompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
		spcustompanelgbSizer->Add( m_spcustomextraargstextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		spcustompanelbSizer->Add( spcustompanelgbSizer, 1, wxEXPAND, 0 );

		m_spcustompanel->SetSizer( spcustompanelbSizer );
		m_spcustompanel->Layout();
		spcustompanelbSizer->Fit( m_spcustompanel );
		m_spgeneralizedbook->AddPage( m_spcustompanel, GAMENAME_CUSTOM );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_CUSTOM)
			m_spgeneralizedbook->ChangeSelection(m_spgeneralizedbook->GetPageCount()-1);
	}


	SPDialogbSizer->Add( m_spgeneralizedbook, 1, wxEXPAND | wxALL, 5 );

	m_spsdbSizer = new wxStdDialogButtonSizer();
	m_spsdbSizerOK = new YANGButton( this, wxID_OK, wxT("Play") );
	m_spsdbSizer->AddButton( m_spsdbSizerOK );
	m_spsdbSizerCancel = new YANGButton( this, wxID_CANCEL );
	m_spsdbSizer->AddButton( m_spsdbSizerCancel );
	m_spsdbSizer->Realize();

	SPDialogbSizer->Add( m_spsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxBOTTOM, 5 );

	SetSizer( SPDialogbSizer );
	Layout();
	SPDialogbSizer->Fit( this );

	Centre();
	SetIcon(wxIcon(yang_xpm));


	wxArrayString l_file_list;

	if (HAVE_BLOOD)
	{
		if (g_configuration->have_dosbloodsw && g_configuration->have_dosbox)
			m_spbloodsourceportchoice->Append(SRCPORTNAME_DOSBLOODSW);
		if (g_configuration->have_dosbloodrg && g_configuration->have_dosbox)
			m_spbloodsourceportchoice->Append(SRCPORTNAME_DOSBLOODRG);
		if (g_configuration->have_dosbloodpp && g_configuration->have_dosbox)
			m_spbloodsourceportchoice->Append(SRCPORTNAME_DOSBLOODPP);
		if (g_configuration->have_dosbloodou && g_configuration->have_dosbox)
			m_spbloodsourceportchoice->Append(SRCPORTNAME_DOSBLOODOU);

		switch (g_configuration->gamelaunch_blood_source_port)
		{
			case SOURCEPORT_DOSBLOODSW: m_spbloodsourceportchoice->SetStringSelection(SRCPORTNAME_DOSBLOODSW); break;
			case SOURCEPORT_DOSBLOODRG: m_spbloodsourceportchoice->SetStringSelection(SRCPORTNAME_DOSBLOODRG); break;
			case SOURCEPORT_DOSBLOODPP: m_spbloodsourceportchoice->SetStringSelection(SRCPORTNAME_DOSBLOODPP); break;
			case SOURCEPORT_DOSBLOODOU: m_spbloodsourceportchoice->SetStringSelection(SRCPORTNAME_DOSBLOODOU); break;
			default: ;
		}

		if (m_spbloodsourceportchoice->GetSelection() == wxNOT_FOUND)
			m_spbloodsourceportchoice->SetSelection(0);

		m_spbloodusecrypticpassagecheckBox->Enable((m_spbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODRG && wxFileExists(((wxFileName)g_configuration->dosbloodrg_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))) ||
												   (m_spbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODPP && wxFileExists(((wxFileName)g_configuration->dosbloodpp_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))) ||
												   (m_spbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODOU && wxFileExists(((wxFileName)g_configuration->dosbloodou_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))));

		if (m_spbloodusecrypticpassagecheckBox->IsEnabled())
			m_spbloodusecrypticpassagecheckBox->SetValue(g_configuration->gamelaunch_blood_use_crypticpassage);

		m_temp_bloodepimap = g_GetBloodLevelSelectionByNum(g_configuration->gamelaunch_bloodlvl);
		m_temp_bloodepimap_cp = g_GetBloodCPLevelSelectionByNum(g_configuration->gamelaunch_bloodlvl_cp);

		if (!m_spbloodusecrypticpassagecheckBox->GetValue())
			AppendLevelsList(GAME_BLOOD, m_temp_bloodepimap);
		else
			AppendLevelsList(GAME_BLOOD, m_temp_bloodepimap_cp);

		l_file_list.Empty();
		g_AddAllBloodMapFiles(&l_file_list);
		m_spbloodusermapchoice->Append(l_file_list);
		m_spbloodusermapchoice->SetStringSelection(g_configuration->gamelaunch_bloodusermap);

		m_spbloodgametypechoice->Append(wxT("Single Player"));
		m_spbloodgametypechoice->Append(*g_blood_gametype_list);
		m_spbloodgametypechoice->SetSelection(g_configuration->gamelaunch_bloodgametype_in_sp);

		m_spbloodextraargstextCtrl->SetValue(g_configuration->gamelaunch_blood_args_in_sp);

		if (m_spbloodusermapchoice->IsEmpty() || m_spbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODSW)
		{
			m_spbloodepimapradioBtn->SetValue(true);
			m_spbloodepimapchoice->Enable();
			m_spbloodusermapradioBtn->Disable();
			m_spbloodusermapchoice->Disable();

			if (!m_spbloodusermapchoice->IsEmpty() && m_spbloodusermapchoice->GetSelection() == wxNOT_FOUND)
				m_spbloodusermapchoice->SetSelection(0);
		}
		else
		{
			m_spbloodusermapradioBtn->Enable();
			if (m_spbloodusermapchoice->GetSelection() == wxNOT_FOUND)
			{
				m_spbloodepimapradioBtn->SetValue(true);
				m_spbloodepimapchoice->Enable();
				m_spbloodusermapchoice->SetSelection(0);
				m_spbloodusermapchoice->Disable();
			}
			else
			{
				m_spbloodusermapradioBtn->SetValue(true);
				m_spbloodepimapchoice->Disable();
				m_spbloodusermapchoice->Enable();
			}
		}

		if (!m_spbloodgametypechoice->GetSelection())
		{
			m_spbloodepimapchoice->SetSelection(0);
			m_spbloodepimapchoice->Disable();
		}

		m_spbloodrecorddemotextCtrl->Disable();
	}

	if (HAVE_DESCENT)
	{
		if (g_configuration->have_dosdescent && g_configuration->have_dosbox)
			m_spdescentsourceportchoice->Append(SRCPORTNAME_DOSDESCENT);
		if (g_configuration->have_d1xrebirth)
			m_spdescentsourceportchoice->Append(SRCPORTNAME_D1XREBIRTH);

		switch (g_configuration->gamelaunch_descent_source_port)
		{
			case SOURCEPORT_DOSDESCENT: m_spdescentsourceportchoice->SetStringSelection(SRCPORTNAME_DOSDESCENT); break;
			case SOURCEPORT_D1XREBIRTH: m_spdescentsourceportchoice->SetStringSelection(SRCPORTNAME_D1XREBIRTH); break;
			default: ;
		}

		if (m_spdescentsourceportchoice->GetSelection() == wxNOT_FOUND)
			m_spdescentsourceportchoice->SetSelection(0);

		m_spdescentepimapchoice->Append((*g_descent_level_list)[0]);
		m_spdescentepimapchoice->SetSelection(0);

		l_file_list.Empty();
		g_AddAllDescentMapFiles(&l_file_list, false);
		m_spdescentusermapchoice->Append(l_file_list);
		m_spdescentusermapchoice->SetStringSelection(g_configuration->gamelaunch_descentusermap);

		m_spdescentextraargstextCtrl->SetValue(g_configuration->gamelaunch_descent_args_in_sp);

		if (m_spdescentusermapchoice->IsEmpty())
		{
			m_spdescentepimapradioBtn->SetValue(true);
			m_spdescentepimapchoice->Enable();
			m_spdescentusermapradioBtn->Disable();
			m_spdescentusermapchoice->Disable();

			if (!m_spdescentusermapchoice->IsEmpty() && m_spdescentusermapchoice->GetSelection() == wxNOT_FOUND)
				m_spdescentusermapchoice->SetSelection(0);
		}
		else
		{
			m_spdescentusermapradioBtn->Enable();
			if (m_spdescentusermapchoice->GetSelection() == wxNOT_FOUND)
			{
				m_spdescentepimapradioBtn->SetValue(true);
				m_spdescentepimapchoice->Enable();
				m_spdescentusermapchoice->SetSelection(0);
				m_spdescentusermapchoice->Disable();
			}
			else
			{
				m_spdescentusermapradioBtn->SetValue(true);
				m_spdescentepimapchoice->Disable();
				m_spdescentusermapchoice->Enable();
			}
		}
	}

	if (HAVE_DESCENT2)
	{
		if (g_configuration->have_dosdescent2 && g_configuration->have_dosbox)
			m_spdescent2sourceportchoice->Append(SRCPORTNAME_DOSDESCENT2);
		if (g_configuration->have_d2xrebirth)
			m_spdescent2sourceportchoice->Append(SRCPORTNAME_D2XREBIRTH);

		switch (g_configuration->gamelaunch_descent2_source_port)
		{
			case SOURCEPORT_DOSDESCENT2: m_spdescent2sourceportchoice->SetStringSelection(SRCPORTNAME_DOSDESCENT2); break;
			case SOURCEPORT_D2XREBIRTH:  m_spdescent2sourceportchoice->SetStringSelection(SRCPORTNAME_D2XREBIRTH); break;
			default: ;
		}

		if (m_spdescent2sourceportchoice->GetSelection() == wxNOT_FOUND)
			m_spdescent2sourceportchoice->SetSelection(0);

		m_spdescent2epimapchoice->Append((*g_descent2_level_list)[0]);
		m_spdescent2epimapchoice->SetSelection(0);

		l_file_list.Empty();
		g_AddAllDescent2MapFiles(&l_file_list, false);
		m_spdescent2usermapchoice->Append(l_file_list);
		m_spdescent2usermapchoice->SetStringSelection(g_configuration->gamelaunch_descent2usermap);

		m_spdescent2extraargstextCtrl->SetValue(g_configuration->gamelaunch_descent2_args_in_sp);

		if (m_spdescent2usermapchoice->IsEmpty())
		{
			m_spdescent2epimapradioBtn->SetValue(true);
			m_spdescent2epimapchoice->Enable();
			m_spdescent2usermapradioBtn->Disable();
			m_spdescent2usermapchoice->Disable();

			if (!m_spdescent2usermapchoice->IsEmpty() && m_spdescent2usermapchoice->GetSelection() == wxNOT_FOUND)
				m_spdescent2usermapchoice->SetSelection(0);
		}
		else
		{
			m_spdescent2usermapradioBtn->Enable();
			if (m_spdescent2usermapchoice->GetSelection() == wxNOT_FOUND)
			{
				m_spdescent2epimapradioBtn->SetValue(true);
				m_spdescent2epimapchoice->Enable();
				m_spdescent2usermapchoice->SetSelection(0);
				m_spdescent2usermapchoice->Disable();
			}
			else
			{
				m_spdescent2usermapradioBtn->SetValue(true);
				m_spdescent2epimapchoice->Disable();
				m_spdescent2usermapchoice->Enable();
			}
		}
	}

	if (HAVE_DN3D)
	{
		if (g_configuration->have_dosdukesw && g_configuration->have_dosbox)
			m_spdn3dsourceportchoice->Append(SRCPORTNAME_DOSDUKESW);
		if (g_configuration->have_dosdukerg && g_configuration->have_dosbox)
			m_spdn3dsourceportchoice->Append(SRCPORTNAME_DOSDUKERG);
		if (g_configuration->have_dosdukeae && g_configuration->have_dosbox)
			m_spdn3dsourceportchoice->Append(SRCPORTNAME_DOSDUKEAE);
		if (g_configuration->have_duke3dw)
			m_spdn3dsourceportchoice->Append(SRCPORTNAME_DUKE3DW);
		if (g_configuration->have_eduke32)
			m_spdn3dsourceportchoice->Append(SRCPORTNAME_EDUKE32);
		if (g_configuration->have_nduke)
			m_spdn3dsourceportchoice->Append(SRCPORTNAME_NDUKE);
		if (g_configuration->have_hduke)
			m_spdn3dsourceportchoice->Append(SRCPORTNAME_HDUKE);
		if (g_configuration->have_xduke)
			m_spdn3dsourceportchoice->Append(SRCPORTNAME_XDUKE);

		switch (g_configuration->gamelaunch_dn3d_source_port)
		{
			case SOURCEPORT_DOSDUKESW: m_spdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_DOSDUKESW); break;
			case SOURCEPORT_DOSDUKERG: m_spdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_DOSDUKERG); break;
			case SOURCEPORT_DOSDUKEAE: m_spdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_DOSDUKEAE); break;
			case SOURCEPORT_DUKE3DW:   m_spdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_DUKE3DW); break;
			case SOURCEPORT_EDUKE32:   m_spdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_EDUKE32); break;
			case SOURCEPORT_NDUKE:     m_spdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_NDUKE); break;
			case SOURCEPORT_HDUKE:     m_spdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_HDUKE); break;
			case SOURCEPORT_XDUKE:     m_spdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_XDUKE); break;
			default: ;
		}

		if (m_spdn3dsourceportchoice->GetSelection() == wxNOT_FOUND)
			m_spdn3dsourceportchoice->SetSelection(0);

		m_spdn3dtcmodprofilechoice->Append(wxT("None"));

		if (g_configuration->dn3dtcmod_profile_list)
		{
			g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

			while (g_configuration->dn3dtcmod_profile_current)
			{
				m_spdn3dtcmodprofilechoice->Append(g_configuration->dn3dtcmod_profile_current->profilename);

				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;
			}

			m_spdn3dtcmodprofilechoice->SetStringSelection(g_configuration->gamelaunch_dn3dtcmod_profilename);

			if (m_spdn3dtcmodprofilechoice->GetSelection() == wxNOT_FOUND)
			{
				m_spdn3dtcmodprofilechoice->SetSelection(0);

				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;
			}
			else
			{
				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

				while (g_configuration->dn3dtcmod_profile_current->profilename != g_configuration->gamelaunch_dn3dtcmod_profilename)
					g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;
			}
		}
		else
		{
			m_spdn3dtcmodprofilechoice->SetSelection(0);
			m_spdn3dtcmodprofilechoice->Disable();
		}

		AppendLevelsList(GAME_DN3D, g_GetDN3DLevelSelectionByNum(g_configuration->gamelaunch_dn3dlvl));

		l_file_list.Empty();
		g_AddAllDN3DMapFiles(&l_file_list);
		m_spdn3dusermapchoice->Append(l_file_list);
		m_spdn3dusermapchoice->SetStringSelection(g_configuration->gamelaunch_dn3dusermap);

		m_spdn3dgametypechoice->Append(wxT("Single Player"));

		for (size_t l_loop_var = 0; l_loop_var < (size_t)(m_spdn3dsourceportchoice->GetStringSelection() != SRCPORTNAME_EDUKE32 ? 3 : 5); l_loop_var++)
			m_spdn3dgametypechoice->Append((*g_dn3d_gametype_list)[l_loop_var]);

		m_spdn3dgametypechoice->SetSelection(g_configuration->gamelaunch_dn3dgametype_in_sp);

		if (m_spdn3dgametypechoice->GetSelection() == wxNOT_FOUND)
			m_spdn3dgametypechoice->SetSelection(0);

		for (size_t l_loop_var = 1; l_loop_var < MAX_NUM_PLAYERS; l_loop_var++)
			m_spdn3dfakeplayerschoice->Append(wxString::Format(wxT("%d"), l_loop_var));
		m_spdn3dfakeplayerschoice->SetSelection(g_configuration->gamelaunch_numoffakeplayers);

		m_spdn3dbotscheckBox->SetValue(g_configuration->gamelaunch_enable_bot_ai);

		m_spdn3dskillchoice->Append(*g_dn3d_skill_list);
		m_spdn3dskillchoice->SetSelection(g_configuration->gamelaunch_dn3d_skill_in_sp);

		m_spdn3dspawnchoice->Append(*g_dn3d_spawn_list);
		m_spdn3dspawnchoice->SetSelection(g_configuration->gamelaunch_dn3dspawn);

		PrepareDemoFileControls(GAME_DN3D);

		m_spdn3dextraargstextCtrl->SetValue(g_configuration->gamelaunch_dn3d_args_in_sp);

		if (m_spdn3dusermapchoice->IsEmpty() || m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKESW)
		{
			m_spdn3depimapradioBtn->SetValue(true);
			m_spdn3depimapchoice->Enable();
			m_spdn3dusermapradioBtn->Disable();
			m_spdn3dusermapchoice->Disable();

			if (!m_spdn3dusermapchoice->IsEmpty() && m_spdn3dusermapchoice->GetSelection() == wxNOT_FOUND)
				m_spdn3dusermapchoice->SetSelection(0);
		}
		else
		{
			m_spdn3dusermapradioBtn->Enable();
			if (m_spdn3dusermapchoice->GetSelection() == wxNOT_FOUND)
			{
				m_spdn3depimapradioBtn->SetValue(true);
				m_spdn3depimapchoice->Enable();
				m_spdn3dusermapchoice->SetSelection(0);
				m_spdn3dusermapchoice->Disable();
			}
			else
			{
				m_spdn3dusermapradioBtn->SetValue(true);
				m_spdn3depimapchoice->Disable();
				m_spdn3dusermapchoice->Enable();
			}
		}

		if (!m_spdn3dgametypechoice->GetSelection())
		{
			m_spdn3dfakeplayersstaticText->Disable();
			m_spdn3dfakeplayerschoice->Disable();
		}

		if (!m_spdn3dgametypechoice->GetSelection() ||
			m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKESW || m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKERG)
			m_spdn3dbotscheckBox->Disable();

		if (m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKESW || m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKERG)
			m_spdn3dbotscheckBox->SetValue(false);
	}

	if (HAVE_SW)
	{
		if (g_configuration->have_dosswsw && g_configuration->have_dosbox)
			m_spswsourceportchoice->Append(SRCPORTNAME_DOSSWSW);
		if (g_configuration->have_dosswrg && g_configuration->have_dosbox)
			m_spswsourceportchoice->Append(SRCPORTNAME_DOSSWRG);
		if (g_configuration->have_voidsw)
			m_spswsourceportchoice->Append(SRCPORTNAME_VOIDSW);
		if (g_configuration->have_swp)
			m_spswsourceportchoice->Append(SRCPORTNAME_SWP);

		switch (g_configuration->gamelaunch_sw_source_port)
		{
			case SOURCEPORT_DOSSWSW: m_spswsourceportchoice->SetStringSelection(SRCPORTNAME_DOSSWSW); break;
			case SOURCEPORT_DOSSWRG: m_spswsourceportchoice->SetStringSelection(SRCPORTNAME_DOSSWRG); break;
			case SOURCEPORT_VOIDSW:  m_spswsourceportchoice->SetStringSelection(SRCPORTNAME_VOIDSW); break;
			case SOURCEPORT_SWP:     m_spswsourceportchoice->SetStringSelection(SRCPORTNAME_SWP); break;
			default: ;
		}

		if (m_spswsourceportchoice->GetSelection() == wxNOT_FOUND)
			m_spswsourceportchoice->SetSelection(0);

		m_spswtcmodprofilechoice->Append(wxT("None"));

		if (g_configuration->swtcmod_profile_list)
		{
			g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

			while (g_configuration->swtcmod_profile_current)
			{
				m_spswtcmodprofilechoice->Append(g_configuration->swtcmod_profile_current->profilename);

				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;
			}

			m_spswtcmodprofilechoice->SetStringSelection(g_configuration->gamelaunch_swtcmod_profilename);

			if (m_spswtcmodprofilechoice->GetSelection() == wxNOT_FOUND)
			{
				m_spswtcmodprofilechoice->SetSelection(0);

				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;
			}
			else
			{
				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

				while (g_configuration->swtcmod_profile_current->profilename != g_configuration->gamelaunch_swtcmod_profilename)
					g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;
			}
		}
		else
		{
			m_spswtcmodprofilechoice->SetSelection(0);
			m_spswtcmodprofilechoice->Disable();
		}

		AppendLevelsList(GAME_SW, g_GetSWLevelSelectionByNum(g_configuration->gamelaunch_swlvl));

		l_file_list.Empty();
		g_AddAllSWMapFiles(&l_file_list);
		m_spswusermapchoice->Append(l_file_list);
		m_spswusermapchoice->SetStringSelection(g_configuration->gamelaunch_swusermap);

		m_spswskillchoice->Append(*g_sw_skill_list);
		m_spswskillchoice->SetSelection(g_configuration->gamelaunch_sw_skill_in_sp);

		PrepareDemoFileControls(GAME_SW);

		m_spswextraargstextCtrl->SetValue(g_configuration->gamelaunch_sw_args_in_sp);

		if (m_spswusermapchoice->IsEmpty() || m_spswsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSSWSW)
		{
			m_spswepimapradioBtn->SetValue(true);
			m_spswepimapchoice->Enable();
			m_spswusermapradioBtn->Disable();
			m_spswusermapchoice->Disable();

			if (!m_spswusermapchoice->IsEmpty() && m_spswusermapchoice->GetSelection() == wxNOT_FOUND)
				m_spswusermapchoice->SetSelection(0);
		}
		else
		{
			m_spswusermapradioBtn->Enable();
			if (m_spswusermapchoice->GetSelection() == wxNOT_FOUND)
			{
				m_spswepimapradioBtn->SetValue(true);
				m_spswepimapchoice->Enable();
				m_spswusermapchoice->SetSelection(0);
				m_spswusermapchoice->Disable();
			}
			else
			{
				m_spswusermapradioBtn->SetValue(true);
				m_spswepimapchoice->Disable();
				m_spswusermapchoice->Enable();
			}
		}
	}

	if (HAVE_CUSTOM)
	{
		g_configuration->custom_profile_current = g_configuration->custom_profile_list;

		while (g_configuration->custom_profile_current)
		{
			m_spcustomprofilechoice->Append(g_configuration->custom_profile_current->profilename);

			g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
		}

		m_spcustomprofilechoice->SetStringSelection(g_configuration->gamelaunch_custom_profilename_in_sp);

		if (m_spcustomprofilechoice->GetSelection() == wxNOT_FOUND)
		{
			m_spcustomprofilechoice->SetSelection(0);

			g_configuration->custom_profile_current = g_configuration->custom_profile_list;
		}
		else
		{
			g_configuration->custom_profile_current = g_configuration->custom_profile_list;

			while (g_configuration->custom_profile_current->profilename != g_configuration->gamelaunch_custom_profilename_in_sp)
				g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
		}

		UpdateSummaryList();

		m_spcustomextraargstextCtrl->SetValue(g_configuration->custom_profile_current->extraargs);
	}
}

SPDialog::~SPDialog()
{
	g_main_frame->ShowMainFrame();
	//g_sp_dialog = NULL;
}

void SPDialog::AppendLevelsList(GameType game, int level_selection)
{
	size_t l_loop_var;

	switch (game)
	{
		case GAME_BLOOD:
			m_spbloodepimapchoice->Clear();

			if (m_spbloodusecrypticpassagecheckBox->GetValue())
				m_spbloodepimapchoice->Append(*g_blood_cp_level_list);
			else if (m_spbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODSW)
				for (l_loop_var = 0; l_loop_var < 10; l_loop_var++)
					m_spbloodepimapchoice->Append((*g_blood_level_list)[l_loop_var]);
			else if (m_spbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODRG)
				for (l_loop_var = 0; l_loop_var < 48; l_loop_var++)
					m_spbloodepimapchoice->Append((*g_blood_level_list)[l_loop_var]);
			else
				m_spbloodepimapchoice->Append(*g_blood_level_list);

			m_spbloodepimapchoice->SetSelection(level_selection);

			if (m_spbloodepimapchoice->GetSelection() == wxNOT_FOUND)
				m_spbloodepimapchoice->SetSelection(0);

			break;

		case GAME_DN3D:
			m_spdn3depimapchoice->Clear();

			if (m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKESW)
				for (l_loop_var = 0; l_loop_var < 8; l_loop_var++)
					m_spdn3depimapchoice->Append((*g_dn3d_level_list)[l_loop_var]);
			else if (m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKERG)
				for (l_loop_var = 0; l_loop_var < 33; l_loop_var++)
					m_spdn3depimapchoice->Append((*g_dn3d_level_list)[l_loop_var]);
			else
				m_spdn3depimapchoice->Append(*g_dn3d_level_list);

			m_spdn3depimapchoice->SetSelection(level_selection);

			if (m_spdn3depimapchoice->GetSelection() == wxNOT_FOUND)
				m_spdn3depimapchoice->SetSelection(0);

			break;

		case GAME_SW:
			m_spswepimapchoice->Clear();

			if (m_spswsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSSWSW)
				for (l_loop_var = 0; l_loop_var < 6; l_loop_var++)
					m_spswepimapchoice->Append((*g_sw_level_list)[l_loop_var]);
			else
				m_spswepimapchoice->Append(*g_sw_level_list);

			m_spswepimapchoice->SetSelection(level_selection);

			if (m_spswepimapchoice->GetSelection() == wxNOT_FOUND)
				m_spswepimapchoice->SetSelection(0);

			break;
		default: ;
	}
}

void SPDialog::PrepareDemoFileControls(GameType game)
{
	wxArrayString l_file_list;
	wxString l_srcport_name;

	switch (game)
	{
		case GAME_DN3D:
			l_srcport_name = m_spdn3dsourceportchoice->GetStringSelection();

			if (l_srcport_name == SRCPORTNAME_DOSDUKEAE)
				g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->dosdukeae_exec).GetPath());
			else if (l_srcport_name == SRCPORTNAME_DUKE3DW)
			{
				if (g_configuration->duke3dw_userpath_use)
					g_AddAllDemoFiles(&l_file_list, g_configuration->duke3dw_userpath);
				else
					g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->duke3dw_exec).GetPath());
			}
			else if (l_srcport_name == SRCPORTNAME_EDUKE32)
			{
				if (g_configuration->eduke32_userpath_use)
					g_AddAllDemoFiles(&l_file_list, g_configuration->eduke32_userpath);
				else
					g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->eduke32_exec).GetPath());
			}
			else if (l_srcport_name == SRCPORTNAME_NDUKE)
			{
				if (g_configuration->nduke_userpath_use)
					g_AddAllDemoFiles(&l_file_list, g_configuration->nduke_userpath);
				else
					g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->nduke_exec).GetPath());
			}
			else if (l_srcport_name == SRCPORTNAME_HDUKE)
			{
				if (g_configuration->hduke_userpath_use)
					g_AddAllDemoFiles(&l_file_list, g_configuration->hduke_userpath);
				else
					g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->hduke_exec).GetPath());
			}
			else if (l_srcport_name == SRCPORTNAME_XDUKE)
				g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->xduke_exec).GetPath());

			m_spdn3dplaydemochoice->Clear();
			m_spdn3dplaydemochoice->Append(l_file_list);
			m_spdn3dplaydemochoice->Disable();
			if (m_spdn3dplaydemochoice->GetCount())
			{
				m_spdn3dplaydemocheckBox->Enable();
				m_spdn3dplaydemochoice->SetSelection(0);
			}
			else
				m_spdn3dplaydemocheckBox->Disable();
			m_spdn3dplaydemocheckBox->SetValue(false);
			m_spdn3drecorddemocheckBox->SetValue(false);

			break;

		case GAME_SW:
			l_srcport_name = m_spswsourceportchoice->GetStringSelection();

			if (l_srcport_name == SRCPORTNAME_DOSSWSW)
				g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->dosswsw_exec).GetPath());
			else if (l_srcport_name == SRCPORTNAME_DOSSWRG)
				g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->dosswrg_exec).GetPath());
			else if (l_srcport_name == SRCPORTNAME_VOIDSW)
			{
				if (g_configuration->voidsw_userpath_use)
					g_AddAllDemoFiles(&l_file_list, g_configuration->voidsw_userpath);
				else
					g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->voidsw_exec).GetPath());
			}
			else if (l_srcport_name == SRCPORTNAME_SWP)
				g_AddAllDemoFiles(&l_file_list, ((wxFileName)g_configuration->swp_exec).GetPath());

			m_spswplaydemochoice->Clear();
			m_spswplaydemochoice->Append(l_file_list);
			m_spswplaydemochoice->Disable();
			if (m_spswplaydemochoice->GetCount())
			{
				m_spswplaydemocheckBox->Enable();
				m_spswplaydemochoice->SetSelection(0);
			}
			else
				m_spswplaydemocheckBox->Disable();
			m_spswplaydemocheckBox->SetValue(false);
			m_spswrecorddemocheckBox->SetValue(false);
			m_spswrecorddemotextCtrl->Disable();

			break;
		default: ;
	}
}

void SPDialog::OnSrcPortSelect(wxCommandEvent& event)
{
	int l_temp_dn3dgametype;

	switch (event.GetId())
	{
		case ID_SPBLOODSELECTSRCPORT:
			m_spbloodusecrypticpassagecheckBox->Enable((m_spbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODRG && wxFileExists(((wxFileName)g_configuration->dosbloodrg_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))) ||
													   (m_spbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODPP && wxFileExists(((wxFileName)g_configuration->dosbloodpp_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))) ||
													   (m_spbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODOU && wxFileExists(((wxFileName)g_configuration->dosbloodou_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))));

			if (!m_spbloodusecrypticpassagecheckBox->IsEnabled() && m_spbloodusecrypticpassagecheckBox->GetValue())
			{
				m_spbloodusecrypticpassagecheckBox->SetValue(false);

				m_temp_bloodepimap_cp = m_spbloodepimapchoice->GetSelection();
				AppendLevelsList(GAME_BLOOD, m_temp_bloodepimap);
			}
			else
				AppendLevelsList(GAME_BLOOD, m_spbloodepimapchoice->GetSelection());

			if (m_spbloodusermapchoice->IsEmpty() || m_spbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODSW)
			{
				m_spbloodepimapradioBtn->SetValue(true);
				if (m_spbloodgametypechoice->GetSelection())
					m_spbloodepimapchoice->Enable();
				m_spbloodusermapradioBtn->Disable();
				m_spbloodusermapchoice->Disable();
			}
			else
			{
				m_spbloodusermapradioBtn->Enable();
				if (m_spbloodusermapchoice->GetSelection() == wxNOT_FOUND)
					m_spbloodusermapchoice->SetSelection(0);
			}

			break;

		case ID_SPDESCENTSELECTSRCPORT:
			if (m_spdescentusermapchoice->IsEmpty())
			{
				m_spdescentepimapradioBtn->SetValue(true);
				m_spdescentepimapchoice->Enable();
				m_spdescentusermapradioBtn->Disable();
				m_spdescentusermapchoice->Disable();
			}
			else
			{
				m_spdescentusermapradioBtn->Enable();
				if (m_spdescentusermapchoice->GetSelection() == wxNOT_FOUND)
					m_spdescentusermapchoice->SetSelection(0);
			}

			break;

		case ID_SPDESCENT2SELECTSRCPORT:
			if (m_spdescent2usermapchoice->IsEmpty())
			{
				m_spdescent2epimapradioBtn->SetValue(true);
				m_spdescent2epimapchoice->Enable();
				m_spdescent2usermapradioBtn->Disable();
				m_spdescent2usermapchoice->Disable();
			}
			else
			{
				m_spdescent2usermapradioBtn->Enable();
				if (m_spdescent2usermapchoice->GetSelection() == wxNOT_FOUND)
					m_spdescent2usermapchoice->SetSelection(0);
			}

			break;

		case ID_SPDN3DSELECTSRCPORT:
			AppendLevelsList(GAME_DN3D, m_spdn3depimapchoice->GetSelection());

			if (m_spdn3dusermapchoice->IsEmpty() || m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKESW)
			{
				m_spdn3depimapradioBtn->SetValue(true);
				m_spdn3depimapchoice->Enable();
				m_spdn3dusermapradioBtn->Disable();
				m_spdn3dusermapchoice->Disable();
			}
			else
			{
				m_spdn3dusermapradioBtn->Enable();
				if (m_spdn3dusermapchoice->GetSelection() == wxNOT_FOUND)
					m_spdn3dusermapchoice->SetSelection(0);
			}

			l_temp_dn3dgametype = m_spdn3dgametypechoice->GetSelection();
			m_spdn3dgametypechoice->Clear();

			m_spdn3dgametypechoice->Append(wxT("Single Player"));

			for (size_t l_loop_var = 0; l_loop_var < (size_t)(m_spdn3dsourceportchoice->GetStringSelection() != SRCPORTNAME_EDUKE32 ? 3 : 5); l_loop_var++)
				m_spdn3dgametypechoice->Append((*g_dn3d_gametype_list)[l_loop_var]);

			m_spdn3dgametypechoice->SetSelection(l_temp_dn3dgametype);

			if (m_spdn3dgametypechoice->GetSelection() == wxNOT_FOUND)
				m_spdn3dgametypechoice->SetSelection(0);

			if (!m_spdn3dgametypechoice->GetSelection() ||
				m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKESW || m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKERG)
				m_spdn3dbotscheckBox->Disable();
			else
				m_spdn3dbotscheckBox->Enable();

			if (m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKESW || m_spdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKERG)
				m_spdn3dbotscheckBox->SetValue(false);

			PrepareDemoFileControls(GAME_DN3D);

			break;

		case ID_SPSWSELECTSRCPORT:
			AppendLevelsList(GAME_SW, m_spswepimapchoice->GetSelection());

			if (m_spswusermapchoice->IsEmpty() || m_spswsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSSWSW)
			{
				m_spswepimapradioBtn->SetValue(true);
				m_spswepimapchoice->Enable();
				m_spswusermapradioBtn->Disable();
				m_spswusermapchoice->Disable();
			}
			else
			{
				m_spswusermapradioBtn->Enable();
				if (m_spswusermapchoice->GetSelection() == wxNOT_FOUND)
					m_spswusermapchoice->SetSelection(0);
			}

			PrepareDemoFileControls(GAME_SW);

			break;
		default: ;
	}
}

void SPDialog::OnGameTypeSelect(wxCommandEvent& event)
{
	switch (event.GetId())
	{
		case ID_SPBLOODSELECTGAMETYPE:
			if (!m_spbloodgametypechoice->GetSelection())
			{
				m_spbloodepimapchoice->SetSelection(0);
				m_spbloodepimapchoice->Disable();
			}
			else
				m_spbloodepimapchoice->Enable(m_spbloodepimapradioBtn->GetValue());

			break;

		case ID_SPDN3DSELECTGAMETYPE:
			if (!m_spdn3dgametypechoice->GetSelection())
			{
				m_spdn3dfakeplayersstaticText->Disable();
				m_spdn3dfakeplayerschoice->Disable();
				m_spdn3dbotscheckBox->Disable();
			}
			else
			{
				m_spdn3dfakeplayersstaticText->Enable();
				m_spdn3dfakeplayerschoice->Enable();
				if (m_spdn3dsourceportchoice->GetStringSelection() != SRCPORTNAME_DOSDUKESW && m_spdn3dsourceportchoice->GetStringSelection() != SRCPORTNAME_DOSDUKERG)
					m_spdn3dbotscheckBox->Enable();
			}

			break;
		default: ;
	}
}

void SPDialog::OnCheckBoxClick(wxCommandEvent& event)
{
	switch (event.GetId())
	{
		case ID_SPDN3DPLAYDEMO:
			m_spdn3dplaydemochoice->Enable(m_spdn3dplaydemocheckBox->GetValue());
			m_spdn3drecorddemocheckBox->SetValue(false);
			break;
		case ID_SPSWPLAYDEMO:
			m_spswplaydemochoice->Enable(m_spswplaydemocheckBox->GetValue());
			m_spswrecorddemocheckBox->SetValue(false);
			m_spswrecorddemotextCtrl->Disable();
			break;
		case ID_SPBLOODRECORDDEMO:
			m_spbloodrecorddemotextCtrl->Enable(m_spbloodrecorddemocheckBox->GetValue());
			break;
		case ID_SPDN3DRECORDDEMO:
			m_spdn3dplaydemocheckBox->SetValue(false);
			m_spdn3dplaydemochoice->Disable();
			break;
		case ID_SPSWRECORDDEMO:
			m_spswrecorddemotextCtrl->Enable(m_spswrecorddemocheckBox->GetValue());
			m_spswplaydemocheckBox->SetValue(false);
			m_spswplaydemochoice->Disable();
			break;
		case ID_SPBLOODUSECRYPTICPASSAGE:
			if (!m_spbloodusecrypticpassagecheckBox->GetValue())
			{
				m_temp_bloodepimap_cp = m_spbloodepimapchoice->GetSelection();
				AppendLevelsList(GAME_BLOOD, m_temp_bloodepimap);
			}
			else
			{
				m_temp_bloodepimap = m_spbloodepimapchoice->GetSelection();
				AppendLevelsList(GAME_BLOOD, m_temp_bloodepimap_cp);
			}

			if (!m_spbloodgametypechoice->GetSelection())
				m_spbloodepimapchoice->SetSelection(0);

			break;
		default: ;
	}
}

void SPDialog::OnRadioBtnClick(wxCommandEvent& event)
{
	switch (event.GetId())
	{
		case ID_SPBLOODUSEEPIMAP:
		case ID_SPBLOODUSEUSERMAP:
			if (m_spbloodgametypechoice->GetSelection())
				m_spbloodepimapchoice->Enable(m_spbloodepimapradioBtn->GetValue());
			m_spbloodusermapchoice->Enable(m_spbloodusermapradioBtn->GetValue());
			break;
		case ID_SPDESCENTUSEEPIMAP:
		case ID_SPDESCENTUSEUSERMAP:
			m_spdescentepimapchoice->Enable(m_spdescentepimapradioBtn->GetValue());
			m_spdescentusermapchoice->Enable(m_spdescentusermapradioBtn->GetValue());
			break;
		case ID_SPDESCENT2USEEPIMAP:
		case ID_SPDESCENT2USEUSERMAP:
			m_spdescent2epimapchoice->Enable(m_spdescent2epimapradioBtn->GetValue());
			m_spdescent2usermapchoice->Enable(m_spdescent2usermapradioBtn->GetValue());
			break;
		case ID_SPDN3DUSEEPIMAP:
		case ID_SPDN3DUSEUSERMAP:
			m_spdn3depimapchoice->Enable(m_spdn3depimapradioBtn->GetValue());
			m_spdn3dusermapchoice->Enable(m_spdn3dusermapradioBtn->GetValue());
			break;
		case ID_SPSWUSEEPIMAP:
		case ID_SPSWUSEUSERMAP:
			m_spswepimapchoice->Enable(m_spswepimapradioBtn->GetValue());
			m_spswusermapchoice->Enable(m_spswusermapradioBtn->GetValue());
			break;
		default: ;
	}
}

void SPDialog::OnSelectCustomProfile(wxCommandEvent& WXUNUSED(event))
{
	wxString l_temp_str = m_spcustomprofilechoice->GetStringSelection();


	g_configuration->custom_profile_current = g_configuration->custom_profile_list;

	while (g_configuration->custom_profile_current->profilename != l_temp_str)
		g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;

	UpdateSummaryList();

	m_spcustomextraargstextCtrl->SetValue(g_configuration->custom_profile_current->extraargs);
}

void SPDialog::UpdateSummaryList()
{
	m_spcustomsummarytextCtrl->Clear();

	m_spcustomsummarytextCtrl->AppendText(wxT("Executable :\n"));
	m_spcustomsummarytextCtrl->AppendText(g_configuration->custom_profile_current->spexecutable);

	if (g_configuration->custom_profile_current->cdmount == CDROMMOUNT_DIR)
	{
		m_spcustomsummarytextCtrl->AppendText(wxT("\n\nCD-ROM location :\n"));
		m_spcustomsummarytextCtrl->AppendText(g_configuration->custom_profile_current->cdlocation);
	}
	else if (g_configuration->custom_profile_current->cdmount == CDROMMOUNT_IMG)
	{
		m_spcustomsummarytextCtrl->AppendText(wxT("\n\nCD-ROM image/block device :\n"));
		m_spcustomsummarytextCtrl->AppendText(g_configuration->custom_profile_current->cdimage);
	}
/*
	if (!g_configuration->custom_profile_current->extraargs.IsEmpty())
	{
		m_spcustomsummarytextCtrl->AppendText(wxT("\n\nExtra arguments :\n"));
		m_spcustomsummarytextCtrl->AppendText(g_configuration->custom_profile_current->extraargs);
	}*/
}

void SPDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	wxString l_game_name = m_spgeneralizedbook->GetPageText(m_spgeneralizedbook->GetSelection());
	wxString l_temp_str;

	ApplySettings();

	GameType l_game;
	SourcePortType l_srcport;
	bool l_use_crypticpassage = false;
	bool l_use_tcmod = false;
	size_t l_episode_map = 0;
	wxString l_user_map, l_extra_usermap;
	bool l_launch_usermap = false;
	BloodMPGameT_Type l_bloodgametype_in_sp = BLOODMPGAMETYPE_BB;
	DN3DMPGameT_Type l_dn3dgametype_in_sp = DN3DMPGAMETYPE_DMSPAWN;
	size_t l_numoffakeplayers = 0;
	bool l_enable_bot_ai = false;
	size_t l_skill = 0;
	DN3DSpawnType l_dn3dspawn = DN3DSPAWN_NONE;
	bool l_playdemo = false;
	wxString l_playdemofilename;
	bool l_recorddemo = false;
	wxString l_recorddemofilename;
	wxString l_extraargs;

	if (l_game_name == GAMENAME_BLOOD)
	{
		l_game = GAME_BLOOD;

		l_temp_str = m_spbloodsourceportchoice->GetStringSelection();
		if (l_temp_str == SRCPORTNAME_DOSBLOODSW)
			l_srcport = SOURCEPORT_DOSBLOODSW;
		else if (l_temp_str == SRCPORTNAME_DOSBLOODRG)
			l_srcport = SOURCEPORT_DOSBLOODRG;
		else if (l_temp_str == SRCPORTNAME_DOSBLOODPP)
			l_srcport = SOURCEPORT_DOSBLOODPP;
		else if (l_temp_str == SRCPORTNAME_DOSBLOODOU)
			l_srcport = SOURCEPORT_DOSBLOODOU;

		l_use_crypticpassage = m_spbloodusecrypticpassagecheckBox->GetValue();

		if (m_spbloodepimapradioBtn->GetValue())
		{
			l_launch_usermap = false;

			if (!l_use_crypticpassage)
				l_episode_map = g_GetBloodLevelNumBySelection(m_spbloodepimapchoice->GetSelection());
			else
				l_episode_map = g_GetBloodCPLevelNumBySelection(m_spbloodepimapchoice->GetSelection());
		}
		else
		{
			l_launch_usermap = true;
			l_user_map = m_spbloodusermapchoice->GetStringSelection();
		}

		l_bloodgametype_in_sp = (BloodMPGameT_Type)m_spbloodgametypechoice->GetSelection();

		l_recorddemo = m_spbloodrecorddemocheckBox->GetValue();
		l_recorddemofilename = m_spbloodrecorddemotextCtrl->GetValue();

		if (l_recorddemofilename != wxEmptyString)
		{
			if (!(l_recorddemofilename.Right(4).IsSameAs(wxT(".dem"), false)))
				l_recorddemofilename << wxT(".dem");

			l_recorddemofilename.MakeUpper();
		}

		l_extraargs = m_spbloodextraargstextCtrl->GetValue();
	}
	else if (l_game_name == GAMENAME_DESCENT)
	{
		l_game = GAME_DESCENT;

		l_temp_str = m_spdescentsourceportchoice->GetStringSelection();
		if (l_temp_str == SRCPORTNAME_DOSDESCENT)
			l_srcport = SOURCEPORT_DOSDESCENT;
		else if (l_temp_str == SRCPORTNAME_D1XREBIRTH)
			l_srcport = SOURCEPORT_D1XREBIRTH;

		if (m_spdescentepimapradioBtn->GetValue())
			l_launch_usermap = false;
		else
		{
			l_launch_usermap = true;
			l_user_map = m_spdescentusermapchoice->GetStringSelection();

			if (!g_FindDescentMSNfile(&l_extra_usermap, l_user_map))
			{
				wxMessageBox(wxT("The \".MSN\" file associated to the \"") + l_user_map + wxT("\" file is missing.\n\nPlease download this missing \"") + l_user_map.Upper().BeforeLast('.') + wxT(".MSN") + wxT("\" file, and try again."), wxT("Missing \".MSN\" mission file"), wxOK|wxICON_EXCLAMATION);
				return;
			}
		}

		l_extraargs = m_spdescentextraargstextCtrl->GetValue();
	}
	else if (l_game_name == GAMENAME_DESCENT2)
	{
		l_game = GAME_DESCENT2;

		l_temp_str = m_spdescent2sourceportchoice->GetStringSelection();
		if (l_temp_str == SRCPORTNAME_DOSDESCENT2)
			l_srcport = SOURCEPORT_DOSDESCENT2;
		else if (l_temp_str == SRCPORTNAME_D2XREBIRTH)
			l_srcport = SOURCEPORT_D2XREBIRTH;

		if (m_spdescent2epimapradioBtn->GetValue())
			l_launch_usermap = false;
		else
		{
			l_launch_usermap = true;
			l_user_map = m_spdescent2usermapchoice->GetStringSelection();

			if (!g_FindDescentMN2file(&l_extra_usermap, l_user_map))
			{
				wxMessageBox(wxT("The \".MN2\" file associated to the \"") + l_user_map + wxT("\" file is missing.\n\nPlease download this missing \"") + l_user_map.Upper().BeforeLast('.') + wxT(".MN2") + wxT("\" file, and try again."), wxT("Missing \".MN2\" mission file"), wxOK|wxICON_EXCLAMATION);
				return;
			}
		}

		l_extraargs = m_spdescent2extraargstextCtrl->GetValue();
	}
	else if (l_game_name == GAMENAME_DN3D)
	{
		l_game = GAME_DN3D;

		l_temp_str = m_spdn3dsourceportchoice->GetStringSelection();
		if (l_temp_str == SRCPORTNAME_DOSDUKESW)
			l_srcport = SOURCEPORT_DOSDUKESW;
		else if (l_temp_str == SRCPORTNAME_DOSDUKERG)
			l_srcport = SOURCEPORT_DOSDUKERG;
		else if (l_temp_str == SRCPORTNAME_DOSDUKEAE)
			l_srcport = SOURCEPORT_DOSDUKEAE;
		else if (l_temp_str == SRCPORTNAME_DUKE3DW)
			l_srcport = SOURCEPORT_DUKE3DW;
		else if (l_temp_str == SRCPORTNAME_EDUKE32)
			l_srcport = SOURCEPORT_EDUKE32;
		else if (l_temp_str == SRCPORTNAME_NDUKE)
			l_srcport = SOURCEPORT_NDUKE;
		else if (l_temp_str == SRCPORTNAME_HDUKE)
			l_srcport = SOURCEPORT_HDUKE;
		else if (l_temp_str == SRCPORTNAME_XDUKE)
			l_srcport = SOURCEPORT_XDUKE;

		l_use_tcmod = m_spdn3dtcmodprofilechoice->GetSelection();

		if (m_spdn3depimapradioBtn->GetValue())
		{
			l_launch_usermap = false;
			l_episode_map = g_GetDN3DLevelNumBySelection(m_spdn3depimapchoice->GetSelection());
		}
		else
		{
			l_launch_usermap = true;
			l_user_map = m_spdn3dusermapchoice->GetStringSelection();
		}

		l_dn3dgametype_in_sp = (DN3DMPGameT_Type)m_spdn3dgametypechoice->GetSelection();

		l_numoffakeplayers = m_spdn3dfakeplayerschoice->GetSelection();

		l_enable_bot_ai = m_spdn3dbotscheckBox->GetValue();

		l_skill = m_spdn3dskillchoice->GetSelection();

		l_dn3dspawn = (DN3DSpawnType)m_spdn3dspawnchoice->GetSelection();

		l_playdemo = m_spdn3dplaydemocheckBox->GetValue();
		l_playdemofilename = m_spdn3dplaydemochoice->GetStringSelection();

		l_recorddemo = m_spdn3drecorddemocheckBox->GetValue();

		l_extraargs = m_spdn3dextraargstextCtrl->GetValue();
	}
	else if (l_game_name == GAMENAME_SW)
	{
		l_game = GAME_SW;

		l_temp_str = m_spswsourceportchoice->GetStringSelection();
		if (l_temp_str == SRCPORTNAME_DOSSWSW)
			l_srcport = SOURCEPORT_DOSSWSW;
		else if (l_temp_str == SRCPORTNAME_DOSSWRG)
			l_srcport = SOURCEPORT_DOSSWRG;
		else if (l_temp_str == SRCPORTNAME_VOIDSW)
			l_srcport = SOURCEPORT_VOIDSW;
		else if (l_temp_str == SRCPORTNAME_SWP)
			l_srcport = SOURCEPORT_SWP;

		l_use_tcmod = m_spswtcmodprofilechoice->GetSelection();

		if (m_spswepimapradioBtn->GetValue())
		{
			l_launch_usermap = false;
			l_episode_map = g_GetSWLevelNumBySelection(m_spswepimapchoice->GetSelection());
		}
		else
		{
			l_launch_usermap = true;
			l_user_map = m_spswusermapchoice->GetStringSelection();
		}

		l_skill = m_spswskillchoice->GetSelection();

		l_playdemo = m_spswplaydemocheckBox->GetValue();
		l_playdemofilename = m_spswplaydemochoice->GetStringSelection();

		l_recorddemo = m_spswrecorddemocheckBox->GetValue();
		l_recorddemofilename = m_spswrecorddemotextCtrl->GetValue();

		if (l_recorddemofilename != wxEmptyString)
		{
			if (!(l_recorddemofilename.Right(4).IsSameAs(wxT(".dmo"), false)))
				l_recorddemofilename << wxT(".dmo");

			l_recorddemofilename.MakeUpper();
		}

		l_extraargs = m_spswextraargstextCtrl->GetValue();
	}
	else if (l_game_name == GAMENAME_CUSTOM)
	{
		l_game = GAME_CUSTOM;

		l_srcport = SOURCEPORT_CUSTOM;

		g_configuration->custom_profile_current = g_configuration->custom_profile_list;

		l_temp_str = m_spcustomprofilechoice->GetStringSelection();

		while (g_configuration->custom_profile_current->profilename != l_temp_str)
			g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;

		l_extraargs = m_spcustomextraargstextCtrl->GetValue();
	}

#ifdef __WXMSW__
	l_temp_str = *g_launcher_user_profile_path + wxT("yang.bat");
#elif (defined __WXMAC__) || (defined __WXCOCOA__)
	l_temp_str = *g_launcher_user_profile_path + wxT("yang.command");
#else
	l_temp_str = *g_launcher_user_profile_path + wxT("yang.sh");
#endif
	if (g_MakeLaunchScript(
		false,
		l_temp_str,
		NULL,
		0,
		l_game,
		l_srcport,
		l_use_crypticpassage,
		l_episode_map,
		l_user_map,
		l_launch_usermap,
		l_bloodgametype_in_sp,
		l_dn3dgametype_in_sp,
		SWMPGAMETYPE_WBSPAWN,
		l_numoffakeplayers+1,
		l_enable_bot_ai,
		l_skill,
		BLOODMPMONSTERTYPE_NONE,
		BLOODMPWEAPONTYPE_PERMANENT,
		BLOODMPITEMTYPE_RESPAWN,
		BLOODMPRESPAWNTYPE_RANDOM,
		l_dn3dspawn,
		l_playdemo,
		l_playdemofilename,
		l_recorddemo,
		l_recorddemofilename,
		0,
		0,
		l_use_tcmod,
		l_extraargs))
#ifdef __WXMSW__
		wxExecute(wxT('"') + l_temp_str + wxT('"'));
#else
		wxExecute(g_configuration->terminal_fullcmd + wxT(" \"") + l_temp_str + wxT('"'));
#endif
	Destroy();
}

void SPDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	Destroy();
}

void SPDialog::ApplySettings()
{
	wxString l_game_name = m_spgeneralizedbook->GetPageText(m_spgeneralizedbook->GetSelection());
	wxString l_srcport_name;

	if (HAVE_BLOOD)
	{
		l_srcport_name = m_spbloodsourceportchoice->GetStringSelection();

		if (l_srcport_name == SRCPORTNAME_DOSBLOODSW)
			g_configuration->gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODSW;
		else if (l_srcport_name == SRCPORTNAME_DOSBLOODRG)
			g_configuration->gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODRG;
		else if (l_srcport_name == SRCPORTNAME_DOSBLOODPP)
			g_configuration->gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODPP;
		else if (l_srcport_name == SRCPORTNAME_DOSBLOODOU)
			g_configuration->gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODOU;

		g_configuration->gamelaunch_blood_use_crypticpassage = m_spbloodusecrypticpassagecheckBox->GetValue();

		if (m_spbloodgametypechoice->GetSelection())
		{
			if (!m_spbloodusecrypticpassagecheckBox->GetValue())
			{
				g_configuration->gamelaunch_bloodlvl = g_GetBloodLevelNumBySelection(m_spbloodepimapchoice->GetSelection());
				g_configuration->gamelaunch_bloodlvl_cp = g_GetBloodCPLevelNumBySelection(m_temp_bloodepimap_cp);
			}
			else
			{
				g_configuration->gamelaunch_bloodlvl = g_GetBloodLevelNumBySelection(m_temp_bloodepimap);
				g_configuration->gamelaunch_bloodlvl_cp = g_GetBloodCPLevelNumBySelection(m_spbloodepimapchoice->GetSelection());
			}
		}

		if (m_spbloodepimapradioBtn->GetValue())
			g_configuration->gamelaunch_bloodusermap = wxEmptyString;
		else
			g_configuration->gamelaunch_bloodusermap = m_spbloodusermapchoice->GetStringSelection();

		g_configuration->gamelaunch_bloodgametype_in_sp = (BloodMPGameT_Type)m_spbloodgametypechoice->GetSelection();

		g_configuration->gamelaunch_blood_args_in_sp = m_spbloodextraargstextCtrl->GetValue();
	}

	if (HAVE_DESCENT)
	{
		l_srcport_name = m_spdescentsourceportchoice->GetStringSelection();

		if (l_srcport_name == SRCPORTNAME_DOSDESCENT)
			g_configuration->gamelaunch_descent_source_port = SOURCEPORT_DOSDESCENT;
		else if (l_srcport_name == SRCPORTNAME_D1XREBIRTH)
			g_configuration->gamelaunch_descent_source_port = SOURCEPORT_D1XREBIRTH;

		if (m_spdescentepimapradioBtn->GetValue())
			g_configuration->gamelaunch_descentusermap = wxEmptyString;
		else
			g_configuration->gamelaunch_descentusermap = m_spdescentusermapchoice->GetStringSelection();

		g_configuration->gamelaunch_descent_args_in_sp = m_spdescentextraargstextCtrl->GetValue();
	}

	if (HAVE_DESCENT2)
	{
		l_srcport_name = m_spdescent2sourceportchoice->GetStringSelection();

		if (l_srcport_name == SRCPORTNAME_DOSDESCENT2)
			g_configuration->gamelaunch_descent2_source_port = SOURCEPORT_DOSDESCENT2;
		else if (l_srcport_name == SRCPORTNAME_D2XREBIRTH)
			g_configuration->gamelaunch_descent2_source_port = SOURCEPORT_D2XREBIRTH;

		if (m_spdescent2epimapradioBtn->GetValue())
			g_configuration->gamelaunch_descent2usermap = wxEmptyString;
		else
			g_configuration->gamelaunch_descent2usermap = m_spdescent2usermapchoice->GetStringSelection();

		g_configuration->gamelaunch_descent2_args_in_sp = m_spdescent2extraargstextCtrl->GetValue();
	}

	if (HAVE_DN3D)
	{
		l_srcport_name = m_spdn3dsourceportchoice->GetStringSelection();

		if (l_srcport_name == SRCPORTNAME_DOSDUKESW)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_DOSDUKESW;
		else if (l_srcport_name == SRCPORTNAME_DOSDUKERG)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_DOSDUKERG;
		else if (l_srcport_name == SRCPORTNAME_DOSDUKEAE)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_DOSDUKEAE;
		else if (l_srcport_name == SRCPORTNAME_DUKE3DW)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_DUKE3DW;
		else if (l_srcport_name == SRCPORTNAME_EDUKE32)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_EDUKE32;
		else if (l_srcport_name == SRCPORTNAME_NDUKE)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_NDUKE;
		else if (l_srcport_name == SRCPORTNAME_HDUKE)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_HDUKE;
		else if (l_srcport_name == SRCPORTNAME_XDUKE)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_XDUKE;

		if (m_spdn3dtcmodprofilechoice->GetSelection())
			g_configuration->gamelaunch_dn3dtcmod_profilename = m_spdn3dtcmodprofilechoice->GetStringSelection();
		else
			g_configuration->gamelaunch_dn3dtcmod_profilename = wxEmptyString;

		g_configuration->gamelaunch_dn3dlvl = g_GetDN3DLevelNumBySelection(m_spdn3depimapchoice->GetSelection());

		if (m_spdn3depimapradioBtn->GetValue())
			g_configuration->gamelaunch_dn3dusermap = wxEmptyString;
		else
			g_configuration->gamelaunch_dn3dusermap = m_spdn3dusermapchoice->GetStringSelection();

		g_configuration->gamelaunch_dn3dgametype_in_sp = (DN3DMPGameT_Type)m_spdn3dgametypechoice->GetSelection();

		g_configuration->gamelaunch_numoffakeplayers = m_spdn3dfakeplayerschoice->GetSelection();

		g_configuration->gamelaunch_enable_bot_ai = m_spdn3dbotscheckBox->GetValue();

		g_configuration->gamelaunch_dn3d_skill_in_sp = m_spdn3dskillchoice->GetSelection();

		g_configuration->gamelaunch_dn3dspawn = (DN3DSpawnType)m_spdn3dspawnchoice->GetSelection();

		g_configuration->gamelaunch_dn3d_args_in_sp = m_spdn3dextraargstextCtrl->GetValue();
	}

	if (HAVE_SW)
	{
		l_srcport_name = m_spswsourceportchoice->GetStringSelection();

		if (l_srcport_name == SRCPORTNAME_DOSSWSW)
			g_configuration->gamelaunch_sw_source_port = SOURCEPORT_DOSSWSW;
		else if (l_srcport_name == SRCPORTNAME_DOSSWRG)
			g_configuration->gamelaunch_sw_source_port = SOURCEPORT_DOSSWRG;
		else if (l_srcport_name == SRCPORTNAME_VOIDSW)
			g_configuration->gamelaunch_sw_source_port = SOURCEPORT_VOIDSW;
		else if (l_srcport_name == SRCPORTNAME_SWP)
			g_configuration->gamelaunch_sw_source_port = SOURCEPORT_SWP;

		if (m_spswtcmodprofilechoice->GetSelection())
			g_configuration->gamelaunch_swtcmod_profilename = m_spswtcmodprofilechoice->GetStringSelection();
		else
			g_configuration->gamelaunch_swtcmod_profilename = wxEmptyString;

		g_configuration->gamelaunch_swlvl = g_GetSWLevelNumBySelection(m_spswepimapchoice->GetSelection());

		if (m_spswepimapradioBtn->GetValue())
			g_configuration->gamelaunch_swusermap = wxEmptyString;
		else
			g_configuration->gamelaunch_swusermap = m_spswusermapchoice->GetStringSelection();

		g_configuration->gamelaunch_sw_skill_in_sp = m_spswskillchoice->GetSelection();

		g_configuration->gamelaunch_sw_args_in_sp = m_spswextraargstextCtrl->GetValue();
	}

	if (HAVE_CUSTOM)
	{
		g_configuration->gamelaunch_custom_profilename_in_sp = m_spcustomprofilechoice->GetStringSelection();

		g_configuration->custom_profile_current->extraargs = m_spcustomextraargstextCtrl->GetValue();
	}

	if (l_game_name == GAMENAME_BLOOD)
		g_configuration->gamelaunch_source_port = g_configuration->gamelaunch_blood_source_port;
	else if (l_game_name == GAMENAME_DESCENT)
		g_configuration->gamelaunch_source_port = g_configuration->gamelaunch_descent_source_port;
	else if (l_game_name == GAMENAME_DESCENT2)
		g_configuration->gamelaunch_source_port = g_configuration->gamelaunch_descent2_source_port;
	else if (l_game_name == GAMENAME_DN3D)
		g_configuration->gamelaunch_source_port = g_configuration->gamelaunch_dn3d_source_port;
	else if (l_game_name == GAMENAME_SW)
		g_configuration->gamelaunch_source_port = g_configuration->gamelaunch_sw_source_port;
	else if (l_game_name == GAMENAME_CUSTOM)
		g_configuration->gamelaunch_source_port = SOURCEPORT_CUSTOM;

	g_configuration->Save();
}
