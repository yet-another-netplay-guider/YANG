#!/bin/sh
if [ ! -d "$HOME/wxGTK-2.8.10" ]; then
  echo "You should have a ~/wxGTK-2.8.10 folder ready"
  echo "for compilation of wxGTK v2.8.10"
  exit 0
fi
cd ~/wxGTK-2.8.10
mkdir gtk-oss-32 gtk-oss-64 gtk-sdl-32 gtk-sdl-64
cd gtk-sdl-32
dchroot -d "linux32 ../configure --disable-shared --prefix=/home/ny00123/wxGTK-static-sdl-32 --disable-exceptions --enable-no_exceptions --enable-unicode --without-subdirs --without-libtiff --without-libjpeg --with-regex=builtin --with-zlib=builtin --with-expat=builtin --disable-compat26 --with-sdl --disable-joystick"
dchroot -d "linux32 make"
dchroot -d "linux32 make install"
cd ../gtk-oss-32
dchroot -d "linux32 ../configure --disable-shared --prefix=/home/ny00123/wxGTK-static-oss-32 --disable-exceptions --enable-no_exceptions --enable-unicode --without-subdirs --without-libtiff --without-libjpeg --with-regex=builtin --with-zlib=builtin --with-expat=builtin --disable-compat26 --disable-joystick"
dchroot -d "linux32 make"
dchroot -d "linux32 make install"
cd ../gtk-sdl-64
../configure --disable-shared --prefix=/home/ny00123/wxGTK-static-sdl-64 --disable-exceptions --enable-no_exceptions --enable-unicode --without-subdirs --without-libtiff --without-libjpeg --with-regex=builtin --with-zlib=builtin --with-expat=builtin --disable-compat26 --with-sdl --disable-joystick
make
make install
cd ../gtk-oss-64
../configure --disable-shared --prefix=/home/ny00123/wxGTK-static-oss-64 --disable-exceptions --enable-no_exceptions --enable-unicode --without-subdirs --without-libtiff --without-libjpeg --with-regex=builtin --with-zlib=builtin --with-expat=builtin --disable-compat26  --disable-joystick
make
make install

