/////////////////////////////////////////////////////////////////////////////
// Source of origin:        src/msw/utils.cpp, wxMSW v2.8.10
// Purpose:                 Get OS description
// Author:                  Julian Smart
// Modified by:             Turrican, NY00123
// Derived from:            wxGetOsDescription function
// Time of origin creation: 04/01/1998
// Time of modification:    01/11/2019
// Copyright:               (c) Julian Smart, Turrican, NY00123
// Licence:                 wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/platform.h>

#if (defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__)
#include <wx/utils.h>
#include <wx/string.h>
#endif

#endif

#ifdef __WXMSW__
#include <windows.h>
#include <wx/msw/winundef.h>
#endif

#if (defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__)
wxString g_GetOsDescription()
{
#ifdef __WXMSW__
    wxString str;

    OSVERSIONINFOEX info;
    ZeroMemory(&info, sizeof(info));

    info.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
    if ( GetVersionEx((OSVERSIONINFO *) &info) )
    {
        switch ( info.dwPlatformId )
        {
            case VER_PLATFORM_WIN32s:
                str = wxT("Win32s on Windows 3.1");
                break;

            case VER_PLATFORM_WIN32_WINDOWS:
                switch (info.dwMinorVersion)
                {
                    case 0:
                        str = wxT("Windows 95");

                        if ( info.szCSDVersion[1] == 'A' ||
                             info.szCSDVersion[1] == 'B' ||
                             info.szCSDVersion[1] == 'C' )
                        {
                            str << wxT(" ") << info.szCSDVersion[1];
                        }
                        break;
                    case 10:
                        str = wxT("Windows 98");

                        if ( info.szCSDVersion[1] == 'A' )
                        {
                            str << wxT(" SE");
                        }
                        break;
                    case 90:
                        str = wxT("Windows ME");
                        break;
                    default:
                        str.Printf(wxT("Windows 9x (%d.%d)"),
                                   info.dwMajorVersion,
                                   info.dwMinorVersion);
                        break;
                }
                break;

            case VER_PLATFORM_WIN32_NT:
                switch ( info.dwMajorVersion )
                {
                    case 5:
                        switch ( info.dwMinorVersion )
                        {
                            case 0:
                                if ( info.wProductType == VER_NT_WORKSTATION )
                                    str.Printf(wxT("Windows 2000 Professional"));
                                else
                                    str.Printf(wxT("Windows 2000 Server"));
                                break;

                            case 1:
                                str.Printf(wxT("Windows XP"));
                                break;

                            case 2:
                                if ( info.wProductType == VER_NT_WORKSTATION )
                                    str.Printf(wxT("Windows XP Version 2003"));
                                else
                                    str.Printf(wxT("Windows Server 2003"));
                                break;
                        }
                        break;

                    case 6:
                        switch ( info.dwMinorVersion )
                        {
                            case 0:
                                if ( info.wProductType == VER_NT_WORKSTATION )
                                    str.Printf(wxT("Windows Vista"));
                                else
                                    str.Printf(wxT("Windows Server 2008"));
                                break;

                            case 1:
                                if ( info.wProductType == VER_NT_WORKSTATION )
                                    str.Printf(wxT("Windows 7"));
                                else
                                    str.Printf(wxT("Windows Server 2008 R2"));
                                break;

                            case 2:
                                if ( info.wProductType == VER_NT_WORKSTATION )
                                    str.Printf(wxT("Windows 8"));
                                else
                                    str.Printf(wxT("Windows Server 2012"));
                                break;

                            case 3:
                                if ( info.wProductType == VER_NT_WORKSTATION )
                                    str.Printf(wxT("Windows 8.1"));
                                else
                                    str.Printf(wxT("Windows Server 2012 R2"));
                                break;
                        }
                        break;

                    case 10:
                        switch ( info.dwMinorVersion )
                        {
                            case 0:
                                if ( info.wProductType == VER_NT_WORKSTATION )
                                    str.Printf(wxT("Windows 10"));
                                else
                                    str.Printf(wxT("Windows Server 2016"));
                                break;
                        }
                        break;
                }

                if ( str.empty() )
                {
                    if ( info.wProductType == VER_NT_WORKSTATION )
                        str.Printf(wxT("Windows NT %lu.%lu Workstation"), info.dwMajorVersion, info.dwMinorVersion);
                    else
                        str.Printf(wxT("Windows NT %lu.%lu Server"), info.dwMajorVersion, info.dwMinorVersion);
                }

                if ( wxIsPlatform64Bit() )
                    str << wxT(" 64-bit");

                break;
        }
    }
    else
    {
        wxFAIL_MSG( wxT("GetVersionEx() failed") ); // should never happen
    }

    return str;
#else
	wxString str;

	int MajorVersion, MinorVersion;

	char *Codename[16] = {"Cheetah", "Puma", "Jaguar", "Panther", "Tiger", "Leopard", "Snow Leopard", "Lion", "Mountain Lion", "Mavericks", "Yosemite", "El Capitan", "Sierra", "High Sierra", "Mojave", "Catalina"};

	wxGetOsVersion(&MajorVersion, &MinorVersion);

	str = wxString::Format(wxT("Mac OS X v%x.%d"), MajorVersion, MinorVersion >> 4);

	if ( (unsigned int)(MinorVersion >> 4) < sizeof( Codename ) / sizeof( *Codename ) )
		str << wxString::Format(wxT(" \"%s\""), wxString::FromAscii(Codename[MinorVersion >> 4]).c_str());

	if ( wxIsPlatformLittleEndian() )
		str << wxT(" (Intel)");
	else
		str << wxT(" (PowerPC)");

	if ( wxIsPlatform64Bit() )
		str << wxT(" 64-bit");

	return str;
#endif
}
#endif

