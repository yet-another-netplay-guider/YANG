/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/filename.h>
#include <wx/msgdlg.h>
#include <wx/textfile.h>
#include <wx/gbsizer.h>
#endif

#include "mp_dialog.h"
#include "config.h"
#include "host_frame.h"
#include "mp_common.h"
#include "yang.h"
#include "theme.h"
#include "yang.xpm"
#include "comm_txt.h"

BEGIN_EVENT_TABLE(MPDialog, YANGDialog)

EVT_BUTTON(wxID_OK, MPDialog::OnOk)
EVT_BUTTON(wxID_CANCEL, MPDialog::OnCancel)

EVT_CHOICE(ID_MPBLOODSELECTSRCPORT, MPDialog::OnSrcPortSelect)
EVT_CHOICE(ID_MPDESCENTSELECTSRCPORT, MPDialog::OnSrcPortSelect)
EVT_CHOICE(ID_MPDESCENT2SELECTSRCPORT, MPDialog::OnSrcPortSelect)
EVT_CHOICE(ID_MPDN3DSELECTSRCPORT, MPDialog::OnSrcPortSelect)
EVT_CHOICE(ID_MPSWSELECTSRCPORT, MPDialog::OnSrcPortSelect)

EVT_CHOICE(ID_MPSELECTCUSTOMPROFILE, MPDialog::OnSelectCustomProfile)

EVT_CHECKBOX(ID_MPBLOODRECORDDEMO, MPDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_MPSWRECORDDEMO, MPDialog::OnCheckBoxClick)

EVT_CHECKBOX(ID_MPPASSPROTECTCHECK, MPDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_MPBLOODUSECRYPTICPASSAGE, MPDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_MPUSEPROFILENAME, MPDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_MPUSENULLMODEM, MPDialog::OnCheckBoxClick)
//EVT_CHECKBOX(ID_MPFORCEIP, MPDialog::OnCheckBoxClick)

EVT_RADIOBUTTON(ID_MPBLOODUSEEPIMAP, MPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_MPDESCENTUSEEPIMAP, MPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_MPDESCENT2USEEPIMAP, MPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_MPDN3DUSEEPIMAP, MPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_MPSWUSEEPIMAP, MPDialog::OnRadioBtnClick)

EVT_RADIOBUTTON(ID_MPBLOODUSEUSERMAP, MPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_MPDESCENTUSEUSERMAP, MPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_MPDESCENT2USEUSERMAP, MPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_MPDN3DUSEUSERMAP, MPDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_MPSWUSEUSERMAP, MPDialog::OnRadioBtnClick)

END_EVENT_TABLE()

MPDialog::MPDialog(bool isPassProtected) : YANGDialog(NULL, wxID_ANY, wxT("Create a room"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* MPDialogbSizer = new wxBoxSizer( wxVERTICAL );

	m_mpnotebook = new YANGNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );

	if (HAVE_BLOOD)
	{
		m_mpbloodpanel = new YANGPanel( m_mpnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* mpbloodpanelmainbSizer = new wxBoxSizer( wxVERTICAL );

		wxBoxSizer* mpbloodpanelsubbSizer = new wxBoxSizer( wxHORIZONTAL );

		wxGridBagSizer* mpbloodpanelleftgbSizer = new wxGridBagSizer( 0, 0 );
		mpbloodpanelleftgbSizer->SetFlexibleDirection( wxBOTH );
		mpbloodpanelleftgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpbloodsourceportstaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelleftgbSizer->Add( m_mpbloodsourceportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodsourceportchoice = new YANGChoice( m_mpbloodpanel, ID_MPBLOODSELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelleftgbSizer->Add( m_mpbloodsourceportchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpbloodusecrypticpassagecheckBox = new YANGCheckBox( m_mpbloodpanel, ID_MPBLOODUSECRYPTICPASSAGE, wxT("Use the Cryptic Passage add-on"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelleftgbSizer->Add( m_mpbloodusecrypticpassagecheckBox, wxGBPosition( 1, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodepimapradioBtn = new YANGRadioButton( m_mpbloodpanel, ID_MPBLOODUSEEPIMAP, wxT("Original map:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelleftgbSizer->Add( m_mpbloodepimapradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodepimapchoice = new YANGChoice( m_mpbloodpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelleftgbSizer->Add( m_mpbloodepimapchoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpbloodusermapradioBtn = new YANGRadioButton( m_mpbloodpanel, ID_MPBLOODUSEUSERMAP, wxT("User map:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelleftgbSizer->Add( m_mpbloodusermapradioBtn, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodusermapchoice = new YANGChoice( m_mpbloodpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelleftgbSizer->Add( m_mpbloodusermapchoice, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodgametypestaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Game type:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelleftgbSizer->Add( m_mpbloodgametypestaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodgametypechoice = new YANGChoice( m_mpbloodpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelleftgbSizer->Add( m_mpbloodgametypechoice, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpbloodskillstaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Game difficulty:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelleftgbSizer->Add( m_mpbloodskillstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodskillchoice = new YANGChoice( m_mpbloodpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelleftgbSizer->Add( m_mpbloodskillchoice, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpbloodmonsterstaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Monster settings:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelleftgbSizer->Add( m_mpbloodmonsterstaticText, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodmonsterchoice = new YANGChoice( m_mpbloodpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelleftgbSizer->Add( m_mpbloodmonsterchoice, wxGBPosition( 6, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpbloodweaponstaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Weapon settings:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelleftgbSizer->Add( m_mpbloodweaponstaticText, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodweaponchoice = new YANGChoice( m_mpbloodpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelleftgbSizer->Add( m_mpbloodweaponchoice, wxGBPosition( 7, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpblooditemstaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Item settings:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelleftgbSizer->Add( m_mpblooditemstaticText, wxGBPosition( 8, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpblooditemchoice = new YANGChoice( m_mpbloodpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelleftgbSizer->Add( m_mpblooditemchoice, wxGBPosition( 8, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpbloodrespawnstaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Respawn settings:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelleftgbSizer->Add( m_mpbloodrespawnstaticText, wxGBPosition( 9, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodrespawnchoice = new YANGChoice( m_mpbloodpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelleftgbSizer->Add( m_mpbloodrespawnchoice, wxGBPosition( 9, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		mpbloodpanelsubbSizer->Add( mpbloodpanelleftgbSizer, 1, wxEXPAND, 0 );

		wxGridBagSizer* mpbloodpanelrightgbSizer = new wxGridBagSizer( 0, 0 );
		mpbloodpanelrightgbSizer->SetFlexibleDirection( wxBOTH );
		mpbloodpanelrightgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpbloodroomnamestaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Room name:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelrightgbSizer->Add( m_mpbloodroomnamestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodroomnametextCtrl = new YANGTextCtrl( m_mpbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelrightgbSizer->Add( m_mpbloodroomnametextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpbloodmaxnumplayersstaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Max. number of players:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelrightgbSizer->Add( m_mpbloodmaxnumplayersstaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodmaxnumplayerschoice = new YANGChoice( m_mpbloodpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
		mpbloodpanelrightgbSizer->Add( m_mpbloodmaxnumplayerschoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodmaxpingstaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Max ping:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelrightgbSizer->Add( m_mpbloodmaxpingstaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodmaxpingtextCtrl = new YANGTextCtrl( m_mpbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
		mpbloodpanelrightgbSizer->Add( m_mpbloodmaxpingtextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodrecorddemocheckBox = new YANGCheckBox( m_mpbloodpanel, ID_MPBLOODRECORDDEMO, wxT("Record demo"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelrightgbSizer->Add( m_mpbloodrecorddemocheckBox, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodrecorddemotextCtrl = new YANGTextCtrl( m_mpbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelrightgbSizer->Add( m_mpbloodrecorddemotextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodextrahostargsstaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Extra args for host only:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelrightgbSizer->Add( m_mpbloodextrahostargsstaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodextrahostargstextCtrl = new YANGTextCtrl( m_mpbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelrightgbSizer->Add( m_mpbloodextrahostargstextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

#if EXTRA_ARGS_FOR_ALL
		m_mpbloodextraallargsstaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Extra args for everyone:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelrightgbSizer->Add( m_mpbloodextraallargsstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodextraallargstextCtrl = new YANGTextCtrl( m_mpbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpbloodpanelrightgbSizer->Add( m_mpbloodextraallargstextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
#endif

		m_mpbloodpacketmodestaticText = new wxStaticText( m_mpbloodpanel, wxID_ANY, wxT("Packet mode:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpbloodpanelrightgbSizer->Add( m_mpbloodpacketmodestaticText, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpbloodpacketmodechoice = new YANGChoice( m_mpbloodpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
		mpbloodpanelrightgbSizer->Add( m_mpbloodpacketmodechoice, wxGBPosition( 6, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		mpbloodpanelsubbSizer->Add( mpbloodpanelrightgbSizer, 1, wxEXPAND, 0 );

		mpbloodpanelmainbSizer->Add( mpbloodpanelsubbSizer, 1, wxEXPAND, 0 );

		m_mpbloodpanel->SetSizer( mpbloodpanelmainbSizer );
		m_mpbloodpanel->Layout();
		mpbloodpanelmainbSizer->Fit( m_mpbloodpanel );
		m_mpnotebook->AddPage( m_mpbloodpanel, GAMENAME_BLOOD );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODSW ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODRG ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODPP ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSBLOODOU)
		{
			m_mpnotebook->ChangeSelection(m_mpnotebook->GetPageCount()-1);
		}
	}


	if (HAVE_DESCENT)
	{
		m_mpdescentpanel = new YANGPanel( m_mpnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* mpdescentpanelmainbSizer = new wxBoxSizer( wxVERTICAL );

		wxBoxSizer* mpdescentpanelsubbSizer = new wxBoxSizer( wxHORIZONTAL );

		wxGridBagSizer* mpdescentpanelleftgbSizer = new wxGridBagSizer( 0, 0 );
		mpdescentpanelleftgbSizer->SetFlexibleDirection( wxBOTH );
		mpdescentpanelleftgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpdescentsourceportstaticText = new wxStaticText( m_mpdescentpanel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescentpanelleftgbSizer->Add( m_mpdescentsourceportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentsourceportchoice = new YANGChoice( m_mpdescentpanel, ID_MPDESCENTSELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescentpanelleftgbSizer->Add( m_mpdescentsourceportchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdescentepimapradioBtn = new YANGRadioButton( m_mpdescentpanel, ID_MPDESCENTUSEEPIMAP, wxT("Original mission:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescentpanelleftgbSizer->Add( m_mpdescentepimapradioBtn, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentepimapchoice = new YANGChoice( m_mpdescentpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescentpanelleftgbSizer->Add( m_mpdescentepimapchoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdescentusermapradioBtn = new YANGRadioButton( m_mpdescentpanel, ID_MPDESCENTUSEUSERMAP, wxT("User mission:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescentpanelleftgbSizer->Add( m_mpdescentusermapradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentusermapchoice = new YANGChoice( m_mpdescentpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescentpanelleftgbSizer->Add( m_mpdescentusermapchoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentgametypestaticText = new wxStaticText( m_mpdescentpanel, wxID_ANY, wxT("Game type:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescentpanelleftgbSizer->Add( m_mpdescentgametypestaticText, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentgametypechoice = new YANGChoice( m_mpdescentpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescentpanelleftgbSizer->Add( m_mpdescentgametypechoice, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		mpdescentpanelsubbSizer->Add( mpdescentpanelleftgbSizer, 1, wxEXPAND, 0 );

		wxGridBagSizer* mpdescentpanelrightgbSizer = new wxGridBagSizer( 0, 0 );
		mpdescentpanelrightgbSizer->SetFlexibleDirection( wxBOTH );
		mpdescentpanelrightgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpdescentroomnamestaticText = new wxStaticText( m_mpdescentpanel, wxID_ANY, wxT("Room name:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescentpanelrightgbSizer->Add( m_mpdescentroomnamestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentroomnametextCtrl = new YANGTextCtrl( m_mpdescentpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescentpanelrightgbSizer->Add( m_mpdescentroomnametextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdescentmaxnumplayersstaticText = new wxStaticText( m_mpdescentpanel, wxID_ANY, wxT("Max. number of players:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescentpanelrightgbSizer->Add( m_mpdescentmaxnumplayersstaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentmaxnumplayerschoice = new YANGChoice( m_mpdescentpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
		mpdescentpanelrightgbSizer->Add( m_mpdescentmaxnumplayerschoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentmaxpingstaticText = new wxStaticText( m_mpdescentpanel, wxID_ANY, wxT("Max ping:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescentpanelrightgbSizer->Add( m_mpdescentmaxpingstaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentmaxpingtextCtrl = new YANGTextCtrl( m_mpdescentpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
		mpdescentpanelrightgbSizer->Add( m_mpdescentmaxpingtextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentextrahostargsstaticText = new wxStaticText( m_mpdescentpanel, wxID_ANY, wxT("Extra args for host only:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescentpanelrightgbSizer->Add( m_mpdescentextrahostargsstaticText, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentextrahostargstextCtrl = new YANGTextCtrl( m_mpdescentpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescentpanelrightgbSizer->Add( m_mpdescentextrahostargstextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

#if EXTRA_ARGS_FOR_ALL
		m_mpdescentextraallargsstaticText = new wxStaticText( m_mpdescentpanel, wxID_ANY, wxT("Extra args for everyone:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescentpanelrightgbSizer->Add( m_mpdescentextraallargsstaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescentextraallargstextCtrl = new YANGTextCtrl( m_mpdescentpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescentpanelrightgbSizer->Add( m_mpdescentextraallargstextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
#endif

		mpdescentpanelsubbSizer->Add( mpdescentpanelrightgbSizer, 1, wxEXPAND, 0 );

		mpdescentpanelmainbSizer->Add( mpdescentpanelsubbSizer, 1, wxEXPAND, 0 );

		m_mpdescentpanel->SetSizer( mpdescentpanelmainbSizer );
		m_mpdescentpanel->Layout();
		mpdescentpanelmainbSizer->Fit( m_mpdescentpanel );
		m_mpnotebook->AddPage( m_mpdescentpanel, GAMENAME_DESCENT );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDESCENT ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_D1XREBIRTH)
		{
			m_mpnotebook->ChangeSelection(m_mpnotebook->GetPageCount()-1);
		}
	}


	if (HAVE_DESCENT2)
	{
		m_mpdescent2panel = new YANGPanel( m_mpnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* mpdescent2panelmainbSizer = new wxBoxSizer( wxVERTICAL );

		wxBoxSizer* mpdescent2panelsubbSizer = new wxBoxSizer( wxHORIZONTAL );

		wxGridBagSizer* mpdescent2panelleftgbSizer = new wxGridBagSizer( 0, 0 );
		mpdescent2panelleftgbSizer->SetFlexibleDirection( wxBOTH );
		mpdescent2panelleftgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpdescent2sourceportstaticText = new wxStaticText( m_mpdescent2panel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescent2panelleftgbSizer->Add( m_mpdescent2sourceportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2sourceportchoice = new YANGChoice( m_mpdescent2panel, ID_MPDESCENT2SELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescent2panelleftgbSizer->Add( m_mpdescent2sourceportchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdescent2epimapradioBtn = new YANGRadioButton( m_mpdescent2panel, ID_MPDESCENT2USEEPIMAP, wxT("Original mission:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescent2panelleftgbSizer->Add( m_mpdescent2epimapradioBtn, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2epimapchoice = new YANGChoice( m_mpdescent2panel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescent2panelleftgbSizer->Add( m_mpdescent2epimapchoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdescent2usermapradioBtn = new YANGRadioButton( m_mpdescent2panel, ID_MPDESCENT2USEUSERMAP, wxT("User mission:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescent2panelleftgbSizer->Add( m_mpdescent2usermapradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2usermapchoice = new YANGChoice( m_mpdescent2panel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescent2panelleftgbSizer->Add( m_mpdescent2usermapchoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2gametypestaticText = new wxStaticText( m_mpdescent2panel, wxID_ANY, wxT("Game type:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescent2panelleftgbSizer->Add( m_mpdescent2gametypestaticText, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2gametypechoice = new YANGChoice( m_mpdescent2panel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescent2panelleftgbSizer->Add( m_mpdescent2gametypechoice, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		mpdescent2panelsubbSizer->Add( mpdescent2panelleftgbSizer, 1, wxEXPAND, 0 );

		wxGridBagSizer* mpdescent2panelrightgbSizer = new wxGridBagSizer( 0, 0 );
		mpdescent2panelrightgbSizer->SetFlexibleDirection( wxBOTH );
		mpdescent2panelrightgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpdescent2roomnamestaticText = new wxStaticText( m_mpdescent2panel, wxID_ANY, wxT("Room name:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescent2panelrightgbSizer->Add( m_mpdescent2roomnamestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2roomnametextCtrl = new YANGTextCtrl( m_mpdescent2panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescent2panelrightgbSizer->Add( m_mpdescent2roomnametextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdescent2maxnumplayersstaticText = new wxStaticText( m_mpdescent2panel, wxID_ANY, wxT("Max. number of players:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescent2panelrightgbSizer->Add( m_mpdescent2maxnumplayersstaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2maxnumplayerschoice = new YANGChoice( m_mpdescent2panel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
		mpdescent2panelrightgbSizer->Add( m_mpdescent2maxnumplayerschoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2maxpingstaticText = new wxStaticText( m_mpdescent2panel, wxID_ANY, wxT("Max ping:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescent2panelrightgbSizer->Add( m_mpdescent2maxpingstaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2maxpingtextCtrl = new YANGTextCtrl( m_mpdescent2panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
		mpdescent2panelrightgbSizer->Add( m_mpdescent2maxpingtextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2extrahostargsstaticText = new wxStaticText( m_mpdescent2panel, wxID_ANY, wxT("Extra args for host only:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescent2panelrightgbSizer->Add( m_mpdescent2extrahostargsstaticText, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2extrahostargstextCtrl = new YANGTextCtrl( m_mpdescent2panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescent2panelrightgbSizer->Add( m_mpdescent2extrahostargstextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

#if EXTRA_ARGS_FOR_ALL
		m_mpdescent2extraallargsstaticText = new wxStaticText( m_mpdescent2panel, wxID_ANY, wxT("Extra args for everyone:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdescent2panelrightgbSizer->Add( m_mpdescent2extraallargsstaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdescent2extraallargstextCtrl = new YANGTextCtrl( m_mpdescent2panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdescent2panelrightgbSizer->Add( m_mpdescent2extraallargstextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
#endif

		mpdescent2panelsubbSizer->Add( mpdescent2panelrightgbSizer, 1, wxEXPAND, 0 );

		mpdescent2panelmainbSizer->Add( mpdescent2panelsubbSizer, 1, wxEXPAND, 0 );

		m_mpdescent2panel->SetSizer( mpdescent2panelmainbSizer );
		m_mpdescent2panel->Layout();
		mpdescent2panelmainbSizer->Fit( m_mpdescent2panel );
		m_mpnotebook->AddPage( m_mpdescent2panel, GAMENAME_DESCENT2 );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDESCENT2 ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_D2XREBIRTH)
		{
			m_mpnotebook->ChangeSelection(m_mpnotebook->GetPageCount()-1);
		}
	}


	if (HAVE_DN3D)
	{
		m_mpdn3dpanel = new YANGPanel( m_mpnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* mpdn3dpanelmainbSizer = new wxBoxSizer( wxVERTICAL );

		wxBoxSizer* mpdn3dpanelsubbSizer = new wxBoxSizer( wxHORIZONTAL );

		wxGridBagSizer* mpdn3dpanelleftgbSizer = new wxGridBagSizer( 0, 0 );
		mpdn3dpanelleftgbSizer->SetFlexibleDirection( wxBOTH );
		mpdn3dpanelleftgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpdn3dsourceportstaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dsourceportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dsourceportchoice = new YANGChoice( m_mpdn3dpanel, ID_MPDN3DSELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dsourceportchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdn3dtcmodprofilestaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("TC/MOD:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dtcmodprofilestaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dtcmodprofilechoice = new YANGChoice( m_mpdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dtcmodprofilechoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdn3depimapradioBtn = new YANGRadioButton( m_mpdn3dpanel, ID_MPDN3DUSEEPIMAP, wxT("Original map:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3depimapradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3depimapchoice = new YANGChoice( m_mpdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3depimapchoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdn3dusermapradioBtn = new YANGRadioButton( m_mpdn3dpanel, ID_MPDN3DUSEUSERMAP, wxT("User map:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dusermapradioBtn, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dusermapchoice = new YANGChoice( m_mpdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dusermapchoice, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dgametypestaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("Game type:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dgametypestaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dgametypechoice = new YANGChoice( m_mpdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dgametypechoice, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdn3dskillstaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("Skill (monsters):"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dskillstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dskillchoice = new YANGChoice( m_mpdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dskillchoice, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdn3dspawnstaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("Spawn:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dspawnstaticText, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dspawnchoice = new YANGChoice( m_mpdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdn3dpanelleftgbSizer->Add( m_mpdn3dspawnchoice, wxGBPosition( 6, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		mpdn3dpanelsubbSizer->Add( mpdn3dpanelleftgbSizer, 1, wxEXPAND, 0 );

		wxGridBagSizer* mpdn3dpanelrightgbSizer = new wxGridBagSizer( 0, 0 );
		mpdn3dpanelrightgbSizer->SetFlexibleDirection( wxBOTH );
		mpdn3dpanelrightgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpdn3droomnamestaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("Room name:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3droomnamestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3droomnametextCtrl = new YANGTextCtrl( m_mpdn3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3droomnametextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpdn3dmaxnumplayersstaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("Max. number of players:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3dmaxnumplayersstaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dmaxnumplayerschoice = new YANGChoice( m_mpdn3dpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3dmaxnumplayerschoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dmaxpingstaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("Max ping:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3dmaxpingstaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dmaxpingtextCtrl = new YANGTextCtrl( m_mpdn3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3dmaxpingtextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3drecorddemocheckBox = new YANGCheckBox( m_mpdn3dpanel, wxID_ANY, wxT("Record demo / hDuke mode select"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3drecorddemocheckBox, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dextrahostargsstaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("Extra args for host only:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3dextrahostargsstaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dextrahostargstextCtrl = new YANGTextCtrl( m_mpdn3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3dextrahostargstextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

#if EXTRA_ARGS_FOR_ALL
		m_mpdn3dextraallargsstaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("Extra args for everyone:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3dextraallargsstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dextraallargstextCtrl = new YANGTextCtrl( m_mpdn3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3dextraallargstextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
#endif

		m_mpdn3dconnectiontypestaticText = new wxStaticText( m_mpdn3dpanel, wxID_ANY, wxT("Connection type:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3dconnectiontypestaticText, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpdn3dconnectiontypechoice = new YANGChoice( m_mpdn3dpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
		mpdn3dpanelrightgbSizer->Add( m_mpdn3dconnectiontypechoice, wxGBPosition( 6, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		mpdn3dpanelsubbSizer->Add( mpdn3dpanelrightgbSizer, 1, wxEXPAND, 0 );

		mpdn3dpanelmainbSizer->Add( mpdn3dpanelsubbSizer, 1, wxEXPAND, 0 );

		m_mpdn3dpanel->SetSizer( mpdn3dpanelmainbSizer );
		m_mpdn3dpanel->Layout();
		mpdn3dpanelmainbSizer->Fit( m_mpdn3dpanel );
		m_mpnotebook->AddPage( m_mpdn3dpanel, GAMENAME_DN3D );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKESW ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKERG ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSDUKEAE ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DUKE3DW ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_EDUKE32 ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_NDUKE ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_HDUKE ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_XDUKE)
		{
			m_mpnotebook->ChangeSelection(m_mpnotebook->GetPageCount()-1);
		}
	}


	if (HAVE_SW)
	{
		m_mpswpanel = new YANGPanel( m_mpnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* mpswpanelmainbSizer = new wxBoxSizer( wxVERTICAL );

		wxBoxSizer* mpswpanelsubbSizer = new wxBoxSizer( wxHORIZONTAL );

		wxGridBagSizer* mpswpanelleftgbSizer = new wxGridBagSizer( 0, 0 );
		mpswpanelleftgbSizer->SetFlexibleDirection( wxBOTH );
		mpswpanelleftgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpswsourceportstaticText = new wxStaticText( m_mpswpanel, wxID_ANY, wxT("Source port:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelleftgbSizer->Add( m_mpswsourceportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswsourceportchoice = new YANGChoice( m_mpswpanel, ID_MPSWSELECTSRCPORT, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpswpanelleftgbSizer->Add( m_mpswsourceportchoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpswtcmodprofilestaticText = new wxStaticText( m_mpswpanel, wxID_ANY, wxT("TC/MOD:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelleftgbSizer->Add( m_mpswtcmodprofilestaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswtcmodprofilechoice = new YANGChoice( m_mpswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpswpanelleftgbSizer->Add( m_mpswtcmodprofilechoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpswepimapradioBtn = new YANGRadioButton( m_mpswpanel, ID_MPSWUSEEPIMAP, wxT("Original map:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelleftgbSizer->Add( m_mpswepimapradioBtn, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswepimapchoice = new YANGChoice( m_mpswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpswpanelleftgbSizer->Add( m_mpswepimapchoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpswusermapradioBtn = new YANGRadioButton( m_mpswpanel, ID_MPSWUSEUSERMAP, wxT("User map:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelleftgbSizer->Add( m_mpswusermapradioBtn, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswusermapchoice = new YANGChoice( m_mpswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpswpanelleftgbSizer->Add( m_mpswusermapchoice, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswgametypestaticText = new wxStaticText( m_mpswpanel, wxID_ANY, wxT("Game type:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelleftgbSizer->Add( m_mpswgametypestaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswgametypechoice = new YANGChoice( m_mpswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpswpanelleftgbSizer->Add( m_mpswgametypechoice, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpswskillstaticText = new wxStaticText( m_mpswpanel, wxID_ANY, wxT("Skill (monsters):"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelleftgbSizer->Add( m_mpswskillstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswskillchoice = new YANGChoice( m_mpswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpswpanelleftgbSizer->Add( m_mpswskillchoice, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		mpswpanelsubbSizer->Add( mpswpanelleftgbSizer, 1, wxEXPAND, 0 );

		wxGridBagSizer* mpswpanelrightgbSizer = new wxGridBagSizer( 0, 0 );
		mpswpanelrightgbSizer->SetFlexibleDirection( wxBOTH );
		mpswpanelrightgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpswroomnamestaticText = new wxStaticText( m_mpswpanel, wxID_ANY, wxT("Room name:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelrightgbSizer->Add( m_mpswroomnamestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswroomnametextCtrl = new YANGTextCtrl( m_mpswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpswpanelrightgbSizer->Add( m_mpswroomnametextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpswmaxnumplayersstaticText = new wxStaticText( m_mpswpanel, wxID_ANY, wxT("Max. number of players:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelrightgbSizer->Add( m_mpswmaxnumplayersstaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswmaxnumplayerschoice = new YANGChoice( m_mpswpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
		mpswpanelrightgbSizer->Add( m_mpswmaxnumplayerschoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswmaxpingstaticText = new wxStaticText( m_mpswpanel, wxID_ANY, wxT("Max ping:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelrightgbSizer->Add( m_mpswmaxpingstaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswmaxpingtextCtrl = new YANGTextCtrl( m_mpswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
		mpswpanelrightgbSizer->Add( m_mpswmaxpingtextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswrecorddemocheckBox = new YANGCheckBox( m_mpswpanel, ID_MPSWRECORDDEMO, wxT("Record demo"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelrightgbSizer->Add( m_mpswrecorddemocheckBox, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswrecorddemotextCtrl = new YANGTextCtrl( m_mpswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpswpanelrightgbSizer->Add( m_mpswrecorddemotextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswextrahostargsstaticText = new wxStaticText( m_mpswpanel, wxID_ANY, wxT("Extra args for host only:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelrightgbSizer->Add( m_mpswextrahostargsstaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswextrahostargstextCtrl = new YANGTextCtrl( m_mpswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpswpanelrightgbSizer->Add( m_mpswextrahostargstextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

#if EXTRA_ARGS_FOR_ALL
		m_mpswextraallargsstaticText = new wxStaticText( m_mpswpanel, wxID_ANY, wxT("Extra args for everyone:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpswpanelrightgbSizer->Add( m_mpswextraallargsstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpswextraallargstextCtrl = new YANGTextCtrl( m_mpswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpswpanelrightgbSizer->Add( m_mpswextraallargstextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
#endif

		mpswpanelsubbSizer->Add( mpswpanelrightgbSizer, 1, wxEXPAND, 0 );

		mpswpanelmainbSizer->Add( mpswpanelsubbSizer, 1, wxEXPAND, 0 );

		m_mpswpanel->SetSizer( mpswpanelmainbSizer );
		m_mpswpanel->Layout();
		mpswpanelmainbSizer->Fit( m_mpswpanel );
		m_mpnotebook->AddPage( m_mpswpanel, GAMENAME_SW );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_DOSSWSW ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_DOSSWRG ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_VOIDSW ||
			g_configuration->gamelaunch_source_port == SOURCEPORT_SWP)
		{
			m_mpnotebook->ChangeSelection(m_mpnotebook->GetPageCount()-1);
		}
	}


	if (HAVE_CUSTOM && g_configuration->have_custom_mpprofile)
	{
		m_mpcustompanel = new YANGPanel( m_mpnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
		wxBoxSizer* mpcustompanelmainbSizer = new wxBoxSizer( wxVERTICAL );

		wxBoxSizer* mpcustompanelsubbSizer = new wxBoxSizer( wxHORIZONTAL );

		wxGridBagSizer* mpcustompanelleftgbSizer = new wxGridBagSizer( 0, 0 );
		mpcustompanelleftgbSizer->SetFlexibleDirection( wxBOTH );
		mpcustompanelleftgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpcustomprofilestaticText = new wxStaticText( m_mpcustompanel, wxID_ANY, wxT("Profile:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpcustompanelleftgbSizer->Add( m_mpcustomprofilestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustomprofilechoice = new YANGChoice( m_mpcustompanel, ID_MPSELECTCUSTOMPROFILE, wxDefaultPosition, wxDLG_UNIT(this, wxSize(160, -1)) );
		mpcustompanelleftgbSizer->Add( m_mpcustomprofilechoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpcustomsummarystaticText = new wxStaticText( m_mpcustompanel, wxID_ANY, wxT("Summary:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpcustompanelleftgbSizer->Add( m_mpcustomsummarystaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustomsummarytextCtrl = new YANGTextCtrl( m_mpcustompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(-1, 140)), wxTE_MULTILINE|wxTE_READONLY );
		mpcustompanelleftgbSizer->Add( m_mpcustomsummarytextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

		mpcustompanelsubbSizer->Add( mpcustompanelleftgbSizer, 1, wxEXPAND, 0 );

		wxGridBagSizer* mpcustompanelrightgbSizer = new wxGridBagSizer( 0, 0 );
		mpcustompanelrightgbSizer->SetFlexibleDirection( wxBOTH );
		mpcustompanelrightgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

		m_mpcustomuseprofilenamecheckBox = new YANGCheckBox( m_mpcustompanel, ID_MPUSEPROFILENAME, wxT("Use profile's name as room name"), wxDefaultPosition, wxDefaultSize, 0 );
		mpcustompanelrightgbSizer->Add( m_mpcustomuseprofilenamecheckBox, wxGBPosition( 0, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustomroomnamestaticText = new wxStaticText( m_mpcustompanel, wxID_ANY, wxT("Room name:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpcustompanelrightgbSizer->Add( m_mpcustomroomnamestaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustomroomnametextCtrl = new YANGTextCtrl( m_mpcustompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpcustompanelrightgbSizer->Add( m_mpcustomroomnametextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

		m_mpcustommaxnumplayersstaticText = new wxStaticText( m_mpcustompanel, wxID_ANY, wxT("Max. number of players:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpcustompanelrightgbSizer->Add( m_mpcustommaxnumplayersstaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustommaxnumplayerschoice = new YANGChoice( m_mpcustompanel, wxID_ANY, wxDefaultPosition, wxDefaultSize );
		mpcustompanelrightgbSizer->Add( m_mpcustommaxnumplayerschoice, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustommaxpingstaticText = new wxStaticText( m_mpcustompanel, wxID_ANY, wxT("Max ping:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpcustompanelrightgbSizer->Add( m_mpcustommaxpingstaticText, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustommaxpingtextCtrl = new YANGTextCtrl( m_mpcustompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
		mpcustompanelrightgbSizer->Add( m_mpcustommaxpingtextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustomextrahostargsstaticText = new wxStaticText( m_mpcustompanel, wxID_ANY, wxT("Extra args for host only:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpcustompanelrightgbSizer->Add( m_mpcustomextrahostargsstaticText, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustomextrahostargstextCtrl = new YANGTextCtrl( m_mpcustompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpcustompanelrightgbSizer->Add( m_mpcustomextrahostargstextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

#if EXTRA_ARGS_FOR_ALL
		m_mpcustomextraallargsstaticText = new wxStaticText( m_mpcustompanel, wxID_ANY, wxT("Extra args for everyone:"), wxDefaultPosition, wxDefaultSize, 0 );
		mpcustompanelrightgbSizer->Add( m_mpcustomextraallargsstaticText, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustomextraallargstextCtrl = new YANGTextCtrl( m_mpcustompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
		mpcustompanelrightgbSizer->Add( m_mpcustomextraallargstextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
#endif

		m_mpcustomusenullmodemcheckBox = new YANGCheckBox( m_mpcustompanel, ID_MPUSENULLMODEM, wxT("Use Null-Modem (COM1) mode instead of LAN mode"), wxDefaultPosition, wxDefaultSize, 0 );
		mpcustompanelrightgbSizer->Add( m_mpcustomusenullmodemcheckBox, wxGBPosition( 6, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustomrxdelaystaticText = new wxStaticText( m_mpcustompanel, wxID_ANY, wxT("Null-Modem \"rxdelay\":"), wxDefaultPosition, wxDefaultSize, 0 );
		mpcustompanelrightgbSizer->Add( m_mpcustomrxdelaystaticText, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		m_mpcustomrxdelaytextCtrl = new YANGTextCtrl( m_mpcustompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
		mpcustompanelrightgbSizer->Add( m_mpcustomrxdelaytextCtrl, wxGBPosition( 7, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

		mpcustompanelsubbSizer->Add( mpcustompanelrightgbSizer, 1, wxEXPAND, 0 );

		mpcustompanelmainbSizer->Add( mpcustompanelsubbSizer, 1, wxEXPAND, 0 );

		m_mpcustompanel->SetSizer( mpcustompanelmainbSizer );
		m_mpcustompanel->Layout();
		mpcustompanelmainbSizer->Fit( m_mpcustompanel );
		m_mpnotebook->AddPage( m_mpcustompanel, GAMENAME_CUSTOM );

		if (g_configuration->gamelaunch_source_port == SOURCEPORT_CUSTOM)
			m_mpnotebook->ChangeSelection(m_mpnotebook->GetPageCount()-1);
	}


	MPDialogbSizer->Add( m_mpnotebook, 1, wxEXPAND | wxALL, 5 );

	m_mppanel = new YANGPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* mppanelmainbSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* mppanelsubbSizer = new wxBoxSizer( wxHORIZONTAL );

#if USE_PASSWORD
	wxGridBagSizer* mppanelleftgbSizer = new wxGridBagSizer( 0, 0 );
	mppanelleftgbSizer->SetFlexibleDirection( wxBOTH );
	mppanelleftgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_mppassprotectcheckBox = new YANGCheckBox( m_mppanel, ID_MPPASSPROTECTCHECK, wxT("Enable password protection:"), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mppassprotectcheckBox, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_passprotecttextCtrl = new YANGTextCtrl( m_mppanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_passprotecttextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_mpkeeppassprotectcheckBox = new YANGCheckBox( m_mppanel, wxID_ANY, wxT("Keep the room password protected in the following times."), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelleftgbSizer->Add( m_mpkeeppassprotectcheckBox, wxGBPosition( 1, 0 ), wxGBSpan( 1, 3 ), wxALL, 5 );

	mppanelsubbSizer->Add( mppanelleftgbSizer, 1, wxEXPAND, 0 );
#endif

	wxGridBagSizer* mppanelrightgbSizer = new wxGridBagSizer( 0, 0 );
	mppanelrightgbSizer->SetFlexibleDirection( wxBOTH );
	mppanelrightgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_mpadvertiseroomcheckBox = new YANGCheckBox( m_mppanel, wxID_ANY, wxT("Advertise room in the rooms list."), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpadvertiseroomcheckBox, wxGBPosition( 0, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_mpnodialogcheckBox = new YANGCheckBox( m_mppanel, wxID_ANY, wxT("Don't show this dialog in the following times."), wxDefaultPosition, wxDefaultSize, 0 );
	mppanelrightgbSizer->Add( m_mpnodialogcheckBox, wxGBPosition( 1, 0 ), wxGBSpan( 1, 3 ), wxALL, 5 );

	mppanelsubbSizer->Add( mppanelrightgbSizer, 1, wxEXPAND, 0 );

	mppanelmainbSizer->Add( mppanelsubbSizer, 1, wxEXPAND, 0 );

	m_mppanel->SetSizer( mppanelmainbSizer );
	m_mppanel->Layout();
	mppanelmainbSizer->Fit( m_mppanel );
	MPDialogbSizer->Add( m_mppanel, 0, wxEXPAND | wxALL, 5 );

	m_mpsdbSizer = new wxStdDialogButtonSizer();
	m_mpsdbSizerOK = new YANGButton( this, wxID_OK );
	m_mpsdbSizer->AddButton( m_mpsdbSizerOK );
	m_mpsdbSizerCancel = new YANGButton( this, wxID_CANCEL );
	m_mpsdbSizer->AddButton( m_mpsdbSizerCancel );
	m_mpsdbSizer->Realize();

	MPDialogbSizer->Add( m_mpsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxALL, 5 );

	SetSizer( MPDialogbSizer );
	Layout();
	MPDialogbSizer->Fit( this );

	Centre();
	SetIcon(wxIcon(yang_xpm));


	wxArrayString l_file_list;

	if (HAVE_BLOOD)
	{
		if (g_configuration->have_dosbloodsw && g_configuration->have_dosbox)
			m_mpbloodsourceportchoice->Append(SRCPORTNAME_DOSBLOODSW);
		if (g_configuration->have_dosbloodrg && g_configuration->have_dosbox)
			m_mpbloodsourceportchoice->Append(SRCPORTNAME_DOSBLOODRG);
		if (g_configuration->have_dosbloodpp && g_configuration->have_dosbox)
			m_mpbloodsourceportchoice->Append(SRCPORTNAME_DOSBLOODPP);
		if (g_configuration->have_dosbloodou && g_configuration->have_dosbox)
			m_mpbloodsourceportchoice->Append(SRCPORTNAME_DOSBLOODOU);

		switch (g_configuration->gamelaunch_blood_source_port)
		{
			case SOURCEPORT_DOSBLOODSW: m_mpbloodsourceportchoice->SetStringSelection(SRCPORTNAME_DOSBLOODSW); break;
			case SOURCEPORT_DOSBLOODRG: m_mpbloodsourceportchoice->SetStringSelection(SRCPORTNAME_DOSBLOODRG); break;
			case SOURCEPORT_DOSBLOODPP: m_mpbloodsourceportchoice->SetStringSelection(SRCPORTNAME_DOSBLOODPP); break;
			case SOURCEPORT_DOSBLOODOU: m_mpbloodsourceportchoice->SetStringSelection(SRCPORTNAME_DOSBLOODOU); break;
			default: ;
		}

		if (m_mpbloodsourceportchoice->GetSelection() == wxNOT_FOUND)
			m_mpbloodsourceportchoice->SetSelection(0);

		m_mpbloodusecrypticpassagecheckBox->Enable((m_mpbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODRG && wxFileExists(((wxFileName)g_configuration->dosbloodrg_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))) ||
												   (m_mpbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODPP && wxFileExists(((wxFileName)g_configuration->dosbloodpp_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))) ||
												   (m_mpbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODOU && wxFileExists(((wxFileName)g_configuration->dosbloodou_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))));

		if (m_mpbloodusecrypticpassagecheckBox->IsEnabled())
			m_mpbloodusecrypticpassagecheckBox->SetValue(g_configuration->gamelaunch_blood_use_crypticpassage);

		m_temp_bloodepimap = g_GetBloodLevelSelectionByNum(g_configuration->gamelaunch_bloodlvl);
		m_temp_bloodepimap_cp = g_GetBloodCPLevelSelectionByNum(g_configuration->gamelaunch_bloodlvl_cp);

		if (!m_mpbloodusecrypticpassagecheckBox->GetValue())
			AppendLevelsList(GAME_BLOOD, m_temp_bloodepimap);
		else
			AppendLevelsList(GAME_BLOOD, m_temp_bloodepimap_cp);

		l_file_list.Empty();
		g_AddAllBloodMapFiles(&l_file_list);
		m_mpbloodusermapchoice->Append(l_file_list);
		m_mpbloodusermapchoice->SetStringSelection(g_configuration->gamelaunch_bloodusermap);

		m_mpbloodgametypechoice->Append(*g_blood_gametype_list);
		m_mpbloodgametypechoice->SetSelection(g_configuration->gamelaunch_bloodgametype-1);

		m_mpbloodskillchoice->Append(*g_blood_skill_list);
		m_mpbloodskillchoice->SetSelection(g_configuration->gamelaunch_blood_skill);

		m_mpbloodmonsterchoice->Append(*g_blood_monstersettings_list);
		m_mpbloodmonsterchoice->SetSelection(g_configuration->gamelaunch_blood_monster);

		m_mpbloodweaponchoice->Append(*g_blood_weaponsettings_list);
		m_mpbloodweaponchoice->SetSelection(g_configuration->gamelaunch_blood_weapon);

		m_mpblooditemchoice->Append(*g_blood_itemsettings_list);
		m_mpblooditemchoice->SetSelection(g_configuration->gamelaunch_blood_item);

		m_mpbloodrespawnchoice->Append(*g_blood_respawnsettings_list);
		m_mpbloodrespawnchoice->SetSelection(g_configuration->gamelaunch_blood_respawn);

		m_mpbloodroomnametextCtrl->SetValue(g_configuration->gamelaunch_blood_roomname);

		for (int l_loop_var = 2; l_loop_var <= MAX_NUM_PLAYERS; l_loop_var++)
			m_mpbloodmaxnumplayerschoice->Append(wxString::Format(wxT("%d"), l_loop_var));
		m_mpbloodmaxnumplayerschoice->SetSelection(g_configuration->gamelaunch_blood_numofplayers - 2);

		m_mpbloodmaxpingtextCtrl->SetValue(wxString::Format(wxT("%ld"), g_configuration->gamelaunch_blood_maxping));

		m_mpbloodextrahostargstextCtrl->SetValue(g_configuration->gamelaunch_blood_args_for_host);

#if EXTRA_ARGS_FOR_ALL
		m_mpbloodextraallargstextCtrl->SetValue(g_configuration->gamelaunch_blood_args_for_all);
#endif

		m_mpbloodpacketmodechoice->Append(*g_blood_packetmode_list);
		m_mpbloodpacketmodechoice->SetSelection(g_configuration->gamelaunch_blood_packetmode);

		if (m_mpbloodusermapchoice->IsEmpty() || m_mpbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODSW)
		{
			m_mpbloodepimapradioBtn->SetValue(true);
			m_mpbloodepimapchoice->Enable();
			m_mpbloodusermapradioBtn->Disable();
			m_mpbloodusermapchoice->Disable();

			if (!m_mpbloodusermapchoice->IsEmpty() && m_mpbloodusermapchoice->GetSelection() == wxNOT_FOUND)
				m_mpbloodusermapchoice->SetSelection(0);
		}
		else
		{
			m_mpbloodusermapradioBtn->Enable();
			if (m_mpbloodusermapchoice->GetSelection() == wxNOT_FOUND)
			{
				m_mpbloodepimapradioBtn->SetValue(true);
				m_mpbloodepimapchoice->Enable();
				m_mpbloodusermapchoice->SetSelection(0);
				m_mpbloodusermapchoice->Disable();
			}
			else
			{
				m_mpbloodusermapradioBtn->SetValue(true);
				m_mpbloodepimapchoice->Disable();
				m_mpbloodusermapchoice->Enable();
			}
		}

		m_mpbloodrecorddemotextCtrl->Disable();
	}

	if (HAVE_DESCENT)
	{
		if (g_configuration->have_dosdescent && g_configuration->have_dosbox)
			m_mpdescentsourceportchoice->Append(SRCPORTNAME_DOSDESCENT);
		if (g_configuration->have_d1xrebirth)
			m_mpdescentsourceportchoice->Append(SRCPORTNAME_D1XREBIRTH);

		switch (g_configuration->gamelaunch_descent_source_port)
		{
			case SOURCEPORT_DOSDESCENT: m_mpdescentsourceportchoice->SetStringSelection(SRCPORTNAME_DOSDESCENT); break;
			case SOURCEPORT_D1XREBIRTH: m_mpdescentsourceportchoice->SetStringSelection(SRCPORTNAME_D1XREBIRTH); break;
			default: ;
		}

		if (m_mpdescentsourceportchoice->GetSelection() == wxNOT_FOUND)
			m_mpdescentsourceportchoice->SetSelection(0);

		m_mpdescentepimapchoice->Append(*g_descent_level_list);
		m_mpdescentepimapchoice->SetSelection(g_configuration->gamelaunch_descentlvl);

		l_file_list.Empty();
		g_AddAllDescentMapFiles(&l_file_list, false);
		m_mpdescentusermapchoice->Append(l_file_list);
		m_mpdescentusermapchoice->SetStringSelection(g_configuration->gamelaunch_descentusermap);

		m_mpdescentgametypechoice->Append(*g_descent_gametype_list);
		m_mpdescentgametypechoice->SetSelection(g_configuration->gamelaunch_descentgametype);

		m_mpdescentroomnametextCtrl->SetValue(g_configuration->gamelaunch_descent_roomname);

		for (int l_loop_var = 2; l_loop_var <= MAX_NUM_PLAYERS; l_loop_var++)
			m_mpdescentmaxnumplayerschoice->Append(wxString::Format(wxT("%d"), l_loop_var));
		m_mpdescentmaxnumplayerschoice->SetSelection(g_configuration->gamelaunch_descent_numofplayers - 2);

		m_mpdescentmaxpingtextCtrl->SetValue(wxString::Format(wxT("%ld"), g_configuration->gamelaunch_descent_maxping));

		m_mpdescentextrahostargstextCtrl->SetValue(g_configuration->gamelaunch_descent_args_for_host);

#if EXTRA_ARGS_FOR_ALL
		m_mpdescentextraallargstextCtrl->SetValue(g_configuration->gamelaunch_descent_args_for_all);
#endif

		if (m_mpdescentusermapchoice->IsEmpty())
		{
			m_mpdescentepimapradioBtn->SetValue(true);
			m_mpdescentepimapchoice->Enable();
			m_mpdescentusermapradioBtn->Disable();
			m_mpdescentusermapchoice->Disable();

			if (!m_mpdescentusermapchoice->IsEmpty() && m_mpdescentusermapchoice->GetSelection() == wxNOT_FOUND)
				m_mpdescentusermapchoice->SetSelection(0);
		}
		else
		{
			m_mpdescentusermapradioBtn->Enable();
			if (m_mpdescentusermapchoice->GetSelection() == wxNOT_FOUND)
			{
				m_mpdescentepimapradioBtn->SetValue(true);
				m_mpdescentepimapchoice->Enable();
				m_mpdescentusermapchoice->SetSelection(0);
				m_mpdescentusermapchoice->Disable();
			}
			else
			{
				m_mpdescentusermapradioBtn->SetValue(true);
				m_mpdescentepimapchoice->Disable();
				m_mpdescentusermapchoice->Enable();
			}
		}
	}

	if (HAVE_DESCENT2)
	{
		if (g_configuration->have_dosdescent2 && g_configuration->have_dosbox)
			m_mpdescent2sourceportchoice->Append(SRCPORTNAME_DOSDESCENT2);
		if (g_configuration->have_d2xrebirth)
			m_mpdescent2sourceportchoice->Append(SRCPORTNAME_D2XREBIRTH);

		switch (g_configuration->gamelaunch_descent2_source_port)
		{
			case SOURCEPORT_DOSDESCENT2: m_mpdescent2sourceportchoice->SetStringSelection(SRCPORTNAME_DOSDESCENT2); break;
			case SOURCEPORT_D2XREBIRTH:  m_mpdescent2sourceportchoice->SetStringSelection(SRCPORTNAME_D2XREBIRTH); break;
			default: ;
		}

		if (m_mpdescent2sourceportchoice->GetSelection() == wxNOT_FOUND)
			m_mpdescent2sourceportchoice->SetSelection(0);

		m_mpdescent2epimapchoice->Append(*g_descent2_level_list);
		m_mpdescent2epimapchoice->SetSelection(g_configuration->gamelaunch_descent2lvl);

		l_file_list.Empty();
		g_AddAllDescent2MapFiles(&l_file_list, false);
		m_mpdescent2usermapchoice->Append(l_file_list);
		m_mpdescent2usermapchoice->SetStringSelection(g_configuration->gamelaunch_descent2usermap);

		m_mpdescent2gametypechoice->Append(*g_descent2_gametype_list);
		m_mpdescent2gametypechoice->SetSelection(g_configuration->gamelaunch_descent2gametype);

		m_mpdescent2roomnametextCtrl->SetValue(g_configuration->gamelaunch_descent2_roomname);

		for (int l_loop_var = 2; l_loop_var <= MAX_NUM_PLAYERS; l_loop_var++)
			m_mpdescent2maxnumplayerschoice->Append(wxString::Format(wxT("%d"), l_loop_var));
		m_mpdescent2maxnumplayerschoice->SetSelection(g_configuration->gamelaunch_descent2_numofplayers - 2);

		m_mpdescent2maxpingtextCtrl->SetValue(wxString::Format(wxT("%ld"), g_configuration->gamelaunch_descent2_maxping));

		m_mpdescent2extrahostargstextCtrl->SetValue(g_configuration->gamelaunch_descent2_args_for_host);

#if EXTRA_ARGS_FOR_ALL
		m_mpdescent2extraallargstextCtrl->SetValue(g_configuration->gamelaunch_descent2_args_for_all);
#endif

		if (m_mpdescent2usermapchoice->IsEmpty())
		{
			m_mpdescent2epimapradioBtn->SetValue(true);
			m_mpdescent2epimapchoice->Enable();
			m_mpdescent2usermapradioBtn->Disable();
			m_mpdescent2usermapchoice->Disable();

			if (!m_mpdescent2usermapchoice->IsEmpty() && m_mpdescent2usermapchoice->GetSelection() == wxNOT_FOUND)
				m_mpdescent2usermapchoice->SetSelection(0);
		}
		else
		{
			m_mpdescent2usermapradioBtn->Enable();
			if (m_mpdescent2usermapchoice->GetSelection() == wxNOT_FOUND)
			{
				m_mpdescent2epimapradioBtn->SetValue(true);
				m_mpdescent2epimapchoice->Enable();
				m_mpdescent2usermapchoice->SetSelection(0);
				m_mpdescent2usermapchoice->Disable();
			}
			else
			{
				m_mpdescent2usermapradioBtn->SetValue(true);
				m_mpdescent2epimapchoice->Disable();
				m_mpdescent2usermapchoice->Enable();
			}
		}
	}

	if (HAVE_DN3D)
	{
		if (g_configuration->have_dosdukesw && g_configuration->have_dosbox)
			m_mpdn3dsourceportchoice->Append(SRCPORTNAME_DOSDUKESW);
		if (g_configuration->have_dosdukerg && g_configuration->have_dosbox)
			m_mpdn3dsourceportchoice->Append(SRCPORTNAME_DOSDUKERG);
		if (g_configuration->have_dosdukeae && g_configuration->have_dosbox)
			m_mpdn3dsourceportchoice->Append(SRCPORTNAME_DOSDUKEAE);
		if (g_configuration->have_duke3dw)
			m_mpdn3dsourceportchoice->Append(SRCPORTNAME_DUKE3DW);
		if (g_configuration->have_eduke32)
			m_mpdn3dsourceportchoice->Append(SRCPORTNAME_EDUKE32);
		if (g_configuration->have_nduke)
			m_mpdn3dsourceportchoice->Append(SRCPORTNAME_NDUKE);
		if (g_configuration->have_hduke)
			m_mpdn3dsourceportchoice->Append(SRCPORTNAME_HDUKE);
		if (g_configuration->have_xduke)
			m_mpdn3dsourceportchoice->Append(SRCPORTNAME_XDUKE);

		switch (g_configuration->gamelaunch_dn3d_source_port)
		{
			case SOURCEPORT_DOSDUKESW: m_mpdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_DOSDUKESW); break;
			case SOURCEPORT_DOSDUKERG: m_mpdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_DOSDUKERG); break;
			case SOURCEPORT_DOSDUKEAE: m_mpdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_DOSDUKEAE); break;
			case SOURCEPORT_DUKE3DW:   m_mpdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_DUKE3DW); break;
			case SOURCEPORT_EDUKE32:   m_mpdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_EDUKE32); break;
			case SOURCEPORT_NDUKE:     m_mpdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_NDUKE); break;
			case SOURCEPORT_HDUKE:     m_mpdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_HDUKE); break;
			case SOURCEPORT_XDUKE:     m_mpdn3dsourceportchoice->SetStringSelection(SRCPORTNAME_XDUKE); break;
			default: ;
		}

		if (m_mpdn3dsourceportchoice->GetSelection() == wxNOT_FOUND)
			m_mpdn3dsourceportchoice->SetSelection(0);

		m_mpdn3dtcmodprofilechoice->Append(wxT("None"));

		if (g_configuration->dn3dtcmod_profile_list)
		{
			g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

			while (g_configuration->dn3dtcmod_profile_current)
			{
				m_mpdn3dtcmodprofilechoice->Append(g_configuration->dn3dtcmod_profile_current->profilename);

				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;
			}

			m_mpdn3dtcmodprofilechoice->SetStringSelection(g_configuration->gamelaunch_dn3dtcmod_profilename);

			if (m_mpdn3dtcmodprofilechoice->GetSelection() == wxNOT_FOUND)
			{
				m_mpdn3dtcmodprofilechoice->SetSelection(0);

				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;
			}
			else
			{
				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

				while (g_configuration->dn3dtcmod_profile_current->profilename != g_configuration->gamelaunch_dn3dtcmod_profilename)
					g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;
			}
		}
		else
		{
			m_mpdn3dtcmodprofilechoice->SetSelection(0);
			m_mpdn3dtcmodprofilechoice->Disable();
		}

		AppendLevelsList(GAME_DN3D, g_GetDN3DLevelSelectionByNum(g_configuration->gamelaunch_dn3dlvl));

		l_file_list.Empty();
		g_AddAllDN3DMapFiles(&l_file_list);
		m_mpdn3dusermapchoice->Append(l_file_list);
		m_mpdn3dusermapchoice->SetStringSelection(g_configuration->gamelaunch_dn3dusermap);

		for (size_t l_loop_var = 0; l_loop_var < (size_t)(m_mpdn3dsourceportchoice->GetStringSelection() != SRCPORTNAME_EDUKE32 ? 3 : 5); l_loop_var++)
			m_mpdn3dgametypechoice->Append((*g_dn3d_gametype_list)[l_loop_var]);

		m_mpdn3dgametypechoice->SetSelection(g_configuration->gamelaunch_dn3dgametype-1);

		if (m_mpdn3dgametypechoice->GetSelection() == wxNOT_FOUND)
			m_mpdn3dgametypechoice->SetSelection(0);

		m_mpdn3dskillchoice->Append(*g_dn3d_skill_list);
		m_mpdn3dskillchoice->SetSelection(g_configuration->gamelaunch_dn3d_skill);

		m_mpdn3dspawnchoice->Append(*g_dn3d_spawn_list);
		m_mpdn3dspawnchoice->SetSelection(g_configuration->gamelaunch_dn3dspawn);

		m_mpdn3droomnametextCtrl->SetValue(g_configuration->gamelaunch_dn3d_roomname);

		for (int l_loop_var = 2; l_loop_var <= MAX_NUM_PLAYERS; l_loop_var++)
			m_mpdn3dmaxnumplayerschoice->Append(wxString::Format(wxT("%d"), l_loop_var));
		m_mpdn3dmaxnumplayerschoice->SetSelection(g_configuration->gamelaunch_dn3d_numofplayers - 2);

		m_mpdn3dmaxpingtextCtrl->SetValue(wxString::Format(wxT("%ld"), g_configuration->gamelaunch_dn3d_maxping));

		m_mpdn3dextrahostargstextCtrl->SetValue(g_configuration->gamelaunch_dn3d_args_for_host);

#if EXTRA_ARGS_FOR_ALL
		m_mpdn3dextraallargstextCtrl->SetValue(g_configuration->gamelaunch_dn3d_args_for_all);
#endif

		m_mpdn3dconnectiontypechoice->Append((*g_dn3d_connectiontype_list)[CONNECTIONTYPE_PEER2PEER]);

		if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DUKE3DW || m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_EDUKE32)
			m_mpdn3dconnectiontypechoice->Append((*g_dn3d_connectiontype_list)[CONNECTIONTYPE_MASTERSLAVE]);

		if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_EDUKE32)
			m_mpdn3dconnectiontypechoice->Append((*g_dn3d_connectiontype_list)[CONNECTIONTYPE_SERVERCLIENT]);

		if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DUKE3DW)
			m_mpdn3dconnectiontypechoice->SetSelection(g_configuration->gamelaunch_duke3dw_connectiontype);
		else if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_EDUKE32)
			m_mpdn3dconnectiontypechoice->SetSelection(g_configuration->gamelaunch_eduke32_connectiontype);
		else
			m_mpdn3dconnectiontypechoice->SetSelection(CONNECTIONTYPE_PEER2PEER);

		m_mpdn3dconnectiontypechoice->Enable(m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DUKE3DW || m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_EDUKE32);

		if (m_mpdn3dusermapchoice->IsEmpty() || m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKESW)
		{
			m_mpdn3depimapradioBtn->SetValue(true);
			m_mpdn3depimapchoice->Enable();
			m_mpdn3dusermapradioBtn->Disable();
			m_mpdn3dusermapchoice->Disable();

			if (!m_mpdn3dusermapchoice->IsEmpty() && m_mpdn3dusermapchoice->GetSelection() == wxNOT_FOUND)
				m_mpdn3dusermapchoice->SetSelection(0);
		}
		else
		{
			m_mpdn3dusermapradioBtn->Enable();
			if (m_mpdn3dusermapchoice->GetSelection() == wxNOT_FOUND)
			{
				m_mpdn3depimapradioBtn->SetValue(true);
				m_mpdn3depimapchoice->Enable();
				m_mpdn3dusermapchoice->SetSelection(0);
				m_mpdn3dusermapchoice->Disable();
			}
			else
			{
				m_mpdn3dusermapradioBtn->SetValue(true);
				m_mpdn3depimapchoice->Disable();
				m_mpdn3dusermapchoice->Enable();
			}
		}
	}

	if (HAVE_SW)
	{
		if (g_configuration->have_dosswsw && g_configuration->have_dosbox)
			m_mpswsourceportchoice->Append(SRCPORTNAME_DOSSWSW);
		if (g_configuration->have_dosswrg && g_configuration->have_dosbox)
			m_mpswsourceportchoice->Append(SRCPORTNAME_DOSSWRG);
		if (g_configuration->have_voidsw)
			m_mpswsourceportchoice->Append(SRCPORTNAME_VOIDSW);
		if (g_configuration->have_swp)
			m_mpswsourceportchoice->Append(SRCPORTNAME_SWP);

		switch (g_configuration->gamelaunch_sw_source_port)
		{
			case SOURCEPORT_DOSSWSW: m_mpswsourceportchoice->SetStringSelection(SRCPORTNAME_DOSSWSW); break;
			case SOURCEPORT_DOSSWRG: m_mpswsourceportchoice->SetStringSelection(SRCPORTNAME_DOSSWRG); break;
			case SOURCEPORT_VOIDSW:  m_mpswsourceportchoice->SetStringSelection(SRCPORTNAME_VOIDSW); break;
			case SOURCEPORT_SWP:     m_mpswsourceportchoice->SetStringSelection(SRCPORTNAME_SWP); break;
			default: ;
		}

		if (m_mpswsourceportchoice->GetSelection() == wxNOT_FOUND)
			m_mpswsourceportchoice->SetSelection(0);

		m_mpswtcmodprofilechoice->Append(wxT("None"));

		if (g_configuration->swtcmod_profile_list)
		{
			g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

			while (g_configuration->swtcmod_profile_current)
			{
				m_mpswtcmodprofilechoice->Append(g_configuration->swtcmod_profile_current->profilename);

				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;
			}

			m_mpswtcmodprofilechoice->SetStringSelection(g_configuration->gamelaunch_swtcmod_profilename);

			if (m_mpswtcmodprofilechoice->GetSelection() == wxNOT_FOUND)
			{
				m_mpswtcmodprofilechoice->SetSelection(0);

				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;
			}
			else
			{
				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

				while (g_configuration->swtcmod_profile_current->profilename != g_configuration->gamelaunch_swtcmod_profilename)
					g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;
			}
		}
		else
		{
			m_mpswtcmodprofilechoice->SetSelection(0);
			m_mpswtcmodprofilechoice->Disable();
		}

		AppendLevelsList(GAME_SW, g_GetSWLevelSelectionByNum(g_configuration->gamelaunch_swlvl));

		l_file_list.Empty();
		g_AddAllSWMapFiles(&l_file_list);
		m_mpswusermapchoice->Append(l_file_list);
		m_mpswusermapchoice->SetStringSelection(g_configuration->gamelaunch_swusermap);

		m_mpswgametypechoice->Append(*g_sw_gametype_list);
		m_mpswgametypechoice->SetSelection(g_configuration->gamelaunch_swgametype);
		m_mpswgametypechoice->Enable(m_mpswsourceportchoice->GetStringSelection() == SRCPORTNAME_SWP);

		m_mpswskillchoice->Append(*g_sw_skill_list);
		m_mpswskillchoice->SetSelection(g_configuration->gamelaunch_sw_skill);

		m_mpswroomnametextCtrl->SetValue(g_configuration->gamelaunch_sw_roomname);

		for (int l_loop_var = 2; l_loop_var <= MAX_NUM_PLAYERS; l_loop_var++)
			m_mpswmaxnumplayerschoice->Append(wxString::Format(wxT("%d"), l_loop_var));
		m_mpswmaxnumplayerschoice->SetSelection(g_configuration->gamelaunch_sw_numofplayers - 2);

		m_mpswmaxpingtextCtrl->SetValue(wxString::Format(wxT("%ld"), g_configuration->gamelaunch_sw_maxping));

		m_mpswextrahostargstextCtrl->SetValue(g_configuration->gamelaunch_sw_args_for_host);

#if EXTRA_ARGS_FOR_ALL
		m_mpswextraallargstextCtrl->SetValue(g_configuration->gamelaunch_sw_args_for_all);
#endif

		if (m_mpswusermapchoice->IsEmpty() || m_mpswsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSSWSW)
		{
			m_mpswepimapradioBtn->SetValue(true);
			m_mpswepimapchoice->Enable();
			m_mpswusermapradioBtn->Disable();
			m_mpswusermapchoice->Disable();

			if (!m_mpswusermapchoice->IsEmpty() && m_mpswusermapchoice->GetSelection() == wxNOT_FOUND)
				m_mpswusermapchoice->SetSelection(0);
		}
		else
		{
			m_mpswusermapradioBtn->Enable();
			if (m_mpswusermapchoice->GetSelection() == wxNOT_FOUND)
			{
				m_mpswepimapradioBtn->SetValue(true);
				m_mpswepimapchoice->Enable();
				m_mpswusermapchoice->SetSelection(0);
				m_mpswusermapchoice->Disable();
			}
			else
			{
				m_mpswusermapradioBtn->SetValue(true);
				m_mpswepimapchoice->Disable();
				m_mpswusermapchoice->Enable();
			}
		}

		m_mpswrecorddemotextCtrl->Disable();
	}

	if (HAVE_CUSTOM && g_configuration->have_custom_mpprofile)
	{
		g_configuration->custom_profile_current = g_configuration->custom_profile_list;

		while (g_configuration->custom_profile_current)
		{
			if (!g_configuration->custom_profile_current->spgameonly)
				m_mpcustomprofilechoice->Append(g_configuration->custom_profile_current->profilename);

			g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
		}

		m_mpcustomprofilechoice->SetStringSelection(g_configuration->gamelaunch_custom_profilename);

		if (m_mpcustomprofilechoice->GetSelection() == wxNOT_FOUND)
		{
			m_mpcustomprofilechoice->SetSelection(0);

			g_configuration->custom_profile_current = g_configuration->custom_profile_list;

			while (g_configuration->custom_profile_current->spgameonly)
				g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
		}
		else
		{
			g_configuration->custom_profile_current = g_configuration->custom_profile_list;

			while (g_configuration->custom_profile_current->profilename != g_configuration->gamelaunch_custom_profilename)
				g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
		}

		UpdateSummaryList();


		m_mpcustomuseprofilenamecheckBox->SetValue(g_configuration->gamelaunch_custom_use_profilename);

		m_mpcustomroomnametextCtrl->Enable(!g_configuration->gamelaunch_custom_use_profilename);

		if (g_configuration->gamelaunch_custom_use_profilename)
			m_mpcustomroomnametextCtrl->SetValue(g_configuration->custom_profile_current->profilename);
		else
			m_mpcustomroomnametextCtrl->SetValue(g_configuration->gamelaunch_custom_roomname);

		m_mpcustommaxnumplayerschoice->Enable(!g_configuration->gamelaunch_custom_use_nullmodem);

		for (int l_loop_var = 2; l_loop_var <= MAX_NUM_PLAYERS; l_loop_var++)
			m_mpcustommaxnumplayerschoice->Append(wxString::Format(wxT("%d"), l_loop_var));

		m_temp_custommaxnumplayer = g_configuration->gamelaunch_custom_numofplayers;

		if (g_configuration->gamelaunch_custom_use_nullmodem)
			m_mpcustommaxnumplayerschoice->SetSelection(0);
		else
			m_mpcustommaxnumplayerschoice->SetSelection(m_temp_custommaxnumplayer - 2);

		m_mpcustommaxpingtextCtrl->SetValue(wxString::Format(wxT("%ld"), g_configuration->gamelaunch_custom_maxping));

		m_mpcustomusenullmodemcheckBox->SetValue(g_configuration->gamelaunch_custom_use_nullmodem);

		m_mpcustomrxdelaystaticText->Enable(g_configuration->gamelaunch_custom_use_nullmodem);
		m_mpcustomrxdelaytextCtrl->Enable(g_configuration->gamelaunch_custom_use_nullmodem);
		m_mpcustomrxdelaytextCtrl->SetValue(wxString::Format(wxT("%ld"), g_configuration->gamelaunch_custom_rxdelay));

		m_mpcustomextrahostargstextCtrl->SetValue(g_configuration->custom_profile_current->extrahostargs);

#if EXTRA_ARGS_FOR_ALL
		m_mpcustomextraallargstextCtrl->SetValue(g_configuration->custom_profile_current->extraallargs);
#endif
	}


#if USE_PASSWORD
	// We could save the password setting, or just open dialog from within the host frame.
	// while it's password protected.
	m_mppassprotectcheckBox->SetValue(isPassProtected || g_configuration->gamelaunch_use_password);

	m_passprotecttextCtrl->SetValue(g_configuration->gamelaunch_password);

	m_mpkeeppassprotectcheckBox->SetValue(g_configuration->gamelaunch_use_password);

	m_passprotecttextCtrl->Enable(m_mppassprotectcheckBox->GetValue());
	m_mpkeeppassprotectcheckBox->Enable(m_mppassprotectcheckBox->GetValue());
#endif

	m_mpadvertiseroomcheckBox->SetValue(g_configuration->gamelaunch_advertise);
	m_mpnodialogcheckBox->SetValue(g_configuration->gamelaunch_skip_settings);
}
/*
MPDialog::~MPDialog()
{
g_main_frame->Show(true);
g_mp_dialog = NULL;
}
*/
void MPDialog::AppendLevelsList(GameType game, int level_selection)
{
	size_t l_loop_var;

	switch (game)
	{
		case GAME_BLOOD:
			m_mpbloodepimapchoice->Clear();

			if (m_mpbloodusecrypticpassagecheckBox->GetValue())
				m_mpbloodepimapchoice->Append(*g_blood_cp_level_list);
			else if (m_mpbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODSW)
				for (l_loop_var = 0; l_loop_var < 10; l_loop_var++)
					m_mpbloodepimapchoice->Append((*g_blood_level_list)[l_loop_var]);
			else if (m_mpbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODRG)
				for (l_loop_var = 0; l_loop_var < 48; l_loop_var++)
					m_mpbloodepimapchoice->Append((*g_blood_level_list)[l_loop_var]);
			else
				m_mpbloodepimapchoice->Append(*g_blood_level_list);

			m_mpbloodepimapchoice->SetSelection(level_selection);

			if (m_mpbloodepimapchoice->GetSelection() == wxNOT_FOUND)
				m_mpbloodepimapchoice->SetSelection(0);

			break;

		case GAME_DN3D:
			m_mpdn3depimapchoice->Clear();

			if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKESW)
				for (l_loop_var = 0; l_loop_var < 8; l_loop_var++)
					m_mpdn3depimapchoice->Append((*g_dn3d_level_list)[l_loop_var]);
			else if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKERG)
				for (l_loop_var = 0; l_loop_var < 33; l_loop_var++)
					m_mpdn3depimapchoice->Append((*g_dn3d_level_list)[l_loop_var]);
			else
				m_mpdn3depimapchoice->Append(*g_dn3d_level_list);

			m_mpdn3depimapchoice->SetSelection(level_selection);

			if (m_mpdn3depimapchoice->GetSelection() == wxNOT_FOUND)
				m_mpdn3depimapchoice->SetSelection(0);

			break;

		case GAME_SW:
			m_mpswepimapchoice->Clear();

			if (m_mpswsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSSWSW)
				for (l_loop_var = 0; l_loop_var < 6; l_loop_var++)
					m_mpswepimapchoice->Append((*g_sw_level_list)[l_loop_var]);
			else
				m_mpswepimapchoice->Append(*g_sw_level_list);

			m_mpswepimapchoice->SetSelection(level_selection);

			if (m_mpswepimapchoice->GetSelection() == wxNOT_FOUND)
				m_mpswepimapchoice->SetSelection(0);

			break;
		default: ;
	}
}

void MPDialog::OnSrcPortSelect(wxCommandEvent& event)
{
	int l_temp_dn3dgametype;

	switch (event.GetId())
	{
		case ID_MPBLOODSELECTSRCPORT:
			m_mpbloodusecrypticpassagecheckBox->Enable((m_mpbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODRG && wxFileExists(((wxFileName)g_configuration->dosbloodrg_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))) ||
													   (m_mpbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODPP && wxFileExists(((wxFileName)g_configuration->dosbloodpp_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))) ||
													   (m_mpbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODOU && wxFileExists(((wxFileName)g_configuration->dosbloodou_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))));

			if (!m_mpbloodusecrypticpassagecheckBox->IsEnabled() && m_mpbloodusecrypticpassagecheckBox->GetValue())
			{
				m_mpbloodusecrypticpassagecheckBox->SetValue(false);

				m_temp_bloodepimap_cp = m_mpbloodepimapchoice->GetSelection();
				AppendLevelsList(GAME_BLOOD, m_temp_bloodepimap);
			}
			else
				AppendLevelsList(GAME_BLOOD, m_mpbloodepimapchoice->GetSelection());

			if (m_mpbloodusermapchoice->IsEmpty() || m_mpbloodsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSBLOODSW)
			{
				m_mpbloodepimapradioBtn->SetValue(true);
				m_mpbloodepimapchoice->Enable();
				m_mpbloodusermapradioBtn->Disable();
				m_mpbloodusermapchoice->Disable();
			}
			else
			{
				m_mpbloodusermapradioBtn->Enable();
				if (m_mpbloodusermapchoice->GetSelection() == wxNOT_FOUND)
					m_mpbloodusermapchoice->SetSelection(0);
			}

			break;

		case ID_MPDESCENTSELECTSRCPORT:
			if (m_mpdescentusermapchoice->IsEmpty())
			{
				m_mpdescentepimapradioBtn->SetValue(true);
				m_mpdescentepimapchoice->Enable();
				m_mpdescentusermapradioBtn->Disable();
				m_mpdescentusermapchoice->Disable();
			}
			else
			{
				m_mpdescentusermapradioBtn->Enable();
				if (m_mpdescentusermapchoice->GetSelection() == wxNOT_FOUND)
					m_mpdescentusermapchoice->SetSelection(0);
			}

			break;

		case ID_MPDESCENT2SELECTSRCPORT:
			if (m_mpdescent2usermapchoice->IsEmpty())
			{
				m_mpdescent2epimapradioBtn->SetValue(true);
				m_mpdescent2epimapchoice->Enable();
				m_mpdescent2usermapradioBtn->Disable();
				m_mpdescent2usermapchoice->Disable();
			}
			else
			{
				m_mpdescent2usermapradioBtn->Enable();
				if (m_mpdescent2usermapchoice->GetSelection() == wxNOT_FOUND)
					m_mpdescent2usermapchoice->SetSelection(0);
			}

			break;

		case ID_MPDN3DSELECTSRCPORT:
			AppendLevelsList(GAME_DN3D, m_mpdn3depimapchoice->GetSelection());

			if (m_mpdn3dusermapchoice->IsEmpty() || m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSDUKESW)
			{
				m_mpdn3depimapradioBtn->SetValue(true);
				m_mpdn3depimapchoice->Enable();
				m_mpdn3dusermapradioBtn->Disable();
				m_mpdn3dusermapchoice->Disable();
			}
			else
			{
				m_mpdn3dusermapradioBtn->Enable();
				if (m_mpdn3dusermapchoice->GetSelection() == wxNOT_FOUND)
					m_mpdn3dusermapchoice->SetSelection(0);
			}

			l_temp_dn3dgametype = m_mpdn3dgametypechoice->GetSelection();
			m_mpdn3dgametypechoice->Clear();

			for (size_t l_loop_var = 0; l_loop_var < (size_t)(m_mpdn3dsourceportchoice->GetStringSelection() != SRCPORTNAME_EDUKE32 ? 3 : 5); l_loop_var++)
				m_mpdn3dgametypechoice->Append((*g_dn3d_gametype_list)[l_loop_var]);

			m_mpdn3dgametypechoice->SetSelection(l_temp_dn3dgametype);

			if (m_mpdn3dgametypechoice->GetSelection() == wxNOT_FOUND)
				m_mpdn3dgametypechoice->SetSelection(0);

			m_mpdn3dconnectiontypechoice->Clear();
			m_mpdn3dconnectiontypechoice->Append((*g_dn3d_connectiontype_list)[CONNECTIONTYPE_PEER2PEER]);

			if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DUKE3DW || m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_EDUKE32)
				m_mpdn3dconnectiontypechoice->Append((*g_dn3d_connectiontype_list)[CONNECTIONTYPE_MASTERSLAVE]);

			if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_EDUKE32)
				m_mpdn3dconnectiontypechoice->Append((*g_dn3d_connectiontype_list)[CONNECTIONTYPE_SERVERCLIENT]);

			if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DUKE3DW)
				m_mpdn3dconnectiontypechoice->SetSelection(g_configuration->gamelaunch_duke3dw_connectiontype);
			else if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_EDUKE32)
				m_mpdn3dconnectiontypechoice->SetSelection(g_configuration->gamelaunch_eduke32_connectiontype);
			else
				m_mpdn3dconnectiontypechoice->SetSelection(CONNECTIONTYPE_PEER2PEER);

			m_mpdn3dconnectiontypechoice->Enable(m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DUKE3DW || m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_EDUKE32);

			break;

		case ID_MPSWSELECTSRCPORT:
			AppendLevelsList(GAME_SW, m_mpswepimapchoice->GetSelection());

			if (m_mpswusermapchoice->IsEmpty() || m_mpswsourceportchoice->GetStringSelection() == SRCPORTNAME_DOSSWSW)
			{
				m_mpswepimapradioBtn->SetValue(true);
				m_mpswepimapchoice->Enable();
				m_mpswusermapradioBtn->Disable();
				m_mpswusermapchoice->Disable();
			}
			else
			{
				m_mpswusermapradioBtn->Enable();
				if (m_mpswusermapchoice->GetSelection() == wxNOT_FOUND)
					m_mpswusermapchoice->SetSelection(0);
			}

			m_mpswgametypechoice->Enable(m_mpswsourceportchoice->GetStringSelection() == SRCPORTNAME_SWP);

			break;
		default: ;
	}
}

void MPDialog::OnCheckBoxClick(wxCommandEvent& event)
{
	switch (event.GetId())
	{
		case ID_MPBLOODRECORDDEMO:
			m_mpbloodrecorddemotextCtrl->Enable(m_mpbloodrecorddemocheckBox->GetValue());
			break;
		case ID_MPSWRECORDDEMO:
			m_mpswrecorddemotextCtrl->Enable(m_mpswrecorddemocheckBox->GetValue());
			break;
#if USE_PASSWORD
		case ID_MPPASSPROTECTCHECK:
			m_passprotecttextCtrl->Enable(m_mppassprotectcheckBox->GetValue());
			m_mpkeeppassprotectcheckBox->Enable(m_mppassprotectcheckBox->GetValue());
			break;
#endif
		case ID_MPBLOODUSECRYPTICPASSAGE:
			if (!m_mpbloodusecrypticpassagecheckBox->GetValue())
			{
				m_temp_bloodepimap_cp = m_mpbloodepimapchoice->GetSelection();
				AppendLevelsList(GAME_BLOOD, m_temp_bloodepimap);
			}
			else
			{
				m_temp_bloodepimap = m_mpbloodepimapchoice->GetSelection();
				AppendLevelsList(GAME_BLOOD, m_temp_bloodepimap_cp);
			}

			break;
		case ID_MPUSEPROFILENAME:
			m_mpcustomroomnametextCtrl->Enable(!m_mpcustomuseprofilenamecheckBox->GetValue());
			m_mpcustomroomnametextCtrl->SetValue(m_mpcustomprofilechoice->GetStringSelection());
			break;
		case ID_MPUSENULLMODEM:
			m_mpcustommaxnumplayerschoice->Enable(!m_mpcustomusenullmodemcheckBox->GetValue());

			if (m_mpcustomusenullmodemcheckBox->GetValue())
			{
				m_temp_custommaxnumplayer = m_mpcustommaxnumplayerschoice->GetSelection()+2;
				m_mpcustommaxnumplayerschoice->SetSelection(0);
			}
			else
				m_mpcustommaxnumplayerschoice->SetSelection(m_temp_custommaxnumplayer - 2);

			m_mpcustomrxdelaystaticText->Enable(m_mpcustomusenullmodemcheckBox->GetValue());
			m_mpcustomrxdelaytextCtrl->Enable(m_mpcustomusenullmodemcheckBox->GetValue());
			break;
		default: ;
	}
}

void MPDialog::OnRadioBtnClick(wxCommandEvent& event)
{
	switch (event.GetId())
	{
		case ID_MPBLOODUSEEPIMAP:
		case ID_MPBLOODUSEUSERMAP:
			m_mpbloodepimapchoice->Enable(m_mpbloodepimapradioBtn->GetValue());
			m_mpbloodusermapchoice->Enable(m_mpbloodusermapradioBtn->GetValue());
			break;
		case ID_MPDESCENTUSEEPIMAP:
		case ID_MPDESCENTUSEUSERMAP:
			m_mpdescentepimapchoice->Enable(m_mpdescentepimapradioBtn->GetValue());
			m_mpdescentusermapchoice->Enable(m_mpdescentusermapradioBtn->GetValue());
			break;
		case ID_MPDESCENT2USEEPIMAP:
		case ID_MPDESCENT2USEUSERMAP:
			m_mpdescent2epimapchoice->Enable(m_mpdescent2epimapradioBtn->GetValue());
			m_mpdescent2usermapchoice->Enable(m_mpdescent2usermapradioBtn->GetValue());
			break;
		case ID_MPDN3DUSEEPIMAP:
		case ID_MPDN3DUSEUSERMAP:
			m_mpdn3depimapchoice->Enable(m_mpdn3depimapradioBtn->GetValue());
			m_mpdn3dusermapchoice->Enable(m_mpdn3dusermapradioBtn->GetValue());
			break;
		case ID_MPSWUSEEPIMAP:
		case ID_MPSWUSEUSERMAP:
			m_mpswepimapchoice->Enable(m_mpswepimapradioBtn->GetValue());
			m_mpswusermapchoice->Enable(m_mpswusermapradioBtn->GetValue());
			break;
		default: ;
	}
}

void MPDialog::OnSelectCustomProfile(wxCommandEvent& WXUNUSED(event))
{
	wxString l_temp_str = m_mpcustomprofilechoice->GetStringSelection();


	g_configuration->custom_profile_current = g_configuration->custom_profile_list;

	while (g_configuration->custom_profile_current->profilename != l_temp_str)
		g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;

	UpdateSummaryList();

	if (m_mpcustomuseprofilenamecheckBox->GetValue())
		m_mpcustomroomnametextCtrl->SetValue(m_mpcustomprofilechoice->GetStringSelection());

	m_mpcustomextrahostargstextCtrl->SetValue(g_configuration->custom_profile_current->extrahostargs);
#if EXTRA_ARGS_FOR_ALL
	m_mpcustomextraallargstextCtrl->SetValue(g_configuration->custom_profile_current->extraallargs);
#endif
}

void MPDialog::UpdateSummaryList()
{
	m_mpcustomsummarytextCtrl->Clear();

	m_mpcustomsummarytextCtrl->AppendText(wxT("Executable :\n"));
	m_mpcustomsummarytextCtrl->AppendText(g_configuration->custom_profile_current->mpexecutable);

	if (g_configuration->custom_profile_current->cdmount == CDROMMOUNT_DIR)
	{
		m_mpcustomsummarytextCtrl->AppendText(wxT("\n\nCD-ROM location :\n"));
		m_mpcustomsummarytextCtrl->AppendText(g_configuration->custom_profile_current->cdlocation);
	}
	else if (g_configuration->custom_profile_current->cdmount == CDROMMOUNT_IMG)
	{
		m_mpcustomsummarytextCtrl->AppendText(wxT("\n\nCD-ROM image/block device :\n"));
		m_mpcustomsummarytextCtrl->AppendText(g_configuration->custom_profile_current->cdimage);
	}

	if (g_configuration->custom_profile_current->ingamesupport)
		m_mpcustomsummarytextCtrl->AppendText(wxT("\n\nSupports in-game joining multiplayer mode."));

	if (g_configuration->custom_profile_current->usenetbios)
	{
		m_mpcustomsummarytextCtrl->AppendText(wxT("\n\nRequires NetBIOS for multiplayer mode :\n"));
		m_mpcustomsummarytextCtrl->AppendText(g_configuration->custom_profile_current->netbiospath);
	}
/*
	if (!g_configuration->custom_profile_current->extrahostargs.IsEmpty())
	{
		m_mpcustomsummarytextCtrl->AppendText(wxT("\n\nExtra arguments for host only :\n"));
		m_mpcustomsummarytextCtrl->AppendText(g_configuration->custom_profile_current->extrahostargs);
	}

#if EXTRA_ARGS_FOR_ALL
	if (!g_configuration->custom_profile_current->extraallargs.IsEmpty())
	{
		m_mpcustomsummarytextCtrl->AppendText(wxT("\n\nExtra arguments for everyone :\n"));
		m_mpcustomsummarytextCtrl->AppendText(g_configuration->custom_profile_current->extraallargs);
	}
#endif
*/
}

void MPDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	if (g_host_frame)
	{
		g_host_frame->Enable();
		g_host_frame->FocusFrame();
		//  g_host_frame->SetFocus();
		//  g_host_frame->Raise();
	}
	else
		g_main_frame->ShowMainFrame();
	Destroy();
}

void MPDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	wxString l_game_name = m_mpnotebook->GetPageText(m_mpnotebook->GetSelection());
	wxString l_temp_str;
	long l_temp_num;

	GameType l_game;
	SourcePortType l_srcport;
	bool l_use_crypticpassage = false;
	wxString l_modname;
	wxArrayString l_modfiles;
	wxString l_modconfile;
	wxString l_moddeffile;
	bool l_showmodurl = false;
	wxString l_modurl;
	size_t l_episode_map = 0;
	wxString l_user_map, l_extra_usermap;
	bool l_launch_usermap = false;
	BloodMPGameT_Type l_bloodgametype = BLOODMPGAMETYPE_BB;
	DescentMPGameT_Type l_descentgametype = DESCENTMPGAMETYPE_ANARCHY;
	Descent2MPGameT_Type l_descent2gametype = DESCENT2MPGAMETYPE_ANARCHY;
	DN3DMPGameT_Type l_dn3dgametype = DN3DMPGAMETYPE_DMSPAWN;
	SWMPGameT_Type l_swgametype = SWMPGAMETYPE_WBSPAWN;
	size_t l_skill = 0;
	DN3DSpawnType l_dn3dspawn = DN3DSPAWN_NONE;
	BloodMPMonsterType l_blood_monster = BLOODMPMONSTERTYPE_NONE;
	BloodMPWeaponType l_blood_weapon = BLOODMPWEAPONTYPE_PERMANENT;
	BloodMPItemType l_blood_item = BLOODMPITEMTYPE_RESPAWN;
	BloodMPRespawnType l_blood_respawn = BLOODMPRESPAWNTYPE_RANDOM;
	size_t l_connectiontype = 0;
	bool l_allow_ingame_joining = (l_game_name == GAMENAME_CUSTOM) && g_configuration->custom_profile_current->ingamesupport;
	wxString l_roomname;
	size_t l_max_num_players;
	size_t l_maxping;
	size_t l_rxdelay = DEFAULT_RXDELAY;
	bool l_recorddemo = false;
	wxString l_recorddemofilename;
	wxString l_args_for_host;
	wxString l_args_for_all;
	bool l_advertise = m_mpadvertiseroomcheckBox->GetValue();
#if USE_PASSWORD
	bool l_is_pass_protected = m_mppassprotectcheckBox->GetValue();
	wxString l_password = m_passprotecttextCtrl->GetValue();
#else
	bool l_is_pass_protected = false;
	wxString l_password;
#endif

	if (l_is_pass_protected && l_password.IsEmpty())
	{
		wxMessageBox(wxT("You can't set an empty password.\nPlease type a non-empty password in the relevant text control."), wxT("Password is empty."), wxOK|wxICON_INFORMATION);
		return;
	}

	wxArrayString l_str_list;
	size_t l_loop_var, l_num_of_items;
	bool l_server_is_up = true;
	char l_buffer[MAX_COMM_TEXT_LENGTH];

	if (l_game_name == GAMENAME_BLOOD)
	{
		l_game = GAME_BLOOD;

		l_temp_str = m_mpbloodsourceportchoice->GetStringSelection();
		if (l_temp_str == SRCPORTNAME_DOSBLOODSW)
			l_srcport = SOURCEPORT_DOSBLOODSW;
		else if (l_temp_str == SRCPORTNAME_DOSBLOODRG)
			l_srcport = SOURCEPORT_DOSBLOODRG;
		else if (l_temp_str == SRCPORTNAME_DOSBLOODPP)
			l_srcport = SOURCEPORT_DOSBLOODPP;
		else if (l_temp_str == SRCPORTNAME_DOSBLOODOU)
			l_srcport = SOURCEPORT_DOSBLOODOU;

		l_use_crypticpassage = m_mpbloodusecrypticpassagecheckBox->GetValue();

		if (m_mpbloodepimapradioBtn->GetValue())
		{
			l_launch_usermap = false;

			if (!l_use_crypticpassage)
				l_episode_map = g_GetBloodLevelNumBySelection(m_mpbloodepimapchoice->GetSelection());
			else
				l_episode_map = g_GetBloodCPLevelNumBySelection(m_mpbloodepimapchoice->GetSelection());
		}
		else
		{
			l_launch_usermap = true;
			l_user_map = m_mpbloodusermapchoice->GetStringSelection();
		}

		l_bloodgametype = (BloodMPGameT_Type)(m_mpbloodgametypechoice->GetSelection()+1);

		l_skill = m_mpbloodskillchoice->GetSelection();

		l_blood_monster = (BloodMPMonsterType)m_mpbloodmonsterchoice->GetSelection();

		l_blood_weapon = (BloodMPWeaponType)m_mpbloodweaponchoice->GetSelection();

		l_blood_item = (BloodMPItemType)m_mpblooditemchoice->GetSelection();

		l_blood_respawn = (BloodMPRespawnType)m_mpbloodrespawnchoice->GetSelection();

		l_roomname = m_mpbloodroomnametextCtrl->GetValue();

		l_max_num_players = m_mpbloodmaxnumplayerschoice->GetSelection()+2;

		m_mpbloodmaxpingtextCtrl->GetValue().ToLong(&l_temp_num);
		l_maxping = l_temp_num;

		l_recorddemo = m_mpbloodrecorddemocheckBox->GetValue();
		l_recorddemofilename = m_mpbloodrecorddemotextCtrl->GetValue();

		if (l_recorddemofilename != wxEmptyString)
		{
			if (!(l_recorddemofilename.Right(4).IsSameAs(wxT(".dem"), false)))
				l_recorddemofilename << wxT(".dem");

			l_recorddemofilename.MakeUpper();
		}

		l_args_for_host = m_mpbloodextrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		l_args_for_all = m_mpbloodextraallargstextCtrl->GetValue();
#endif

		l_connectiontype = m_mpbloodpacketmodechoice->GetSelection();
	}
	else if (l_game_name == GAMENAME_DESCENT)
	{
		l_game = GAME_DESCENT;

		l_temp_str = m_mpdescentsourceportchoice->GetStringSelection();
		if (l_temp_str == SRCPORTNAME_DOSDESCENT)
			l_srcport = SOURCEPORT_DOSDESCENT;
		else if (l_temp_str == SRCPORTNAME_D1XREBIRTH)
			l_srcport = SOURCEPORT_D1XREBIRTH;

		if (m_mpdescentepimapradioBtn->GetValue())
		{
			l_launch_usermap = false;
			l_episode_map = m_mpdescentepimapchoice->GetSelection();
		}
		else
		{
			l_launch_usermap = true;
			l_user_map = m_mpdescentusermapchoice->GetStringSelection();

			if (!g_FindDescentMSNfile(&l_extra_usermap, l_user_map))
			{
				wxMessageBox(wxT("The \".MSN\" file associated to the \"") + l_user_map + wxT("\" file is missing.\n\nPlease download this missing \"") + l_user_map.Upper().BeforeLast('.') + wxT(".MSN") + wxT("\" file, and try again."), wxT("Missing \".MSN\" mission file"), wxOK|wxICON_EXCLAMATION);
				return;
			}
		}

		l_descentgametype = (DescentMPGameT_Type)m_mpdescentgametypechoice->GetSelection();

		l_roomname = m_mpdescentroomnametextCtrl->GetValue();

		l_max_num_players = m_mpdescentmaxnumplayerschoice->GetSelection()+2;

		m_mpdescentmaxpingtextCtrl->GetValue().ToLong(&l_temp_num);
		l_maxping = l_temp_num;

		l_args_for_host = m_mpdescentextrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		l_args_for_all = m_mpdescentextraallargstextCtrl->GetValue();
#endif
	}
	else if (l_game_name == GAMENAME_DESCENT2)
	{
		l_game = GAME_DESCENT2;

		l_temp_str = m_mpdescent2sourceportchoice->GetStringSelection();
		if (l_temp_str == SRCPORTNAME_DOSDESCENT2)
			l_srcport = SOURCEPORT_DOSDESCENT2;
		else if (l_temp_str == SRCPORTNAME_D2XREBIRTH)
			l_srcport = SOURCEPORT_D2XREBIRTH;

		if (m_mpdescent2epimapradioBtn->GetValue())
		{
			l_launch_usermap = false;
			l_episode_map = m_mpdescent2epimapchoice->GetSelection();
		}
		else
		{
			l_launch_usermap = true;
			l_user_map = m_mpdescent2usermapchoice->GetStringSelection();

			if (!g_FindDescentMN2file(&l_extra_usermap, l_user_map))
			{
				wxMessageBox(wxT("The \".MN2\" file associated to the \"") + l_user_map + wxT("\" file is missing.\n\nPlease download this missing \"") + l_user_map.Upper().BeforeLast('.') + wxT(".MN2") + wxT("\" file, and try again."), wxT("Missing \".MN2\" mission file"), wxOK|wxICON_EXCLAMATION);
				return;
			}
		}

		l_descent2gametype = (Descent2MPGameT_Type)m_mpdescent2gametypechoice->GetSelection();

		l_roomname = m_mpdescent2roomnametextCtrl->GetValue();

		l_max_num_players = m_mpdescent2maxnumplayerschoice->GetSelection()+2;

		m_mpdescent2maxpingtextCtrl->GetValue().ToLong(&l_temp_num);
		l_maxping = l_temp_num;

		l_args_for_host = m_mpdescent2extrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		l_args_for_all = m_mpdescent2extraallargstextCtrl->GetValue();
#endif
	}
	else if (l_game_name == GAMENAME_DN3D)
	{
		l_game = GAME_DN3D;

		l_temp_str = m_mpdn3dsourceportchoice->GetStringSelection();
		if (l_temp_str == SRCPORTNAME_DOSDUKESW)
			l_srcport = SOURCEPORT_DOSDUKESW;
		else if (l_temp_str == SRCPORTNAME_DOSDUKERG)
			l_srcport = SOURCEPORT_DOSDUKERG;
		else if (l_temp_str == SRCPORTNAME_DOSDUKEAE)
			l_srcport = SOURCEPORT_DOSDUKEAE;
		else if (l_temp_str == SRCPORTNAME_DUKE3DW)
			l_srcport = SOURCEPORT_DUKE3DW;
		else if (l_temp_str == SRCPORTNAME_EDUKE32)
			l_srcport = SOURCEPORT_EDUKE32;
		else if (l_temp_str == SRCPORTNAME_NDUKE)
			l_srcport = SOURCEPORT_NDUKE;
		else if (l_temp_str == SRCPORTNAME_HDUKE)
			l_srcport = SOURCEPORT_HDUKE;
		else if (l_temp_str == SRCPORTNAME_XDUKE)
			l_srcport = SOURCEPORT_XDUKE;

		if (m_mpdn3dtcmodprofilechoice->GetSelection())
		{
			g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

			l_temp_str = m_mpdn3dtcmodprofilechoice->GetStringSelection();

			while (g_configuration->dn3dtcmod_profile_current->profilename != l_temp_str)
				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;

			l_modname = g_configuration->dn3dtcmod_profile_current->profilename;

			l_num_of_items = g_configuration->dn3dtcmod_profile_current->modfiles.GetCount();

			for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
				l_modfiles.Add(((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName());

			l_modconfile = g_configuration->dn3dtcmod_profile_current->confile;

			l_moddeffile = g_configuration->dn3dtcmod_profile_current->deffile;

			l_showmodurl = g_configuration->dn3dtcmod_profile_current->usemodurl;
			l_modurl = g_configuration->dn3dtcmod_profile_current->modurl;
		}

		if (m_mpdn3depimapradioBtn->GetValue())
		{
			l_launch_usermap = false;
			l_episode_map = g_GetDN3DLevelNumBySelection(m_mpdn3depimapchoice->GetSelection());
		}
		else
		{
			l_launch_usermap = true;
			l_user_map = m_mpdn3dusermapchoice->GetStringSelection();
		}

		l_dn3dgametype = (DN3DMPGameT_Type)(m_mpdn3dgametypechoice->GetSelection()+1);

		l_skill = m_mpdn3dskillchoice->GetSelection();

		l_dn3dspawn = (DN3DSpawnType)m_mpdn3dspawnchoice->GetSelection();

		l_roomname = m_mpdn3droomnametextCtrl->GetValue();

		l_max_num_players = m_mpdn3dmaxnumplayerschoice->GetSelection()+2;

		m_mpdn3dmaxpingtextCtrl->GetValue().ToLong(&l_temp_num);
		l_maxping = l_temp_num;

		l_recorddemo = m_mpdn3drecorddemocheckBox->GetValue();

		l_args_for_host = m_mpdn3dextrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		l_args_for_all = m_mpdn3dextraallargstextCtrl->GetValue();
#endif

		l_connectiontype = m_mpdn3dconnectiontypechoice->GetSelection();
	}
	else if (l_game_name == GAMENAME_SW)
	{
		l_game = GAME_SW;

		l_temp_str = m_mpswsourceportchoice->GetStringSelection();
		if (l_temp_str == SRCPORTNAME_DOSSWSW)
			l_srcport = SOURCEPORT_DOSSWSW;
		else if (l_temp_str == SRCPORTNAME_DOSSWRG)
			l_srcport = SOURCEPORT_DOSSWRG;
		else if (l_temp_str == SRCPORTNAME_VOIDSW)
			l_srcport = SOURCEPORT_VOIDSW;
		else if (l_temp_str == SRCPORTNAME_SWP)
			l_srcport = SOURCEPORT_SWP;

		if (m_mpswtcmodprofilechoice->GetSelection())
		{
			g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

			l_temp_str = m_mpswtcmodprofilechoice->GetStringSelection();

			while (g_configuration->swtcmod_profile_current->profilename != l_temp_str)
				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;

			l_modname = g_configuration->swtcmod_profile_current->profilename;

			l_num_of_items = g_configuration->swtcmod_profile_current->modfiles.GetCount();

			for (l_loop_var = 0; l_loop_var < l_num_of_items; l_loop_var++)
				l_modfiles.Add(((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName());

			l_moddeffile = g_configuration->swtcmod_profile_current->deffile;

			l_showmodurl = g_configuration->swtcmod_profile_current->usemodurl;
			l_modurl = g_configuration->swtcmod_profile_current->modurl;
		}

		if (m_mpswepimapradioBtn->GetValue())
		{
			l_launch_usermap = false;
			l_episode_map = g_GetSWLevelNumBySelection(m_mpswepimapchoice->GetSelection());
		}
		else
		{
			l_launch_usermap = true;
			l_user_map = m_mpswusermapchoice->GetStringSelection();
		}

		l_swgametype = (SWMPGameT_Type)m_mpswgametypechoice->GetSelection();

		l_skill = m_mpswskillchoice->GetSelection();

		l_roomname = m_mpswroomnametextCtrl->GetValue();

		l_max_num_players = m_mpswmaxnumplayerschoice->GetSelection()+2;

		m_mpswmaxpingtextCtrl->GetValue().ToLong(&l_temp_num);
		l_maxping = l_temp_num;

		l_recorddemo = m_mpswrecorddemocheckBox->GetValue();
		l_recorddemofilename = m_mpswrecorddemotextCtrl->GetValue();

		if (l_recorddemofilename != wxEmptyString)
		{
			if (!(l_recorddemofilename.Right(4).IsSameAs(wxT(".dmo"), false)))
				l_recorddemofilename << wxT(".dmo");

			l_recorddemofilename.MakeUpper();
		}

		l_args_for_host = m_mpswextrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		l_args_for_all = m_mpswextraallargstextCtrl->GetValue();
#endif

		l_connectiontype =
			(l_srcport == SOURCEPORT_VOIDSW) ?
			CONNECTIONTYPE_MASTERSLAVE : CONNECTIONTYPE_PEER2PEER;
	}
	else if (l_game_name == GAMENAME_CUSTOM)
	{
		l_game = GAME_CUSTOM;

		l_srcport = SOURCEPORT_CUSTOM;

		if (m_mpcustomuseprofilenamecheckBox->GetValue())
			l_roomname = g_configuration->custom_profile_current->profilename;
		else
			l_roomname = m_mpcustomroomnametextCtrl->GetValue();

		if (m_mpcustomusenullmodemcheckBox->GetValue())
			l_max_num_players = 2;
		else
			l_max_num_players = m_mpcustommaxnumplayerschoice->GetSelection()+2;

		m_mpcustommaxpingtextCtrl->GetValue().ToLong(&l_temp_num);
		l_maxping = l_temp_num;

		l_args_for_host = m_mpcustomextrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		l_args_for_all = m_mpcustomextraallargstextCtrl->GetValue();
#endif

		l_connectiontype = m_mpcustomusenullmodemcheckBox->GetValue();

		if (!((m_mpcustomrxdelaytextCtrl->GetValue()).ToLong(&l_temp_num)))
			l_rxdelay = DEFAULT_RXDELAY;
		else
			l_rxdelay = l_temp_num;
	}

	// Let's begin a short "simulation" first...
	// to check that the packets to send to the clients have enough room
	// to contain the given information.

	HostedRoomFrame* l_host_frame = new HostedRoomFrame;
	// Should work when the number of clients and the number of in-room players
	// are both initialized to 0.
	// IMPORTANT!!! Pass *false* instead of l_advertise!
	l_host_frame->UpdateGameSettings(
		l_is_pass_protected,
		l_password,
		l_game,
		l_srcport,
		l_use_crypticpassage,
		l_roomname,
		l_max_num_players,
		l_recorddemo,
		l_recorddemofilename,
		l_skill,
		l_bloodgametype,
		l_descentgametype,
		l_descent2gametype,
		l_dn3dgametype,
		l_swgametype,
		l_dn3dspawn,
		l_blood_monster,
		l_blood_weapon,
		l_blood_item,
		l_blood_respawn,
		l_args_for_host,
		l_args_for_all,
		l_maxping,
		l_rxdelay,
		l_launch_usermap,
		l_episode_map,
		l_user_map,
		l_modfiles,
		l_modname,
		l_modconfile,
		l_moddeffile,
		l_showmodurl,
		l_modurl,
		false,
		l_connectiontype,
		l_allow_ingame_joining);

	if (!l_modname.IsEmpty())
	{
		l_str_list = l_host_frame->GetServerModFilesSockStrList();
		if (!g_ConvertStringListToCommText(l_str_list, l_buffer))
		{
			wxMessageBox(wxT("When sending clients information about the used MOD files, there won't be room for all of that information.\nYou can use less files, shorter filenames, but most important - avoid using non-English characters in filenames and folders as much as possible."), wxT("Too much MOD files info"), wxOK|wxICON_INFORMATION);
			l_host_frame->Destroy();
			return;
		}
	}
	l_str_list = l_host_frame->GetServerInfoSockStrList();
	if (!g_ConvertStringListToCommText(l_str_list, l_buffer))
	{
		wxMessageBox(wxT("When sending clients information about the room (excluding list of MOD files), there won't be room for all of that information.\nYou can use shorter names for the room name and/or the usermap file name, but most important - Avoid using non-English characters in filenames and folders as much as possible.\nIn title-names non-English characters may currently work, although internally they probably take more space."), wxT("Too much game room info"), wxOK|wxICON_INFORMATION);
		l_host_frame->Destroy();
		return;
	}
	l_host_frame->Destroy();

	ApplySettings();

	if (g_host_frame)
		g_host_frame->Enable();
	else
	{
		g_host_frame = new HostedRoomFrame;
		l_server_is_up = g_host_frame->StartServer();
		if (!l_server_is_up)
		{
			wxMessageBox(wxString::Format(wxT("ERROR: Couldn't start server on port %d for an unknown reason.\nPossible reasons:\n* The configured server port number is already in use by another listener.\n* The server has disconnected a client by itself. At least in that case you might have to wait for about a minute before you can rehost on the same port.\n* Somehow a disconnection hasn't been done properly."), (int)g_configuration->server_port_number),
				wxT("Couldn't start listening"), wxOK|wxICON_EXCLAMATION);
			g_host_frame->Destroy();
			g_host_frame = NULL;
			g_main_frame->ShowMainFrame();
		}
		else
		{
#ifdef ENABLE_UPNP
			if (g_configuration->enable_upnp)
			{
				g_UPNP_Forward(g_configuration->game_port_number, false);
				g_UPNP_Forward(g_configuration->server_port_number, true);
			}
#endif
			g_host_frame->Show(true);
			//    g_host_frame->Raise();
		}
	}
	if (l_server_is_up)
	{
		g_host_frame->UpdateGameSettings(
			l_is_pass_protected,
			l_password,
			l_game,
			l_srcport,
			l_use_crypticpassage,
			l_roomname,
			l_max_num_players,
			l_recorddemo,
			l_recorddemofilename,
			l_skill,
			l_bloodgametype,
			l_descentgametype,
			l_descent2gametype,
			l_dn3dgametype,
			l_swgametype,
			l_dn3dspawn,
			l_blood_monster,
			l_blood_weapon,
			l_blood_item,
			l_blood_respawn,
			l_args_for_host,
			l_args_for_all,
			l_maxping,
			l_rxdelay,
			l_launch_usermap,
			l_episode_map,
			l_user_map,
			l_modfiles,
			l_modname,
			l_modconfile,
			l_moddeffile,
			l_showmodurl,
			l_modurl,
			l_advertise,
			l_connectiontype,
			l_allow_ingame_joining);
		g_host_frame->FocusFrame();
		//  g_host_frame->Raise();
		//  g_host_frame->SetFocus();
	}
	Destroy();
}

void MPDialog::ApplySettings()
{
	wxString l_game_name = m_mpnotebook->GetPageText(m_mpnotebook->GetSelection());
	wxString l_srcport_name;

	if (HAVE_BLOOD)
	{
		l_srcport_name = m_mpbloodsourceportchoice->GetStringSelection();

		if (l_srcport_name == SRCPORTNAME_DOSBLOODSW)
			g_configuration->gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODSW;
		else if (l_srcport_name == SRCPORTNAME_DOSBLOODRG)
			g_configuration->gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODRG;
		else if (l_srcport_name == SRCPORTNAME_DOSBLOODPP)
			g_configuration->gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODPP;
		else if (l_srcport_name == SRCPORTNAME_DOSBLOODOU)
			g_configuration->gamelaunch_blood_source_port = SOURCEPORT_DOSBLOODOU;

		g_configuration->gamelaunch_blood_use_crypticpassage = m_mpbloodusecrypticpassagecheckBox->GetValue();

		if (!m_mpbloodusecrypticpassagecheckBox->GetValue())
		{
			g_configuration->gamelaunch_bloodlvl = g_GetBloodLevelNumBySelection(m_mpbloodepimapchoice->GetSelection());
			g_configuration->gamelaunch_bloodlvl_cp = g_GetBloodCPLevelNumBySelection(m_temp_bloodepimap_cp);
		}
		else
		{
			g_configuration->gamelaunch_bloodlvl = g_GetBloodLevelNumBySelection(m_temp_bloodepimap);
			g_configuration->gamelaunch_bloodlvl_cp = g_GetBloodCPLevelNumBySelection(m_mpbloodepimapchoice->GetSelection());
		}

		if (m_mpbloodepimapradioBtn->GetValue())
			g_configuration->gamelaunch_bloodusermap = wxEmptyString;
		else
			g_configuration->gamelaunch_bloodusermap = m_mpbloodusermapchoice->GetStringSelection();

		g_configuration->gamelaunch_bloodgametype = (BloodMPGameT_Type)(m_mpbloodgametypechoice->GetSelection()+1);

		g_configuration->gamelaunch_blood_skill = m_mpbloodskillchoice->GetSelection();

		g_configuration->gamelaunch_blood_monster = (BloodMPMonsterType)m_mpbloodmonsterchoice->GetSelection();

		g_configuration->gamelaunch_blood_weapon = (BloodMPWeaponType)m_mpbloodweaponchoice->GetSelection();

		g_configuration->gamelaunch_blood_item = (BloodMPItemType)m_mpblooditemchoice->GetSelection();

		g_configuration->gamelaunch_blood_respawn = (BloodMPRespawnType)m_mpbloodrespawnchoice->GetSelection();

		g_configuration->gamelaunch_blood_roomname = m_mpbloodroomnametextCtrl->GetValue();

		g_configuration->gamelaunch_blood_numofplayers = m_mpbloodmaxnumplayerschoice->GetSelection()+2;

		if (!((m_mpbloodmaxpingtextCtrl->GetValue()).ToLong(&(g_configuration->gamelaunch_blood_maxping))))
			g_configuration->gamelaunch_blood_maxping = DEFAULT_MAXPING;

		g_configuration->gamelaunch_blood_args_for_host = m_mpbloodextrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		g_configuration->gamelaunch_blood_args_for_all = m_mpbloodextraallargstextCtrl->GetValue();
#endif

		g_configuration->gamelaunch_blood_packetmode = (PacketMode)m_mpbloodpacketmodechoice->GetSelection();
	}

	if (HAVE_DESCENT)
	{
		l_srcport_name = m_mpdescentsourceportchoice->GetStringSelection();

		if (l_srcport_name == SRCPORTNAME_DOSDESCENT)
			g_configuration->gamelaunch_descent_source_port = SOURCEPORT_DOSDESCENT;
		else if (l_srcport_name == SRCPORTNAME_D1XREBIRTH)
			g_configuration->gamelaunch_descent_source_port = SOURCEPORT_D1XREBIRTH;

		g_configuration->gamelaunch_descentlvl = m_mpdescentepimapchoice->GetSelection();

		if (m_mpdescentepimapradioBtn->GetValue())
			g_configuration->gamelaunch_descentusermap = wxEmptyString;
		else
			g_configuration->gamelaunch_descentusermap = m_mpdescentusermapchoice->GetStringSelection();

		g_configuration->gamelaunch_descentgametype = (DescentMPGameT_Type)m_mpdescentgametypechoice->GetSelection();

		g_configuration->gamelaunch_descent_roomname = m_mpdescentroomnametextCtrl->GetValue();

		g_configuration->gamelaunch_descent_numofplayers = m_mpdescentmaxnumplayerschoice->GetSelection()+2;

		if (!((m_mpdescentmaxpingtextCtrl->GetValue()).ToLong(&(g_configuration->gamelaunch_descent_maxping))))
			g_configuration->gamelaunch_descent_maxping = DEFAULT_MAXPING;

		g_configuration->gamelaunch_descent_args_for_host = m_mpdescentextrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		g_configuration->gamelaunch_descent_args_for_all = m_mpdescentextraallargstextCtrl->GetValue();
#endif
	}

	if (HAVE_DESCENT2)
	{
		l_srcport_name = m_mpdescent2sourceportchoice->GetStringSelection();

		if (l_srcport_name == SRCPORTNAME_DOSDESCENT2)
			g_configuration->gamelaunch_descent2_source_port = SOURCEPORT_DOSDESCENT2;
		else if (l_srcport_name == SRCPORTNAME_D2XREBIRTH)
			g_configuration->gamelaunch_descent2_source_port = SOURCEPORT_D2XREBIRTH;

		g_configuration->gamelaunch_descent2lvl = m_mpdescent2epimapchoice->GetSelection();

		if (m_mpdescent2epimapradioBtn->GetValue())
			g_configuration->gamelaunch_descent2usermap = wxEmptyString;
		else
			g_configuration->gamelaunch_descent2usermap = m_mpdescent2usermapchoice->GetStringSelection();

		g_configuration->gamelaunch_descent2gametype = (Descent2MPGameT_Type)m_mpdescent2gametypechoice->GetSelection();

		g_configuration->gamelaunch_descent2_roomname = m_mpdescent2roomnametextCtrl->GetValue();

		g_configuration->gamelaunch_descent2_numofplayers = m_mpdescent2maxnumplayerschoice->GetSelection()+2;

		if (!((m_mpdescent2maxpingtextCtrl->GetValue()).ToLong(&(g_configuration->gamelaunch_descent2_maxping))))
			g_configuration->gamelaunch_descent2_maxping = DEFAULT_MAXPING;

		g_configuration->gamelaunch_descent2_args_for_host = m_mpdescent2extrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		g_configuration->gamelaunch_descent2_args_for_all = m_mpdescent2extraallargstextCtrl->GetValue();
#endif
	}

	if (HAVE_DN3D)
	{
		l_srcport_name = m_mpdn3dsourceportchoice->GetStringSelection();

		if (l_srcport_name == SRCPORTNAME_DOSDUKESW)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_DOSDUKESW;
		else if (l_srcport_name == SRCPORTNAME_DOSDUKERG)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_DOSDUKERG;
		else if (l_srcport_name == SRCPORTNAME_DOSDUKEAE)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_DOSDUKEAE;
		else if (l_srcport_name == SRCPORTNAME_DUKE3DW)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_DUKE3DW;
		else if (l_srcport_name == SRCPORTNAME_EDUKE32)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_EDUKE32;
		else if (l_srcport_name == SRCPORTNAME_NDUKE)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_NDUKE;
		else if (l_srcport_name == SRCPORTNAME_HDUKE)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_HDUKE;
		else if (l_srcport_name == SRCPORTNAME_XDUKE)
			g_configuration->gamelaunch_dn3d_source_port = SOURCEPORT_XDUKE;

		if (m_mpdn3dtcmodprofilechoice->GetSelection())
			g_configuration->gamelaunch_dn3dtcmod_profilename = m_mpdn3dtcmodprofilechoice->GetStringSelection();
		else
			g_configuration->gamelaunch_dn3dtcmod_profilename = wxEmptyString;

		g_configuration->gamelaunch_dn3dlvl = g_GetDN3DLevelNumBySelection(m_mpdn3depimapchoice->GetSelection());

		if (m_mpdn3depimapradioBtn->GetValue())
			g_configuration->gamelaunch_dn3dusermap = wxEmptyString;
		else
			g_configuration->gamelaunch_dn3dusermap = m_mpdn3dusermapchoice->GetStringSelection();

		g_configuration->gamelaunch_dn3dgametype = (DN3DMPGameT_Type)(m_mpdn3dgametypechoice->GetSelection()+1);

		g_configuration->gamelaunch_dn3d_skill = m_mpdn3dskillchoice->GetSelection();

		g_configuration->gamelaunch_dn3dspawn = (DN3DSpawnType)m_mpdn3dspawnchoice->GetSelection();

		g_configuration->gamelaunch_dn3d_roomname = m_mpdn3droomnametextCtrl->GetValue();

		g_configuration->gamelaunch_dn3d_numofplayers = m_mpdn3dmaxnumplayerschoice->GetSelection()+2;

		if (!((m_mpdn3dmaxpingtextCtrl->GetValue()).ToLong(&(g_configuration->gamelaunch_dn3d_maxping))))
			g_configuration->gamelaunch_dn3d_maxping = DEFAULT_MAXPING;

		g_configuration->gamelaunch_dn3d_args_for_host = m_mpdn3dextrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		g_configuration->gamelaunch_dn3d_args_for_all = m_mpdn3dextraallargstextCtrl->GetValue();
#endif

		if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_DUKE3DW)
			g_configuration->gamelaunch_duke3dw_connectiontype = (ConnectionType)m_mpdn3dconnectiontypechoice->GetSelection();
		else if (m_mpdn3dsourceportchoice->GetStringSelection() == SRCPORTNAME_EDUKE32)
			g_configuration->gamelaunch_eduke32_connectiontype = (ConnectionType)m_mpdn3dconnectiontypechoice->GetSelection();
	}

	if (HAVE_SW)
	{
		l_srcport_name = m_mpswsourceportchoice->GetStringSelection();

		if (l_srcport_name == SRCPORTNAME_DOSSWSW)
			g_configuration->gamelaunch_sw_source_port = SOURCEPORT_DOSSWSW;
		else if (l_srcport_name == SRCPORTNAME_DOSSWRG)
			g_configuration->gamelaunch_sw_source_port = SOURCEPORT_DOSSWRG;
		else if (l_srcport_name == SRCPORTNAME_VOIDSW)
			g_configuration->gamelaunch_sw_source_port = SOURCEPORT_VOIDSW;
		else if (l_srcport_name == SRCPORTNAME_SWP)
			g_configuration->gamelaunch_sw_source_port = SOURCEPORT_SWP;

		if (m_mpswtcmodprofilechoice->GetSelection())
			g_configuration->gamelaunch_swtcmod_profilename = m_mpswtcmodprofilechoice->GetStringSelection();
		else
			g_configuration->gamelaunch_swtcmod_profilename = wxEmptyString;

		g_configuration->gamelaunch_swlvl = g_GetSWLevelNumBySelection(m_mpswepimapchoice->GetSelection());

		if (m_mpswepimapradioBtn->GetValue())
			g_configuration->gamelaunch_swusermap = wxEmptyString;
		else
			g_configuration->gamelaunch_swusermap = m_mpswusermapchoice->GetStringSelection();

		g_configuration->gamelaunch_swgametype = (SWMPGameT_Type)m_mpswgametypechoice->GetSelection();

		g_configuration->gamelaunch_sw_skill = m_mpswskillchoice->GetSelection();

		g_configuration->gamelaunch_sw_roomname = m_mpswroomnametextCtrl->GetValue();

		g_configuration->gamelaunch_sw_numofplayers = m_mpswmaxnumplayerschoice->GetSelection()+2;

		if (!((m_mpswmaxpingtextCtrl->GetValue()).ToLong(&(g_configuration->gamelaunch_sw_maxping))))
			g_configuration->gamelaunch_sw_maxping = DEFAULT_MAXPING;

		g_configuration->gamelaunch_sw_args_for_host = m_mpswextrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		g_configuration->gamelaunch_sw_args_for_all = m_mpswextraallargstextCtrl->GetValue();
#endif
	}

	if (HAVE_CUSTOM && g_configuration->have_custom_mpprofile)
	{
		g_configuration->gamelaunch_custom_profilename = m_mpcustomprofilechoice->GetStringSelection();

		g_configuration->gamelaunch_custom_use_profilename = m_mpcustomuseprofilenamecheckBox->GetValue();

		g_configuration->gamelaunch_custom_roomname = m_mpcustomroomnametextCtrl->GetValue();

		if (m_mpcustomusenullmodemcheckBox->GetValue())
			g_configuration->gamelaunch_custom_numofplayers = m_temp_custommaxnumplayer;
		else
			g_configuration->gamelaunch_custom_numofplayers = m_mpcustommaxnumplayerschoice->GetSelection()+2;

		if (!((m_mpcustommaxpingtextCtrl->GetValue()).ToLong(&(g_configuration->gamelaunch_custom_maxping))))
			g_configuration->gamelaunch_custom_maxping = DEFAULT_MAXPING;

		g_configuration->gamelaunch_custom_use_nullmodem = m_mpcustomusenullmodemcheckBox->GetValue();

		if (!((m_mpcustomrxdelaytextCtrl->GetValue()).ToLong(&(g_configuration->gamelaunch_custom_rxdelay))))
			g_configuration->gamelaunch_custom_rxdelay = DEFAULT_RXDELAY;

		g_configuration->custom_profile_current->extrahostargs = m_mpcustomextrahostargstextCtrl->GetValue();

#if EXTRA_ARGS_FOR_ALL
		g_configuration->custom_profile_current->extraallargs = m_mpcustomextraallargstextCtrl->GetValue();
#endif
	}

	if (l_game_name == GAMENAME_BLOOD)
		g_configuration->gamelaunch_source_port = g_configuration->gamelaunch_blood_source_port;
	else if (l_game_name == GAMENAME_DESCENT)
		g_configuration->gamelaunch_source_port = g_configuration->gamelaunch_descent_source_port;
	else if (l_game_name == GAMENAME_DESCENT2)
		g_configuration->gamelaunch_source_port = g_configuration->gamelaunch_descent2_source_port;
	else if (l_game_name == GAMENAME_DN3D)
		g_configuration->gamelaunch_source_port = g_configuration->gamelaunch_dn3d_source_port;
	else if (l_game_name == GAMENAME_SW)
		g_configuration->gamelaunch_source_port = g_configuration->gamelaunch_sw_source_port;
	else if (l_game_name == GAMENAME_CUSTOM)
		g_configuration->gamelaunch_source_port = SOURCEPORT_CUSTOM;

//	g_configuration->gamelaunch_modname = m_mpmodnametextCtrl->GetValue();
//	g_configuration->gamelaunch_show_modurl = m_mpmodurlcheckBox->GetValue();
//	g_configuration->gamelaunch_modurl = m_mpmodurltextCtrl->GetValue();

#if USE_PASSWORD
	g_configuration->gamelaunch_use_password = m_mppassprotectcheckBox->GetValue() && m_mpkeeppassprotectcheckBox->GetValue();
	g_configuration->gamelaunch_password = m_passprotecttextCtrl->GetValue();
#else
	g_configuration->gamelaunch_use_password = false;
	g_configuration->gamelaunch_password = wxT("");
#endif

	g_configuration->gamelaunch_advertise = m_mpadvertiseroomcheckBox->GetValue();
	g_configuration->gamelaunch_skip_settings = m_mpnodialogcheckBox->GetValue();

	g_configuration->Save();
}
