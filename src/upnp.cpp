/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#endif

#include "upnp.h"
#include "mp_common.h"

UPNPData::UPNPData()
{
	m_addr = wxEmptyString;
	m_port_num = 0;
}

void UPNPData::Detect()
{
	wxCriticalSectionLocker l_locker(m_critical_section);
	size_t n_trials = 6, l_output_size;
	int l_pos, l_pos2;
	wxIPV4address m_localaddr;
	m_localaddr.AnyAddress ();
	m_localaddr.Service(8502);
	wxDatagramSocket *m_socket = new wxDatagramSocket(m_localaddr, wxSOCKET_NOBIND|wxSOCKET_BROADCAST);
	wxIPV4address l_addr;
	wxString l_url;
	wxSocketClient* m_client;
	char l_input_buffer[1024]
	= "M-SEARCH * HTTP/1.1\r\n" \
		"MX: 2\r\n" \
		"HOST: 239.255.255.250:1900\r\n" \
		"MAN: \"ssdp:discover\"\r\n" \
		"ST: urn:schemas-upnp-org:service:WANPPPConnection:1\r\n" \
		"\r\n",
		l_output_buffer[8192];
	wxString l_converted_buffer;
	l_addr.BroadcastAddress();
	l_addr.Service(1900);
	while (n_trials > 0)
	{
		m_socket->SendTo(l_addr, l_input_buffer, strlen(l_input_buffer));
		wxSleep(1);
		m_socket->SendTo(l_addr, l_input_buffer, strlen(l_input_buffer));
		wxSleep(1);
		m_socket->SendTo(l_addr, l_input_buffer, strlen(l_input_buffer));
		m_socket->RecvFrom(l_addr, l_output_buffer, 8192);
		l_output_size = m_socket->LastCount();
		if (l_output_size)
		{
			l_converted_buffer = wxString(l_output_buffer, wxConvUTF8, l_output_size);
			//    l_pos = l_converted_buffer.Lower().Find(wxT("schemas-upnp-org:service:WANPPPConnection:1"));
			//    if (l_pos == wxNOT_FOUND)
			//      l_pos = l_converted_buffer.Lower().Find(wxT("schemas-upnp-org:service:WANIPConnection:1"));
			//    if (l_pos != wxNOT_FOUND)
			//    {
			//      l_converted_buffer = l_converted_buffer.Mid(l_pos);
			l_pos = l_converted_buffer.Lower().Find(wxT("location:"));
			if (l_pos != wxNOT_FOUND)
			{
				l_converted_buffer = l_converted_buffer.Mid(l_pos+9);
				l_pos = l_converted_buffer.Find(wxT("http://"));
				if (l_pos != wxNOT_FOUND)
				{
					l_converted_buffer = l_converted_buffer.Mid(l_pos+7);
					l_pos = l_converted_buffer.Find(wxT(':'));
					if (l_pos != wxNOT_FOUND)
					{
						l_pos2 = l_converted_buffer.Mid(l_pos+1).Find(wxT('/'));
						if (l_pos2 != wxNOT_FOUND)
						{ // Just checking the port number...
							if (l_converted_buffer.Mid(l_pos+1, l_pos2).ToULong(&m_port_num))
							{ // Almost there!
								m_addr = l_converted_buffer.Left(l_pos);
								l_url = l_converted_buffer.Mid(l_pos+l_pos2+1);
//								l_url = wxT("http://") + l_converted_buffer;
								l_pos = l_url.Find(wxT("\r\n"));
								if (l_pos != wxNOT_FOUND)
									l_url = l_url.Left(l_pos);
								m_is_wanip = (n_trials < 4);
								break;
							}
						}
					}
				}
			}
			//    }
		}
		n_trials--;
		if (n_trials == 3)
			strcpy(l_input_buffer,
			"M-SEARCH * HTTP/1.1\r\n"
			"MX: 2\r\n"
			"HOST: 239.255.255.250:1900\r\n"
			"MAN: \"ssdp:discover\"\r\n"
			"ST: urn:schemas-upnp-org:service:WANIPConnection:1\r\n"
			"\r\n");
	}
	if (n_trials > 0)
	{
		m_client = new wxSocketClient;
		m_client->SetTimeout(5);
		m_client->SetFlags(wxSOCKET_BLOCK|wxSOCKET_WAITALL);
		l_addr.Hostname(m_addr);
		l_addr.Service(m_port_num);

		if (m_client->Connect(l_addr, true))
		{
			sprintf(l_input_buffer, "GET %s HTTP/1.1\r\nHOST: %s:%lu\r\nACCEPT-LANGUAGE: en\r\n\r\n",
				(const char *)l_url.mb_str(wxConvUTF8), (const char *)m_addr.mb_str(wxConvUTF8), m_port_num);
			m_client->Write(l_input_buffer, strlen(l_input_buffer));
			m_client->Read(l_output_buffer, 8192);
			l_output_size = m_client->LastCount();
			if (l_output_size > 0)
			{
				l_converted_buffer = wxString(l_output_buffer, wxConvUTF8, l_output_size);
				if (m_is_wanip)
					l_pos = l_converted_buffer.Find(wxT("<serviceType>urn:schemas-upnp-org:service:WANIPConnection:1</serviceType>"));
				else
					l_pos = l_converted_buffer.Find(wxT("<serviceType>urn:schemas-upnp-org:service:WANPPPConnection:1</serviceType>"));
				if (l_pos != wxNOT_FOUND)
				{
					l_converted_buffer = l_converted_buffer.Mid(l_pos);
					l_pos = l_converted_buffer.Find(wxT("<controlURL>"));
					if (l_pos != wxNOT_FOUND)
					{
						l_converted_buffer = l_converted_buffer.Mid(l_pos+12);
						l_pos = l_converted_buffer.Find(wxT("</controlURL>"));
						if (l_pos != wxNOT_FOUND) // We're done.
							m_url = l_converted_buffer.Left(l_pos);
					}
				}
			}
		}
		m_client->Destroy();
	}
}

void UPNPData::Forward(long port_num, bool is_tcp)
{
	wxCriticalSectionLocker l_locker(m_critical_section);
	char l_service[4], l_protocol[4], l_soapmsg[1920], l_header[384];
	wxSocketClient* m_client;
	wxIPV4address l_addr;
	int l_content_length;
	if (m_url != wxEmptyString)
	{
		m_client = new wxSocketClient;
		m_client->SetTimeout(5);
		m_client->SetFlags(wxSOCKET_BLOCK|wxSOCKET_WAITALL);
		l_addr.Hostname(m_addr);
		l_addr.Service(m_port_num);

		if (m_client->Connect(l_addr, true))
		{
			if (m_is_wanip)
				strcpy(l_service, "IP");
			else
				strcpy(l_service, "PPP");
			if (is_tcp)
				strcpy(l_protocol, "TCP");
			else
				strcpy(l_protocol, "UDP");
			m_client->GetLocal(l_addr);
			sprintf(l_soapmsg, "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				"<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
				"<s:Body><u:AddPortMapping xmlns:u=\"urn:schemas-upnp-org:service:WAN%sConnection:1\">"
				"<NewRemoteHost></NewRemoteHost><NewExternalPort>%ld</NewExternalPort><NewProtocol>%s</NewProtocol>"
				"<NewInternalPort>%ld</NewInternalPort><NewInternalClient>%s</NewInternalClient>"
				"<NewEnabled>1</NewEnabled>""<NewPortMappingDescription>YANG_%s</NewPortMappingDescription>"
				"<NewLeaseDuration>0</NewLeaseDuration></u:AddPortMapping></s:Body></s:Envelope>",
				l_service, port_num, l_protocol, port_num,
				(const char *)(l_addr.IPAddress().mb_str(wxConvUTF8)), l_protocol);
			l_content_length = strlen(l_soapmsg);

			sprintf(l_header, "POST %s HTTP/1.1\r\nHOST: %s:%lu\r\nCONTENT-LENGTH: %d\r\n"
				"CONTENT-TYPE: text/xml; charset=\"utf-8\"\r\n"
				"SOAPACTION: \"urn:schemas-upnp-org:service:WAN%sConnection:1#AddPortMapping\"\r\n\r\n",
				(const char *)m_url.mb_str(wxConvUTF8), (const char *)m_addr.mb_str(wxConvUTF8), m_port_num, l_content_length, l_service);
			m_client->Write(l_header, strlen(l_header));
			m_client->Write(l_soapmsg, strlen(l_soapmsg));
			// In case of failure.
			sprintf(l_header, "	M-POST %s HTTP/1.1\r\nHOST: %s:%lu\r\nCONTENT-LENGTH: %d\r\n"
				"CONTENT-TYPE: text/xml; charset=\"utf-8\"\r\n"
				"MAN: \"http://schemas.xmlsoap.org/soap/envelope/\"; ns=01\r\n"
				"01-SOAPACTION: \"urn:schemas-upnp-org:service:WAN%sConnection:1#AddPortMapping\"\r\n\r\n",
				(const char *)m_url.mb_str(wxConvUTF8), (const char *)m_addr.mb_str(wxConvUTF8), m_port_num, l_content_length, l_service);
			m_client->Write(l_header, strlen(l_header));
			m_client->Write(l_soapmsg, strlen(l_soapmsg));
		}
		m_client->Destroy();
	}
}

void UPNPData::UnForward(long port_num, bool is_tcp)
{
	wxCriticalSectionLocker l_locker(m_critical_section);
	char l_service[4], l_protocol[4], l_soapmsg[1920], l_header[384];
	wxSocketClient* m_client;
	wxIPV4address l_addr;
	int l_content_length;
	if (m_url != wxEmptyString)
	{
		m_client = new wxSocketClient;
		m_client->SetTimeout(5);
		m_client->SetFlags(wxSOCKET_BLOCK|wxSOCKET_WAITALL);
		l_addr.Hostname(m_addr);
		l_addr.Service(m_port_num);

		if (m_client->Connect(l_addr, true))
		{
			if (m_is_wanip)
				strcpy(l_service, "IP");
			else
				strcpy(l_service, "PPP");
			if (is_tcp)
				strcpy(l_protocol, "TCP");
			else
				strcpy(l_protocol, "UDP");
			sprintf(l_soapmsg, "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				"<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"
				"<s:Body><u:DeletePortMapping xmlns:u=\"urn:schemas-upnp-org:service:WAN%sConnection:1\">"
				"<NewRemoteHost></NewRemoteHost><NewExternalPort>%ld</NewExternalPort>"
				"<NewProtocol>%s</NewProtocol></u:DeletePortMapping></s:Body></s:Envelope>",
				l_service, port_num, l_protocol);
			l_content_length = strlen(l_soapmsg);

			sprintf(l_header, "POST %s HTTP/1.1\r\nHOST: %s:%lu\r\nCONTENT-LENGTH: %d\r\n"
				"CONTENT-TYPE: text/xml; charset=\"utf-8\"\r\n"
				"SOAPACTION: \"urn:schemas-upnp-org:service:WAN%sConnection:1#DeletePortMapping\"\r\n\r\n",
				(const char *)m_url.mb_str(wxConvUTF8), (const char *)m_addr.mb_str(wxConvUTF8), m_port_num, l_content_length, l_service);
			m_client->Write(l_header, strlen(l_header));
			m_client->Write(l_soapmsg, strlen(l_soapmsg));
			// In case of failure.
			sprintf(l_header, "M-POST %s HTTP/1.1\r\nHOST: %s:%lu\r\nCONTENT-LENGTH: %d\r\n"
				"CONTENT-TYPE: text/xml; charset=\"utf-8\"\r\n"
				"MAN: \"http://schemas.xmlsoap.org/soap/envelope/\"; ns=01\r\n"
				"01-SOAPACTION: \"urn:schemas-upnp-org:service:WAN%sConnection:1#DeletePortMapping\"\r\n\r\n",
				(const char *)m_url.mb_str(wxConvUTF8), (const char *)m_addr.mb_str(wxConvUTF8), m_port_num, l_content_length, l_service);
			m_client->Write(l_header, strlen(l_header));
			m_client->Write(l_soapmsg, strlen(l_soapmsg));
		}
		m_client->Destroy();
	}
}
