/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/dirdlg.h>
#include <wx/filedlg.h>
#include <wx/filename.h>
#include <wx/gbsizer.h>
#include <wx/msgdlg.h>
#include <wx/bmpbuttn.h>
#include <wx/mstream.h>
#include <wx/image.h>
#endif

#include "srcports_dialog.h"
#include "network_dialog.h"
#include "sp_dialog.h"
#include "yang.h"
#include "theme.h"
#include "yang.xpm"
#include "yang_images.h"

BEGIN_EVENT_TABLE(SRCPortsDialog, YANGDialog)

EVT_BUTTON(wxID_OK, SRCPortsDialog::OnOk)
//EVT_BUTTON(wxID_APPLY, SRCPortsDialog::OnApply)
EVT_BUTTON(wxID_CANCEL, SRCPortsDialog::OnCancel)

EVT_BUTTON(ID_LOCATEDOSBLOODSW, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSBLOODRG, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSBLOODPP, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSBLOODOU, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSDESCENT, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATED1XREBIRTH, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSDESCENT2, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATED2XREBIRTH, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSDUKESW, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSDUKERG, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSDUKEAE, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDUKE3DW, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEEDUKE32, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATENDUKE, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEHDUKE, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEXDUKE, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSSWSW, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSSWRG, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEVOIDSW, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATESWP, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATECUSTOMSP, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATECUSTOMMP, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATECUSTOMCDIMAGE, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATENETBIOS, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSBOX, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSBOXCONF, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEDOSBOXCDIMAGE, SRCPortsDialog::OnLocateFile)
EVT_BUTTON(ID_LOCATEBMOUSE, SRCPortsDialog::OnLocateFile)
#ifndef __WXMSW__
EVT_BUTTON(ID_LOCATEWINE, SRCPortsDialog::OnLocateFile)
#endif

EVT_BUTTON(ID_SELECTBLOODMAPSFOLDER, SRCPortsDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTDESCENTMAPSFOLDER, SRCPortsDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTDESCENT2MAPSFOLDER, SRCPortsDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTDUKEMAPSFOLDER, SRCPortsDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTSWMAPSFOLDER, SRCPortsDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTCUSTOMCDLOCATION, SRCPortsDialog::OnSelectDir)
EVT_BUTTON(ID_SELECTDOSBOXCDLOCATION, SRCPortsDialog::OnSelectDir)

EVT_BUTTON(ID_ADDNEWCUSTOMPROFILE, SRCPortsDialog::OnAddNewProfile)
EVT_BUTTON(ID_DELETECUSTOMPROFILE, SRCPortsDialog::OnDeleteProfile)

EVT_CHECKBOX(ID_DOSBLOODSWCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSBLOODRGCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSBLOODPPCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSBLOODOUCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSDESCENTCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_D1XREBIRTHCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSDESCENT2CHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_D2XREBIRTHCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSDUKESWCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSDUKERGCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSDUKEAECHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DUKE3DWCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_EDUKE32CHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_NDUKECHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_HDUKECHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_XDUKECHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSSWSWCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSSWRGCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_VOIDSWCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SWPCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_CUSTOMSPONLYCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_CUSTOMSAMEBINCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_INGAMESUPPORTCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_NETBIOSCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSBOXCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_DOSBOXCONFCHECKBOX, SRCPortsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_BMOUSECHECKBOX, SRCPortsDialog::OnCheckBoxClick)
#ifndef __WXMSW__
EVT_CHECKBOX(ID_WINECHECKBOX, SRCPortsDialog::OnCheckBoxClick)
#endif

//EVT_COMBOBOX(ID_SELECTCUSTOMPROFILE, SRCPortsDialog::OnSelectCustomProfile)

EVT_RADIOBUTTON(ID_CUSTOMNOCDRADIOBTN, SRCPortsDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_CUSTOMCDLOCATIONRADIOBTN, SRCPortsDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_CUSTOMCDIMAGERADIOBTN, SRCPortsDialog::OnRadioBtnClick)

EVT_RADIOBUTTON(ID_DOSBOXNOCDRADIOBTN, SRCPortsDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_DOSBOXCDLOCATIONRADIOBTN, SRCPortsDialog::OnRadioBtnClick)
EVT_RADIOBUTTON(ID_DOSBOXCDIMAGERADIOBTN, SRCPortsDialog::OnRadioBtnClick)

EVT_TEXT(ID_SELECTCUSTOMPROFILE, SRCPortsDialog::OnUpdateTextInput)
EVT_TEXT(ID_SPEXECUTABLEINPUTTEXT, SRCPortsDialog::OnUpdateTextInput)
EVT_TEXT(ID_MPEXECUTABLEINPUTTEXT, SRCPortsDialog::OnUpdateTextInput)
EVT_TEXT(ID_CDLOCATIONINPUTTEXT, SRCPortsDialog::OnUpdateTextInput)
EVT_TEXT(ID_CDIMAGEINPUTTEXT, SRCPortsDialog::OnUpdateTextInput)
EVT_TEXT(ID_NETBIOSINPUTTEXT, SRCPortsDialog::OnUpdateTextInput)
EVT_TEXT(ID_EXTRAARGSINPUTTEXT, SRCPortsDialog::OnUpdateTextInput)
EVT_TEXT(ID_EXTRAHOSTARGSINPUTTEXT, SRCPortsDialog::OnUpdateTextInput)
#if EXTRA_ARGS_FOR_ALL
EVT_TEXT(ID_EXTRAALLARGSINPUTTEXT, SRCPortsDialog::OnUpdateTextInput)
#endif

END_EVENT_TABLE()

SRCPortsDialog::SRCPortsDialog() : YANGDialog(NULL, wxID_ANY, wxT("Source ports"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	int i;
	wxBoxSizer* SRCPortsDialogbSizer = new wxBoxSizer( wxVERTICAL );

	m_srcportsnotebook = new YANGNotebook( this, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(440, -1)), wxTAB_TRAVERSAL );
//	m_srcportslistbook = new wxListbook( this, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(88, -1)), wxLB_DEFAULT|wxTAB_TRAVERSAL );

	m_srcportbloodpanel = new YANGPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bloodpanelbSizer = new wxBoxSizer( wxVERTICAL );

	i = -1;
	wxGridBagSizer* bloodgbSizer = new wxGridBagSizer( 0, 0 );
	bloodgbSizer->SetFlexibleDirection( wxBOTH );
	bloodgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_dosbloodswavailcheckBox = new YANGCheckBox( m_srcportbloodpanel, ID_DOSBLOODSWCHECKBOX, wxT("DOS Shareware v1.11 (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	bloodgbSizer->Add( m_dosbloodswavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodswbintextCtrl = new YANGTextCtrl( m_srcportbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	bloodgbSizer->Add( m_dosbloodswbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodswlocatebutton = new YANGButton( m_srcportbloodpanel, ID_LOCATEDOSBLOODSW, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	bloodgbSizer->Add( m_dosbloodswlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodrgavailcheckBox = new YANGCheckBox( m_srcportbloodpanel, ID_DOSBLOODRGCHECKBOX, wxT("DOS Registered v1.11 (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	bloodgbSizer->Add( m_dosbloodrgavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodrgbintextCtrl = new YANGTextCtrl( m_srcportbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	bloodgbSizer->Add( m_dosbloodrgbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodrglocatebutton = new YANGButton( m_srcportbloodpanel, ID_LOCATEDOSBLOODRG, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	bloodgbSizer->Add( m_dosbloodrglocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodppavailcheckBox = new YANGCheckBox( m_srcportbloodpanel, ID_DOSBLOODPPCHECKBOX, wxT("DOS Plasma Pak v1.11 (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	bloodgbSizer->Add( m_dosbloodppavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodppbintextCtrl = new YANGTextCtrl( m_srcportbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	bloodgbSizer->Add( m_dosbloodppbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodpplocatebutton = new YANGButton( m_srcportbloodpanel, ID_LOCATEDOSBLOODPP, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	bloodgbSizer->Add( m_dosbloodpplocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodouavailcheckBox = new YANGCheckBox( m_srcportbloodpanel, ID_DOSBLOODOUCHECKBOX, wxT("DOS One Whole Unit v1.21 (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	bloodgbSizer->Add( m_dosbloodouavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodoubintextCtrl = new YANGTextCtrl( m_srcportbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	bloodgbSizer->Add( m_dosbloodoubintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosbloodoulocatebutton = new YANGButton( m_srcportbloodpanel, ID_LOCATEDOSBLOODOU, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	bloodgbSizer->Add( m_dosbloodoulocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_prefbloodstaticline = new wxStaticLine( m_srcportbloodpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	bloodgbSizer->Add( m_prefbloodstaticline, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_bloodmapsstaticText = new wxStaticText( m_srcportbloodpanel, wxID_ANY, wxT("User maps directory"), wxDefaultPosition, wxDefaultSize, 0 );
	bloodgbSizer->Add( m_bloodmapsstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_bloodmapstextCtrl = new YANGTextCtrl( m_srcportbloodpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	bloodgbSizer->Add( m_bloodmapstextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_bloodmapsselectbutton = new YANGButton( m_srcportbloodpanel, ID_SELECTBLOODMAPSFOLDER, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	bloodgbSizer->Add( m_bloodmapsselectbutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	bloodgbSizer->AddGrowableCol( 1 );
	bloodpanelbSizer->Add( bloodgbSizer, 1, wxEXPAND, 0 );

	m_srcportbloodpanel->SetSizer( bloodpanelbSizer );
	m_srcportbloodpanel->Layout();
	bloodpanelbSizer->Fit( m_srcportbloodpanel );

	m_srcportsnotebook->AddPage( m_srcportbloodpanel, GAMENAME_BLOOD, true );


	m_srcportdescentpanel = new YANGPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* descentpanelbSizer = new wxBoxSizer( wxVERTICAL );

	i = -1;
	wxGridBagSizer* descentgbSizer = new wxGridBagSizer( 0, 0 );
	descentgbSizer->SetFlexibleDirection( wxBOTH );
	descentgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_dosdescentavailcheckBox = new YANGCheckBox( m_srcportdescentpanel, ID_DOSDESCENTCHECKBOX, wxT("DOS Descent (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	descentgbSizer->Add( m_dosdescentavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdescentbintextCtrl = new YANGTextCtrl( m_srcportdescentpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	descentgbSizer->Add( m_dosdescentbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdescentlocatebutton = new YANGButton( m_srcportdescentpanel, ID_LOCATEDOSDESCENT, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	descentgbSizer->Add( m_dosdescentlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_d1xrebirthavailcheckBox = new YANGCheckBox( m_srcportdescentpanel, ID_D1XREBIRTHCHECKBOX, wxT("D1X-Rebirth"), wxDefaultPosition, wxDefaultSize, 0 );
	descentgbSizer->Add( m_d1xrebirthavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_d1xrebirthbintextCtrl = new YANGTextCtrl( m_srcportdescentpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	descentgbSizer->Add( m_d1xrebirthbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_d1xrebirthlocatebutton = new YANGButton( m_srcportdescentpanel, ID_LOCATED1XREBIRTH, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	descentgbSizer->Add( m_d1xrebirthlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_prefdescentstaticline = new wxStaticLine( m_srcportdescentpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	descentgbSizer->Add( m_prefdescentstaticline, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_descentmapsstaticText = new wxStaticText( m_srcportdescentpanel, wxID_ANY, wxT("User maps directory"), wxDefaultPosition, wxDefaultSize, 0 );
	descentgbSizer->Add( m_descentmapsstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_descentmapstextCtrl = new YANGTextCtrl( m_srcportdescentpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	descentgbSizer->Add( m_descentmapstextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_descentmapsselectbutton = new YANGButton( m_srcportdescentpanel, ID_SELECTDESCENTMAPSFOLDER, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	descentgbSizer->Add( m_descentmapsselectbutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	descentgbSizer->AddGrowableCol( 1 );
	descentpanelbSizer->Add( descentgbSizer, 1, wxEXPAND, 0 );

	m_srcportdescentpanel->SetSizer( descentpanelbSizer );
	m_srcportdescentpanel->Layout();
	descentpanelbSizer->Fit( m_srcportdescentpanel );

	m_srcportsnotebook->AddPage( m_srcportdescentpanel, GAMENAME_DESCENT, false );


	m_srcportdescent2panel = new YANGPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* descent2panelbSizer = new wxBoxSizer( wxVERTICAL );

	i = -1;
	wxGridBagSizer* descent2gbSizer = new wxGridBagSizer( 0, 0 );
	descent2gbSizer->SetFlexibleDirection( wxBOTH );
	descent2gbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_dosdescent2availcheckBox = new YANGCheckBox( m_srcportdescent2panel, ID_DOSDESCENT2CHECKBOX, wxT("DOS Descent 2 (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	descent2gbSizer->Add( m_dosdescent2availcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdescent2bintextCtrl = new YANGTextCtrl( m_srcportdescent2panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	descent2gbSizer->Add( m_dosdescent2bintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdescent2locatebutton = new YANGButton( m_srcportdescent2panel, ID_LOCATEDOSDESCENT2, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	descent2gbSizer->Add( m_dosdescent2locatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_d2xrebirthavailcheckBox = new YANGCheckBox( m_srcportdescent2panel, ID_D2XREBIRTHCHECKBOX, wxT("D2X-Rebirth"), wxDefaultPosition, wxDefaultSize, 0 );
	descent2gbSizer->Add( m_d2xrebirthavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_d2xrebirthbintextCtrl = new YANGTextCtrl( m_srcportdescent2panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	descent2gbSizer->Add( m_d2xrebirthbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_d2xrebirthlocatebutton = new YANGButton( m_srcportdescent2panel, ID_LOCATED2XREBIRTH, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	descent2gbSizer->Add( m_d2xrebirthlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_prefdescent2staticline = new wxStaticLine( m_srcportdescent2panel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	descent2gbSizer->Add( m_prefdescent2staticline, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_descent2mapsstaticText = new wxStaticText( m_srcportdescent2panel, wxID_ANY, wxT("User maps directory"), wxDefaultPosition, wxDefaultSize, 0 );
	descent2gbSizer->Add( m_descent2mapsstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_descent2mapstextCtrl = new YANGTextCtrl( m_srcportdescent2panel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	descent2gbSizer->Add( m_descent2mapstextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_descent2mapsselectbutton = new YANGButton( m_srcportdescent2panel, ID_SELECTDESCENT2MAPSFOLDER, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	descent2gbSizer->Add( m_descent2mapsselectbutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	descent2gbSizer->AddGrowableCol( 1 );
	descent2panelbSizer->Add( descent2gbSizer, 1, wxEXPAND, 0 );

	m_srcportdescent2panel->SetSizer( descent2panelbSizer );
	m_srcportdescent2panel->Layout();
	descent2panelbSizer->Fit( m_srcportdescent2panel );

	m_srcportsnotebook->AddPage( m_srcportdescent2panel, GAMENAME_DESCENT2, false );


	m_srcportduke3dpanel = new YANGPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* duke3dpanelbSizer = new wxBoxSizer( wxVERTICAL );

	i = -1;
	wxGridBagSizer* duke3dgbSizer = new wxGridBagSizer( 0, 0 );
	duke3dgbSizer->SetFlexibleDirection( wxBOTH );
	duke3dgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_dosdukeswavailcheckBox = new YANGCheckBox( m_srcportduke3dpanel, ID_DOSDUKESWCHECKBOX, wxT("DOS Shareware v1.3D (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_dosdukeswavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdukeswbintextCtrl = new YANGTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_dosdukeswbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdukeswlocatebutton = new YANGButton( m_srcportduke3dpanel, ID_LOCATEDOSDUKESW, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_dosdukeswlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdukergavailcheckBox = new YANGCheckBox( m_srcportduke3dpanel, ID_DOSDUKERGCHECKBOX, wxT("DOS Registered v1.3D (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_dosdukergavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdukergbintextCtrl = new YANGTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_dosdukergbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdukerglocatebutton = new YANGButton( m_srcportduke3dpanel, ID_LOCATEDOSDUKERG, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_dosdukerglocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdukeaeavailcheckBox = new YANGCheckBox( m_srcportduke3dpanel, ID_DOSDUKEAECHECKBOX, wxT("DOS Atomic Edition v1.5 (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_dosdukeaeavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdukeaebintextCtrl = new YANGTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_dosdukeaebintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosdukeaelocatebutton = new YANGButton( m_srcportduke3dpanel, ID_LOCATEDOSDUKEAE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_dosdukeaelocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_duke3dwavailcheckBox = new YANGCheckBox( m_srcportduke3dpanel, ID_DUKE3DWCHECKBOX, SRCPORTNAME_DUKE3DW, wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_duke3dwavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_duke3dwbintextCtrl = new YANGTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_duke3dwbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_duke3dwlocatebutton = new YANGButton( m_srcportduke3dpanel, ID_LOCATEDUKE3DW, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_duke3dwlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_eduke32availcheckBox = new YANGCheckBox( m_srcportduke3dpanel, ID_EDUKE32CHECKBOX, SRCPORTNAME_EDUKE32, wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_eduke32availcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_eduke32bintextCtrl = new YANGTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_eduke32bintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_eduke32locatebutton = new YANGButton( m_srcportduke3dpanel, ID_LOCATEEDUKE32, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_eduke32locatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_ndukeavailcheckBox = new YANGCheckBox( m_srcportduke3dpanel, ID_NDUKECHECKBOX, SRCPORTNAME_NDUKE, wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_ndukeavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_ndukebintextCtrl = new YANGTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_ndukebintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_ndukelocatebutton = new YANGButton( m_srcportduke3dpanel, ID_LOCATENDUKE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_ndukelocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_hdukeavailcheckBox = new YANGCheckBox( m_srcportduke3dpanel, ID_HDUKECHECKBOX, SRCPORTNAME_HDUKE, wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_hdukeavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hdukebintextCtrl = new YANGTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_hdukebintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_hdukelocatebutton = new YANGButton( m_srcportduke3dpanel, ID_LOCATEHDUKE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_hdukelocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_xdukeavailcheckBox = new YANGCheckBox( m_srcportduke3dpanel, ID_XDUKECHECKBOX, wxT("xDuke (formerly Rancidmeat Reloaded)"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_xdukeavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_xdukebintextCtrl = new YANGTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_xdukebintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_xdukelocatebutton = new YANGButton( m_srcportduke3dpanel, ID_LOCATEXDUKE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_xdukelocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_prefduke3dstaticline = new wxStaticLine( m_srcportduke3dpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	duke3dgbSizer->Add( m_prefduke3dstaticline, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_duke3dmapsstaticText = new wxStaticText( m_srcportduke3dpanel, wxID_ANY, wxT("User maps directory"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_duke3dmapsstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_duke3dmapstextCtrl = new YANGTextCtrl( m_srcportduke3dpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	duke3dgbSizer->Add( m_duke3dmapstextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_duke3dmapsselectbutton = new YANGButton( m_srcportduke3dpanel, ID_SELECTDUKEMAPSFOLDER, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	duke3dgbSizer->Add( m_duke3dmapsselectbutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	duke3dgbSizer->AddGrowableCol( 1 );
	duke3dpanelbSizer->Add( duke3dgbSizer, 1, wxEXPAND, 0 );

	m_srcportduke3dpanel->SetSizer( duke3dpanelbSizer );
	m_srcportduke3dpanel->Layout();
	duke3dpanelbSizer->Fit( m_srcportduke3dpanel );

	m_srcportsnotebook->AddPage( m_srcportduke3dpanel, GAMENAME_DN3D, false );


	m_srcportswpanel = new YANGPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* swpanelbSizer = new wxBoxSizer( wxVERTICAL );

	i = -1;
	wxGridBagSizer* swgbSizer = new wxGridBagSizer( 0, 0 );
	swgbSizer->SetFlexibleDirection( wxBOTH );
	swgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_dosswswavailcheckBox = new YANGCheckBox( m_srcportswpanel, ID_DOSSWSWCHECKBOX, wxT("DOS Shareware v1.2 (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_dosswswavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosswswbintextCtrl = new YANGTextCtrl( m_srcportswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	swgbSizer->Add( m_dosswswbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosswswlocatebutton = new YANGButton( m_srcportswpanel, ID_LOCATEDOSSWSW, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_dosswswlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosswrgavailcheckBox = new YANGCheckBox( m_srcportswpanel, ID_DOSSWRGCHECKBOX, wxT("DOS Registered v1.2 (Requires DOSBox)"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_dosswrgavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosswrgbintextCtrl = new YANGTextCtrl( m_srcportswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	swgbSizer->Add( m_dosswrgbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosswrglocatebutton = new YANGButton( m_srcportswpanel, ID_LOCATEDOSSWRG, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_dosswrglocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_voidswavailcheckBox = new YANGCheckBox( m_srcportswpanel, ID_VOIDSWCHECKBOX, SRCPORTNAME_VOIDSW, wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_voidswavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_voidswbintextCtrl = new YANGTextCtrl( m_srcportswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	swgbSizer->Add( m_voidswbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_voidswlocatebutton = new YANGButton( m_srcportswpanel, ID_LOCATEVOIDSW, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_voidswlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_swpavailcheckBox = new YANGCheckBox( m_srcportswpanel, ID_SWPCHECKBOX, SRCPORTNAME_SWP, wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_swpavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_swpbintextCtrl = new YANGTextCtrl( m_srcportswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	swgbSizer->Add( m_swpbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_swplocatebutton = new YANGButton( m_srcportswpanel, ID_LOCATESWP, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_swplocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_prefswstaticline = new wxStaticLine( m_srcportswpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	swgbSizer->Add( m_prefswstaticline, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_swmapsstaticText = new wxStaticText( m_srcportswpanel, wxID_ANY, wxT("User maps directory"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_swmapsstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_swmapstextCtrl = new YANGTextCtrl( m_srcportswpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	swgbSizer->Add( m_swmapstextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_swmapsselectbutton = new YANGButton( m_srcportswpanel, ID_SELECTSWMAPSFOLDER, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	swgbSizer->Add( m_swmapsselectbutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	swgbSizer->AddGrowableCol( 1 );
	swpanelbSizer->Add( swgbSizer, 1, wxEXPAND, 0 );

	m_srcportswpanel->SetSizer( swpanelbSizer );
	m_srcportswpanel->Layout();
	swpanelbSizer->Fit( m_srcportswpanel );

	m_srcportsnotebook->AddPage( m_srcportswpanel, GAMENAME_SW, false );


	m_srcportcustompanel = new YANGPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* custompanelbSizer = new wxBoxSizer( wxVERTICAL );

	i = -1;
	wxGridBagSizer* customgbSizer = new wxGridBagSizer( 0, 0 );
	customgbSizer->SetFlexibleDirection( wxBOTH );
	customgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_customprofilestaticText = new wxStaticText( m_srcportcustompanel, wxID_ANY, wxT("Custom DOS game profile (Requires DOSBox):"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customprofilestaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customprofilechoice = new YANGComboBox( m_srcportcustompanel, ID_SELECTCUSTOMPROFILE, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)));
	customgbSizer->Add( m_customprofilechoice, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	wxBoxSizer* custombuttonbSizer = new wxBoxSizer( wxHORIZONTAL );

	wxMemoryInputStream istream_add(add_png, sizeof add_png);

	m_customaddnewbutton = new YANGBitmapButton( m_srcportcustompanel, ID_ADDNEWCUSTOMPROFILE, wxBitmap(wxImage(istream_add, wxBITMAP_TYPE_PNG)), wxDefaultPosition, wxDLG_UNIT(this, wxSize(14, 14)) );
	custombuttonbSizer->Add( m_customaddnewbutton, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	wxMemoryInputStream istream_cross(cross_png, sizeof cross_png);

	m_customdeletebutton = new YANGBitmapButton( m_srcportcustompanel, ID_DELETECUSTOMPROFILE, wxBitmap(wxImage(istream_cross, wxBITMAP_TYPE_PNG)), wxDefaultPosition, wxDLG_UNIT(this, wxSize(14, 14)) );
	custombuttonbSizer->Add( m_customdeletebutton, 0, wxALIGN_CENTER_VERTICAL, 0 );

	customgbSizer->Add( custombuttonbSizer, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customstaticline = new wxStaticLine( m_srcportcustompanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	customgbSizer->Add( m_customstaticline, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customsponlycheckBox = new YANGCheckBox( m_srcportcustompanel, ID_CUSTOMSPONLYCHECKBOX, wxT("Single-player mode game only"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customsponlycheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customspbinstaticText = new wxStaticText( m_srcportcustompanel, wxID_ANY, wxT("Single-player mode executable"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customspbinstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customspbintextCtrl = new YANGTextCtrl( m_srcportcustompanel, ID_SPEXECUTABLEINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	customgbSizer->Add( m_customspbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_customspbinlocatebutton = new YANGButton( m_srcportcustompanel, ID_LOCATECUSTOMSP, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customspbinlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customsamebincheckBox = new YANGCheckBox( m_srcportcustompanel, ID_CUSTOMSAMEBINCHECKBOX, wxT("Use same executable for multiplayer mode"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customsamebincheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_custommpbinstaticText = new wxStaticText( m_srcportcustompanel, wxID_ANY, wxT("Multiplayer mode executable"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_custommpbinstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_custommpbintextCtrl = new YANGTextCtrl( m_srcportcustompanel, ID_MPEXECUTABLEINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	customgbSizer->Add( m_custommpbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_custommpbinlocatebutton = new YANGButton( m_srcportcustompanel, ID_LOCATECUSTOMMP, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_custommpbinlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customnocdradioBtn = new YANGRadioButton( m_srcportcustompanel, ID_CUSTOMNOCDRADIOBTN, wxT("Don't mount a CD-ROM"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customnocdradioBtn, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customcdlocationradioBtn = new YANGRadioButton( m_srcportcustompanel, ID_CUSTOMCDLOCATIONRADIOBTN, wxT("Mount a CD-ROM location"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customcdlocationradioBtn, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customcdlocationtextCtrl = new YANGTextCtrl( m_srcportcustompanel, ID_CDLOCATIONINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	customgbSizer->Add( m_customcdlocationtextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_customcdlocationselectbutton = new YANGButton( m_srcportcustompanel, ID_SELECTCUSTOMCDLOCATION, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customcdlocationselectbutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customcdimageradioBtn = new YANGRadioButton( m_srcportcustompanel, ID_CUSTOMCDIMAGERADIOBTN, wxT("Mount a CD-ROM image/block device"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customcdimageradioBtn, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customcdimagetextCtrl = new YANGTextCtrl( m_srcportcustompanel, ID_CDIMAGEINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	customgbSizer->Add( m_customcdimagetextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_customcdimagelocatebutton = new YANGButton( m_srcportcustompanel, ID_LOCATECUSTOMCDIMAGE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customcdimagelocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_customingamesupportcheckBox = new YANGCheckBox( m_srcportcustompanel, ID_INGAMESUPPORTCHECKBOX, wxT("Supports in-game joining multiplayer mode"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customingamesupportcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customnetbioscheckBox = new YANGCheckBox( m_srcportcustompanel, ID_NETBIOSCHECKBOX, wxT("Requires NetBIOS for multiplayer mode"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customnetbioscheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customnetbiostextCtrl = new YANGTextCtrl( m_srcportcustompanel, ID_NETBIOSINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	customgbSizer->Add( m_customnetbiostextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_customnetbioslocatebutton = new YANGButton( m_srcportcustompanel, ID_LOCATENETBIOS, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customnetbioslocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customextraargsstaticText = new wxStaticText( m_srcportcustompanel, wxID_ANY, wxT("Extra arguments in single-player mode:"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customextraargsstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customextraargstextCtrl = new YANGTextCtrl( m_srcportcustompanel, ID_EXTRAARGSINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	customgbSizer->Add( m_customextraargstextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_customextrahostargsstaticText = new wxStaticText( m_srcportcustompanel, wxID_ANY, wxT("Extra arguments just for host (MP mode):"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customextrahostargsstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customextrahostargstextCtrl = new YANGTextCtrl( m_srcportcustompanel, ID_EXTRAHOSTARGSINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	customgbSizer->Add( m_customextrahostargstextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

#if EXTRA_ARGS_FOR_ALL
	m_customextraallargsstaticText = new wxStaticText( m_srcportcustompanel, wxID_ANY, wxT("Extra arguments for everyone (MP mode):"), wxDefaultPosition, wxDefaultSize, 0 );
	customgbSizer->Add( m_customextraallargsstaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_customextraallargstextCtrl = new YANGTextCtrl( m_srcportcustompanel, ID_EXTRAALLARGSINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	customgbSizer->Add( m_customextraallargstextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );
#endif

	customgbSizer->AddGrowableCol( 1 );
	custompanelbSizer->Add( customgbSizer, 1, wxEXPAND, 0 );

	m_srcportcustompanel->SetSizer( custompanelbSizer );
	m_srcportcustompanel->Layout();
	custompanelbSizer->Fit( m_srcportcustompanel );
	m_srcportsnotebook->AddPage( m_srcportcustompanel, wxT("Custom DOS games"), false );


	m_srcportdosboxpanel = new YANGPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* dosboxpanelbSizer = new wxBoxSizer( wxVERTICAL );

	i = -1;
	wxGridBagSizer* dosboxgbSizer = new wxGridBagSizer( 0, 0 );
	dosboxgbSizer->SetFlexibleDirection( wxBOTH );
	dosboxgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_dosboxavailcheckBox = new YANGCheckBox( m_srcportdosboxpanel, ID_DOSBOXCHECKBOX, wxT("I have DOSBox"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxavailcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxbintextCtrl = new YANGTextCtrl( m_srcportdosboxpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	dosboxgbSizer->Add( m_dosboxbintextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxlocatebutton = new YANGButton( m_srcportdosboxpanel, ID_LOCATEDOSBOX, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxlocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxconfcheckBox = new YANGCheckBox( m_srcportdosboxpanel, ID_DOSBOXCONFCHECKBOX, wxT("Use the following configuration file as a base"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxconfcheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxconftextCtrl = new YANGTextCtrl( m_srcportdosboxpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	dosboxgbSizer->Add( m_dosboxconftextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxconflocatebutton = new YANGButton( m_srcportdosboxpanel, ID_LOCATEDOSBOXCONF, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxconflocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxnocdradioBtn = new YANGRadioButton( m_srcportdosboxpanel, ID_DOSBOXNOCDRADIOBTN, wxT("Don't mount a CD-ROM"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxnocdradioBtn, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxcdlocationradioBtn = new YANGRadioButton( m_srcportdosboxpanel, ID_DOSBOXCDLOCATIONRADIOBTN, wxT("Mount a CD-ROM location"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxcdlocationradioBtn, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxcdlocationtextCtrl = new YANGTextCtrl( m_srcportdosboxpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	dosboxgbSizer->Add( m_dosboxcdlocationtextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxcdlocationselectbutton = new YANGButton( m_srcportdosboxpanel, ID_SELECTDOSBOXCDLOCATION, wxT("Select"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxcdlocationselectbutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxcdimageradioBtn = new YANGRadioButton( m_srcportdosboxpanel, ID_DOSBOXCDIMAGERADIOBTN, wxT("Mount a CD-ROM image/block device"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxcdimageradioBtn, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxcdimagetextCtrl = new YANGTextCtrl( m_srcportdosboxpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	dosboxgbSizer->Add( m_dosboxcdimagetextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxcdimagelocatebutton = new YANGButton( m_srcportdosboxpanel, ID_LOCATEDOSBOXCDIMAGE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxcdimagelocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_dosboxhyperlink = new YANGHyperlinkCtrl( m_srcportdosboxpanel, wxID_ANY, wxT("Get DOSBox"), wxT("http://www.dosbox.com/"), wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE );
	dosboxgbSizer->Add( m_dosboxhyperlink, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_dosboxstaticline = new wxStaticLine( m_srcportdosboxpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	dosboxgbSizer->Add( m_dosboxstaticline, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxbmousecheckBox = new YANGCheckBox( m_srcportdosboxpanel, ID_BMOUSECHECKBOX, wxT("Use bMouse with the DOS Build Engine games"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxbmousecheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxbmousetextCtrl = new YANGTextCtrl( m_srcportdosboxpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	dosboxgbSizer->Add( m_dosboxbmousetextCtrl, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_dosboxbmouselocatebutton = new YANGButton( m_srcportdosboxpanel, ID_LOCATEBMOUSE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	dosboxgbSizer->Add( m_dosboxbmouselocatebutton, wxGBPosition( i, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	dosboxgbSizer->AddGrowableCol( 1 );
	dosboxpanelbSizer->Add( dosboxgbSizer, 1, wxEXPAND, 0 );

	m_srcportdosboxpanel->SetSizer( dosboxpanelbSizer );
	m_srcportdosboxpanel->Layout();
	dosboxpanelbSizer->Fit( m_srcportdosboxpanel );
	m_srcportsnotebook->AddPage( m_srcportdosboxpanel, wxT("DOSBox"), false );
#ifndef __WXMSW__
	m_srcportwinepanel = new YANGPanel( m_srcportsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* winepanelbSizer = new wxBoxSizer( wxVERTICAL );

	i = -1;
	wxGridBagSizer* winepanelgbSizer = new wxGridBagSizer( 0, 0 );
	winepanelgbSizer->SetFlexibleDirection( wxBOTH );
	winepanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_usewinecheckBox = new YANGCheckBox( m_srcportwinepanel, ID_WINECHECKBOX, wxT("Use a Windows compatibility pre-loader* for .EXE files"), wxDefaultPosition, wxDefaultSize, 0 );
	winepanelgbSizer->Add( m_usewinecheckBox, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_winebintextCtrl = new YANGTextCtrl( m_srcportwinepanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	winepanelgbSizer->Add( m_winebintextCtrl, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_winelocatebutton = new YANGButton( m_srcportwinepanel, ID_LOCATEWINE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	winepanelgbSizer->Add( m_winelocatebutton, wxGBPosition( i, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_winestaticText = new wxStaticText( m_srcportwinepanel, wxID_ANY, wxT("* That means some kind of a layer or emulator like Wine.\nIt isn't used with DOS games.\n\nBefore the binary you may add some kind\nof a sound wrapper like aoss or padsp."), wxDefaultPosition, wxDefaultSize, 0 );
	winepanelgbSizer->Add( m_winestaticText, wxGBPosition( ++i, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	winepanelgbSizer->AddGrowableCol( 0 );
	winepanelbSizer->Add( winepanelgbSizer, 1, wxEXPAND, 0 );

	m_srcportwinepanel->SetSizer( winepanelbSizer );
	m_srcportwinepanel->Layout();
	winepanelbSizer->Fit( m_srcportwinepanel );
	m_srcportsnotebook->AddPage( m_srcportwinepanel, wxT("Windows compatibility"), false );
#endif
	SRCPortsDialogbSizer->Add( m_srcportsnotebook, 1, wxEXPAND | wxALL, 5 );

	m_srcportssdbSizer = new wxStdDialogButtonSizer();
	m_srcportssdbSizerOK = new YANGButton( this, wxID_OK );
	m_srcportssdbSizer->AddButton( m_srcportssdbSizerOK );
	m_srcportssdbSizerCancel = new YANGButton( this, wxID_CANCEL );
	m_srcportssdbSizer->AddButton( m_srcportssdbSizerCancel );
	m_srcportssdbSizer->Realize();
	SRCPortsDialogbSizer->Add( m_srcportssdbSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxBOTTOM, 5 );

//	m_srcportsnotebook->Layout();
	SetSizer( SRCPortsDialogbSizer );
	Layout();
	SRCPortsDialogbSizer->Fit( this );

	Centre();
	SetIcon(wxIcon(yang_xpm));

	m_dosbloodswavailcheckBox->SetValue(g_configuration->have_dosbloodsw);
	m_dosbloodswavailcheckBox->Enable(g_configuration->have_dosbox);
	m_dosbloodswbintextCtrl->SetValue(g_configuration->dosbloodsw_exec);
	m_dosbloodswbintextCtrl->Enable(g_configuration->have_dosbloodsw && g_configuration->have_dosbox);
	m_dosbloodswlocatebutton->Enable(g_configuration->have_dosbloodsw && g_configuration->have_dosbox);

	m_dosbloodrgavailcheckBox->SetValue(g_configuration->have_dosbloodrg);
	m_dosbloodrgavailcheckBox->Enable(g_configuration->have_dosbox);
	m_dosbloodrgbintextCtrl->SetValue(g_configuration->dosbloodrg_exec);
	m_dosbloodrgbintextCtrl->Enable(g_configuration->have_dosbloodrg && g_configuration->have_dosbox);
	m_dosbloodrglocatebutton->Enable(g_configuration->have_dosbloodrg && g_configuration->have_dosbox);

	m_dosbloodppavailcheckBox->SetValue(g_configuration->have_dosbloodpp);
	m_dosbloodppavailcheckBox->Enable(g_configuration->have_dosbox);
	m_dosbloodppbintextCtrl->SetValue(g_configuration->dosbloodpp_exec);
	m_dosbloodppbintextCtrl->Enable(g_configuration->have_dosbloodpp && g_configuration->have_dosbox);
	m_dosbloodpplocatebutton->Enable(g_configuration->have_dosbloodpp && g_configuration->have_dosbox);

	m_dosbloodouavailcheckBox->SetValue(g_configuration->have_dosbloodou);
	m_dosbloodouavailcheckBox->Enable(g_configuration->have_dosbox);
	m_dosbloodoubintextCtrl->SetValue(g_configuration->dosbloodou_exec);
	m_dosbloodoubintextCtrl->Enable(g_configuration->have_dosbloodou && g_configuration->have_dosbox);
	m_dosbloodoulocatebutton->Enable(g_configuration->have_dosbloodou && g_configuration->have_dosbox);

	m_bloodmapstextCtrl->SetValue(g_configuration->blood_maps_dir);

	m_dosdescentavailcheckBox->SetValue(g_configuration->have_dosdescent);
	m_dosdescentavailcheckBox->Enable(g_configuration->have_dosbox);
	m_dosdescentbintextCtrl->SetValue(g_configuration->dosdescent_exec);
	m_dosdescentbintextCtrl->Enable(g_configuration->have_dosdescent && g_configuration->have_dosbox);
	m_dosdescentlocatebutton->Enable(g_configuration->have_dosdescent && g_configuration->have_dosbox);

	m_d1xrebirthavailcheckBox->SetValue(g_configuration->have_d1xrebirth);
	m_d1xrebirthbintextCtrl->SetValue(g_configuration->d1xrebirth_exec);
	m_d1xrebirthbintextCtrl->Enable(g_configuration->have_d1xrebirth);
	m_d1xrebirthlocatebutton->Enable(g_configuration->have_d1xrebirth);

	m_descentmapstextCtrl->SetValue(g_configuration->descent_maps_dir);

	m_dosdescent2availcheckBox->SetValue(g_configuration->have_dosdescent2);
	m_dosdescent2availcheckBox->Enable(g_configuration->have_dosbox);
	m_dosdescent2bintextCtrl->SetValue(g_configuration->dosdescent2_exec);
	m_dosdescent2bintextCtrl->Enable(g_configuration->have_dosdescent2 && g_configuration->have_dosbox);
	m_dosdescent2locatebutton->Enable(g_configuration->have_dosdescent2 && g_configuration->have_dosbox);

	m_d2xrebirthavailcheckBox->SetValue(g_configuration->have_d2xrebirth);
	m_d2xrebirthbintextCtrl->SetValue(g_configuration->d2xrebirth_exec);
	m_d2xrebirthbintextCtrl->Enable(g_configuration->have_d2xrebirth);
	m_d2xrebirthlocatebutton->Enable(g_configuration->have_d2xrebirth);

	m_descent2mapstextCtrl->SetValue(g_configuration->descent2_maps_dir);

	m_dosdukeswavailcheckBox->SetValue(g_configuration->have_dosdukesw);
	m_dosdukeswavailcheckBox->Enable(g_configuration->have_dosbox);
	m_dosdukeswbintextCtrl->SetValue(g_configuration->dosdukesw_exec);
	m_dosdukeswbintextCtrl->Enable(g_configuration->have_dosdukesw && g_configuration->have_dosbox);
	m_dosdukeswlocatebutton->Enable(g_configuration->have_dosdukesw && g_configuration->have_dosbox);

	m_dosdukergavailcheckBox->SetValue(g_configuration->have_dosdukerg);
	m_dosdukergavailcheckBox->Enable(g_configuration->have_dosbox);
	m_dosdukergbintextCtrl->SetValue(g_configuration->dosdukerg_exec);
	m_dosdukergbintextCtrl->Enable(g_configuration->have_dosdukerg && g_configuration->have_dosbox);
	m_dosdukerglocatebutton->Enable(g_configuration->have_dosdukerg && g_configuration->have_dosbox);

	m_dosdukeaeavailcheckBox->SetValue(g_configuration->have_dosdukeae);
	m_dosdukeaeavailcheckBox->Enable(g_configuration->have_dosbox);
	m_dosdukeaebintextCtrl->SetValue(g_configuration->dosdukeae_exec);
	m_dosdukeaebintextCtrl->Enable(g_configuration->have_dosdukeae && g_configuration->have_dosbox);
	m_dosdukeaelocatebutton->Enable(g_configuration->have_dosdukeae && g_configuration->have_dosbox);

	m_duke3dwavailcheckBox->SetValue(g_configuration->have_duke3dw);
	m_duke3dwbintextCtrl->SetValue(g_configuration->duke3dw_exec);
	m_duke3dwbintextCtrl->Enable(g_configuration->have_duke3dw);
	m_duke3dwlocatebutton->Enable(g_configuration->have_duke3dw);

	m_eduke32availcheckBox->SetValue(g_configuration->have_eduke32);
	m_eduke32bintextCtrl->SetValue(g_configuration->eduke32_exec);
	m_eduke32bintextCtrl->Enable(g_configuration->have_eduke32);
	m_eduke32locatebutton->Enable(g_configuration->have_eduke32);

	m_ndukeavailcheckBox->SetValue(g_configuration->have_nduke);
	m_ndukebintextCtrl->SetValue(g_configuration->nduke_exec);
	m_ndukebintextCtrl->Enable(g_configuration->have_nduke);
	m_ndukelocatebutton->Enable(g_configuration->have_nduke);

	m_hdukeavailcheckBox->SetValue(g_configuration->have_hduke);
	m_hdukebintextCtrl->SetValue(g_configuration->hduke_exec);
	m_hdukebintextCtrl->Enable(g_configuration->have_hduke);
	m_hdukelocatebutton->Enable(g_configuration->have_hduke);

	m_xdukeavailcheckBox->SetValue(g_configuration->have_xduke);
	m_xdukebintextCtrl->SetValue(g_configuration->xduke_exec);
	m_xdukebintextCtrl->Enable(g_configuration->have_xduke);
	m_xdukelocatebutton->Enable(g_configuration->have_xduke);

	m_duke3dmapstextCtrl->SetValue(g_configuration->dn3d_maps_dir);

	m_dosswswavailcheckBox->SetValue(g_configuration->have_dosswsw);
	m_dosswswavailcheckBox->Enable(g_configuration->have_dosbox);
	m_dosswswbintextCtrl->SetValue(g_configuration->dosswsw_exec);
	m_dosswswbintextCtrl->Enable(g_configuration->have_dosswsw && g_configuration->have_dosbox);
	m_dosswswlocatebutton->Enable(g_configuration->have_dosswsw && g_configuration->have_dosbox);

	m_dosswrgavailcheckBox->SetValue(g_configuration->have_dosswrg);
	m_dosswrgavailcheckBox->Enable(g_configuration->have_dosbox);
	m_dosswrgbintextCtrl->SetValue(g_configuration->dosswrg_exec);
	m_dosswrgbintextCtrl->Enable(g_configuration->have_dosswrg && g_configuration->have_dosbox);
	m_dosswrglocatebutton->Enable(g_configuration->have_dosswrg && g_configuration->have_dosbox);

	m_voidswavailcheckBox->SetValue(g_configuration->have_voidsw);
	m_voidswbintextCtrl->SetValue(g_configuration->voidsw_exec);
	m_voidswbintextCtrl->Enable(g_configuration->have_voidsw);
	m_voidswlocatebutton->Enable(g_configuration->have_voidsw);

	m_swpavailcheckBox->SetValue(g_configuration->have_swp);
	m_swpbintextCtrl->SetValue(g_configuration->swp_exec);
	m_swpbintextCtrl->Enable(g_configuration->have_swp);
	m_swplocatebutton->Enable(g_configuration->have_swp);

	m_swmapstextCtrl->SetValue(g_configuration->sw_maps_dir);

	m_dosboxavailcheckBox->SetValue(g_configuration->have_dosbox);
	m_dosboxbintextCtrl->SetValue(g_configuration->dosbox_exec);
	m_dosboxbintextCtrl->Enable(g_configuration->have_dosbox);
	m_dosboxlocatebutton->Enable(g_configuration->have_dosbox);

	m_dosboxconfcheckBox->SetValue(g_configuration->dosbox_use_conf);
	m_dosboxconfcheckBox->Enable(g_configuration->have_dosbox);
	m_dosboxconftextCtrl->SetValue(g_configuration->dosbox_conf);
	m_dosboxconftextCtrl->Enable(g_configuration->have_dosbox && g_configuration->dosbox_use_conf);
	m_dosboxconflocatebutton->Enable(g_configuration->have_dosbox && g_configuration->dosbox_use_conf);

	m_dosboxnocdradioBtn->SetValue((g_configuration->dosbox_cdmount == CDROMMOUNT_NONE));
	m_dosboxnocdradioBtn->Enable(g_configuration->have_dosbox);

	bool l_temp_bool = (g_configuration->dosbox_cdmount == CDROMMOUNT_DIR);
	m_dosboxcdlocationradioBtn->SetValue(l_temp_bool);
	m_dosboxcdlocationradioBtn->Enable(g_configuration->have_dosbox);
	m_dosboxcdlocationtextCtrl->SetValue(g_configuration->dosbox_cdrom_location);
	l_temp_bool = (g_configuration->have_dosbox && l_temp_bool);
	m_dosboxcdlocationtextCtrl->Enable(l_temp_bool);
	m_dosboxcdlocationselectbutton->Enable(l_temp_bool);

	l_temp_bool = (g_configuration->dosbox_cdmount == CDROMMOUNT_IMG);
	m_dosboxcdimageradioBtn->SetValue(l_temp_bool);
	m_dosboxcdimageradioBtn->Enable(g_configuration->have_dosbox);
	m_dosboxcdimagetextCtrl->SetValue(g_configuration->dosbox_cdrom_image);
	l_temp_bool = (g_configuration->have_dosbox && l_temp_bool);
	m_dosboxcdimagetextCtrl->Enable(l_temp_bool);
	m_dosboxcdimagelocatebutton->Enable(l_temp_bool);

	m_dosboxbmousecheckBox->SetValue(g_configuration->have_bmouse);
	m_dosboxbmousecheckBox->Enable(g_configuration->have_dosbox);
	m_dosboxbmousetextCtrl->SetValue(g_configuration->bmouse_exec);
	m_dosboxbmousetextCtrl->Enable(g_configuration->have_dosbox && g_configuration->have_bmouse);
	m_dosboxbmouselocatebutton->Enable(g_configuration->have_dosbox && g_configuration->have_bmouse);

#ifndef __WXMSW__
	m_usewinecheckBox->SetValue(g_configuration->have_wine);
	m_winebintextCtrl->SetValue(g_configuration->wine_exec);
	m_winebintextCtrl->Enable(g_configuration->have_wine);
	m_winelocatebutton->Enable(g_configuration->have_wine);
#endif

	m_customprofilestaticText->Enable(g_configuration->have_dosbox);
	m_customprofilechoice->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customaddnewbutton->Enable(g_configuration->have_dosbox);
	m_customdeletebutton->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customsponlycheckBox->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customspbinstaticText->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customspbintextCtrl->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customspbinlocatebutton->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customsamebincheckBox->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_custommpbinstaticText->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_custommpbintextCtrl->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_custommpbinlocatebutton->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customnocdradioBtn->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customcdlocationradioBtn->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customcdlocationtextCtrl->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customcdlocationselectbutton->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customcdimageradioBtn->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customcdimagetextCtrl->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customcdimagelocatebutton->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customingamesupportcheckBox->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customnetbioscheckBox->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customnetbiostextCtrl->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customnetbioslocatebutton->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customextraargsstaticText->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customextraargstextCtrl->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customextrahostargsstaticText->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customextrahostargstextCtrl->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
#if EXTRA_ARGS_FOR_ALL
	m_customextraallargsstaticText->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
	m_customextraallargstextCtrl->Enable(g_configuration->have_dosbox && g_configuration->custom_profile_list);
#endif

	if (g_configuration->custom_profile_list)
	{
		g_configuration->custom_profile_current = g_configuration->custom_profile_list;

		custom_profile_list_temp = new custom_profile_t;
		*custom_profile_list_temp = *g_configuration->custom_profile_current;

		while (g_configuration->custom_profile_current->next)
		{
			g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;

			custom_profile_list_temp->next = new custom_profile_t;
			*custom_profile_list_temp->next = *g_configuration->custom_profile_current;
			custom_profile_list_temp->next->previous = custom_profile_list_temp;
			custom_profile_list_temp = custom_profile_list_temp->next;
		}

		while (custom_profile_list_temp->previous)
			custom_profile_list_temp = custom_profile_list_temp->previous;


		custom_profile_current_temp = custom_profile_list_temp;

		while (custom_profile_current_temp)
		{
			m_customprofilechoice->Append(custom_profile_current_temp->profilename);

			custom_profile_current_temp = custom_profile_current_temp->next;
		}

		m_customprofilechoice->Select(0);

		custom_profile_index = 0;

		custom_profile_current_temp = custom_profile_list_temp;

		m_customsponlycheckBox->SetValue(custom_profile_current_temp->spgameonly);

		m_customspbintextCtrl->ChangeValue(custom_profile_current_temp->spexecutable);

		m_customsamebincheckBox->SetValue(custom_profile_current_temp->sameexecutable);
		m_customsamebincheckBox->Enable(!custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);

		m_custommpbinstaticText->Enable(!custom_profile_current_temp->sameexecutable && !custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);
		m_custommpbintextCtrl->ChangeValue(custom_profile_current_temp->mpexecutable);
		m_custommpbintextCtrl->Enable(!custom_profile_current_temp->sameexecutable && !custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);
		m_custommpbinlocatebutton->Enable(!custom_profile_current_temp->sameexecutable && !custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);

		m_customnocdradioBtn->SetValue((custom_profile_current_temp->cdmount == CDROMMOUNT_NONE));

		bool l_temp_bool = (custom_profile_current_temp->cdmount == CDROMMOUNT_DIR);
		m_customcdlocationradioBtn->SetValue(l_temp_bool);
		m_customcdlocationtextCtrl->ChangeValue(custom_profile_current_temp->cdlocation);
		m_customcdlocationtextCtrl->Enable(l_temp_bool && g_configuration->have_dosbox);
		m_customcdlocationselectbutton->Enable(l_temp_bool && g_configuration->have_dosbox);

		l_temp_bool = (custom_profile_current_temp->cdmount == CDROMMOUNT_IMG);
		m_customcdimageradioBtn->SetValue(l_temp_bool);
		m_customcdimagetextCtrl->ChangeValue(custom_profile_current_temp->cdimage);
		m_customcdimagetextCtrl->Enable(l_temp_bool && g_configuration->have_dosbox);
		m_customcdimagelocatebutton->Enable(l_temp_bool && g_configuration->have_dosbox);

		m_customingamesupportcheckBox->SetValue(custom_profile_current_temp->ingamesupport);
		m_customingamesupportcheckBox->Enable(!custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);

		m_customnetbioscheckBox->SetValue(custom_profile_current_temp->usenetbios);
		m_customnetbioscheckBox->Enable(!custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);
		m_customnetbiostextCtrl->ChangeValue(custom_profile_current_temp->netbiospath);
		m_customnetbiostextCtrl->Enable(custom_profile_current_temp->usenetbios && !custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);
		m_customnetbioslocatebutton->Enable(custom_profile_current_temp->usenetbios && !custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);

		m_customextraargstextCtrl->ChangeValue(custom_profile_current_temp->extraargs);

		m_customextrahostargsstaticText->Enable(!custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);
		m_customextrahostargstextCtrl->ChangeValue(custom_profile_current_temp->extrahostargs);
		m_customextrahostargstextCtrl->Enable(!custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);

#if EXTRA_ARGS_FOR_ALL
		m_customextraallargsstaticText->Enable(!custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);
		m_customextraallargstextCtrl->ChangeValue(custom_profile_current_temp->extraallargs);
		m_customextraallargstextCtrl->Enable(!custom_profile_current_temp->spgameonly && g_configuration->have_dosbox);
#endif
	}
	else
	{
		custom_profile_list_temp = NULL;

		m_customsamebincheckBox->SetValue(true);
		m_customnocdradioBtn->SetValue(true);
	}
}

SRCPortsDialog::~SRCPortsDialog()
{
	if (g_main_frame)
		g_main_frame->ShowMainFrame();
	else
	{
		g_network_dialog = new NetworkDialog;
		g_network_dialog->Show(true);
		g_network_dialog->SetFocus();
		//  g_network_dialog->Raise();
	}
	//g_srcports_dialog = NULL;
}

void SRCPortsDialog::OnLocateFile(wxCommandEvent& event)
{
	wxFileDialog l_dialog(NULL, wxT("Select a file"), wxEmptyString, wxEmptyString,
		wxT("All files|*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);
	if (l_dialog.ShowModal() == wxID_OK)
	{
		wxString l_fullpath = l_dialog.GetPath();
		switch (event.GetId())
		{
			case ID_LOCATEDOSBLOODSW:    m_dosbloodswbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSBLOODRG:    m_dosbloodrgbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSBLOODPP:    m_dosbloodppbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSBLOODOU:    m_dosbloodoubintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSDESCENT:    m_dosdescentbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATED1XREBIRTH:    m_d1xrebirthbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSDESCENT2:   m_dosdescent2bintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATED2XREBIRTH:    m_d2xrebirthbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSDUKESW:     m_dosdukeswbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSDUKERG:     m_dosdukergbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSDUKEAE:     m_dosdukeaebintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDUKE3DW:       m_duke3dwbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEEDUKE32:       m_eduke32bintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATENDUKE:         m_ndukebintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEHDUKE:         m_hdukebintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEXDUKE:         m_xdukebintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSSWSW:       m_dosswswbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSSWRG:       m_dosswrgbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEVOIDSW:        m_voidswbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATESWP:           m_swpbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATECUSTOMSP:      m_customspbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATECUSTOMMP:      m_custommpbintextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATECUSTOMCDIMAGE: m_customcdimagetextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATENETBIOS:       m_customnetbiostextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSBOX:        m_dosboxbintextCtrl->SetValue(l_fullpath);
				l_fullpath = ((wxFileName)l_fullpath).GetPath(wxPATH_GET_VOLUME|wxPATH_GET_SEPARATOR) + wxT("dosbox.conf");
				// A TINY TRICK HERE... Continue and set the configuration file
				if (!wxFileExists(l_fullpath))
					break;
			case ID_LOCATEDOSBOXCONF:    m_dosboxconftextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEDOSBOXCDIMAGE: m_dosboxcdimagetextCtrl->SetValue(l_fullpath); break;
			case ID_LOCATEBMOUSE:        m_dosboxbmousetextCtrl->SetValue(l_fullpath); break;
#ifndef __WXMSW__
			case ID_LOCATEWINE:          m_winebintextCtrl->SetValue(l_fullpath); break;
#endif
			default: ;
		}
	}
}

void SRCPortsDialog::OnSelectDir(wxCommandEvent& event)
{
	wxDirDialog l_dialog(NULL, wxT("Select a directory"), wxEmptyString,
		wxDD_DEFAULT_STYLE|wxDD_DIR_MUST_EXIST);
	if (l_dialog.ShowModal() == wxID_OK)
	{
		wxString l_path = l_dialog.GetPath();
		switch (event.GetId())
		{
			case ID_SELECTBLOODMAPSFOLDER:    m_bloodmapstextCtrl->SetValue(l_path); break;
			case ID_SELECTDESCENTMAPSFOLDER:  m_descentmapstextCtrl->SetValue(l_path); break;
			case ID_SELECTDESCENT2MAPSFOLDER: m_descent2mapstextCtrl->SetValue(l_path); break;
			case ID_SELECTDUKEMAPSFOLDER:     m_duke3dmapstextCtrl->SetValue(l_path); break;
			case ID_SELECTSWMAPSFOLDER:       m_swmapstextCtrl->SetValue(l_path); break;
			case ID_SELECTCUSTOMCDLOCATION:   m_customcdlocationtextCtrl->SetValue(l_path); break;
			case ID_SELECTDOSBOXCDLOCATION:   m_dosboxcdlocationtextCtrl->SetValue(l_path); break;
			default: ;
		}
	}
}

void SRCPortsDialog::OnCheckBoxClick(wxCommandEvent& event)
{
	bool l_selection = event.IsChecked();

	switch (event.GetId())
	{
		case ID_DOSBLOODSWCHECKBOX:
			m_dosbloodswbintextCtrl->Enable(l_selection);
			m_dosbloodswlocatebutton->Enable(l_selection);
			break;
		case ID_DOSBLOODRGCHECKBOX:
			m_dosbloodrgbintextCtrl->Enable(l_selection);
			m_dosbloodrglocatebutton->Enable(l_selection);
			break;
		case ID_DOSBLOODPPCHECKBOX:
			m_dosbloodppbintextCtrl->Enable(l_selection);
			m_dosbloodpplocatebutton->Enable(l_selection);
			break;
		case ID_DOSBLOODOUCHECKBOX:
			m_dosbloodoubintextCtrl->Enable(l_selection);
			m_dosbloodoulocatebutton->Enable(l_selection);
			break;
		case ID_DOSDESCENTCHECKBOX:
			m_dosdescentbintextCtrl->Enable(l_selection);
			m_dosdescentlocatebutton->Enable(l_selection);
			break;
		case ID_D1XREBIRTHCHECKBOX:
			m_d1xrebirthbintextCtrl->Enable(l_selection);
			m_d1xrebirthlocatebutton->Enable(l_selection);
			break;
		case ID_DOSDESCENT2CHECKBOX:
			m_dosdescent2bintextCtrl->Enable(l_selection);
			m_dosdescent2locatebutton->Enable(l_selection);
			break;
		case ID_D2XREBIRTHCHECKBOX:
			m_d2xrebirthbintextCtrl->Enable(l_selection);
			m_d2xrebirthlocatebutton->Enable(l_selection);
			break;
		case ID_DOSDUKESWCHECKBOX:
			m_dosdukeswbintextCtrl->Enable(l_selection);
			m_dosdukeswlocatebutton->Enable(l_selection);
			break;
		case ID_DOSDUKERGCHECKBOX:
			m_dosdukergbintextCtrl->Enable(l_selection);
			m_dosdukerglocatebutton->Enable(l_selection);
			break;
		case ID_DOSDUKEAECHECKBOX:
			m_dosdukeaebintextCtrl->Enable(l_selection);
			m_dosdukeaelocatebutton->Enable(l_selection);
			break;
		case ID_DUKE3DWCHECKBOX:
			m_duke3dwbintextCtrl->Enable(l_selection);
			m_duke3dwlocatebutton->Enable(l_selection);
			break;
		case ID_EDUKE32CHECKBOX:
			m_eduke32bintextCtrl->Enable(l_selection);
			m_eduke32locatebutton->Enable(l_selection);
			break;
		case ID_NDUKECHECKBOX:
			m_ndukebintextCtrl->Enable(l_selection);
			m_ndukelocatebutton->Enable(l_selection);
			break;
		case ID_HDUKECHECKBOX:
			m_hdukebintextCtrl->Enable(l_selection);
			m_hdukelocatebutton->Enable(l_selection);
			break;
		case ID_XDUKECHECKBOX:
			m_xdukebintextCtrl->Enable(l_selection);
			m_xdukelocatebutton->Enable(l_selection);
			break;
		case ID_DOSSWSWCHECKBOX:
			m_dosswswbintextCtrl->Enable(l_selection);
			m_dosswswlocatebutton->Enable(l_selection);
			break;
		case ID_DOSSWRGCHECKBOX:
			m_dosswrgbintextCtrl->Enable(l_selection);
			m_dosswrglocatebutton->Enable(l_selection);
			break;
		case ID_VOIDSWCHECKBOX:
			m_voidswbintextCtrl->Enable(l_selection);
			m_voidswlocatebutton->Enable(l_selection);
			break;
		case ID_SWPCHECKBOX:
			m_swpbintextCtrl->Enable(l_selection);
			m_swplocatebutton->Enable(l_selection);
			break;
		case ID_CUSTOMSPONLYCHECKBOX:
			m_customsamebincheckBox->Enable(!l_selection);
			m_custommpbinstaticText->Enable(!l_selection);
			m_custommpbintextCtrl->Enable(!l_selection);
			m_custommpbinlocatebutton->Enable(!l_selection);
			m_customingamesupportcheckBox->Enable(!l_selection);
			m_customnetbioscheckBox->Enable(!l_selection);
			m_customnetbiostextCtrl->Enable(!l_selection && custom_profile_current_temp->usenetbios);
			m_customnetbioslocatebutton->Enable(!l_selection && custom_profile_current_temp->usenetbios);
			m_customextrahostargsstaticText->Enable(!l_selection);
			m_customextrahostargstextCtrl->Enable(!l_selection);
#if EXTRA_ARGS_FOR_ALL
			m_customextraallargsstaticText->Enable(!l_selection);
			m_customextraallargstextCtrl->Enable(!l_selection);
#endif

			custom_profile_current_temp->spgameonly = l_selection;

			if (l_selection)
			{
				m_customsamebincheckBox->SetValue(false);
				custom_profile_current_temp->sameexecutable = false;
				m_custommpbintextCtrl->ChangeValue(wxEmptyString);
				custom_profile_current_temp->mpexecutable = wxEmptyString;
				m_customingamesupportcheckBox->SetValue(false);
				custom_profile_current_temp->ingamesupport = false;
				m_customnetbioscheckBox->SetValue(false);
				custom_profile_current_temp->usenetbios = false;
				m_customnetbiostextCtrl->ChangeValue(wxEmptyString);
				custom_profile_current_temp->netbiospath = wxEmptyString;
				m_customextrahostargstextCtrl->ChangeValue(wxEmptyString);
				custom_profile_current_temp->extrahostargs = wxEmptyString;
#if EXTRA_ARGS_FOR_ALL
				m_customextraallargstextCtrl->ChangeValue(wxEmptyString);
				custom_profile_current_temp->extraallargs = wxEmptyString;
#endif
			}
			break;
		case ID_CUSTOMSAMEBINCHECKBOX:
			m_custommpbinstaticText->Enable(!l_selection);
			m_custommpbintextCtrl->Enable(!l_selection);
			m_custommpbinlocatebutton->Enable(!l_selection);

			custom_profile_current_temp->sameexecutable = l_selection;

			if (l_selection)
			{
				m_custommpbintextCtrl->ChangeValue(custom_profile_current_temp->spexecutable);
				custom_profile_current_temp->mpexecutable = custom_profile_current_temp->spexecutable;
			}
			else
			{
				m_custommpbintextCtrl->ChangeValue(wxEmptyString);
				custom_profile_current_temp->mpexecutable = wxEmptyString;
			}
			break;
		case ID_INGAMESUPPORTCHECKBOX:
			custom_profile_current_temp->ingamesupport = l_selection;
			break;
		case ID_NETBIOSCHECKBOX:
			m_customnetbiostextCtrl->Enable(l_selection);
			m_customnetbioslocatebutton->Enable(l_selection);

			custom_profile_current_temp->usenetbios = l_selection;

			if (!l_selection)
			{
				m_customnetbiostextCtrl->ChangeValue(wxEmptyString);
				custom_profile_current_temp->netbiospath = wxEmptyString;
			}
			break;
		case ID_DOSBOXCHECKBOX:
			m_dosboxbintextCtrl->Enable(l_selection);
			m_dosboxlocatebutton->Enable(l_selection);
			m_dosboxconfcheckBox->Enable(l_selection);
			bool l_temp_bool;
			l_temp_bool = (l_selection && (m_dosboxconfcheckBox->GetValue()));
			m_dosboxconftextCtrl->Enable(l_temp_bool);
			m_dosboxconflocatebutton->Enable(l_temp_bool);
			m_dosboxnocdradioBtn->Enable(l_selection);
			m_dosboxcdlocationradioBtn->Enable(l_selection);
			l_temp_bool = (l_selection && m_dosboxcdlocationradioBtn->GetValue());
			m_dosboxcdlocationtextCtrl->Enable(l_temp_bool);
			m_dosboxcdlocationselectbutton->Enable(l_temp_bool);
			m_dosboxcdimageradioBtn->Enable(l_selection);
			l_temp_bool = (l_selection && m_dosboxcdimageradioBtn->GetValue());
			m_dosboxcdimagetextCtrl->Enable(l_temp_bool);
			m_dosboxcdimagelocatebutton->Enable(l_temp_bool);
			m_dosboxbmousecheckBox->Enable(l_selection);
			l_temp_bool = (l_selection && (m_dosboxbmousecheckBox->GetValue()));
			m_dosboxbmousetextCtrl->Enable(l_temp_bool);
			m_dosboxbmouselocatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosbloodswavailcheckBox->GetValue());
			m_dosbloodswavailcheckBox->Enable(l_selection);
			m_dosbloodswbintextCtrl->Enable(l_temp_bool);
			m_dosbloodswlocatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosbloodrgavailcheckBox->GetValue());
			m_dosbloodrgavailcheckBox->Enable(l_selection);
			m_dosbloodrgbintextCtrl->Enable(l_temp_bool);
			m_dosbloodrglocatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosbloodppavailcheckBox->GetValue());
			m_dosbloodppavailcheckBox->Enable(l_selection);
			m_dosbloodppbintextCtrl->Enable(l_temp_bool);
			m_dosbloodpplocatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosbloodouavailcheckBox->GetValue());
			m_dosbloodouavailcheckBox->Enable(l_selection);
			m_dosbloodoubintextCtrl->Enable(l_temp_bool);
			m_dosbloodoulocatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosdescentavailcheckBox->GetValue());
			m_dosdescentavailcheckBox->Enable(l_selection);
			m_dosdescentbintextCtrl->Enable(l_temp_bool);
			m_dosdescentlocatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosdescent2availcheckBox->GetValue());
			m_dosdescent2availcheckBox->Enable(l_selection);
			m_dosdescent2bintextCtrl->Enable(l_temp_bool);
			m_dosdescent2locatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosdukeswavailcheckBox->GetValue());
			m_dosdukeswavailcheckBox->Enable(l_selection);
			m_dosdukeswbintextCtrl->Enable(l_temp_bool);
			m_dosdukeswlocatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosdukergavailcheckBox->GetValue());
			m_dosdukergavailcheckBox->Enable(l_selection);
			m_dosdukergbintextCtrl->Enable(l_temp_bool);
			m_dosdukerglocatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosdukeaeavailcheckBox->GetValue());
			m_dosdukeaeavailcheckBox->Enable(l_selection);
			m_dosdukeaebintextCtrl->Enable(l_temp_bool);
			m_dosdukeaelocatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosswswavailcheckBox->GetValue());
			m_dosswswavailcheckBox->Enable(l_selection);
			m_dosswswbintextCtrl->Enable(l_temp_bool);
			m_dosswswlocatebutton->Enable(l_temp_bool);
			l_temp_bool = (l_selection && m_dosswrgavailcheckBox->GetValue());
			m_dosswrgavailcheckBox->Enable(l_selection);
			m_dosswrgbintextCtrl->Enable(l_temp_bool);
			m_dosswrglocatebutton->Enable(l_temp_bool);

			m_customprofilestaticText->Enable(l_selection);
			m_customprofilechoice->Enable(l_selection && custom_profile_list_temp);
			m_customaddnewbutton->Enable(l_selection);
			m_customdeletebutton->Enable(l_selection && custom_profile_list_temp);
			m_customsponlycheckBox->Enable(l_selection && custom_profile_list_temp);
			m_customspbinstaticText->Enable(l_selection && custom_profile_list_temp);
			m_customspbintextCtrl->Enable(l_selection && custom_profile_list_temp);
			m_customspbinlocatebutton->Enable(l_selection && custom_profile_list_temp);
			m_customsamebincheckBox->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly);
			m_custommpbinstaticText->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly && !custom_profile_current_temp->sameexecutable);
			m_custommpbintextCtrl->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly && !custom_profile_current_temp->sameexecutable);
			m_custommpbinlocatebutton->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly && !custom_profile_current_temp->sameexecutable);
			m_customnocdradioBtn->Enable(l_selection && custom_profile_list_temp);
			m_customcdlocationradioBtn->Enable(l_selection && custom_profile_list_temp);
			m_customcdlocationtextCtrl->Enable(l_selection && custom_profile_list_temp && custom_profile_current_temp->cdmount == CDROMMOUNT_DIR);
			m_customcdlocationselectbutton->Enable(l_selection && custom_profile_list_temp && custom_profile_current_temp->cdmount == CDROMMOUNT_DIR);
			m_customcdimageradioBtn->Enable(l_selection && custom_profile_list_temp);
			m_customcdimagetextCtrl->Enable(l_selection && custom_profile_list_temp && custom_profile_current_temp->cdmount == CDROMMOUNT_IMG);
			m_customcdimagelocatebutton->Enable(l_selection && custom_profile_list_temp && custom_profile_current_temp->cdmount == CDROMMOUNT_IMG);
			m_customingamesupportcheckBox->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly);
			m_customnetbioscheckBox->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly);
			m_customnetbiostextCtrl->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly && custom_profile_current_temp->usenetbios);
			m_customnetbioslocatebutton->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly && custom_profile_current_temp->usenetbios);
			m_customextraargsstaticText->Enable(l_selection && custom_profile_list_temp);
			m_customextraargstextCtrl->Enable(l_selection && custom_profile_list_temp);
			m_customextrahostargsstaticText->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly);
			m_customextrahostargstextCtrl->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly);
#if EXTRA_ARGS_FOR_ALL
			m_customextraallargsstaticText->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly);
			m_customextraallargstextCtrl->Enable(l_selection && custom_profile_list_temp && !custom_profile_current_temp->spgameonly);
#endif
			break;
		case ID_DOSBOXCONFCHECKBOX:
			m_dosboxconftextCtrl->Enable(l_selection);
			m_dosboxconflocatebutton->Enable(l_selection);
			break;
		case ID_BMOUSECHECKBOX:
			m_dosboxbmousetextCtrl->Enable(l_selection);
			m_dosboxbmouselocatebutton->Enable(l_selection);
			break;
#ifndef __WXMSW__
		case ID_WINECHECKBOX:
			m_winebintextCtrl->Enable(l_selection);
			m_winelocatebutton->Enable(l_selection);
			break;
#endif
		default: ;
	}
}

void SRCPortsDialog::OnRadioBtnClick(wxCommandEvent& event)
{
	switch (event.GetId())
	{
		case ID_CUSTOMNOCDRADIOBTN:
		case ID_CUSTOMCDLOCATIONRADIOBTN:
		case ID_CUSTOMCDIMAGERADIOBTN:
			m_customcdlocationtextCtrl->Enable(m_customcdlocationradioBtn->GetValue());
			m_customcdlocationselectbutton->Enable(m_customcdlocationradioBtn->GetValue());
			m_customcdimagetextCtrl->Enable(m_customcdimageradioBtn->GetValue());
			m_customcdimagelocatebutton->Enable(m_customcdimageradioBtn->GetValue());

			if (m_customnocdradioBtn->GetValue())
			{
				custom_profile_current_temp->cdmount = CDROMMOUNT_NONE;
				m_customcdlocationtextCtrl->ChangeValue(wxEmptyString);
				custom_profile_current_temp->cdlocation = wxEmptyString;
				m_customcdimagetextCtrl->ChangeValue(wxEmptyString);
				custom_profile_current_temp->cdimage = wxEmptyString;
			}
			else if (m_customcdlocationradioBtn->GetValue())
			{
				custom_profile_current_temp->cdmount = CDROMMOUNT_DIR;
				m_customcdimagetextCtrl->ChangeValue(wxEmptyString);
				custom_profile_current_temp->cdimage = wxEmptyString;
			}
			else
			{
				custom_profile_current_temp->cdmount = CDROMMOUNT_IMG;
				m_customcdlocationtextCtrl->ChangeValue(wxEmptyString);
				custom_profile_current_temp->cdlocation = wxEmptyString;
			}

			break;

		case ID_DOSBOXNOCDRADIOBTN:
		case ID_DOSBOXCDLOCATIONRADIOBTN:
		case ID_DOSBOXCDIMAGERADIOBTN:
			m_dosboxcdlocationtextCtrl->Enable(m_dosboxcdlocationradioBtn->GetValue());
			m_dosboxcdlocationselectbutton->Enable(m_dosboxcdlocationradioBtn->GetValue());
			m_dosboxcdimagetextCtrl->Enable(m_dosboxcdimageradioBtn->GetValue());
			m_dosboxcdimagelocatebutton->Enable(m_dosboxcdimageradioBtn->GetValue());
			break;
		default: ;
	}
}

//void SRCPortsDialog::OnSelectCustomProfile(wxCommandEvent& WXUNUSED(event))
void SRCPortsDialog::SelectCustomProfile()
{
	int l_loop_var, custom_profile_index_temp;


	custom_profile_index_temp = m_customprofilechoice->GetSelection();

	if (custom_profile_index_temp != wxNOT_FOUND)
	{
		custom_profile_index = custom_profile_index_temp;

		custom_profile_current_temp = custom_profile_list_temp;

		for (l_loop_var = 0; l_loop_var < custom_profile_index; l_loop_var++)
			custom_profile_current_temp = custom_profile_current_temp->next;

		m_customsponlycheckBox->SetValue(custom_profile_current_temp->spgameonly);

		m_customspbintextCtrl->ChangeValue(custom_profile_current_temp->spexecutable);

		m_customsamebincheckBox->SetValue(custom_profile_current_temp->sameexecutable);
		m_customsamebincheckBox->Enable(!custom_profile_current_temp->spgameonly);

		m_custommpbinstaticText->Enable(!custom_profile_current_temp->sameexecutable && !custom_profile_current_temp->spgameonly);
		m_custommpbintextCtrl->ChangeValue(custom_profile_current_temp->mpexecutable);
		m_custommpbintextCtrl->Enable(!custom_profile_current_temp->sameexecutable && !custom_profile_current_temp->spgameonly);
		m_custommpbinlocatebutton->Enable(!custom_profile_current_temp->sameexecutable && !custom_profile_current_temp->spgameonly);

		m_customnocdradioBtn->SetValue((custom_profile_current_temp->cdmount == CDROMMOUNT_NONE));

		bool l_temp_bool = (custom_profile_current_temp->cdmount == CDROMMOUNT_DIR);
		m_customcdlocationradioBtn->SetValue(l_temp_bool);
		m_customcdlocationtextCtrl->ChangeValue(custom_profile_current_temp->cdlocation);
		m_customcdlocationtextCtrl->Enable(l_temp_bool);
		m_customcdlocationselectbutton->Enable(l_temp_bool);

		l_temp_bool = (custom_profile_current_temp->cdmount == CDROMMOUNT_IMG);
		m_customcdimageradioBtn->SetValue(l_temp_bool);
		m_customcdimagetextCtrl->ChangeValue(custom_profile_current_temp->cdimage);
		m_customcdimagetextCtrl->Enable(l_temp_bool);
		m_customcdimagelocatebutton->Enable(l_temp_bool);

		m_customingamesupportcheckBox->SetValue(custom_profile_current_temp->ingamesupport);
		m_customingamesupportcheckBox->Enable(!custom_profile_current_temp->spgameonly);

		m_customnetbioscheckBox->SetValue(custom_profile_current_temp->usenetbios);
		m_customnetbioscheckBox->Enable(!custom_profile_current_temp->spgameonly);
		m_customnetbiostextCtrl->ChangeValue(custom_profile_current_temp->netbiospath);
		m_customnetbiostextCtrl->Enable(custom_profile_current_temp->usenetbios && !custom_profile_current_temp->spgameonly);
		m_customnetbioslocatebutton->Enable(custom_profile_current_temp->usenetbios && !custom_profile_current_temp->spgameonly);

		m_customextraargstextCtrl->ChangeValue(custom_profile_current_temp->extraargs);

		m_customextrahostargsstaticText->Enable(!custom_profile_current_temp->spgameonly);
		m_customextrahostargstextCtrl->ChangeValue(custom_profile_current_temp->extrahostargs);
		m_customextrahostargstextCtrl->Enable(!custom_profile_current_temp->spgameonly);

#if EXTRA_ARGS_FOR_ALL
		m_customextraallargsstaticText->Enable(!custom_profile_current_temp->spgameonly);
		m_customextraallargstextCtrl->ChangeValue(custom_profile_current_temp->extraallargs);
		m_customextraallargstextCtrl->Enable(!custom_profile_current_temp->spgameonly);
#endif
	}
}

void SRCPortsDialog::OnUpdateTextInput(wxCommandEvent& event)
{
	long cursor;

	switch (event.GetId())
	{
		case ID_SELECTCUSTOMPROFILE:
			SelectCustomProfile();

			cursor = m_customprofilechoice->GetInsertionPoint();

			custom_profile_current_temp->profilename = m_customprofilechoice->GetValue();

			m_customprofilechoice->SetString(custom_profile_index, custom_profile_current_temp->profilename);

			m_customprofilechoice->Select(custom_profile_index);

			m_customprofilechoice->SetInsertionPoint(cursor);
			break;

		case ID_SPEXECUTABLEINPUTTEXT:
			custom_profile_current_temp->spexecutable = m_customspbintextCtrl->GetValue();

			if (custom_profile_current_temp->sameexecutable)
			{
				m_custommpbintextCtrl->ChangeValue(custom_profile_current_temp->spexecutable);
				custom_profile_current_temp->mpexecutable = custom_profile_current_temp->spexecutable;
			}
			break;

		case ID_MPEXECUTABLEINPUTTEXT:
			custom_profile_current_temp->mpexecutable = m_custommpbintextCtrl->GetValue();
			break;

		case ID_CDLOCATIONINPUTTEXT:
			custom_profile_current_temp->cdlocation = m_customcdlocationtextCtrl->GetValue();
			break;

		case ID_CDIMAGEINPUTTEXT:
			custom_profile_current_temp->cdimage = m_customcdimagetextCtrl->GetValue();
			break;

		case ID_NETBIOSINPUTTEXT:
			custom_profile_current_temp->netbiospath = m_customnetbiostextCtrl->GetValue();
			break;

		case ID_EXTRAARGSINPUTTEXT:
			custom_profile_current_temp->extraargs = m_customextraargstextCtrl->GetValue();
			break;

		case ID_EXTRAHOSTARGSINPUTTEXT:
			custom_profile_current_temp->extrahostargs = m_customextrahostargstextCtrl->GetValue();
			break;

#if EXTRA_ARGS_FOR_ALL
		case ID_EXTRAALLARGSINPUTTEXT:
			custom_profile_current_temp->extraallargs = m_customextraallargstextCtrl->GetValue();
			break;
#endif
		default: ;
	}
}

void SRCPortsDialog::OnAddNewProfile(wxCommandEvent& WXUNUSED(event))
{
	if (!custom_profile_list_temp)
	{
		custom_profile_list_temp = new custom_profile_t;
		custom_profile_list_temp->next = NULL;

		m_customprofilechoice->Enable();
		m_customdeletebutton->Enable();
		m_customsponlycheckBox->Enable();
		m_customspbinstaticText->Enable();
		m_customspbintextCtrl->Enable();
		m_customspbinlocatebutton->Enable();
		m_customsamebincheckBox->Enable();
		m_customnocdradioBtn->Enable();
		m_customcdlocationradioBtn->Enable();
		m_customcdimageradioBtn->Enable();
		m_customingamesupportcheckBox->Enable();
		m_customnetbioscheckBox->Enable();
		m_customextraargsstaticText->Enable();
		m_customextraargstextCtrl->Enable();
		m_customextrahostargsstaticText->Enable();
		m_customextrahostargstextCtrl->Enable();
#if EXTRA_ARGS_FOR_ALL
		m_customextraallargsstaticText->Enable();
		m_customextraallargstextCtrl->Enable();
#endif
	}
	else
	{
		custom_profile_list_temp->previous = new custom_profile_t;
		custom_profile_list_temp->previous->next = custom_profile_list_temp;
		custom_profile_list_temp = custom_profile_list_temp->previous;

		m_customsponlycheckBox->SetValue(false);
		m_customspbintextCtrl->ChangeValue(wxEmptyString);
		m_customsamebincheckBox->SetValue(true);
		m_customsamebincheckBox->Enable();
		m_custommpbinstaticText->Disable();
		m_custommpbintextCtrl->ChangeValue(wxEmptyString);
		m_custommpbintextCtrl->Disable();
		m_custommpbinlocatebutton->Disable();
		m_customnocdradioBtn->SetValue(true);
		m_customcdlocationtextCtrl->ChangeValue(wxEmptyString);
		m_customcdlocationtextCtrl->Disable();
		m_customcdlocationselectbutton->Disable();
		m_customcdimagetextCtrl->ChangeValue(wxEmptyString);
		m_customcdimagetextCtrl->Disable();
		m_customcdimagelocatebutton->Disable();
		m_customingamesupportcheckBox->SetValue(false);
		m_customingamesupportcheckBox->Enable();
		m_customnetbioscheckBox->SetValue(false);
		m_customnetbioscheckBox->Enable();
		m_customnetbiostextCtrl->ChangeValue(wxEmptyString);
		m_customnetbiostextCtrl->Disable();
		m_customnetbioslocatebutton->Disable();
		m_customextraargstextCtrl->ChangeValue(wxEmptyString);
		m_customextrahostargsstaticText->Enable();
		m_customextrahostargstextCtrl->ChangeValue(wxEmptyString);
		m_customextrahostargstextCtrl->Enable();
#if EXTRA_ARGS_FOR_ALL
		m_customextraallargsstaticText->Enable();
		m_customextraallargstextCtrl->ChangeValue(wxEmptyString);
		m_customextraallargstextCtrl->Enable();
#endif
	}

	custom_profile_list_temp->previous = NULL;

	m_customprofilechoice->Insert(wxT("New profile (rename to the game's name to add)"), 0);

	m_customprofilechoice->Select(0);

	custom_profile_index = 0;

	custom_profile_current_temp = custom_profile_list_temp;

	custom_profile_current_temp->profilename = m_customprofilechoice->GetValue();
	custom_profile_current_temp->spgameonly = false;
	custom_profile_current_temp->sameexecutable = true;
	custom_profile_current_temp->cdmount = CDROMMOUNT_NONE;
	custom_profile_current_temp->ingamesupport = false;
	custom_profile_current_temp->usenetbios = false;

	m_customprofilechoice->SetFocus();
}

void SRCPortsDialog::OnDeleteProfile(wxCommandEvent& WXUNUSED(event))
{
	if (wxMessageBox(wxT("Are you sure you want to delete the \"") + m_customprofilechoice->GetValue() + wxT("\" custom DOS game profile ?\n\nNote that if you press the \"Cancel\" button at the bottom of this \"Source ports\" configuration dialog,\nany change you may have done will be reverted/cancelled, including the added and deleted profiles."), wxT("Confirm custom DOS game profile deletion"), wxYES_NO|wxICON_INFORMATION) == wxYES)
	{
		m_customprofilechoice->Delete(custom_profile_index);

		custom_profile_index = 0;

		if (custom_profile_current_temp->previous)
			custom_profile_current_temp->previous->next = custom_profile_current_temp->next;
		else
			custom_profile_list_temp = custom_profile_list_temp->next;

		if (custom_profile_current_temp->next)
			custom_profile_current_temp->next->previous = custom_profile_current_temp->previous;

		delete custom_profile_current_temp;

		custom_profile_current_temp = custom_profile_list_temp;

		if (!custom_profile_list_temp)
		{
			m_customprofilechoice->Clear();
			m_customprofilechoice->Disable();
			m_customdeletebutton->Disable();
			m_customsponlycheckBox->SetValue(false);
			m_customsponlycheckBox->Disable();
			m_customspbinstaticText->Disable();
			m_customspbintextCtrl->ChangeValue(wxEmptyString);
			m_customspbintextCtrl->Disable();
			m_customspbinlocatebutton->Disable();
			m_customsamebincheckBox->SetValue(true);
			m_customsamebincheckBox->Disable();
			m_custommpbinstaticText->Disable();
			m_custommpbintextCtrl->ChangeValue(wxEmptyString);
			m_custommpbintextCtrl->Disable();
			m_custommpbinlocatebutton->Disable();
			m_customnocdradioBtn->SetValue(true);
			m_customnocdradioBtn->Disable();
			m_customcdlocationradioBtn->Disable();
			m_customcdlocationtextCtrl->ChangeValue(wxEmptyString);
			m_customcdlocationtextCtrl->Disable();
			m_customcdlocationselectbutton->Disable();
			m_customcdimageradioBtn->Disable();
			m_customcdimagetextCtrl->ChangeValue(wxEmptyString);
			m_customcdimagetextCtrl->Disable();
			m_customcdimagelocatebutton->Disable();
			m_customingamesupportcheckBox->SetValue(false);
			m_customingamesupportcheckBox->Disable();
			m_customnetbioscheckBox->SetValue(false);
			m_customnetbioscheckBox->Disable();
			m_customnetbiostextCtrl->ChangeValue(wxEmptyString);
			m_customnetbiostextCtrl->Disable();
			m_customnetbioslocatebutton->Disable();
			m_customextraargsstaticText->Disable();
			m_customextraargstextCtrl->ChangeValue(wxEmptyString);
			m_customextraargstextCtrl->Disable();
			m_customextrahostargsstaticText->Disable();
			m_customextrahostargstextCtrl->ChangeValue(wxEmptyString);
			m_customextrahostargstextCtrl->Disable();
#if EXTRA_ARGS_FOR_ALL
			m_customextraallargsstaticText->Disable();
			m_customextraallargstextCtrl->ChangeValue(wxEmptyString);
			m_customextraallargstextCtrl->Disable();
#endif
		}
		else
		{
			m_customprofilechoice->Select(0);

			m_customsponlycheckBox->SetValue(custom_profile_current_temp->spgameonly);

			m_customspbintextCtrl->ChangeValue(custom_profile_current_temp->spexecutable);

			m_customsamebincheckBox->SetValue(custom_profile_current_temp->sameexecutable);
			m_customsamebincheckBox->Enable(!custom_profile_current_temp->spgameonly);

			m_custommpbinstaticText->Enable(!custom_profile_current_temp->sameexecutable && !custom_profile_current_temp->spgameonly);
			m_custommpbintextCtrl->ChangeValue(custom_profile_current_temp->mpexecutable);
			m_custommpbintextCtrl->Enable(!custom_profile_current_temp->sameexecutable && !custom_profile_current_temp->spgameonly);
			m_custommpbinlocatebutton->Enable(!custom_profile_current_temp->sameexecutable && !custom_profile_current_temp->spgameonly);

			m_customnocdradioBtn->SetValue((custom_profile_current_temp->cdmount == CDROMMOUNT_NONE));

			bool l_temp_bool = (custom_profile_current_temp->cdmount == CDROMMOUNT_DIR);
			m_customcdlocationradioBtn->SetValue(l_temp_bool);
			m_customcdlocationtextCtrl->ChangeValue(custom_profile_current_temp->cdlocation);
			m_customcdlocationtextCtrl->Enable(l_temp_bool);
			m_customcdlocationselectbutton->Enable(l_temp_bool);

			l_temp_bool = (custom_profile_current_temp->cdmount == CDROMMOUNT_IMG);
			m_customcdimageradioBtn->SetValue(l_temp_bool);
			m_customcdimagetextCtrl->ChangeValue(custom_profile_current_temp->cdimage);
			m_customcdimagetextCtrl->Enable(l_temp_bool);
			m_customcdimagelocatebutton->Enable(l_temp_bool);

			m_customingamesupportcheckBox->SetValue(custom_profile_current_temp->ingamesupport);
			m_customingamesupportcheckBox->Enable(!custom_profile_current_temp->spgameonly);

			m_customnetbioscheckBox->SetValue(custom_profile_current_temp->usenetbios);
			m_customnetbioscheckBox->Enable(!custom_profile_current_temp->spgameonly);
			m_customnetbiostextCtrl->ChangeValue(custom_profile_current_temp->netbiospath);
			m_customnetbiostextCtrl->Enable(custom_profile_current_temp->usenetbios && !custom_profile_current_temp->spgameonly);
			m_customnetbioslocatebutton->Enable(custom_profile_current_temp->usenetbios && !custom_profile_current_temp->spgameonly);

			m_customextraargstextCtrl->ChangeValue(custom_profile_current_temp->extraargs);

			m_customextrahostargsstaticText->Enable(!custom_profile_current_temp->spgameonly);
			m_customextrahostargstextCtrl->ChangeValue(custom_profile_current_temp->extrahostargs);
			m_customextrahostargstextCtrl->Enable(!custom_profile_current_temp->spgameonly);

#if EXTRA_ARGS_FOR_ALL
			m_customextraallargsstaticText->Enable(!custom_profile_current_temp->spgameonly);
			m_customextraallargstextCtrl->ChangeValue(custom_profile_current_temp->extraallargs);
			m_customextraallargstextCtrl->Enable(!custom_profile_current_temp->spgameonly);
#endif
		}
	}
}
/*
void SRCPortsDialog::OnApply(wxCommandEvent& WXUNUSED(event))
{
ApplySettings();
}
*/
void SRCPortsDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	if (ApplySettings())
		Destroy();
}

void SRCPortsDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	if (custom_profile_list_temp)
	{
		while (custom_profile_list_temp->next)
		{
			custom_profile_current_temp = custom_profile_list_temp->next;

			while (custom_profile_current_temp->next)
				custom_profile_current_temp = custom_profile_current_temp->next;

			custom_profile_current_temp->previous->next = NULL;

			delete custom_profile_current_temp;
		}

		delete custom_profile_list_temp;
	}

	Destroy();
}

bool SRCPortsDialog::ApplySettings()
{
	wxFile file;
	custom_profile_t *custom_profile_list_comp, *custom_profile_current_comp;

	if (custom_profile_list_temp)
	{
		custom_profile_current_temp = custom_profile_list_temp;

		while (custom_profile_current_temp)
		{
			custom_profile_current_temp->profilename.Trim(false);

			if (custom_profile_current_temp->profilename.IsEmpty())
			{
				wxMessageBox(wxT("You have set one or many custom DOS game profile(s) to an empty name."), wxT("Custom DOS game profile(s) with empty name"), wxOK|wxICON_EXCLAMATION);

				return false;
			}

			custom_profile_current_temp = custom_profile_current_temp->next;
		}
	}

	if (custom_profile_list_temp)
	{
		if (custom_profile_list_temp->next)
		{
			custom_profile_list_comp = custom_profile_list_temp;

			while (custom_profile_list_comp)
			{
				custom_profile_current_comp = custom_profile_list_comp->next;

				while (custom_profile_current_comp)
				{
					if (custom_profile_list_comp->profilename.IsSameAs(custom_profile_current_comp->profilename, false))
					{
						m_srcportsnotebook->ChangeSelection(GAME_CUSTOM);

						wxMessageBox(wxT("You have named many different custom DOS game profiles to \"") + custom_profile_list_comp->profilename + wxT("\"\n\nPlease use an unique name for each of the different custom DOS game profiles."), wxT("Different custom DOS game profiles with same name"), wxOK|wxICON_EXCLAMATION);

						return false;
					}

					custom_profile_current_comp = custom_profile_current_comp->next;
				}

				custom_profile_list_comp = custom_profile_list_comp->next;
			}
		}
	}

	if (m_dosbloodswavailcheckBox->GetValue() && !g_CheckPortCRC32(SOURCEPORT_DOSBLOODSW, m_dosbloodswbintextCtrl->GetValue()))
		return false;
	if (m_dosbloodrgavailcheckBox->GetValue() && !g_CheckPortCRC32(SOURCEPORT_DOSBLOODRG, m_dosbloodrgbintextCtrl->GetValue()))
		return false;
	if (m_dosbloodppavailcheckBox->GetValue() && !g_CheckPortCRC32(SOURCEPORT_DOSBLOODPP, m_dosbloodppbintextCtrl->GetValue()))
		return false;
	if (m_dosbloodouavailcheckBox->GetValue() && !g_CheckPortCRC32(SOURCEPORT_DOSBLOODOU, m_dosbloodoubintextCtrl->GetValue()))
		return false;
	if (m_dosdukeswavailcheckBox->GetValue() && !g_CheckPortCRC32(SOURCEPORT_DOSDUKESW, m_dosdukeswbintextCtrl->GetValue()))
		return false;
	if (m_dosdukergavailcheckBox->GetValue() && !g_CheckPortCRC32(SOURCEPORT_DOSDUKERG, m_dosdukergbintextCtrl->GetValue()))
		return false;
	if (m_dosdukeaeavailcheckBox->GetValue() && !g_CheckPortCRC32(SOURCEPORT_DOSDUKEAE, m_dosdukeaebintextCtrl->GetValue()))
		return false;
	if (m_dosswswavailcheckBox->GetValue() && !g_CheckPortCRC32(SOURCEPORT_DOSSWSW, m_dosswswbintextCtrl->GetValue()))
		return false;
	if (m_dosswrgavailcheckBox->GetValue() && !g_CheckPortCRC32(SOURCEPORT_DOSSWRG, m_dosswrgbintextCtrl->GetValue()))
		return false;

	if (m_dosboxbmousecheckBox->GetValue() && (!file.Access(m_dosboxbmousetextCtrl->GetValue(), wxFile::read) || !file.Open(m_dosboxbmousetextCtrl->GetValue(), wxFile::read)))
	{
		wxMessageBox(wxT("Invalid pathname/filename set for bMouse"), wxT("Invalid pathname/filename detected"), wxOK|wxICON_ERROR);
		return false;
	}

	g_configuration->have_dosbloodsw = m_dosbloodswavailcheckBox->GetValue();
	g_configuration->dosbloodsw_exec = m_dosbloodswbintextCtrl->GetValue();
	g_configuration->have_dosbloodrg = m_dosbloodrgavailcheckBox->GetValue();
	g_configuration->dosbloodrg_exec = m_dosbloodrgbintextCtrl->GetValue();
	g_configuration->have_dosbloodpp = m_dosbloodppavailcheckBox->GetValue();
	g_configuration->dosbloodpp_exec = m_dosbloodppbintextCtrl->GetValue();
	g_configuration->have_dosbloodou = m_dosbloodouavailcheckBox->GetValue();
	g_configuration->dosbloodou_exec = m_dosbloodoubintextCtrl->GetValue();
	g_configuration->have_dosdescent = m_dosdescentavailcheckBox->GetValue();
	g_configuration->dosdescent_exec = m_dosdescentbintextCtrl->GetValue();
	g_configuration->have_dosdescent2 = m_dosdescent2availcheckBox->GetValue();
	g_configuration->dosdescent2_exec = m_dosdescent2bintextCtrl->GetValue();
	g_configuration->have_dosdukesw = m_dosdukeswavailcheckBox->GetValue();
	g_configuration->dosdukesw_exec = m_dosdukeswbintextCtrl->GetValue();
	g_configuration->have_dosdukerg = m_dosdukergavailcheckBox->GetValue();
	g_configuration->dosdukerg_exec = m_dosdukergbintextCtrl->GetValue();
	g_configuration->have_dosdukeae = m_dosdukeaeavailcheckBox->GetValue();
	g_configuration->dosdukeae_exec = m_dosdukeaebintextCtrl->GetValue();
	g_configuration->have_dosswsw = m_dosswswavailcheckBox->GetValue();
	g_configuration->dosswsw_exec = m_dosswswbintextCtrl->GetValue();
	g_configuration->have_dosswrg = m_dosswrgavailcheckBox->GetValue();
	g_configuration->dosswrg_exec = m_dosswrgbintextCtrl->GetValue();

	g_configuration->blood_maps_dir = m_bloodmapstextCtrl->GetValue();

	g_configuration->have_d1xrebirth = m_d1xrebirthavailcheckBox->GetValue();
	g_configuration->d1xrebirth_exec = m_d1xrebirthbintextCtrl->GetValue();
	g_configuration->descent_maps_dir = m_descentmapstextCtrl->GetValue();

	g_configuration->have_d2xrebirth = m_d2xrebirthavailcheckBox->GetValue();
	g_configuration->d2xrebirth_exec = m_d2xrebirthbintextCtrl->GetValue();
	g_configuration->descent2_maps_dir = m_descent2mapstextCtrl->GetValue();

	g_configuration->have_duke3dw = m_duke3dwavailcheckBox->GetValue();
	g_configuration->duke3dw_exec = m_duke3dwbintextCtrl->GetValue();
	g_configuration->have_eduke32 = m_eduke32availcheckBox->GetValue();
	g_configuration->eduke32_exec = m_eduke32bintextCtrl->GetValue();
	g_configuration->have_nduke = m_ndukeavailcheckBox->GetValue();
	g_configuration->nduke_exec = m_ndukebintextCtrl->GetValue();
	g_configuration->have_hduke = m_hdukeavailcheckBox->GetValue();
	g_configuration->hduke_exec = m_hdukebintextCtrl->GetValue();
	g_configuration->have_xduke = m_xdukeavailcheckBox->GetValue();
	g_configuration->xduke_exec = m_xdukebintextCtrl->GetValue();
	g_configuration->dn3d_maps_dir = m_duke3dmapstextCtrl->GetValue();

	g_configuration->have_voidsw = m_voidswavailcheckBox->GetValue();
	g_configuration->voidsw_exec = m_voidswbintextCtrl->GetValue();
	g_configuration->have_swp = m_swpavailcheckBox->GetValue();
	g_configuration->swp_exec = m_swpbintextCtrl->GetValue();
	g_configuration->sw_maps_dir = m_swmapstextCtrl->GetValue();

	g_configuration->have_dosbox = m_dosboxavailcheckBox->GetValue();
	g_configuration->dosbox_exec = m_dosboxbintextCtrl->GetValue();
	g_configuration->dosbox_use_conf = m_dosboxconfcheckBox->GetValue();
	g_configuration->dosbox_conf = m_dosboxconftextCtrl->GetValue();
	if (m_dosboxnocdradioBtn->GetValue())
		g_configuration->dosbox_cdmount = CDROMMOUNT_NONE;
	else if (m_dosboxcdlocationradioBtn->GetValue())
		g_configuration->dosbox_cdmount = CDROMMOUNT_DIR;
	else
		g_configuration->dosbox_cdmount = CDROMMOUNT_IMG;
	g_configuration->dosbox_cdrom_location = m_dosboxcdlocationtextCtrl->GetValue();
	g_configuration->dosbox_cdrom_image = m_dosboxcdimagetextCtrl->GetValue();

	g_configuration->have_bmouse = m_dosboxbmousecheckBox->GetValue();
	g_configuration->bmouse_exec = m_dosboxbmousetextCtrl->GetValue();

#ifndef __WXMSW__
	g_configuration->have_wine = m_usewinecheckBox->GetValue();
	g_configuration->wine_exec = m_winebintextCtrl->GetValue();
#endif

	if (g_configuration->custom_profile_list)
	{
		while (g_configuration->custom_profile_list->next)
		{
			g_configuration->custom_profile_current = g_configuration->custom_profile_list->next;

			while (g_configuration->custom_profile_current->next)
				g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;

			g_configuration->custom_profile_current->previous->next = NULL;

			delete g_configuration->custom_profile_current;
		}

		delete g_configuration->custom_profile_list;
	}

	g_configuration->custom_profile_list = NULL;

	if (custom_profile_list_temp)
	{
		if (custom_profile_list_temp->next)
		{
			while (custom_profile_list_temp)
			{
				custom_profile_list_comp = custom_profile_list_temp;

				custom_profile_current_comp = custom_profile_list_temp->next;

				while (custom_profile_current_comp)
				{
					if (custom_profile_list_comp->profilename.CmpNoCase(custom_profile_current_comp->profilename) > 0)
						custom_profile_list_comp = custom_profile_current_comp;

					custom_profile_current_comp = custom_profile_current_comp->next;
				}

				if (custom_profile_list_comp->previous)
					custom_profile_list_comp->previous->next = custom_profile_list_comp->next;
				else
					custom_profile_list_temp = custom_profile_list_temp->next;

				if (custom_profile_list_comp->next)
					custom_profile_list_comp->next->previous = custom_profile_list_comp->previous;

				if (!g_configuration->custom_profile_list)
				{
					g_configuration->custom_profile_list = custom_profile_list_comp;
					g_configuration->custom_profile_list->previous = NULL;
				}
				else
				{
					g_configuration->custom_profile_list->next = custom_profile_list_comp;
					g_configuration->custom_profile_list->next->previous = g_configuration->custom_profile_list;
					g_configuration->custom_profile_list = g_configuration->custom_profile_list->next;
					g_configuration->custom_profile_list->next = NULL;
				}
			}

			while (g_configuration->custom_profile_list->previous)
				g_configuration->custom_profile_list = g_configuration->custom_profile_list->previous;
		}
		else
			g_configuration->custom_profile_list = custom_profile_list_temp;
	}

	g_configuration->Save();

	return true;
}

