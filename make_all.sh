#!/bin/sh
if [ "`whoami`" != "root" ]; then
  echo "You must run this script using fakeroot, or as a real root."
  exit 0
fi
if [ ! "$1" ]; then
  echo "PLEASE PROVIDE A VERSION NUMBER LIKE 0.15!"
  exit 0
fi
#if [ ! -e "yang_win.exe" ]; then
#  echo "PLEASE MAKE A yang_win.exe FILE READY!"
#  exit 0
#fi
if [ ! -d "msvc_old" ]; then
  echo "PLEASE MAKE A READY msvc_old DIRECTORY!"
  exit 0
fi

# VERSION NUMBER!
echo YANGVer:$1: > yangver.txt

mkdir -p yang-$1-win32 yang-$1-linux-i386-oss yang-$1-linux-x86_64-oss \
         yang-$1-linux-i386-sdl yang-$1-linux-x86_64-sdl yang-$1-src/obj
#make clean
./make_mingw.sh clean
./make_mingw.sh client
#cp -R snddata deflists.cfg readme.txt gpl-2.0.txt yang.exe mingwm10.dll opencow.dll yang-$1-win32
#cp -R snddata deflists.cfg readme.txt gpl-2.0.txt GeoIP.dat yang.exe yang-$1-win32
#cp yang_win.exe yang.exe
upx yang.exe
cp -R snddata deflists.cfg readme.txt license.txt GeoIP.dat GeoIP/win32/GeoIP.dll mingwm10.dll yang.exe yang-$1-win32
#unix2dos yang-$1-win32/readme.txt yang-$1-win32/gpl-2.0.txt
unix2dos yang-$1-win32/readme.txt yang-$1-win32/license.txt

cd yang-$1-win32
cp ../yang.nsi .
makensis yang.nsi
mv setup.exe ../yang-$1-win32.exe
rm yang.nsi
cd ..

./make_mingw.sh clean
mkdir -p buildroot/usr/bin
mkdir -p buildroot/usr/share/yang
mkdir -p buildroot/usr/share/applications
mkdir -p buildroot/usr/share/icons
mkdir -p buildroot/DEBIAN 
#cp -R snddata deflists.cfg readme.txt gpl-2.0.txt GeoIP.dat buildroot/usr/share/yang
cp -R snddata deflists.cfg readme.txt license.txt GeoIP.dat buildroot/usr/share/yang
cp rsrc/yang.desktop buildroot/usr/share/applications
cp rsrc/yang.png buildroot/usr/share/icons
chown -R root:root buildroot

dchroot -d "make client"
upx yang
cp yang buildroot/usr/bin
sed "s/\${VER}/$1/g;s/\${ARCH}/i386/g" deb-gen-control > buildroot/DEBIAN/control
dpkg-deb -b buildroot yang_$1_i386.deb
make clean

make client
upx yang
cp yang buildroot/usr/bin
sed "s/\${VER}/$1/g;s/\${ARCH}/amd64/g" deb-gen-control > buildroot/DEBIAN/control
dpkg-deb -b buildroot yang_$1_amd64.deb
make clean

PATH=~/wxGTK-static-oss-32/bin:$PATH dchroot -d "make client"
upx yang
#cp -R snddata deflists.cfg readme.txt gpl-2.0.txt GeoIP.dat yang yang-$1-linux-i386-oss
cp -R snddata deflists.cfg readme.txt license.txt GeoIP.dat GeoIP/lib32/libGeoIP.so.1.4.6 run_yang yang yang-$1-linux-i386-oss
cd yang-$1-linux-i386-oss
ln -s libGeoIP.so.1.4.6 libGeoIP.so.1
ln -s libGeoIP.so.1 libGeoIP.so
cd ..
make clean
PATH=~/wxGTK-static-oss-64/bin:$PATH make client
upx yang
#cp -R snddata deflists.cfg readme.txt gpl-2.0.txt GeoIP.dat yang yang-$1-linux-x86_64-oss
cp -R snddata deflists.cfg readme.txt license.txt GeoIP.dat GeoIP/lib64/libGeoIP.so.1.4.6 run_yang yang yang-$1-linux-x86_64-oss
cd yang-$1-linux-x86_64-oss
ln -s libGeoIP.so.1.4.6 libGeoIP.so.1
ln -s libGeoIP.so.1 libGeoIP.so
cd ..
make clean
PATH=~/wxGTK-static-sdl-32/bin:$PATH dchroot -d "make client"
upx yang
#cp -R snddata deflists.cfg readme.txt gpl-2.0.txt GeoIP.dat yang yang-$1-linux-i386-sdl
cp -R snddata deflists.cfg readme.txt license.txt GeoIP.dat GeoIP/lib32/libGeoIP.so.1.4.6 run_yang yang yang-$1-linux-i386-sdl
cd yang-$1-linux-i386-sdl
ln -s libGeoIP.so.1.4.6 libGeoIP.so.1
ln -s libGeoIP.so.1 libGeoIP.so
cd ..
make clean
PATH=~/wxGTK-static-sdl-64/bin:$PATH make client
upx yang
#cp -R snddata deflists.cfg readme.txt gpl-2.0.txt GeoIP.dat yang yang-$1-linux-x86_64-sdl
cp -R snddata deflists.cfg readme.txt license.txt GeoIP.dat GeoIP/lib64/libGeoIP.so.1.4.6 run_yang yang yang-$1-linux-x86_64-sdl
cd yang-$1-linux-x86_64-sdl
ln -s libGeoIP.so.1.4.6 libGeoIP.so.1
ln -s libGeoIP.so.1 libGeoIP.so
cd ..
make clean
#cp -R snddata deflists.cfg readme.txt gpl-2.0.txt GeoIP.dat master_sample.cfg master.txt compiling packaging.txt Makefile msvc src rsrc yang.fbp yang-$1-src
cp -R snddata deflists.cfg readme.txt license.txt GeoIP.dat master_sample.cfg master.txt compiling packaging.txt Makefile msvc src rsrc yang.fbp yang-$1-src

zip -9r yang-$1-win32.zip yang-$1-win32
tar -cvf yang-$1-linux-i386-oss.tar yang-$1-linux-i386-oss
bzip2 -9v yang-$1-linux-i386-oss.tar
tar -cvf yang-$1-linux-x86_64-oss.tar yang-$1-linux-x86_64-oss
bzip2 -9v yang-$1-linux-x86_64-oss.tar
tar -cvf yang-$1-linux-i386-sdl.tar yang-$1-linux-i386-sdl
bzip2 -9v yang-$1-linux-i386-sdl.tar
tar -cvf yang-$1-linux-x86_64-sdl.tar yang-$1-linux-x86_64-sdl
bzip2 -9v yang-$1-linux-x86_64-sdl.tar
tar -cvf yang-$1-src.tar yang-$1-src
bzip2 -9v yang-$1-src.tar
