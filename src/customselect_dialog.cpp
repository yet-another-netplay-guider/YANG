/**************************************************************************

Copyright 2011-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/dialog.h>
#endif

#include "customselect_dialog.h"
#include "yang.h"
#include "yang.xpm"

BEGIN_EVENT_TABLE(CustomSelectDialog, YANGDialog)

EVT_BUTTON(wxID_OK, CustomSelectDialog::OnOk)
EVT_BUTTON(wxID_CANCEL, CustomSelectDialog::OnCancel)

EVT_CHOICE(ID_CUSTOMSELECTPROFILE, CustomSelectDialog::OnSelectCustomProfile)

END_EVENT_TABLE()

CustomSelectDialog::CustomSelectDialog( wxWindow* parent, const wxString& roomname ) : YANGDialog( parent, wxID_ANY, wxT("Select a custom DOS game profile"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE )
{
	wxBoxSizer* CustomSelectDialogbSizer = new wxBoxSizer( wxVERTICAL );

	m_CustomSelectpanel = new YANGPanel( this , wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* CustomSelectpanelbSizer = new wxBoxSizer( wxVERTICAL );

	wxGridBagSizer* CustomSelectpanelgbSizer = new wxGridBagSizer( 0, 0 );
	CustomSelectpanelgbSizer->SetFlexibleDirection( wxBOTH );
	CustomSelectpanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_CustomSelectroomnamestaticText = new wxStaticText( m_CustomSelectpanel, wxID_ANY, wxT("Select one of your configured custom DOS game profiles,\nmatching the DOS game that the host has selected,\nwhich should be indicated in his gameroom name:\n\n") + roomname, wxDefaultPosition, wxDefaultSize, 0 );
	CustomSelectpanelgbSizer->Add( m_CustomSelectroomnamestaticText, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_CustomSelectprofilestaticText = new wxStaticText( m_CustomSelectpanel, wxID_ANY, wxT("Profile:"), wxDefaultPosition, wxDefaultSize, 0 );
	CustomSelectpanelgbSizer->Add( m_CustomSelectprofilestaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_CustomSelectprofilechoice = new YANGChoice( m_CustomSelectpanel, ID_CUSTOMSELECTPROFILE, wxDefaultPosition, wxDLG_UNIT(this, wxSize(160, -1)));
	CustomSelectpanelgbSizer->Add( m_CustomSelectprofilechoice, wxGBPosition( 1, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_CustomSelectsummarystaticText = new wxStaticText( m_CustomSelectpanel, wxID_ANY, wxT("Summary:"), wxDefaultPosition, wxDefaultSize, 0 );
	CustomSelectpanelgbSizer->Add( m_CustomSelectsummarystaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_CustomSelectsummarytextCtrl = new YANGTextCtrl( m_CustomSelectpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(-1, 140)), wxTE_MULTILINE|wxTE_READONLY );
	CustomSelectpanelgbSizer->Add( m_CustomSelectsummarytextCtrl, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	CustomSelectpanelbSizer->Add( CustomSelectpanelgbSizer, 1, wxEXPAND, 0 );

	m_CustomSelectsdbSizer = new wxStdDialogButtonSizer();
	m_CustomSelectsdbSizerOK = new YANGButton( m_CustomSelectpanel, wxID_OK );
	m_CustomSelectsdbSizer->AddButton( m_CustomSelectsdbSizerOK );
	m_CustomSelectsdbSizerCancel = new YANGButton( m_CustomSelectpanel, wxID_CANCEL );
	m_CustomSelectsdbSizer->AddButton( m_CustomSelectsdbSizerCancel );
	m_CustomSelectsdbSizer->Realize();
	CustomSelectpanelbSizer->Add( m_CustomSelectsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxBOTTOM, 5 );

	m_CustomSelectpanel->SetSizer( CustomSelectpanelbSizer );
	m_CustomSelectpanel->Layout();
	CustomSelectpanelbSizer->Fit( m_CustomSelectpanel );
	CustomSelectDialogbSizer->Add( m_CustomSelectpanel, 1, wxEXPAND | wxALL, 0 );

	SetSizer( CustomSelectDialogbSizer );
	Layout();
	CustomSelectDialogbSizer->Fit( this );

	Centre();
	SetIcon(wxIcon(yang_xpm));


	g_configuration->custom_profile_current = g_configuration->custom_profile_list;

	while (g_configuration->custom_profile_current)
	{
		if (!g_configuration->custom_profile_current->spgameonly)
			m_CustomSelectprofilechoice->Append(g_configuration->custom_profile_current->profilename);

		g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
	}

	m_CustomSelectprofilechoice->SetStringSelection(g_configuration->gamelaunch_customselect_profilename);

	if (m_CustomSelectprofilechoice->GetSelection() == wxNOT_FOUND)
	{
		m_CustomSelectprofilechoice->SetSelection(0);

		g_configuration->custom_profile_current = g_configuration->custom_profile_list;

		while (g_configuration->custom_profile_current->spgameonly)
			g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
	}
	else
	{
		g_configuration->custom_profile_current = g_configuration->custom_profile_list;

		while (g_configuration->custom_profile_current->profilename != g_configuration->gamelaunch_customselect_profilename)
			g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
	}

	UpdateSummaryList();
}

void CustomSelectDialog::OnSelectCustomProfile(wxCommandEvent& WXUNUSED(event))
{
	wxString l_temp_str = m_CustomSelectprofilechoice->GetStringSelection();


	g_configuration->custom_profile_current = g_configuration->custom_profile_list;

	while (g_configuration->custom_profile_current->profilename != l_temp_str)
		g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;

	UpdateSummaryList();
}

void CustomSelectDialog::UpdateSummaryList()
{
	m_CustomSelectsummarytextCtrl->Clear();

	m_CustomSelectsummarytextCtrl->AppendText(wxT("Executable :\n"));
	m_CustomSelectsummarytextCtrl->AppendText(g_configuration->custom_profile_current->mpexecutable);

	if (g_configuration->custom_profile_current->cdmount == CDROMMOUNT_DIR)
	{
		m_CustomSelectsummarytextCtrl->AppendText(wxT("\n\nCD-ROM location :\n"));
		m_CustomSelectsummarytextCtrl->AppendText(g_configuration->custom_profile_current->cdlocation);
	}
	else if (g_configuration->custom_profile_current->cdmount == CDROMMOUNT_IMG)
	{
		m_CustomSelectsummarytextCtrl->AppendText(wxT("\n\nCD-ROM image/block device :\n"));
		m_CustomSelectsummarytextCtrl->AppendText(g_configuration->custom_profile_current->cdimage);
	}

	if (g_configuration->custom_profile_current->ingamesupport)
		m_CustomSelectsummarytextCtrl->AppendText(wxT("\n\nSupports in-game joining multiplayer mode."));

	if (g_configuration->custom_profile_current->usenetbios)
	{
		m_CustomSelectsummarytextCtrl->AppendText(wxT("\n\nRequires NetBIOS for multiplayer mode :\n"));
		m_CustomSelectsummarytextCtrl->AppendText(g_configuration->custom_profile_current->netbiospath);
	}

	if (!g_configuration->custom_profile_current->extrahostargs.IsEmpty())
	{
		m_CustomSelectsummarytextCtrl->AppendText(wxT("\n\nExtra arguments for host only :\n"));
		m_CustomSelectsummarytextCtrl->AppendText(g_configuration->custom_profile_current->extrahostargs);
	}

#if EXTRA_ARGS_FOR_ALL
	if (!g_configuration->custom_profile_current->extraallargs.IsEmpty())
	{
		m_CustomSelectsummarytextCtrl->AppendText(wxT("\n\nExtra arguments for everyone :\n"));
		m_CustomSelectsummarytextCtrl->AppendText(g_configuration->custom_profile_current->extraallargs);
	}
#endif
}

void CustomSelectDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	g_configuration->gamelaunch_customselect_profilename = m_CustomSelectprofilechoice->GetStringSelection();

	g_configuration->Save();

	customprofile_selected = true;

	EndModal(0);
}

void CustomSelectDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	customprofile_selected = false;

	EndModal(0);
}
