/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_MPCOMMON_H_
#define _YANG_MPCOMMON_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#include <wx/stopwatch.h>
#endif

#include "common.h"
#include "config.h"
#include "file_transfer.h"

#define MAX_NUM_MSG_LINES 512
#define MAX_NUM_CONNECTED_CLIENTS 64

typedef struct
{
	wxString nickname, ingameip;
	long game_port_number;
	wxString os_description;
	bool ready;
} RoomPlayerData;

class RoomPlayersTable
{
public:
	RoomPlayerData players[MAX_NUM_PLAYERS];
	size_t num_players;

	RoomPlayersTable();
	bool Add(const wxString& nickname,
		const wxString& ingameip, long game_port_number);
	bool DelByIndex(size_t index);
	size_t FindIndexByNick(const wxString& nickname);
};

typedef struct
{
	TransferRequestType transferrequest;
	wxString filename;
	long filesize;
	bool awaiting_download;
} TransferRequestData;

class ExtendedRoomPlayersTable : public RoomPlayersTable
{
public:
	TransferRequestData requests[MAX_NUM_PLAYERS];

	ExtendedRoomPlayersTable();
	bool Add(const wxString& nickname,
		const wxString& ingameip, long game_port_number,
		const wxString& os_description, bool ready);
	bool DelByIndex(size_t index);
};

/*
typedef struct
{
wxString nickname, ingameip, filename;
long game_port_number, filesize;
TransferRequestType transferrequest;
} ExtendedRoomPlayerData;

class ExtendedRoomPlayersTable
{
public:
ExtendedRoomPlayerData players[MAX_NUM_PLAYERS];
size_t num_players;

ExtendedRoomPlayersTable();
bool Add(const wxString& nickname,
const wxString& ingameip, long game_port_number);
bool DelByIndex(size_t index);
size_t FindIndexByNick(const wxString& nickname);
};
*/
typedef struct
{
	wxSocketBase* sock;
	wxString peerdetectedip, selfdetectedip, nickname;
	wxStopWatch* stopwatch;
} HostRoomClientData;

class HostRoomClientsTable
{
public:
	HostRoomClientData clients[MAX_NUM_CONNECTED_CLIENTS+1]; // One more for file-transfer.
	size_t num_clients;

	HostRoomClientsTable();
	bool Add(wxSocketBase* sock,
		const wxString& peerdetectedip,//const wxString& selfdetectedip
		wxStopWatch* stopwatch);
	bool PutInRoom(size_t index, size_t inroom_num_clients);
	bool DelBySock(wxSocketBase* sock);
	bool DelByIndex(size_t index);
	size_t FindIndexBySock(wxSocketBase* sock);
};

#endif
