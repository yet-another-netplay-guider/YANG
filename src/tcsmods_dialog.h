/**************************************************************************

Copyright 2011-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_TCSMODSDIALOG_H_
#define _YANG_TCSMODSDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/statline.h>
#endif

#include "config.h"
#include "theme.h"

#define ID_SELECTDN3DMOD 1000
#define ID_ADDNEWDN3DMOD 1001
#define ID_DELETEDN3DMOD 1002
#define ID_ADDDN3DMODFILE 1003
#define ID_REMDN3DMODFILE 1004
#define ID_SETDN3DCONFILE 1005
#define ID_SETDN3DDEFFILE 1006
#define ID_USEDN3DCONFILE 1007
#define ID_DN3DCONFILETEXT 1008
#define ID_USEDN3DDEFFILE 1009
#define ID_DN3DDEFFILETEXT 1010
#define ID_USEDN3DMODURL 1011
#define ID_DN3DMODURLTEXT 1012

#define ID_SELECTSWMOD 1013
#define ID_ADDNEWSWMOD 1014
#define ID_DELETESWMOD 1015
#define ID_ADDSWMODFILE 1016
#define ID_REMSWMODFILE 1017
#define ID_SETSWDEFFILE 1018
#define ID_USESWDEFFILE 1019
#define ID_SWDEFFILETEXT 1020
#define ID_USESWMODURL 1021
#define ID_SWMODURLTEXT 1022

class TCsMODsDialog : public YANGDialog
{
public:
	TCsMODsDialog();
	~TCsMODsDialog();
	void OnAddModFile(wxCommandEvent& event);
	void OnRemModFile(wxCommandEvent& event);
	void OnSetConFile(wxCommandEvent& event);
	void OnSetDefFile(wxCommandEvent& event);
	void OnCheckBoxClick(wxCommandEvent& event);
	void SelectModProfile(GameType game);
	void OnUpdateTextInput(wxCommandEvent& event);
	void OnAddNewProfile(wxCommandEvent& event);
	void OnDeleteProfile(wxCommandEvent& event);

	void OnCancel(wxCommandEvent& event);
	void OnOk(wxCommandEvent& event);
	bool ApplySettings();

private:
	YANGNotebook* m_tcsmodsnotebook;

	YANGPanel* m_tcsmodsdn3dpanel;
	wxStaticText* m_tcsmodsdn3dprofilestaticText;
	YANGComboBox* m_tcsmodsdn3dprofilechoice;
	YANGBitmapButton* m_tcsmodsdn3daddnewbutton;
	YANGBitmapButton* m_tcsmodsdn3ddeletebutton;
	wxStaticLine* m_tcsmodsdn3dstaticline;
	wxStaticText* m_tcsmodsdn3dmodfilesstaticText;
	YANGListBox* m_tcsmodsdn3dmodfileslistBox;
	YANGButton* m_tcsmodsdn3daddmodfilebutton;
	YANGButton* m_tcsmodsdn3dremmodfilebutton;
	YANGButton* m_tcsmodsdn3dconfilebutton;
	YANGButton* m_tcsmodsdn3ddeffilebutton;
	YANGCheckBox* m_tcsmodsdn3dconfilecheckBox;
	YANGTextCtrl* m_tcsmodsdn3dconfiletextCtrl;
	YANGCheckBox* m_tcsmodsdn3ddeffilecheckBox;
	YANGTextCtrl* m_tcsmodsdn3ddeffiletextCtrl;
	YANGCheckBox* m_tcsmodsdn3dmodurlcheckBox;
	YANGTextCtrl* m_tcsmodsdn3dmodurltextCtrl;

	YANGPanel* m_tcsmodsswpanel;
	wxStaticText* m_tcsmodsswprofilestaticText;
	YANGComboBox* m_tcsmodsswprofilechoice;
	YANGBitmapButton* m_tcsmodsswaddnewbutton;
	YANGBitmapButton* m_tcsmodsswdeletebutton;
	wxStaticLine* m_tcsmodsswstaticline;
	wxStaticText* m_tcsmodsswmodfilesstaticText;
	YANGListBox* m_tcsmodsswmodfileslistBox;
	YANGButton* m_tcsmodsswaddmodfilebutton;
	YANGButton* m_tcsmodsswremmodfilebutton;
	YANGButton* m_tcsmodsswdeffilebutton;
	YANGCheckBox* m_tcsmodsswdeffilecheckBox;
	YANGTextCtrl* m_tcsmodsswdeffiletextCtrl;
	YANGCheckBox* m_tcsmodsswmodurlcheckBox;
	YANGTextCtrl* m_tcsmodsswmodurltextCtrl;

	wxStdDialogButtonSizer* m_tcsmodssdbSizer;
	YANGButton* m_tcsmodssdbSizerOK;
	YANGButton* m_tcsmodssdbSizerCancel;

	wxString last_directory;

	int dn3dtcmod_profile_index, swtcmod_profile_index;

	dn3dtcmod_profile_t *dn3dtcmod_profile_list_temp, *dn3dtcmod_profile_current_temp;
	swtcmod_profile_t *swtcmod_profile_list_temp, *swtcmod_profile_current_temp;

	DECLARE_EVENT_TABLE()
};

#endif

