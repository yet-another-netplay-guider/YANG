/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/filename.h>
#include <wx/log.h>
#include <wx/msgdlg.h>
#include <wx/textdlg.h>
#include <wx/sizer.h>
#include <wx/socket.h>
#include <wx/mstream.h>
#include <wx/image.h>
#endif

#include <string.h>
#include "client_frame.h"
#include "config.h"
#include "mapselection_dialog.h"
#include "customselect_dialog.h"
#include "tcmodselect_dialog.h"
#include "comm_txt.h"
#include "yang.h"
#include "theme.h"
#include "yang.xpm"
#include "unknown.xpm"
#include "countryflags.h"
#include "yang_images.h"

BEGIN_EVENT_TABLE(ClientRoomFrame, YANGFrame)
//EVT_SIZE(ClientRoomFrame::OnResizeWindow)
//EVT_MAXIMIZE(ClientRoomFrame::OnMaximize)

EVT_BUTTON(ID_CLIENTTRANSFERUPLOAD, ClientRoomFrame::OnUploadButtonClick)
EVT_BUTTON(ID_CLIENTGENERALTRANSFERPURPOSE, ClientRoomFrame::OnTransferButtonClick)

//EVT_BUTTON(ID_CLIENTADDNEWLINE, ClientRoomFrame::OnInsertNewLine)
EVT_BUTTON(wxID_EXIT, ClientRoomFrame::OnLeaveRoom)
EVT_BUTTON(ID_CLIENTSHOWROOMSLIST, ClientRoomFrame::OnShowRoomsList)
EVT_BUTTON(ID_CLIENTCHATOUTPUTSTYLE, ClientRoomFrame::OnOpenChatOutputStyle)

EVT_BUTTON(ID_MAPSELECTIONCLOSE, ClientRoomFrame::OnMapSelectionClose)

EVT_BUTTON(ID_TRANSFERTHREADEND, ClientRoomFrame::OnTransferThreadEnd)

EVT_BUTTON(ID_CLIENTLOOKANDFEELCLOSE, ClientRoomFrame::OnLookAndFeelClose)

EVT_CHOICE(ID_CLIENTPLAYERCOLORCHOICE, ClientRoomFrame::OnPlayerColorChoice)

EVT_TOGGLEBUTTON(ID_CLIENTREADY, ClientRoomFrame::OnClientPressReady)

EVT_CLOSE(ClientRoomFrame::OnCloseWindow)

EVT_TEXT_URL(ID_CLIENTOUTPUTTEXT, ClientRoomFrame::OnTextURLEvent)

EVT_TEXT_ENTER(ID_CLIENTINPUTTEXT, ClientRoomFrame::OnEnterText)
//EVT_TEXT(ID_CLIENTINPUTTEXT, ClientRoomFrame::OnUpdateTextInput)

EVT_SOCKET(ID_CLIENTSOCKET, ClientRoomFrame::OnSocketEvent)
EVT_SOCKET(ID_CLIENTFILETRANSFERSOCKET, ClientRoomFrame::OnFileTransferSocketEvent)

EVT_TIMER(ID_CLIENTTIMER, ClientRoomFrame::OnTimer)
EVT_TIMER(ID_CLIENTFILETRANSFERTIMER, ClientRoomFrame::OnFileTransferTimer)
EVT_TIMER(ID_CLIENTPINGTIMER, ClientRoomFrame::OnPingTimer)
EVT_TIMER(ID_CLIENTPINGREFRESHTIMER, ClientRoomFrame::OnPingRefreshTimer)

EVT_CHECKBOX(ID_CLIENTTIMESTAMPS, ClientRoomFrame::OnTimeStampsCheck)
//EVT_CHECKBOX(ID_CLIENTNATFREE, ClientRoomFrame::OnNatFreeCheck)
END_EVENT_TABLE()

ClientRoomFrame::ClientRoomFrame() : YANGFrame(NULL, wxID_ANY, wxEmptyString)
{
	Maximize(g_configuration->client_is_maximized);

	wxBoxSizer* ClientRoomFramebSizer = new wxBoxSizer( wxVERTICAL );

	m_clientroompanel = new YANGPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* clientroompanelbSizer = new wxBoxSizer( wxVERTICAL );

	m_clientoutputtextCtrl = new YANGTextCtrl( m_clientroompanel, ID_CLIENTOUTPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(-1, 120)), wxTE_AUTO_URL|wxTE_MULTILINE|wxTE_READONLY|wxTE_RICH );
	clientroompanelbSizer->Add( m_clientoutputtextCtrl, 1, wxTOP|wxLEFT|wxRIGHT|wxEXPAND, 5 );

	wxBoxSizer* clientroompanelplayercolorbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_clientroomtimestampscheckBox = new YANGCheckBox( m_clientroompanel, ID_CLIENTTIMESTAMPS, wxT("Show time stamps"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelplayercolorbSizer->Add( m_clientroomtimestampscheckBox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	clientroompanelplayercolorbSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_clientplayercolorstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxT("Player color (Duke3dw/SWP):"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelplayercolorbSizer->Add( m_clientplayercolorstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_clientplayercolorchoice = new YANGChoice( m_clientroompanel, ID_CLIENTPLAYERCOLORCHOICE, wxDefaultPosition, wxDLG_UNIT(this, wxSize(48, -1)) );
	clientroompanelplayercolorbSizer->Add( m_clientplayercolorchoice, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	clientroompanelbSizer->Add( clientroompanelplayercolorbSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* clientroompanelinputbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_clientinputtextCtrl = new YANGTextCtrl( m_clientroompanel, ID_CLIENTINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, 16)),   /*wxTE_MULTILINE|*/wxTE_PROCESS_ENTER );
#if ((defined __WXMAC__) || (defined __WXCOCOA__))
	m_clientinputtextCtrl->MacCheckSpelling(true);
#endif 
	clientroompanelinputbSizer->Add( m_clientinputtextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_clientchatoutputstylebutton = new YANGButton( m_clientroompanel, ID_CLIENTCHATOUTPUTSTYLE, wxT("Chat output style"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinputbSizer->Add( m_clientchatoutputstylebutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	/*	m_clientnewlinebutton = new YANGButton( m_clientroompanel, ID_CLIENTADDNEWLINE, wxT("Insert a new line"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelinputbSizer->Add( m_clientnewlinebutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );*/

	clientroompanelbSizer->Add( clientroompanelinputbSizer, 0, wxEXPAND, 5 );

	/*	m_clientroomnatfreecheckBox = new wxCheckBox( m_clientroompanel, ID_CLIENTNATFREE, wxT("Use UDP hole-punching in game (NAT free)."), wxDefaultPosition, wxDefaultSize, 0 );

	clientroompanelbSizer->Add( m_clientroomnatfreecheckBox, 0, wxALL, 5 );*/

	wxBoxSizer* clientroompaneltransferbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_clienttransfergauge = new wxGauge( m_clientroompanel, wxID_ANY, 100, wxDefaultPosition, wxDLG_UNIT(this, wxSize(128, -1)), wxGA_HORIZONTAL );
	clientroompaneltransferbSizer->Add( m_clienttransfergauge, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_clienttransferstaticText = new wxStaticText( m_clientroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	clientroompaneltransferbSizer->Add( m_clienttransferstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	clientroompaneltransferbSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_clienttransferdynamicbutton = new YANGButton( m_clientroompanel, ID_CLIENTGENERALTRANSFERPURPOSE, wxT("Download map"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompaneltransferbSizer->Add( m_clienttransferdynamicbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_clienttransferuploadbutton = new YANGButton( m_clientroompanel, ID_CLIENTTRANSFERUPLOAD, wxT("Upload a map"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompaneltransferbSizer->Add( m_clienttransferuploadbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	clientroompanelbSizer->Add( clientroompaneltransferbSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* clientroompanellistsbSizer = new wxBoxSizer( wxHORIZONTAL );

	//	m_clientplayerslistCtrl = new YANGListCtrl( m_clientroompanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	m_clientplayerslistCtrl = new YANGListCtrl( m_clientroompanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(374,-1)), wxLC_REPORT );

	m_ImageList = new wxImageList(16, 16);
	m_clientplayerslistCtrl->AssignImageList(m_ImageList, wxIMAGE_LIST_SMALL);
	wxMemoryInputStream istream_cancel(cancel_png, sizeof cancel_png);
	m_ImageList->Add(wxImage(istream_cancel, wxBITMAP_TYPE_PNG));
	wxMemoryInputStream istream_accept(accept_png, sizeof accept_png);
	m_ImageList->Add(wxImage(istream_accept, wxBITMAP_TYPE_PNG));

	wxListItem itemCol;
	itemCol.SetText(wxT("Rdy"));
	//	itemCol.SetAlign(wxLIST_FORMAT_LEFT);
	//	itemCol.SetImage(-1);
	m_clientplayerslistCtrl->InsertColumn(COL_CLIENTREADY, itemCol);
	m_clientplayerslistCtrl->SetColumnWidth(COL_CLIENTREADY, g_configuration->client_col_ready_len);
	itemCol.SetText(wxT("Location"));
	m_clientplayerslistCtrl->InsertColumn(COL_CLIENTLOCATION, itemCol);
	m_clientplayerslistCtrl->SetColumnWidth(COL_CLIENTLOCATION, g_configuration->client_col_loc_len);
	itemCol.SetText(wxT("Nicknames"));
	m_clientplayerslistCtrl->InsertColumn(COL_CLIENTNICKNAMES, itemCol);
	m_clientplayerslistCtrl->SetColumnWidth(COL_CLIENTNICKNAMES, g_configuration->client_col_nick_len);
	itemCol.SetText(wxT("Ping"));
	m_clientplayerslistCtrl->InsertColumn(COL_CLIENTPING, itemCol);
	m_clientplayerslistCtrl->SetColumnWidth(COL_CLIENTPING, g_configuration->client_col_ping_len);
	itemCol.SetText(wxT("Flux"));
	m_clientplayerslistCtrl->InsertColumn(COL_CLIENTFLUX, itemCol);
	m_clientplayerslistCtrl->SetColumnWidth(COL_CLIENTFLUX, g_configuration->client_col_flux_len);
	itemCol.SetText(wxT("Operating System"));
	m_clientplayerslistCtrl->InsertColumn(COL_CLIENTOS, itemCol);
	m_clientplayerslistCtrl->SetColumnWidth(COL_CLIENTOS, g_configuration->client_col_os_len);
	itemCol.SetText(wxT("Detected IPs"));
	m_clientplayerslistCtrl->InsertColumn(COL_CLIENTDETECTED_IPS, itemCol);
	m_clientplayerslistCtrl->SetColumnWidth(COL_CLIENTDETECTED_IPS, g_configuration->client_col_detectedip_len);
	itemCol.SetText(wxT("In-game Addresses"));
	m_clientplayerslistCtrl->InsertColumn(COL_CLIENTINGAME_IPS, itemCol);
	m_clientplayerslistCtrl->SetColumnWidth(COL_CLIENTINGAME_IPS, g_configuration->client_col_ingameip_len);
	//	RefreshListCtrlSize();

	clientroompanellistsbSizer->Add( m_clientplayerslistCtrl, 1, wxRIGHT|wxEXPAND, 5 );

	m_clientsettingslistCtrl = new YANGListCtrl( m_clientroompanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(145, 101)), wxLC_REPORT );

	itemCol.SetText(wxT("Settings"));
	m_clientsettingslistCtrl->InsertColumn(0, itemCol);
	m_clientsettingslistCtrl->SetColumnWidth(0, g_configuration->client_col_settings_len);
	itemCol.SetText(wxT("Value"));
	m_clientsettingslistCtrl->InsertColumn(1, itemCol);
	m_clientsettingslistCtrl->SetColumnWidth(1, g_configuration->client_col_value_len);

	clientroompanellistsbSizer->Add( m_clientsettingslistCtrl, 0, wxEXPAND );

	clientroompanelbSizer->Add( clientroompanellistsbSizer, 0, wxTOP|wxLEFT|wxRIGHT|wxEXPAND, 5 );

	wxBoxSizer* clientroompanelbuttonsbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_clientreadytoggleBtn = new YANGToggleButton( m_clientroompanel, ID_CLIENTREADY, wxT("Ready"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelbuttonsbSizer->Add( m_clientreadytoggleBtn, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	clientroompanelbuttonsbSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	//	m_clientsendpmbutton = new YANGButton( m_clientroompanel, ID_CLIENTSENDPM, wxT("Send private message"), wxDefaultPosition, wxDefaultSize, 0 );
	//	clientroompanelbuttonsbSizer->Add( m_clientsendpmbutton, 0, wxALL, 5 );

	m_clientshowroomslistbutton = new YANGButton( m_clientroompanel, ID_CLIENTSHOWROOMSLIST, wxT("Show rooms list"), wxDefaultPosition, wxDefaultSize, 0 );
	clientroompanelbuttonsbSizer->Add( m_clientshowroomslistbutton, 0, wxALL, 5 );

	//	m_clientleaveroombutton = new YANGButton( m_clientroompanel, wxID_EXIT, wxT("Leave Room"), wxDefaultPosition, wxDefaultSize, 0 );
	//	clientroompanelbuttonsbSizer->Add( m_clientleaveroombutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	clientroompanelbSizer->Add( clientroompanelbuttonsbSizer, 0, wxEXPAND, 5 );

	m_clientroompanel->SetSizer( clientroompanelbSizer );
	m_clientroompanel->Layout();
	//	clientroompanelbSizer->Fit( m_clientroompanel );
	clientroompanelbSizer->SetSizeHints(m_clientroompanel);
	ClientRoomFramebSizer->Add( m_clientroompanel, 1, wxEXPAND | wxALL, 0 );

	SetSizer( ClientRoomFramebSizer );
	Layout();
	//	ClientRoomFramebSizer->Fit( this );
	ClientRoomFramebSizer->SetSizeHints(this);

	SetIcon(wxIcon(yang_xpm));
	SetSize(g_configuration->client_win_width, g_configuration->client_win_height);
	Centre();

	m_mainframe = new MainFrame(this);

	//m_clientroomnatfreecheckBox->SetValue(g_configuration->gamelaunch_enable_natfree);

	m_in_room = false;
	m_temp_in_game = false;
	m_allow_sounds = true;
	m_timer = NULL;

	m_transfer_client = NULL;
	m_transfer_state = TRANSFERSTATE_NONE;
	m_transfer_request = TRANSFERREQUEST_NONE;
	m_mapselection_dialog = NULL;
	//m_transfer_awaiting_download = false;
	// May prevent a few problems in case the player becomes in-room
	// before actually getting information about the room
	// (e.g. game and level) for some reason.

	m_clientplayercolorchoice->Disable();

	m_launch_usermap = false;
	m_nickname = g_configuration->nickname;

	m_clientroomtimestampscheckBox->SetValue(g_configuration->show_timestamps);

	// We aren't really in room, yet.
	m_clienttransferuploadbutton->Disable();
	m_clienttransferdynamicbutton->Disable();
	//m_clientnewlinebutton->Disable();
	//m_clientsendpmbutton->Disable();
	m_clientreadytoggleBtn->Disable();
	m_ping_timer = new wxTimer(this, ID_CLIENTPINGTIMER);
	m_pingrefresh_timer = new wxTimer(this, ID_CLIENTPINGREFRESHTIMER);

	m_lookandfeel_dialog = new LookAndFeelDialog(this, false);
	SetChatStyle();

	m_password_dialog = false;
}

/*
ClientRoomFrame::~ClientRoomFrame()
{
g_main_frame->ShowMainFrame();
}

void ClientRoomFrame::RefreshListCtrlSize()
{
int l_width;
if (m_clientplayerslistCtrl->GetItemCount())
l_width = wxLIST_AUTOSIZE;
else
l_width = wxLIST_AUTOSIZE_USEHEADER;
m_clientplayerslistCtrl->SetColumnWidth(0, l_width);
m_clientplayerslistCtrl->SetColumnWidth(1, l_width);
m_clientplayerslistCtrl->SetColumnWidth(2, l_width);
}

void ClientRoomFrame::OnResizeWindow(wxSizeEvent& event)
{
wxSize l_size = event.GetSize();
if (!IsMaximized())
{
g_configuration->client_win_width = l_size.GetWidth();
g_configuration->client_win_height = l_size.GetHeight();
}
}

void ClientRoomFrame::OnMaximize(wxMaximizeEvent& WXUNUSED(event))
{
if (IsMaximized())
GetSize(&g_configuration->client_win_width, &g_configuration->client_win_height);
}
*/
void ClientRoomFrame::FocusFrame()
{
	m_clientinputtextCtrl->SetFocus();
}

void ClientRoomFrame::AddMessage(const wxString& msg, bool allow_timestamp)
{
	if (allow_timestamp && g_configuration->show_timestamps)
		(*m_clientoutputtextCtrl) << wxT('(') << wxDateTime::Now().FormatISOTime() << wxT(") ");
	//m_clientoutputtextCtrl->AppendText(wxT('\n') + msg + wxT('\n'));
	(*m_clientoutputtextCtrl) << msg << wxT('\n');
	while (m_clientoutputtextCtrl->GetNumberOfLines() > MAX_NUM_MSG_LINES)
		m_clientoutputtextCtrl->Remove(0, (m_clientoutputtextCtrl->GetLineText(0)).Len()+1);
	// Without that, text scrolling may not work as expected.
#ifdef __WXMSW__
	m_clientoutputtextCtrl->ScrollLines(-1);
#elif ((defined __WXMAC__) || (defined __WXCOCOA__))
	((wxWindow*)m_clientoutputtextCtrl)->SetScrollPos(wxVERTICAL, ((wxWindow*)m_clientoutputtextCtrl)->GetScrollRange(wxVERTICAL));
#endif
}

void ClientRoomFrame::OnTextURLEvent(wxTextUrlEvent& event)
{
	if (event.GetMouseEvent().GetEventType() == wxEVT_LEFT_DOWN)
		g_OpenURL(m_clientoutputtextCtrl->GetRange(event.GetURLStart(), event.GetURLEnd()));
}

void ClientRoomFrame::UpdateGameSettings(const wxArrayString& l_info_input)
/*                                       GameType game,
SourcePortType srcport,
const wxString& roomname,
size_t max_num_players,
size_t skill,
DN3DMPGameT_Type dn3dgametype,
DN3DSpawnType dn3dspawn,
const wxString& extra_args,
bool launch_usermap,
size_t epimap_num,
const wxString& usermap,
const wxString& modname,
bool showmodurl,
const wxString& modurl,
bool usemasterslave)*/
{
	wxString l_temp_str;
	long l_temp_num;
	size_t l_num_of_items = l_info_input.GetCount(), l_temp_nonneg_num;

	GameType l_last_game = m_game;
	wxString l_last_usermap = m_usermap;
	bool l_last_launch_usermap = m_launch_usermap;
	bool l_last_eduke32_status = (m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT);

	m_last_roomname = m_roomname;
	m_roomname = l_info_input[2];
	SetTitle(m_roomname);
	//m_num_players = l_info_input[3];
	if (l_info_input[3].ToLong(&l_temp_num))
	{
		if ((l_temp_num < 0) || (l_temp_num > MAX_NUM_PLAYERS))
			m_max_num_players = DEFAULT_NUM_PLAYERS;
		else
			m_max_num_players = l_temp_num;
	}
	else
		m_max_num_players = DEFAULT_NUM_PLAYERS;
	s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);
	m_temp_in_game = (l_info_input[4] == wxT("yes"));

	l_temp_str = l_info_input[5];
	if (l_info_input[1] == GAMECODE_BLOOD)
	{
		m_game = GAME_BLOOD;
		s_gamename = GAMENAME_BLOOD;
		if (l_temp_str == SRCPORTCODE_DOSBLOODSW)
		{
			m_srcport = SOURCEPORT_DOSBLOODSW;
			s_srcport = SRCPORTNAME_DOSBLOODSW;
		}
		else if (l_temp_str == SRCPORTCODE_DOSBLOODRG)
		{
			m_srcport = SOURCEPORT_DOSBLOODRG;
			s_srcport = SRCPORTNAME_DOSBLOODRG;
		}
		else if (l_temp_str == SRCPORTCODE_DOSBLOODPP)
		{
			m_srcport = SOURCEPORT_DOSBLOODPP;
			s_srcport = SRCPORTNAME_DOSBLOODPP;
		}
		else
		{
			m_srcport = SOURCEPORT_DOSBLOODOU;
			s_srcport = SRCPORTNAME_DOSBLOODOU;
		}

		m_use_crypticpassage = (l_info_input[6] == wxT("yes"));

		if (l_info_input[7].ToLong(&l_temp_num))
		{
			if ((l_temp_num < 0) && (l_temp_num > BLOOD_MAX_SKILLNUM))
				m_skill = BLOOD_DEFAULT_SKILL;
			else
				m_skill = l_temp_num;
		}
		else
			m_skill = BLOOD_DEFAULT_SKILL;
		s_skill = (*g_blood_skill_list)[m_skill];

		l_temp_str = l_info_input[8];
		if (l_temp_str == wxT("coop"))
			m_bloodgametype = BLOODMPGAMETYPE_COOP;
		else if (l_temp_str == wxT("bb"))
			m_bloodgametype = BLOODMPGAMETYPE_BB;
		else
			m_bloodgametype = BLOODMPGAMETYPE_TEAMS;
		s_gametype = (*g_blood_gametype_list)[m_bloodgametype-1];

		l_temp_str = l_info_input[9];
		if (l_temp_str == wxT("none"))
			m_blood_monster = BLOODMPMONSTERTYPE_NONE;
		else if (l_temp_str == wxT("enabled"))
			m_blood_monster = BLOODMPMONSTERTYPE_ENABLED;
		else
			m_blood_monster = BLOODMPMONSTERTYPE_RESPAWN;
		s_monster = (*g_blood_monstersettings_list)[m_blood_monster];

		l_temp_str = l_info_input[10];
		if (l_temp_str == wxT("norespawn"))
			m_blood_weapon = BLOODMPWEAPONTYPE_NORESPAWN;
		else if (l_temp_str == wxT("permanent"))
			m_blood_weapon = BLOODMPWEAPONTYPE_PERMANENT;
		else if (l_temp_str == wxT("respawn"))
			m_blood_weapon = BLOODMPWEAPONTYPE_RESPAWN;
		else
			m_blood_weapon = BLOODMPWEAPONTYPE_MARKERS;
		s_weapon = (*g_blood_weaponsettings_list)[m_blood_weapon];

		l_temp_str = l_info_input[11];
		if (l_temp_str == wxT("norespawn"))
			m_blood_item = BLOODMPITEMTYPE_NORESPAWN;
		else if (l_temp_str == wxT("respawn"))
			m_blood_item = BLOODMPITEMTYPE_RESPAWN;
		else
			m_blood_item = BLOODMPITEMTYPE_MARKERS;
		s_item = (*g_blood_itemsettings_list)[m_blood_item];

		l_temp_str = l_info_input[12];
		if (l_temp_str == wxT("random"))
			m_blood_respawn = BLOODMPRESPAWNTYPE_RANDOM;
		else if (l_temp_str == wxT("weapons"))
			m_blood_respawn = BLOODMPRESPAWNTYPE_WEAPONS;
		else
			m_blood_respawn = BLOODMPRESPAWNTYPE_ENEMIES;
		s_respawn = (*g_blood_respawnsettings_list)[m_blood_respawn];

		m_args_for_host = l_info_input[13];
#if EXTRA_ARGS_FOR_ALL
		m_args_for_all = l_info_input[14];
#endif

		m_use_password = (l_info_input[15] == wxT("yes"));

		l_info_input[16].ToLong(&l_temp_num);
		m_maxping = l_temp_num;
		s_maxping = l_info_input[16];

		m_recorddemo = (l_info_input[17] == wxT("yes"));
		m_demofilename = l_info_input[18];

		m_launch_usermap = (l_info_input[19] == wxT("usermap"));
		if (m_launch_usermap)
		{
			m_usermap = l_info_input[20];
			s_map = m_usermap;
		}
		else
		{
			if (l_info_input[20].ToLong(&l_temp_num))
			{
				m_epimap_num = l_temp_num;

				if (!m_use_crypticpassage)
					l_temp_nonneg_num = g_GetBloodLevelSelectionByNum(m_epimap_num);
				else
					l_temp_nonneg_num = g_GetBloodCPLevelSelectionByNum(m_epimap_num);

				if ((l_temp_nonneg_num <= 0) || (l_temp_nonneg_num >= (!m_use_crypticpassage ? g_blood_level_list->GetCount() : g_blood_cp_level_list->GetCount())))
				{
					m_epimap_num = 0;
					l_temp_nonneg_num = 0;
				}
			}
			else
			{
				m_epimap_num = 0;
				l_temp_nonneg_num = 0;
			}

			if (!m_use_crypticpassage)
				s_map = (*g_blood_level_list)[l_temp_nonneg_num];
			else
				s_map = (*g_blood_cp_level_list)[l_temp_nonneg_num];
		}

		m_connectiontype = PACKETMODE_AUTO;

		if (l_num_of_items >= 22)
		{
			if (l_info_input[21] == wxT("broadcast"))
				m_connectiontype = PACKETMODE_BROADCAST;
			else if (l_info_input[21] == wxT("masterslave"))
				m_connectiontype = PACKETMODE_MASTERSLAVE;
		}

		switch (m_connectiontype)
		{
			case PACKETMODE_AUTO:        s_packetmode = wxT("Auto"); break;
			case PACKETMODE_BROADCAST:   s_packetmode = wxT("Broadcast"); break;
			case PACKETMODE_MASTERSLAVE: s_packetmode = wxT("Master/Slave"); break;
			default: ;
		}

		s_connection = wxT("Emulated IPX/SPX");
	}
	else if (l_info_input[1] == GAMECODE_DESCENT)
	{
		m_game = GAME_DESCENT;
		s_gamename = GAMENAME_DESCENT;
		if (l_temp_str == SRCPORTCODE_DOSDESCENT)
		{
			m_srcport = SOURCEPORT_DOSDESCENT;
			s_srcport = SRCPORTNAME_DOSDESCENT;
		}
		else
		{
			m_srcport = SOURCEPORT_D1XREBIRTH;
			s_srcport = SRCPORTNAME_D1XREBIRTH;
		}
		l_temp_str = l_info_input[6];
		if (l_temp_str == wxT("anarchy"))
			m_descentgametype = DESCENTMPGAMETYPE_ANARCHY;
		else if (l_temp_str == wxT("team_anarchy"))
			m_descentgametype = DESCENTMPGAMETYPE_TEAM_ANARCHY;
		else if (l_temp_str == wxT("robo_anarchy"))
			m_descentgametype = DESCENTMPGAMETYPE_ROBO_ANARCHY;
		else
			m_descentgametype = DESCENTMPGAMETYPE_COOPERATIVE;
		s_gametype = (*g_descent_gametype_list)[m_descentgametype];

		m_args_for_host = l_info_input[7];
#if EXTRA_ARGS_FOR_ALL
		m_args_for_all = l_info_input[8];
#endif

		m_use_password = (l_info_input[9] == wxT("yes"));

		l_info_input[10].ToLong(&l_temp_num);
		m_maxping = l_temp_num;
		s_maxping = l_info_input[10];

		m_recorddemo = false;

		m_launch_usermap = (l_info_input[11] == wxT("usermap"));
		if (m_launch_usermap)
		{
			m_usermap = l_info_input[12];
			s_map = m_usermap;
		}
		else
		{
			if (l_info_input[12].ToLong(&l_temp_num))
			{
				m_epimap_num = l_temp_num;
				if ((m_epimap_num <= 0) || (m_epimap_num >= g_descent_level_list->GetCount()))
					m_epimap_num = 0;
			}
			else
				m_epimap_num = 0;
			s_map = (*g_descent_level_list)[m_epimap_num];
		}

		if (m_srcport == SOURCEPORT_DOSDESCENT)
			s_connection = wxT("Emulated IPX/SPX");
		else
			s_connection = wxT("Peer-2-Peer");
	}
	else if (l_info_input[1] == GAMECODE_DESCENT2)
	{
		m_game = GAME_DESCENT2;
		s_gamename = GAMENAME_DESCENT2;
		if (l_temp_str == SRCPORTCODE_DOSDESCENT2)
		{
			m_srcport = SOURCEPORT_DOSDESCENT2;
			s_srcport = SRCPORTNAME_DOSDESCENT2;
		}
		else
		{
			m_srcport = SOURCEPORT_D2XREBIRTH;
			s_srcport = SRCPORTNAME_D2XREBIRTH;
		}
		l_temp_str = l_info_input[6];
		if (l_temp_str == wxT("anarchy"))
			m_descent2gametype = DESCENT2MPGAMETYPE_ANARCHY;
		else if (l_temp_str == wxT("team_anarchy"))
			m_descent2gametype = DESCENT2MPGAMETYPE_TEAM_ANARCHY;
		else if (l_temp_str == wxT("robo_anarchy"))
			m_descent2gametype = DESCENT2MPGAMETYPE_ROBO_ANARCHY;
		else if (l_temp_str == wxT("cooperative"))
			m_descent2gametype = DESCENT2MPGAMETYPE_COOPERATIVE;
		else if (l_temp_str == wxT("capture_the_flag"))
			m_descent2gametype = DESCENT2MPGAMETYPE_CAPTURE_THE_FLAG;
		else if (l_temp_str == wxT("hoard"))
			m_descent2gametype = DESCENT2MPGAMETYPE_HOARD;
		else
			m_descent2gametype = DESCENT2MPGAMETYPE_TEAM_HOARD;
		s_gametype = (*g_descent2_gametype_list)[m_descent2gametype];

		m_args_for_host = l_info_input[7];
#if EXTRA_ARGS_FOR_ALL
		m_args_for_all = l_info_input[8];
#endif

		m_use_password = (l_info_input[9] == wxT("yes"));

		l_info_input[10].ToLong(&l_temp_num);
		m_maxping = l_temp_num;
		s_maxping = l_info_input[10];

		m_recorddemo = false;

		m_launch_usermap = (l_info_input[11] == wxT("usermap"));
		if (m_launch_usermap)
		{
			m_usermap = l_info_input[12];
			s_map = m_usermap;
		}
		else
		{
			if (l_info_input[12].ToLong(&l_temp_num))
			{
				m_epimap_num = l_temp_num;
				if ((m_epimap_num <= 0) || (m_epimap_num >= g_descent2_level_list->GetCount()))
					m_epimap_num = 0;
			}
			else
				m_epimap_num = 0;
			s_map = (*g_descent2_level_list)[m_epimap_num];
		}

		if (m_srcport == SOURCEPORT_DOSDESCENT2)
			s_connection = wxT("Emulated IPX/SPX");
		else
			s_connection = wxT("Peer-2-Peer");
	}
	else if (l_info_input[1] == GAMECODE_DN3D)
	{
		m_game = GAME_DN3D;
		s_gamename = GAMENAME_DN3D;
		if (l_temp_str == SRCPORTCODE_DOSDUKESW)
		{
			m_srcport = SOURCEPORT_DOSDUKESW;
			s_srcport = SRCPORTNAME_DOSDUKESW;
		}
		else if (l_temp_str == SRCPORTCODE_DOSDUKERG)
		{
			m_srcport = SOURCEPORT_DOSDUKERG;
			s_srcport = SRCPORTNAME_DOSDUKERG;
		}
		else if (l_temp_str == SRCPORTCODE_DOSDUKEAE)
		{
			m_srcport = SOURCEPORT_DOSDUKEAE;
			s_srcport = SRCPORTNAME_DOSDUKEAE;
		}
		else if (l_temp_str == SRCPORTCODE_DUKE3DW)
		{
			m_srcport = SOURCEPORT_DUKE3DW;
			s_srcport = SRCPORTNAME_DUKE3DW;
		}
		else if (l_temp_str == SRCPORTCODE_EDUKE32)
		{
			m_srcport = SOURCEPORT_EDUKE32;
			s_srcport = SRCPORTNAME_EDUKE32;
		}
		else if (l_temp_str == SRCPORTCODE_NDUKE)
		{
			m_srcport = SOURCEPORT_NDUKE;
			s_srcport = SRCPORTNAME_NDUKE;
		}
		else if (l_temp_str == SRCPORTCODE_HDUKE)
		{
			m_srcport = SOURCEPORT_HDUKE;
			s_srcport = SRCPORTNAME_HDUKE;
		}
		else
		{
			m_srcport = SOURCEPORT_XDUKE;
			s_srcport = SRCPORTNAME_XDUKE;
		}
		if (l_info_input[6].ToLong(&l_temp_num))
		{
			if ((l_temp_num < 0) && (l_temp_num > DN3D_MAX_SKILLNUM))
				m_skill = DN3D_DEFAULT_SKILL;
			else
				m_skill = l_temp_num;
		}
		else
			m_skill = DN3D_DEFAULT_SKILL;
		s_skill = (*g_dn3d_skill_list)[m_skill];
		l_temp_str = l_info_input[7];
		if (l_temp_str == wxT("dmspawn"))
			m_dn3dgametype = DN3DMPGAMETYPE_DMSPAWN;
		else if (l_temp_str == wxT("coop"))
			m_dn3dgametype = DN3DMPGAMETYPE_COOP;
		else if (l_temp_str == wxT("dmnospawn"))
			m_dn3dgametype = DN3DMPGAMETYPE_DMNOSPAWN;
		else if (l_temp_str == wxT("tdmspawn"))
			m_dn3dgametype = DN3DMPGAMETYPE_TDMSPAWN;
		else
			m_dn3dgametype = DN3DMPGAMETYPE_TDMNOSPAWN;
		s_gametype = (*g_dn3d_gametype_list)[m_dn3dgametype-1];
		l_temp_str = l_info_input[8];
		if (l_temp_str == wxT("none"))
			m_dn3dspawn = DN3DSPAWN_NONE;
		else if (l_temp_str == wxT("monsters"))
			m_dn3dspawn = DN3DSPAWN_MONSTERS;
		else if (l_temp_str == wxT("items"))
			m_dn3dspawn = DN3DSPAWN_ITEMS;
		else if (l_temp_str == wxT("inventory"))
			m_dn3dspawn = DN3DSPAWN_INVENTORY;
		else
			m_dn3dspawn = DN3DSPAWN_ALL;
		s_spawn = (*g_dn3d_spawn_list)[m_dn3dspawn];

		m_args_for_host = l_info_input[9];
#if EXTRA_ARGS_FOR_ALL
		m_args_for_all = l_info_input[10];
#endif

		m_use_password = (l_info_input[11] == wxT("yes"));

		l_info_input[12].ToLong(&l_temp_num);
		m_maxping = l_temp_num;
		s_maxping = l_info_input[12];

		m_recorddemo = (l_info_input[13] == wxT("yes"));
		m_demofilename = l_info_input[14];

		m_launch_usermap = (l_info_input[15] == wxT("usermap"));
		if (m_launch_usermap)
		{
			m_usermap = l_info_input[16];
			s_map = m_usermap;
		}
		else
		{
			if (l_info_input[16].ToLong(&l_temp_num))
			{
				m_epimap_num = l_temp_num;
				l_temp_nonneg_num = g_GetDN3DLevelSelectionByNum(m_epimap_num);
				if ((l_temp_nonneg_num <= 0) || (l_temp_nonneg_num >= g_dn3d_level_list->GetCount()))
				{
					m_epimap_num = 0;
					l_temp_nonneg_num = 0;
				}
			}
			else
			{
				m_epimap_num = 0;
				l_temp_nonneg_num = 0;
			}
			s_map = (*g_dn3d_level_list)[l_temp_nonneg_num];
		}

		m_connectiontype = CONNECTIONTYPE_PEER2PEER;

		if (l_num_of_items >= 18)
		{
			if (l_info_input[17] == wxT("masterslave"))
				m_connectiontype = CONNECTIONTYPE_MASTERSLAVE;
			else if (l_info_input[17] == wxT("serverclient"))
				m_connectiontype = CONNECTIONTYPE_SERVERCLIENT;
		}

		if (m_connectiontype == CONNECTIONTYPE_SERVERCLIENT)
			s_connection = wxT("Server/Client (BROKEN!)");
		else if (m_connectiontype == CONNECTIONTYPE_MASTERSLAVE)
			s_connection = wxT("Master/Slave");
		else if (m_srcport == SOURCEPORT_DOSDUKESW || m_srcport == SOURCEPORT_DOSDUKERG || m_srcport == SOURCEPORT_DOSDUKEAE)
			s_connection = wxT("Emulated IPX/SPX");
		else
			s_connection = wxT("Peer-2-Peer");
	}
	else if (l_info_input[1] == GAMECODE_SW)
	{
		m_game = GAME_SW;
		s_gamename = GAMENAME_SW;
		if (l_temp_str == SRCPORTCODE_DOSSWSW)
		{
			m_srcport = SOURCEPORT_DOSSWSW;
			s_srcport = SRCPORTNAME_DOSSWSW;
		}
		else if (l_temp_str == SRCPORTCODE_DOSSWRG)
		{
			m_srcport = SOURCEPORT_DOSSWRG;
			s_srcport = SRCPORTNAME_DOSSWRG;
		}
		else if (l_temp_str == SRCPORTCODE_VOIDSW)
		{
			m_srcport = SOURCEPORT_VOIDSW;
			s_srcport = SRCPORTNAME_VOIDSW;
		}
		else
		{
			m_srcport = SOURCEPORT_SWP;
			s_srcport = SRCPORTNAME_SWP;
		}
		if (l_info_input[6].ToLong(&l_temp_num))
		{
			if ((l_temp_num < 0) && (l_temp_num > SW_MAX_SKILLNUM))
				m_skill = SW_DEFAULT_SKILL;
			else
				m_skill = l_temp_num;
		}
		else
			m_skill = SW_DEFAULT_SKILL;
		s_skill = (*g_sw_skill_list)[m_skill];

		if (m_srcport == SOURCEPORT_SWP)
		{
			l_temp_str = l_info_input[7];
			if (l_temp_str == wxT("wbspawn"))
				m_swgametype = SWMPGAMETYPE_WBSPAWN;
			else if (l_temp_str == wxT("wbnospawn"))
				m_swgametype = SWMPGAMETYPE_WBNOSPAWN;
			else
				m_swgametype = SWMPGAMETYPE_COOP;
			s_gametype = (*g_sw_gametype_list)[m_swgametype];
		}
		else
			s_gametype = wxEmptyString;

		m_args_for_host = l_info_input[8];
#if EXTRA_ARGS_FOR_ALL
		m_args_for_all = l_info_input[9];
#endif

		m_use_password = (l_info_input[10] == wxT("yes"));

		l_info_input[11].ToLong(&l_temp_num);
		m_maxping = l_temp_num;
		s_maxping = l_info_input[11];

		m_recorddemo = (l_info_input[12] == wxT("yes"));
		m_demofilename = l_info_input[13];

		m_launch_usermap = (l_info_input[14] == wxT("usermap"));
		if (m_launch_usermap)
		{
			m_usermap = l_info_input[15];
			s_map = m_usermap;
		}
		else
		{
			if (l_info_input[15].ToLong(&l_temp_num))
			{
				m_epimap_num = l_temp_num;
				l_temp_nonneg_num = g_GetSWLevelSelectionByNum(m_epimap_num);
				if ((l_temp_nonneg_num <= 0) || (l_temp_nonneg_num >= g_sw_level_list->GetCount()))
				{
					m_epimap_num = 0;
					l_temp_nonneg_num = 0;
				}
			}
			else
			{
				m_epimap_num = 0;
				l_temp_nonneg_num = 0;
			}
			s_map = (*g_sw_level_list)[l_temp_nonneg_num];
		}

		m_connectiontype =
			(m_srcport == SOURCEPORT_VOIDSW) ? CONNECTIONTYPE_MASTERSLAVE : CONNECTIONTYPE_PEER2PEER;

		if (m_srcport == SOURCEPORT_DOSSWSW || m_srcport == SOURCEPORT_DOSSWRG)
			s_connection = wxT("Emulated IPX/SPX");
		else
			s_connection = wxT("Peer-2-Peer");
	}
	else if (l_info_input[1] == GAMECODE_CUSTOM)
	{
		m_game = GAME_CUSTOM;
		s_gamename = GAMENAME_CUSTOM;
		m_srcport = SOURCEPORT_CUSTOM;

		s_gametype = wxEmptyString;

		m_args_for_host = l_info_input[6];
#if EXTRA_ARGS_FOR_ALL
		m_args_for_all = l_info_input[7];
#endif

		m_use_password = (l_info_input[8] == wxT("yes"));

		l_info_input[9].ToLong(&l_temp_num);
		m_maxping = l_temp_num;
		s_maxping = l_info_input[9];

		m_recorddemo = false;

		m_connectiontype = 0;

		if (l_num_of_items >= 11)
		{
			m_connectiontype = (l_info_input[10] == wxT("nullmodem"));

			if (l_num_of_items >= 12)
			{
				l_info_input[11].ToLong(&l_temp_num);
				m_rxdelay = l_temp_num;
				s_rxdelay = l_info_input[11];
			}
			else
			{
				m_rxdelay = DEFAULT_RXDELAY;
				s_rxdelay = wxString::Format(wxT("%lu"), (unsigned long)m_rxdelay);
			}
		}

		if (m_connectiontype)
			s_connection = wxT("Null-Modem");
		else
			s_connection = wxT("Emulated IPX/SPX");
	}

	UpdateSettingsList();

	m_clientplayercolorchoice->Clear();
	if (m_srcport == SOURCEPORT_DUKE3DW)
	{
		m_clientplayercolorchoice->Append(*g_dn3d_playercol_list);
		m_clientplayercolorchoice->SetSelection(g_configuration->gamelaunch_dn3d_playercolnum);
	}
	else if (m_srcport == SOURCEPORT_SWP)
	{
		m_clientplayercolorchoice->Append(*g_sw_playercol_list);
		m_clientplayercolorchoice->SetSelection(g_configuration->gamelaunch_sw_playercolnum);
	}
	m_clientplayercolorchoice->Enable((m_srcport == SOURCEPORT_DUKE3DW) || (m_srcport == SOURCEPORT_SWP));

	// Stuff related to file transfer...

	if (m_mapselection_dialog && (l_last_game != m_game))
	{
		m_mapselection_dialog->m_game = m_game;
		m_mapselection_dialog->RefreshMapsList();
	}

	if (((l_last_game != m_game) || (l_last_usermap != m_usermap) || (l_last_launch_usermap != m_launch_usermap) ||
		(l_last_eduke32_status != (m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT)))
		&& (m_transfer_state == TRANSFERSTATE_NONE))
	{
		/*if ((l_last_game != m_game) || (l_last_usermap != m_usermap) || (l_last_launch_usermap != m_launch_usermap))
		switch (m_transfer_state)
		{
		case TRANSFERSTATE_NONE:*/
		if (m_transfer_request != TRANSFERREQUEST_NONE)
		{
			g_SendCStringToSocket(m_client, "1:requestcancel:");

			if (m_launch_usermap &&
				(((m_game == GAME_DESCENT) && !g_FindDescentMSNfile(&m_extra_usermap, m_usermap))
				|| ((m_game == GAME_DESCENT2) && !g_FindDescentMN2file(&m_extra_usermap, m_usermap))))
			{
				g_SendCStringToSocket(m_client, "1:requestextradownload:");
			}

			if (m_launch_usermap && !(m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT) &&
				(((m_game == GAME_BLOOD) && !wxFileExists(g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap))
				|| ((m_game == GAME_DESCENT) && !wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap))
				|| ((m_game == GAME_DESCENT2) && !wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap))
				|| ((m_game == GAME_DN3D) && !wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap))
				|| ((m_game == GAME_SW) && !wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))
			{
				g_SendCStringToSocket(m_client, "1:requestdownload:");
				m_transfer_filename = m_usermap;
				m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
				m_clienttransferstaticText->SetLabel(m_transfer_filename);
			}
			else
			{
				m_transfer_request = TRANSFERREQUEST_NONE;
				m_clienttransferstaticText->SetLabel(wxEmptyString);
				m_clienttransferuploadbutton->Enable();
				m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
			}
		}
		else
		{
			if ((m_mapselection_dialog == NULL) && m_in_room && m_launch_usermap &&
				(((m_game == GAME_DESCENT) && !g_FindDescentMSNfile(&m_extra_usermap, m_usermap))
				|| ((m_game == GAME_DESCENT2) && !g_FindDescentMN2file(&m_extra_usermap, m_usermap))))
			{
				g_SendCStringToSocket(m_client, "1:requestextradownload:");
			}

			if ((m_mapselection_dialog == NULL) && m_in_room && m_launch_usermap && !(m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT) &&
				(((m_game == GAME_BLOOD) && !wxFileExists(g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap))
				|| ((m_game == GAME_DESCENT) && !wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap))
				|| ((m_game == GAME_DESCENT2) && !wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap))
				|| ((m_game == GAME_DN3D) && !wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap))
				|| ((m_game == GAME_SW) && !wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))
			{
				g_SendCStringToSocket(m_client, "1:requestdownload:");
				m_transfer_filename = m_usermap;
				m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
				m_clienttransferstaticText->SetLabel(m_transfer_filename);
				m_clienttransferuploadbutton->Disable();
				m_clienttransferdynamicbutton->Enable();
				m_clienttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
			}
			else
				m_clienttransferdynamicbutton->Disable();
		}
	}
	/*      break;
	case TRANSFERSTATE_PENDING:
	m_transfer_state = TRANSFERSTATE_NONE;
	g_SendCStringToSocket(m_client, "1:requestcancel:");
	m_transfer_client->Destroy();
	m_transfer_client = NULL;
	if (m_launch_usermap &&
	(((m_game == GAME_DN3D) && !wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap))
	|| ((m_game == GAME_SW) && !wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))
	{
	g_SendCStringToSocket(m_client, "1:requestdownload:");
	m_transfer_filename = m_usermap;
	m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
	m_clienttransferstaticText->SetLabel(m_transfer_filename);
	}
	else
	{
	m_transfer_request = TRANSFERREQUEST_NONE;
	m_clienttransferstaticText->SetLabel(wxEmptyString);
	m_clienttransferuploadbutton->Enable();
	m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
	}
	break;
	default: // BUSY, ABORTED. In the case of BUSY, the host should ask for abortion.
	if (m_transfer_state == TRANSFERSTATE_BUSY)
	{
	m_transfer_state = TRANSFERSTATE_ABORTED;
	g_SendCStringToSocket(m_client, "1:aborttransfer:");
	}
	m_transfer_awaiting_download = (m_launch_usermap &&
	(((m_game == GAME_DN3D) && !wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap))
	|| ((m_game == GAME_SW) && !wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))));
	}*/
}

void ClientRoomFrame::UpdateSettingsList()
{
	size_t l_index = 0;

	m_clientsettingslistCtrl->DeleteAllItems();

	m_clientsettingslistCtrl->InsertItem(l_index, wxT("Game"));
	m_clientsettingslistCtrl->SetItem(l_index, 1, s_gamename);

	if (m_game != GAME_CUSTOM)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Port"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_srcport);
	}

	if (m_game == GAME_BLOOD && m_use_crypticpassage)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Add-on"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, wxT("Cryptic Passage"));
	}

	l_index++;
	m_clientsettingslistCtrl->InsertItem(l_index, wxT("Players"));
	m_clientsettingslistCtrl->SetItem(l_index, 1, s_players);

	if (m_game != GAME_CUSTOM)
	{
		l_index++;
		if (m_game != GAME_DESCENT && m_game != GAME_DESCENT2)
			m_clientsettingslistCtrl->InsertItem(l_index, wxT("Map"));
		else
			m_clientsettingslistCtrl->InsertItem(l_index, wxT("Mission"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_map);
	}

	if (s_mod != wxEmptyString)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("TC/MOD"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_mod);
	}

	if (s_gametype != wxEmptyString)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Game Type"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_gametype);
	}

	if (m_game != GAME_DESCENT && m_game != GAME_DESCENT2 && m_game != GAME_CUSTOM)
	{
		l_index++;
		if (m_game != GAME_BLOOD)
			m_clientsettingslistCtrl->InsertItem(l_index, wxT("Skill"));
		else
			m_clientsettingslistCtrl->InsertItem(l_index, wxT("Difficulty"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_skill);
	}

	if (m_game == GAME_DN3D)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Spawn"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_spawn);
	}

	if (m_game == GAME_BLOOD)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Monsters"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_monster);

		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Weapons"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_weapon);

		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Items"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_item);

		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Respawn"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_respawn);
	}

	if (m_use_password)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Private"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, wxT("Yes"));
	}

	if (m_maxping)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Max Ping"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_maxping);
	}

	if (m_recorddemo)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(
			l_index,
			(m_srcport == SOURCEPORT_HDUKE) ?
			wxT("hDuke mode select") : wxT("Record demo"));
		if (m_demofilename == wxEmptyString)
			m_clientsettingslistCtrl->SetItem(l_index, 1, wxT("Yes"));
		else
			m_clientsettingslistCtrl->SetItem(l_index, 1, m_demofilename);
	}

	if (m_args_for_host != wxEmptyString)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Host args"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, m_args_for_host);
	}

#if EXTRA_ARGS_FOR_ALL
	if (m_args_for_all != wxEmptyString)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Extra args"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, m_args_for_all);
	}
#endif

	if (m_game == GAME_BLOOD && m_connectiontype)
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Packet mode"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_packetmode);
	}

	if (m_srcport == SOURCEPORT_DUKE3DW || m_srcport == SOURCEPORT_EDUKE32 || (m_game == GAME_CUSTOM && m_connectiontype))
	{
		l_index++;
		m_clientsettingslistCtrl->InsertItem(l_index, wxT("Connection"));
		m_clientsettingslistCtrl->SetItem(l_index, 1, s_connection);

		if (m_game == GAME_CUSTOM)
		{
			l_index++;
			m_clientsettingslistCtrl->InsertItem(l_index, wxT("rxdelay"));
			m_clientsettingslistCtrl->SetItem(l_index, 1, s_rxdelay + (m_rxdelay == DEFAULT_RXDELAY ? wxT(" (DOSBox's default)") : wxEmptyString));
		}
	}

	for (l_index = 0; (signed)l_index < m_clientsettingslistCtrl->GetItemCount(); l_index++)
		m_clientsettingslistCtrl->SetItemTextColour(l_index, m_def_listctrl_color);
}

void ClientRoomFrame::UpdateModFilesInfo(const wxArrayString& l_info_input)
{
	size_t l_loop_var, l_num_of_items = l_info_input.GetCount();

	if (l_num_of_items >= 5)
	{
		m_last_modname = m_modname;
		m_modname = l_info_input[1];
		s_mod = m_modname;
		UpdateSettingsList();
		m_modconfile = l_info_input[2];
		m_moddeffile = l_info_input[3];
		m_showmodurl = (l_info_input[4] == wxT("withurl"));
		if (m_showmodurl && (l_num_of_items >= 6))
		{
			m_modurl = l_info_input[5];
			l_loop_var = 6;
		}
		else
			l_loop_var = 5;
		m_modfiles.Empty();
		while (l_loop_var < l_num_of_items)
		{
			m_modfiles.Add(l_info_input[l_loop_var]);
			l_loop_var++;
		}
	}
}

bool ClientRoomFrame::ConnectToServer(wxString ip_address, long port_num)
//void ClientRoomFrame::ConnectToServer(wxString ip_address, long port_num)
{
	wxIPV4address l_addr;
	wxArrayString l_str_list;
	l_addr.Hostname(ip_address);
	l_addr.Service(port_num);

	m_client = new wxSocketClient();

	m_client->SetTimeout(10);
	m_client->SetEventHandler(*this, ID_CLIENTSOCKET);
	m_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
	//m_client->SetNotify(wxSOCKET_CONNECTION_FLAG |
	//                    wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
	m_client->SetNotify(wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
	m_client->Notify(true);
	if (m_client->Connect(l_addr, true))
	{
		m_client->SetTimeout(1);
		m_timer = new wxTimer(this, ID_CLIENTTIMER);
		m_timer->Start(30000, wxTIMER_ONE_SHOT);
		l_str_list.Add(wxT("requestroominfo"));
		l_str_list.Add(YANG_STR_NET_VERSION);
		l_str_list.Add(m_nickname);
		g_SendStringListToSocket(m_client, l_str_list);
		return true;
	}  
	/*m_client->Connect(l_addr, false);
	return true;
	if (!m_client->IsConnected());
	if ((m_client->Connect(l_addr, false))
	|| (m_client->WaitOnConnect(10, 0)))
	if (m_client->IsConnected())
	{
	m_client->SetTimeout(1);
	m_timer = new wxTimer(this, ID_CLIENTTIMER);
	m_timer->Start(30000, wxTIMER_ONE_SHOT);
	l_str_list.Add(wxT("requestroominfo"));
	l_str_list.Add(YANG_STR_NET_VERSION);
	l_str_list.Add(wxT(m_nickname);
	g_SendStringListToSocket(m_client, l_str_list);
	return true;
	}*/
	m_client->Destroy();
	return false;
}

void ClientRoomFrame::OnUploadButtonClick(wxCommandEvent& WXUNUSED(event))
{
	if (m_mapselection_dialog == NULL)
	{
		m_mapselection_dialog = new MapSelectionDialog(this, m_game);
		m_mapselection_dialog->RefreshMapsList();
		m_mapselection_dialog->ShowDialog();
		m_clienttransferdynamicbutton->Disable();
	}
	else
		m_mapselection_dialog->Raise();
}

void ClientRoomFrame::OnTransferButtonClick(wxCommandEvent& WXUNUSED(event))
{
	if (m_transfer_state == TRANSFERSTATE_BUSY)
	{ // Abortion
		m_transfer_state = TRANSFERSTATE_ABORTED;
		g_SendCStringToSocket(m_client, "1:aborttransfer:");
	}
	else if (m_transfer_state == TRANSFERSTATE_PENDING)
	{ // Ready to connect to the file-transfer server...
		m_transfer_state = TRANSFERSTATE_NONE;
		m_transfer_request = TRANSFERREQUEST_NONE;
		g_SendCStringToSocket(m_client, "1:requestcancel:");
		m_transfer_client->Destroy();
		m_transfer_client = NULL;
		m_clienttransferstaticText->SetLabel(wxEmptyString);
		m_clienttransferuploadbutton->Enable();
		m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
	}
	else if (m_transfer_state == TRANSFERSTATE_NONE)
	{
		if (m_transfer_request != TRANSFERREQUEST_NONE)
		{
			m_transfer_request = TRANSFERREQUEST_NONE;
			g_SendCStringToSocket(m_client, "1:requestcancel:");
			m_clienttransferstaticText->SetLabel(wxEmptyString);
			m_clienttransferuploadbutton->Enable();
			m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
		}
		else // "Download" button.
		{
			if (((m_game == GAME_DESCENT) && !g_FindDescentMSNfile(&m_extra_usermap, m_usermap))
				|| ((m_game == GAME_DESCENT2) && !g_FindDescentMN2file(&m_extra_usermap, m_usermap)))
			{
				g_SendCStringToSocket(m_client, "1:requestextradownload:");
			}

			g_SendCStringToSocket(m_client, "1:requestdownload:");
			m_transfer_filename = m_usermap;
			m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
			m_clienttransferstaticText->SetLabel(m_transfer_filename);
			m_clienttransferuploadbutton->Disable();
			m_clienttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
		}
	} // In the case of ABORTED, we should wait for the file transfer thread end.

	// Disable button only when it should be a "Download" button afterwards,
	// and there's no missing map.
	// In other words, enable when the current state isn't NONE (or PENDING ==> NONE),
	// or there's a missing map.

	m_clienttransferdynamicbutton->Enable((m_transfer_state != TRANSFERSTATE_NONE) ||
		(m_launch_usermap &&
		(((m_game == GAME_BLOOD) && (!wxFileExists(g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_DESCENT) && (!wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_DESCENT2) && (!wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))));
}

void ClientRoomFrame::OnTimeStampsCheck(wxCommandEvent& WXUNUSED(event))
{
	g_configuration->show_timestamps = m_clientroomtimestampscheckBox->GetValue();
	g_configuration->Save();
}
/*
void ClientRoomFrame::OnNatFreeCheck(wxCommandEvent& WXUNUSED(event))
{
m_allow_sounds = true;
g_configuration->gamelaunch_enable_natfree = m_clientroomnatfreecheckBox->GetValue();
g_configuration->Save();
}
*/
void ClientRoomFrame::AskToRename(const wxString& newname)
{
	wxArrayString l_str_list;
	size_t l_loop_var;
	if (newname == m_nickname)
	{
		AddMessage(wxT("* This is already your nickname."), false);
		if (g_configuration->play_snd_error)
			g_PlaySound(g_configuration->snd_error_file);
		return;
	}
	for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
		if (m_players_table.players[l_loop_var].nickname == newname)
		{
			AddMessage(wxT("* You can't rename yourself to the nickname of someone else."), false);
			if (g_configuration->play_snd_error)
				g_PlaySound(g_configuration->snd_error_file);
			return;
		}
		l_str_list.Add(wxT("requestrename"));
		l_str_list.Add(newname);
		g_SendStringListToSocket(m_client, l_str_list);
}

void ClientRoomFrame::DoEnterText(const wxString& text)
{
	wxArrayString l_str_list;
	l_str_list.Add(wxT("msg"));
	l_str_list.Add(text);
	g_SendStringListToSocket(m_client, l_str_list);
	AddMessage(m_nickname + wxT(": ") + text, true);
	if (m_allow_sounds && g_configuration->play_snd_send)
		g_PlaySound(g_configuration->snd_send_file);
}
/*
void ClientRoomFrame::OnInsertNewLine(wxCommandEvent& WXUNUSED(event))
{
m_clientinputtextCtrl->WriteText(wxT("\n"));
m_clientinputtextCtrl->SetFocus();
}
*/
void ClientRoomFrame::OnEnterText(wxCommandEvent& WXUNUSED(event))
{
	wxString l_text;
	wxArrayString l_str_list;
	size_t l_len;
	m_allow_sounds = true;
	if (m_in_room)
	{
		l_text = m_clientinputtextCtrl->GetValue();
		if (l_text.IsEmpty())
		{
			if (g_configuration->play_snd_error)
				g_PlaySound(g_configuration->snd_error_file);
		}
		else if (l_text[0] == wxT('/'))
		{
			l_len = l_text.Len();
			if (l_text == wxT("/clear"))
				m_clientoutputtextCtrl->Clear();
			else if (l_text == wxT("/selectprofile"))
			{
				if (m_game == GAME_CUSTOM && g_configuration->have_dosbox && g_configuration->custom_profile_list && g_configuration->have_custom_mpprofile)
					SelectCustomProfile(false);
				else if (m_game == GAME_CUSTOM && !g_configuration->have_dosbox)
					AddMessage(wxT("* You can't use this command when you haven't configured DOSBox."), false);
				else if (m_game == GAME_CUSTOM && !g_configuration->custom_profile_list)
					AddMessage(wxT("* You can't use this command when you haven't configured any custom DOS game profile."), false);
				else if (m_game == GAME_CUSTOM && !g_configuration->have_custom_mpprofile)
					AddMessage(wxT("* You can't use this command when you have only configured custom DOS game profile(s) for single-player mode only."), false);
				else
					AddMessage(wxT("* You can only use this command when the host has selected a custom DOS game."), false);
			}
			else if (l_text == wxT("/selectmod"))
			{
				if (!m_modname.IsEmpty() && ((m_game == GAME_DN3D && g_configuration->dn3dtcmod_profile_list) || (m_game == GAME_SW && g_configuration->swtcmod_profile_list)))
					SelectModProfile();
				else if ((m_game == GAME_DN3D && !g_configuration->dn3dtcmod_profile_list) || (m_game == GAME_SW && !g_configuration->swtcmod_profile_list))
					AddMessage(wxT("* You can't use this command when you haven't configured any TC/MOD profile for the game currently selected by the host (") + s_gamename + wxT(")."), false);
				else
					AddMessage(wxT("* You can only use this command when the host has selected a TC/MOD."), false);
			}
			else if ((l_len >= 7) && ((l_text.Left(6) == wxT("/nick ")) || (l_text.Left(6) == wxT("/name "))))
				AskToRename(l_text.Mid(6));
			else if (l_text == wxT("/wake"))
				g_SendCStringToSocket(m_client, "1:wake:");
			else if ((l_len >= 2) && (l_text[1] == wxT('/')))
				DoEnterText(l_text.Mid(1));
			else
				AddMessage(wxT("\n*** List of commands:\n\n/clear - Clear the text output.\n/help - Show this list of commands.\n/nick <name> - Rename yourself to <name>.\n/name <name> - Same as /nick.\n/wake - Wake the host's room, if allowed on the host side.\n"), false);
		}
		else
			DoEnterText(l_text);
	}
	else
	{
		AddMessage(wxT("* Can't parse text while not really in room."), false);
		if (g_configuration->play_snd_error)
			g_PlaySound(g_configuration->snd_error_file);
	}
	m_clientinputtextCtrl->Clear();
}

bool ClientRoomFrame::SelectCustomProfile(bool leave)
{
	bool l_customprofile_selected;

	CustomSelectDialog *m_customselect_dialog = new CustomSelectDialog(NULL, m_roomname);
	m_customselect_dialog->ShowModal();
	l_customprofile_selected = m_customselect_dialog->customprofile_selected;
	m_customselect_dialog->Destroy();

	if (l_customprofile_selected)
	{
		AddMessage(wxT("\nYou have selected the \"") + g_configuration->gamelaunch_customselect_profilename + wxT("\" custom DOS game profile (you can change it again with the \"/selectprofile\" command)."), false);

		g_configuration->custom_profile_current = g_configuration->custom_profile_list;

		while (g_configuration->custom_profile_current->profilename != g_configuration->gamelaunch_customselect_profilename)
			g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
	}
	else if (leave)
	{
		Close();
		return true;
	}
	else
		AddMessage(wxT("\nYou haven't selected any custom DOS game profile : you can still select it with the \"/selectprofile\" command."), false);
	return false;
}

void ClientRoomFrame::SelectModProfile()
{
	bool l_tcmodprofile_selected;

	TCMODSelectDialog *m_tcmodselect_dialog = new TCMODSelectDialog(NULL, m_game, m_modname, m_modconfile, m_moddeffile, m_showmodurl, m_modurl, m_modfiles);
	m_tcmodselect_dialog->ShowModal();
	l_tcmodprofile_selected = m_tcmodselect_dialog->tcmodprofile_selected;
	m_tcmodselect_dialog->Destroy();

	switch (m_game)
	{
		case GAME_DN3D:
			if (l_tcmodprofile_selected)
			{
				AddMessage(wxT("\nYou have selected the \"") + g_configuration->gamelaunch_dn3dtcmodselect_profilename + wxT("\" TC/MOD profile (you can change it again with the \"/selectmod\" command)."), false);

				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

				while (g_configuration->dn3dtcmod_profile_current->profilename != g_configuration->gamelaunch_dn3dtcmodselect_profilename)
					g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;
			}
			else
				AddMessage(wxT("\nYou haven't selected any TC/MOD profile : you can still select it with the \"/selectmod\" command."), false);

			break;

		case GAME_SW:
			if (l_tcmodprofile_selected)
			{
				AddMessage(wxT("\nYou have selected the \"") + g_configuration->gamelaunch_swtcmodselect_profilename + wxT("\" TC/MOD profile (you can change it again with the \"/selectmod\" command)."), false);

				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

				while (g_configuration->swtcmod_profile_current->profilename != g_configuration->gamelaunch_swtcmodselect_profilename)
					g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;
			}
			else
				AddMessage(wxT("\nYou haven't selected any TC/MOD profile : you can still select it with the \"/selectmod\" command."), false);

			break;
		default: ;
	}
}

void ClientRoomFrame::OnPlayerColorChoice(wxCommandEvent& WXUNUSED(event))
{
	if (m_srcport == SOURCEPORT_DUKE3DW)
		g_configuration->gamelaunch_dn3d_playercolnum = m_clientplayercolorchoice->GetSelection();
	else // SOURCEPORT_SWP
		g_configuration->gamelaunch_sw_playercolnum = m_clientplayercolorchoice->GetSelection();
	g_configuration->Save();
}

void ClientRoomFrame::OnMapSelectionClose(wxCommandEvent& WXUNUSED(event))
{
	wxString l_filename;
	wxFile file;
	char *fileptr;

	if (m_mapselection_dialog->m_response && (m_game == m_mapselection_dialog->m_game))
	{ // Second check is done in order to avoid a race condition!
		wxArrayString l_str_list;
		m_transfer_filename = m_mapselection_dialog->m_response_filename;
		m_transfer_request = TRANSFERREQUEST_UPLOAD;
		m_clienttransferstaticText->SetLabel(m_transfer_filename);
		m_clienttransferuploadbutton->Disable();
		m_clienttransferdynamicbutton->Enable();
		m_clienttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
		l_str_list.Add(wxT("requestupload"));
		l_str_list.Add(m_transfer_filename);

		switch (m_game)
		{
			case GAME_BLOOD:    l_filename = g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_DESCENT:  l_filename = g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_DESCENT2: l_filename = g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_DN3D:     l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_SW:       l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			default: ;
		}

		m_transfer_filesize = (size_t)wxFileName::GetSize(l_filename).ToULong();
		l_str_list.Add(wxString::Format(wxT("%ld"), (long)m_transfer_filesize));

		if (file.Access(l_filename, wxFile::read) && file.Open(l_filename, wxFile::read))
		{
			fileptr = new char[m_transfer_filesize];
			file.Read(fileptr, m_transfer_filesize);
			file.Close();

			l_str_list.Add(wxString::Format(wxT("%u"), (unsigned)crc32buf(fileptr, m_transfer_filesize)));

			delete[] fileptr;
		}
		else
			wxLogError(wxT("Can't open file for reading!"));

		g_SendStringListToSocket(m_client, l_str_list);
	}
	// If we start an upload, we should enable the new "Abort transfer" button.
	// If we don't start an upload, should we enable the "Download" button?
	// Only when a map file selected by the host, if any, doesn't exist.
	m_clienttransferdynamicbutton->Enable((m_mapselection_dialog->m_response && (m_game == m_mapselection_dialog->m_game)) ||
		(m_launch_usermap &&
		(((m_game == GAME_BLOOD) && (!wxFileExists(g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_DESCENT) && (!wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_DESCENT2) && (!wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))));
	m_mapselection_dialog->Destroy();
	m_mapselection_dialog = NULL;
}

void ClientRoomFrame::OnTransferThreadEnd(wxCommandEvent& WXUNUSED(event))
{
	wxString l_filename;
	wxFile file;
	char *fileptr;
	unsigned long filesize;
	DWORD l_crc32;
	wxArrayString l_str_list;
	bool l_no_download_completed = true;
	m_transfer_timer->Stop();
	delete m_transfer_timer;

	m_transfer_fp->Close();
	delete m_transfer_fp;
	m_transfer_fp = NULL;

	if (m_transfer_request == TRANSFERREQUEST_DOWNLOAD)
	{
		switch (m_game)
		{
			case GAME_BLOOD:    l_filename = g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_DESCENT:  l_filename = g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_DESCENT2: l_filename = g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_DN3D:     l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_SW:       l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			default: ;
		}

		filesize = (size_t)wxFileName::GetSize(l_filename).ToULong();

		if (file.Access(l_filename, wxFile::read) && file.Open(l_filename, wxFile::read))
		{
			fileptr = new char[filesize];
			file.Read(fileptr, filesize);
			file.Close();

			l_crc32 = crc32buf(fileptr, filesize);

			delete[] fileptr;
		}
		else
			wxLogError(wxT("Can't open file for reading!"));

		// Also make sure that the client hasn't left the room.
		if ((m_transfer_state == TRANSFERSTATE_BUSY) && (m_transfer_client != NULL)
			&& ((long)filesize == m_transfer_filesize) && l_crc32 == m_transfer_crc32)
		{
			l_no_download_completed = false;
			//    m_usermap = m_transfer_filename;
			AddMessage(wxT("* The \"") + m_transfer_filename + wxString::Format(wxT("\" file has been received successfully. (Size: %lu / CRC32: %08X)"), (long)m_transfer_filesize, (unsigned)m_transfer_crc32), true);

			l_str_list.Add(wxT("filetransfersuccessful"));
			l_str_list.Add(m_transfer_filename);
			g_SendStringListToSocket(m_client, l_str_list);

			if ((m_temp_in_game && m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT) ||
				(m_temp_in_game && !m_launch_usermap && ((m_game == GAME_DESCENT) || (m_game == GAME_DESCENT2))) ||
				(m_temp_in_game && m_launch_usermap &&
				(((m_game == GAME_DESCENT) && wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap) && g_FindDescentMSNfile(&m_extra_usermap, m_usermap))
				|| ((m_game == GAME_DESCENT2) && wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap) && g_FindDescentMN2file(&m_extra_usermap, m_usermap)))))
			{
				JoinGame(m_srcport);
			}
		}
		else
		{
			if (wxFileExists(l_filename))
				wxRemoveFile(l_filename);

			if ((long)filesize == m_transfer_filesize)
				wxMessageBox(wxT("The received \"") + m_transfer_filename + wxString::Format(wxT("\" file is corrupted :\n\nExpected CRC32: %08X\n   Found CRC32: %08X\n\nPlease try to download it again."), (unsigned)m_transfer_crc32, (unsigned)l_crc32),
				wxT("Corrupted file received"), wxOK|wxICON_EXCLAMATION);
		}
	}
	m_transfer_state = TRANSFERSTATE_NONE;

	if (m_transfer_client == NULL) // Close the room?
	{
		Destroy();
		return;
	}
	m_transfer_client = NULL;
	m_clienttransfergauge->SetValue(0);

	/*if (m_transfer_awaiting_download)
	{
	m_transfer_awaiting_download = false;
	g_SendCStringToSocket(m_client, "1:requestdownload:");
	m_transfer_filename = m_usermap;
	m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
	m_clienttransferstaticText->SetLabel(m_transfer_filename);
	}
	else
	{*/
	m_transfer_request = TRANSFERREQUEST_NONE;
	m_clienttransferstaticText->SetLabel(wxEmptyString);
	m_clienttransferuploadbutton->Enable();
	m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
	m_clienttransferdynamicbutton->Enable(l_no_download_completed && m_launch_usermap &&
		(((m_game == GAME_BLOOD) && (!wxFileExists(g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_DESCENT) && (!wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_DESCENT2) && (!wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
		|| ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap)))));
	//}
}

void ClientRoomFrame::OnFileTransferSocketEvent(wxSocketEvent& event)
{
	//wxSocketBase *l_sock = event.GetSocket();
	if (!m_transfer_client) // Shutdown?
		return;
	wxString l_filename;
	char l_buffer[17]; // strlen("1:readyforupload:")
	if (m_transfer_state == TRANSFERSTATE_PENDING)
	{
		switch (event.GetSocketEvent())
		{
			case wxSOCKET_CONNECTION: // Let's begin!
				if (m_transfer_request == TRANSFERREQUEST_DOWNLOAD)
				{
					m_transfer_client->Notify(false);

					switch (m_game)
					{
						case GAME_BLOOD:
							if (!wxDirExists(g_configuration->blood_maps_dir))
								wxFileName::Mkdir(g_configuration->blood_maps_dir, 0777, wxPATH_MKDIR_FULL);
							l_filename = g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;

							break;

						case GAME_DESCENT:
							if (!wxDirExists(g_configuration->descent_maps_dir))
								wxFileName::Mkdir(g_configuration->descent_maps_dir, 0777, wxPATH_MKDIR_FULL);
							l_filename = g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;

							break;

						case GAME_DESCENT2:
							if (!wxDirExists(g_configuration->descent2_maps_dir))
								wxFileName::Mkdir(g_configuration->descent2_maps_dir, 0777, wxPATH_MKDIR_FULL);
							l_filename = g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;

							break;

						case GAME_DN3D:
							if (!wxDirExists(g_configuration->dn3d_maps_dir))
								wxFileName::Mkdir(g_configuration->dn3d_maps_dir, 0777, wxPATH_MKDIR_FULL);
							l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;

							break;

						case GAME_SW:
							if (!wxDirExists(g_configuration->sw_maps_dir))
								wxFileName::Mkdir(g_configuration->sw_maps_dir, 0777, wxPATH_MKDIR_FULL);
							l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;

							break;
						default: ;
					}

					m_transfer_fp = new wxFile(l_filename, wxFile::write);
					if (!m_transfer_fp->IsOpened())
					{
						wxLogError(wxT("Can't open file for writing!"));
						delete m_transfer_fp;
						m_transfer_fp = NULL;
						m_transfer_client->Notify(true);
					}
					else
					{
						m_transfer_thread = new FileReceiveThread(m_transfer_client, this, m_transfer_fp,
							m_transfer_filesize, &m_transfer_state);
						if ( m_transfer_thread->Create() != wxTHREAD_NO_ERROR )
						{
							wxLogError(wxT("Can't create file-transfer thread!"));
							m_transfer_thread->Delete();
							m_transfer_fp->Close();
							delete m_transfer_fp;
							m_transfer_fp = NULL;
							m_transfer_client->Notify(true);
						}
						else
						{
							g_SendCStringToSocket(m_transfer_client, "1:readyfortransfer:");
							m_transfer_state = TRANSFERSTATE_BUSY;
							m_transfer_thread->Run();
							m_clienttransfergauge->SetRange(m_transfer_filesize);
							m_transfer_timer = new wxTimer(this, ID_CLIENTFILETRANSFERTIMER);
							m_transfer_timer->Start(500, false);
						}
					}
				}
				else
				{
					m_transfer_client->SetNotify(wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
					g_SendCStringToSocket(m_transfer_client, "1:readyfortransfer:");
				}
				break;
			case wxSOCKET_INPUT:
				m_transfer_client->Read(l_buffer, 17); // strlen("1:readyforupload:")
				if ((m_transfer_request == TRANSFERREQUEST_UPLOAD)
					&& (!memcmp(l_buffer, "1:readyforupload:", 17)))
				{
					m_transfer_client->Notify(false);

					switch (m_game)
					{
						case GAME_BLOOD:    l_filename = g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
						case GAME_DESCENT:  l_filename = g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
						case GAME_DESCENT2: l_filename = g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
						case GAME_DN3D:     l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
						case GAME_SW:       l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
						default: ;
					}

					m_transfer_fp = new wxFile(l_filename, wxFile::read);
					if (!m_transfer_fp->IsOpened())
					{
						wxLogError(wxT("Can't open file for reading!"));
						delete m_transfer_fp;
						m_transfer_fp = NULL;
						m_transfer_client->Notify(true);
					}
					else
					{
						m_transfer_thread = new FileSendThread(m_transfer_client, this, m_transfer_fp, &m_transfer_state);
						if ( m_transfer_thread->Create() != wxTHREAD_NO_ERROR )
						{
							wxLogError(wxT("Can't create file-transfer thread!"));
							m_transfer_thread->Delete();
							m_transfer_fp->Close();
							delete m_transfer_fp;
							m_transfer_fp = NULL;
							m_transfer_client->Notify(true);
						}
						else
						{
							m_transfer_state = TRANSFERSTATE_BUSY;
							m_transfer_thread->Run();
							m_clienttransfergauge->SetRange(m_transfer_filesize);
							m_transfer_timer = new wxTimer(this, ID_CLIENTFILETRANSFERTIMER);
							m_transfer_timer->Start(500, false);
						}
					}
				}
				break;
			case wxSOCKET_LOST:
				m_transfer_state = TRANSFERSTATE_NONE;
				m_transfer_request = TRANSFERREQUEST_NONE;
				m_transfer_client->Destroy();
				m_transfer_client = NULL;
				m_clienttransferstaticText->SetLabel(wxEmptyString);
				m_clienttransferuploadbutton->Enable();
				m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
				m_clienttransferdynamicbutton->Enable(m_launch_usermap &&
					(((m_game == GAME_BLOOD) && (!wxFileExists(g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap)))
					|| ((m_game == GAME_DESCENT) && (!wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap)))
					|| ((m_game == GAME_DESCENT2) && (!wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap)))
					|| ((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
					|| ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap)))));
				break;
			default: ;
		}
	}
}

void ClientRoomFrame::OnSocketEvent(wxSocketEvent& event)
{
	if (!m_client) // Shutdown?
		return;
	//wxSocketBase *l_sock = event.GetSocket();
	wxArrayString l_str_list;
	size_t l_num_of_items, l_loop_var, l_index, l_num_of_read_attempts;
	long l_temp_num;
	//bool l_script_is_ready;
	char l_short_buffer[MAX_COMM_TEXT_LENGTH];
	wxString l_temp_str, l_long_buffer;
	wxIPV4address l_addr;
	wxFile extra_usermap;
	unsigned long filesize, l_temp_ulong;
	const char * returnedCountry;

	switch (event.GetSocketEvent())
	{
/*		case wxSOCKET_CONNECTION:
			m_client->SetTimeout(1);
			m_timer = new wxTimer(this, ID_CLIENTTIMER);
			m_timer->Start(30000, wxTIMER_ONE_SHOT);
			l_str_list.Add(wxT("requestroominfo"));
			l_str_list.Add(YANG_STR_NET_VERSION);
			l_str_list.Add(wxT(m_nickname);
			g_SendStringListToSocket(m_client, l_str_list);
			AddMessage(wxT("Connected."));
			//    Show(true);
			//    if (g_manualjoin_dialog) // Should be.
			//      g_manualjoin_dialog->Destroy();
			break;*/
		case wxSOCKET_INPUT:
			l_num_of_read_attempts = 0;
			do
			{
				m_client->Read(l_short_buffer, MAX_COMM_TEXT_LENGTH);
				/*      if (m_client->Error())
				{
				AddMessage(wxString::Format(wxT("* A communication error has occured while trying to fetch data: %d"), (int)m_client->LastError()));
				if (m_allow_sounds && g_configuration->play_snd_error)
				g_PlaySound(g_configuration->snd_error_file);
				break;
				}*/
				l_temp_num = m_client->LastCount();
				if (l_temp_num < MAX_COMM_TEXT_LENGTH)
					l_num_of_read_attempts = MAX_COMM_READ_ATTEMPTS;
				else
					l_num_of_read_attempts++;
				l_long_buffer << wxString(l_short_buffer, wxConvUTF8, l_temp_num);
				l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
				while (l_num_of_items)
				{
					l_num_of_items = l_str_list.GetCount();

					if ((l_num_of_items == 2) && (l_str_list[0] == wxT("error")))
					{
						m_client->SetNotify(0);
						m_client->Notify(false);
						m_client->Destroy();
						m_client = NULL;
						if (m_timer)
						{
							m_timer->Stop();
							delete m_timer;
							m_timer = NULL;
						}
						if (m_ping_timer)
						{
							m_ping_timer->Stop();
							delete m_ping_timer;
							m_ping_timer = NULL;
						}
						if (m_pingrefresh_timer)
						{
							m_pingrefresh_timer->Stop();
							delete m_pingrefresh_timer;
							m_pingrefresh_timer = NULL;
						}
						if (m_transfer_client)
						{
							if (m_transfer_state == TRANSFERSTATE_BUSY)
								m_transfer_state = TRANSFERSTATE_ABORTED;
							else if (m_transfer_state == TRANSFERSTATE_PENDING)
							{
								m_transfer_client->Destroy();
								m_transfer_client = NULL;
							}
							//            m_transfer_awaiting_download = false;
						}
						if (m_password_dialog)
							break;
						wxMessageBox(l_str_list[1], wxT("Error reported by host"), wxOK|wxICON_EXCLAMATION);
						if (m_transfer_client)
						{
							m_transfer_client = NULL;
							Hide();
						}
						else
							Destroy();
						g_main_frame->ShowMainFrame();
						return;
					}
					else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("pingquery")))
					{
						if (!m_pingrefresh_timer->IsRunning()) // Maybe stopped after a game.
							m_pingrefresh_timer->Start(1000, wxTIMER_ONE_SHOT);
						l_str_list.RemoveAt(0);
						l_str_list.Insert(wxT("pingreply"), 0);
						g_SendStringListToSocket(m_client, l_str_list);
					}
					else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("pingreply")) && (l_str_list[1] == wxString::Format(wxT("%d"), (int)m_ping_id)))
					{
						ping.Pause();
						m_ping_timer->Stop();
						if (ping.Time() < m_min_ping)
							m_min_ping = ping.Time();

						if (ping.Time() > m_max_ping)
							m_max_ping = ping.Time();

						m_ping_times++;

						if (m_ping_times < 4)
						{
							m_ping_id = rand();
							l_str_list.Empty();
							l_str_list.Add(wxT("pingquery"));
							l_str_list.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
							g_SendStringListToSocket(m_client, l_str_list);
							m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
							ping.Start( 0 );
						}
						else
						{
							m_clientplayerslistCtrl->SetItem(0, COL_CLIENTPING, wxString::Format(wxT("%ld"), (long)(m_max_ping < 999 ? m_max_ping : 999)));

							if (m_max_ping < 999)
								m_clientplayerslistCtrl->SetItem(0, COL_CLIENTFLUX, wxString::Format(wxT("%c"), 177) + wxString::Format(wxT("%ld"), (long)(m_max_ping-m_min_ping <= 99 ? m_max_ping-m_min_ping : 99)));
							else
								m_clientplayerslistCtrl->SetItem(0, COL_CLIENTFLUX, wxEmptyString);

#if (defined __WXMAC__) || (defined __WXCOCOA__)
							m_clientplayerslistCtrl->RefreshItem(0);
#endif
						}
					}
					else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("checkpingquery")))
					{
						l_str_list.RemoveAt(0);
						l_str_list.Insert(wxT("checkpingreply"), 0);
						g_SendStringListToSocket(m_client, l_str_list);
					}
					else if ((l_num_of_items >= 10) && (l_num_of_items <= 22)
						&& (l_str_list[0] == wxT("info")))
					{
						UpdateGameSettings(l_str_list);

						if ((m_srcport == SOURCEPORT_DOSBLOODSW && (!g_configuration->have_dosbloodsw || !g_configuration->have_dosbox))
							|| (m_srcport == SOURCEPORT_DOSBLOODRG && (!g_configuration->have_dosbloodrg || !g_configuration->have_dosbox || (m_use_crypticpassage && !wxFileExists(((wxFileName)g_configuration->dosbloodrg_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE")))))
							|| (m_srcport == SOURCEPORT_DOSBLOODPP && (!g_configuration->have_dosbloodpp || !g_configuration->have_dosbox || (m_use_crypticpassage && !wxFileExists(((wxFileName)g_configuration->dosbloodpp_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE")))))
							|| (m_srcport == SOURCEPORT_DOSBLOODOU && (!g_configuration->have_dosbloodou || !g_configuration->have_dosbox || (m_use_crypticpassage && !wxFileExists(((wxFileName)g_configuration->dosbloodou_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE")))))
							|| (m_srcport == SOURCEPORT_DOSDESCENT && (!g_configuration->have_dosdescent || !g_configuration->have_dosbox))
							|| (m_srcport == SOURCEPORT_D1XREBIRTH && !g_configuration->have_d1xrebirth)
							|| (m_srcport == SOURCEPORT_DOSDESCENT2 && (!g_configuration->have_dosdescent2 || !g_configuration->have_dosbox))
							|| (m_srcport == SOURCEPORT_D2XREBIRTH && !g_configuration->have_d2xrebirth)
							|| (m_srcport == SOURCEPORT_DOSDUKESW && (!g_configuration->have_dosdukesw || !g_configuration->have_dosbox))
							|| (m_srcport == SOURCEPORT_DOSDUKERG && (!g_configuration->have_dosdukerg || !g_configuration->have_dosbox))
							|| (m_srcport == SOURCEPORT_DOSDUKEAE && (!g_configuration->have_dosdukeae || !g_configuration->have_dosbox))
							|| (m_srcport == SOURCEPORT_DUKE3DW && !g_configuration->have_duke3dw)
							|| (m_srcport == SOURCEPORT_EDUKE32 && !g_configuration->have_eduke32)
							|| (m_srcport == SOURCEPORT_NDUKE && !g_configuration->have_nduke)
							|| (m_srcport == SOURCEPORT_HDUKE && !g_configuration->have_hduke)
							|| (m_srcport == SOURCEPORT_XDUKE && !g_configuration->have_xduke)
							|| (m_srcport == SOURCEPORT_DOSSWSW && (!g_configuration->have_dosswsw || !g_configuration->have_dosbox))
							|| (m_srcport == SOURCEPORT_DOSSWRG && (!g_configuration->have_dosswrg || !g_configuration->have_dosbox))
							|| (m_srcport == SOURCEPORT_VOIDSW && !g_configuration->have_voidsw)
							|| (m_srcport == SOURCEPORT_SWP && !g_configuration->have_swp)
							|| (m_game == GAME_CUSTOM && (!g_configuration->custom_profile_list || !g_configuration->have_custom_mpprofile || !g_configuration->have_dosbox)))
						{
							if (m_in_room)
							{
								if ((m_srcport == SOURCEPORT_DOSBLOODSW || m_srcport == SOURCEPORT_DOSBLOODRG || m_srcport == SOURCEPORT_DOSBLOODPP || m_srcport == SOURCEPORT_DOSBLOODOU
									|| m_srcport == SOURCEPORT_DOSDESCENT || m_srcport == SOURCEPORT_DOSDESCENT2
									|| m_srcport == SOURCEPORT_DOSDUKESW || m_srcport == SOURCEPORT_DOSDUKERG || m_srcport == SOURCEPORT_DOSDUKEAE
									|| m_srcport == SOURCEPORT_DOSSWSW || m_srcport == SOURCEPORT_DOSSWRG
									|| m_game == GAME_CUSTOM)
									&& !g_configuration->have_dosbox)
									AddMessage(wxT("* WARNING: You haven't configured DOSBox for the DOS game currently selected by the host."), false);
								else if (m_game == GAME_CUSTOM && !g_configuration->custom_profile_list)
									AddMessage(wxT("* WARNING: You haven't configured any custom DOS game profile."), false);
								else if (m_game == GAME_CUSTOM && !g_configuration->have_custom_mpprofile)
									AddMessage(wxT("* WARNING: You have only configured custom DOS game profile(s) for single-player mode only."), false);
								else if ((m_srcport == SOURCEPORT_DOSBLOODRG && m_use_crypticpassage && !wxFileExists(((wxFileName)g_configuration->dosbloodrg_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE")))
									|| (m_srcport == SOURCEPORT_DOSBLOODPP && m_use_crypticpassage && !wxFileExists(((wxFileName)g_configuration->dosbloodpp_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE")))
									|| (m_srcport == SOURCEPORT_DOSBLOODOU && m_use_crypticpassage && !wxFileExists(((wxFileName)g_configuration->dosbloodou_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))))
									AddMessage(wxT("* WARNING: You haven't installed the Cryptic Passage add-on currently selected by the host."), false);
								else
									AddMessage(wxT("* WARNING: You haven't configured the game source port currently selected by the host."), false);

								if (m_allow_sounds && g_configuration->play_snd_error)
									g_PlaySound(g_configuration->snd_error_file);
							}
							else
							{
								m_client->SetNotify(0);
								m_client->Notify(false);
								m_client->Destroy();
								m_client = NULL;

								if (m_timer)
								{
									m_timer->Stop();
									delete m_timer;
									m_timer = NULL;
								}

								if (m_ping_timer)
								{
									m_ping_timer->Stop();
									delete m_ping_timer;
									m_ping_timer = NULL;
								}

								if (m_pingrefresh_timer)
								{
									m_pingrefresh_timer->Stop();
									delete m_pingrefresh_timer;
									m_pingrefresh_timer = NULL;
								}

								if (m_transfer_client)
								{
									if (m_transfer_state == TRANSFERSTATE_BUSY)
										m_transfer_state = TRANSFERSTATE_ABORTED;
									else if (m_transfer_state == TRANSFERSTATE_PENDING)
									{
										m_transfer_client->Destroy();
										m_transfer_client = NULL;
									}
									//                  m_transfer_awaiting_download = false;
								}

								if (m_password_dialog)
									break;

								if ((m_srcport == SOURCEPORT_DOSBLOODSW || m_srcport == SOURCEPORT_DOSBLOODRG || m_srcport == SOURCEPORT_DOSBLOODPP || m_srcport == SOURCEPORT_DOSBLOODOU
									|| m_srcport == SOURCEPORT_DOSDESCENT || m_srcport == SOURCEPORT_DOSDESCENT2
									|| m_srcport == SOURCEPORT_DOSDUKESW || m_srcport == SOURCEPORT_DOSDUKERG || m_srcport == SOURCEPORT_DOSDUKEAE
									|| m_srcport == SOURCEPORT_DOSSWSW || m_srcport == SOURCEPORT_DOSSWRG
									|| m_game == GAME_CUSTOM)
									&& !g_configuration->have_dosbox)
								{
									wxMessageBox(wxT("You haven't configured DOSBox.\n\nPlease go in the menu \"Settings -> Source ports\" to configure it."),
										wxT("DOSBox isn't configured"), wxOK|wxICON_EXCLAMATION);
								}
								else if (m_game == GAME_CUSTOM && !g_configuration->custom_profile_list)
								{
									wxMessageBox(wxT("You haven't configured any custom DOS game profile.\n\nPlease go in the menu \"Settings -> Source ports\" to configure it."),
										wxT("No custom DOS game profile configured"), wxOK|wxICON_EXCLAMATION);
								}
								else if (m_game == GAME_CUSTOM && !g_configuration->have_custom_mpprofile)
								{
									wxMessageBox(wxT("You have only configured custom DOS game profile(s) for single-player mode only.\n\nPlease go in the menu \"Settings -> Source ports\" to configure it."),
										wxT("No multiplayer custom DOS game profile configured"), wxOK|wxICON_EXCLAMATION);
								}
								else if (m_srcport == SOURCEPORT_DOSBLOODSW || (m_srcport == SOURCEPORT_DOSBLOODRG && !g_configuration->have_dosbloodrg) || (m_srcport == SOURCEPORT_DOSBLOODPP && !g_configuration->have_dosbloodpp) || (m_srcport == SOURCEPORT_DOSBLOODOU && !g_configuration->have_dosbloodou)
									|| m_srcport == SOURCEPORT_DOSDUKESW || m_srcport == SOURCEPORT_DOSDUKERG || m_srcport == SOURCEPORT_DOSDUKEAE
									|| m_srcport == SOURCEPORT_DOSSWSW || m_srcport == SOURCEPORT_DOSSWRG)
								{
									wxMessageBox(wxT("You haven't configured the selected DOS game: ") + s_gamename + wxT(' ') + s_srcport + wxT("\n\nPlease go in the menu \"Settings -> Source ports\" to configure it."),
										wxT("Game source port isn't configured"), wxOK|wxICON_EXCLAMATION);
								}
								else if ((m_srcport == SOURCEPORT_DOSBLOODRG && m_use_crypticpassage && !wxFileExists(((wxFileName)g_configuration->dosbloodrg_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE")))
									|| (m_srcport == SOURCEPORT_DOSBLOODPP && m_use_crypticpassage && !wxFileExists(((wxFileName)g_configuration->dosbloodpp_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE")))
									|| (m_srcport == SOURCEPORT_DOSBLOODOU && m_use_crypticpassage && !wxFileExists(((wxFileName)g_configuration->dosbloodou_exec).GetPath() + wxFILE_SEP_PATH + wxT("CRYPTIC.EXE"))))
								{
									wxMessageBox(wxT("You haven't installed the Cryptic Passage add-on with: ") + s_gamename + wxT(' ') + s_srcport + wxT("\n\nPlease make sure the \"CRYPTIC.EXE\" file is present along with the \"BLOOD.EXE\" file."),
										wxT("Cryptic Passage add-on isn't found"), wxOK|wxICON_EXCLAMATION);
								}
								else
								{
									wxMessageBox(wxT("You haven't configured the selected source port: ") + s_srcport + wxT("\n\nPlease go in the menu \"Settings -> Source ports\" to configure it."),
										wxT("Game source port isn't configured"), wxOK|wxICON_EXCLAMATION);
								}

								if (m_transfer_client)
								{
									m_transfer_client = NULL;
									Hide();
								}
								else
									Destroy();

								g_main_frame->ShowMainFrame();

								return;
							}
						}
						else if (!m_in_room)
						{
							m_timer->Start(30000, wxTIMER_ONE_SHOT);
							l_str_list.Empty();
							l_str_list.Add(wxT("join"));
							l_str_list.Add(YANG_STR_NET_VERSION);
							m_client->GetPeer(l_addr);
							l_str_list.Add(g_EncryptForCommText(l_addr.IPAddress(), COMM_KEY_IPLIST_FROM_CLIENT_TO_HOST));
							m_client->GetLocal(l_addr);
							l_str_list.Add(g_EncryptForCommText(l_addr.IPAddress(), COMM_KEY_IPLIST_FROM_CLIENT_TO_HOST));
							l_str_list.Add(wxString::Format(wxT("%d"), (int)g_configuration->game_port_number));
							//                l_str_list.Add(m_nickname);
							l_str_list.Add(*g_osdescription);
							g_SendStringListToSocket(m_client, l_str_list);
						}
						else if (m_game == GAME_CUSTOM && m_last_roomname != m_roomname)
						{
							SelectCustomProfile(false);
						}
					}
					else if ((!m_in_room) && (l_num_of_items % 6 == 2) && (l_num_of_items > 13)
						&& (l_str_list[0] == wxT("roominit")))
					{
						m_timer->Stop();
						delete m_timer;
						m_timer = NULL;
						l_loop_var = 2;
						while (l_loop_var < l_num_of_items)
						{
							l_str_list[l_loop_var+1] = g_DecryptFromCommText(l_str_list[l_loop_var+1], COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT);
							l_str_list[l_loop_var+2] = g_DecryptFromCommText(l_str_list[l_loop_var+2], COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT);

							l_str_list[l_loop_var+3].ToLong(&l_temp_num);

							m_players_table.Add(l_str_list[l_loop_var],
								l_str_list[l_loop_var+2], l_temp_num);
							l_index = m_players_table.num_players - 1;

							l_str_list[l_loop_var+5].ToLong(&l_temp_num);

							m_clientplayerslistCtrl->InsertItem(l_index, l_temp_num);

							if (g_geoip_db != NULL)
							{
								returnedCountry = GeoIP_country_code_by_addr(g_geoip_db,l_str_list[l_loop_var+1].mb_str(wxConvUTF8));
								if (returnedCountry != NULL)
									m_ImageList->Add(getFlag(returnedCountry));
								else
									m_ImageList->Add(wxIcon(unknown));

								m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTLOCATION, wxString(GeoIP_country_name_by_addr(g_geoip_db,l_str_list[l_loop_var+1].mb_str(wxConvUTF8)), wxConvUTF8), l_index+IMG_CLIENTNUMBER);
							}
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTNICKNAMES, m_players_table.players[l_index].nickname);
							if (g_configuration->theme_is_dark)
								m_clientplayerslistCtrl->SetItemTextColour(l_index, *wxGREEN);
							if (l_loop_var > 2)
								m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTPING, wxT("N/A"));
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTOS, l_str_list[l_loop_var+4]);
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTDETECTED_IPS, l_str_list[l_loop_var+1]);
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTINGAME_IPS, m_players_table.players[l_index].ingameip +
								wxString::Format(wxT(":%d"), (int)m_players_table.players[l_index].game_port_number));
							//              RefreshListCtrlSize();
							l_loop_var += 6;
						}

						m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTPING, wxT("0"));

						if (g_configuration->theme_is_dark)
						{
							m_def_listctrl_color = *wxGREEN;
							for (l_index = 0; (signed)l_index < m_clientplayerslistCtrl->GetItemCount(); l_index++)
								m_clientplayerslistCtrl->SetItemTextColour(l_index, m_def_listctrl_color);
						}
						// Default text color detection; Even though l_num_of_items should be more than 1, we check to avoid a crash.
						else if (m_clientplayerslistCtrl->GetItemCount() > 0)
							m_def_listctrl_color = m_clientplayerslistCtrl->GetItemTextColour(0);

						/*            m_ping_id = rand();
						l_str_list.Empty();
						l_str_list.Add(wxT("pingquery"));
						l_str_list.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
						g_SendStringListToSocket(m_client, l_str_list);
						m_ping_times = m_max_ping = 0;
						m_min_ping = 999;*/
						if (!m_temp_in_game) // Maybe there's an ongoing game, so don't ping for now.
							m_pingrefresh_timer->Start(1000, wxTIMER_ONE_SHOT);
						/*            m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
						ping.Start( 0 );*/

						AddMessage(wxT("For a list of commands, enter /help command."), false);

						m_in_room = true;
#ifdef ENABLE_UPNP
						if (g_configuration->enable_upnp)
							g_UPNP_Forward(g_configuration->game_port_number, false);
#endif
						s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);

						UpdateSettingsList();

						//            m_clientnewlinebutton->Enable();
						m_clientreadytoggleBtn->Enable();

						if (m_launch_usermap &&
							(((m_game == GAME_DESCENT) && !g_FindDescentMSNfile(&m_extra_usermap, m_usermap))
							|| ((m_game == GAME_DESCENT2) && !g_FindDescentMN2file(&m_extra_usermap, m_usermap))))
						{
							g_SendCStringToSocket(m_client, "1:requestextradownload:");
						}

						if (m_launch_usermap && !(m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT) &&
							(((m_game == GAME_BLOOD) && !wxFileExists(g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap))
							|| ((m_game == GAME_DESCENT) && !wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap))
							|| ((m_game == GAME_DESCENT2) && !wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap))
							|| ((m_game == GAME_DN3D) && !wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap))
							|| ((m_game == GAME_SW) && !wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap))))
						{
							g_SendCStringToSocket(m_client, "1:requestdownload:");
							m_transfer_filename = m_usermap;
							m_transfer_request = TRANSFERREQUEST_DOWNLOAD;
							m_clienttransferstaticText->SetLabel(m_transfer_filename);
							m_clienttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
							m_clienttransferdynamicbutton->Enable();
						}
						else
							m_clienttransferuploadbutton->Enable();

						if (m_game == GAME_CUSTOM)
						{
							if (SelectCustomProfile(true))
								return;
						}

						if ((m_temp_in_game && m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT) ||
							(m_temp_in_game && m_game == GAME_CUSTOM && l_str_list[1] == wxT("yes")) ||
							(m_temp_in_game && !m_launch_usermap && ((m_game == GAME_DESCENT) || (m_game == GAME_DESCENT2))) ||
							(m_temp_in_game && m_launch_usermap &&
							(((m_game == GAME_DESCENT) && wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap) && g_FindDescentMSNfile(&m_extra_usermap, m_usermap))
							|| ((m_game == GAME_DESCENT2) && wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap) && g_FindDescentMN2file(&m_extra_usermap, m_usermap)))))
						{
							JoinGame(m_srcport);
						}
					}
					else if ((l_num_of_items >= 5) && (l_str_list[0] == wxT("mod")))
					{
						UpdateModFilesInfo(l_str_list);

						if (!m_modname.IsEmpty() && m_last_modname != m_modname)
						{
							if ((m_game == GAME_DN3D && g_configuration->dn3dtcmod_profile_list) || (m_game == GAME_SW && g_configuration->swtcmod_profile_list))
								SelectModProfile();
							else
							{
								AddMessage(wxT("* WARNING: You haven't configured any TC/MOD profile for the game currently selected by the host (") + s_gamename + wxT(")."), false);
								if (m_showmodurl)
									AddMessage(wxT("* The host (") + m_players_table.players[0].nickname + wxT(") recommends downloading the TC/MOD at: ") + m_modurl, false);
							}
						}
					}
					// The following cases are only relevant while in room.
					else if (m_in_room)
					{
						if ((l_num_of_items == 3) && (l_str_list[0] == wxT("ready")))
						{
							l_str_list[2].ToLong(&l_temp_num);

							if ((l_temp_num >= 1) && ((unsigned long)l_temp_num < m_players_table.num_players))
							{
								if (l_str_list[1] == wxT("1"))
									m_clientplayerslistCtrl->SetItemColumnImage((unsigned long)l_temp_num, COL_CLIENTREADY, IMG_CLIENTACCEPT);
								else
									m_clientplayerslistCtrl->SetItemColumnImage((unsigned long)l_temp_num, COL_CLIENTREADY, IMG_CLIENTCANCEL);
							}
						}
						else if ((l_num_of_items == 3) && (l_str_list[0] == wxT("hostaddrupdate")))
						{
							l_str_list[1] = g_DecryptFromCommText(l_str_list[1], COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT);
							l_str_list[2] = g_DecryptFromCommText(l_str_list[2], COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT);

							m_players_table.players[0].ingameip = l_str_list[2];
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTDETECTED_IPS, l_str_list[1]);
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTINGAME_IPS, l_str_list[2] +
								wxString::Format(wxT(":%d"), (int)m_players_table.players[0].game_port_number));
							//              RefreshListCtrlSize();
						}
						else if ((l_num_of_items == 3) && (l_str_list[0] == wxT("msg")) && (l_str_list[1] != m_nickname))
						{
							AddMessage(l_str_list[1] + wxT(": ") + l_str_list[2], true);
							if (m_allow_sounds && g_configuration->play_snd_receive)
								g_PlaySound(g_configuration->snd_receive_file);
						}
						else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("newtitle")))
							SetTitle(l_str_list[1]);
						else if ((l_num_of_items == 3) && (l_str_list[0] == wxT("rename")))
						{
							l_index = m_players_table.FindIndexByNick(l_str_list[1]);
							if ((l_index != m_players_table.num_players))
							{
								AddMessage(wxT("* ") + m_players_table.players[l_index].nickname + wxT(" has renamed to ") + l_str_list[2] + wxT('.'), true);
								m_players_table.players[l_index].nickname = l_str_list[2];
								m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTNICKNAMES, l_str_list[2]);
								if (l_str_list[1] == m_nickname)
									m_nickname = l_str_list[2];
							}
						}
						else if ((l_num_of_items == 6) && (l_str_list[0] == wxT("joined")))
						{
							l_str_list[2] = g_DecryptFromCommText(l_str_list[2], COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT);
							l_str_list[3] = g_DecryptFromCommText(l_str_list[3], COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT);

							l_str_list[4].ToLong(&l_temp_num);

							m_players_table.Add(l_str_list[1], l_str_list[3], l_temp_num);
							l_index = m_players_table.num_players - 1;

							m_clientplayerslistCtrl->InsertItem(l_index, IMG_CLIENTCANCEL);

							if (g_geoip_db != NULL)
							{
								returnedCountry = GeoIP_country_code_by_addr(g_geoip_db,l_str_list[2].mb_str(wxConvUTF8));
								if (returnedCountry != NULL)
									m_ImageList->Add(getFlag(returnedCountry));
								else
									m_ImageList->Add(wxIcon(unknown));

								m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTLOCATION, wxString(GeoIP_country_name_by_addr(g_geoip_db,l_str_list[2].mb_str(wxConvUTF8)), wxConvUTF8), l_index+IMG_CLIENTNUMBER);
							}

							if (g_configuration->theme_is_dark)
								m_clientplayerslistCtrl->SetItemTextColour(l_index, *wxGREEN);
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTNICKNAMES, m_players_table.players[l_index].nickname);
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTPING, wxT("N/A"));
							m_clientplayerslistCtrl->SetItemTextColour(l_index, m_def_listctrl_color);
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTOS, l_str_list[5]);
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTDETECTED_IPS, l_str_list[2]);
							m_clientplayerslistCtrl->SetItem(l_index, COL_CLIENTINGAME_IPS, m_players_table.players[l_index].ingameip + wxT(':') + l_str_list[4]);
							//                RefreshListCtrlSize();
							s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);
							UpdateSettingsList();
							AddMessage(wxT("* ") + m_players_table.players[l_index].nickname + wxT(" has joined the room."), true);
							if (m_allow_sounds && g_configuration->play_snd_join)
								g_PlaySound(g_configuration->snd_join_file);
						}
						else if ((l_num_of_items == 2) &&
							((l_str_list[0] == wxT("left")) || (l_str_list[0] == wxT("disconnect"))))
						{
							l_temp_str = l_str_list[1];
							l_index = m_players_table.FindIndexByNick(l_temp_str);
							if (((l_index != m_players_table.num_players))
								&& (l_temp_str != m_nickname))
							{
								m_players_table.DelByIndex(l_index);

								m_clientplayerslistCtrl->DeleteItem(l_index);
								m_ImageList->Remove(l_index+IMG_CLIENTNUMBER);
								//                  RefreshListCtrlSize();

								for (l_loop_var = l_index; l_loop_var < m_players_table.num_players; l_loop_var++)
									m_clientplayerslistCtrl->SetItemColumnImage(l_loop_var, COL_CLIENTLOCATION, l_loop_var+IMG_CLIENTNUMBER);

								s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);
								UpdateSettingsList();
								if (l_str_list[0] == wxT("left"))
									AddMessage(wxT("* ") + l_temp_str + wxT(" has left the room."), true);
								else
									AddMessage(wxT("* ") + l_temp_str + wxT(" has got disconnected from the room."), true);
								if (m_allow_sounds && g_configuration->play_snd_leave)
									g_PlaySound(g_configuration->snd_leave_file);
							}
						}
						else if ((l_num_of_items == 2) && ((l_str_list[0] == wxT("kicked")) || (l_str_list[0] == wxT("banned")) || (l_str_list[0] == wxT("globanned"))))
						{
							l_temp_str = l_str_list[1];
							if (l_temp_str == m_nickname)
							{
								m_client->SetNotify(0);
								m_client->Notify(false);
								m_client->Destroy();
								m_client = NULL;
								if (m_ping_timer)
								{
									m_ping_timer->Stop();
									delete m_ping_timer;
									m_ping_timer = NULL;
								}
								if (m_pingrefresh_timer)
								{
									m_pingrefresh_timer->Stop();
									delete m_pingrefresh_timer;
									m_pingrefresh_timer = NULL;
								}
								if (m_transfer_client)
								{
									if (m_transfer_state == TRANSFERSTATE_BUSY)
										m_transfer_state = TRANSFERSTATE_ABORTED;
									else if (m_transfer_state == TRANSFERSTATE_PENDING)
									{
										m_transfer_client->Destroy();
										m_transfer_client = NULL;
									}
									//                    m_transfer_awaiting_download = false;
								}
								if (l_str_list[0] == wxT("kicked"))
									wxMessageBox(wxT("You've been kicked from the room."), wxT("Kick notification"), wxOK);
								else if (l_str_list[0] == wxT("banned"))
									wxMessageBox(wxT("You've been banned from the room."), wxT("Ban notification"), wxOK);
								else // Global ban
									wxMessageBox(wxT("You've been automatically kicked from the room, due to a global YANG network ban."), wxT("Auto-kick notification"), wxOK);
								if (m_transfer_client)
								{
									m_transfer_client = NULL;
									Hide();
								}
								else
									Destroy();
								g_main_frame->ShowMainFrame();
								return;
							}
							else
							{
								l_index = m_players_table.FindIndexByNick(l_temp_str);
								if (l_index != m_players_table.num_players)
								{
									m_players_table.DelByIndex(l_index);

									m_clientplayerslistCtrl->DeleteItem(l_index);
									m_ImageList->Remove(l_index+IMG_CLIENTNUMBER);
									//                    RefreshListCtrlSize();

									for (l_loop_var = l_index; l_loop_var < m_players_table.num_players; l_loop_var++)
										m_clientplayerslistCtrl->SetItemColumnImage(l_loop_var, COL_CLIENTLOCATION, l_loop_var+IMG_CLIENTNUMBER);

									s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);
									UpdateSettingsList();
									if (l_str_list[0] == wxT("kicked"))
										AddMessage(wxT("* ") + l_temp_str + wxT(" has been kicked."), true);
									else if (l_str_list[0] == wxT("banned"))
										AddMessage(wxT("* ") + l_temp_str + wxT(" has been banned."), true);
									else // Global ban
										AddMessage(wxT("* ") + l_temp_str + wxT(" has been automatically kicked, due to a global ban."), true);
									if (m_allow_sounds && g_configuration->play_snd_leave)
										g_PlaySound(g_configuration->snd_leave_file);
								}
							}
						}
						else if ((l_num_of_items == 5) && (l_str_list[0] == wxT("extradownload")) &&
							((m_game == GAME_DESCENT) || (m_game == GAME_DESCENT2)))
						{
							l_str_list[2].ToULong(&filesize);
							l_str_list[3].ToULong(&l_temp_ulong);

							if (l_str_list[4].Len() >= filesize && crc32buf(l_str_list[4].char_str(wxConvUTF8), filesize) == l_temp_ulong)
							{
								if (m_game == GAME_DESCENT)
								{
									if (!wxDirExists(g_configuration->descent_maps_dir))
										wxFileName::Mkdir(g_configuration->descent_maps_dir, 0777, wxPATH_MKDIR_FULL);
									l_temp_str = g_configuration->descent_maps_dir + wxFILE_SEP_PATH + l_str_list[1];
								}
								else
								{
									if (!wxDirExists(g_configuration->descent2_maps_dir))
										wxFileName::Mkdir(g_configuration->descent2_maps_dir, 0777, wxPATH_MKDIR_FULL);
									l_temp_str = g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + l_str_list[1];
								}

								if (extra_usermap.Open(l_temp_str, wxFile::write))
								{
									extra_usermap.Write(l_str_list[4].mb_str(wxConvUTF8), filesize);
									extra_usermap.Close();

									AddMessage(wxT("* The \"") + l_str_list[1] + wxString::Format(wxT("\" file has been received successfully. (Size: %lu / CRC32: %08X)"), filesize, (unsigned)l_temp_ulong), true);

									l_temp_str = l_str_list[1];
									l_str_list.Empty();
									l_str_list.Add(wxT("filetransfersuccessful"));
									l_str_list.Add(l_temp_str);
									g_SendStringListToSocket(m_client, l_str_list);

									if ((m_temp_in_game && m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT) ||
										(m_temp_in_game && !m_launch_usermap && ((m_game == GAME_DESCENT) || (m_game == GAME_DESCENT2))) ||
										(m_temp_in_game && m_launch_usermap &&
										(((m_game == GAME_DESCENT) && wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap) && g_FindDescentMSNfile(&m_extra_usermap, m_usermap))
										|| ((m_game == GAME_DESCENT2) && wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap) && g_FindDescentMN2file(&m_extra_usermap, m_usermap)))))
									{
										JoinGame(m_srcport);
									}
								}
								else
									wxLogError(wxT("Can't open file for writing!"));
							}
							else if (wxMessageBox(wxT("The received \"") + l_str_list[1] + wxT("\" file is corrupted :\nThis may be because the filesize is abnormally too large.\n\nDo you want to try downloading it again ?"),
								wxT("Corrupted file received"), wxYES_NO|wxICON_EXCLAMATION) == wxYES)
							{
								g_SendCStringToSocket(m_client, "1:requestextradownload:");
							}
						}
						else if ((l_num_of_items == 3) && (l_str_list[0] == wxT("downloadaccept")) &&
							(m_transfer_state == TRANSFERSTATE_NONE) && (m_transfer_request == TRANSFERREQUEST_DOWNLOAD))
						{
							if (l_str_list[1].ToLong(&l_temp_num))
								if (l_temp_num >= 0)
								{
									m_transfer_filesize = l_temp_num;
									l_str_list[2].ToULong(&l_temp_ulong);
									m_transfer_crc32 = l_temp_ulong;
									m_transfer_state = TRANSFERSTATE_PENDING;
									m_client->GetPeer(l_addr);
									m_transfer_client = new wxSocketClient();
									m_transfer_client->SetEventHandler(*this, ID_CLIENTFILETRANSFERSOCKET);
									m_transfer_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
									m_transfer_client->SetTimeout(1);
									m_transfer_client->SetNotify(wxSOCKET_CONNECTION_FLAG | wxSOCKET_LOST_FLAG);
									m_transfer_client->Notify(true);
									m_transfer_client->Connect(l_addr, false);
								}
						}
						else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("uploadaccept")) &&
							(m_transfer_state == TRANSFERSTATE_NONE) && (m_transfer_request == TRANSFERREQUEST_UPLOAD))
						{
							m_transfer_state = TRANSFERSTATE_PENDING;
							m_client->GetPeer(l_addr);
							m_transfer_client = new wxSocketClient();
							m_transfer_client->SetEventHandler(*this, ID_CLIENTFILETRANSFERSOCKET);
							m_transfer_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
							m_transfer_client->SetTimeout(1);
							m_transfer_client->SetNotify(wxSOCKET_CONNECTION_FLAG | wxSOCKET_LOST_FLAG);
							m_transfer_client->Notify(true);
							m_transfer_client->Connect(l_addr, false);
						}
						else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("requestrefuse")) &&
							((m_transfer_state == TRANSFERSTATE_PENDING) ||
							((m_transfer_state == TRANSFERSTATE_NONE) && (m_transfer_request != TRANSFERREQUEST_NONE))))
						{
							if (m_transfer_state == TRANSFERSTATE_PENDING)
							{
								m_transfer_state = TRANSFERSTATE_NONE;
								m_transfer_client->Destroy();
								m_transfer_client = NULL;
							}
							m_transfer_request = TRANSFERREQUEST_NONE;
							m_clienttransferstaticText->SetLabel(wxEmptyString);
							m_clienttransferuploadbutton->Enable();
							m_clienttransferdynamicbutton->SetLabel(wxT("Download map"));
							m_clienttransferdynamicbutton->Enable(m_launch_usermap &&
								(((m_game == GAME_BLOOD) && (!wxFileExists(g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap)))
								|| ((m_game == GAME_DESCENT) && (!wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap)))
								|| ((m_game == GAME_DESCENT2) && (!wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap)))
								|| ((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
								|| ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap)))));
						}
						else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("filetransfersuccessful")))
						{
							AddMessage(wxT("* ") + m_players_table.players[0].nickname + wxT(" has received the \"") + l_str_list[1] + wxT("\" file successfully."), true);
						}
						else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("aborttransfer"))
							&& (m_transfer_state == TRANSFERSTATE_BUSY))
							m_transfer_state = TRANSFERSTATE_ABORTED;
						else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("launch")))
						{
							if (m_game != GAME_CUSTOM || (m_game == GAME_CUSTOM && g_configuration->custom_profile_list && g_configuration->have_custom_mpprofile && g_configuration->have_dosbox))
								LaunchGame();
							else
							{
								for (size_t l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
									m_clientplayerslistCtrl->SetItemColumnImage(l_loop_var, COL_CLIENTREADY, IMG_CLIENTCANCEL);

								m_clientreadytoggleBtn->SetValue(false);
							}
						}
						else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("shutdown")))
						{
							m_client->SetNotify(0);
							m_client->Notify(false);
							m_client->Destroy();
							m_client = NULL;
							if (m_ping_timer)
							{
								m_ping_timer->Stop();
								delete m_ping_timer;
								m_ping_timer = NULL;
							}
							if (m_pingrefresh_timer)
							{
								m_pingrefresh_timer->Stop();
								delete m_pingrefresh_timer;
								m_pingrefresh_timer = NULL;
							}
							if (m_transfer_client)
							{
								if (m_transfer_state == TRANSFERSTATE_BUSY)
									m_transfer_state = TRANSFERSTATE_ABORTED;
								else if (m_transfer_state == TRANSFERSTATE_PENDING)
								{
									m_transfer_client->Destroy();
									m_transfer_client = NULL;
								}
								//                  m_transfer_awaiting_download = false;
							}
							wxMessageBox(wxT("The host has closed the room."), wxT("Closed room notification"), wxOK|wxICON_INFORMATION);
							if (m_transfer_client)
							{
								m_transfer_client = NULL;
								Hide();
							}
							else
								Destroy();
							g_main_frame->ShowMainFrame();
							return;
						}
					}
					// Only when not in room.
					else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("requestpass")))
					{
						m_timer->Stop();

						m_password_dialog = true;
						l_temp_str = wxGetPasswordFromUser(wxT("Please enter a password for this room."), wxT("Room is password-protected."), wxEmptyString, this);
						m_password_dialog = false;

						if (m_client)
						{
							if (l_temp_str.IsEmpty())
								Close();
							else
							{
								m_timer->Start(30000, wxTIMER_ONE_SHOT);
								l_str_list.Empty();
								l_str_list.Add(wxT("password"));
								l_str_list.Add(l_temp_str);
								g_SendStringListToSocket(m_client, l_str_list);
							}
						}
						else
						{
							wxMessageBox(wxT("It looks like you've been disconnected from the host for any\npossible reason while staying with the password dialog."), wxT("Disconnected from host"), wxOK);
							Destroy();
							g_main_frame->ShowMainFrame();
						}
					}
					l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
				}
			}
			while (l_num_of_read_attempts < MAX_COMM_READ_ATTEMPTS);
			break;
		case wxSOCKET_LOST:
			m_client->SetNotify(0);
			m_client->Notify(false);
			m_client->Destroy();
			m_client = NULL;
			if (m_timer)
			{
				m_timer->Stop();
				delete m_timer;
				m_timer = NULL;
			}
			if (m_ping_timer)
			{
				m_ping_timer->Stop();
				delete m_ping_timer;
				m_ping_timer = NULL;
			}
			if (m_pingrefresh_timer)
			{
				m_pingrefresh_timer->Stop();
				delete m_pingrefresh_timer;
				m_pingrefresh_timer = NULL;
			}
			/*    wxMessageBox(wxT("Connection timed out or lost."), wxEmptyString, wxOK);
			if (g_manualjoin_dialog)
			g_manualjoin_dialog->Enable();
			else
			g_main_frame->Show();*/
			if (m_transfer_client)
			{
				if (m_transfer_state == TRANSFERSTATE_BUSY)
					m_transfer_state = TRANSFERSTATE_ABORTED;
				else if (m_transfer_state == TRANSFERSTATE_PENDING)
				{
					m_transfer_client->Destroy();
					m_transfer_client = NULL;
				}
				//      m_transfer_awaiting_download = false;
			}
			if (m_password_dialog)
				break;
			wxMessageBox(wxT("Disconnected from host."), wxEmptyString, wxOK);
			if (m_transfer_client)
			{
				m_transfer_client = NULL;
				Hide();
			}
			else
				Destroy();
			g_main_frame->ShowMainFrame();
			break;
		default: ;
	}
}

void ClientRoomFrame::JoinGame(SourcePortType srcport)
{
	wxString l_temp_str;

	if (srcport == SOURCEPORT_DOSDESCENT || srcport == SOURCEPORT_DOSDESCENT2)
		l_temp_str = wxT("The game has already been launched by the host, and the players are already in-game.\n\nDo you want to launch the game, so that you can join them ?\n\n(to join the game, once you are in the main menu,\ngo to the \"Multiplayer\" menu, then choose \"Join a network game\")");
	else if (srcport == SOURCEPORT_D1XREBIRTH || srcport == SOURCEPORT_D2XREBIRTH)
		l_temp_str = wxT("The game has already been launched by the host, and the players are already in-game.\n\nDo you want to launch the game, so that you can join them ?\n\n(to join the game, once you are in the main menu,\ngo to the \"Multiplayer\" menu, then choose \"Join game manually\")");
	else
		l_temp_str = wxT("The game has already been launched by the host, and the players are already in-game.\n\nDo you want to launch the game, so that you can join them ?");

	if (wxMessageBox(l_temp_str, wxT("Players already in-game"), wxYES_NO|wxICON_INFORMATION) == wxYES)
		LaunchGame();
}

void ClientRoomFrame::LaunchGame()
{
	bool l_script_is_ready;

#ifdef __WXMSW__
	wxString l_temp_str = *g_launcher_user_profile_path + wxT("\\yang.bat");
#elif (defined __WXMAC__) || (defined __WXCOCOA__)
	wxString l_temp_str = *g_launcher_user_profile_path + wxT("/yang.command");
#else
	wxString l_temp_str = *g_launcher_user_profile_path + wxT("/yang.sh");
#endif
	m_allow_sounds = !(g_configuration->mute_sounds_while_ingame);

	l_script_is_ready = g_MakeLaunchScript(
		true,
		l_temp_str,
		&m_players_table,
		m_players_table.FindIndexByNick(m_nickname),
		m_game,
		m_srcport,
		m_use_crypticpassage,
		!(m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT) ? m_epimap_num : 0,
		m_usermap,
		!(m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT) ? m_launch_usermap : false,
		m_bloodgametype,
		m_dn3dgametype,
		m_swgametype,
		0,
		false,
		m_skill,
		m_blood_monster,
		m_blood_weapon,
		m_blood_item,
		m_blood_respawn,
		m_dn3dspawn,
		false,
		wxEmptyString,
		m_recorddemo,
		m_demofilename,
		m_connectiontype,
		m_rxdelay,
		!m_modname.IsEmpty() && ((m_game == GAME_DN3D && g_configuration->dn3dtcmod_profile_list) || (m_game == GAME_SW && g_configuration->swtcmod_profile_list)),
		m_args_for_all);

	if (l_script_is_ready)
	{
		m_pingrefresh_timer->Stop(); // Save bandwidth.
		m_clientplayerslistCtrl->SetItem(0, COL_CLIENTPING, wxEmptyString);
		m_clientplayerslistCtrl->SetItem(0, COL_CLIENTFLUX, wxEmptyString);

		if (!m_temp_in_game && ((m_srcport == SOURCEPORT_EDUKE32 && m_connectiontype == CONNECTIONTYPE_SERVERCLIENT) || (m_game == GAME_CUSTOM && m_connectiontype)))
			wxSleep(3);

		AddMessage(wxT("* Launching game..."), true);
#ifdef __WXMSW__
		wxExecute(wxT('"') + l_temp_str + wxT('"'));
#else
		wxExecute(g_configuration->terminal_fullcmd + wxT(" \"") + l_temp_str + wxT('"'));
#endif
	}

	for (size_t l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
		m_clientplayerslistCtrl->SetItemColumnImage(l_loop_var, COL_CLIENTREADY, IMG_CLIENTCANCEL);

	m_clientreadytoggleBtn->SetValue(false);
}

void ClientRoomFrame::OnLeaveRoom(wxCommandEvent& WXUNUSED(event))
{
	Close();
}

void ClientRoomFrame::OnShowRoomsList(wxCommandEvent& WXUNUSED(event))
{
	m_mainframe->Show();
	m_mainframe->Raise();
	m_mainframe->RefreshList();
}

void ClientRoomFrame::OnOpenChatOutputStyle(wxCommandEvent& WXUNUSED(event))
{
	if (m_lookandfeel_dialog->IsShown())
		m_lookandfeel_dialog->Raise();
	else
		m_lookandfeel_dialog->ShowDialog();
}

void ClientRoomFrame::OnClientPressReady(wxCommandEvent& WXUNUSED(event))
{
	wxArrayString l_str_list;
	l_str_list.Add(wxT("ready"));
	l_str_list.Add(wxString::Format(wxT("%d"), (int)m_clientreadytoggleBtn->GetValue()));
	g_SendStringListToSocket(m_client, l_str_list);

	m_clientplayerslistCtrl->SetItemColumnImage(m_players_table.FindIndexByNick(m_nickname), COL_CLIENTREADY, m_clientreadytoggleBtn->GetValue());
}

void ClientRoomFrame::SetChatStyle()
{
	wxFont l_font;
	wxColour l_text, l_back;
	YANGFrame* l_frame = new YANGFrame(NULL, wxID_ANY, wxEmptyString);
	YANGPanel* l_panel = new YANGPanel( l_frame, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	YANGTextCtrl* l_textctrl = new YANGTextCtrl(l_panel, wxID_ANY);
	if (g_configuration->use_custom_chat_text_layout)
	{
		l_font.SetFaceName(g_configuration->chat_font);
		l_font.SetPointSize(g_configuration->chat_font_size);
	}
	else
		l_font = l_textctrl->GetFont();
	if (g_configuration->use_custom_chat_text_colors)
	{
		l_text = (wxColour(g_configuration->chat_textcolor_red,
			g_configuration->chat_textcolor_green,
			g_configuration->chat_textcolor_blue));
		l_back = (wxColour(g_configuration->chat_backcolor_red,
			g_configuration->chat_backcolor_green,
			g_configuration->chat_backcolor_blue));
	}
	else
	{
		l_text = l_textctrl->GetForegroundColour();
		l_back = l_textctrl->GetBackgroundColour();
	}
	m_clientoutputtextCtrl->SetForegroundColour(l_text);
	m_clientoutputtextCtrl->SetBackgroundColour(l_back);
	m_clientoutputtextCtrl->SetFont(l_font);
#ifdef __WXMSW__
	m_clientoutputtextCtrl->SetDefaultStyle(wxTextAttr(l_text, l_back, l_font));
	m_clientoutputtextCtrl->SetStyle(0, m_clientoutputtextCtrl->GetLastPosition(), wxTextAttr(l_text, l_back, l_font));
#endif
	m_clientoutputtextCtrl->SetValue(m_clientoutputtextCtrl->GetValue());
	m_clientinputtextCtrl->SetForegroundColour(l_text);
	m_clientinputtextCtrl->SetBackgroundColour(l_back);
	m_clientinputtextCtrl->SetFont(l_font);
	m_clientinputtextCtrl->SetValue(m_clientinputtextCtrl->GetValue());
	m_clientinputtextCtrl->Refresh();

	l_frame->Destroy();
}

void ClientRoomFrame::OnLookAndFeelClose(wxCommandEvent& WXUNUSED(event))
{
	SetChatStyle();
}

void ClientRoomFrame::OnTimer(wxTimerEvent& WXUNUSED(event))
{
	if (m_timer != NULL)
	{
		delete m_timer;
		if (m_ping_timer)
		{
			m_ping_timer->Stop();
			delete m_ping_timer;
			m_ping_timer = NULL;
		}
		if (m_pingrefresh_timer)
		{
			m_pingrefresh_timer->Stop();
			delete m_pingrefresh_timer;
			m_pingrefresh_timer = NULL;
		}
		if (m_client)
		{
			m_client->Destroy();
			m_client = NULL;
		}
		wxMessageBox(wxT("Timed out while waiting for an appropriate response from the host."),
			wxEmptyString, wxOK);
		Destroy();
		g_main_frame->ShowMainFrame();
	}
}

void ClientRoomFrame::OnPingTimer(wxTimerEvent& WXUNUSED(event))
{
	if (m_ping_timer != NULL)
	{
		m_clientplayerslistCtrl->SetItem(0, COL_CLIENTPING, wxT("999"));
		m_clientplayerslistCtrl->SetItem(0, COL_CLIENTFLUX, wxEmptyString);
#if (defined __WXMAC__) || (defined __WXCOCOA__)
		m_clientplayerslistCtrl->RefreshItem(0);
#endif
	}
}

void ClientRoomFrame::OnPingRefreshTimer(wxTimerEvent& WXUNUSED(event))
{
	wxArrayString l_str_ping;

	if (m_pingrefresh_timer != NULL)
	{
		m_pingrefresh_timer->Start(10000, wxTIMER_ONE_SHOT);
		m_ping_id = rand();
		l_str_ping.Empty();
		l_str_ping.Add(wxT("pingquery"));
		l_str_ping.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
		g_SendStringListToSocket(m_client, l_str_ping);
		m_ping_times = m_max_ping = 0;
		m_min_ping = 999;
		m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
		ping.Start( 0 );
	}
}

void ClientRoomFrame::OnFileTransferTimer(wxTimerEvent& WXUNUSED(event))
{
	wxFileOffset l_position;
	if (m_transfer_fp)
	{
		l_position = m_transfer_fp->Tell();
		m_clienttransfergauge->SetValue(l_position);
		// Temporary wrapper for a possible bug with MinGW?
#if (defined __WXMSW__) && (wxCHECK_VERSION(2, 8, 9)) && (!wxCHECK_VERSION(2, 8, 10))
		m_clienttransferstaticText->SetLabel(m_transfer_filename + wxString::Format(wxT(": %lld / "), (long long)l_position)
			+ wxString::Format(wxT("%lld"), (long long)m_clienttransfergauge->GetRange()));
#else
		m_clienttransferstaticText->SetLabel(m_transfer_filename + wxString::Format(wxT(": %lld / %lld"), (long long)l_position, (long long)m_clienttransfergauge->GetRange()));
#endif
	}
}

void ClientRoomFrame::OnCloseWindow(wxCloseEvent& WXUNUSED(event))
{
#ifdef ENABLE_UPNP
	if (m_in_room && g_configuration->enable_upnp)
		g_UPNP_UnForward(g_configuration->game_port_number, false);
#endif
	if (m_timer)
	{
		m_timer->Stop();
		delete m_timer;
		m_timer = NULL;
	}
	if (m_transfer_client)
	{
		if (m_transfer_state == TRANSFERSTATE_BUSY)
		{
			m_transfer_state = TRANSFERSTATE_ABORTED;
			g_SendCStringToSocket(m_client, "1:aborttransfer:");
			m_transfer_client = NULL;
			//    Hide();
		}
		else if (m_transfer_state == TRANSFERSTATE_PENDING)
		{
			m_transfer_client->Destroy();
			m_transfer_client = NULL;
			Destroy();
		}
		//  m_transfer_awaiting_download = false;
	}
	else
		Destroy();
	if (m_ping_timer)
	{
		m_ping_timer->Stop();
		delete m_ping_timer;
		m_ping_timer = NULL;
	}
	if (m_pingrefresh_timer)
	{
		m_pingrefresh_timer->Stop();
		delete m_pingrefresh_timer;
		m_pingrefresh_timer = NULL;
	}

	g_configuration->client_col_ready_len = m_clientplayerslistCtrl->GetColumnWidth(COL_CLIENTREADY);
	g_configuration->client_col_loc_len = m_clientplayerslistCtrl->GetColumnWidth(COL_CLIENTLOCATION);
	g_configuration->client_col_nick_len = m_clientplayerslistCtrl->GetColumnWidth(COL_CLIENTNICKNAMES);
	g_configuration->client_col_ping_len = m_clientplayerslistCtrl->GetColumnWidth(COL_CLIENTPING);
	g_configuration->client_col_flux_len = m_clientplayerslistCtrl->GetColumnWidth(COL_CLIENTFLUX);
	g_configuration->client_col_os_len = m_clientplayerslistCtrl->GetColumnWidth(COL_CLIENTOS);
	g_configuration->client_col_detectedip_len = m_clientplayerslistCtrl->GetColumnWidth(COL_CLIENTDETECTED_IPS);
	g_configuration->client_col_ingameip_len = m_clientplayerslistCtrl->GetColumnWidth(COL_CLIENTINGAME_IPS);
	g_configuration->client_col_settings_len = m_clientsettingslistCtrl->GetColumnWidth(0);
	g_configuration->client_col_value_len = m_clientsettingslistCtrl->GetColumnWidth(1);
	g_configuration->client_is_maximized = IsMaximized();
	Hide();
	/*Maximize(false);
	Layout();*/
	if (!g_configuration->client_is_maximized)
		GetSize(&g_configuration->client_win_width, &g_configuration->client_win_height);
	g_configuration->Save();

	if (m_client)
	{
		g_SendCStringToSocket(m_client, "1:leave:");
		m_client->Destroy();
		m_client = NULL;
	}
	g_main_frame->ShowMainFrame();
}
