/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/filename.h>
#include <wx/log.h>
#include <wx/msgdlg.h>
#include <wx/sizer.h>
#include <wx/socket.h>
#include <wx/textfile.h>
#include <wx/mstream.h>
#include <wx/image.h>
#endif

#include "host_frame.h"
#include "config.h"
#include "comm_txt.h"
#include "yang.h"
#include "theme.h"
#include "yang.xpm"
#include "unknown.xpm"
#include "countryflags.h"
#include "yang_images.h"

BEGIN_EVENT_TABLE(HostedRoomFrame, YANGFrame)
//EVT_SIZE(HostedRoomFrame::OnResizeWindow)
//EVT_MAXIMIZE(HostedRoomFrame::OnMaximize)

//EVT_BUTTON(ID_HOSTADDNEWLINE, HostedRoomFrame::OnInsertNewLine)
EVT_BUTTON(wxID_EXIT, HostedRoomFrame::OnCloseRoom)
EVT_BUTTON(ID_HOSTSHOWROOMSLIST, HostedRoomFrame::OnShowRoomsList)
EVT_BUTTON(ID_HOSTCHATOUTPUTSTYLE, HostedRoomFrame::OnOpenChatOutputStyle)
EVT_BUTTON(ID_HOSTKICK, HostedRoomFrame::OnKickOrBanClient)
EVT_BUTTON(ID_HOSTBAN, HostedRoomFrame::OnKickOrBanClient)
EVT_BUTTON(ID_HOSTBANLIST, HostedRoomFrame::OnOpenBanListDialog)
EVT_BUTTON(ID_HOSTROOMSETTINGS, HostedRoomFrame::OnOpenRoomSettings)
EVT_BUTTON(ID_HOSTROOMADVERTISE, HostedRoomFrame::OnAdvertiseButtonClick)
EVT_BUTTON(wxID_OK, HostedRoomFrame::OnLaunchGame)

EVT_BUTTON(ID_HOSTGENERALTRANSFERPURPOSE, HostedRoomFrame::OnTransferButtonClick)
EVT_BUTTON(ID_HOSTREFUSESELECTIONS, HostedRoomFrame::OnRefuseButtonClick)

EVT_BUTTON(ID_TRANSFERTHREADEND, HostedRoomFrame::OnTransferThreadEnd)

EVT_BUTTON(ID_BANLISTCLOSE, HostedRoomFrame::OnBanListClose)
EVT_BUTTON(ID_HOSTLOOKANDFEELCLOSE, HostedRoomFrame::OnLookAndFeelClose)

EVT_CHOICE(ID_HOSTPLAYERCOLORCHOICE, HostedRoomFrame::OnPlayerColorChoice)

EVT_CLOSE(HostedRoomFrame::OnCloseWindow)

EVT_TEXT_URL(ID_HOSTOUTPUTTEXT, HostedRoomFrame::OnTextURLEvent)

EVT_TEXT_ENTER(ID_HOSTINPUTTEXT, HostedRoomFrame::OnEnterText)
//EVT_TEXT(ID_HOSTINPUTTEXT, HostedRoomFrame::OnUpdateTextInput)

EVT_SOCKET(ID_HOSTSERVER, HostedRoomFrame::OnServerEvent)
EVT_SOCKET(ID_HOSTSOCKET, HostedRoomFrame::OnSocketEvent)
EVT_SOCKET(ID_MASTERSOCKET, HostedRoomFrame::OnServerListSocketEvent)

EVT_TIMER(ID_ADVERTISETIMER, HostedRoomFrame::OnServerListTimer)
EVT_TIMER(ID_HOSTTIMER, HostedRoomFrame::OnTimer)
EVT_TIMER(ID_HOSTFILETRANSFERTIMER, HostedRoomFrame::OnFileTransferTimer)
EVT_TIMER(ID_HOSTPINGTIMER, HostedRoomFrame::OnPingTimer)
EVT_TIMER(ID_HOSTFIRSTPINGTIMER, HostedRoomFrame::OnFirstPingTimer)
EVT_TIMER(ID_HOSTCHECKPINGTIMER, HostedRoomFrame::OnCheckPingTimer)
EVT_TIMER(ID_HOSTPINGREFRESHTIMER, HostedRoomFrame::OnPingRefreshTimer)

EVT_CHECKBOX(ID_HOSTAUTOACCEPTCHECK, HostedRoomFrame::OnAutoAcceptCheck)
EVT_CHECKBOX(ID_HOSTTIMESTAMPS, HostedRoomFrame::OnTimeStampsCheck)
//EVT_CHECKBOX(ID_HOSTNATFREE, HostedRoomFrame::OnNatFreeCheck)
EVT_END_PROCESS(ID_HOSTPROCESS, HostedRoomFrame::OnProcessTerm)
END_EVENT_TABLE()

HostedRoomFrame::HostedRoomFrame() : YANGFrame(NULL, wxID_ANY, wxEmptyString)
{
	Maximize(g_configuration->host_is_maximized);

	wxBoxSizer* HostedRoomFramebSizer = new wxBoxSizer( wxVERTICAL );

	m_hostedroompanel = new YANGPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* hostedroompanelbSizer = new wxBoxSizer( wxVERTICAL );

	m_hostoutputtextCtrl = new YANGTextCtrl( m_hostedroompanel, ID_HOSTOUTPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(-1, 105)), wxTE_AUTO_URL|wxTE_MULTILINE|wxTE_READONLY|wxTE_RICH );
	hostedroompanelbSizer->Add( m_hostoutputtextCtrl, 1, wxTOP|wxLEFT|wxRIGHT|wxEXPAND, 5 );

	wxBoxSizer* hostedroompanelplayercolorbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_hostedroomtimestampscheckBox = new YANGCheckBox( m_hostedroompanel, ID_HOSTTIMESTAMPS, wxT("Show time stamps"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelplayercolorbSizer->Add( m_hostedroomtimestampscheckBox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	hostedroompanelplayercolorbSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_hostplayercolorstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxT("Player color (Duke3dw/SWP):"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelplayercolorbSizer->Add( m_hostplayercolorstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hostplayercolorchoice = new YANGChoice( m_hostedroompanel, ID_HOSTPLAYERCOLORCHOICE, wxDefaultPosition, wxDLG_UNIT(this, wxSize(48, -1)) );
	hostedroompanelplayercolorbSizer->Add( m_hostplayercolorchoice, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	hostedroompanelbSizer->Add( hostedroompanelplayercolorbSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* hostedroompanelinputbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_hostinputtextCtrl = new YANGTextCtrl( m_hostedroompanel, ID_HOSTINPUTTEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, 16)), /*wxTE_MULTILINE|*/wxTE_PROCESS_ENTER );
#if ((defined __WXMAC__) || (defined __WXCOCOA__))
	m_hostinputtextCtrl->MacCheckSpelling(true);
#endif 
	hostedroompanelinputbSizer->Add( m_hostinputtextCtrl, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hostchatoutputstylebutton = new YANGButton( m_hostedroompanel, ID_HOSTCHATOUTPUTSTYLE, wxT("Chat output style"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinputbSizer->Add( m_hostchatoutputstylebutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	/*	m_hostnewlinebutton = new YANGButton( m_hostedroompanel, ID_HOSTADDNEWLINE, wxT("Insert a new line"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelinputbSizer->Add( m_hostnewlinebutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );*/

	hostedroompanelbSizer->Add( hostedroompanelinputbSizer, 0, wxEXPAND, 5 );

	/*	m_hostedroomnatfreecheckBox = new wxCheckBox( m_hostedroompanel, ID_HOSTNATFREE, wxT("Use UDP hole-punching in game (NAT free)."), wxDefaultPosition, wxDefaultSize, 0 );

	hostedroompanelbSizer->Add( m_hostedroomnatfreecheckBox, 0, wxALL, 5 );*/

	wxBoxSizer* hostedroompaneltransferbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_hosttransfergauge = new wxGauge( m_hostedroompanel, wxID_ANY, 100, wxDefaultPosition, wxDLG_UNIT(this, wxSize(128, -1)), wxGA_HORIZONTAL );
	hostedroompaneltransferbSizer->Add( m_hosttransfergauge, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hosttransferstaticText = new wxStaticText( m_hostedroompanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(96, -1)) );
	hostedroompaneltransferbSizer->Add( m_hosttransferstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );


	hostedroompaneltransferbSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_hosttransfercheckBox = new YANGCheckBox( m_hostedroompanel, ID_HOSTAUTOACCEPTCHECK, wxT("Auto-accept downloads"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompaneltransferbSizer->Add( m_hosttransfercheckBox, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hosttransferdynamicbutton = new YANGButton( m_hostedroompanel, ID_HOSTGENERALTRANSFERPURPOSE, wxT("Accept selections"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompaneltransferbSizer->Add( m_hosttransferdynamicbutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_hosttransferrefusebutton = new YANGButton( m_hostedroompanel, ID_HOSTREFUSESELECTIONS, wxT("Refuse selections"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompaneltransferbSizer->Add( m_hosttransferrefusebutton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	hostedroompanelbSizer->Add( hostedroompaneltransferbSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* hostedroompanelplayersbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_hostkicknicknamebutton = new YANGButton( m_hostedroompanel, ID_HOSTKICK, wxT("Kick selected clients"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelplayersbSizer->Add( m_hostkicknicknamebutton, 0, wxALL, 5 );

	m_hostbannicknamebutton = new YANGButton( m_hostedroompanel, ID_HOSTBAN, wxT("Ban selected clients"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelplayersbSizer->Add( m_hostbannicknamebutton, 0, wxALL, 5 );

	m_hostbanlistbutton = new YANGButton( m_hostedroompanel, ID_HOSTBANLIST, wxT("Ban list"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelplayersbSizer->Add( m_hostbanlistbutton, 0, wxALL, 5 );

	hostedroompanelbSizer->Add( hostedroompanelplayersbSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* hostedroompanellistsbSizer = new wxBoxSizer( wxHORIZONTAL );

	//	m_hostplayerslistCtrl = new YANGListCtrl( m_hostedroompanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	m_hostplayerslistCtrl = new YANGListCtrl( m_hostedroompanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(374, -1)), wxLC_REPORT );

	m_ImageList = new wxImageList(16, 16);
	m_hostplayerslistCtrl->AssignImageList(m_ImageList, wxIMAGE_LIST_SMALL);
	wxMemoryInputStream istream_cancel(cancel_png, sizeof cancel_png);
	m_ImageList->Add(wxImage(istream_cancel, wxBITMAP_TYPE_PNG));
	wxMemoryInputStream istream_accept(accept_png, sizeof accept_png);
	m_ImageList->Add(wxImage(istream_accept, wxBITMAP_TYPE_PNG));

	wxListItem itemCol;
	itemCol.SetText(wxT("Rdy"));
	//	itemCol.SetAlign(wxLIST_FORMAT_LEFT);
	//	itemCol.SetImage(-1);
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTREADY, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTREADY, g_configuration->host_col_ready_len);
	itemCol.SetText(wxT("Location"));
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTLOCATION, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTLOCATION, g_configuration->host_col_loc_len);
	itemCol.SetText(wxT("Nicknames"));
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTNICKNAMES, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTNICKNAMES, g_configuration->host_col_nick_len);
	itemCol.SetText(wxT("Ping"));
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTPING, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTPING, g_configuration->host_col_ping_len);
	itemCol.SetText(wxT("Flux"));
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTFLUX, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTFLUX, g_configuration->host_col_flux_len);
	itemCol.SetText(wxT("Operating System"));
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTOS, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTOS, g_configuration->host_col_os_len);
	itemCol.SetText(wxT("Detected IPs"));
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTDETECTED_IPS, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTDETECTED_IPS, g_configuration->host_col_detectedip_len);
	itemCol.SetText(wxT("In-game Addresses"));
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTINGAME_IPS, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTINGAME_IPS, g_configuration->host_col_ingameip_len);
	itemCol.SetText(wxT("Requests"));
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTREQUESTS, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTREQUESTS, g_configuration->host_col_requests_len);
	itemCol.SetText(wxT("File Names"));
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTFILENAMES, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTFILENAMES, g_configuration->host_col_filenames_len);
	itemCol.SetText(wxT("File Sizes"));
	m_hostplayerslistCtrl->InsertColumn(COL_HOSTFILESIZES, itemCol);
	m_hostplayerslistCtrl->SetColumnWidth(COL_HOSTFILESIZES, g_configuration->host_col_filesizes_len);
	//	RefreshListCtrlSize();

	hostedroompanellistsbSizer->Add( m_hostplayerslistCtrl, 1, wxRIGHT|wxEXPAND, 5 );

	m_hostsettingslistCtrl = new YANGListCtrl( m_hostedroompanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(145, 101)), wxLC_REPORT );

	itemCol.SetText(wxT("Settings"));
	m_hostsettingslistCtrl->InsertColumn(0, itemCol);
	m_hostsettingslistCtrl->SetColumnWidth(0, g_configuration->host_col_settings_len);
	itemCol.SetText(wxT("Value"));
	m_hostsettingslistCtrl->InsertColumn(1, itemCol);
	m_hostsettingslistCtrl->SetColumnWidth(1, g_configuration->host_col_value_len);

	hostedroompanellistsbSizer->Add( m_hostsettingslistCtrl, 0, wxEXPAND );

	hostedroompanelbSizer->Add( hostedroompanellistsbSizer, 0, wxLEFT|wxRIGHT|wxEXPAND, 5 );

	wxBoxSizer* hostedroompanelbuttonsbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_hostlaunchbutton = new YANGButton( m_hostedroompanel, wxID_OK, wxT("Launch game"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelbuttonsbSizer->Add( m_hostlaunchbutton, 0, wxALL, 5 );

	hostedroompanelbuttonsbSizer->Add( 0, 0, 1, wxEXPAND, 5 );

	m_hostadvertisebutton = new YANGButton( m_hostedroompanel, ID_HOSTROOMADVERTISE, wxT(" Advertise room "), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelbuttonsbSizer->Add( m_hostadvertisebutton, 0, wxALL, 5 );

	//	m_hostsendpmbutton = new YANGButton( m_hostedroompanel, ID_HOSTSENDPM, wxT("Send private message"), wxDefaultPosition, wxDefaultSize, 0 );
	//	hostedroompanelbuttonsbSizer->Add( m_hostsendpmbutton, 0, wxALL, 5 );

	m_hostsettingsbutton = new YANGButton( m_hostedroompanel, ID_HOSTROOMSETTINGS, wxT("Room settings"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelbuttonsbSizer->Add( m_hostsettingsbutton, 0, wxALL, 5 );

	m_hostshowroomslistbutton = new YANGButton( m_hostedroompanel, ID_HOSTSHOWROOMSLIST, wxT("Show rooms list"), wxDefaultPosition, wxDefaultSize, 0 );
	hostedroompanelbuttonsbSizer->Add( m_hostshowroomslistbutton, 0, wxALL, 5 );

	//	m_hostcloseroombutton = new YANGButton( m_hostedroompanel, wxID_EXIT, wxT("Close Room"), wxDefaultPosition, wxDefaultSize, 0 );
	//	hostedroompanelbuttonsbSizer->Add( m_hostcloseroombutton, 0, wxALL, 5 );

	hostedroompanelbSizer->Add( hostedroompanelbuttonsbSizer, 0, wxEXPAND, 5 );

	m_hostedroompanel->SetSizer( hostedroompanelbSizer );
	m_hostedroompanel->Layout();
	//	hostedroompanelbSizer->Fit( m_hostedroompanel );
	hostedroompanelbSizer->SetSizeHints(m_hostedroompanel);
	HostedRoomFramebSizer->Add( m_hostedroompanel, 1, wxEXPAND | wxALL, 0 );

	SetSizer( HostedRoomFramebSizer );
	Layout();
	//	HostedRoomFramebSizer->Fit( this );
	HostedRoomFramebSizer->SetSizeHints(this);

	SetIcon(wxIcon(yang_xpm));
	SetSize(g_configuration->host_win_width, g_configuration->host_win_height);
	Centre();

	m_mainframe = new MainFrame(this);

	m_hosttransfercheckBox->SetValue(g_configuration->host_autoaccept_downloads);
	//m_hostedroomnatfreecheckBox->SetValue(g_configuration->gamelaunch_enable_natfree);

	m_num_of_lists = g_serverlists_portnums->GetCount();
	m_num_of_extra_lists = g_configuration->extraservers_portnums.GetCount();
	m_server = NULL;
	m_in_game = false;
	m_allow_sounds = true;
	m_not_first_update = false;
	//m_num_players = 0;
	//m_max_num_players = MAX_NUM_PLAYERS;
	m_transfer_state = TRANSFERSTATE_NONE;

	m_is_advertised = false;
	m_client = NULL;
	m_process = NULL;
	m_connection_timeout_timer = new wxTimer(this, ID_HOSTTIMER);
	m_serverlist_timer = new wxTimer(this, ID_ADVERTISETIMER);

	m_ping_timer = new wxTimer(this, ID_HOSTPINGTIMER);
	m_firstping_timer = new wxTimer(this, ID_HOSTFIRSTPINGTIMER);
	m_checkping_timer = new wxTimer(this, ID_HOSTCHECKPINGTIMER);
	m_pingrefresh_timer = new wxTimer(this, ID_HOSTPINGREFRESHTIMER);

	m_checkping_index = -1;

	m_hostedroomtimestampscheckBox->SetValue(g_configuration->show_timestamps);

	m_hostplayercolorchoice->Disable();

	if (g_configuration->theme_is_dark)
		m_transfer_listctrl_color = *wxWHITE;
	else
		m_transfer_listctrl_color = *wxGREEN;

	m_banlist_dialog = new BanListDialog(this);
	m_lookandfeel_dialog = new LookAndFeelDialog(this, true);

	SetChatStyle();
	m_hostinputtextCtrl->SetFocus();
}

HostedRoomFrame::~HostedRoomFrame()
{
	// The MP Dialog also creates its own hidden HostedRoomFrame object
	// for testing; It should NOT touch g_host_frame!
	if (this == g_host_frame)
	{
		if (m_process != NULL)
			m_process->Detach();

		g_host_frame = NULL;
		g_main_frame->ShowMainFrame();
	}
}
/*
void HostedRoomFrame::RefreshListCtrlSize()
{
int l_width;
if (m_hostplayerslistCtrl->GetItemCount())
l_width = wxLIST_AUTOSIZE;
else
l_width = wxLIST_AUTOSIZE_USEHEADER;
m_hostplayerslistCtrl->SetColumnWidth(0, l_width);
m_hostplayerslistCtrl->SetColumnWidth(1, l_width);
m_hostplayerslistCtrl->SetColumnWidth(2, l_width);
m_hostplayerslistCtrl->SetColumnWidth(3, l_width);
m_hostplayerslistCtrl->SetColumnWidth(4, l_width);
m_hostplayerslistCtrl->SetColumnWidth(5, l_width);
}

void HostedRoomFrame::OnResizeWindow(wxSizeEvent& event)
{
wxSize l_size = event.GetSize();
if (!IsMaximized())
{
g_configuration->host_win_width = l_size.GetWidth();
g_configuration->host_win_height = l_size.GetHeight();
}
}

void HostedRoomFrame::OnMaximize(wxMaximizeEvent& WXUNUSED(event))
{
if (IsMaximized())
GetSize(&g_configuration->host_win_width, &g_configuration->host_win_height);
}
*/
void HostedRoomFrame::FocusFrame()
{
	m_hostinputtextCtrl->SetFocus();
}

void HostedRoomFrame::SendRoomInfo(bool is_new_room)
{
	wxArrayString l_str_list;
	wxIPV4address l_addr;

	if (is_new_room)
	{
		l_str_list.Add(wxT("advertise"));
		l_str_list.Add(YANGMASTER_STR_NET_VERSION);
		l_str_list.Add(YANG_STR_NET_VERSION);
		m_client->GetLocal(l_addr);
		l_str_list.Add(g_EncryptForCommText(l_addr.IPAddress(), COMM_KEY_IPLIST_FROM_HOST_TO_MASTER));
		l_str_list.Add(wxString::Format(wxT("%ld"), (long)g_configuration->server_port_number));
	}
	else
		l_str_list.Add(wxT("updateinfo"));

	if (m_use_password)
		l_str_list.Add(wxT("yes"));
	else
		l_str_list.Add(wxT("no"));

	switch (m_game)
	{
		case GAME_BLOOD:
			l_str_list.Add(GAMECODE_BLOOD);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			switch (m_srcport)
			{
				case SOURCEPORT_DOSBLOODSW: l_str_list.Add(SRCPORTCODE_DOSBLOODSW); break;
				case SOURCEPORT_DOSBLOODRG: l_str_list.Add(SRCPORTCODE_DOSBLOODRG); break;
				case SOURCEPORT_DOSBLOODPP: l_str_list.Add(SRCPORTCODE_DOSBLOODPP); break;
				case SOURCEPORT_DOSBLOODOU: l_str_list.Add(SRCPORTCODE_DOSBLOODOU); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_skill));

			switch (m_bloodgametype)
			{
				case BLOODMPGAMETYPE_COOP:  l_str_list.Add(wxT("coop")); break;
				case BLOODMPGAMETYPE_BB:    l_str_list.Add(wxT("bb")); break;
				case BLOODMPGAMETYPE_TEAMS: l_str_list.Add(wxT("teams")); break;
				default: ;
			}

			break;

		case GAME_DESCENT:
			l_str_list.Add(GAMECODE_DESCENT);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			switch (m_srcport)
			{
				case SOURCEPORT_DOSDESCENT: l_str_list.Add(SRCPORTCODE_DOSDESCENT); break;
				case SOURCEPORT_D1XREBIRTH: l_str_list.Add(SRCPORTCODE_D1XREBIRTH); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_skill));

			switch (m_descentgametype)
			{
				case DESCENTMPGAMETYPE_ANARCHY:      l_str_list.Add(wxT("anarchy")); break;
				case DESCENTMPGAMETYPE_TEAM_ANARCHY: l_str_list.Add(wxT("team_anarchy")); break;
				case DESCENTMPGAMETYPE_ROBO_ANARCHY: l_str_list.Add(wxT("robo_anarchy")); break;
				case DESCENTMPGAMETYPE_COOPERATIVE:  l_str_list.Add(wxT("cooperative")); break;
				default: ;
			}

			break;

		case GAME_DESCENT2:
			l_str_list.Add(GAMECODE_DESCENT2);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			switch (m_srcport)
			{
				case SOURCEPORT_DOSDESCENT2: l_str_list.Add(SRCPORTCODE_DOSDESCENT2); break;
				case SOURCEPORT_D2XREBIRTH:  l_str_list.Add(SRCPORTCODE_D2XREBIRTH); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_skill));

			switch (m_descent2gametype)
			{
				case DESCENT2MPGAMETYPE_ANARCHY:          l_str_list.Add(wxT("anarchy")); break;
				case DESCENT2MPGAMETYPE_TEAM_ANARCHY:     l_str_list.Add(wxT("team_anarchy")); break;
				case DESCENT2MPGAMETYPE_ROBO_ANARCHY:     l_str_list.Add(wxT("robo_anarchy")); break;
				case DESCENT2MPGAMETYPE_COOPERATIVE:      l_str_list.Add(wxT("cooperative")); break;
				case DESCENT2MPGAMETYPE_CAPTURE_THE_FLAG: l_str_list.Add(wxT("capture_the_flag")); break;
				case DESCENT2MPGAMETYPE_HOARD:            l_str_list.Add(wxT("hoard")); break;
				case DESCENT2MPGAMETYPE_TEAM_HOARD:       l_str_list.Add(wxT("team_hoard")); break;
				default: ;
			}

			break;

		case GAME_DN3D:
			l_str_list.Add(GAMECODE_DN3D);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			switch (m_srcport)
			{
				case SOURCEPORT_DOSDUKESW: l_str_list.Add(SRCPORTCODE_DOSDUKESW); break;
				case SOURCEPORT_DOSDUKERG: l_str_list.Add(SRCPORTCODE_DOSDUKERG); break;
				case SOURCEPORT_DOSDUKEAE: l_str_list.Add(SRCPORTCODE_DOSDUKEAE); break;
				case SOURCEPORT_DUKE3DW:   l_str_list.Add(SRCPORTCODE_DUKE3DW); break;
				case SOURCEPORT_EDUKE32:   l_str_list.Add(SRCPORTCODE_EDUKE32); break;
				case SOURCEPORT_NDUKE:     l_str_list.Add(SRCPORTCODE_NDUKE); break;
				case SOURCEPORT_HDUKE:     l_str_list.Add(SRCPORTCODE_HDUKE); break;
				case SOURCEPORT_XDUKE:     l_str_list.Add(SRCPORTCODE_XDUKE); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_skill));

			switch (m_dn3dgametype)
			{
				case DN3DMPGAMETYPE_DMSPAWN:    l_str_list.Add(wxT("dmspawn")); break;
				case DN3DMPGAMETYPE_COOP:       l_str_list.Add(wxT("coop")); break;
				case DN3DMPGAMETYPE_DMNOSPAWN:  l_str_list.Add(wxT("dmnospawn")); break;
				case DN3DMPGAMETYPE_TDMSPAWN:   l_str_list.Add(wxT("tdmspawn")); break;
				case DN3DMPGAMETYPE_TDMNOSPAWN: l_str_list.Add(wxT("tdmnospawn")); break;
				default: ;
			}

			break;

		case GAME_SW:
			l_str_list.Add(GAMECODE_SW);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			switch (m_srcport)
			{
				case SOURCEPORT_DOSSWSW: l_str_list.Add(SRCPORTCODE_DOSSWSW); break;
				case SOURCEPORT_DOSSWRG: l_str_list.Add(SRCPORTCODE_DOSSWRG); break;
				case SOURCEPORT_VOIDSW:  l_str_list.Add(SRCPORTCODE_VOIDSW); break;
				case SOURCEPORT_SWP:     l_str_list.Add(SRCPORTCODE_SWP); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_skill));

			switch (m_swgametype)
			{
				case SWMPGAMETYPE_WBSPAWN:   l_str_list.Add(wxT("wbspawn")); break;
				case SWMPGAMETYPE_WBNOSPAWN: l_str_list.Add(wxT("wbnospawn")); break;
				case SWMPGAMETYPE_COOP:      l_str_list.Add(wxT("coop")); break;
				default: ;
			}

			break;

		case GAME_CUSTOM:
			l_str_list.Add(GAMECODE_CUSTOM);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			l_str_list.Add(SRCPORTCODE_CUSTOM);
			l_str_list.Add(wxEmptyString);
			l_str_list.Add(wxEmptyString);

			break;
		default: ;
	}

	if (m_launch_usermap)
	{
		l_str_list.Add(wxT("usermap"));
		l_str_list.Add(m_usermap);
	}
	else
	{
		l_str_list.Add(wxT("epimap"));
		l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_epimap_num));
	}

	l_str_list.Add(m_modname);

	if (m_use_crypticpassage)
		l_str_list.Add(wxT("yes"));
	else
		l_str_list.Add(wxT("no"));

	g_SendStringListToSocket(m_client, l_str_list);
}

void HostedRoomFrame::UpdateGameSettings(bool isPassProtected,
										 const wxString& password,
										 GameType game,
										 SourcePortType srcport,
										 bool use_crypticpassage,
										 const wxString& roomname,
										 size_t max_num_players,
										 bool recorddemo,
										 const wxString& demofilename,
										 size_t skill,
										 BloodMPGameT_Type bloodgametype,
										 DescentMPGameT_Type descentgametype,
										 Descent2MPGameT_Type descent2gametype,
										 DN3DMPGameT_Type dn3dgametype,
										 SWMPGameT_Type swgametype,
										 DN3DSpawnType dn3dspawn,
										 BloodMPMonsterType blood_monster,
										 BloodMPWeaponType blood_weapon,
										 BloodMPItemType blood_item,
										 BloodMPRespawnType blood_respawn,
										 const wxString& args_for_host,
										 const wxString& args_for_all,
										 size_t maxping,
										 size_t rxdelay,
										 bool launch_usermap,
										 size_t epimap_num,
										 const wxString& usermap,
										 const wxArrayString& modfiles,
										 const wxString& modname,
										 const wxString& modconfile,
										 const wxString& moddeffile,
										 bool showmodurl,
										 const wxString& modurl,
										 bool advertise,
										 size_t connectiontype,
										 bool allow_ingame_joining)
{
	size_t l_loop_var;
	wxArrayString l_str_list;

	// Is there an on-going transfer? Then we may have to abort a current transfer,
	// and we should refuse all transfer requests anyway.
	// That would happen if there's a change in the game (e.g. DN3D to SW),
	// or when there's a change in the user map,
	// or from a user map to an episode map.
	// On a change from episode map to user map there's nothing to do,
	// as there's no transfer in that case.

	if (m_not_first_update &&
		((m_game != game) || (m_launch_usermap && (m_usermap != usermap)) ||
		(m_launch_usermap && (!launch_usermap))))
	{ // In case of no transfer or a pending transfer, we simply refuse all requests.
		if ((m_transfer_state == TRANSFERSTATE_NONE) ||
			(m_transfer_state == TRANSFERSTATE_PENDING))
		{
			m_transfer_state = TRANSFERSTATE_NONE;
			for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
				if (m_players_table.requests[l_loop_var].transferrequest != TRANSFERREQUEST_NONE)
				{
					m_players_table.requests[l_loop_var].transferrequest = TRANSFERREQUEST_NONE;
					g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:requestrefuse:");
					m_hostplayerslistCtrl->SetItemTextColour(l_loop_var, m_def_listctrl_color);
					m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTREQUESTS, wxEmptyString);
					m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTFILENAMES, wxEmptyString);
					m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTFILESIZES, wxEmptyString);
					//        RefreshListCtrlSize();
				}
				m_hosttransferstaticText->SetLabel(wxEmptyString);
				m_hosttransferdynamicbutton->SetLabel(wxT("Accept selections"));
				m_hostlaunchbutton->Enable();
		}
		else // Just don't refuse for the client who's in the middle of a file transfer.
		{
			if (m_transfer_state == TRANSFERSTATE_BUSY) // Abort and wait for thread end.
			{
				m_transfer_state = TRANSFERSTATE_ABORTED;
				g_SendCStringToSocket(m_host_clients_table.clients[m_transfer_client_index].sock, "1:aborttransfer:");
			}

			/**************************************************************************
			If transfer isn't busy, then it's aborted and we're waiting for thread end.
			In case it has been aborted and the relevant client has recently been
			deleted from the list, m_transfer_client_index == 0.
			The loops will still "just work", from client 1 upwards.
			**************************************************************************/

			for (l_loop_var = 1; l_loop_var < m_transfer_client_index; l_loop_var++)
				if (m_players_table.requests[l_loop_var].transferrequest != TRANSFERREQUEST_NONE)
				{
					m_players_table.requests[l_loop_var].transferrequest = TRANSFERREQUEST_NONE;
					g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:requestrefuse:");
					m_hostplayerslistCtrl->SetItemTextColour(l_loop_var, m_def_listctrl_color);
					m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTREQUESTS, wxEmptyString);
					m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTFILENAMES, wxEmptyString);
					m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTFILESIZES, wxEmptyString);
					//        RefreshListCtrlSize();
				}
				for (l_loop_var = m_transfer_client_index+1; l_loop_var < m_players_table.num_players; l_loop_var++)
					if (m_players_table.requests[l_loop_var].transferrequest != TRANSFERREQUEST_NONE)
					{
						m_players_table.requests[l_loop_var].transferrequest = TRANSFERREQUEST_NONE;
						g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:requestrefuse:");
						m_hostplayerslistCtrl->SetItemTextColour(l_loop_var, m_def_listctrl_color);
						m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTREQUESTS, wxEmptyString);
						m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTFILENAMES, wxEmptyString);
						m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTFILESIZES, wxEmptyString);
						//        RefreshListCtrlSize();
					}
		}
	}
	m_not_first_update = true;

	m_game = game;
	m_srcport = srcport;
	m_use_crypticpassage = use_crypticpassage;
	m_roomname = roomname;
	if (m_players_table.num_players > max_num_players)
	{
		AddMessage(wxT("* You can't reduce the maximum amount of players to be lower than currently in room."), false);
		if (g_configuration->play_snd_error)
			g_PlaySound(g_configuration->snd_error_file);
	}
	else
		//{
		m_max_num_players = max_num_players;
	/*
	// Send message only if we have enough clients to send to
	// (excluding the last one), and avoid assigning the negative value -1
	// to l_loop_var (which would happen if num_clients == 0).
	if (m_host_clients_table.num_clients > 1)
	{
	// There may still be a few clients connected but not in room.
	// Let's disconnect them, from the latest who's got connected,
	// until we have enough room.
	// We should reserve one more for the case of file transfer.
	// Better keep the last one.
	l_loop_var = m_host_clients_table.num_clients - 1; // Last client - We skip it.
	while (max_num_players < l_loop_var)
	{
	l_loop_var--;
	g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "2:error:Server is full.:");
	m_host_clients_table.clients[l_loop_var].sock->Destroy();
	m_host_clients_table.DelByIndex(l_loop_var);
	}
	}
	}
	*/
	m_recorddemo = recorddemo;
	m_demofilename = demofilename;
	m_skill = skill;
	m_bloodgametype = bloodgametype;
	m_descentgametype = descentgametype;
	m_descent2gametype = descent2gametype;
	m_dn3dgametype = dn3dgametype;
	m_swgametype = swgametype;
	m_dn3dspawn = dn3dspawn;
	m_blood_monster = blood_monster;
	m_blood_weapon = blood_weapon;
	m_blood_item = blood_item;
	m_blood_respawn = blood_respawn;
	m_args_for_host = args_for_host;
#if EXTRA_ARGS_FOR_ALL
	m_args_for_all = args_for_all;
#endif
	m_maxping = maxping;
	m_rxdelay = rxdelay;
	m_launch_usermap = launch_usermap;
	m_epimap_num = epimap_num;
	m_usermap = usermap;
	m_modfiles = modfiles;
	m_modname = modname;
	m_modconfile = modconfile;
	m_moddeffile = moddeffile;
	m_showmodurl = showmodurl;
	m_modurl = modurl;

	m_connectiontype = connectiontype;
	if (connectiontype && game == GAME_CUSTOM && m_players_table.num_players > 2)
	{
		m_connectiontype = 0;
		// To continue from an earlier error message...
		AddMessage(wxT("Thus, Null-Modem connection can't be established; So IPX/SPX is used for now."), false);
		if (g_configuration->play_snd_error)
			g_PlaySound(g_configuration->snd_error_file);
	}

	m_allow_ingame_joining = allow_ingame_joining;

	switch (game)
	{
		case GAME_BLOOD:
			s_gamename = GAMENAME_BLOOD;
			switch (srcport)
			{
				case SOURCEPORT_DOSBLOODSW: s_srcport = SRCPORTNAME_DOSBLOODSW; break;
				case SOURCEPORT_DOSBLOODRG: s_srcport = SRCPORTNAME_DOSBLOODRG; break;
				case SOURCEPORT_DOSBLOODPP: s_srcport = SRCPORTNAME_DOSBLOODPP; break;
				case SOURCEPORT_DOSBLOODOU: s_srcport = SRCPORTNAME_DOSBLOODOU; break;
				default: ;
			}
			s_skill = (*g_blood_skill_list)[skill];
			s_gametype = (*g_blood_gametype_list)[bloodgametype-1];
			s_monster = (*g_blood_monstersettings_list)[blood_monster];
			s_weapon = (*g_blood_weaponsettings_list)[blood_weapon];
			s_item = (*g_blood_itemsettings_list)[blood_item];
			s_respawn = (*g_blood_respawnsettings_list)[blood_respawn];
			if (launch_usermap)
				s_map = usermap;
			else
			{
				if (!m_use_crypticpassage)
					s_map = (*g_blood_level_list)[g_GetBloodLevelSelectionByNum(epimap_num)];
				else
					s_map = (*g_blood_cp_level_list)[g_GetBloodCPLevelSelectionByNum(epimap_num)];
			}

			break;

		case GAME_DESCENT:
			s_gamename = GAMENAME_DESCENT;
			switch (srcport)
			{
				case SOURCEPORT_DOSDESCENT: s_srcport = SRCPORTNAME_DOSDESCENT; break;
				case SOURCEPORT_D1XREBIRTH: s_srcport = SRCPORTNAME_D1XREBIRTH; break;
				default: ;
			}
			s_gametype = (*g_descent_gametype_list)[descentgametype];
			if (launch_usermap)
				s_map = usermap;
			else
				s_map = (*g_descent_level_list)[epimap_num];

			break;

		case GAME_DESCENT2:
			s_gamename = GAMENAME_DESCENT2;
			switch (srcport)
			{
				case SOURCEPORT_DOSDESCENT2: s_srcport = SRCPORTNAME_DOSDESCENT2; break;
				case SOURCEPORT_D2XREBIRTH:  s_srcport = SRCPORTNAME_D2XREBIRTH; break;
				default: ;
			}
			s_gametype = (*g_descent2_gametype_list)[descent2gametype];
			if (launch_usermap)
				s_map = usermap;
			else
				s_map = (*g_descent2_level_list)[epimap_num];

			break;

		case GAME_DN3D:
			s_gamename = GAMENAME_DN3D;
			switch (srcport)
			{
				case SOURCEPORT_DOSDUKESW: s_srcport = SRCPORTNAME_DOSDUKESW; break;
				case SOURCEPORT_DOSDUKERG: s_srcport = SRCPORTNAME_DOSDUKERG; break;
				case SOURCEPORT_DOSDUKEAE: s_srcport = SRCPORTNAME_DOSDUKEAE; break;
				case SOURCEPORT_DUKE3DW:   s_srcport = SRCPORTNAME_DUKE3DW; break;
				case SOURCEPORT_EDUKE32:   s_srcport = SRCPORTNAME_EDUKE32; break;
				case SOURCEPORT_NDUKE:     s_srcport = SRCPORTNAME_NDUKE; break;
				case SOURCEPORT_HDUKE:     s_srcport = SRCPORTNAME_HDUKE; break;
				case SOURCEPORT_XDUKE:     s_srcport = SRCPORTNAME_XDUKE; break;
				default: ;
			}
			s_skill = (*g_dn3d_skill_list)[skill];
			s_gametype = (*g_dn3d_gametype_list)[dn3dgametype-1];
			s_spawn = (*g_dn3d_spawn_list)[dn3dspawn];
			if (launch_usermap)
				s_map = usermap;
			else
				s_map = (*g_dn3d_level_list)[g_GetDN3DLevelSelectionByNum(epimap_num)];

			break;

		case GAME_SW:
			s_gamename = GAMENAME_SW;
			switch (srcport)
			{
				case SOURCEPORT_DOSSWSW: s_srcport = SRCPORTNAME_DOSSWSW; break;
				case SOURCEPORT_DOSSWRG: s_srcport = SRCPORTNAME_DOSSWRG; break;
				case SOURCEPORT_VOIDSW:  s_srcport = SRCPORTNAME_VOIDSW; break;
				case SOURCEPORT_SWP:     s_srcport = SRCPORTNAME_SWP; break;
				default: ;
			}
			s_skill = (*g_sw_skill_list)[skill];
			if (srcport == SOURCEPORT_SWP)
				s_gametype = (*g_sw_gametype_list)[swgametype];
			else
				s_gametype = wxEmptyString;
			if (launch_usermap)
				s_map = usermap;
			else
				s_map = (*g_sw_level_list)[g_GetSWLevelSelectionByNum(epimap_num)];

			break;

		case GAME_CUSTOM:
			s_gamename = GAMENAME_CUSTOM;

			s_gametype = wxEmptyString;

			break;
		default: ;
	}

	SetTitle(roomname);
	s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);
	s_maxping = wxString::Format(wxT("%ld"), (long)m_maxping);
	s_rxdelay = wxString::Format(wxT("%lu"), (unsigned long)m_rxdelay);
	s_mod = modname;

	if (game == GAME_BLOOD)
	{
		switch (connectiontype)
		{
			case PACKETMODE_AUTO:        s_packetmode = wxT("Auto"); break;
			case PACKETMODE_BROADCAST:   s_packetmode = wxT("Broadcast"); break;
			case PACKETMODE_MASTERSLAVE: s_packetmode = wxT("Master/Slave"); break;
			default: ;
		}
	}

	if (srcport == SOURCEPORT_EDUKE32 && connectiontype == CONNECTIONTYPE_SERVERCLIENT)
		s_connection = wxT("Server/Client (BROKEN!)");
	else if ((srcport == SOURCEPORT_DUKE3DW || srcport == SOURCEPORT_EDUKE32) && connectiontype == CONNECTIONTYPE_MASTERSLAVE)
		s_connection = wxT("Master/Slave");
	else if (game == GAME_CUSTOM && connectiontype)
		s_connection = wxT("Null-Modem");
	else if ((srcport == SOURCEPORT_DOSBLOODSW) || (srcport == SOURCEPORT_DOSBLOODRG) || (srcport == SOURCEPORT_DOSBLOODPP) || (srcport == SOURCEPORT_DOSBLOODOU) ||
		(srcport == SOURCEPORT_DOSDESCENT) || (srcport == SOURCEPORT_DOSDESCENT2) ||
		(srcport == SOURCEPORT_DOSDUKESW) || (srcport == SOURCEPORT_DOSDUKERG) || (srcport == SOURCEPORT_DOSDUKEAE) ||
		(srcport == SOURCEPORT_DOSSWSW) || (srcport == SOURCEPORT_DOSSWRG) ||
		(game == GAME_CUSTOM))
		s_connection = wxT("Emulated IPX/SPX");
	else
		s_connection = wxT("Peer-2-Peer");

	m_use_password = isPassProtected;
	m_password = password;

	UpdateSettingsList();

	if (((m_game == GAME_DESCENT) || (m_game == GAME_DESCENT2)) && m_launch_usermap && !m_hosttransfercheckBox->GetValue())
		AddMessage(wxT("WARNING : It is recommended to enable the \"Auto-accept downloads\" option, when playing Descent or Descent 2, so that when you are in-game, people joining the gameroom can automatically download the map."), false);

	// If there are clients, that means it's just a refresh.
	// For all clients we'll tell the main info (e.g. source port).

	if (m_host_clients_table.num_clients > 1)
	{
		l_str_list = GetServerInfoSockStrList();
		for (l_loop_var = 1; l_loop_var < m_host_clients_table.num_clients; l_loop_var++)
			g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
		// For the ones actually in the room, changes to MOD files list as well.
		if (m_players_table.num_players > 1)
		{
			l_str_list = GetServerModFilesSockStrList();
			for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
				g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
		}
	}

	m_hostplayercolorchoice->Clear();
	if (srcport == SOURCEPORT_DUKE3DW)
	{
		m_hostplayercolorchoice->Append(*g_dn3d_playercol_list);
		m_hostplayercolorchoice->SetSelection(g_configuration->gamelaunch_dn3d_playercolnum);
	}
	else if (srcport == SOURCEPORT_SWP)
	{
		m_hostplayercolorchoice->Append(*g_sw_playercol_list);
		m_hostplayercolorchoice->SetSelection(g_configuration->gamelaunch_sw_playercolnum);
	}
	m_hostplayercolorchoice->Enable((srcport == SOURCEPORT_DUKE3DW) || (srcport == SOURCEPORT_SWP));

	if (m_is_advertised)
		SendRoomInfo(false);
	else if ((advertise) && (m_hostadvertisebutton->GetLabel() == wxT(" Advertise room ")))
		Advertise();
}

void HostedRoomFrame::UpdateSettingsList()
{
	size_t l_index = 0;

	m_hostsettingslistCtrl->DeleteAllItems();

	m_hostsettingslistCtrl->InsertItem(l_index, wxT("Game"));
	m_hostsettingslistCtrl->SetItem(l_index, 1, s_gamename);

	if (m_game != GAME_CUSTOM)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Port"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_srcport);
	}

	if (m_game == GAME_BLOOD && m_use_crypticpassage)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Add-on"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, wxT("Cryptic Passage"));
	}

	l_index++;
	m_hostsettingslistCtrl->InsertItem(l_index, wxT("Players"));
	m_hostsettingslistCtrl->SetItem(l_index, 1, s_players);

	if (m_game != GAME_CUSTOM)
	{
		l_index++;
		if (m_game != GAME_DESCENT && m_game != GAME_DESCENT2)
			m_hostsettingslistCtrl->InsertItem(l_index, wxT("Map"));
		else
			m_hostsettingslistCtrl->InsertItem(l_index, wxT("Mission"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_map);
	}

	if (s_mod != wxEmptyString)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("TC/MOD"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_mod);
	}

	if (s_gametype != wxEmptyString)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Game Type"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_gametype);
	}

	if (m_game != GAME_DESCENT && m_game != GAME_DESCENT2 && m_game != GAME_CUSTOM)
	{
		l_index++;
		if (m_game != GAME_BLOOD)
			m_hostsettingslistCtrl->InsertItem(l_index, wxT("Skill"));
		else
			m_hostsettingslistCtrl->InsertItem(l_index, wxT("Difficulty"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_skill);
	}

	if (m_game == GAME_DN3D)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Spawn"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_spawn);
	}

	if (m_game == GAME_BLOOD)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Monsters"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_monster);

		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Weapons"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_weapon);

		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Items"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_item);

		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Respawn"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_respawn);
	}

	if (m_use_password)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Private"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, wxT("Yes"));
	}

	if (m_maxping)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Max Ping"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_maxping);
	}

	if (m_recorddemo)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(
			l_index,
			(m_srcport == SOURCEPORT_HDUKE) ?
			wxT("hDuke mode select") : wxT("Record demo"));
		if (m_demofilename == wxEmptyString)
			m_hostsettingslistCtrl->SetItem(l_index, 1, wxT("Yes"));
		else
			m_hostsettingslistCtrl->SetItem(l_index, 1, m_demofilename);
	}

	if (m_args_for_host != wxEmptyString)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Host args"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, m_args_for_host);
	}

#if EXTRA_ARGS_FOR_ALL
	if (m_args_for_all != wxEmptyString)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Extra args"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, m_args_for_all);
	}
#endif

	if (m_game == GAME_BLOOD && m_connectiontype)
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Packet mode"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_packetmode);
	}

	if (m_srcport == SOURCEPORT_DUKE3DW || m_srcport == SOURCEPORT_EDUKE32 || (m_game == GAME_CUSTOM && m_connectiontype))
	{
		l_index++;
		m_hostsettingslistCtrl->InsertItem(l_index, wxT("Connection"));
		m_hostsettingslistCtrl->SetItem(l_index, 1, s_connection);

		if (m_game == GAME_CUSTOM)
		{
			l_index++;
			m_hostsettingslistCtrl->InsertItem(l_index, wxT("rxdelay"));
			m_hostsettingslistCtrl->SetItem(l_index, 1, s_rxdelay + (m_rxdelay == DEFAULT_RXDELAY ? wxT(" (DOSBox's default)") : wxEmptyString));
		}
	}

	for (l_index = 0; (signed)l_index < m_hostsettingslistCtrl->GetItemCount(); l_index++)
		m_hostsettingslistCtrl->SetItemTextColour(l_index, m_def_listctrl_color);
}

void l_AddBanListToArray(wxArrayString& str_list)
{
	wxTextFile l_banlistfile;
	size_t l_num_of_lines, l_loop_var;
	if (wxFile::Access(g_configuration->banlist_filepath, wxFile::read))
	{
		l_banlistfile.Open(g_configuration->banlist_filepath);
		l_num_of_lines = l_banlistfile.GetLineCount();
		for (l_loop_var = 0 ; l_loop_var < l_num_of_lines; l_loop_var++)
			str_list.Add(l_banlistfile[l_loop_var]);
		l_banlistfile.Close();
	}
}

void l_CopyStringArrayToHash(wxArrayString& str_list, YANGHashString& str_hash)
{
	YANGHashString::value_type l_pair(wxEmptyString, true);
	size_t l_num_of_strings = str_list.GetCount(), l_loop_var;
	for (l_loop_var = 0 ; l_loop_var < l_num_of_strings; l_loop_var++)
	{
		l_pair.first = str_list[l_loop_var];
		str_hash.insert(l_pair);
	}
}

bool l_SaveBanListToFile(wxArrayString& str_list)
{
	wxTextFile l_banlistfile;
	wxString l_banlistpath = ((wxFileName)g_configuration->banlist_filepath).GetPath();
	size_t l_num_of_lines, l_loop_var;
	if (!wxDirExists(l_banlistpath))
		if (!wxFileName::Mkdir(l_banlistpath, 0777, wxPATH_MKDIR_FULL))
			return false;
	if (((!wxFileExists(g_configuration->banlist_filepath)) || (wxRemoveFile(g_configuration->banlist_filepath)))
		&& (l_banlistfile.Create(g_configuration->banlist_filepath)))
	{
		l_num_of_lines = str_list.GetCount();
		for (l_loop_var = 0 ; l_loop_var < l_num_of_lines; l_loop_var++)
			l_banlistfile.AddLine(str_list[l_loop_var]);
		l_banlistfile.Write();
		l_banlistfile.Close();
		return true;
	}
	return false;
}

bool IsStringInHash(const wxString& str, YANGHashString& str_hash)
{
	return (str_hash.find(str) != str_hash.end());
}

void AddStringToHash(const wxString& str, YANGHashString& str_hash)
{
	YANGHashString::value_type l_pair(str, true);
	str_hash.insert(l_pair);
}

bool HostedRoomFrame::StartServer()
{
	wxIPV4address l_addr;
	l_addr.Service(g_configuration->server_port_number);

	m_server = new wxSocketServer(l_addr);

	if (!m_server->Ok())
	{
		delete m_server;
		m_server = NULL;
		return false;
	}

	m_server->SetEventHandler(*this, ID_HOSTSERVER);
	m_server->SetFlags(wxSOCKET_REUSEADDR);
	m_server->SetNotify(wxSOCKET_CONNECTION_FLAG);
	m_server->Notify(true);

	m_players_table.Add(g_configuration->nickname, wxEmptyString,
		g_configuration->game_port_number, *g_osdescription, true);
	m_host_clients_table.Add(NULL, wxEmptyString, NULL);
	m_hostplayerslistCtrl->InsertItem(0, IMG_HOSTACCEPT);
	m_hostplayerslistCtrl->SetItem(0, COL_HOSTNICKNAMES, g_configuration->nickname);
	m_hostplayerslistCtrl->SetItem(0, COL_HOSTPING, wxT("0"));
	m_hostplayerslistCtrl->SetItem(0, COL_HOSTOS, m_players_table.players[0].os_description);
	if (g_configuration->theme_is_dark)
	{
		m_def_listctrl_color = *wxGREEN;
		m_hostplayerslistCtrl->SetItemTextColour(0, m_def_listctrl_color);
	}
	else
		m_def_listctrl_color = m_hostplayerslistCtrl->GetTextColour();
	//RefreshListCtrlSize();

	m_pingrefresh_timer->Start(10000, wxTIMER_ONE_SHOT);
	m_pinging = 0;

	l_AddBanListToArray(m_banned_ips_array);
	l_CopyStringArrayToHash(m_banned_ips_array, m_banned_ips_hash);

	AddMessage(wxT("For a list of commands, enter /help command."), false);

	m_connection_timeout_timer->Start(5000, wxTIMER_ONE_SHOT);

	return true;
}

void HostedRoomFrame::AddMessage(const wxString& msg, bool allow_timestamp)
{
	if (allow_timestamp && g_configuration->show_timestamps)
		(*m_hostoutputtextCtrl) << wxT('(') << wxDateTime::Now().FormatISOTime() << wxT(") ");
	//m_hostoutputtextCtrl->AppendText(wxT('\n') + msg + wxT('\n'));
	(*m_hostoutputtextCtrl) << msg << wxT('\n');
	while (m_hostoutputtextCtrl->GetNumberOfLines() > MAX_NUM_MSG_LINES)
		m_hostoutputtextCtrl->Remove(0, (m_hostoutputtextCtrl->GetLineText(0)).Len()+1);
	// Without that, text scrolling may not work as expected.
#ifdef __WXMSW__
	m_hostoutputtextCtrl->ScrollLines(-1);
#elif ((defined __WXMAC__) || (defined __WXCOCOA__))
	((wxWindow*)m_hostoutputtextCtrl)->SetScrollPos(wxVERTICAL, ((wxWindow*)m_hostoutputtextCtrl)->GetScrollRange(wxVERTICAL));
#endif
}

void HostedRoomFrame::SetInGame(bool in_game)
{
	m_in_game = in_game;
	size_t l_loop_var;
	if (in_game)
	{
		m_pingrefresh_timer->Stop(); // Save bandwidth.
		for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
		{
			m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTPING, wxEmptyString);
			m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTFLUX, wxEmptyString);
		}
	}
	else if (!m_pingrefresh_timer->IsRunning()) // Maybe stopped after a game.
		m_pingrefresh_timer->Start(1000, wxTIMER_ONE_SHOT);
}

void HostedRoomFrame::OnTextURLEvent(wxTextUrlEvent& event)
{
	if (event.GetMouseEvent().GetEventType() == wxEVT_LEFT_DOWN)
		g_OpenURL(m_hostoutputtextCtrl->GetRange(event.GetURLStart(), event.GetURLEnd()));
}

void HostedRoomFrame::DoRename(size_t index, const wxString& newname)
{
	wxArrayString l_str_list;
	size_t l_loop_var;
	if (newname == m_players_table.players[index].nickname)
	{
		if (index == 0)
		{
			AddMessage(wxT("* This is already your nickname."), false);
			if (g_configuration->play_snd_error)
				g_PlaySound(g_configuration->snd_error_file);
		}
		//  else
		//    g_SendCStringToSocket(m_host_clients_table.clients[index].sock, "2:errnick:There's no need to rename yourself to the current nickname.");
		return;
	}
	for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
		if ((l_loop_var != index) && (m_players_table.players[l_loop_var].nickname == newname))
		{
			if (index == 0)
			{
				AddMessage(wxT("* You can't rename yourself with the nickname of someone else."), false);
				if (g_configuration->play_snd_error)
					g_PlaySound(g_configuration->snd_error_file);
			}
			//    else
			//      g_SendCStringToSocket(m_host_clients_table.clients[index].sock, "2:errnick:You can't rename yourself with the nickname of someone else.");
			return;
		}
		AddMessage(wxT("* ") + m_players_table.players[index].nickname + wxT(" has renamed to ") + newname + wxT('.'), true);
		l_str_list.Add(wxT("rename"));
		l_str_list.Add(m_players_table.players[index].nickname); // Old nick
		l_str_list.Add(newname);
		m_players_table.players[index].nickname = newname; // New nick
		m_host_clients_table.clients[index].nickname = newname; // Safer to do...
		m_hostplayerslistCtrl->SetItem(index, COL_HOSTNICKNAMES, newname);
		for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
			g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
		if (m_is_advertised)
		{
			l_str_list.Empty();
			l_str_list.Add(wxT("players"));
			for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
			{
				l_str_list.Add(m_players_table.players[l_loop_var].nickname);
				l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_loop_var].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_MASTER));
				l_str_list.Add(m_players_table.players[l_loop_var].os_description);
			}
			g_SendStringListToSocket(m_client, l_str_list);
		}
}

void HostedRoomFrame::DoSetRoomName(const wxString& newname)
{
	wxArrayString l_str_list;
	size_t l_loop_var;
	m_roomname = newname;
	SetTitle(newname);
	if (m_is_advertised)
		SendRoomInfo(false);
	l_str_list.Add(wxT("newtitle"));
	l_str_list.Add(newname);
	for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
		g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
}

void HostedRoomFrame::DoEnterText(const wxString& text)
{
	wxArrayString l_str_list;
	size_t l_loop_var;
	AddMessage(m_players_table.players[0].nickname + wxT(": ") + text, true);
	if (g_configuration->play_snd_send)
		g_PlaySound(g_configuration->snd_send_file);
	l_str_list.Add(wxT("msg"));
	l_str_list.Add(m_players_table.players[0].nickname);
	l_str_list.Add(text);
	for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
		g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
}
/*
void HostedRoomFrame::OnInsertNewLine(wxCommandEvent& WXUNUSED(event))
{
m_hostinputtextCtrl->WriteText(wxT("\n"));
m_hostinputtextCtrl->SetFocus();
}
*/
void HostedRoomFrame::OnEnterText(wxCommandEvent& WXUNUSED(event))
{
	wxString l_text = m_hostinputtextCtrl->GetValue();
	wxArrayString l_str_list;
	size_t l_len;

#ifndef __WXMSW__
	SetInGame(false);
	if (m_is_advertised)
		SendRoomInfo(false);
	m_allow_sounds = true;
#endif

	if (l_text.IsEmpty())
	{
		if (g_configuration->play_snd_error)
			g_PlaySound(g_configuration->snd_error_file);
	}
	else if (l_text[0] == wxT('/'))
	{
		l_len = l_text.Len();
		if (l_text == wxT("/clear"))
			m_hostoutputtextCtrl->Clear();
		else if ((l_len >= 7) && ((l_text.Left(6) == wxT("/nick ")) || (l_text.Left(6) == wxT("/name "))))
			DoRename(0, l_text.Mid(6));
		else if (l_text == wxT("/nowake"))
		{
			g_configuration->host_accept_wakeup = false;
			g_configuration->Save();
		}
		else if (l_text == wxT("/wake"))
		{
			g_configuration->host_accept_wakeup = true;
			g_configuration->Save();
		}
		else if ((l_len >= 10) && (l_text.Left(10) == wxT("/roomname ")))
			DoSetRoomName(l_text.Mid(10));
		else if ((l_len >= 12) && (l_text.Left(12) == wxT("/servername ")))
			DoSetRoomName(l_text.Mid(12));
		else if ((l_len >= 2) && (l_text[1] == wxT('/')))
			DoEnterText(l_text.Mid(1));
		else
			AddMessage(wxT("\n*** List of commands:\n\n/clear - Clear the text output.\n/help - Show this list of commands.\n/nick <name> - Rename yourself to <name>.\n/name <name> - Same as /nick.\n/nowake - Avoid clients from waking your room up.\n/wake - Let clients wake your room.\n/roomname <name> - Set room name to <name>.\n/servername <name> - Same as /roomname.\n"), false);
	}
	else
		DoEnterText(l_text);
	m_hostinputtextCtrl->Clear();
}
/*
void HostedRoomFrame::OnUpdateTextInput(wxCommandEvent& WXUNUSED(event))
{
if (g_configuration->use_custom_chat_text_layout)
SetChatInputStyleOnUpdate();
}
*/
void HostedRoomFrame::OnPlayerColorChoice(wxCommandEvent& WXUNUSED(event))
{
	if (m_srcport == SOURCEPORT_DUKE3DW)
		g_configuration->gamelaunch_dn3d_playercolnum = m_hostplayercolorchoice->GetSelection();
	else // SOURCEPORT_SWP
		g_configuration->gamelaunch_sw_playercolnum = m_hostplayercolorchoice->GetSelection();
	g_configuration->Save();
}

void HostedRoomFrame::OnKickOrBanClient(wxCommandEvent& event)
{
	size_t l_loop_var;
	wxArrayString l_str_list;
	bool l_is_ban = (event.GetId() == ID_HOSTBAN);
	long l_selection = m_hostplayerslistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);

#ifndef __WXMSW__
	SetInGame(false);
	if (m_is_advertised)
		SendRoomInfo(false);
	m_allow_sounds = true;
#endif

	if (!l_selection) // Number 0, or the host.
	{
		AddMessage(wxT("* You can't kick/ban yourself. Please deselect yourself in the list before retrying to kick/ban."), false);
		if (g_configuration->play_snd_error)
			g_PlaySound(g_configuration->snd_error_file);
	}
	else if (l_selection < 0) // -1
	{
		AddMessage(wxT("* You should select at least one client in the list for kicking/banning (apart from yourself)."), false);
		if (g_configuration->play_snd_error)
			g_PlaySound(g_configuration->snd_error_file);
	}
	else
	{
		do // NOTICE: Following l_selection values would get decreased by 1!
		{  // We'd keep all kinds of lists (UI and internal) IN SYNC while looping.
			if (l_is_ban)
			{
				l_str_list.Add(wxT("banned"));
				l_str_list.Add(m_players_table.players[l_selection].nickname);
				m_banned_ips_array.Add(m_host_clients_table.clients[l_selection].peerdetectedip);
				AddStringToHash(m_host_clients_table.clients[l_selection].peerdetectedip, m_banned_ips_hash);
				for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
					g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
				AddMessage(wxT("* ") + m_players_table.players[l_selection].nickname + wxT(" has been banned."), true);
			}
			else
			{
				l_str_list.Add(wxT("kicked"));
				l_str_list.Add(m_players_table.players[l_selection].nickname);
				for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
					g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
				AddMessage(wxT("* ") + m_players_table.players[l_selection].nickname + wxT(" has been kicked."), true);
			}
			m_host_clients_table.clients[l_selection].sock->Destroy();
			RemoveInRoomClient(l_selection);
			m_hostplayerslistCtrl->DeleteItem(l_selection);
			m_ImageList->Remove(l_selection+IMG_HOSTNUMBER);
			//      RefreshListCtrlSize();

			for (l_loop_var = l_selection; l_loop_var < m_players_table.num_players; l_loop_var++)
				m_hostplayerslistCtrl->SetItemColumnImage(l_loop_var, COL_HOSTLOCATION, l_loop_var+IMG_HOSTNUMBER);

			l_selection = m_hostplayerslistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
			l_str_list.Empty();
		}
		while (l_selection >= 0); // Not -1
		if (l_is_ban)
			if (!l_SaveBanListToFile(m_banned_ips_array))
				wxLogError(wxT("ERROR: Couldn't save ban list to ") + g_configuration->banlist_filepath);
		if (g_configuration->play_snd_leave)
			g_PlaySound(g_configuration->snd_leave_file);
		s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);
		UpdateSettingsList();
		if (m_is_advertised)
		{
			l_str_list.Empty();
			l_str_list.Add(wxT("players"));
			for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
			{
				l_str_list.Add(m_players_table.players[l_loop_var].nickname);
				l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_loop_var].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_MASTER));
				l_str_list.Add(m_players_table.players[l_loop_var].os_description);
			}
			g_SendStringListToSocket(m_client, l_str_list);
		}
	}
}

void HostedRoomFrame::OnOpenBanListDialog(wxCommandEvent& WXUNUSED(event))
{
	m_banlist_dialog->m_banlistBox->Set(m_banned_ips_array);
	m_banlist_dialog->ShowBanListDialog();
}

void HostedRoomFrame::OnOpenRoomSettings(wxCommandEvent& WXUNUSED(event))
{
	Disable();

#ifndef __WXMSW__
	SetInGame(false);
	if (m_is_advertised)
		SendRoomInfo(false);
	m_allow_sounds = true;
#endif

	g_mp_dialog = new MPDialog(m_use_password);
	g_mp_dialog->Show(true);
	g_mp_dialog->SetFocus();
	//g_mp_dialog->Raise();
}

void HostedRoomFrame::OnProcessTerm(wxProcessEvent& WXUNUSED(event))
{
	SetInGame(false);
	if (m_is_advertised)
		SendRoomInfo(false);
	m_allow_sounds = true;

//	AddMessage(wxT("* Process terminated"), true);

	if (m_process)
	{
		delete m_process;
		m_process = NULL;
	}
}

void HostedRoomFrame::OnLaunchGame(wxCommandEvent& WXUNUSED(event))
{
	bool l_script_is_ready, l_launch_is_ready = true;

	for (size_t l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
		if (!m_players_table.players[l_loop_var].ready)
		{
			l_launch_is_ready = false;

			AddMessage(wxT("* ") + m_players_table.players[l_loop_var].nickname + wxT(" is not ready : please, ask him to click on the \"Ready\" button."), false);
		}

		if (l_launch_is_ready)
		{
#ifdef __WXMSW__
			wxString l_temp_str = *g_launcher_user_profile_path + wxT("yang.bat");
#elif (defined __WXMAC__) || (defined __WXCOCOA__)
			wxString l_temp_str = *g_launcher_user_profile_path + wxT("yang.command");
#else
			wxString l_temp_str = *g_launcher_user_profile_path + wxT("yang.sh");
#endif
			if (m_players_table.num_players == 1)
				l_script_is_ready = g_MakeLaunchScript(
				true,
				l_temp_str,
				NULL,
				0,
				m_game,
				m_srcport,
				m_use_crypticpassage,
				m_epimap_num,
				m_usermap,
				m_launch_usermap,
				m_bloodgametype,
				m_dn3dgametype,
				m_swgametype,
				0,
				false,
				m_skill,
				m_blood_monster,
				m_blood_weapon,
				m_blood_item,
				m_blood_respawn,
				m_dn3dspawn,
				false,
				wxEmptyString,
				m_recorddemo,
				m_demofilename,
				m_connectiontype,
				m_rxdelay,
				!m_modname.IsEmpty(),
				m_args_for_host + wxT(' ') + m_args_for_all);
			else
			{
				l_script_is_ready = g_MakeLaunchScript(
					true,
					l_temp_str,
					&m_players_table,
					0,
					m_game,
					m_srcport,
					m_use_crypticpassage,
					m_epimap_num,
					m_usermap,
					m_launch_usermap,
					m_bloodgametype,
					m_dn3dgametype,
					m_swgametype,
					0,
					false,
					m_skill,
					m_blood_monster,
					m_blood_weapon,
					m_blood_item,
					m_blood_respawn,
					m_dn3dspawn,
					false,
					wxEmptyString,
					m_recorddemo,
					m_demofilename,
					m_connectiontype,
					m_rxdelay,
					!m_modname.IsEmpty(),
					m_args_for_host + wxT(' ') + m_args_for_all);

				for (size_t l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
					g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:launch:");
			}

			if (l_script_is_ready)
			{
				AddMessage(wxT("* Launching game..."), true);

#ifdef __WXMSW__
				m_process = new wxProcess(this, ID_HOSTPROCESS);

				if (wxExecute(wxT('"') + l_temp_str + wxT('"'), wxEXEC_ASYNC, m_process))
				{
					m_allow_sounds = !(g_configuration->mute_sounds_while_ingame);
					SetInGame(true);
					if (m_is_advertised)
						SendRoomInfo(false);
				}
#else
				wxExecute(g_configuration->terminal_fullcmd + wxT(" \"") + l_temp_str + wxT('"'));

				m_allow_sounds = !(g_configuration->mute_sounds_while_ingame);
				SetInGame(true);
				if (m_is_advertised)
					SendRoomInfo(false);
#endif

				for (size_t l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
				{
					m_players_table.players[l_loop_var].ready = false;
					m_hostplayerslistCtrl->SetItemColumnImage(l_loop_var, COL_HOSTREADY, IMG_HOSTCANCEL);
				}
			}
		}
}

wxArrayString HostedRoomFrame::GetServerInfoSockStrList()
{
	wxArrayString l_str_list;

	l_str_list.Add(wxT("info"));

	switch (m_game)
	{
		case GAME_BLOOD:
			l_str_list.Add(GAMECODE_BLOOD);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			switch (m_srcport)
			{
				case SOURCEPORT_DOSBLOODSW: l_str_list.Add(SRCPORTCODE_DOSBLOODSW); break;
				case SOURCEPORT_DOSBLOODRG: l_str_list.Add(SRCPORTCODE_DOSBLOODRG); break;
				case SOURCEPORT_DOSBLOODPP: l_str_list.Add(SRCPORTCODE_DOSBLOODPP); break;
				case SOURCEPORT_DOSBLOODOU: l_str_list.Add(SRCPORTCODE_DOSBLOODOU); break;
				default: ;
			}

			if (m_use_crypticpassage)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_skill));
			switch (m_bloodgametype)
			{
				case BLOODMPGAMETYPE_COOP:  l_str_list.Add(wxT("coop")); break;
				case BLOODMPGAMETYPE_BB:    l_str_list.Add(wxT("bb")); break;
				case BLOODMPGAMETYPE_TEAMS: l_str_list.Add(wxT("teams")); break;
				default: ;
			}
			switch (m_blood_monster)
			{
				case BLOODMPMONSTERTYPE_NONE:    l_str_list.Add(wxT("none")); break;
				case BLOODMPMONSTERTYPE_ENABLED: l_str_list.Add(wxT("enabled")); break;
				case BLOODMPMONSTERTYPE_RESPAWN: l_str_list.Add(wxT("respawn")); break;
				default: ;
			}
			switch (m_blood_weapon)
			{
				case BLOODMPWEAPONTYPE_NORESPAWN: l_str_list.Add(wxT("norespawn")); break;
				case BLOODMPWEAPONTYPE_PERMANENT: l_str_list.Add(wxT("permanent")); break;
				case BLOODMPWEAPONTYPE_RESPAWN:   l_str_list.Add(wxT("respawn")); break;
				case BLOODMPWEAPONTYPE_MARKERS:   l_str_list.Add(wxT("markers")); break;
				default: ;
			}
			switch (m_blood_item)
			{
				case BLOODMPITEMTYPE_NORESPAWN: l_str_list.Add(wxT("norespawn")); break;
				case BLOODMPITEMTYPE_RESPAWN:   l_str_list.Add(wxT("respawn")); break;
				case BLOODMPITEMTYPE_MARKERS:   l_str_list.Add(wxT("markers")); break;
				default: ;
			}
			switch (m_blood_respawn)
			{
				case BLOODMPRESPAWNTYPE_RANDOM:  l_str_list.Add(wxT("random")); break;
				case BLOODMPRESPAWNTYPE_WEAPONS: l_str_list.Add(wxT("weapons")); break;
				case BLOODMPRESPAWNTYPE_ENEMIES: l_str_list.Add(wxT("enemies")); break;
				default: ;
			}
			l_str_list.Add(m_args_for_host);
			l_str_list.Add(m_args_for_all);

			if (m_use_password)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			l_str_list.Add(wxString::Format(wxT("%ld"), (long)m_maxping));

			if (m_recorddemo)
			{
				l_str_list.Add(wxT("yes"));
				l_str_list.Add(m_demofilename);
			}
			else
			{
				l_str_list.Add(wxT("no"));
				l_str_list.Add(wxEmptyString);
			}

			if (m_launch_usermap)
			{
				l_str_list.Add(wxT("usermap"));
				l_str_list.Add(m_usermap);
			}
			else
			{
				l_str_list.Add(wxT("epimap"));
				l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_epimap_num));
			}

			if (m_connectiontype == PACKETMODE_BROADCAST)
				l_str_list.Add(wxT("broadcast"));
			else if (m_connectiontype == PACKETMODE_MASTERSLAVE)
				l_str_list.Add(wxT("masterslave"));

			break;

		case GAME_DESCENT:
			l_str_list.Add(GAMECODE_DESCENT);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			switch (m_srcport)
			{
				case SOURCEPORT_DOSDESCENT: l_str_list.Add(SRCPORTCODE_DOSDESCENT); break;
				case SOURCEPORT_D1XREBIRTH: l_str_list.Add(SRCPORTCODE_D1XREBIRTH); break;
				default: ;
			}
			switch (m_descentgametype)
			{
				case DESCENTMPGAMETYPE_ANARCHY:      l_str_list.Add(wxT("anarchy")); break;
				case DESCENTMPGAMETYPE_TEAM_ANARCHY: l_str_list.Add(wxT("team_anarchy")); break;
				case DESCENTMPGAMETYPE_ROBO_ANARCHY: l_str_list.Add(wxT("robo_anarchy")); break;
				case DESCENTMPGAMETYPE_COOPERATIVE:  l_str_list.Add(wxT("cooperative")); break;
				default: ;
			}
			l_str_list.Add(m_args_for_host);
			l_str_list.Add(m_args_for_all);

			if (m_use_password)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			l_str_list.Add(wxString::Format(wxT("%ld"), (long)m_maxping));

			if (m_launch_usermap)
			{
				l_str_list.Add(wxT("usermap"));
				l_str_list.Add(m_usermap);
			}
			else
			{
				l_str_list.Add(wxT("epimap"));
				l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_epimap_num));
			}

			break;

		case GAME_DESCENT2:
			l_str_list.Add(GAMECODE_DESCENT2);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			switch (m_srcport)
			{
				case SOURCEPORT_DOSDESCENT2: l_str_list.Add(SRCPORTCODE_DOSDESCENT2); break;
				case SOURCEPORT_D2XREBIRTH:  l_str_list.Add(SRCPORTCODE_D2XREBIRTH); break;
				default: ;
			}
			switch (m_descent2gametype)
			{
				case DESCENT2MPGAMETYPE_ANARCHY:          l_str_list.Add(wxT("anarchy")); break;
				case DESCENT2MPGAMETYPE_TEAM_ANARCHY:     l_str_list.Add(wxT("team_anarchy")); break;
				case DESCENT2MPGAMETYPE_ROBO_ANARCHY:     l_str_list.Add(wxT("robo_anarchy")); break;
				case DESCENT2MPGAMETYPE_COOPERATIVE:      l_str_list.Add(wxT("cooperative")); break;
				case DESCENT2MPGAMETYPE_CAPTURE_THE_FLAG: l_str_list.Add(wxT("capture_the_flag")); break;
				case DESCENT2MPGAMETYPE_HOARD:            l_str_list.Add(wxT("hoard")); break;
				case DESCENT2MPGAMETYPE_TEAM_HOARD:       l_str_list.Add(wxT("team_hoard")); break;
				default: ;
			}
			l_str_list.Add(m_args_for_host);
			l_str_list.Add(m_args_for_all);

			if (m_use_password)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			l_str_list.Add(wxString::Format(wxT("%ld"), (long)m_maxping));

			if (m_launch_usermap)
			{
				l_str_list.Add(wxT("usermap"));
				l_str_list.Add(m_usermap);
			}
			else
			{
				l_str_list.Add(wxT("epimap"));
				l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_epimap_num));
			}

			break;

		case GAME_DN3D:
			l_str_list.Add(GAMECODE_DN3D);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			switch (m_srcport)
			{
				case SOURCEPORT_DOSDUKESW: l_str_list.Add(SRCPORTCODE_DOSDUKESW); break;
				case SOURCEPORT_DOSDUKERG: l_str_list.Add(SRCPORTCODE_DOSDUKERG); break;
				case SOURCEPORT_DOSDUKEAE: l_str_list.Add(SRCPORTCODE_DOSDUKEAE); break;
				case SOURCEPORT_DUKE3DW:   l_str_list.Add(SRCPORTCODE_DUKE3DW); break;
				case SOURCEPORT_EDUKE32:   l_str_list.Add(SRCPORTCODE_EDUKE32); break;
				case SOURCEPORT_NDUKE:     l_str_list.Add(SRCPORTCODE_NDUKE); break;
				case SOURCEPORT_HDUKE:     l_str_list.Add(SRCPORTCODE_HDUKE); break;
				case SOURCEPORT_XDUKE:     l_str_list.Add(SRCPORTCODE_XDUKE); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_skill));
			switch (m_dn3dgametype)
			{
				case DN3DMPGAMETYPE_DMSPAWN:    l_str_list.Add(wxT("dmspawn")); break;
				case DN3DMPGAMETYPE_COOP:       l_str_list.Add(wxT("coop")); break;
				case DN3DMPGAMETYPE_DMNOSPAWN:  l_str_list.Add(wxT("dmnospawn")); break;
				case DN3DMPGAMETYPE_TDMSPAWN:   l_str_list.Add(wxT("tdmspawn")); break;
				case DN3DMPGAMETYPE_TDMNOSPAWN: l_str_list.Add(wxT("tdmnospawn")); break;
				default: ;
			}
			switch (m_dn3dspawn)
			{
				case DN3DSPAWN_NONE:      l_str_list.Add(wxT("none")); break;
				case DN3DSPAWN_MONSTERS:  l_str_list.Add(wxT("monsters")); break;
				case DN3DSPAWN_ITEMS:     l_str_list.Add(wxT("items")); break;
				case DN3DSPAWN_INVENTORY: l_str_list.Add(wxT("inventory")); break;
				case DN3DSPAWN_ALL:       l_str_list.Add(wxT("all")); break;
				default: ;
			}
			l_str_list.Add(m_args_for_host);
			l_str_list.Add(m_args_for_all);

			if (m_use_password)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			l_str_list.Add(wxString::Format(wxT("%ld"), (long)m_maxping));

			if (m_recorddemo)
			{
				l_str_list.Add(wxT("yes"));
				l_str_list.Add(m_demofilename);
			}
			else
			{
				l_str_list.Add(wxT("no"));
				l_str_list.Add(wxEmptyString);
			}

			if (m_launch_usermap)
			{
				l_str_list.Add(wxT("usermap"));
				l_str_list.Add(m_usermap);
			}
			else
			{
				l_str_list.Add(wxT("epimap"));
				l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_epimap_num));
			}

			if (m_connectiontype == CONNECTIONTYPE_MASTERSLAVE)
				l_str_list.Add(wxT("masterslave"));
			else if (m_connectiontype == CONNECTIONTYPE_SERVERCLIENT)
				l_str_list.Add(wxT("serverclient"));

			break;

		case GAME_SW:
			l_str_list.Add(GAMECODE_SW);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			switch (m_srcport)
			{
				case SOURCEPORT_DOSSWSW: l_str_list.Add(SRCPORTCODE_DOSSWSW); break;
				case SOURCEPORT_DOSSWRG: l_str_list.Add(SRCPORTCODE_DOSSWRG); break;
				case SOURCEPORT_VOIDSW:  l_str_list.Add(SRCPORTCODE_VOIDSW); break;
				case SOURCEPORT_SWP:     l_str_list.Add(SRCPORTCODE_SWP); break;
				default: ;
			}
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_skill));
			switch (m_swgametype)
			{
				case SWMPGAMETYPE_WBSPAWN:   l_str_list.Add(wxT("wbspawn")); break;
				case SWMPGAMETYPE_WBNOSPAWN: l_str_list.Add(wxT("wbnospawn")); break;
				case SWMPGAMETYPE_COOP:      l_str_list.Add(wxT("coop")); break;
				default: ;
			}
			l_str_list.Add(m_args_for_host);
			l_str_list.Add(m_args_for_all);

			if (m_use_password)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			l_str_list.Add(wxString::Format(wxT("%ld"), (long)m_maxping));

			if (m_recorddemo)
			{
				l_str_list.Add(wxT("yes"));
				l_str_list.Add(m_demofilename);
			}
			else
			{
				l_str_list.Add(wxT("no"));
				l_str_list.Add(wxEmptyString);
			}

			if (m_launch_usermap)
			{
				l_str_list.Add(wxT("usermap"));
				l_str_list.Add(m_usermap);
			}
			else
			{
				l_str_list.Add(wxT("epimap"));
				l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_epimap_num));
			}

			break;

		case GAME_CUSTOM:
			l_str_list.Add(GAMECODE_CUSTOM);
			l_str_list.Add(m_roomname);
			l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_max_num_players));
			if (m_in_game)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));
			l_str_list.Add(SRCPORTCODE_CUSTOM);
			l_str_list.Add(m_args_for_host);
			l_str_list.Add(m_args_for_all);

			if (m_use_password)
				l_str_list.Add(wxT("yes"));
			else
				l_str_list.Add(wxT("no"));

			l_str_list.Add(wxString::Format(wxT("%ld"), (long)m_maxping));

			if (m_connectiontype)
			{
				l_str_list.Add(wxT("nullmodem"));

				if (m_rxdelay != DEFAULT_RXDELAY)
					l_str_list.Add(wxString::Format(wxT("%lu"), (unsigned long)m_rxdelay));
			}

			break;
		default: ;
	}

	return l_str_list;
}

wxArrayString HostedRoomFrame::GetServerModFilesSockStrList()
{
	wxArrayString l_str_list;
	size_t l_loop_var, num_of_files = m_modfiles.GetCount();

	l_str_list.Add(wxT("mod"));
	l_str_list.Add(m_modname);
	l_str_list.Add(m_modconfile);
	l_str_list.Add(m_moddeffile);
	if (m_showmodurl)
	{
		l_str_list.Add(wxT("withurl"));
		l_str_list.Add(m_modurl);
	}
	else
		l_str_list.Add(wxT("nourl"));
	for (l_loop_var = 0; l_loop_var < num_of_files; l_loop_var++)
		l_str_list.Add(m_modfiles[l_loop_var]);
	return l_str_list;
}

void HostedRoomFrame::RemoveInRoomClient(size_t index)
{
	//size_t l_loop_var;
	if (m_ping_index == (signed)index)
	{
		m_ping_timer->Stop();
		m_pinging = 0;
		m_ping_index = -1;
	}
	else if (m_ping_index > (signed)index)
		m_ping_index--;

	m_players_table.DelByIndex(index);
	m_host_clients_table.DelByIndex(index);

	//  Is there a file transfer? Maybe we need to update its client's index.
	if (m_transfer_state != TRANSFERSTATE_NONE)
	{
		// If m_transfer_client_index is 0, meaning another client has recently
		// been deleted using this very same function,
		// nothing would get changed, as index > 0 anyway (it isn't the host).
		if (m_transfer_client_index > index)
			m_transfer_client_index--;
		else if (m_transfer_client_index == index) // Is it the very same client?
		{
			// Mark that the client has been deleted.
			// 0 is the host and can't be a client anyway.
			m_transfer_client_index = 0;
			// In case the state is TRANSFERSTATE_BUSY, abort the transfer
			// and wait for thread end.
			if (m_transfer_state == TRANSFERSTATE_BUSY)
				m_transfer_state = TRANSFERSTATE_ABORTED;
			else if (m_transfer_state == TRANSFERSTATE_PENDING)
			{
				// TRANSFERSTATE_PENDING - Set to NONE and check for other download requests.
				m_transfer_state = TRANSFERSTATE_NONE;
				FindNextDownloadRequest();
			}
		}
	}
	/*
	m_num_players--;
	for (l_loop_var = index; l_loop_var < m_num_players; l_loop_var++)
	m_selfdetectedips[l_loop_var] = m_selfdetectedips[l_loop_var+1];
	*/
}

void HostedRoomFrame::FindNextDownloadRequest()
{
	size_t l_loop_var = 1;
	while (l_loop_var < m_players_table.num_players)
	{
		if ((m_players_table.requests[l_loop_var].transferrequest == TRANSFERREQUEST_DOWNLOAD)
			&& (m_players_table.requests[l_loop_var].awaiting_download))
		{
			AcceptTransferRequest(l_loop_var);
			break;
		}
		l_loop_var++;
	}
	if (l_loop_var == m_players_table.num_players) // Haven't found?
	{
		m_hosttransferstaticText->SetLabel(wxEmptyString);
		m_hosttransferdynamicbutton->SetLabel(wxT("Accept selections"));
		m_hostlaunchbutton->Enable();
	}
}

void HostedRoomFrame::AcceptTransferRequest(size_t player_index)
{
	wxArrayString l_str_list;
	wxIPV4address l_client_actual_ip;
	m_transfer_state = TRANSFERSTATE_PENDING;
	m_transfer_client_index = player_index;

	m_host_clients_table.clients[player_index].sock->GetPeer(l_client_actual_ip);
	m_transfer_ipaddress = l_client_actual_ip.IPAddress();

	m_transfer_is_download = (m_players_table.requests[player_index].transferrequest == TRANSFERREQUEST_DOWNLOAD);

	wxString l_filename;
	wxFile file;
	char *fileptr;
	unsigned long long filesize;

	if (m_transfer_is_download)
	{
		l_str_list.Add(wxT("downloadaccept"));

		switch (m_game)
		{
			case GAME_BLOOD:    l_filename = g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap; break;
			case GAME_DESCENT:  l_filename = g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap; break;
			case GAME_DESCENT2: l_filename = g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap; break;
			case GAME_DN3D:     l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap; break;
			case GAME_SW:       l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap; break;
			default: ;
		}

		filesize = (unsigned long long)wxFileName::GetSize(l_filename).ToULong();
		l_str_list.Add(wxString::Format(wxT("%llu"), filesize));

		if (file.Access(l_filename, wxFile::read) && file.Open(l_filename, wxFile::read))
		{
			fileptr = new char[filesize];
			file.Read(fileptr, filesize);
			file.Close();

			l_str_list.Add(wxString::Format(wxT("%u"), (unsigned)crc32buf(fileptr, filesize)));

			delete[] fileptr;
		}
		else
			wxLogError(wxT("Can't open file for reading!"));

		g_SendStringListToSocket(m_host_clients_table.clients[player_index].sock, l_str_list);
	}
	else
	{
		g_SendCStringToSocket(m_host_clients_table.clients[player_index].sock, "1:uploadaccept:");
		m_transfer_filename = m_players_table.requests[player_index].filename;
	}
	m_hosttransferstaticText->SetLabel(m_transfer_filename);
	m_players_table.requests[player_index].transferrequest = TRANSFERREQUEST_NONE;
	m_hostplayerslistCtrl->SetItemTextColour(player_index, m_transfer_listctrl_color);
}

void HostedRoomFrame::OnTransferButtonClick(wxCommandEvent& WXUNUSED(event))
{
	long l_item;
	size_t l_client_index;
	TransferRequestType l_request;
	bool l_bad_selection;

#ifndef __WXMSW__
	SetInGame(false);
	if (m_is_advertised)
		SendRoomInfo(false);
	m_allow_sounds = true;
#endif

	if (m_transfer_state == TRANSFERSTATE_NONE) // "Accept" button
	{
		l_request = TRANSFERREQUEST_NONE;
		l_bad_selection = false;
		l_item = m_hostplayerslistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
		while (l_item >= 0)
		{
			if (m_players_table.requests[l_item].transferrequest == TRANSFERREQUEST_DOWNLOAD)
			{
				if (l_request == TRANSFERREQUEST_UPLOAD) // Mixing is unallowed.
				{
					l_bad_selection = true;
					break;
				}
				else if (l_request == TRANSFERREQUEST_NONE)
				{
					l_request = TRANSFERREQUEST_DOWNLOAD;
					l_client_index = l_item;
					//          l_client_index = (unsigned long)l_item;
				}
			}
			else if (m_players_table.requests[l_item].transferrequest == TRANSFERREQUEST_UPLOAD)
			{
				if (l_request == TRANSFERREQUEST_NONE)
				{
					l_request = TRANSFERREQUEST_UPLOAD;
					l_client_index = l_item;
					//          l_client_index = (unsigned long)l_item;
				}
				else // Only one upload request may be selected,
				{    // and again, mixing is unallowed.
					l_bad_selection = true;
					break;
				}
			}
			l_item = m_hostplayerslistCtrl->GetNextItem(l_item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
		}
		if (l_bad_selection)
		{
			AddMessage(wxT("* You may accept downloads or an upload, but not both. Furthermore, only one upload may be accepted."), false);
			if (g_configuration->play_snd_error)
				g_PlaySound(g_configuration->snd_error_file);
		}
		else if (l_request == TRANSFERREQUEST_UPLOAD)
		{
			m_hosttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
			m_hostlaunchbutton->Disable();
			AcceptTransferRequest(l_client_index);
		}
		else if (l_request == TRANSFERREQUEST_DOWNLOAD)
		{
			l_item = m_hostplayerslistCtrl->GetNextItem(l_client_index, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
			while (l_item >= 0) // Make all selected downloads occur later,
			{                   // For bit better performance, don't check if everybody has a request.
				// That doesn't matter for clients with no download requests anyway.
				m_players_table.requests[l_item].awaiting_download = true;
				l_item = m_hostplayerslistCtrl->GetNextItem(l_item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
			}
			m_hosttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
			m_hostlaunchbutton->Disable();
			AcceptTransferRequest(l_client_index);
		}
	}
	else // "Abort" button
	{
		if (m_transfer_state == TRANSFERSTATE_BUSY)
		{
			m_transfer_state = TRANSFERSTATE_ABORTED;
			g_SendCStringToSocket(m_host_clients_table.clients[m_transfer_client_index].sock, "1:aborttransfer:");
		}
		else if (m_transfer_state == TRANSFERSTATE_PENDING)
		{
			m_transfer_state = TRANSFERSTATE_NONE;
			m_players_table.requests[m_transfer_client_index].transferrequest = TRANSFERREQUEST_NONE;
			g_SendCStringToSocket(m_host_clients_table.clients[m_transfer_client_index].sock, "1:requestrefuse:");
			m_hostplayerslistCtrl->SetItemTextColour(m_transfer_client_index, m_def_listctrl_color);
			m_hostplayerslistCtrl->SetItem(m_transfer_client_index, COL_HOSTREQUESTS, wxEmptyString);
			m_hostplayerslistCtrl->SetItem(m_transfer_client_index, COL_HOSTFILENAMES, wxEmptyString);
			m_hostplayerslistCtrl->SetItem(m_transfer_client_index, COL_HOSTFILESIZES, wxEmptyString);
			//      RefreshListCtrlSize();
			// Let's check if there's a pending download request
			// from another client.
			l_item = 1;
			while ((l_item) && ((unsigned long)l_item < m_transfer_client_index))
				if ((m_players_table.requests[l_item].transferrequest == TRANSFERREQUEST_DOWNLOAD)
					&& (m_players_table.requests[l_item].awaiting_download))
				{
					AcceptTransferRequest(l_item);
					l_item = 0; // Mark we've found a client.
				}
				else
					l_item++;
			if (l_item) // Have we not found a client?
			{
				l_item = m_transfer_client_index+1; // We skip the client for which we've just cancelled a request.
				while ((l_item) && ((unsigned long)l_item < m_transfer_client_index))
					if ((m_players_table.requests[l_item].transferrequest == TRANSFERREQUEST_DOWNLOAD)
						&& (m_players_table.requests[l_item].awaiting_download))
					{
						AcceptTransferRequest(l_item);
						l_item = 0; // Mark we've found a client.
					}
					else
						l_item++;
			}
			if (l_item) // Still haven't found?
			{               // Let's make a little "clean up".
				m_hosttransferstaticText->SetLabel(wxEmptyString);
				m_hosttransferdynamicbutton->SetLabel(wxT("Accept selections"));
				m_hostlaunchbutton->Enable();
			}
		}
		// In case of TRANSFERSTATE_ABORTED, the relevant socket
		// deletion should end the file transfer thread and
		// therefore trigger an event used to continue.
	}
}

void HostedRoomFrame::OnRefuseButtonClick(wxCommandEvent& WXUNUSED(event))
{
	long l_item = m_hostplayerslistCtrl->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);

#ifndef __WXMSW__
	SetInGame(false);
	if (m_is_advertised)
		SendRoomInfo(false);
	m_allow_sounds = true;
#endif

	if (!l_item) // Equals 0? If yes, without that there may be a problem in case m_transfer_client_index == 0.
		l_item = m_hostplayerslistCtrl->GetNextItem(0, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
	while (l_item >= 0)
	{
		// We remove requests only for clients which are NOT in a middle of a transfer.
		// The expression (unsigned long) is provided so no warning is produced by g++.
		if ((m_transfer_state == TRANSFERSTATE_NONE) || (m_transfer_client_index != (unsigned long)l_item))
		{
			m_players_table.requests[l_item].transferrequest = TRANSFERREQUEST_NONE;
			g_SendCStringToSocket(m_host_clients_table.clients[l_item].sock, "1:requestrefuse:");
			m_hostplayerslistCtrl->SetItemTextColour(l_item, m_def_listctrl_color);
			m_hostplayerslistCtrl->SetItem(l_item, COL_HOSTREQUESTS, wxEmptyString);
			m_hostplayerslistCtrl->SetItem(l_item, COL_HOSTFILENAMES, wxEmptyString);
			m_hostplayerslistCtrl->SetItem(l_item, COL_HOSTFILESIZES, wxEmptyString);
			//    RefreshListCtrlSize();
		}
		l_item = m_hostplayerslistCtrl->GetNextItem(l_item, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
	}
}

void HostedRoomFrame::OnTransferThreadEnd(wxCommandEvent& WXUNUSED(event))
{
	size_t l_loop_var;
	wxString l_filename;
	wxFile file;
	char *fileptr;
	unsigned long filesize;
	DWORD l_crc32;
	wxArrayString l_str_list;
	bool check_for_download_request = true;
	m_transfer_timer->Stop();
	delete m_transfer_timer;

	if (m_server != NULL) // Not closing the room?
	{
		m_hosttransfergauge->SetValue(0);

		if (m_transfer_client_index) // If it's 0, it means the client has been deleted from list.
		{
			m_players_table.requests[m_transfer_client_index].transferrequest = TRANSFERREQUEST_NONE;
			m_hostplayerslistCtrl->SetItemTextColour(m_transfer_client_index, m_def_listctrl_color);
			m_hostplayerslistCtrl->SetItem(m_transfer_client_index, COL_HOSTREQUESTS, wxEmptyString);
			m_hostplayerslistCtrl->SetItem(m_transfer_client_index, COL_HOSTFILENAMES, wxEmptyString);
			m_hostplayerslistCtrl->SetItem(m_transfer_client_index, COL_HOSTFILESIZES, wxEmptyString);
			//    RefreshListCtrlSize();
		}
	}
	m_transfer_fp->Close();
	delete m_transfer_fp;
	m_transfer_fp = NULL;

	if (!m_transfer_is_download)
	{
		switch (m_game)
		{
			case GAME_BLOOD:    l_filename = g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_DESCENT:  l_filename = g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_DESCENT2: l_filename = g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_DN3D:     l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			case GAME_SW:       l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename; break;
			default: ;
		}

		filesize = (size_t)wxFileName::GetSize(l_filename).ToULong();

		if (file.Access(l_filename, wxFile::read) && file.Open(l_filename, wxFile::read))
		{
			fileptr = new char[filesize];
			file.Read(fileptr, filesize);
			file.Close();

			l_crc32 = crc32buf(fileptr, filesize);

			delete[] fileptr;
		}
		else
			wxLogError(wxT("Can't open file for reading!"));

		// Also make sure it's a client still in list and not deleted,
		// and that the host hasn't closed the room.
		if ((m_transfer_state == TRANSFERSTATE_BUSY) &&
			(m_transfer_client_index) && (m_server != NULL) &&
			((long)filesize == m_players_table.requests[m_transfer_client_index].filesize) && l_crc32 == m_transfer_crc32)
		{
			check_for_download_request = false;

			AddMessage(wxT("* The \"") + m_transfer_filename + wxString::Format(wxT("\" file has been received successfully. (Size: %lu / CRC32: %08X)"), filesize, (unsigned)m_transfer_crc32), true);

			l_str_list.Add(wxT("filetransfersuccessful"));
			l_str_list.Add(m_transfer_filename);
			g_SendStringListToSocket(m_host_clients_table.clients[m_transfer_client_index].sock, l_str_list);

			if (m_game != GAME_DESCENT && m_game != GAME_DESCENT2)
			{
				m_usermap = m_transfer_filename;
				m_launch_usermap = true;
				s_map = m_usermap;
				UpdateSettingsList();
			}

			l_str_list = GetServerInfoSockStrList();
			for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
			{
				if (m_players_table.requests[l_loop_var].transferrequest != TRANSFERREQUEST_NONE)
				{
					m_players_table.requests[l_loop_var].transferrequest = TRANSFERREQUEST_NONE;
					g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:requestrefuse:");
					m_hostplayerslistCtrl->SetItemTextColour(l_loop_var, m_def_listctrl_color);
					m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTREQUESTS, wxEmptyString);
					m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTFILENAMES, wxEmptyString);
					m_hostplayerslistCtrl->SetItem(l_loop_var, COL_HOSTFILESIZES, wxEmptyString);
					//        RefreshListCtrlSize();
				}
				g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
			}
		}
		else
		{
			if (wxFileExists(l_filename))
				wxRemoveFile(l_filename);

			if ((long)filesize == m_players_table.requests[m_transfer_client_index].filesize)
				wxMessageBox(wxT("The received \"") + m_transfer_filename + wxString::Format(wxT("\" file is corrupted :\n\nExpected CRC32: %08X\n   Found CRC32: %08X\n\nPlease try to download it again."), (unsigned)m_transfer_crc32, (unsigned)l_crc32),
				wxT("Corrupted file received"), wxOK|wxICON_EXCLAMATION);
		}
	}
	m_transfer_state = TRANSFERSTATE_NONE;

	if (m_server == NULL) // Close the room?
	{
		Destroy();
		return;
	}

	if (check_for_download_request)
		FindNextDownloadRequest();
	else
	{
		m_hosttransferstaticText->SetLabel(wxEmptyString);
		m_hosttransferdynamicbutton->SetLabel(wxT("Accept selections"));
		m_hostlaunchbutton->Enable();
	}
}

void HostedRoomFrame::OnBanListClose(wxCommandEvent& WXUNUSED(event))
{
	m_banned_ips_array = m_banlist_dialog->m_banlistBox->GetStrings();
	m_banned_ips_hash.clear();
	l_CopyStringArrayToHash(m_banned_ips_array, m_banned_ips_hash);
	if (!l_SaveBanListToFile(m_banned_ips_array))
		wxLogError(wxT("ERROR: Couldn't saved updated ban list to file ") + g_configuration->banlist_filepath);
}

void HostedRoomFrame::SetChatStyle()
{
	wxFont l_font;
	wxColour l_text, l_back;
	YANGFrame* l_frame = new YANGFrame(NULL, wxID_ANY, wxEmptyString);
	YANGPanel* l_panel = new YANGPanel( l_frame, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	YANGTextCtrl* l_textctrl = new YANGTextCtrl(l_panel, wxID_ANY);
	if (g_configuration->use_custom_chat_text_layout)
	{
		l_font.SetFaceName(g_configuration->chat_font);
		l_font.SetPointSize(g_configuration->chat_font_size);
	}
	else
		l_font = l_textctrl->GetFont();
	if (g_configuration->use_custom_chat_text_colors)
	{
		l_text = (wxColour(g_configuration->chat_textcolor_red,
			g_configuration->chat_textcolor_green,
			g_configuration->chat_textcolor_blue));
		l_back = (wxColour(g_configuration->chat_backcolor_red,
			g_configuration->chat_backcolor_green,
			g_configuration->chat_backcolor_blue));
	}
	else
	{
		l_text = l_textctrl->GetForegroundColour();
		l_back = l_textctrl->GetBackgroundColour();
	}
	m_hostoutputtextCtrl->SetForegroundColour(l_text);
	m_hostoutputtextCtrl->SetBackgroundColour(l_back);
	m_hostoutputtextCtrl->SetFont(l_font);
#ifdef __WXMSW__
	m_hostoutputtextCtrl->SetDefaultStyle(wxTextAttr(l_text, l_back, l_font));
	m_hostoutputtextCtrl->SetStyle(0, m_hostoutputtextCtrl->GetLastPosition(), wxTextAttr(l_text, l_back, l_font));
#endif
	m_hostoutputtextCtrl->SetValue(m_hostoutputtextCtrl->GetValue());
	m_hostinputtextCtrl->SetForegroundColour(l_text);
	m_hostinputtextCtrl->SetBackgroundColour(l_back);
	m_hostinputtextCtrl->SetFont(l_font);
	m_hostinputtextCtrl->SetValue(m_hostinputtextCtrl->GetValue());
	m_hostinputtextCtrl->Refresh();

	l_frame->Destroy();
}

void HostedRoomFrame::OnLookAndFeelClose(wxCommandEvent& WXUNUSED(event))
{
	SetChatStyle();
}

void HostedRoomFrame::OnTimer(wxTimerEvent& WXUNUSED(event))
{
	size_t l_loop_var = 1;
	while (l_loop_var < m_host_clients_table.num_clients)
		if ((m_host_clients_table.clients[l_loop_var].stopwatch) && (m_host_clients_table.clients[l_loop_var].stopwatch->Time() > 60000))
		{
			delete m_host_clients_table.clients[l_loop_var].stopwatch;
			g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "2:error:Timed out while waiting for joining:");
			m_host_clients_table.clients[l_loop_var].sock->Destroy();
			m_host_clients_table.DelByIndex(l_loop_var);
			// Don't increase l_loop_var as indices have been shifted
		}
		else
			l_loop_var++;
	// This is also a good chance to clear the following structures
	// ("queues"), holding join requests and IPs for ban checks.
	// Basically, we remove older elements until we get "small" structures.
	while (m_join_requests_arr.GetCount() > 12)
	{	// IMPORTANT: All arrays MUST have EXACTLY THE SAME SIZE! (i.e. num. of items)
		m_join_requests_arr.RemoveAt(0);
		m_join_requests_sock_arr.RemoveAt(0);
		m_join_requests_address_arr.RemoveAt(0);
	}
	while (m_unbanned_ips_arr.GetCount() > 12)
		m_unbanned_ips_arr.RemoveAt(0);
	m_connection_timeout_timer->Start(5000, wxTIMER_ONE_SHOT);
}

void HostedRoomFrame::OnFirstPingTimer(wxTimerEvent& WXUNUSED(event))
{
	wxArrayString l_str_list;
	if ((!m_pinging) && (m_players_table.num_players > 1))
	{
		m_pinging = 1;
		m_ping_index = m_players_table.num_players-1;
		m_ping_id = rand();
		l_str_list.Add(wxT("pingquery"));
		l_str_list.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
		g_SendStringListToSocket(m_host_clients_table.clients[m_ping_index].sock, l_str_list);
		m_ping_times = m_max_ping = 0;
		m_min_ping = 999;
		m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
		ping.Start( 0 );
	}
}

void HostedRoomFrame::OnPingTimer(wxTimerEvent& WXUNUSED(event))
{
	wxArrayString l_str_ping;

	if ((m_ping_timer != NULL) && (m_ping_index > 0))
	{
		m_hostplayerslistCtrl->SetItem(m_ping_index, COL_HOSTPING, wxT("999"));
		m_hostplayerslistCtrl->SetItem(m_ping_index, COL_HOSTFLUX, wxEmptyString);
#if (defined __WXMAC__) || (defined __WXCOCOA__)
		m_hostplayerslistCtrl->RefreshItem(m_ping_index);
#endif
		if (m_ping_index < (signed)m_players_table.num_players -1)
		{
			m_ping_index++;
			m_ping_id = rand();
			l_str_ping.Empty();
			l_str_ping.Add(wxT("pingquery"));
			l_str_ping.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
			g_SendStringListToSocket(m_host_clients_table.clients[m_ping_index].sock, l_str_ping);
			m_ping_times = m_max_ping = 0;
			m_min_ping = 999;
			m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
			ping.Start( 0 );
		}
		else
		{
			m_pinging = 0;
			m_ping_index = -1;
		}
	}
}

void HostedRoomFrame::OnCheckPingTimer(wxTimerEvent& WXUNUSED(event))
{
	wxArrayString l_str_list;

	if (m_checkping_timer != NULL)
	{
		AddMessage(wxT("* ") + m_host_clients_table.clients[m_checkping_index].nickname + wxT(" from ") + wxString(GeoIP_country_name_by_addr(g_geoip_db,m_host_clients_table.clients[m_checkping_index].peerdetectedip.mb_str(wxConvUTF8)), wxConvUTF8) + wxString::Format(wxT(" has tried to join the room with a Ping of %ld ms."), (long)checkping.Time()), true);
		delete m_host_clients_table.clients[m_checkping_index].stopwatch;
		//		m_host_clients_table.clients[m_checkping_index].timer->Stop();
		//		delete m_host_clients_table.clients[m_checkping_index].timer;
		l_str_list.Empty();
		l_str_list.Add(wxT("error"));
		l_str_list.Add(wxString::Format(wxT("You couldn't connect to the gameroom, because the host hasn't received any reply\nfrom you, after 1 second (the Max Ping specified by the host is %ld ms).\n\nPlease, make sure there isn't any download or upload running on your computer,\nor on the other computer(s) if you've got a shared Internet connection,\nas it results in Lag spikes, and then try to connect again.\n\nNote : The Ping is the Latency, and so, the rule is \"the Lower, the Better\"."), (long int)m_maxping));
		g_SendStringListToSocket(m_host_clients_table.clients[m_checkping_index].sock, l_str_list);
		m_host_clients_table.clients[m_checkping_index].sock->Destroy();
		m_host_clients_table.DelByIndex(m_checkping_index);
		m_checkping_index = -1;
	}
}

void HostedRoomFrame::OnFileTransferTimer(wxTimerEvent& WXUNUSED(event))
{
	wxFileOffset l_position;
	if (m_transfer_fp)
	{
		l_position = m_transfer_fp->Tell();
		m_hosttransfergauge->SetValue(l_position);
		if (m_transfer_is_download)
			// Temporary wrapper for a possible bug with MinGW?
#if (defined __WXMSW__) && (wxCHECK_VERSION(2, 8, 9)) && (!wxCHECK_VERSION(2, 8, 10))
			m_hosttransferstaticText->SetLabel(m_usermap + wxString::Format(wxT(": %lld / "), (long long)l_position)
			+ wxString::Format(wxT("%lld"), (long long)m_hosttransfergauge->GetRange()));
		else
			m_hosttransferstaticText->SetLabel(m_transfer_filename + wxString::Format(wxT(": %lld / "), (long long)l_position)
			+ wxString::Format(wxT("%lld"), (long long)m_hosttransfergauge->GetRange()));
#else
			m_hosttransferstaticText->SetLabel(m_usermap + wxString::Format(wxT(": %lld / %lld"), (long long)l_position, (long long)m_hosttransfergauge->GetRange()));
		else
			m_hosttransferstaticText->SetLabel(m_transfer_filename + wxString::Format(wxT(": %lld / %lld"), (long long)l_position, (long long)m_hosttransfergauge->GetRange()));
#endif
	}
}

void HostedRoomFrame::OnServerEvent(wxSocketEvent& WXUNUSED(event))
{
	if (!m_server) // Shutdown?
		return;
	wxSocketBase* l_sock = m_server->Accept(false);
	wxArrayString l_str_list;
	wxIPV4address l_sock_addr;
	l_sock->GetPeer(l_sock_addr);
	// While the room may be full, it may just be a file transfer socket
	// from the same address of an already connected client.
	if ((m_host_clients_table.num_clients > MAX_NUM_CONNECTED_CLIENTS) ||
		((m_host_clients_table.num_clients == MAX_NUM_CONNECTED_CLIENTS)
		&& ((m_transfer_state != TRANSFERSTATE_PENDING) ||
		(l_sock_addr.IPAddress() != m_transfer_ipaddress))))
		// Destroy socket right away - don't even try sending any error
		// message. Maybe someone tries to connect a lot of times...
		l_sock->Destroy();
	else
	{
		l_sock->SetTimeout(1);
		// If the room is advertised, enforce global ban early
		if (m_is_advertised)
		{
			//l_str_list.Empty();
			l_str_list.Add(wxT("globanquery"));
			l_str_list.Add(g_EncryptForCommText(l_sock_addr.IPAddress(), COMM_KEY_IPLIST_FROM_HOST_TO_MASTER));
			g_SendStringListToSocket(m_client, l_str_list);
		}
		//  l_sock->GetPeer(l_sock_addr);
		m_host_clients_table.Add(l_sock, l_sock_addr.IPAddress(), new wxStopWatch());
		//  m_host_clients_table.clients[m_host_clients_table.num_clients-1].timer->Start(60000, wxTIMER_ONE_SHOT);
		/*
		m_players_table.Add(l_sock, wxEmptyString, l_sock_addr.IPAddress(),
		wxEmptyString, 0, false);
		*/
		// Tell the new socket how and where to process its events
		l_sock->SetEventHandler(*this, ID_HOSTSOCKET);
		l_sock->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
		l_sock->SetNotify(wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
		l_sock->Notify(true);
	}
}

// String array format:
// "join" (FIXME: Remove this! But then you should FIX ALL ARRAY INDICES),
// expected YANG_STR_NET_VERSION, peer detected IP,
// self detected IP (e.g. local IP), port number, operating system.
void HostedRoomFrame::AcceptClientJoin(wxSocketBase* sock, const wxArrayString& client_join_request)
{
	unsigned long l_index, l_loop_var;
	for (l_index = 0; l_index < m_host_clients_table.num_clients; l_index++)
		if (sock == m_host_clients_table.clients[l_index].sock)
			break;
	if (l_index == m_host_clients_table.num_clients)
		return;

	long l_temp_num;
	wxArrayString l_str_list;
	const char * returnedCountry;
	wxIPV4address l_sock_addr;
	
	delete m_host_clients_table.clients[l_index].stopwatch;
	m_host_clients_table.clients[l_index].stopwatch = NULL;
	//                m_host_clients_table.clients[l_index].timer->Stop();
	//                delete m_host_clients_table.clients[l_index].timer;
	if (m_players_table.num_players == m_max_num_players)
	{
		m_host_clients_table.DelByIndex(l_index);
		g_SendCStringToSocket(sock, "2:error:Room is full.:");
		sock->Destroy();
		return;
	}
	if (client_join_request[0] != YANG_STR_NET_VERSION)
	{
		m_host_clients_table.DelByIndex(l_index);
		g_SendCStringToSocket(sock, "2:error:Server is hosted using an incompatible version of YANG.:");
		sock->Destroy();
		return;
	}
	if (m_host_clients_table.clients[l_index].nickname.Len() > NICK_MAX_LENGTH)
	{
		m_host_clients_table.DelByIndex(l_index);
//		l_str_list.Empty();
		l_str_list.Add(wxT("error"));
		l_str_list.Add(wxString::Format(wxT("Your configured nickname is too long. It shouldn't exceed %d characters."), NICK_MAX_LENGTH));
		g_SendStringListToSocket(sock, l_str_list);
		sock->Destroy();
		return;
	}
	// That way all players in room appear in the beginning of the array.
	// That's to avoid mixing the orders
	// while clients are not aware of that.  
	m_host_clients_table.PutInRoom(l_index, m_players_table.num_players);
	// The index is now moved to the new location in the players list.
	l_index = m_players_table.num_players;
	m_host_clients_table.clients[l_index].selfdetectedip = client_join_request[2];
	if ((m_host_clients_table.clients[0].peerdetectedip).IsEmpty())
	{
		m_host_clients_table.clients[0].peerdetectedip = client_join_request[1];

		if (g_geoip_db != NULL)
		{
			returnedCountry = GeoIP_country_code_by_addr(g_geoip_db,m_host_clients_table.clients[0].peerdetectedip.mb_str(wxConvUTF8));
			if (returnedCountry != NULL)
				m_ImageList->Add(getFlag(returnedCountry));
			else
				m_ImageList->Add(wxIcon(unknown));

			m_hostplayerslistCtrl->SetItem(0, COL_HOSTLOCATION, wxString(GeoIP_country_name_by_addr(g_geoip_db,m_host_clients_table.clients[0].peerdetectedip.mb_str(wxConvUTF8)), wxConvUTF8), IMG_HOSTNUMBER);
		}

		m_hostplayerslistCtrl->SetItem(0, COL_HOSTDETECTED_IPS, client_join_request[1]);
		sock->GetLocal(l_sock_addr);
		m_host_clients_table.clients[0].selfdetectedip = l_sock_addr.IPAddress();
		m_players_table.players[0].ingameip = m_host_clients_table.clients[0].selfdetectedip;
		m_hostplayerslistCtrl->SetItem(0, COL_HOSTINGAME_IPS, m_players_table.players[0].ingameip +
			wxString::Format(wxT(":%d"), (int)g_configuration->game_port_number));
	}
	client_join_request[3].ToLong(&l_temp_num);
	if (g_configuration->enable_localip_optimization &&
		(client_join_request[1] == m_host_clients_table.clients[l_index].peerdetectedip) &&
		(client_join_request[1] == m_host_clients_table.clients[0].peerdetectedip))
		m_players_table.Add(m_host_clients_table.clients[l_index].nickname, m_host_clients_table.clients[l_index].selfdetectedip, l_temp_num, client_join_request[4], false);
	// This case is useful if someone joins by local IP, with a room of both Internet and local players.
	else if ((client_join_request[1] == m_host_clients_table.clients[0].selfdetectedip) &&
		(m_host_clients_table.clients[0].peerdetectedip != m_host_clients_table.clients[0].selfdetectedip))
	{ // Host and new client should have the same external IP.
		m_host_clients_table.clients[l_index].peerdetectedip = m_host_clients_table.clients[0].peerdetectedip;
		m_players_table.Add(m_host_clients_table.clients[l_index].nickname, m_host_clients_table.clients[l_index].selfdetectedip, l_temp_num, client_join_request[4], false);
	}
	else
		m_players_table.Add(m_host_clients_table.clients[l_index].nickname, m_host_clients_table.clients[l_index].peerdetectedip, l_temp_num, client_join_request[4], false);

	m_hostplayerslistCtrl->InsertItem(l_index, IMG_HOSTCANCEL);

	if (g_geoip_db != NULL)
	{
		returnedCountry = GeoIP_country_code_by_addr(g_geoip_db,m_host_clients_table.clients[l_index].peerdetectedip.mb_str(wxConvUTF8));
		if (returnedCountry != NULL)
			m_ImageList->Add(getFlag(returnedCountry));
		else
			m_ImageList->Add(wxIcon(unknown));

		m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTLOCATION, wxString(GeoIP_country_name_by_addr(g_geoip_db,m_host_clients_table.clients[l_index].peerdetectedip.mb_str(wxConvUTF8)), wxConvUTF8), l_index+IMG_HOSTNUMBER);
	}

	m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTNICKNAMES, m_players_table.players[l_index].nickname);
	m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTOS, client_join_request[4]);
	m_hostplayerslistCtrl->SetItemTextColour(l_index, m_def_listctrl_color);
	m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTDETECTED_IPS, m_host_clients_table.clients[l_index].peerdetectedip);
	m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTINGAME_IPS, m_players_table.players[l_index].ingameip + wxT(':') + client_join_request[3]);
	//                RefreshListCtrlSize();

	AddMessage(wxT("* ") + m_players_table.players[l_index].nickname + wxT(" has joined the room."), true);
	if (m_allow_sounds && g_configuration->play_snd_join)
		g_PlaySound(g_configuration->snd_join_file);
	s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);
	UpdateSettingsList();
	// l_index should now be the latest, or the number of in-room players minus 1.
	for (l_loop_var = 1; l_loop_var < l_index; l_loop_var++)
	{
//		l_str_list.Empty();
		l_str_list.Add(wxT("joined"));
		l_str_list.Add(m_players_table.players[l_index].nickname);
		l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_index].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));
		if (g_configuration->enable_localip_optimization &&
			(m_host_clients_table.clients[l_index].peerdetectedip == m_host_clients_table.clients[l_loop_var].peerdetectedip))
			l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_index].selfdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));
		else
			l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_index].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));
		l_str_list.Add(wxString::Format(wxT("%d"), (int)m_players_table.players[l_index].game_port_number));
		l_str_list.Add(m_players_table.players[l_index].os_description);
		g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
		l_str_list.Empty();
	}
	if (m_is_advertised)
	{
//		l_str_list.Empty();
		l_str_list.Add(wxT("players"));
		for (l_loop_var = 0; l_loop_var <= l_index; l_loop_var++)
		{
			l_str_list.Add(m_players_table.players[l_loop_var].nickname);
			l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_loop_var].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_MASTER));
			l_str_list.Add(m_players_table.players[l_loop_var].os_description);
		}
		g_SendStringListToSocket(m_client, l_str_list);
		l_str_list.Empty();
	}
	// Sending a few things for initializations:
	// - A value telling if in-game joining is support (relevant to custom DOS games only)
	// - The player table
//	l_str_list.Empty();
	l_str_list.Add(wxT("roominit"));
	if (m_game == GAME_CUSTOM && m_allow_ingame_joining)
		l_str_list.Add(wxT("yes"));
	else
		l_str_list.Add(wxT("no"));
	for (l_loop_var = 0; l_loop_var < l_index; l_loop_var++)
	{
		l_str_list.Add(m_players_table.players[l_loop_var].nickname);
		l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_loop_var].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));
		if (g_configuration->enable_localip_optimization &&
			(m_host_clients_table.clients[l_index].peerdetectedip == m_host_clients_table.clients[l_loop_var].peerdetectedip))
			l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_loop_var].selfdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));
		else
			l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_loop_var].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));
		l_str_list.Add(wxString::Format(wxT("%d"), (int)m_players_table.players[l_loop_var].game_port_number));
		l_str_list.Add(m_players_table.players[l_loop_var].os_description);
		l_str_list.Add(wxString::Format(wxT("%d"), (int)m_players_table.players[l_loop_var].ready));
	}
	l_str_list.Add(m_players_table.players[l_index].nickname);
	l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_index].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));
	l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_index].selfdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));
	l_str_list.Add(wxString::Format(wxT("%d"), (int)m_players_table.players[l_index].game_port_number));
	l_str_list.Add(m_players_table.players[l_index].os_description);
	l_str_list.Add(wxString::Format(wxT("%d"), (int)m_players_table.players[l_index].ready));

	g_SendStringListToSocket(sock, l_str_list);

	if (!m_modname.IsEmpty())
	{
		l_str_list = GetServerModFilesSockStrList();
		g_SendStringListToSocket(sock, l_str_list);
	}

	m_firstping_timer->Start(500, wxTIMER_ONE_SHOT);
	/*                if (!m_pinging)
	{
	m_pinging = 1;
	m_ping_index = l_index;
	m_ping_id = rand();
	l_str_list.Empty();
	l_str_list.Add(wxT("pingquery"));
	l_str_list.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
	g_SendStringListToSocket(sock, l_str_list);
	m_ping_times = m_max_ping = 0;
	m_min_ping = 999;
	m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
	ping.Start( 0 );
	}*/
}

void HostedRoomFrame::OnSocketEvent(wxSocketEvent& event)
{
	if (!m_server) // Shutdown?
		return;
	wxSocketBase *l_sock = event.GetSocket();
	wxArrayString l_str_list;
	size_t l_num_of_items,
		l_index = m_host_clients_table.FindIndexBySock(l_sock),
		l_loop_var, l_num_of_read_attempts;
	long l_temp_num;
	char l_short_buffer[MAX_COMM_TEXT_LENGTH];
	wxString l_long_buffer, l_filename, l_addrstr;
	wxIPV4address l_sock_addr;
	wxFile extra_usermap;
	char *fileptr;
	unsigned long filesize, l_temp_ulong;
	if (l_index < m_host_clients_table.num_clients)
	{
		l_sock->GetPeer(l_sock_addr);
		l_addrstr = l_sock_addr.IPAddress();
		switch (event.GetSocketEvent())
		{
			case wxSOCKET_INPUT:
				l_num_of_read_attempts = 0;
				do
				{
					l_sock->Read(l_short_buffer, MAX_COMM_TEXT_LENGTH);
					/*        if (l_sock->Error())
					{
					AddMessage(wxString::Format(wxT("* A communication error has occured while trying to fetch data: %d"), (int)l_sock->LastError()));
					if (m_allow_sounds && g_configuration->play_snd_error)
					g_PlaySound(g_configuration->snd_error_file);
					break;
					}*/
					l_temp_num = l_sock->LastCount();
					if (l_temp_num < MAX_COMM_TEXT_LENGTH)
						l_num_of_read_attempts = MAX_COMM_READ_ATTEMPTS;
					else
						l_num_of_read_attempts++;
					l_long_buffer << wxString(l_short_buffer, wxConvUTF8, l_temp_num);
					l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
					while (l_num_of_items)
					{
						if ((l_num_of_items == 2) && (l_str_list[0] == wxT("pingquery")))
						{
							l_str_list.RemoveAt(0);
							l_str_list.Insert(wxT("pingreply"), 0);
							g_SendStringListToSocket(l_sock, l_str_list);
						}
						else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("pingreply")) && (l_str_list[1] == wxString::Format(wxT("%d"), (int)m_ping_id)))
						{
							if (m_ping_index == (signed)l_index)
							{
								ping.Pause();
								m_ping_timer->Stop();

								if (ping.Time() < m_min_ping)
									m_min_ping = ping.Time();

								if (ping.Time() > m_max_ping)
									m_max_ping = ping.Time();

								m_ping_times++;

								if (m_ping_times < 4)
								{
									m_ping_id = rand();
									l_str_list.Empty();
									l_str_list.Add(wxT("pingquery"));
									l_str_list.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
									g_SendStringListToSocket(l_sock, l_str_list);
									m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
									ping.Start( 0 );
								}
								else
								{
									m_hostplayerslistCtrl->SetItem(m_ping_index, COL_HOSTPING, wxString::Format(wxT("%ld"), (long)(m_max_ping < 999 ? m_max_ping : 999)));

									if (m_max_ping < 999)
										m_hostplayerslistCtrl->SetItem(m_ping_index, COL_HOSTFLUX, wxString::Format(wxT("%c"), 177) + wxString::Format(wxT("%ld"), (long)(m_max_ping-m_min_ping <= 99 ? m_max_ping-m_min_ping : 99)));
									else
										m_hostplayerslistCtrl->SetItem(m_ping_index, COL_HOSTFLUX, wxEmptyString);

#if (defined __WXMAC__) || (defined __WXCOCOA__)
									m_hostplayerslistCtrl->RefreshItem(m_ping_index);
#endif
									if (m_ping_index < (signed)m_players_table.num_players -1)
									{
										m_ping_index++;
										m_ping_id = rand();
										l_str_list.Empty();
										l_str_list.Add(wxT("pingquery"));
										l_str_list.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
										g_SendStringListToSocket(m_host_clients_table.clients[m_ping_index].sock, l_str_list);
										m_ping_times = m_max_ping = 0;
										m_min_ping = 999;
										m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
										ping.Start( 0 );
									}
									else
									{
										m_pinging = 0;
										m_ping_index = -1;
									}
								}
							}
						}
						else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("checkpingreply")))
						{
							if ((m_checkping_index == (signed)l_index) && (l_str_list[1] == wxString::Format(wxT("%d"), (int)m_checkping_id)))
							{
								checkping.Pause();
								m_checkping_timer->Stop();

								if (checkping.Time() > m_maxping)
								{
									m_checkping_index = -1;
									AddMessage(wxT("* ") + m_host_clients_table.clients[l_index].nickname + wxT(" from ") + wxString(GeoIP_country_name_by_addr(g_geoip_db,m_host_clients_table.clients[l_index].peerdetectedip.mb_str(wxConvUTF8)), wxConvUTF8) + wxString::Format(wxT(" has tried to join the room with a Ping of %d ms."), (int)checkping.Time()), true);
									delete m_host_clients_table.clients[l_index].stopwatch;
									//                m_host_clients_table.clients[l_index].timer->Stop();
									//                delete m_host_clients_table.clients[l_index].timer;
									m_host_clients_table.DelByIndex(l_index);
									l_str_list.Empty();
									l_str_list.Add(wxT("error"));
									l_str_list.Add(wxString::Format(wxT("You couldn't connect to the gameroom, because your Ping with the host (%d ms)\nis higher than the Max Ping specified by the host (%d ms).\n\nPlease, make sure there isn't any download or upload running on your computer,\nor on the other computer(s) if you've got a shared Internet connection,\nas it results in Lag spikes, and then try to connect again.\n\nNote : The Ping is the Latency, and so, the rule is \"the Lower, the Better\"."), (int)checkping.Time(), (int)m_maxping));
									g_SendStringListToSocket(l_sock, l_str_list);
									l_sock->Destroy();
									break;
								}

								m_checkping_times++;

								if (m_checkping_times < 8)
								{
									m_checkping_id = rand();
									l_str_list.Empty();
									l_str_list.Add(wxT("checkpingquery"));
									l_str_list.Add(wxString::Format(wxT("%d"), (int)m_checkping_id));
									g_SendStringListToSocket(l_sock, l_str_list);
									m_checkping_timer->Start(999, wxTIMER_ONE_SHOT);
									checkping.Start( 0 );
								}
								else
								{
									m_checkping_index = -1;
									g_SendStringListToSocket(l_sock, GetServerInfoSockStrList());
								}
							}
							else
							{
								if (m_checkping_index == -1)
								{
									m_checkping_index = l_index;
									m_checkping_times = 0;
									m_checkping_timer->Start(999, wxTIMER_ONE_SHOT);
									checkping.Start( 0 );
									m_checkping_id = rand();
								}
								l_str_list.Empty();
								l_str_list.Add(wxT("checkpingquery"));
								l_str_list.Add(wxString::Format(wxT("%d"), (int)m_checkping_id));
								g_SendStringListToSocket(l_sock, l_str_list);
							}
						}
						else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("requestroominfo")))
						{
							m_host_clients_table.DelByIndex(l_index);
							g_SendCStringToSocket(l_sock, "2:error:Server is hosted using an incompatible version of YANG.:");
							l_sock->Destroy();
							break;
						}
						else if ((l_num_of_items == 3) && (l_str_list[0] == wxT("requestroominfo")))
						{
							if (l_str_list[1] != YANG_STR_NET_VERSION)
							{
								m_host_clients_table.DelByIndex(l_index);
								g_SendCStringToSocket(l_sock, "2:error:Server is hosted using an incompatible version of YANG.:");
								l_sock->Destroy();
								break;
							}
							m_host_clients_table.clients[l_index].nickname = l_str_list[2];
							if (IsStringInHash(m_host_clients_table.clients[l_index].peerdetectedip, m_banned_ips_hash))
							{
								delete m_host_clients_table.clients[l_index].stopwatch;
								//              m_host_clients_table.clients[l_index].timer->Stop();
								//              delete m_host_clients_table.clients[l_index].timer;
								m_host_clients_table.DelByIndex(l_index);
								g_SendCStringToSocket(l_sock, "2:error:You're banned from this room.:");
								l_sock->Destroy();
								break;
							}
							if (m_players_table.FindIndexByNick(m_host_clients_table.clients[l_index].nickname) != m_players_table.num_players)
							{
								m_host_clients_table.DelByIndex(l_index);
								g_SendCStringToSocket(l_sock, "2:error:Your configured nickname is already in use.:");
								l_sock->Destroy();
								break;
							}
							if (m_use_password)
								g_SendCStringToSocket(l_sock, "1:requestpass:");
							else
							{
								delete m_host_clients_table.clients[l_index].stopwatch;
								m_host_clients_table.clients[l_index].stopwatch = NULL;
								//              m_host_clients_table.clients[l_index].timer->Stop();
								if (m_maxping)
								{
									if (m_checkping_index == -1) // New client to check its ping.
									{
										m_checkping_index = l_index;
										m_checkping_times = 0;
										m_checkping_timer->Start(999, wxTIMER_ONE_SHOT);
										checkping.Start( 0 );
										m_checkping_id = rand();
									} // Otherwise there's another pending client, but send anyway.
									l_str_list.Empty();
									l_str_list.Add(wxT("checkpingquery"));
									l_str_list.Add(wxString::Format(wxT("%d"), (int)m_checkping_id));
									g_SendStringListToSocket(l_sock, l_str_list);
								}
								else
									g_SendStringListToSocket(l_sock, GetServerInfoSockStrList());
							}
						}
						// Clients not in room only.
						else if (m_players_table.num_players <= l_index)
						{
							if ((l_num_of_items == 2) && (l_str_list[0] == wxT("password")))
							{
								if ((!m_use_password) || (m_password == l_str_list[1]))
								{
									delete m_host_clients_table.clients[l_index].stopwatch;
									m_host_clients_table.clients[l_index].stopwatch = NULL;
									//                  m_host_clients_table.clients[l_index].timer->Stop();
									if (m_maxping)
									{
										if (m_checkping_index == -1) // New client to check its ping.
										{
											m_checkping_index = l_index;
											m_checkping_times = 0;
											m_checkping_timer->Start(999, wxTIMER_ONE_SHOT);
											checkping.Start( 0 );
											m_checkping_id = rand();
										} // Otherwise there's another pending client, but send anyway.
										l_str_list.Empty();
										l_str_list.Add(wxT("checkpingquery"));
										l_str_list.Add(wxString::Format(wxT("%d"), (int)m_checkping_id));
										g_SendStringListToSocket(l_sock, l_str_list);
									}
									else
										g_SendStringListToSocket(l_sock, GetServerInfoSockStrList());
								}
								else if (m_use_password) // Wrong password provided, ask again.
									g_SendCStringToSocket(l_sock, "1:requestpass:");
							}
							else if ((l_num_of_items == 6) && (l_str_list[0] == wxT("join")))
							{
								l_str_list.RemoveAt(0);

								l_str_list[1] = g_DecryptFromCommText(l_str_list[1], COMM_KEY_IPLIST_FROM_CLIENT_TO_HOST);
								l_str_list[2] = g_DecryptFromCommText(l_str_list[2], COMM_KEY_IPLIST_FROM_CLIENT_TO_HOST);

								if (m_is_advertised)
								{
									for (l_loop_var = 0; l_loop_var < m_unbanned_ips_arr.GetCount(); l_loop_var++)
										if (m_unbanned_ips_arr[l_loop_var] == l_addrstr)
										{
											m_unbanned_ips_arr.RemoveAt(l_loop_var);
											AcceptClientJoin(l_sock, l_str_list);
											l_loop_var--; // This is important, after the "for" loop!! (works even if unsigned...)
											break;
										}
									if (l_loop_var == m_unbanned_ips_arr.GetCount())
									{
										m_join_requests_arr.Add(l_str_list);
										m_join_requests_sock_arr.Add(l_sock);
										m_join_requests_address_arr.Add(l_addrstr);
									}
								}
								else
									AcceptClientJoin(l_sock, l_str_list);
							}
							else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("readyfortransfer")) &&
								 (m_transfer_state == TRANSFERSTATE_PENDING) && (l_addrstr == m_transfer_ipaddress))
							{
								delete m_host_clients_table.clients[l_index].stopwatch;
								m_host_clients_table.clients[l_index].stopwatch = NULL;
								//                  m_host_clients_table.clients[l_index].timer->Stop();
								//                  delete m_host_clients_table.clients[l_index].timer;
								m_host_clients_table.DelByIndex(l_index);
								l_sock->Notify(false);
								if (m_transfer_is_download)
								{
									switch (m_game)
									{
										case GAME_BLOOD:    l_filename = g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap; break;
										case GAME_DESCENT:  l_filename = g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap; break;
										case GAME_DESCENT2: l_filename = g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap; break;
										case GAME_DN3D:     l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap; break;
										case GAME_SW:       l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap; break;
										default: ;
									}

									m_transfer_fp = new wxFile(l_filename, wxFile::read);
									if (!m_transfer_fp->IsOpened())
									{
										wxLogError(wxT("Can't open file for reading!"));
										delete m_transfer_fp;    
										m_transfer_fp = NULL;
										l_sock->Destroy();
									}
									else
									{
										m_transfer_thread = new FileSendThread(l_sock, this, m_transfer_fp, &m_transfer_state);
										if ( m_transfer_thread->Create() != wxTHREAD_NO_ERROR )
										{
											wxLogError(wxT("Can't create file-transfer thread!"));
											m_transfer_thread->Delete();
											m_transfer_fp->Close();
											delete m_transfer_fp;
											m_transfer_fp = NULL;
											l_sock->Destroy();
										}
										else
										{
											m_transfer_state = TRANSFERSTATE_BUSY;
											m_transfer_thread->Run();
											m_hosttransfergauge->SetRange((wxFileName::GetSize(l_filename)).ToULong());
											m_transfer_timer = new wxTimer(this, ID_HOSTFILETRANSFERTIMER);
											m_transfer_timer->Start(500, wxTIMER_CONTINUOUS);
										}
									}
								}
								else
								{
									g_SendCStringToSocket(l_sock, "1:readyforupload:");

									switch (m_game)
									{
										case GAME_BLOOD:
											if (!wxDirExists(g_configuration->blood_maps_dir))
												wxFileName::Mkdir(g_configuration->blood_maps_dir, 0777, wxPATH_MKDIR_FULL);
											l_filename = g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;

											break;

										case GAME_DESCENT:
											if (!wxDirExists(g_configuration->descent_maps_dir))
												wxFileName::Mkdir(g_configuration->descent_maps_dir, 0777, wxPATH_MKDIR_FULL);
											l_filename = g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;

											break;

										case GAME_DESCENT2:
											if (!wxDirExists(g_configuration->descent2_maps_dir))
												wxFileName::Mkdir(g_configuration->descent2_maps_dir, 0777, wxPATH_MKDIR_FULL);
											l_filename = g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;

											break;

										case GAME_DN3D:
											if (!wxDirExists(g_configuration->dn3d_maps_dir))
												wxFileName::Mkdir(g_configuration->dn3d_maps_dir, 0777, wxPATH_MKDIR_FULL);
											l_filename = g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;

											break;

										case GAME_SW:
											if (!wxDirExists(g_configuration->sw_maps_dir))
												wxFileName::Mkdir(g_configuration->sw_maps_dir, 0777, wxPATH_MKDIR_FULL);
											l_filename = g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_transfer_filename;

											break;
										default: ;
									}

									// If file already exists, delete, as...
									if (wxFileExists(l_filename))
										wxRemoveFile(l_filename);
									// ...if current map selected by the host is the very same,
									// deselect and update all clients!
									// As long as the newly uploaded map isn't ready, don't assume it is!
									// The file transfer may also get aborted at some point,
									// so for now, no map should be selected.

									// Finally, the reason we don't just compare m_transfer_filename to m_usermap
									// is that, on certain operating systems, we'd have to make a case-insensitive
									// string comparison, while on others we'd have to make a case-sensitive one!

									// The following method should work as expected on all platforms,
									// at least in theory.
									if (m_launch_usermap &&
										(((m_game == GAME_BLOOD) && (!wxFileExists(g_configuration->blood_maps_dir + wxFILE_SEP_PATH + m_usermap)))
										|| ((m_game == GAME_DESCENT) && (!wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_usermap)))
										|| ((m_game == GAME_DESCENT2) && (!wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_usermap)))
										|| ((m_game == GAME_DN3D) && (!wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + m_usermap)))
										|| ((m_game == GAME_SW) && (!wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + m_usermap)))))
									{
										m_launch_usermap = false;
										m_epimap_num = 0;
										// For any game this should be none.
										s_map = (*g_dn3d_level_list)[g_GetDN3DLevelSelectionByNum(0)];
										UpdateSettingsList();
										l_str_list = GetServerInfoSockStrList();
										for (l_loop_var = 1; l_loop_var < m_host_clients_table.num_clients; l_loop_var++)
											g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
									}

									m_transfer_fp = new wxFile(l_filename, wxFile::write);
									if (!m_transfer_fp->IsOpened())
									{
										wxLogError(wxT("Can't open file for writing!"));
										delete m_transfer_fp;
										m_transfer_fp = NULL;
										l_sock->Destroy();
									}
									else
									{
										m_transfer_thread = new FileReceiveThread(l_sock, this, m_transfer_fp,
											m_players_table.requests[m_transfer_client_index].filesize,
											&m_transfer_state);
										if ( m_transfer_thread->Create() != wxTHREAD_NO_ERROR )
										{
											wxLogError(wxT("Can't create file-transfer thread!"));
											m_transfer_thread->Delete();
											m_transfer_fp->Close();
											delete m_transfer_fp;
											m_transfer_fp = NULL;
											l_sock->Destroy();
										}
										else
										{
											m_transfer_state = TRANSFERSTATE_BUSY;
											m_transfer_thread->Run();
											m_hosttransfergauge->SetRange(m_players_table.requests[m_transfer_client_index].filesize);
											m_transfer_timer = new wxTimer(this, ID_HOSTFILETRANSFERTIMER);
											m_transfer_timer->Start(500, wxTIMER_CONTINUOUS);
										}
									}
								}
							}
						}
						else // In-room clients only.
						{
							if ((l_num_of_items == 2) && (l_str_list[0] == wxT("ready")))
							{
								l_str_list[1].ToLong(&l_temp_num);

								m_players_table.players[l_index].ready = l_temp_num;

								m_hostplayerslistCtrl->SetItemColumnImage(l_index, COL_HOSTREADY, l_temp_num);

								l_str_list.Add(wxString::Format(wxT("%d"), (int)l_index));

								for (l_loop_var = 1; l_loop_var < l_index; l_loop_var++)
									g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
								for (l_loop_var = l_index+1; l_loop_var < m_players_table.num_players; l_loop_var++)
									g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
							}
							else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("leave")))
							{
								l_str_list.Empty();
								l_str_list.Add(wxT("left"));
								l_str_list.Add(m_players_table.players[l_index].nickname);
								AddMessage(wxT("* ") + m_players_table.players[l_index].nickname + wxT(" has left the room."), true);
								if (m_allow_sounds && g_configuration->play_snd_leave)
									g_PlaySound(g_configuration->snd_leave_file);
								RemoveInRoomClient(l_index);
								s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);
								UpdateSettingsList();
								m_hostplayerslistCtrl->DeleteItem(l_index);
								m_ImageList->Remove(l_index+IMG_HOSTNUMBER);
								//              RefreshListCtrlSize();

								for (l_loop_var = l_index; l_loop_var < m_players_table.num_players; l_loop_var++)
									m_hostplayerslistCtrl->SetItemColumnImage(l_loop_var, COL_HOSTLOCATION, l_loop_var+IMG_HOSTNUMBER);

								l_sock->Destroy();
								for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
									g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
								if (m_is_advertised)
								{
									l_str_list.Empty();
									l_str_list.Add(wxT("players"));
									for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
									{
										l_str_list.Add(m_players_table.players[l_loop_var].nickname);
										l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_loop_var].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_MASTER));
										l_str_list.Add(m_players_table.players[l_loop_var].os_description);
									}
									g_SendStringListToSocket(m_client, l_str_list);
								}
							}
							else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("wake")))
							{
								if (g_configuration->host_accept_wakeup)
									Raise();
							}
							else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("requestrename")))
							{
								if (l_str_list[1] == wxEmptyString)
									g_SendCStringToSocket(m_host_clients_table.clients[l_index].sock, "2:errnick:You can't rename to have an empty nickname.");
								else
									DoRename(l_index, l_str_list[1]);
							}
							else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("msg")))
							{
								AddMessage(m_players_table.players[l_index].nickname + wxT(": ") + l_str_list[1], true);
								if (m_allow_sounds && g_configuration->play_snd_receive)
									g_PlaySound(g_configuration->snd_receive_file);
								l_str_list.Insert(m_players_table.players[l_index].nickname, 1);
								for (l_loop_var = 1; l_loop_var < l_index; l_loop_var++)
									g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
								for (l_loop_var = l_index+1; l_loop_var < m_players_table.num_players; l_loop_var++)
									g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
							}
							else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("requestextradownload")))
							{
								if (m_game == GAME_DESCENT)
								{
									if (g_FindDescentMSNfile(&m_extra_usermap, m_usermap))
										l_filename = g_configuration->descent_maps_dir + wxFILE_SEP_PATH + m_extra_usermap;
								}
								else if (m_game == GAME_DESCENT2)
								{
									if (g_FindDescentMN2file(&m_extra_usermap, m_usermap))
										l_filename = g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + m_extra_usermap;
								}

								filesize = (size_t)wxFileName::GetSize(l_filename).ToULong();

								if (extra_usermap.Access(l_filename, wxFile::read) && extra_usermap.Open(l_filename, wxFile::read))
								{
									fileptr = new char[filesize];
									extra_usermap.Read(fileptr, filesize);
									extra_usermap.Close();

									l_str_list.Empty();
									l_str_list.Add(wxT("extradownload"));
									l_str_list.Add(((wxFileName)l_filename).GetFullName());
									l_str_list.Add(wxString::Format(wxT("%lu"), filesize));
									l_str_list.Add(wxString::Format(wxT("%u"), (unsigned)crc32buf(fileptr, filesize)));
									l_str_list.Add(wxString(fileptr, wxConvUTF8, filesize));
									g_SendStringListToSocket(l_sock, l_str_list);

									delete[] fileptr;
								}
								else
									wxLogError(wxT("Can't open file for reading!"));
							}
							else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("requestdownload")))
							{
								// We can do 4 things: Refuse, add to list of requests,
								//                     add to list and waiting (while someone is downloading),
								//                     or accept immediately.

								// Refuse download when no user map is currently selected.
								// We could also refuse in case there's an upload, but it's
								// better to let the host decide in that case.
								// However, refuse if the very same client is in the middle
								// of a transfer.
								if ((!m_launch_usermap) ||
									((m_transfer_state != TRANSFERSTATE_NONE) && (m_transfer_client_index == l_index)))
									g_SendCStringToSocket(l_sock, "1:requestrefuse:");
								// We may auto-accept a download if there's no transfer,
								// we aren't in a game, and auto-acception is enabled.
								else if ((g_configuration->host_autoaccept_downloads)
									&& (!m_in_game || m_game == GAME_DESCENT || m_game == GAME_DESCENT2)
									&& (m_transfer_state == TRANSFERSTATE_NONE))
								{
									// The following lines are needed for some preparation.
									m_players_table.requests[l_index].transferrequest = TRANSFERREQUEST_DOWNLOAD;
									m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTREQUESTS, wxT("Download"));
									//                    RefreshListCtrlSize();
									m_hosttransferdynamicbutton->SetLabel(wxT("Abort transfer"));
									m_hostlaunchbutton->Disable();
									AcceptTransferRequest(l_index);
								}
								else // Add to list of requests.
								{
									m_players_table.requests[l_index].transferrequest = TRANSFERREQUEST_DOWNLOAD;
									// If downloads are automatically accepted, it should be
									// auto-accepted sometime if another download is completed.
									m_players_table.requests[l_index].awaiting_download = g_configuration->host_autoaccept_downloads;

									m_hostplayerslistCtrl->SetItemTextColour(l_index, *wxBLUE);
									// Empty strings are set in case the client has recently requested an upload.
									// The host should already be notified about a cancel
									// of the upload request though.
									m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTREQUESTS, wxT("Download"));
									m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTFILENAMES, wxEmptyString);
									m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTFILESIZES, wxEmptyString);
									//                    RefreshListCtrlSize();
								}
							}
							else if ((l_num_of_items == 4) && (l_str_list[0] == wxT("requestupload")))
							{
								// We can do 2 things: Refuse, or add to list of requests.

								// Refuse upload when the very same client is in the middle of a transfer.
								if ((m_transfer_state != TRANSFERSTATE_NONE) && (m_transfer_client_index == l_index))
									g_SendCStringToSocket(l_sock, "1:requestrefuse:");
								// Valid file size?
								else if (!l_str_list[2].ToLong(&l_temp_num))
									g_SendCStringToSocket(l_sock, "1:requestrefuse:");
								else if (l_temp_num < 0)
									g_SendCStringToSocket(l_sock, "1:requestrefuse:");
								else
								{
									// The following should also work if the client has
									// requested something else before.
									// The host should already be notified about a cancel
									// of the other request, though.

									m_players_table.requests[l_index].transferrequest = TRANSFERREQUEST_UPLOAD;
									m_players_table.requests[l_index].filename = l_str_list[1];
									m_players_table.requests[l_index].filesize = l_temp_num;
									l_str_list[3].ToULong(&l_temp_ulong);
									m_transfer_crc32 = l_temp_ulong;
									// Use red color to warn the host about an existing file with the same name?
									if (((m_game == GAME_BLOOD) && (wxFileExists(g_configuration->blood_maps_dir + wxFILE_SEP_PATH + l_str_list[1])))
										|| ((m_game == GAME_DESCENT) && (wxFileExists(g_configuration->descent_maps_dir + wxFILE_SEP_PATH + l_str_list[1])))
										|| ((m_game == GAME_DESCENT2) && (wxFileExists(g_configuration->descent2_maps_dir + wxFILE_SEP_PATH + l_str_list[1])))
										|| ((m_game == GAME_DN3D) && (wxFileExists(g_configuration->dn3d_maps_dir + wxFILE_SEP_PATH + l_str_list[1])))
										|| ((m_game == GAME_SW) && (wxFileExists(g_configuration->sw_maps_dir + wxFILE_SEP_PATH + l_str_list[1]))))
										m_hostplayerslistCtrl->SetItemTextColour(l_index, *wxRED);
									else
										m_hostplayerslistCtrl->SetItemTextColour(l_index, *wxBLUE);
									m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTREQUESTS, wxT("Upload"));
									m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTFILENAMES, l_str_list[1]);
									m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTFILESIZES, l_str_list[2]);
									//                      RefreshListCtrlSize();
								}
							}
							else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("requestcancel")))
							{
								// Is the client in the middle of a transfer, or pending for one?
								// In case it has already been aborted,
								// that should be quite recent, so the request is simply ignored.
								if ((m_transfer_state != TRANSFERSTATE_NONE) && (m_transfer_client_index == l_index))
								{
									if (m_transfer_state == TRANSFERSTATE_BUSY)
									{
										m_transfer_state = TRANSFERSTATE_ABORTED;
										g_SendCStringToSocket(l_sock, "1:aborttransfer:");
									}
									else if (m_transfer_state == TRANSFERSTATE_PENDING)
									{
										m_transfer_state = TRANSFERSTATE_NONE;
										m_players_table.requests[l_index].transferrequest = TRANSFERREQUEST_NONE;
										m_hostplayerslistCtrl->SetItemTextColour(l_index, m_def_listctrl_color);
										m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTREQUESTS, wxEmptyString);
										m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTFILENAMES, wxEmptyString);
										m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTFILESIZES, wxEmptyString);
										//                      RefreshListCtrlSize();
										// Let's check if there's a pending download request
										// from another client.
										FindNextDownloadRequest();
									}
									// In case of TRANSFERSTATE_ABORTED, the relevant socket
									// deletion should end the file transfer thread and
									// therefore trigger an event used to continue.
								}
								else // A simple cancel of a request instead.
								{
									m_players_table.requests[l_index].transferrequest = TRANSFERREQUEST_NONE;
									m_hostplayerslistCtrl->SetItemTextColour(l_index, m_def_listctrl_color);
									m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTREQUESTS, wxEmptyString);
									m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTFILENAMES, wxEmptyString);
									m_hostplayerslistCtrl->SetItem(l_index, COL_HOSTFILESIZES, wxEmptyString);
									//                  RefreshListCtrlSize();
								}
							}
							else if ((l_num_of_items == 2) && (l_str_list[0] == wxT("filetransfersuccessful")))
							{
								AddMessage(wxT("* ") + m_players_table.players[l_index].nickname + wxT(" has received the \"") + l_str_list[1] + wxT("\" file successfully."), true);
							}
							else if ((l_num_of_items == 1) && (l_str_list[0] == wxT("aborttransfer"))
								&& (m_transfer_state == TRANSFERSTATE_BUSY))
								m_transfer_state = TRANSFERSTATE_ABORTED;
						}
						l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
					}
				}
				while (l_num_of_read_attempts < MAX_COMM_READ_ATTEMPTS);
				break;
			case wxSOCKET_LOST:
				if (l_index < m_players_table.num_players)
				{
					l_str_list.Add(wxT("disconnect"));
					l_str_list.Add(m_players_table.players[l_index].nickname);
					AddMessage(wxT("* ") + m_players_table.players[l_index].nickname + wxT(" has got disconnected from the room."), true);
					if (m_allow_sounds && g_configuration->play_snd_leave)
						g_PlaySound(g_configuration->snd_leave_file);
					RemoveInRoomClient(l_index);
					s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);
					UpdateSettingsList();
					m_hostplayerslistCtrl->DeleteItem(l_index);
					m_ImageList->Remove(l_index+IMG_HOSTNUMBER);
					//        RefreshListCtrlSize();

					for (l_loop_var = l_index; l_loop_var < m_players_table.num_players; l_loop_var++)
						m_hostplayerslistCtrl->SetItemColumnImage(l_loop_var, COL_HOSTLOCATION, l_loop_var+IMG_HOSTNUMBER);

					for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
						g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
					if (m_is_advertised)
					{
						l_str_list.Empty();
						l_str_list.Add(wxT("players"));
						for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
						{
							l_str_list.Add(m_players_table.players[l_loop_var].nickname);
							l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_loop_var].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_MASTER));
							l_str_list.Add(m_players_table.players[l_loop_var].os_description);
						}
						g_SendStringListToSocket(m_client, l_str_list);
					}
				}
				else
				{
					delete m_host_clients_table.clients[l_index].stopwatch;
					//        m_host_clients_table.clients[l_index].timer->Stop();
					//        delete m_host_clients_table.clients[l_index].timer;
					m_host_clients_table.DelByIndex(l_index);
				}
				l_sock->Destroy();
				break;
			default: ;
		}
	}
}

void HostedRoomFrame::OnServerListSocketEvent(wxSocketEvent& event)
{
	size_t l_num_of_items;
	size_t l_loop_var, l_loop_var2;
	wxArrayString l_str_list;
	char l_short_buffer[MAX_COMM_TEXT_LENGTH];
	wxString l_long_buffer;
	wxIPV4address l_addr;
	wxSocketBase* l_sock = event.GetSocket();
	const char * returnedCountry;
	if (l_sock == m_client)
	{
		switch (event.GetSocketEvent())
		{
			case wxSOCKET_CONNECTION:
				m_client->SetTimeout(2);
				SendRoomInfo(true);
				m_serverlist_timer->Stop();
				l_str_list.Add(wxT("players"));
				for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
				{
					l_str_list.Add(m_players_table.players[l_loop_var].nickname);
					l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_loop_var].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_MASTER));
					l_str_list.Add(m_players_table.players[l_loop_var].os_description);
				}
				g_SendStringListToSocket(m_client, l_str_list);
				m_serverlist_timer->Start(25000, wxTIMER_ONE_SHOT);
				break;
			case wxSOCKET_INPUT:
				m_client->Read(l_short_buffer, MAX_COMM_TEXT_LENGTH);
				l_long_buffer << wxString(l_short_buffer, wxConvUTF8, m_client->LastCount());
				l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
				if (l_num_of_items >= 2)
				{
					if (l_str_list[0] == wxT("error"))
					{
						AddMessage(wxT("* Error from master server: ") + l_str_list[1], true);
						m_is_advertised = false;
						g_SendCStringToSocket(m_client, "1:disconnect:");
						m_client->Destroy();
						m_curr_server = (m_curr_server + 1) % (m_num_of_lists + m_num_of_extra_lists);
						ConnectToServerList();
					}
					else if ((l_str_list[0] == wxT("advertised")) && (!m_is_advertised))
					{ // Send heartbeats to the master server.
						m_serverlist_timer->Start(60000, wxTIMER_CONTINUOUS);
						m_is_advertised = true;
						AddMessage(wxT("* Server is now advertised."), true);
						if (l_num_of_items == 3)
							AddMessage(wxT("\n* Message from the master server:\n") + l_str_list[2], true);
						// Decrypt IP
						l_str_list[1] = g_DecryptFromCommText(l_str_list[1], COMM_KEY_IPLIST_FROM_MASTER_TO_HOST); 
						if (l_str_list[1] != (m_host_clients_table.clients[0].peerdetectedip))
						{
							m_host_clients_table.clients[0].peerdetectedip = l_str_list[1];

							if (g_geoip_db != NULL)
							{
								returnedCountry = GeoIP_country_code_by_addr(g_geoip_db,m_host_clients_table.clients[0].peerdetectedip.mb_str(wxConvUTF8));
								if (returnedCountry != NULL)
								{
									if (m_ImageList->GetImageCount() == IMG_HOSTNUMBER)
										m_ImageList->Add(getFlag(returnedCountry));
									else
										m_ImageList->Replace(IMG_HOSTNUMBER, getFlag(returnedCountry));
								}
								else
									m_ImageList->Add(wxIcon(unknown));

								m_hostplayerslistCtrl->SetItem(0, COL_HOSTLOCATION, wxString(GeoIP_country_name_by_addr(g_geoip_db,m_host_clients_table.clients[0].peerdetectedip.mb_str(wxConvUTF8)), wxConvUTF8), IMG_HOSTNUMBER);
							}

							m_hostplayerslistCtrl->SetItem(0, COL_HOSTDETECTED_IPS, l_str_list[1]);
							l_sock->GetLocal(l_addr);
							m_host_clients_table.clients[0].selfdetectedip = l_addr.IPAddress();
							m_players_table.players[0].ingameip = m_host_clients_table.clients[0].selfdetectedip;
							m_hostplayerslistCtrl->SetItem(0, COL_HOSTINGAME_IPS, m_players_table.players[0].ingameip + wxString::Format(wxT(":%d"), (int)g_configuration->game_port_number));
#if (defined __WXMAC__) || (defined __WXCOCOA__)
							m_hostplayerslistCtrl->RefreshItem(0);
#endif
							//              RefreshListCtrlSize();
							l_str_list.Empty();
							l_str_list.Add(wxT("hostaddrupdate"));
							l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[0].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));

							for (l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
							{
								if (g_configuration->enable_localip_optimization &&
									(m_host_clients_table.clients[0].peerdetectedip == m_host_clients_table.clients[l_loop_var].peerdetectedip))
									l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[0].selfdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));
								else
									l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[0].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT));
								g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var].sock, l_str_list);
								l_str_list.RemoveAt(3);
							}

							l_str_list.Empty();
							l_str_list.Add(wxT("players"));
							for (l_loop_var = 0; l_loop_var < m_players_table.num_players; l_loop_var++)
							{
								l_str_list.Add(m_players_table.players[l_loop_var].nickname);
								l_str_list.Add(g_EncryptForCommText(m_host_clients_table.clients[l_loop_var].peerdetectedip, COMM_KEY_IPLIST_FROM_HOST_TO_MASTER));
								l_str_list.Add(m_players_table.players[l_loop_var].os_description);
							}
							g_SendStringListToSocket(m_client, l_str_list);
						}
					}
					else if (l_str_list[0] == wxT("bannedaddrs"))
					{
						for (l_loop_var = 1; l_loop_var < l_str_list.GetCount(); l_loop_var++)
						{
							size_t l_client_index = 1;
							wxArrayString l_msg_strlist;
							// First go over in-room users
							while (l_client_index < m_players_table.num_players)
								if (m_host_clients_table.clients[l_client_index].peerdetectedip == g_DecryptFromCommText(l_str_list[l_loop_var], COMM_KEY_IPLIST_FROM_MASTER_TO_HOST))
								{
									l_msg_strlist.Add(wxT("globanned"));
									l_msg_strlist.Add(m_players_table.players[l_client_index].nickname);
									for (l_loop_var2 = 1; l_loop_var2 < m_players_table.num_players; l_loop_var2++)
										g_SendStringListToSocket(m_host_clients_table.clients[l_loop_var2].sock, l_msg_strlist);
									AddMessage(wxT("* ") + m_players_table.players[l_client_index].nickname + wxT(" has been automatically kicked, due to a global ban."), true);
									m_host_clients_table.clients[l_client_index].sock->Destroy();
									RemoveInRoomClient(l_client_index);
									m_hostplayerslistCtrl->DeleteItem(l_client_index);
									m_ImageList->Remove(l_client_index+IMG_HOSTNUMBER);
									//RefreshListCtrlSize();

									for (l_loop_var2 = l_client_index; l_loop_var2 < m_players_table.num_players; l_loop_var2++)
										m_hostplayerslistCtrl->SetItemColumnImage(l_loop_var2, COL_HOSTLOCATION, l_loop_var2+IMG_HOSTNUMBER);
									l_msg_strlist.Empty();
								}
								else
									l_client_index++;
							s_players = wxString::Format(wxT("%lu/%lu"), (unsigned long)m_players_table.num_players, (unsigned long)m_max_num_players);
							UpdateSettingsList();
							// Now continue with the rest of the clients
							while (l_client_index < m_host_clients_table.num_clients)
								if (m_host_clients_table.clients[l_client_index].peerdetectedip == g_DecryptFromCommText(l_str_list[l_loop_var], COMM_KEY_IPLIST_FROM_MASTER_TO_HOST))
								{
									delete m_host_clients_table.clients[l_client_index].stopwatch;
									g_SendCStringToSocket(m_host_clients_table.clients[l_client_index].sock, "2:error:You've been automatically kicked, due to a global YANG ban.:");
									m_host_clients_table.clients[l_client_index].sock->Destroy();
									m_host_clients_table.DelByIndex(l_client_index);
								}
								else
									l_client_index++;
						}
					}
					else if (l_str_list[0] == wxT("unbannedaddrs"))
					{
						// Apply delayed joins after ban checks?
						for (l_loop_var = 1; l_loop_var < l_str_list.GetCount(); l_loop_var++)
						{
							// Decrypt IP
							l_str_list[l_loop_var] = g_DecryptFromCommText(l_str_list[l_loop_var], COMM_KEY_IPLIST_FROM_MASTER_TO_HOST);

							for (l_loop_var2 = 0; l_loop_var2 < m_join_requests_sock_arr.GetCount(); l_loop_var2++)
								if (m_join_requests_address_arr[l_loop_var2] == l_str_list[l_loop_var])
								{
									AcceptClientJoin(m_join_requests_sock_arr[l_loop_var2], m_join_requests_arr[l_loop_var2]);
									m_join_requests_arr.RemoveAt(l_loop_var2);
									m_join_requests_sock_arr.RemoveAt(l_loop_var2);
									m_join_requests_address_arr.RemoveAt(l_loop_var2);
									l_loop_var2--; // This is important, after the "for" loop!! (works even if unsigned...)
									break;
								}
							if (l_loop_var2 == m_join_requests_sock_arr.GetCount())
								m_unbanned_ips_arr.Add(l_str_list[l_loop_var]);
						}
					}
					//        l_num_of_items = g_ConvertStringBufToStringList(l_long_buffer, l_str_list);
				}
				break;
			case wxSOCKET_LOST:
				AddMessage(wxT("* Disconnected from master server."), true);
				m_is_advertised = false;
				m_client->Destroy();
				m_curr_server = (m_curr_server + 1) % (m_num_of_lists + m_num_of_extra_lists);
				ConnectToServerList();
				break;
			default: ;
		}
	}
}

void HostedRoomFrame::ConnectToServerList()
{
	wxIPV4address l_addr;
	m_serverlist_timer->Stop();
	AddMessage(wxString::Format(wxT("* Trying master server %d, please wait..."), (int)(m_curr_server+1)), true);
	if (m_curr_server < m_num_of_extra_lists)
	{
		l_addr.Hostname(g_configuration->extraservers_addrs[m_curr_server]);
		l_addr.Service(g_configuration->extraservers_addrs[m_curr_server]);
	}
	else
	{
		l_addr.Hostname((*g_serverlists_addrs)[m_curr_server - m_num_of_extra_lists]);
		l_addr.Service((*g_serverlists_portnums)[m_curr_server - m_num_of_extra_lists]);
	}
	m_client = new wxSocketClient;
	m_client->SetTimeout(3);
	m_client->SetEventHandler(*this, ID_MASTERSOCKET);
	m_client->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
	m_client->SetNotify(wxSOCKET_CONNECTION_FLAG |
		wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
	m_client->Notify(true);

	m_client->Connect(l_addr, false);
	m_serverlist_timer->Start(15000, wxTIMER_ONE_SHOT);
}

void HostedRoomFrame::OnPingRefreshTimer(wxTimerEvent& WXUNUSED(event))
{
	wxArrayString l_str_ping;

	if (m_pingrefresh_timer == NULL)
		return;

	m_pingrefresh_timer->Start(10000, wxTIMER_ONE_SHOT);

	if ((!m_pinging) && (m_players_table.num_players > 1))
	{
		m_pinging = 1;
		m_ping_index = 1;
		m_ping_id = rand();
		l_str_ping.Empty();
		l_str_ping.Add(wxT("pingquery"));
		l_str_ping.Add(wxString::Format(wxT("%d"), (int)m_ping_id));
		g_SendStringListToSocket(m_host_clients_table.clients[m_ping_index].sock, l_str_ping);
		m_ping_times = m_max_ping = 0;
		m_min_ping = 999;
		m_ping_timer->Start(999, wxTIMER_ONE_SHOT);
		ping.Start( 0 );
	}
}

void HostedRoomFrame::Advertise()
{
	if (!(g_serverlists_portnums->GetCount() | g_configuration->extraservers_portnums.GetCount()))
	{
		//  m_hostadvertisebutton->SetLabel(wxT(" Advertise room "));
		m_hostadvertisebutton->Disable();
		AddMessage(wxString::Format(wxT("ERROR: The list of master servers seems to be empty.")), true);
		if (g_configuration->play_snd_error)
			g_PlaySound(g_configuration->snd_error_file);
	}
	else
	{
		m_hostadvertisebutton->SetLabel(wxT("Stop advertising"));
		m_curr_server = rand() % (m_num_of_lists + m_num_of_extra_lists);
		ConnectToServerList();
	}
}

void HostedRoomFrame::OnAdvertiseButtonClick(wxCommandEvent& WXUNUSED(event))
{
#ifndef __WXMSW__
	SetInGame(false);
	m_allow_sounds = true;
#endif

	if (m_hostadvertisebutton->GetLabel() == wxT(" Advertise room "))
		Advertise();
	else
	{
		m_is_advertised = false;
		m_hostadvertisebutton->SetLabel(wxT(" Advertise room "));
		if (m_client)
		{
			g_SendCStringToSocket(m_client, "1:disconnect:");
			m_client->Destroy();
			m_client = NULL;
		}
		m_serverlist_timer->Stop();
		AddMessage(wxString::Format(wxT("Server is now not advertised.")), true);
	}
}

void HostedRoomFrame::OnTimeStampsCheck(wxCommandEvent& WXUNUSED(event))
{
	g_configuration->show_timestamps = m_hostedroomtimestampscheckBox->GetValue();
	g_configuration->Save();
}
/*
void HostedRoomFrame::OnNatFreeCheck(wxCommandEvent& WXUNUSED(event))
{
#ifndef __WXMSW__
SetInGame(false);
if (m_is_advertised)
SendRoomInfo(false);
m_allow_sounds = true;
#endif
g_configuration->gamelaunch_enable_natfree = m_hostedroomnatfreecheckBox->GetValue();
g_configuration->Save();
}
*/
void HostedRoomFrame::OnServerListTimer(wxTimerEvent& WXUNUSED(event))
{
	if (m_client != NULL)
	{
		if (m_is_advertised)
			g_SendCStringToSocket(m_client, "1:heartbeat:");
		else
		{
			g_SendCStringToSocket(m_client, "1:disconnect:");
			m_client->Destroy();
			m_curr_server = (m_curr_server + 1) % (m_num_of_lists + m_num_of_extra_lists);
			ConnectToServerList();
		}
	}
}

void HostedRoomFrame::ClearPingTimers()
{
	m_ping_timer->Stop();
	delete m_ping_timer;
	m_ping_timer = NULL;
	m_firstping_timer->Stop();
	delete m_firstping_timer;
	m_firstping_timer = NULL;
	m_checkping_timer->Stop();
	delete m_checkping_timer;
	m_checkping_timer = NULL;
	m_pingrefresh_timer->Stop();
	delete m_pingrefresh_timer;
	m_pingrefresh_timer = NULL;
}

void HostedRoomFrame::SaveUIControlSizes()
{
	g_configuration->host_col_ready_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTREADY);
	g_configuration->host_col_loc_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTLOCATION);
	g_configuration->host_col_nick_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTNICKNAMES);
	g_configuration->host_col_ping_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTPING);
	g_configuration->host_col_flux_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTFLUX);
	g_configuration->host_col_os_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTOS);
	g_configuration->host_col_detectedip_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTDETECTED_IPS);
	g_configuration->host_col_ingameip_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTINGAME_IPS);
	g_configuration->host_col_requests_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTREQUESTS);
	g_configuration->host_col_filenames_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTFILENAMES);
	g_configuration->host_col_filesizes_len = m_hostplayerslistCtrl->GetColumnWidth(COL_HOSTFILESIZES);
	g_configuration->host_col_settings_len = m_hostsettingslistCtrl->GetColumnWidth(0);
	g_configuration->host_col_value_len = m_hostsettingslistCtrl->GetColumnWidth(1);
	g_configuration->host_is_maximized = IsMaximized();
	Hide();
	/*Maximize(false);
	Layout();*/
	if (!g_configuration->host_is_maximized)
		GetSize(&g_configuration->host_win_width, &g_configuration->host_win_height);
	g_configuration->Save();
}

void HostedRoomFrame::OnCloseWindow(wxCloseEvent& WXUNUSED(event))
{
	m_allow_sounds = true;
	if (m_server && (m_players_table.num_players > 1))
		if (wxNO == wxMessageBox(wxT("There are clients in your room. Are you sure you want to close it?"),
		    wxT("Confirmation"), wxYES_NO|wxICON_EXCLAMATION))
			return;
#ifdef ENABLE_UPNP
	if (g_configuration->enable_upnp)
	{
		g_UPNP_UnForward(g_configuration->game_port_number, false);
		g_UPNP_UnForward(g_configuration->server_port_number, true);
	}
#endif
	m_connection_timeout_timer->Stop();
	delete m_connection_timeout_timer;

#ifndef __WXMSW__
	SetInGame(false);
	if (m_is_advertised)
		SendRoomInfo(false);
#endif
	if (m_server)
	{
		ClearPingTimers();
		SaveUIControlSizes();
		for (size_t l_loop_var = 1; l_loop_var < m_players_table.num_players; l_loop_var++)
			g_SendCStringToSocket(m_host_clients_table.clients[l_loop_var].sock, "1:shutdown:"); 
		for (size_t l_loop_var = 1; l_loop_var < m_host_clients_table.num_clients; l_loop_var++)
			m_host_clients_table.clients[l_loop_var].sock->Destroy();
		delete m_server;
		m_is_advertised = false;
		if (m_client)
		{
			g_SendCStringToSocket(m_client, "1:disconnect:");
			m_client->Destroy();
			m_client = NULL;
		}
		m_serverlist_timer->Stop();
		m_server = NULL; // In case there's a pending event.
		// Is there an ongoing transfer?
		// - If pending, there's nothing to do.
		// - If busy, we should abort and wait for the transfer thread end.
		// - If already aborted, we should just wait, so there's nothing to do.
		if (m_transfer_state == TRANSFERSTATE_BUSY)
			m_transfer_state = TRANSFERSTATE_ABORTED;
		// Don't destroy the window if we need to wait for file transfer abortion.
		/*    if (m_transfer_state == TRANSFERSTATE_ABORTED)
		Hide();
		else
		Destroy();*/
		if (m_transfer_state != TRANSFERSTATE_ABORTED)
			Destroy();
	}
	else
	{
		ClearPingTimers();
		SaveUIControlSizes();
		Destroy();
	}
}

void HostedRoomFrame::OnCloseRoom(wxCommandEvent& WXUNUSED(event))
{
	Close();
}

void HostedRoomFrame::OnShowRoomsList(wxCommandEvent& WXUNUSED(event))
{
	m_mainframe->Show();
	m_mainframe->Raise();
	m_mainframe->RefreshList();
}

void HostedRoomFrame::OnOpenChatOutputStyle(wxCommandEvent& WXUNUSED(event))
{
	if (m_lookandfeel_dialog->IsShown())
		m_lookandfeel_dialog->Raise();
	else
		m_lookandfeel_dialog->ShowDialog();
}

void HostedRoomFrame::OnAutoAcceptCheck(wxCommandEvent& WXUNUSED(event))
{
#ifndef __WXMSW__
	SetInGame(false);
	if (m_is_advertised)
		SendRoomInfo(false);
	m_allow_sounds = true;
#endif

	g_configuration->host_autoaccept_downloads = m_hosttransfercheckBox->GetValue();
	g_configuration->Save();

	if (((m_game == GAME_DESCENT) || (m_game == GAME_DESCENT2)) && m_launch_usermap && !m_hosttransfercheckBox->GetValue())
		AddMessage(wxT("WARNING : It is recommended to enable the \"Auto-accept downloads\" option, when playing Descent or Descent 2, so that when you are in-game, people joining the gameroom can automatically download the map."), false);
}
