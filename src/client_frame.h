/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_CLIENTFRAME_H_
#define _YANG_CLIENTFRAME_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#include <wx/imaglist.h>
#include <wx/gauge.h>
#endif

#include "config.h"
#include "mp_common.h"
#include "lookandfeel_dialog.h"
#include "mapselection_dialog.h"
#include "pingstopwatch.h"
#include "crc.h"
#include "theme.h"
#include "main_frame.h"

#define ID_CLIENTPLAYERCOLORCHOICE 1000
#define ID_CLIENTTRANSFERUPLOAD 1001
#define ID_CLIENTGENERALTRANSFERPURPOSE 1002
#define ID_CLIENTOUTPUTTEXT 1003
#define ID_CLIENTINPUTTEXT 1004
#define ID_CLIENTCHATOUTPUTSTYLE 1005
//#define ID_CLIENTADDNEWLINE 1006
//#define ID_CLIENTNATFREE 1007
#define ID_CLIENTTIMESTAMPS 1008
#define ID_CLIENTSHOWROOMSLIST 1009
//#define ID_CLIENTSENDPM 1010
#define ID_CLIENTREADY 1011
#define ID_CLIENTPINGTIMER 1012
#define ID_CLIENTPINGREFRESHTIMER 1013
#define ID_CLIENTLOOKANDFEELCLOSE 1014

enum { ID_CLIENTSOCKET = wxID_HIGHEST+1, ID_CLIENTFILETRANSFERSOCKET,
ID_CLIENTTIMER, ID_CLIENTFILETRANSFERTIMER };

#define COL_CLIENTREADY 0
#define COL_CLIENTLOCATION 1
#define COL_CLIENTNICKNAMES 2
#define COL_CLIENTPING 3
#define COL_CLIENTFLUX 4
#define COL_CLIENTOS 5
#define COL_CLIENTDETECTED_IPS 6
#define COL_CLIENTINGAME_IPS 7

#define IMG_CLIENTCANCEL 0
#define IMG_CLIENTACCEPT 1

#define IMG_CLIENTNUMBER 2

class ClientRoomFrame : public YANGFrame 
{
public:
	ClientRoomFrame();
	//~ClientRoomFrame();
	//void RefreshListCtrlSize();
	//void OnResizeWindow(wxSizeEvent& event);
	//void OnMaximize(wxMaximizeEvent& event);
	void FocusFrame();

	/*void UpdateGameSettings(GameType game,
	SourcePortType srcport,
	const wxString& roomname,
	size_t max_num_players,
	size_t skill,
	DN3DMPGameT_Type dn3dgametype,
	DN3DSpawnType dn3dspawn,
	const wxString& extra_args,
	bool launch_usermap,
	size_t epimap_num,
	const wxString& usermap,
	const wxString& modname,
	bool showmodurl,
	const wxString& modurl,
	bool usemasterslave);
	void UpdateModFiles(const wxArrayString& modfiles);*/
	void UpdateGameSettings(const wxArrayString& l_info_input);
	void UpdateSettingsList();
	void UpdateModFilesInfo(const wxArrayString& l_info_input);

	bool ConnectToServer(wxString ip_address, long port_num);
	//void ConnectToServer(wxString ip_address, long port_num);
	void OnTextURLEvent(wxTextUrlEvent& event);
	//void OnInsertNewLine(wxCommandEvent& event);
	void OnEnterText(wxCommandEvent& event);
	bool SelectCustomProfile(bool leave);
	void SelectModProfile();
	//void OnUpdateTextInput(wxCommandEvent& event);
	void OnPlayerColorChoice(wxCommandEvent& event);
	void OnUploadButtonClick(wxCommandEvent& event);
	void OnTransferButtonClick(wxCommandEvent& event);
	void OnTimeStampsCheck(wxCommandEvent& event);
	//void OnNatFreeCheck(wxCommandEvent& event);
	void OnMapSelectionClose(wxCommandEvent& event);
	void OnTransferThreadEnd(wxCommandEvent& event);
	void OnLookAndFeelClose(wxCommandEvent& event);
	void OnFileTransferSocketEvent(wxSocketEvent& event);
	void OnSocketEvent(wxSocketEvent& event);
	void JoinGame(SourcePortType srcport);
	void LaunchGame();
	void OnLeaveRoom(wxCommandEvent& event);
	void OnShowRoomsList(wxCommandEvent& event);
	void OnOpenChatOutputStyle(wxCommandEvent& event);
	void OnClientPressReady(wxCommandEvent& event);
	void OnCloseWindow(wxCloseEvent& event);
	void OnTimer(wxTimerEvent& event);
	void OnPingTimer(wxTimerEvent& event);
	void OnPingRefreshTimer(wxTimerEvent& event);
	void OnFileTransferTimer(wxTimerEvent& event);

private:
	YANGPanel* m_clientroompanel;
	YANGListCtrl* m_clientplayerslistCtrl;
	YANGListCtrl* m_clientsettingslistCtrl;
	wxGauge* m_clienttransfergauge;
	wxStaticText* m_clienttransferstaticText;

	YANGButton* m_clienttransferuploadbutton;
	YANGButton* m_clienttransferdynamicbutton;
	YANGTextCtrl* m_clientoutputtextCtrl;
	YANGTextCtrl* m_clientinputtextCtrl;
	YANGButton* m_clientchatoutputstylebutton;
	//YANGButton* m_clientnewlinebutton;
	//wxCheckBox* m_clientroomnatfreecheckBox;
	YANGCheckBox* m_clientroomtimestampscheckBox;
	wxStaticText* m_clientplayercolorstaticText;
	YANGChoice* m_clientplayercolorchoice;
	YANGButton* m_clientleaveroombutton;
	YANGButton* m_clientshowroomslistbutton;

	//YANGButton* m_clientsendpmbutton;
	YANGToggleButton* m_clientreadytoggleBtn;
	wxImageList* m_ImageList;

	void AskToRename(const wxString& newname);
	void DoEnterText(const wxString& text);
	void AddMessage(const wxString& msg, bool allow_timestamp);
	void SetChatInputStyleOnUpdate();
	void SetChatStyle();

	MainFrame* m_mainframe;

	wxTimer* m_timer;

	wxSocketClient* m_client;
	bool m_in_room;

	TransferStateType m_transfer_state;
	TransferRequestType m_transfer_request;
	wxSocketClient* m_transfer_client;
	wxString m_transfer_filename;
	long m_transfer_filesize, m_transfer_port_num;
	DWORD m_transfer_crc32;
	wxThread* m_transfer_thread;
	wxFile* m_transfer_fp;
	//bool m_transfer_awaiting_download;
	wxTimer* m_transfer_timer;
	wxTimer* m_ping_timer;
	wxTimer* m_pingrefresh_timer;

	long m_ping_times, m_min_ping, m_max_ping, m_ping_id;
	PingStopWatch ping;

	MapSelectionDialog* m_mapselection_dialog;
	LookAndFeelDialog* m_lookandfeel_dialog;

	RoomPlayersTable m_players_table;

	wxTextAttr m_chatlayoutstyle;

	bool m_allow_sounds;

	GameType m_game;
	SourcePortType m_srcport;
	bool m_use_crypticpassage;
	wxString m_roomname, m_last_roomname;
	wxString m_nickname;
	size_t m_max_num_players;
	size_t m_skill;
	BloodMPGameT_Type m_bloodgametype;
	DescentMPGameT_Type m_descentgametype;
	Descent2MPGameT_Type m_descent2gametype;
	DN3DMPGameT_Type m_dn3dgametype;
	SWMPGameT_Type m_swgametype;
	DN3DSpawnType m_dn3dspawn;
	BloodMPMonsterType m_blood_monster;
	BloodMPWeaponType m_blood_weapon;
	BloodMPItemType m_blood_item;
	BloodMPRespawnType m_blood_respawn;
	wxString m_args_for_host, m_args_for_all;
	long m_maxping;
	size_t m_rxdelay;
	bool m_recorddemo;
	wxString m_demofilename;
	bool m_launch_usermap;
	size_t m_epimap_num;
	wxString m_usermap, m_extra_usermap;
	wxArrayString m_modfiles;
	wxString m_modname, m_last_modname;
	wxString m_modconfile;
	wxString m_moddeffile;
	bool m_showmodurl;
	wxString m_modurl;
	size_t m_connectiontype;
	bool m_temp_in_game;

	bool m_use_password;
	bool m_password_dialog;

	wxString s_gamename, s_srcport, s_players, s_map, s_mod, s_gametype, s_skill, s_spawn, s_monster, s_weapon, s_item, s_respawn, s_maxping, s_rxdelay, s_packetmode, s_connection;

	wxColour m_def_listctrl_color;

	DECLARE_EVENT_TABLE()
};

#endif

