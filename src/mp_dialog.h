/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_MPDIALOG_H_
#define _YANG_MPDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "config.h"
#include "theme.h"

#define ID_MPBLOODSELECTSRCPORT 1000
#define ID_MPDESCENTSELECTSRCPORT 1001
#define ID_MPDESCENT2SELECTSRCPORT 1002
#define ID_MPDN3DSELECTSRCPORT 1003
#define ID_MPSWSELECTSRCPORT 1004
#define ID_MPBLOODRECORDDEMO 1005
#define ID_MPSWRECORDDEMO 1006
#define ID_MPPASSPROTECTCHECK 1007
//#define ID_MPFORCEIP 1008
#define ID_MPBLOODUSECRYPTICPASSAGE 1009
#define ID_MPBLOODUSEEPIMAP 1010
#define ID_MPDESCENTUSEEPIMAP 1011
#define ID_MPDESCENT2USEEPIMAP 1012
#define ID_MPDN3DUSEEPIMAP 1013
#define ID_MPSWUSEEPIMAP 1014
#define ID_MPBLOODUSEUSERMAP 1015
#define ID_MPDESCENTUSEUSERMAP 1016
#define ID_MPDESCENT2USEUSERMAP 1017
#define ID_MPDN3DUSEUSERMAP 1018
#define ID_MPSWUSEUSERMAP 1019
#define ID_MPSELECTCUSTOMPROFILE 1020
#define ID_MPUSEPROFILENAME 1021
#define ID_MPUSENULLMODEM 1022

class MPDialog : public YANGDialog
{
public:
	MPDialog(bool isPassProtected = false);
	//~MPDialog();

	void OnSrcPortSelect(wxCommandEvent& event);
	void OnCheckBoxClick(wxCommandEvent& event);
	void OnRadioBtnClick(wxCommandEvent& event);
	void OnSelectCustomProfile(wxCommandEvent& event);

	void OnCancel(wxCommandEvent& event);
	void OnOk(wxCommandEvent& event);
	void ApplySettings();

private:
	void UpdateSummaryList();
	void AppendLevelsList(GameType game, int level_selection);

	YANGNotebook* m_mpnotebook;

	YANGPanel* m_mpbloodpanel;
	wxStaticText* m_mpbloodsourceportstaticText;
	YANGChoice* m_mpbloodsourceportchoice;
	YANGCheckBox* m_mpbloodusecrypticpassagecheckBox;
	YANGRadioButton* m_mpbloodepimapradioBtn;
	YANGChoice* m_mpbloodepimapchoice;
	YANGRadioButton* m_mpbloodusermapradioBtn;
	YANGChoice* m_mpbloodusermapchoice;
	wxStaticText* m_mpbloodgametypestaticText;
	YANGChoice* m_mpbloodgametypechoice;
	wxStaticText* m_mpbloodskillstaticText;
	YANGChoice* m_mpbloodskillchoice;
	wxStaticText* m_mpbloodmonsterstaticText;
	YANGChoice* m_mpbloodmonsterchoice;
	wxStaticText* m_mpbloodweaponstaticText;
	YANGChoice* m_mpbloodweaponchoice;
	wxStaticText* m_mpblooditemstaticText;
	YANGChoice* m_mpblooditemchoice;
	wxStaticText* m_mpbloodrespawnstaticText;
	YANGChoice* m_mpbloodrespawnchoice;
	wxStaticText* m_mpbloodroomnamestaticText;
	YANGTextCtrl* m_mpbloodroomnametextCtrl;
	wxStaticText* m_mpbloodmaxnumplayersstaticText;
	YANGChoice* m_mpbloodmaxnumplayerschoice;
	wxStaticText* m_mpbloodmaxpingstaticText;
	YANGTextCtrl* m_mpbloodmaxpingtextCtrl;
	YANGCheckBox* m_mpbloodrecorddemocheckBox;
	YANGTextCtrl* m_mpbloodrecorddemotextCtrl;
	wxStaticText* m_mpbloodextrahostargsstaticText;
	YANGTextCtrl* m_mpbloodextrahostargstextCtrl;
#if EXTRA_ARGS_FOR_ALL
	wxStaticText* m_mpbloodextraallargsstaticText;
	YANGTextCtrl* m_mpbloodextraallargstextCtrl;
#endif
	wxStaticText* m_mpbloodpacketmodestaticText;
	YANGChoice* m_mpbloodpacketmodechoice;

	YANGPanel* m_mpdescentpanel;
	wxStaticText* m_mpdescentsourceportstaticText;
	YANGChoice* m_mpdescentsourceportchoice;
	YANGRadioButton* m_mpdescentepimapradioBtn;
	YANGChoice* m_mpdescentepimapchoice;
	YANGRadioButton* m_mpdescentusermapradioBtn;
	YANGChoice* m_mpdescentusermapchoice;
	wxStaticText* m_mpdescentgametypestaticText;
	YANGChoice* m_mpdescentgametypechoice;
	wxStaticText* m_mpdescentroomnamestaticText;
	YANGTextCtrl* m_mpdescentroomnametextCtrl;
	wxStaticText* m_mpdescentmaxnumplayersstaticText;
	YANGChoice* m_mpdescentmaxnumplayerschoice;
	wxStaticText* m_mpdescentmaxpingstaticText;
	YANGTextCtrl* m_mpdescentmaxpingtextCtrl;
	wxStaticText* m_mpdescentextrahostargsstaticText;
	YANGTextCtrl* m_mpdescentextrahostargstextCtrl;
#if EXTRA_ARGS_FOR_ALL
	wxStaticText* m_mpdescentextraallargsstaticText;
	YANGTextCtrl* m_mpdescentextraallargstextCtrl;
#endif

	YANGPanel* m_mpdescent2panel;
	wxStaticText* m_mpdescent2sourceportstaticText;
	YANGChoice* m_mpdescent2sourceportchoice;
	YANGRadioButton* m_mpdescent2epimapradioBtn;
	YANGChoice* m_mpdescent2epimapchoice;
	YANGRadioButton* m_mpdescent2usermapradioBtn;
	YANGChoice* m_mpdescent2usermapchoice;
	wxStaticText* m_mpdescent2gametypestaticText;
	YANGChoice* m_mpdescent2gametypechoice;
	wxStaticText* m_mpdescent2roomnamestaticText;
	YANGTextCtrl* m_mpdescent2roomnametextCtrl;
	wxStaticText* m_mpdescent2maxnumplayersstaticText;
	YANGChoice* m_mpdescent2maxnumplayerschoice;
	wxStaticText* m_mpdescent2maxpingstaticText;
	YANGTextCtrl* m_mpdescent2maxpingtextCtrl;
	wxStaticText* m_mpdescent2extrahostargsstaticText;
	YANGTextCtrl* m_mpdescent2extrahostargstextCtrl;
#if EXTRA_ARGS_FOR_ALL
	wxStaticText* m_mpdescent2extraallargsstaticText;
	YANGTextCtrl* m_mpdescent2extraallargstextCtrl;
#endif

	YANGPanel* m_mpdn3dpanel;
	wxStaticText* m_mpdn3dsourceportstaticText;
	YANGChoice* m_mpdn3dsourceportchoice;
	wxStaticText* m_mpdn3dtcmodprofilestaticText;
	YANGChoice* m_mpdn3dtcmodprofilechoice;
	YANGRadioButton* m_mpdn3depimapradioBtn;
	YANGChoice* m_mpdn3depimapchoice;
	YANGRadioButton* m_mpdn3dusermapradioBtn;
	YANGChoice* m_mpdn3dusermapchoice;
	wxStaticText* m_mpdn3dgametypestaticText;
	YANGChoice* m_mpdn3dgametypechoice;
	wxStaticText* m_mpdn3dskillstaticText;
	YANGChoice* m_mpdn3dskillchoice;
	wxStaticText* m_mpdn3dspawnstaticText;
	YANGChoice* m_mpdn3dspawnchoice;
	wxStaticText* m_mpdn3droomnamestaticText;
	YANGTextCtrl* m_mpdn3droomnametextCtrl;
	wxStaticText* m_mpdn3dmaxnumplayersstaticText;
	YANGChoice* m_mpdn3dmaxnumplayerschoice;
	wxStaticText* m_mpdn3dmaxpingstaticText;
	YANGTextCtrl* m_mpdn3dmaxpingtextCtrl;
	YANGCheckBox* m_mpdn3drecorddemocheckBox;
	wxStaticText* m_mpdn3dextrahostargsstaticText;
	YANGTextCtrl* m_mpdn3dextrahostargstextCtrl;
#if EXTRA_ARGS_FOR_ALL
	wxStaticText* m_mpdn3dextraallargsstaticText;
	YANGTextCtrl* m_mpdn3dextraallargstextCtrl;
#endif
	wxStaticText* m_mpdn3dconnectiontypestaticText;
	YANGChoice* m_mpdn3dconnectiontypechoice;

	YANGPanel* m_mpswpanel;
	wxStaticText* m_mpswsourceportstaticText;
	YANGChoice* m_mpswsourceportchoice;
	wxStaticText* m_mpswtcmodprofilestaticText;
	YANGChoice* m_mpswtcmodprofilechoice;
	YANGRadioButton* m_mpswepimapradioBtn;
	YANGChoice* m_mpswepimapchoice;
	YANGRadioButton* m_mpswusermapradioBtn;
	YANGChoice* m_mpswusermapchoice;
	wxStaticText* m_mpswgametypestaticText;
	YANGChoice* m_mpswgametypechoice;
	wxStaticText* m_mpswskillstaticText;
	YANGChoice* m_mpswskillchoice;
	wxStaticText* m_mpswroomnamestaticText;
	YANGTextCtrl* m_mpswroomnametextCtrl;
	wxStaticText* m_mpswmaxnumplayersstaticText;
	YANGChoice* m_mpswmaxnumplayerschoice;
	wxStaticText* m_mpswmaxpingstaticText;
	YANGTextCtrl* m_mpswmaxpingtextCtrl;
	YANGCheckBox* m_mpswrecorddemocheckBox;
	YANGTextCtrl* m_mpswrecorddemotextCtrl;
	wxStaticText* m_mpswextrahostargsstaticText;
	YANGTextCtrl* m_mpswextrahostargstextCtrl;
#if EXTRA_ARGS_FOR_ALL
	wxStaticText* m_mpswextraallargsstaticText;
	YANGTextCtrl* m_mpswextraallargstextCtrl;
#endif

	YANGPanel* m_mpcustompanel;
	wxStaticText* m_mpcustomprofilestaticText;
	YANGChoice* m_mpcustomprofilechoice;
	wxStaticText* m_mpcustomsummarystaticText;
	YANGTextCtrl* m_mpcustomsummarytextCtrl;
	YANGCheckBox* m_mpcustomuseprofilenamecheckBox;
	wxStaticText* m_mpcustomroomnamestaticText;
	YANGTextCtrl* m_mpcustomroomnametextCtrl;
	wxStaticText* m_mpcustommaxnumplayersstaticText;
	YANGChoice* m_mpcustommaxnumplayerschoice;
	wxStaticText* m_mpcustommaxpingstaticText;
	YANGTextCtrl* m_mpcustommaxpingtextCtrl;
	wxStaticText* m_mpcustomextrahostargsstaticText;
	YANGTextCtrl* m_mpcustomextrahostargstextCtrl;
#if EXTRA_ARGS_FOR_ALL
	wxStaticText* m_mpcustomextraallargsstaticText;
	YANGTextCtrl* m_mpcustomextraallargstextCtrl;
#endif
	YANGCheckBox* m_mpcustomusenullmodemcheckBox;
	wxStaticText* m_mpcustomrxdelaystaticText;
	YANGTextCtrl* m_mpcustomrxdelaytextCtrl;

	YANGPanel* m_mppanel;
	YANGCheckBox* m_mppassprotectcheckBox;
	YANGTextCtrl* m_passprotecttextCtrl;
	YANGCheckBox* m_mpkeeppassprotectcheckBox;
	YANGCheckBox* m_mpadvertiseroomcheckBox;
	YANGCheckBox* m_mpnodialogcheckBox;

	wxStdDialogButtonSizer* m_mpsdbSizer;
	YANGButton* m_mpsdbSizerOK;
	YANGButton* m_mpsdbSizerCancel;

	int m_temp_bloodepimap, m_temp_bloodepimap_cp, m_temp_custommaxnumplayer;

	DECLARE_EVENT_TABLE()
};

#endif
