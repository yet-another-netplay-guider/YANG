/**************************************************************************

Copyright 2011-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_BANLISTDIALOG_H_
#define _YANG_BANLISTDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/gbsizer.h>
#include <wx/listbox.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/dialog.h>
#endif

#include "theme.h"

#define ID_BANLISTENTER 1000
#define ID_BANLISTADD 1001
#define ID_BANLISTREM 1002

class BanListDialog : public YANGDialog 
{
public:
	BanListDialog(wxWindow* parent);
	void ShowBanListDialog();
	void OnBanListAdd(wxCommandEvent& event);
	void OnBanListRem(wxCommandEvent& event);
	void OnOk(wxCommandEvent& event);
	YANGListBox* m_banlistBox;
private:
	wxWindow* m_parent;
	YANGPanel* m_BanListpanel;
	wxStaticText* m_banliststaticText;
	YANGTextCtrl* m_banlisttextCtrl;
	YANGButton* m_banlistaddbutton;
	YANGButton* m_banlistrembutton;
	wxStdDialogButtonSizer* m_BanListsdbSizer;
	YANGButton* m_BanListsdbSizerOK;
	YANGButton* m_BanListsdbSizerCancel;

	DECLARE_EVENT_TABLE()
};

#endif //__wxfbguis__

