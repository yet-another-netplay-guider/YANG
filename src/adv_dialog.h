/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_ADVANCEDDIALOG_H_
#define _YANG_ADVANCEDDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "theme.h"

enum {
	ID_EDUKE32USERPATHCHECKBOX = 1000,
	ID_SELECTEDUKE32USERPATH,
	ID_NDUKEUSERPATHCHECKBOX,
	ID_HDUKEUSERPATHCHECKBOX,
	ID_DUKE3DWUSERPATHCHECKBOX,
	ID_SELECTDUKE3DWUSERPATH,
	ID_SELECTNDUKEUSERPATH,
	ID_SELECTHDUKEUSERPATH,
	ID_VOIDSWUSERPATHCHECKBOX,
	ID_SELECTVOIDSWUSERPATH,
	ID_SELECTBANLISTFILE,
	ID_BROWSERCHECKBOX,
	ID_LOCATEBROWSER,
	ID_SOUNDCMDCHECKBOX,
	ID_SERVERLISTADD,
	ID_SERVERLISTREM,
};

class AdvDialog : public YANGDialog
{
public:
	AdvDialog();
	~AdvDialog();
	void OnSelectDir(wxCommandEvent& event);
	void OnLocateBanListTextFile(wxCommandEvent& event);
	void OnLocateExec(wxCommandEvent& event);
	void OnCheckBoxClick(wxCommandEvent& event);

	void OnServerListAdd(wxCommandEvent& event);
	void OnServerListRem(wxCommandEvent& event);

	//void OnApply(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);
	void OnOk(wxCommandEvent& event);
	void ApplySettings();
private:
	YANGNotebook* m_advancednotebook;
	YANGPanel* m_advpanel;
	wxStaticText* m_userpathstaticText;
	YANGCheckBox* m_eduke32userpathcheckBox;
	YANGTextCtrl* m_eduke32userpathtextCtrl;
	YANGButton* m_eduke32userpathbutton;
	YANGCheckBox* m_duke3dwuserpathcheckBox;
	YANGTextCtrl* m_duke3dwuserpathtextCtrl;
	YANGButton* m_duke3dwuserpathbutton;
	YANGCheckBox* m_ndukeuserpathcheckBox;
	YANGTextCtrl* m_ndukeuserpathtextCtrl;
	YANGButton* m_ndukeuserpathbutton;
	YANGCheckBox* m_hdukeuserpathcheckBox;
	YANGTextCtrl* m_hdukeuserpathtextCtrl;
	YANGButton* m_hdukeuserpathbutton;
	YANGCheckBox* m_voidswuserpathcheckBox;
	YANGTextCtrl* m_voidswuserpathtextCtrl;
	YANGButton* m_voidswuserpathbutton;
	wxStaticText* m_banliststaticText;
	YANGTextCtrl* m_banlisttextCtrl;
	YANGButton* m_banlistbutton;
	YANGCheckBox* m_overridebrowsercheckBox;
	YANGTextCtrl* m_browsertextCtrl;
	YANGButton* m_browserlocatebutton;
	YANGCheckBox* m_overridesoundcheckBox;
	YANGTextCtrl* m_soundcmdtextCtrl;
	YANGCheckBox* m_localipoptimizecheckBox;
#ifndef __WXMSW__
	wxStaticText* m_terminalstaticText;
	YANGTextCtrl* m_terminaltextCtrl;
#endif
	YANGPanel* m_extraserverspanel;
	wxStaticText* m_extraserveraddrstaticText;
	wxStaticText* m_extraserverportnumstaticText;
	YANGTextCtrl* m_extraserverhostaddrtextCtrl;
	wxStaticText* m_extraserverseparatorstaticText;
	YANGTextCtrl* m_extraserverportnumtextCtrl;
	YANGButton* m_extraserveraddbutton;
	YANGListBox* m_extraserverlistBox;
	YANGButton* m_extraserverrembutton;

	wxStdDialogButtonSizer* m_advsdbSizer;
	YANGButton* m_advsdbSizerOK;
	YANGButton* m_advsdbSizerCancel;

	wxArrayString m_extra_addrs;
	wxArrayLong m_extra_portnums;

	DECLARE_EVENT_TABLE()
};

#endif

