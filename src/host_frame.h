/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_HOSTFRAME_H_
#define _YANG_HOSTFRAME_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#include <wx/wfstream.h>
#include <wx/imaglist.h>
#include <wx/gauge.h>
#include <wx/process.h>
#endif

#include "banlist_dialog.h"
#include "lookandfeel_dialog.h"
#include "config.h"
#include "main_frame.h"
#include "mp_common.h"
#include "file_transfer.h"
#include "pingstopwatch.h"
#include "crc.h"
#include "theme.h"

#define ID_HOSTKICK 1000
#define ID_HOSTBAN 1001
#define ID_HOSTBANLIST 1002
#define ID_HOSTREFUSESELECTIONS 1003
#define ID_HOSTGENERALTRANSFERPURPOSE 1004
#define ID_HOSTAUTOACCEPTCHECK 1005
#define ID_HOSTOUTPUTTEXT 1006
#define ID_HOSTINPUTTEXT 1007
#define ID_HOSTCHATOUTPUTSTYLE 1008
//#define ID_HOSTADDNEWLINE 1009
//#define ID_HOSTNATFREE 1010
#define ID_HOSTTIMESTAMPS 1011
#define ID_HOSTPLAYERCOLORCHOICE 1012
#define ID_HOSTSHOWROOMSLIST 1013
//#define ID_HOSTSENDPM 1014
#define ID_HOSTROOMSETTINGS 1015
#define ID_HOSTROOMADVERTISE 1016
#define ID_ADVERTISETIMER 1017
#define ID_HOSTPINGTIMER 1018
#define ID_HOSTFIRSTPINGTIMER 1019
#define ID_HOSTCHECKPINGTIMER 1020
#define ID_HOSTPINGREFRESHTIMER 1021
#define ID_BANLISTCLOSE 1022
#define ID_HOSTLOOKANDFEELCLOSE 1023

#define ID_HOSTPROCESS 1024

enum { ID_HOSTSERVER = wxID_HIGHEST+1, ID_HOSTSOCKET,
ID_HOSTTIMER, ID_HOSTFILETRANSFERTIMER, ID_MASTERSOCKET };

#define COL_HOSTREADY 0
#define COL_HOSTLOCATION 1
#define COL_HOSTNICKNAMES 2
#define COL_HOSTPING 3
#define COL_HOSTFLUX 4
#define COL_HOSTOS 5
#define COL_HOSTDETECTED_IPS 6
#define COL_HOSTINGAME_IPS 7
#define COL_HOSTREQUESTS 8
#define COL_HOSTFILENAMES 9
#define COL_HOSTFILESIZES 10

#define IMG_HOSTCANCEL 0
#define IMG_HOSTACCEPT 1

#define IMG_HOSTNUMBER 2

WX_DECLARE_STRING_HASH_MAP( bool, YANGHashString );

class HostedRoomFrame : public YANGFrame 
{
public:
	HostedRoomFrame();
	~HostedRoomFrame();
	//void RefreshListCtrlSize();
	//void OnResizeWindow(wxSizeEvent& event);
	//void OnMaximize(wxMaximizeEvent& event);
	void FocusFrame();

	void UpdateGameSettings(
		bool isPassProtected,
		const wxString& password,
		GameType game,
		SourcePortType srcport,
		bool use_crypticpassage,
		const wxString& roomname,
		size_t max_num_players,
		bool recorddemo,
		const wxString& demofilename,
		size_t skill,
		BloodMPGameT_Type bloodgametype,
		DescentMPGameT_Type descentgametype,
		Descent2MPGameT_Type descent2gametype,
		DN3DMPGameT_Type dn3dgametype,
		SWMPGameT_Type swgametype,
		DN3DSpawnType dn3dspawn,
		BloodMPMonsterType blood_monster,
		BloodMPWeaponType blood_weapon,
		BloodMPItemType blood_item,
		BloodMPRespawnType blood_respawn,
		const wxString& args_for_host,
		const wxString& args_for_all,
		size_t maxping,
		size_t rxdelay,
		bool launch_usermap,
		size_t epimap_num,
		const wxString& usermap,
		const wxArrayString& modfiles,
		const wxString& modname,
		const wxString& modconfile,
		const wxString& moddeffile,
		bool showmodurl,
		const wxString& modurl,
		bool advertise,
		size_t connectiontype,
		bool allow_ingame_joining);

	void UpdateSettingsList();
	bool StartServer();
	void OnTextURLEvent(wxTextUrlEvent& event);
	//void OnInsertNewLine(wxCommandEvent& event);
	void OnEnterText(wxCommandEvent& event);
	//void OnUpdateTextInput(wxCommandEvent& event);
	void OnPlayerColorChoice(wxCommandEvent& event);
	void OnKickOrBanClient(wxCommandEvent& event);
	void OnOpenBanListDialog(wxCommandEvent& event);
	void OnOpenRoomSettings(wxCommandEvent& event);
	void OnLaunchGame(wxCommandEvent& event);
	void FindNextDownloadRequest();
	void AcceptTransferRequest(size_t player_index);
	void OnTransferButtonClick(wxCommandEvent& event);
	void OnRefuseButtonClick(wxCommandEvent& event);
	void OnTransferThreadEnd(wxCommandEvent& event);
	void OnBanListClose(wxCommandEvent& event);
	void OnLookAndFeelClose(wxCommandEvent& event);
	void OnTimer(wxTimerEvent& event);
	void OnPingTimer(wxTimerEvent& event);
	void OnFirstPingTimer(wxTimerEvent& event);
	void OnCheckPingTimer(wxTimerEvent& event);
	void OnFileTransferTimer(wxTimerEvent& event);
	void OnServerEvent(wxSocketEvent& event);
	void OnSocketEvent(wxSocketEvent& event);
	void SendRoomInfo(bool is_new_room);
	void OnServerListSocketEvent(wxSocketEvent& event);
	void ConnectToServerList();
	void Advertise();
	void OnAdvertiseButtonClick(wxCommandEvent& event);
	void OnTimeStampsCheck(wxCommandEvent& event);
	//void OnNatFreeCheck(wxCommandEvent& event);
	void OnServerListTimer(wxTimerEvent& event);
	void OnPingRefreshTimer(wxTimerEvent& event);
	void OnCloseRoom(wxCommandEvent& event);
	void OnShowRoomsList(wxCommandEvent& event);
	void OnOpenChatOutputStyle(wxCommandEvent& event);
	void OnCloseWindow(wxCloseEvent& event);

	void OnAutoAcceptCheck(wxCommandEvent& event);

	void OnProcessTerm(wxProcessEvent& event);

	wxArrayString GetServerInfoSockStrList();
	wxArrayString GetServerModFilesSockStrList();

private:
	YANGPanel* m_hostedroompanel;
	YANGListCtrl* m_hostplayerslistCtrl;
	YANGListCtrl* m_hostsettingslistCtrl;
	YANGButton* m_hostkicknicknamebutton;
	YANGButton* m_hostbannicknamebutton;
	YANGButton* m_hostbanlistbutton;

	YANGButton* m_hosttransferrefusebutton;
	YANGButton* m_hosttransferdynamicbutton;
	wxGauge* m_hosttransfergauge;
	wxStaticText* m_hosttransferstaticText;

	YANGCheckBox* m_hosttransfercheckBox;
	YANGTextCtrl* m_hostoutputtextCtrl;
	YANGTextCtrl* m_hostinputtextCtrl;
	YANGButton* m_hostchatoutputstylebutton;
	//YANGButton* m_hostnewlinebutton;
	//wxCheckBox* m_hostedroomnatfreecheckBox;
	YANGCheckBox* m_hostedroomtimestampscheckBox;
	wxStaticText* m_hostplayercolorstaticText;
	YANGChoice* m_hostplayercolorchoice;
	YANGButton* m_hostcloseroombutton;
	YANGButton* m_hostshowroomslistbutton;
	//YANGButton* m_hostsendpmbutton;
	YANGButton* m_hostsettingsbutton;
	YANGButton* m_hostadvertisebutton;
	YANGButton* m_hostlaunchbutton;
	wxImageList* m_ImageList;
	wxProcess *m_process;

	void AddMessage(const wxString& msg, bool allow_timestamp);
	void AcceptClientJoin(wxSocketBase* sock, const wxArrayString& client_join_request);
	void SetInGame(bool in_game);
	void RemoveInRoomClient(size_t index);
	void ClearPingTimers();
	void SaveUIControlSizes();
	void DoRename(size_t index, const wxString& newname);
	void DoSetRoomName(const wxString& newname);
	void DoEnterText(const wxString& text);
	void SetChatInputStyleOnUpdate();
	void SetChatStyle();

	MainFrame* m_mainframe;

	wxSocketServer* m_server;

	// Avoids references to uninitialized variables on settings update when we don't want to!
	// This is related to file transfer.
	bool m_not_first_update;
	TransferStateType m_transfer_state;
	wxSocketServer* m_transfer_server;
	//wxSocketBase* m_transfer_sock;
	size_t m_transfer_client_index;
	wxString m_transfer_filename;
	DWORD m_transfer_crc32;
	wxThread* m_transfer_thread;
	wxFile* m_transfer_fp;
	bool m_transfer_is_download;
	wxTimer* m_transfer_timer;
	// We need to store the actual IP the client is connected to us with.
	// Maybe there's a mixed Internet/LAN room, with the host being one of
	// the LAN players, and the LAN IP being used with the local players.
	wxString m_transfer_ipaddress;

	ExtendedRoomPlayersTable m_players_table;
	HostRoomClientsTable m_host_clients_table;
	//wxString m_selfdetectedips[MAX_NUM_PLAYERS];
	//size_t m_num_players;

	BanListDialog* m_banlist_dialog;
	LookAndFeelDialog* m_lookandfeel_dialog;

	bool m_allow_sounds;

	GameType m_game;
	SourcePortType m_srcport;
	bool m_use_crypticpassage;
	wxString m_roomname;
	size_t m_max_num_players;
	size_t m_skill;
	BloodMPGameT_Type m_bloodgametype;
	DescentMPGameT_Type m_descentgametype;
	Descent2MPGameT_Type m_descent2gametype;
	DN3DMPGameT_Type m_dn3dgametype;
	SWMPGameT_Type m_swgametype;
	DN3DSpawnType m_dn3dspawn;
	BloodMPMonsterType m_blood_monster;
	BloodMPWeaponType m_blood_weapon;
	BloodMPItemType m_blood_item;
	BloodMPRespawnType m_blood_respawn;
	wxString m_args_for_host, m_args_for_all;
	long m_maxping;
	size_t m_rxdelay;
	bool m_recorddemo;
	wxString m_demofilename;
	bool m_launch_usermap;
	size_t m_epimap_num;
	wxString m_usermap, m_extra_usermap;
	wxArrayString m_modfiles;
	wxString m_modname;
	wxString m_modconfile;
	wxString m_moddeffile;
	bool m_showmodurl;
	wxString m_modurl;
	size_t m_connectiontype;
	bool m_allow_ingame_joining;
	bool m_in_game;

	bool m_use_password;
	wxString m_password;
	bool m_is_advertised;

	wxString s_gamename, s_srcport, s_players, s_map, s_mod, s_gametype, s_skill, s_spawn, s_monster, s_weapon, s_item, s_respawn, s_maxping, s_rxdelay, s_packetmode, s_connection;

	size_t m_curr_server, m_num_of_lists, m_num_of_extra_lists;

	wxSocketClient* m_client;
	wxTimer *m_connection_timeout_timer, *m_serverlist_timer;
	wxTimer *m_ping_timer, *m_firstping_timer, *m_checkping_timer, *m_pingrefresh_timer;

	size_t m_ping_times, m_checkping_times, m_pinging, m_ping_id, m_checkping_id;
	long m_min_ping, m_max_ping, m_ping_index, m_checkping_index;
	PingStopWatch ping, checkping;

	wxColour m_def_listctrl_color;
	wxColour m_transfer_listctrl_color;

	wxArrayString m_banned_ips_array;
	YANGHashString m_banned_ips_hash;

	ArrayOfStringArrays m_join_requests_arr;
	ArrayOfSocketPtrs m_join_requests_sock_arr;
	wxArrayString m_unbanned_ips_arr, m_join_requests_address_arr;

	DECLARE_EVENT_TABLE()
};

#endif

