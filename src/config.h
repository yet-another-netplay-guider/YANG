/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_CONFIG_H_
#define _YANG_CONFIG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/arrstr.h>
#include <wx/string.h>
#endif

#define YANG_CONFIG_FULLFILEPATH (*g_launcher_user_profile_path+wxT("yang.cfg"))

// [JM] Get rid of these when we have structs for games.
#define GAMECODE_BLOOD wxT("blood")
#define GAMECODE_DESCENT wxT("descent")
#define GAMECODE_DESCENT2 wxT("descent2")
#define GAMECODE_DN3D wxT("dn3d")
#define GAMECODE_SW wxT("sw")
#define GAMECODE_CUSTOM wxT("custom")

// [JM] Get rid of these when we have structs for ports.
#define SRCPORTCODE_DOSBLOODSW wxT("dosbloodsw")
#define SRCPORTCODE_DOSBLOODRG wxT("dosbloodrg")
#define SRCPORTCODE_DOSBLOODPP wxT("dosbloodpp")
#define SRCPORTCODE_DOSBLOODOU wxT("dosbloodou")
#define SRCPORTCODE_DOSDESCENT wxT("dosdescent")
#define SRCPORTCODE_D1XREBIRTH wxT("d1xrebirth")
#define SRCPORTCODE_DOSDESCENT2 wxT("dosdescent2")
#define SRCPORTCODE_D2XREBIRTH wxT("d2xrebirth")
#define SRCPORTCODE_DOSDUKESW wxT("dosdukesw")
#define SRCPORTCODE_DOSDUKERG wxT("dosdukerg")
#define SRCPORTCODE_DOSDUKEAE wxT("dosdukeae")
#define SRCPORTCODE_DUKE3DW wxT("duke3dw")
#define SRCPORTCODE_EDUKE32 wxT("eduke32")
#define SRCPORTCODE_NDUKE wxT("nduke")
#define SRCPORTCODE_HDUKE wxT("hduke")
#define SRCPORTCODE_XDUKE wxT("xduke")
#define SRCPORTCODE_DOSSWSW wxT("dosswsw")
#define SRCPORTCODE_DOSSWRG wxT("dosswrg")
#define SRCPORTCODE_VOIDSW wxT("voidsw")
#define SRCPORTCODE_SWP wxT("swp")
#define SRCPORTCODE_CUSTOM wxT("doscustom")

#define HAVE_BLOOD ((g_configuration->have_dosbloodsw || g_configuration->have_dosbloodrg || g_configuration->have_dosbloodpp || g_configuration->have_dosbloodou) && g_configuration->have_dosbox)
#define HAVE_DESCENT ((g_configuration->have_dosdescent && g_configuration->have_dosbox) || g_configuration->have_d1xrebirth)
#define HAVE_DESCENT2 ((g_configuration->have_dosdescent2 && g_configuration->have_dosbox) || g_configuration->have_d2xrebirth)
#define HAVE_DN3D (((g_configuration->have_dosdukesw || g_configuration->have_dosdukerg || g_configuration->have_dosdukeae) && g_configuration->have_dosbox) || g_configuration->have_duke3dw || g_configuration->have_eduke32 || g_configuration->have_nduke || g_configuration->have_hduke || g_configuration->have_xduke)
#define HAVE_SW (((g_configuration->have_dosswsw || g_configuration->have_dosswrg) && g_configuration->have_dosbox) || g_configuration->have_voidsw || g_configuration->have_swp)
#define HAVE_CUSTOM (g_configuration->custom_profile_list && g_configuration->have_dosbox)

enum GameType
{
	GAME_BLOOD,
	GAME_DESCENT,
	GAME_DESCENT2,
	GAME_DN3D,
	GAME_SW,
	GAME_CUSTOM,
	GAME_NUMOFGAMES
};

enum SourcePortType
{
	SOURCEPORT_DOSBLOODSW,
	SOURCEPORT_DOSBLOODRG,
	SOURCEPORT_DOSBLOODPP,
	SOURCEPORT_DOSBLOODOU,
	SOURCEPORT_DOSDESCENT,
	SOURCEPORT_D1XREBIRTH,
	SOURCEPORT_DOSDESCENT2,
	SOURCEPORT_D2XREBIRTH,
	SOURCEPORT_DOSDUKESW,
	SOURCEPORT_DOSDUKERG,
	SOURCEPORT_DOSDUKEAE,
	SOURCEPORT_DUKE3DW,
	SOURCEPORT_EDUKE32,
	SOURCEPORT_NDUKE,
	SOURCEPORT_HDUKE,
	SOURCEPORT_XDUKE,
	SOURCEPORT_DOSSWSW,
	SOURCEPORT_DOSSWRG,
	SOURCEPORT_VOIDSW,
	SOURCEPORT_SWP,
	SOURCEPORT_CUSTOM,
	SOURCEPORT_NUMOFPORTS
};

enum CDRomMountType
{
	CDROMMOUNT_NONE,
	CDROMMOUNT_DIR,
	CDROMMOUNT_IMG
};

enum BloodMPGameT_Type
{
	BLOODMPGAMETYPE_COOP = 1,
	BLOODMPGAMETYPE_BB,
	BLOODMPGAMETYPE_TEAMS
};

enum DescentMPGameT_Type
{
	DESCENTMPGAMETYPE_ANARCHY,
	DESCENTMPGAMETYPE_TEAM_ANARCHY,
	DESCENTMPGAMETYPE_ROBO_ANARCHY,
	DESCENTMPGAMETYPE_COOPERATIVE
};

enum Descent2MPGameT_Type
{
	DESCENT2MPGAMETYPE_ANARCHY,
	DESCENT2MPGAMETYPE_TEAM_ANARCHY,
	DESCENT2MPGAMETYPE_ROBO_ANARCHY,
	DESCENT2MPGAMETYPE_COOPERATIVE,
	DESCENT2MPGAMETYPE_CAPTURE_THE_FLAG,
	DESCENT2MPGAMETYPE_HOARD,
	DESCENT2MPGAMETYPE_TEAM_HOARD
};

enum DN3DMPGameT_Type
{
	DN3DMPGAMETYPE_DMSPAWN = 1,
	DN3DMPGAMETYPE_COOP,
	DN3DMPGAMETYPE_DMNOSPAWN,
	DN3DMPGAMETYPE_TDMSPAWN,
	DN3DMPGAMETYPE_TDMNOSPAWN
};

enum SWMPGameT_Type
{
	SWMPGAMETYPE_WBSPAWN,
	SWMPGAMETYPE_WBNOSPAWN,
	SWMPGAMETYPE_COOP
};

enum DN3DSpawnType
{
	DN3DSPAWN_NONE,
	DN3DSPAWN_MONSTERS,
	DN3DSPAWN_ITEMS,
	DN3DSPAWN_INVENTORY,
	DN3DSPAWN_ALL
};

enum BloodMPMonsterType
{
	BLOODMPMONSTERTYPE_NONE,
	BLOODMPMONSTERTYPE_ENABLED,
	BLOODMPMONSTERTYPE_RESPAWN
};

enum BloodMPWeaponType
{
	BLOODMPWEAPONTYPE_NORESPAWN,
	BLOODMPWEAPONTYPE_PERMANENT,
	BLOODMPWEAPONTYPE_RESPAWN,
	BLOODMPWEAPONTYPE_MARKERS
};

enum BloodMPItemType
{
	BLOODMPITEMTYPE_NORESPAWN,
	BLOODMPITEMTYPE_RESPAWN,
	BLOODMPITEMTYPE_MARKERS
};

enum BloodMPRespawnType
{
	BLOODMPRESPAWNTYPE_RANDOM,
	BLOODMPRESPAWNTYPE_WEAPONS,
	BLOODMPRESPAWNTYPE_ENEMIES
};

enum PacketMode
{
	PACKETMODE_AUTO,
	PACKETMODE_BROADCAST,
	PACKETMODE_MASTERSLAVE
};

enum ConnectionType
{
	CONNECTIONTYPE_PEER2PEER,
	CONNECTIONTYPE_MASTERSLAVE,
	CONNECTIONTYPE_SERVERCLIENT
};

#define SOURCEPORT_FIRSTBLOODPORT SOURCEPORT_DOSBLOODSW
#define SOURCEPORT_FIRSTDESCENTPORT SOURCEPORT_DOSDESCENT
#define SOURCEPORT_FIRSTDESCENT2PORT SOURCEPORT_DOSDESCENT2
#define SOURCEPORT_FIRSTDUKEPORT SOURCEPORT_DOSDUKESW
#define SOURCEPORT_FIRSTSWPORT SOURCEPORT_DOSSWSW
#define CONFIG_DEFAULT_VERSION 900

//#define COLORDIALOG_NUM_CUSTOM_COLORS 20
#define CUSTOM_FONT_DEFAULT_SIZE 12

#define DN3D_NUM_OF_PLAYER_COLORS 10
#define SW_NUM_OF_PLAYER_COLORS 8

void RestoreDefaultSounds(wxString& snd_join, wxString& snd_leave,
						  wxString& snd_send, wxString& snd_receive, wxString& snd_error);

struct custom_profile_t
{
	wxString profilename;
	bool spgameonly;
	wxString spexecutable;
	bool sameexecutable;
	wxString mpexecutable;
	CDRomMountType cdmount;
	wxString cdlocation;
	wxString cdimage;
	bool ingamesupport;
	bool usenetbios;
	wxString netbiospath;
	wxString extraargs;
	wxString extrahostargs;
	wxString extraallargs;
	custom_profile_t* previous;
	custom_profile_t* next;
};

struct dn3dtcmod_profile_t
{
	wxString profilename;
	wxArrayString modfiles;
	wxString confile;
	wxString deffile;
	bool usemodurl;
	wxString modurl;
	dn3dtcmod_profile_t* previous;
	dn3dtcmod_profile_t* next;
};

struct swtcmod_profile_t
{
	wxString profilename;
	wxArrayString modfiles;
	wxString deffile;
	bool usemodurl;
	wxString modurl;
	swtcmod_profile_t* previous;
	swtcmod_profile_t* next;
};

class LauncherConfig
{
public:
	LauncherConfig();
	bool Load();
	bool Save();
//	bool Remove();
#if (!((defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__)))
	void DetectTerminal();
#endif
#if ASK_TO_ACCEPT_LICENSE
	bool is_license_accepted;
#endif
	bool have_custom_mpprofile;
	// Visual appearance
	bool theme_is_dark, use_custom_chat_text_layout, use_custom_chat_text_colors;
	wxString chat_font;
	int chat_font_size;
	unsigned char chat_textcolor_red, chat_textcolor_green, chat_textcolor_blue;
	unsigned char chat_backcolor_red, chat_backcolor_green, chat_backcolor_blue;
	/*  unsigned char custcolor_red[COLORDIALOG_NUM_CUSTOM_COLORS],
	custcolor_green[COLORDIALOG_NUM_CUSTOM_COLORS],
	custcolor_blue[COLORDIALOG_NUM_CUSTOM_COLORS];*/

	bool check_for_updates_on_startup;
	// Configuration version number
	unsigned long cfg_ver;
	// Room Host / Single Player settings
	enum SourcePortType gamelaunch_source_port, gamelaunch_blood_source_port, gamelaunch_descent_source_port, gamelaunch_descent2_source_port, gamelaunch_dn3d_source_port, gamelaunch_sw_source_port;
	bool gamelaunch_blood_use_crypticpassage;
	wxString gamelaunch_custom_profilename, gamelaunch_custom_profilename_in_sp, gamelaunch_customselect_profilename;
	wxString gamelaunch_dn3dtcmod_profilename, gamelaunch_dn3dtcmodselect_profilename;
	wxString gamelaunch_swtcmod_profilename, gamelaunch_swtcmodselect_profilename;
	long gamelaunch_blood_skill, gamelaunch_dn3d_skill, gamelaunch_sw_skill, gamelaunch_dn3d_skill_in_sp, gamelaunch_sw_skill_in_sp;
	wxString gamelaunch_blood_args_in_sp, gamelaunch_descent_args_in_sp, gamelaunch_descent2_args_in_sp, gamelaunch_dn3d_args_in_sp, gamelaunch_sw_args_in_sp;
	long gamelaunch_bloodlvl, gamelaunch_bloodlvl_cp, gamelaunch_descentlvl, gamelaunch_descent2lvl, gamelaunch_dn3dlvl, gamelaunch_swlvl;
	wxString gamelaunch_bloodusermap, gamelaunch_descentusermap, gamelaunch_descent2usermap, gamelaunch_dn3dusermap, gamelaunch_swusermap;
	wxString gamelaunch_blood_roomname, gamelaunch_descent_roomname, gamelaunch_descent2_roomname, gamelaunch_dn3d_roomname, gamelaunch_sw_roomname, gamelaunch_custom_roomname;
	long gamelaunch_numoffakeplayers;
	bool gamelaunch_enable_bot_ai;
	long gamelaunch_blood_numofplayers, gamelaunch_descent_numofplayers, gamelaunch_descent2_numofplayers, gamelaunch_dn3d_numofplayers, gamelaunch_sw_numofplayers, gamelaunch_custom_numofplayers;
	long gamelaunch_blood_maxping, gamelaunch_descent_maxping, gamelaunch_descent2_maxping, gamelaunch_dn3d_maxping, gamelaunch_sw_maxping, gamelaunch_custom_maxping;
	long gamelaunch_custom_rxdelay;
	BloodMPGameT_Type gamelaunch_bloodgametype, gamelaunch_bloodgametype_in_sp;
	DescentMPGameT_Type gamelaunch_descentgametype;
	Descent2MPGameT_Type gamelaunch_descent2gametype;
	DN3DMPGameT_Type gamelaunch_dn3dgametype, gamelaunch_dn3dgametype_in_sp;
	SWMPGameT_Type gamelaunch_swgametype;
	DN3DSpawnType gamelaunch_dn3dspawn;
	BloodMPMonsterType gamelaunch_blood_monster;
	BloodMPWeaponType gamelaunch_blood_weapon;
	BloodMPItemType gamelaunch_blood_item;
	BloodMPRespawnType gamelaunch_blood_respawn;
	PacketMode gamelaunch_blood_packetmode;
	ConnectionType gamelaunch_duke3dw_connectiontype;
	ConnectionType gamelaunch_eduke32_connectiontype;
	bool gamelaunch_custom_use_nullmodem;
	wxString gamelaunch_blood_args_for_host, gamelaunch_descent_args_for_host, gamelaunch_descent2_args_for_host, gamelaunch_dn3d_args_for_host, gamelaunch_sw_args_for_host;
	wxString gamelaunch_blood_args_for_all, gamelaunch_descent_args_for_all, gamelaunch_descent2_args_for_all, gamelaunch_dn3d_args_for_all, gamelaunch_sw_args_for_all;
	wxString gamelaunch_modname, gamelaunch_modurl;
	bool gamelaunch_custom_use_profilename;
	bool gamelaunch_show_modurl, gamelaunch_advertise, gamelaunch_skip_settings;
	long gamelaunch_dn3d_playercolnum, gamelaunch_sw_playercolnum;
	bool gamelaunch_use_password;
	wxString gamelaunch_password;
	//  unsigned long last_serverlist_in_cycle;
	//  bool gamelaunch_enable_natfree;
	bool show_timestamps;
	// Blood Ports Setup
	bool have_dosbloodsw, have_dosbloodrg, have_dosbloodpp, have_dosbloodou;
	wxString dosbloodsw_exec, dosbloodrg_exec, dosbloodpp_exec, dosbloodou_exec;
	wxString blood_maps_dir;
	// Descent Ports Setup
	bool have_dosdescent, have_d1xrebirth;
	wxString dosdescent_exec, d1xrebirth_exec;
	wxString descent_maps_dir;
	// Descent 2 Ports Setup
	bool have_dosdescent2, have_d2xrebirth;
	wxString dosdescent2_exec, d2xrebirth_exec;
	wxString descent2_maps_dir;
	// Duke Nukem 3D Ports Setup
	bool have_dosdukesw, have_dosdukerg, have_dosdukeae, have_duke3dw, have_eduke32, have_nduke, have_hduke, have_xduke;
	wxString dosdukesw_exec, dosdukerg_exec, dosdukeae_exec, duke3dw_exec, eduke32_exec, nduke_exec, hduke_exec, xduke_exec;
	wxString dn3d_maps_dir;
	dn3dtcmod_profile_t *dn3dtcmod_profile_list, *dn3dtcmod_profile_current;
	// Shadow Warrior Ports Setup
	bool have_dosswsw, have_dosswrg, have_voidsw, have_swp;
	wxString dosswsw_exec, dosswrg_exec, voidsw_exec, swp_exec;
	wxString sw_maps_dir;
	swtcmod_profile_t *swtcmod_profile_list, *swtcmod_profile_current;
	// Custom DOS games Setup
	custom_profile_t *custom_profile_list, *custom_profile_current;
	// DOSBox Setup
	bool have_dosbox, dosbox_use_conf, have_bmouse;
	CDRomMountType dosbox_cdmount;
	wxString dosbox_exec, dosbox_conf, dosbox_cdrom_location, dosbox_cdrom_image, bmouse_exec;
#ifndef __WXMSW__
	// Windows compatibility (e.g. Wine) Setup, and a terminal.
	bool have_wine;
	wxString wine_exec, terminal_fullcmd;
#endif
	// Multiplayer and network settings
	bool play_snd_join, play_snd_leave, play_snd_send, play_snd_receive, play_snd_error,
		mute_sounds_while_ingame;
	wxString nickname, game_nickname,
		snd_join_file, snd_leave_file, snd_send_file, snd_receive_file, snd_error_file;
	long game_port_number, server_port_number;
#ifdef ENABLE_UPNP
	bool enable_upnp;
#endif
	// Advanced options
	bool duke3dw_userpath_use, eduke32_userpath_use, nduke_userpath_use, hduke_userpath_use, voidsw_userpath_use,
		override_browser, override_soundcmd, enable_localip_optimization;
	wxString duke3dw_userpath, eduke32_userpath, nduke_userpath, hduke_userpath, voidsw_userpath,
		banlist_filepath, playsound_cmd, browser_exec;
	wxArrayString extraservers_addrs;
	wxArrayLong extraservers_portnums;
	// Host in-room options
	bool host_autoaccept_downloads, host_accept_wakeup;
	// Main window: Retrieve rooms on startup, and stuff for manual joining.
	bool lookup_on_startup;
	wxArrayString manualjoin_names, manualjoin_addrs;
	wxArrayLong manualjoin_portnums;
	wxString game_filter_tree;

	int main_col_private_len, main_col_loc_len, main_col_roomname_len, main_col_ping_len, main_col_flux_len,
		main_col_game_len, main_col_srcport_len, main_col_players_len, main_col_map_len,
		main_col_tcmod_len, main_col_gametype_len, main_col_skill_len, main_col_ingame_len;
	int mainplayer_col_loc_len, mainplayer_col_nick_len,
		mainplayer_col_ip_len, mainplayer_col_os_len;
	int host_col_ready_len, host_col_loc_len, host_col_nick_len, host_col_ping_len, host_col_flux_len,
		host_col_os_len, host_col_detectedip_len, host_col_ingameip_len,
		host_col_requests_len, host_col_filenames_len, host_col_filesizes_len;
	int host_col_settings_len, host_col_value_len;
	int client_col_ready_len, client_col_loc_len, client_col_nick_len, client_col_ping_len, client_col_flux_len,
		client_col_os_len, client_col_detectedip_len, client_col_ingameip_len;
	int client_col_settings_len, client_col_value_len;

	int main_win_width, main_win_height, host_win_width,
		host_win_height, client_win_width, client_win_height;

	bool main_is_maximized, host_is_maximized, client_is_maximized;
};

#endif
