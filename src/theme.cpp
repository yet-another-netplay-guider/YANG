/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/frame.h>
#include <wx/dialog.h>
#include <wx/panel.h>
#include <wx/notebook.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/radiobut.h>
#include <wx/textctrl.h>
#include <wx/hyperlink.h>
#include <wx/listbox.h>
#include <wx/choice.h>
#include <wx/combobox.h>
#include <wx/listctrl.h>
#include <wx/treectrl.h>
#include <wx/statline.h>
#endif

#include "theme.h"
#include "yang.h"
/*
void SetDefaultThemeColour(wxWindow* window)
{
wxVisualAttributes l_attr = window->GetDefaultAttributes();
window->SetForegroundColour(l_attr.colFg);
window->SetBackgroundColour(l_attr.colBg);
}

void SetStatusBarThemeColour(wxStatusBar* status_bar)
{
if (g_configuration && g_configuration->theme_is_dark)
status_bar->SetBackgroundColour(wxColour(191, 191, 191));
//l_status_bar->SetForegroundColour(wxColour(255, 255, 0));
//else
//  SetDefaultThemeColour(status_bar);
}
*/
YANGFrame::YANGFrame(): wxFrame()
{
	SetThemeColour();
}
YANGFrame::YANGFrame(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxFrame(parent, id, title, pos, size, style, name)
{
	SetThemeColour();
}
/*
//wxStatusBar* YANGFrame::OnCreateStatusBar(int number, long style, wxWindowID id, const wxString& name)
wxStatusBar* YANGFrame::CreateStatusBar(int number, long style, wxWindowID id, const wxString& name)
{
wxStatusBar* l_status_bar = new wxStatusBar(this, id, style, name);
l_status_bar->SetFieldsCount(number);
//SetStatusBarThemeColour(l_status_bar);
SetStatusBar(l_status_bar);
return l_status_bar;
}
*/
void YANGFrame::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(255, 255, 0));
		SetBackgroundColour(wxColour(127, 127, 127));
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGDialog::YANGDialog(): wxDialog()
{
	SetThemeColour();

}
YANGDialog::YANGDialog(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxDialog(parent, id, title, pos, size, style, name)
{
	SetThemeColour();
}

void YANGDialog::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(255, 255, 0));
		SetBackgroundColour(wxColour(127, 127, 127));
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGPanel::YANGPanel(): wxPanel()
{
	SetThemeColour();
}

YANGPanel::YANGPanel(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxPanel(parent, id, pos, size, style, name)
{
	SetThemeColour();
}

void YANGPanel::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(255, 255, 0));
		SetBackgroundColour(wxColour(127, 127, 127));
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGListbook::YANGListbook(): wxListbook()
{
	SetThemeColour();
}

YANGListbook::YANGListbook(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxListbook(parent, id, pos, size, style, name)
{
	SetThemeColour();
}

void YANGListbook::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(*wxGREEN);
		SetBackgroundColour(*wxBLACK);
		GetListView()->SetForegroundColour(*wxGREEN);
		GetListView()->SetBackgroundColour(*wxBLACK);
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGNotebook::YANGNotebook(): wxNotebook()
{
	SetThemeColour();
}

YANGNotebook::YANGNotebook(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxNotebook(parent, id, pos, size, style|wxNB_NOPAGETHEME, name)
{
	SetThemeColour();
}

void YANGNotebook::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(255, 255, 0));
		SetBackgroundColour(wxColour(127, 127, 127));
	}
	//else
	//  SetDefaultThemeColour(this);
}
/*
YANGMenuBar::YANGMenuBar(long style): wxMenuBar(style)
{
SetThemeColour();
}

YANGMenuBar::YANGMenuBar(size_t n, wxMenu* menus[], const wxString titles[], long style): wxMenuBar(n, menus, titles, style)
{
SetThemeColour();
}

void YANGMenuBar::SetThemeColour()
{
if (g_configuration && g_configuration->theme_is_dark)
SetBackgroundColour(wxColour(191, 191, 191));
//else
//  SetDefaultThemeColour(this);
}
*/
YANGButton::YANGButton(): wxButton()
{
	SetThemeColour();
}

YANGButton::YANGButton(wxWindow *parent, wxWindowID id,
					   const wxString& label,
					   const wxPoint& pos,
					   const wxSize& size, long style,
					   const wxValidator& validator,
					   const wxString& name):
wxButton(parent, id, label, pos, size, style, validator, name)
{
	SetThemeColour();
}

void YANGButton::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(0, 0, 127));
		SetBackgroundColour(wxColour(191, 191, 191));
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGBitmapButton::YANGBitmapButton(): wxBitmapButton()
{
	SetThemeColour();
}

YANGBitmapButton::YANGBitmapButton(wxWindow *parent, wxWindowID id,
					   const wxBitmap& bitmap,
					   const wxPoint& pos,
					   const wxSize& size, long style,
					   const wxValidator& validator,
					   const wxString& name):
wxBitmapButton(parent, id, bitmap, pos, size, style, validator, name)
{
	SetThemeColour();
}

void YANGBitmapButton::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(0, 0, 127));
		SetBackgroundColour(wxColour(191, 191, 191));
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGToggleButton::YANGToggleButton(): wxToggleButton()
{
	SetThemeColour();
}

YANGToggleButton::YANGToggleButton(wxWindow* parent, wxWindowID id,
								   const wxString& label, const wxPoint& pos,
								   const wxSize& size, long style,
								   const wxValidator& val,
								   const wxString& name):
wxToggleButton(parent, id, label, pos, size, style, val, name)
{
	SetThemeColour();
}

void YANGToggleButton::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(0, 0, 127));
		SetBackgroundColour(wxColour(191, 191, 191));
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGCheckBox::YANGCheckBox(): wxCheckBox()
{
	SetThemeColour();
}

YANGCheckBox::YANGCheckBox(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos, const wxSize& size, long style, const wxValidator& val, const wxString& name):
wxCheckBox(parent, id, label, pos, size, style, val, name)
{
	SetThemeColour();
}

void YANGCheckBox::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(255, 255, 0));
		SetBackgroundColour(wxColour(127, 127, 127));
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGRadioButton::YANGRadioButton(): wxRadioButton()
{
	SetThemeColour();
}

YANGRadioButton::YANGRadioButton(wxWindow* parent, wxWindowID id, const wxString& label, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name):
wxRadioButton(parent, id, label, pos, size, style, validator, name)
{
	SetThemeColour();
}

void YANGRadioButton::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(255, 255, 0));
		SetBackgroundColour(wxColour(127, 127, 127));
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGTextCtrl::YANGTextCtrl(): wxTextCtrl()
{
	SetThemeColour();
}

YANGTextCtrl::YANGTextCtrl(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name):
wxTextCtrl(parent, id, value, pos, size, style, validator, name)
{
	SetThemeColour();
}

void YANGTextCtrl::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(*wxGREEN);
		SetBackgroundColour(*wxBLACK);
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGHyperlinkCtrl::YANGHyperlinkCtrl(): wxHyperlinkCtrl()
{
	SetThemeColour();
}

YANGHyperlinkCtrl::YANGHyperlinkCtrl(wxWindow* parent, wxWindowID id, const wxString & label, const wxString & url, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxHyperlinkCtrl(parent, id, label, url, pos, size, style, name)
{
	SetThemeColour();
}

void YANGHyperlinkCtrl::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetNormalColour(*wxCYAN);
		SetVisitedColour(*wxCYAN);
		SetBackgroundColour(wxColour(127, 127, 127));
	}
	else
		SetVisitedColour(GetNormalColour());
}

YANGListBox::YANGListBox(): wxListBox()
{
	SetThemeColour();
}

YANGListBox::YANGListBox(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, int n, const wxString choices[], long style, const wxValidator& validator, const wxString& name):
wxListBox(parent, id, pos, size, n, choices, style, validator, name)
{
	SetThemeColour();
}

YANGListBox::YANGListBox(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style, const wxValidator& validator, const wxString& name):
wxListBox(parent, id, pos, size, choices, style, validator, name)
{
	SetThemeColour();
}

void YANGListBox::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(*wxGREEN);
		SetBackgroundColour(*wxBLACK);
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGChoice::YANGChoice(): wxChoice()
{
	SetThemeColour();
}

YANGChoice::YANGChoice(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, int n, const wxString choices[], long style, const wxValidator& validator, const wxString& name):
wxChoice(parent, id, pos, size, n, choices, style, validator, name)
{
	SetThemeColour();
}

YANGChoice::YANGChoice(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style, const wxValidator& validator, const wxString& name):
wxChoice(parent, id, pos, size, choices, style, validator, name)
{
	SetThemeColour();
}

void YANGChoice::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(0, 0, 127));
		SetBackgroundColour(wxColour(191, 191, 191));
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGComboBox::YANGComboBox(): wxComboBox()
{
	SetThemeColour();
}

YANGComboBox::YANGComboBox(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, int n, const wxString choices[], long style, const wxValidator& validator, const wxString& name):
wxComboBox(parent, id, value, pos, size, n, choices, style, validator, name)
{
	SetThemeColour();
}

YANGComboBox::YANGComboBox(wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, const wxArrayString& choices, long style, const wxValidator& validator, const wxString& name):
wxComboBox(parent, id, value, pos, size, choices, style, validator, name)
{
	SetThemeColour();
}

void YANGComboBox::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(0, 0, 127));
		SetBackgroundColour(wxColour(191, 191, 191));
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGListCtrl::YANGListCtrl(): wxListCtrl()
{
	SetThemeColour();
}

YANGListCtrl::YANGListCtrl(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name):
wxListCtrl(parent, id, pos, size, style, validator, name)
{
	SetThemeColour();
}

void YANGListCtrl::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(wxColour(127, 0, 0));
		SetBackgroundColour(*wxBLACK);
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGTreeCtrl::YANGTreeCtrl(): wxTreeCtrl()
{
	SetThemeColour();
}

YANGTreeCtrl::YANGTreeCtrl(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name):
wxTreeCtrl(parent, id, pos, size, style, validator, name)
{
	SetThemeColour();
}

void YANGTreeCtrl::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetForegroundColour(*wxGREEN);
		SetBackgroundColour(*wxBLACK);
	}
	//else
	//  SetDefaultThemeColour(this);
}

YANGStaticLine::YANGStaticLine(): wxStaticLine()
{
	SetThemeColour();
}

YANGStaticLine::YANGStaticLine(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name):
wxStaticLine(parent, id, pos, size, style, name)
{
	SetThemeColour();
}

void YANGStaticLine::SetThemeColour()
{
	if (g_configuration && g_configuration->theme_is_dark)
	{
		SetBackgroundColour(wxColour(127, 127, 127));
	}
	//else
	//  SetDefaultThemeColour(this);
}
