/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_LOOKANDFEELDIALOG_H_
#define _YANG_LOOKANDFEELDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/colordlg.h>
#include <wx/cmndata.h>
#endif

#include "theme.h"

///////////////////////////////////////////////////////////////////////////

#define ID_LOOKANDFEELUSECUSTOMFONT 1000
#define ID_LOOKANDFEELUSECUSTOMCOLORS 1001
#define ID_LOOKANDFEELFONTCHOICE 1002
#define ID_LOOKANDFEELFONTSIZECOMBOBOX 1003
#define ID_LOOKANDFEELTEXTCOLORCHOICE 1004
#define ID_LOOKANDFEELBACKGROUNDCOLORCHOICE 1005

class LookAndFeelDialog : public YANGDialog 
{
public:
	LookAndFeelDialog(wxWindow* parent, bool isHost);
	//~LookAndFeelDialog();
	void ShowDialog();
	void OnUseCustomStyleAnyCheck(wxCommandEvent& event);
	void OnFontChoice(wxCommandEvent& event);
	void OnFontSizeUpdate(wxCommandEvent& event);
	void OnTextColorSet(wxHyperlinkEvent& event);
	void OnBackgroundColorSet(wxHyperlinkEvent& event);
	void OnOk(wxCommandEvent& event);
	//void OnCancel(wxCommandEvent& event);

private:
	wxWindow* m_parent;
	bool m_is_host;

	YANGPanel* m_LookAndFeelpanel;
	//YANGCheckBox* m_darkthemecheckBox;
	YANGCheckBox* m_chatfontlayoutcheckBox;
	YANGCheckBox* m_chatcolorlayoutcheckBox;
	wxStaticText* m_fontstaticText;
	YANGChoice* m_fontchoice;
	wxStaticText* m_fontsizestaticText;
	YANGComboBox* m_fontsizecomboBox;
	YANGHyperlinkCtrl* m_textcolorhyperlink;
	YANGHyperlinkCtrl* m_backcolorhyperlink;
	wxStaticText* m_chatsamplestaticText;
	YANGTextCtrl* m_chatsampletextCtrl;
	wxStdDialogButtonSizer* m_LookAndFeelsdbSizer;
	YANGButton* m_LookAndFeelsdbSizerOK;
	YANGButton* m_LookAndFeelsdbSizerCancel;

	void RefreshSampleText();

	wxFont m_font, m_def_font;
	wxColour m_textcolor, m_backcolor, m_def_textcolor, m_def_backcolor;
	wxColourData m_colordata;

	DECLARE_EVENT_TABLE()
};

#endif //__wxfbguis__

