/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANGMASTER_ROOMSTABLE_H_
#define _YANGMASTER_ROOMSTABLE_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/hashmap.h>
#include <wx/socket.h>
#endif

#include "config.h"
#include "common.h"

typedef struct
{
	wxSocketBase* sock;
	wxString address, localip;
	wxString netver;
	long port_num;
	bool password_protected;
	GameType game;
	wxString roomname; // String length should be self-limited in networking code.
	unsigned long /*num_players, */max_num_players, skill;
	bool in_game;
	SourcePortType srcport;
	BloodMPGameT_Type bloodgametype;
	DescentMPGameT_Type descentgametype;
	Descent2MPGameT_Type descent2gametype;
	DN3DMPGameT_Type dn3dgametype;
	SWMPGameT_Type swgametype;
	//DN3DSpawnType dn3dspawn;
	bool launch_usermap;
	wxString usermap; // Same.
	unsigned long epimap_num;
	wxString modname; // Same.
	//bool usemasterslave;
	bool use_crypticpassage;
} RoomInfo;

typedef struct
{
	RoomInfo info;
	wxArrayString players; // Same.
} MasterRoom;

WX_DECLARE_STRING_HASH_MAP( long, IPStrToNumOfRoomsHash );

class MasterRoomsTable
{
public:
	MasterRoomsTable(int max_rooms, int max_rooms_per_ip);
	~MasterRoomsTable();
	int Count(const wxString& address);
	bool IsFull();
	bool TryAdd(MasterRoom& room);
	void UpdateInfo(RoomInfo& room_info);
	void UpdatePlayers(wxSocketBase* sock, const wxArrayString& players);
	void UpdateIps(wxSocketBase* sock, const wxArrayString& ips);
	//void AddNickname(wxSocketBase* sock, const wxString& nickname);
	//void RemNickname(wxSocketBase* sock, const wxString& nickname);
	bool TryGet(int index, MasterRoom& room);
	void Delete(wxSocketBase* sock);
	size_t GetCountByPort(SourcePortType srcport);
	MasterRoom* m_rooms;
	IPStrToNumOfRoomsHash *m_iptorooms_hash;
	int m_num_of_rooms, m_max_num_of_rooms, m_max_num_of_rooms_per_ip;
	size_t m_room_counts_by_port[SOURCEPORT_NUMOFPORTS];
	wxCriticalSection m_critical_section;
};

#endif
