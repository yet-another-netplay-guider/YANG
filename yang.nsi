; yang.nsi
;
; This script is based on example2.nsi
;
;--------------------------------

; The name of the installer
Name "YANG (Yet Another Netplay Guider)"

; The file to write
OutFile "setup.exe"

; The default installation directory
InstallDir $PROGRAMFILES\YANG

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\YANG" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "YANG (required)"

  SectionIn RO
  
  ; Set output paths and put files.
  
  SetOutPath "$INSTDIR"
  File "yang.exe"
  File "readme.txt"
; File "gpl-2.0.txt"
  File "license.txt"
  File "GeoIP.dat"
  File "GeoIP.dll"
  File "open-data-license.txt"
; File "mingwm10.dll"
  File "deflists.cfg"
  SetOutPath "$INSTDIR\snddata"
  File "snddata\join.wav"
  File "snddata\leave.wav"
  File "snddata\send.wav"
  File "snddata\receive.wav"
  File "snddata\error.wav"
  SetOutPath "$INSTDIR"
  
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\YANG "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\YANG" "DisplayName" "YANG (Yet Another Netplay Guider)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\YANG" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\YANG" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\YANG" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  SetShellVarContext all

  CreateDirectory "$SMPROGRAMS\YANG (Yet Another Netplay Guider)"
  CreateShortCut "$SMPROGRAMS\YANG (Yet Another Netplay Guider)\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\YANG (Yet Another Netplay Guider)\YANG.lnk" "$INSTDIR\yang.exe" "" "$INSTDIR\yang.exe" 0
  CreateShortCut "$SMPROGRAMS\YANG (Yet Another Netplay Guider)\Readme.lnk" "$INSTDIR\readme.txt" "" "$INSTDIR\readme.txt" 0
; CreateShortCut "$SMPROGRAMS\YANG (Yet Another Netplay Guider)\GNU General Public License v2.lnk" "$INSTDIR\gpl-2.0.txt" "" "$INSTDIR\gpl-2.0.txt" 0
  Delete "$SMPROGRAMS\YANG (Yet Another Netplay Guider)\GNU General Public License v2.lnk"
  CreateShortCut "$SMPROGRAMS\YANG (Yet Another Netplay Guider)\License.lnk" "$INSTDIR\license.txt" "" "$INSTDIR\license.txt" 0
  CreateShortCut "$SMPROGRAMS\YANG (Yet Another Netplay Guider)\open-data-license.lnk" "$INSTDIR\open-data-license.txt" "" "$INSTDIR\open-data-license.txt" 0
  
SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\YANG"
  DeleteRegKey HKLM SOFTWARE\YANG

  ; Remove files and uninstaller
  Delete $INSTDIR\yang.exe
  Delete $INSTDIR\readme.txt
; Delete $INSTDIR\gpl-2.0.txt
  Delete $INSTDIR\license.txt
  Delete $INSTDIR\deflists.cfg
  Delete $INSTDIR\GeoIP.dat
  Delete $INSTDIR\GeoIP.dll
  Delete $INSTDIR\open-data-license.txt
; Delete $INSTDIR\mingwm10.dll
  Delete $INSTDIR\snddata\join.wav
  Delete $INSTDIR\snddata\leave.wav
  Delete $INSTDIR\snddata\send.wav
  Delete $INSTDIR\snddata\receive.wav
  Delete $INSTDIR\snddata\error.wav
  Delete $INSTDIR\uninstall.exe

  SetShellVarContext all

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\YANG (Yet Another Netplay Guider)\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\YANG (Yet Another Netplay Guider)"
  RMDir "$INSTDIR"

SectionEnd
