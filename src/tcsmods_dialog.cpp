/**************************************************************************

Copyright 2011-2023 - NY00123
Copyright 2011-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/filedlg.h>
#include <wx/filename.h>
#include <wx/gbsizer.h>
#include <wx/msgdlg.h>
#include <wx/bmpbuttn.h>
#include <wx/mstream.h>
#include <wx/image.h>
#endif

#include "tcsmods_dialog.h"
#include "yang.h"
#include "theme.h"
#include "yang.xpm"
#include "yang_images.h"

BEGIN_EVENT_TABLE(TCsMODsDialog, YANGDialog)

EVT_BUTTON(wxID_OK, TCsMODsDialog::OnOk)
EVT_BUTTON(wxID_CANCEL, TCsMODsDialog::OnCancel)

EVT_BUTTON(ID_ADDNEWDN3DMOD, TCsMODsDialog::OnAddNewProfile)
EVT_BUTTON(ID_ADDNEWSWMOD, TCsMODsDialog::OnAddNewProfile)

EVT_BUTTON(ID_DELETEDN3DMOD, TCsMODsDialog::OnDeleteProfile)
EVT_BUTTON(ID_DELETESWMOD, TCsMODsDialog::OnDeleteProfile)

EVT_BUTTON(ID_ADDDN3DMODFILE, TCsMODsDialog::OnAddModFile)
EVT_BUTTON(ID_ADDSWMODFILE, TCsMODsDialog::OnAddModFile)

EVT_BUTTON(ID_REMDN3DMODFILE, TCsMODsDialog::OnRemModFile)
EVT_BUTTON(ID_REMSWMODFILE, TCsMODsDialog::OnRemModFile)

EVT_BUTTON(ID_SETDN3DCONFILE, TCsMODsDialog::OnSetConFile)

EVT_BUTTON(ID_SETDN3DDEFFILE, TCsMODsDialog::OnSetDefFile)
EVT_BUTTON(ID_SETSWDEFFILE, TCsMODsDialog::OnSetDefFile)

EVT_CHECKBOX(ID_USEDN3DCONFILE, TCsMODsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_USEDN3DDEFFILE, TCsMODsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_USEDN3DMODURL, TCsMODsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_USESWDEFFILE, TCsMODsDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_USESWMODURL, TCsMODsDialog::OnCheckBoxClick)

EVT_TEXT(ID_SELECTDN3DMOD, TCsMODsDialog::OnUpdateTextInput)
EVT_TEXT(ID_DN3DCONFILETEXT, TCsMODsDialog::OnUpdateTextInput)
EVT_TEXT(ID_DN3DDEFFILETEXT, TCsMODsDialog::OnUpdateTextInput)
EVT_TEXT(ID_DN3DMODURLTEXT, TCsMODsDialog::OnUpdateTextInput)
EVT_TEXT(ID_SELECTSWMOD, TCsMODsDialog::OnUpdateTextInput)
EVT_TEXT(ID_SWDEFFILETEXT, TCsMODsDialog::OnUpdateTextInput)
EVT_TEXT(ID_SWMODURLTEXT, TCsMODsDialog::OnUpdateTextInput)

END_EVENT_TABLE()

TCsMODsDialog::TCsMODsDialog() : YANGDialog(NULL, wxID_ANY, wxT("TCs and MODs"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* TCsMODsDialogbSizer = new wxBoxSizer( wxVERTICAL );

	m_tcsmodsnotebook = new YANGNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );

	m_tcsmodsdn3dpanel = new YANGPanel( m_tcsmodsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* tcsmodsdn3dpanelbSizer = new wxBoxSizer( wxVERTICAL );

	wxGridBagSizer* tcsmodsdn3dgbSizer = new wxGridBagSizer( 0, 0 );
	tcsmodsdn3dgbSizer->SetFlexibleDirection( wxBOTH );
	tcsmodsdn3dgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_tcsmodsdn3dprofilestaticText = new wxStaticText( m_tcsmodsdn3dpanel, wxID_ANY, wxT("TC/MOD profile:"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3dprofilestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3dprofilechoice = new YANGComboBox( m_tcsmodsdn3dpanel, ID_SELECTDN3DMOD, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(300, -1)) );
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3dprofilechoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	tcsmodsdn3dgbSizer->AddGrowableCol( 1 );
	
	wxBoxSizer* tcsmodsdn3dmodnamebuttonbSizer = new wxBoxSizer( wxHORIZONTAL );

	wxMemoryInputStream istreamdn3d_add(add_png, sizeof add_png);

	m_tcsmodsdn3daddnewbutton = new YANGBitmapButton( m_tcsmodsdn3dpanel, ID_ADDNEWDN3DMOD, wxBitmap(wxImage(istreamdn3d_add, wxBITMAP_TYPE_PNG)), wxDefaultPosition, wxDLG_UNIT(this, wxSize(14, 14)) );
	tcsmodsdn3dmodnamebuttonbSizer->Add( m_tcsmodsdn3daddnewbutton, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	wxMemoryInputStream istreamdn3d_cross(cross_png, sizeof cross_png);

	m_tcsmodsdn3ddeletebutton = new YANGBitmapButton( m_tcsmodsdn3dpanel, ID_DELETEDN3DMOD, wxBitmap(wxImage(istreamdn3d_cross, wxBITMAP_TYPE_PNG)), wxDefaultPosition, wxDLG_UNIT(this, wxSize(14, 14)) );
	tcsmodsdn3dmodnamebuttonbSizer->Add( m_tcsmodsdn3ddeletebutton, 0, wxALIGN_CENTER_VERTICAL, 0 );

	tcsmodsdn3dgbSizer->Add( tcsmodsdn3dmodnamebuttonbSizer, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3dstaticline = new wxStaticLine( m_tcsmodsdn3dpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3dstaticline, wxGBPosition( 1, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3dmodfilesstaticText = new wxStaticText( m_tcsmodsdn3dpanel, wxID_ANY, wxT("TC/MOD files:"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3dmodfilesstaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3dmodfileslistBox = new YANGListBox( m_tcsmodsdn3dpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(-1, 181)), 0, NULL, wxLB_EXTENDED|wxLB_SORT ); 
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3dmodfileslistBox, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	wxBoxSizer* tcsmodsdn3dmodfilebuttonbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_tcsmodsdn3daddmodfilebutton = new YANGButton( m_tcsmodsdn3dpanel, ID_ADDDN3DMODFILE, wxT("Add file(s)"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsdn3dmodfilebuttonbSizer->Add( m_tcsmodsdn3daddmodfilebutton, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3dremmodfilebutton = new YANGButton( m_tcsmodsdn3dpanel, ID_REMDN3DMODFILE, wxT("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsdn3dmodfilebuttonbSizer->Add( m_tcsmodsdn3dremmodfilebutton, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3dconfilebutton = new YANGButton( m_tcsmodsdn3dpanel, ID_SETDN3DCONFILE, wxT("Set CON file"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsdn3dmodfilebuttonbSizer->Add( m_tcsmodsdn3dconfilebutton, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3ddeffilebutton = new YANGButton( m_tcsmodsdn3dpanel, ID_SETDN3DDEFFILE, wxT("Set DEF file"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsdn3dmodfilebuttonbSizer->Add( m_tcsmodsdn3ddeffilebutton, 0, wxALIGN_CENTER_VERTICAL, 0 );

	tcsmodsdn3dgbSizer->Add( tcsmodsdn3dmodfilebuttonbSizer, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3dconfilecheckBox = new YANGCheckBox( m_tcsmodsdn3dpanel, ID_USEDN3DCONFILE, wxT("Main CON file:"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3dconfilecheckBox, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3dconfiletextCtrl = new YANGTextCtrl( m_tcsmodsdn3dpanel, ID_DN3DCONFILETEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, -1)) );
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3dconfiletextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3ddeffilecheckBox = new YANGCheckBox( m_tcsmodsdn3dpanel, ID_USEDN3DDEFFILE, wxT("Main DEF file:"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3ddeffilecheckBox, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3ddeffiletextCtrl = new YANGTextCtrl( m_tcsmodsdn3dpanel, ID_DN3DDEFFILETEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, -1)) );
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3ddeffiletextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3dmodurlcheckBox = new YANGCheckBox( m_tcsmodsdn3dpanel, ID_USEDN3DMODURL, wxT("TC/MOD URL:"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3dmodurlcheckBox, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsdn3dmodurltextCtrl = new YANGTextCtrl( m_tcsmodsdn3dpanel, ID_DN3DMODURLTEXT, wxEmptyString, wxDefaultPosition, wxDefaultSize );
	tcsmodsdn3dgbSizer->Add( m_tcsmodsdn3dmodurltextCtrl, wxGBPosition( 6, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	tcsmodsdn3dpanelbSizer->Add( tcsmodsdn3dgbSizer, 1, wxEXPAND, 0 );

	m_tcsmodsdn3dpanel->SetSizer( tcsmodsdn3dpanelbSizer );
	m_tcsmodsdn3dpanel->Layout();
	tcsmodsdn3dpanelbSizer->Fit( m_tcsmodsdn3dpanel );

	m_tcsmodsnotebook->AddPage( m_tcsmodsdn3dpanel, GAMENAME_DN3D, true );


	m_tcsmodsswpanel = new YANGPanel( m_tcsmodsnotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* tcsmodsswpanelbSizer = new wxBoxSizer( wxVERTICAL );

	wxGridBagSizer* tcsmodsswgbSizer = new wxGridBagSizer( 0, 0 );
	tcsmodsswgbSizer->SetFlexibleDirection( wxBOTH );
	tcsmodsswgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_tcsmodsswprofilestaticText = new wxStaticText( m_tcsmodsswpanel, wxID_ANY, wxT("TC/MOD profile:"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsswgbSizer->Add( m_tcsmodsswprofilestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsswprofilechoice = new YANGComboBox( m_tcsmodsswpanel, ID_SELECTSWMOD, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(300, -1)) );
	tcsmodsswgbSizer->Add( m_tcsmodsswprofilechoice, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	tcsmodsswgbSizer->AddGrowableCol( 1 );

	wxBoxSizer* tcsmodsswmodnamebuttonbSizer = new wxBoxSizer( wxHORIZONTAL );

	wxMemoryInputStream istreamsw_add(add_png, sizeof add_png);

	m_tcsmodsswaddnewbutton = new YANGBitmapButton( m_tcsmodsswpanel, ID_ADDNEWSWMOD, wxBitmap(wxImage(istreamsw_add, wxBITMAP_TYPE_PNG)), wxDefaultPosition, wxDLG_UNIT(this, wxSize(14, 14)) );
	tcsmodsswmodnamebuttonbSizer->Add( m_tcsmodsswaddnewbutton, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	wxMemoryInputStream istreamsw_cross(cross_png, sizeof cross_png);

	m_tcsmodsswdeletebutton = new YANGBitmapButton( m_tcsmodsswpanel, ID_DELETESWMOD, wxBitmap(wxImage(istreamsw_cross, wxBITMAP_TYPE_PNG)), wxDefaultPosition, wxDLG_UNIT(this, wxSize(14, 14)) );
	tcsmodsswmodnamebuttonbSizer->Add( m_tcsmodsswdeletebutton, 0, wxALIGN_CENTER_VERTICAL, 0 );

	tcsmodsswgbSizer->Add( tcsmodsswmodnamebuttonbSizer, wxGBPosition( 0, 2 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsswstaticline = new wxStaticLine( m_tcsmodsswpanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	tcsmodsswgbSizer->Add( m_tcsmodsswstaticline, wxGBPosition( 1, 0 ), wxGBSpan( 1, 3 ), wxEXPAND|wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsswmodfilesstaticText = new wxStaticText( m_tcsmodsswpanel, wxID_ANY, wxT("TC/MOD files:"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsswgbSizer->Add( m_tcsmodsswmodfilesstaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsswmodfileslistBox = new YANGListBox( m_tcsmodsswpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(-1, 200)), 0, NULL, wxLB_EXTENDED|wxLB_SORT ); 
	tcsmodsswgbSizer->Add( m_tcsmodsswmodfileslistBox, wxGBPosition( 2, 1 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	wxBoxSizer* tcsmodsswmodfilebuttonbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_tcsmodsswaddmodfilebutton = new YANGButton( m_tcsmodsswpanel, ID_ADDSWMODFILE, wxT("Add file(s)"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsswmodfilebuttonbSizer->Add( m_tcsmodsswaddmodfilebutton, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsswremmodfilebutton = new YANGButton( m_tcsmodsswpanel, ID_REMSWMODFILE, wxT("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsswmodfilebuttonbSizer->Add( m_tcsmodsswremmodfilebutton, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsswdeffilebutton = new YANGButton( m_tcsmodsswpanel, ID_SETSWDEFFILE, wxT("Set DEF file"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsswmodfilebuttonbSizer->Add( m_tcsmodsswdeffilebutton, 0, wxALIGN_CENTER_VERTICAL, 0 );

	tcsmodsswgbSizer->Add( tcsmodsswmodfilebuttonbSizer, wxGBPosition( 3, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsswdeffilecheckBox = new YANGCheckBox( m_tcsmodsswpanel, ID_USESWDEFFILE, wxT("Main DEF file:"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsswgbSizer->Add( m_tcsmodsswdeffilecheckBox, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsswdeffiletextCtrl = new YANGTextCtrl( m_tcsmodsswpanel, ID_SWDEFFILETEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, -1)) );
	tcsmodsswgbSizer->Add( m_tcsmodsswdeffiletextCtrl, wxGBPosition( 4, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsswmodurlcheckBox = new YANGCheckBox( m_tcsmodsswpanel, ID_USESWMODURL, wxT("TC/MOD URL:"), wxDefaultPosition, wxDefaultSize, 0 );
	tcsmodsswgbSizer->Add( m_tcsmodsswmodurlcheckBox, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_tcsmodsswmodurltextCtrl = new YANGTextCtrl( m_tcsmodsswpanel, ID_SWMODURLTEXT, wxEmptyString, wxDefaultPosition, wxDefaultSize );
	tcsmodsswgbSizer->Add( m_tcsmodsswmodurltextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	tcsmodsswpanelbSizer->Add( tcsmodsswgbSizer, 1, wxEXPAND, 0 );

	m_tcsmodsswpanel->SetSizer( tcsmodsswpanelbSizer );
	m_tcsmodsswpanel->Layout();
	tcsmodsswpanelbSizer->Fit( m_tcsmodsswpanel );

	m_tcsmodsnotebook->AddPage( m_tcsmodsswpanel, GAMENAME_SW, false );


	TCsMODsDialogbSizer->Add( m_tcsmodsnotebook, 1, wxEXPAND | wxALL, 5 );

	m_tcsmodssdbSizer = new wxStdDialogButtonSizer();
	m_tcsmodssdbSizerOK = new YANGButton( this, wxID_OK );
	m_tcsmodssdbSizer->AddButton( m_tcsmodssdbSizerOK );
	m_tcsmodssdbSizerCancel = new YANGButton( this, wxID_CANCEL );
	m_tcsmodssdbSizer->AddButton( m_tcsmodssdbSizerCancel );
	m_tcsmodssdbSizer->Realize();
	TCsMODsDialogbSizer->Add( m_tcsmodssdbSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxBOTTOM, 5 );

//	m_tcsmodsnotebook->Layout();
	SetSizer( TCsMODsDialogbSizer );
	Layout();
	TCsMODsDialogbSizer->Fit( this );

	Centre();
	SetIcon(wxIcon(yang_xpm));


	if (g_configuration->dn3dtcmod_profile_list)
	{
		g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

		dn3dtcmod_profile_list_temp = new dn3dtcmod_profile_t;
		*dn3dtcmod_profile_list_temp = *g_configuration->dn3dtcmod_profile_current;

		while (g_configuration->dn3dtcmod_profile_current->next)
		{
			g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;

			dn3dtcmod_profile_list_temp->next = new dn3dtcmod_profile_t;
			*dn3dtcmod_profile_list_temp->next = *g_configuration->dn3dtcmod_profile_current;
			dn3dtcmod_profile_list_temp->next->previous = dn3dtcmod_profile_list_temp;
			dn3dtcmod_profile_list_temp = dn3dtcmod_profile_list_temp->next;
		}

		while (dn3dtcmod_profile_list_temp->previous)
			dn3dtcmod_profile_list_temp = dn3dtcmod_profile_list_temp->previous;


		dn3dtcmod_profile_current_temp = dn3dtcmod_profile_list_temp;

		while (dn3dtcmod_profile_current_temp)
		{
			m_tcsmodsdn3dprofilechoice->Append(dn3dtcmod_profile_current_temp->profilename);

			dn3dtcmod_profile_current_temp = dn3dtcmod_profile_current_temp->next;
		}

		m_tcsmodsdn3dprofilechoice->Select(0);

		dn3dtcmod_profile_index = 0;

		dn3dtcmod_profile_current_temp = dn3dtcmod_profile_list_temp;

		m_tcsmodsdn3dmodfileslistBox->Set(dn3dtcmod_profile_current_temp->modfiles);

		m_tcsmodsdn3dremmodfilebutton->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty());
		m_tcsmodsdn3dconfilebutton->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->confile.IsEmpty());
		m_tcsmodsdn3ddeffilebutton->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->deffile.IsEmpty());

		m_tcsmodsdn3dconfilecheckBox->SetValue(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->confile.IsEmpty());
		m_tcsmodsdn3dconfilecheckBox->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty());
		m_tcsmodsdn3dconfiletextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->confile);
		m_tcsmodsdn3dconfiletextCtrl->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->confile.IsEmpty());

		m_tcsmodsdn3ddeffilecheckBox->SetValue(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->deffile.IsEmpty());
		m_tcsmodsdn3ddeffilecheckBox->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty());
		m_tcsmodsdn3ddeffiletextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->deffile);
		m_tcsmodsdn3ddeffiletextCtrl->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->deffile.IsEmpty());

		m_tcsmodsdn3dmodurlcheckBox->SetValue(dn3dtcmod_profile_current_temp->usemodurl);
		m_tcsmodsdn3dmodurltextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->modurl);
		m_tcsmodsdn3dmodurltextCtrl->Enable(dn3dtcmod_profile_current_temp->usemodurl);
	}
	else
	{
		dn3dtcmod_profile_list_temp = NULL;

		m_tcsmodsdn3dprofilechoice->Disable();
		m_tcsmodsdn3ddeletebutton->Disable();
		m_tcsmodsdn3dmodfilesstaticText->Disable();
		m_tcsmodsdn3dmodfileslistBox->Disable();
		m_tcsmodsdn3daddmodfilebutton->Disable();
		m_tcsmodsdn3dremmodfilebutton->Disable();
		m_tcsmodsdn3dconfilebutton->Disable();
		m_tcsmodsdn3ddeffilebutton->Disable();
		m_tcsmodsdn3dconfilecheckBox->Disable();
		m_tcsmodsdn3dconfiletextCtrl->Disable();
		m_tcsmodsdn3ddeffilecheckBox->Disable();
		m_tcsmodsdn3ddeffiletextCtrl->Disable();
		m_tcsmodsdn3dmodurlcheckBox->Disable();
		m_tcsmodsdn3dmodurltextCtrl->Disable();
	}


	if (g_configuration->swtcmod_profile_list)
	{
		g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

		swtcmod_profile_list_temp = new swtcmod_profile_t;
		*swtcmod_profile_list_temp = *g_configuration->swtcmod_profile_current;

		while (g_configuration->swtcmod_profile_current->next)
		{
			g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;

			swtcmod_profile_list_temp->next = new swtcmod_profile_t;
			*swtcmod_profile_list_temp->next = *g_configuration->swtcmod_profile_current;
			swtcmod_profile_list_temp->next->previous = swtcmod_profile_list_temp;
			swtcmod_profile_list_temp = swtcmod_profile_list_temp->next;
		}

		while (swtcmod_profile_list_temp->previous)
			swtcmod_profile_list_temp = swtcmod_profile_list_temp->previous;


		swtcmod_profile_current_temp = swtcmod_profile_list_temp;

		while (swtcmod_profile_current_temp)
		{
			m_tcsmodsswprofilechoice->Append(swtcmod_profile_current_temp->profilename);

			swtcmod_profile_current_temp = swtcmod_profile_current_temp->next;
		}

		m_tcsmodsswprofilechoice->Select(0);

		swtcmod_profile_index = 0;

		swtcmod_profile_current_temp = swtcmod_profile_list_temp;

		m_tcsmodsswmodfileslistBox->Set(swtcmod_profile_current_temp->modfiles);

		m_tcsmodsswremmodfilebutton->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty());
		m_tcsmodsswdeffilebutton->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty() && !swtcmod_profile_current_temp->deffile.IsEmpty());

		m_tcsmodsswdeffilecheckBox->SetValue(!swtcmod_profile_current_temp->modfiles.IsEmpty() && !swtcmod_profile_current_temp->deffile.IsEmpty());
		m_tcsmodsswdeffilecheckBox->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty());
		m_tcsmodsswdeffiletextCtrl->ChangeValue(swtcmod_profile_current_temp->deffile);
		m_tcsmodsswdeffiletextCtrl->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty() && !swtcmod_profile_current_temp->deffile.IsEmpty());

		m_tcsmodsswmodurlcheckBox->SetValue(swtcmod_profile_current_temp->usemodurl);
		m_tcsmodsswmodurltextCtrl->ChangeValue(swtcmod_profile_current_temp->modurl);
		m_tcsmodsswmodurltextCtrl->Enable(swtcmod_profile_current_temp->usemodurl);
	}
	else
	{
		swtcmod_profile_list_temp = NULL;

		m_tcsmodsswprofilechoice->Disable();
		m_tcsmodsswdeletebutton->Disable();
		m_tcsmodsswmodfilesstaticText->Disable();
		m_tcsmodsswmodfileslistBox->Disable();
		m_tcsmodsswaddmodfilebutton->Disable();
		m_tcsmodsswremmodfilebutton->Disable();
		m_tcsmodsswdeffilebutton->Disable();
		m_tcsmodsswdeffilecheckBox->Disable();
		m_tcsmodsswdeffiletextCtrl->Disable();
		m_tcsmodsswmodurlcheckBox->Disable();
		m_tcsmodsswmodurltextCtrl->Disable();
	}
}

TCsMODsDialog::~TCsMODsDialog()
{
	if (!g_main_frame)
		g_main_frame = new MainFrame(NULL);

	g_main_frame->ShowMainFrame();
}

void TCsMODsDialog::OnAddModFile(wxCommandEvent& event)
{
	wxFileDialog *l_dialog;
	wxArrayString l_fullpaths, l_modfiles;
	size_t l_loop_var = 0, l_loop_var2 = 0, l_num_of_items, l_num_of_files;

	switch (event.GetId())
	{
		case ID_ADDDN3DMODFILE:
			// Duke Nukem 3D files (*.bin;*.tmb)
			l_dialog = new wxFileDialog(NULL, wxT("Select one or multiple files"), !last_directory.IsEmpty() ? last_directory : !dn3dtcmod_profile_current_temp->modfiles.IsEmpty() ? ((wxFileName)dn3dtcmod_profile_current_temp->modfiles[0]).GetPath() : wxString(wxEmptyString), wxEmptyString,
				wxT("Duke Nukem 3D files (*.anm;*.art;*.con;*.dat;*.def;*.dmo;*.grp;*.pk3;*.map;*.mid;*.mp3;*.ogg;*.voc;*.wav;*.zip)|*.anm;*.art;*.con;*.dat;*.def;*.dmo;*.grp;*.pk3;*.map;*.mid;*.mp3;*.ogg;*.voc;*.wav;*.zip|All files|*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST|wxFD_MULTIPLE);

			if (l_dialog->ShowModal() == wxID_OK)
			{
				l_dialog->GetPaths(l_fullpaths);
				l_num_of_items = l_fullpaths.GetCount();

				l_modfiles = m_tcsmodsdn3dmodfileslistBox->GetStrings();
				l_num_of_files = l_modfiles.GetCount();

				while (l_loop_var < l_num_of_files && !((wxFileName)l_modfiles[l_loop_var]).GetFullName().IsSameAs(((wxFileName)l_fullpaths[l_loop_var2]).GetFullName(), false))
				{
					if (l_loop_var2 < l_num_of_items-1)
						l_loop_var2++;
					else
					{
						l_loop_var++;
						l_loop_var2 = 0;
					}
				}

				if (l_loop_var == l_num_of_files)
				{
					m_tcsmodsdn3dmodfileslistBox->Append(l_fullpaths);
					dn3dtcmod_profile_current_temp->modfiles = m_tcsmodsdn3dmodfileslistBox->GetStrings();

					last_directory = ((wxFileName)l_fullpaths[0]).GetPath();

					if (!dn3dtcmod_profile_current_temp->modfiles.IsEmpty())
					{
						m_tcsmodsdn3dremmodfilebutton->Enable();
						m_tcsmodsdn3dconfilecheckBox->Enable();
						m_tcsmodsdn3ddeffilecheckBox->Enable();
					}
				}
				else
					wxMessageBox(wxT("You already have a TC/MOD file with the \"") + ((wxFileName)l_modfiles[l_loop_var]).GetFullName() + wxT("\" name."), wxT("TC/MOD file(s) with same name"), wxOK|wxICON_EXCLAMATION);
			}

			break;

		case ID_ADDSWMODFILE:
			// Shadow Warrior files (*.bin;*.sym;*.tmb;*.txt)
			l_dialog = new wxFileDialog(NULL, wxT("Select one or multiple files"), !last_directory.IsEmpty() ? last_directory : !swtcmod_profile_current_temp->modfiles.IsEmpty() ? ((wxFileName)swtcmod_profile_current_temp->modfiles[0]).GetPath() : wxString(wxEmptyString), wxEmptyString,
				wxT("Shadow Warrior files (*.anm;*.art;*.dat;*.def;*.grp;*.pk3;*.kvx;*.map;*.mid;*.mp3;*.ogg;*.pal;*.voc;*.wav;*.zip)|*.anm;*.art;*.dat;*.def;*.grp;*.pk3;*.kvx;*.map;*.mid;*.mp3;*.ogg;*.pal;*.voc;*.wav;*.zip|All files|*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST|wxFD_MULTIPLE);

			if (l_dialog->ShowModal() == wxID_OK)
			{
				l_dialog->GetPaths(l_fullpaths);
				l_num_of_items = l_fullpaths.GetCount();

				l_modfiles = m_tcsmodsswmodfileslistBox->GetStrings();
				l_num_of_files = l_modfiles.GetCount();

				while (l_loop_var < l_num_of_files && !((wxFileName)l_modfiles[l_loop_var]).GetFullName().IsSameAs(((wxFileName)l_fullpaths[l_loop_var2]).GetFullName(), false))
				{
					if (l_loop_var2 < l_num_of_items-1)
						l_loop_var2++;
					else
					{
						l_loop_var++;
						l_loop_var2 = 0;
					}
				}

				if (l_loop_var == l_num_of_files)
				{
					m_tcsmodsswmodfileslistBox->Append(l_fullpaths);
					swtcmod_profile_current_temp->modfiles = m_tcsmodsswmodfileslistBox->GetStrings();

					last_directory = ((wxFileName)l_fullpaths[0]).GetPath();

					if (!swtcmod_profile_current_temp->modfiles.IsEmpty())
					{
						m_tcsmodsswremmodfilebutton->Enable();
						m_tcsmodsswdeffilecheckBox->Enable();
					}
				}
				else
					wxMessageBox(wxT("You already have a TC/MOD file with the \"") + ((wxFileName)l_modfiles[l_loop_var]).GetFullName() + wxT("\" name."), wxT("TC/MOD file(s) with same name"), wxOK|wxICON_EXCLAMATION);
			}

			break;
		default: ;
	}
}

void TCsMODsDialog::OnRemModFile(wxCommandEvent& event)
{
	wxArrayInt l_selections;
	size_t l_loop_var, l_num_of_items;

	switch (event.GetId())
	{
		case ID_REMDN3DMODFILE:
			m_tcsmodsdn3dmodfileslistBox->GetSelections(l_selections);

			if (!l_selections.IsEmpty())
			{
				l_num_of_items = l_selections.GetCount();

				for (l_loop_var = l_num_of_items; l_loop_var > 0; l_loop_var--)
					m_tcsmodsdn3dmodfileslistBox->Delete(l_selections[l_loop_var-1]);

				dn3dtcmod_profile_current_temp->modfiles = m_tcsmodsdn3dmodfileslistBox->GetStrings();

				if (dn3dtcmod_profile_current_temp->modfiles.IsEmpty())
				{
					m_tcsmodsdn3dremmodfilebutton->Disable();
					m_tcsmodsdn3dconfilebutton->Disable();
					m_tcsmodsdn3ddeffilebutton->Disable();
					m_tcsmodsdn3dconfilecheckBox->SetValue(false);
					m_tcsmodsdn3dconfilecheckBox->Disable();
					dn3dtcmod_profile_current_temp->confile = wxEmptyString;
					m_tcsmodsdn3dconfiletextCtrl->ChangeValue(wxEmptyString);
					m_tcsmodsdn3dconfiletextCtrl->Disable();
					m_tcsmodsdn3ddeffilecheckBox->SetValue(false);
					m_tcsmodsdn3ddeffilecheckBox->Disable();
					dn3dtcmod_profile_current_temp->deffile = wxEmptyString;
					m_tcsmodsdn3ddeffiletextCtrl->ChangeValue(wxEmptyString);
					m_tcsmodsdn3ddeffiletextCtrl->Disable();
				}
			}
			else
				wxMessageBox(wxT("Please select the file(s) to remove."), wxT("No file is selected"), wxOK|wxICON_EXCLAMATION);

			break;

		case ID_REMSWMODFILE:
			m_tcsmodsswmodfileslistBox->GetSelections(l_selections);

			if (!l_selections.IsEmpty())
			{
				l_num_of_items = l_selections.GetCount();

				for (l_loop_var = l_num_of_items; l_loop_var > 0; l_loop_var--)
					m_tcsmodsswmodfileslistBox->Delete(l_selections[l_loop_var-1]);

				swtcmod_profile_current_temp->modfiles = m_tcsmodsswmodfileslistBox->GetStrings();

				if (swtcmod_profile_current_temp->modfiles.IsEmpty())
				{
					m_tcsmodsswremmodfilebutton->Disable();
					m_tcsmodsswdeffilebutton->Disable();
					m_tcsmodsswdeffilecheckBox->SetValue(false);
					m_tcsmodsswdeffilecheckBox->Disable();
					swtcmod_profile_current_temp->deffile = wxEmptyString;
					m_tcsmodsswdeffiletextCtrl->ChangeValue(wxEmptyString);
					m_tcsmodsswdeffiletextCtrl->Disable();
				}
			}
			else
				wxMessageBox(wxT("Please select the file(s) to remove."), wxT("No file is selected"), wxOK|wxICON_EXCLAMATION);

			break;
		default: ;
	}
}

void TCsMODsDialog::OnSetConFile(wxCommandEvent& WXUNUSED(event))
{
	wxArrayInt l_selections;
	size_t l_num_of_items;

	m_tcsmodsdn3dmodfileslistBox->GetSelections(l_selections);

	l_num_of_items = l_selections.GetCount();

	if (!l_num_of_items)
		wxMessageBox(wxT("Please select a \".con\" file to use as the main Compile file (\"/x\" command line option)."), wxT("No file is selected"), wxOK|wxICON_EXCLAMATION);
	else if (l_num_of_items > 1)
		wxMessageBox(wxT("Please select a \".con\" file to use as the main Compile file (\"/x\" command line option)."), wxT("Multiple files selected"), wxOK|wxICON_EXCLAMATION);
	else if (!m_tcsmodsdn3dmodfileslistBox->GetString(l_selections[0]).Right(4).IsSameAs(wxT(".con"), false))
		wxMessageBox(wxT("Please select a \".con\" file to use as the main Compile file (\"/x\" command line option)."), wxT("Wrong file type selected"), wxOK|wxICON_EXCLAMATION);
	else
	{
		dn3dtcmod_profile_current_temp->confile = ((wxFileName)m_tcsmodsdn3dmodfileslistBox->GetString(l_selections[0])).GetFullName();
		m_tcsmodsdn3dconfiletextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->confile);
	}
}

void TCsMODsDialog::OnSetDefFile(wxCommandEvent& event)
{
	wxArrayInt l_selections;
	size_t l_num_of_items;

	switch (event.GetId())
	{
		case ID_SETDN3DDEFFILE:
			m_tcsmodsdn3dmodfileslistBox->GetSelections(l_selections);

			l_num_of_items = l_selections.GetCount();

			if (!l_num_of_items)
				wxMessageBox(wxT("Please select a \".def\" file to use instead of DUKE3D.DEF (\"/h\" command line option)."), wxT("No file is selected"), wxOK|wxICON_EXCLAMATION);
			else if (l_num_of_items > 1)
				wxMessageBox(wxT("Please select a \".def\" file to use instead of DUKE3D.DEF (\"/h\" command line option)."), wxT("Multiple files selected"), wxOK|wxICON_EXCLAMATION);
			else if (!m_tcsmodsdn3dmodfileslistBox->GetString(l_selections[0]).Right(4).IsSameAs(wxT(".def"), false))
				wxMessageBox(wxT("Please select a \".def\" file to use instead of DUKE3D.DEF (\"/h\" command line option)."), wxT("Wrong file type selected"), wxOK|wxICON_EXCLAMATION);
			else
			{
				dn3dtcmod_profile_current_temp->deffile = ((wxFileName)m_tcsmodsdn3dmodfileslistBox->GetString(l_selections[0])).GetFullName();
				m_tcsmodsdn3ddeffiletextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->deffile);
			}

			break;

		case ID_SETSWDEFFILE:
			m_tcsmodsswmodfileslistBox->GetSelections(l_selections);

			l_num_of_items = l_selections.GetCount();

			if (!l_num_of_items)
				wxMessageBox(wxT("Please select a \".def\" file to use instead of SW.DEF (\"-h\" command line option)."), wxT("No file is selected"), wxOK|wxICON_EXCLAMATION);
			else if (l_num_of_items > 1)
				wxMessageBox(wxT("Please select a \".def\" file to use instead of SW.DEF (\"-h\" command line option)."), wxT("Multiple files selected"), wxOK|wxICON_EXCLAMATION);
			else if (!m_tcsmodsswmodfileslistBox->GetString(l_selections[0]).Right(4).IsSameAs(wxT(".def"), false))
				wxMessageBox(wxT("Please select a \".def\" file to use instead of SW.DEF (\"-h\" command line option)."), wxT("Wrong file type selected"), wxOK|wxICON_EXCLAMATION);
			else
			{
				swtcmod_profile_current_temp->deffile = ((wxFileName)m_tcsmodsswmodfileslistBox->GetString(l_selections[0])).GetFullName();
				m_tcsmodsswdeffiletextCtrl->ChangeValue(swtcmod_profile_current_temp->deffile);
			}

			break;
		default: ;
	}
}

void TCsMODsDialog::OnCheckBoxClick(wxCommandEvent& event)
{
	bool l_selection = event.IsChecked();

	switch (event.GetId())
	{
		case ID_USEDN3DCONFILE:
			m_tcsmodsdn3dconfilebutton->Enable(l_selection);
			m_tcsmodsdn3dconfiletextCtrl->Enable(l_selection);

			if (!l_selection)
			{
				dn3dtcmod_profile_current_temp->confile = wxEmptyString;
				m_tcsmodsdn3dconfiletextCtrl->ChangeValue(wxEmptyString);
			}
			break;
		case ID_USEDN3DDEFFILE:
			m_tcsmodsdn3ddeffilebutton->Enable(l_selection);
			m_tcsmodsdn3ddeffiletextCtrl->Enable(l_selection);

			if (!l_selection)
			{
				dn3dtcmod_profile_current_temp->deffile = wxEmptyString;
				m_tcsmodsdn3ddeffiletextCtrl->ChangeValue(wxEmptyString);
			}
			break;
		case ID_USEDN3DMODURL:
			dn3dtcmod_profile_current_temp->usemodurl = l_selection;
			m_tcsmodsdn3dmodurltextCtrl->Enable(l_selection);

			if (!l_selection)
			{
				dn3dtcmod_profile_current_temp->modurl = wxEmptyString;
				m_tcsmodsdn3dmodurltextCtrl->ChangeValue(wxEmptyString);
			}
			break;
		case ID_USESWDEFFILE:
			m_tcsmodsswdeffilebutton->Enable(l_selection);
			m_tcsmodsswdeffiletextCtrl->Enable(l_selection);

			if (!l_selection)
			{
				swtcmod_profile_current_temp->deffile = wxEmptyString;
				m_tcsmodsswdeffiletextCtrl->ChangeValue(wxEmptyString);
			}
			break;
		case ID_USESWMODURL:
			swtcmod_profile_current_temp->usemodurl = l_selection;
			m_tcsmodsswmodurltextCtrl->Enable(l_selection);

			if (!l_selection)
			{
				swtcmod_profile_current_temp->modurl = wxEmptyString;
				m_tcsmodsswmodurltextCtrl->ChangeValue(wxEmptyString);
			}
			break;
		default: ;
	}
}

void TCsMODsDialog::SelectModProfile(GameType game)
{
	int l_loop_var, dn3dtcmod_profile_index_temp, swtcmod_profile_index_temp;


	switch (game)
	{
		case GAME_DN3D:
			dn3dtcmod_profile_index_temp = m_tcsmodsdn3dprofilechoice->GetSelection();

			if (dn3dtcmod_profile_index_temp != wxNOT_FOUND)
			{
				dn3dtcmod_profile_index = dn3dtcmod_profile_index_temp;

				dn3dtcmod_profile_current_temp = dn3dtcmod_profile_list_temp;

				for (l_loop_var = 0; l_loop_var < dn3dtcmod_profile_index; l_loop_var++)
					dn3dtcmod_profile_current_temp = dn3dtcmod_profile_current_temp->next;

				m_tcsmodsdn3dmodfileslistBox->Set(dn3dtcmod_profile_current_temp->modfiles);

				m_tcsmodsdn3dremmodfilebutton->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty());
				m_tcsmodsdn3dconfilebutton->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->confile.IsEmpty());
				m_tcsmodsdn3ddeffilebutton->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->deffile.IsEmpty());

				m_tcsmodsdn3dconfilecheckBox->SetValue(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->confile.IsEmpty());
				m_tcsmodsdn3dconfilecheckBox->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty());
				m_tcsmodsdn3dconfiletextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->confile);
				m_tcsmodsdn3dconfiletextCtrl->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->confile.IsEmpty());

				m_tcsmodsdn3ddeffilecheckBox->SetValue(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->deffile.IsEmpty());
				m_tcsmodsdn3ddeffilecheckBox->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty());
				m_tcsmodsdn3ddeffiletextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->deffile);
				m_tcsmodsdn3ddeffiletextCtrl->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->deffile.IsEmpty());

				m_tcsmodsdn3dmodurlcheckBox->SetValue(dn3dtcmod_profile_current_temp->usemodurl);
				m_tcsmodsdn3dmodurltextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->modurl);
				m_tcsmodsdn3dmodurltextCtrl->Enable(dn3dtcmod_profile_current_temp->usemodurl);
			}

			break;

		case GAME_SW:
			swtcmod_profile_index_temp = m_tcsmodsswprofilechoice->GetSelection();

			if (swtcmod_profile_index_temp != wxNOT_FOUND)
			{
				swtcmod_profile_index = swtcmod_profile_index_temp;

				swtcmod_profile_current_temp = swtcmod_profile_list_temp;

				for (l_loop_var = 0; l_loop_var < swtcmod_profile_index; l_loop_var++)
					swtcmod_profile_current_temp = swtcmod_profile_current_temp->next;

				m_tcsmodsswmodfileslistBox->Set(swtcmod_profile_current_temp->modfiles);

				m_tcsmodsswremmodfilebutton->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty());
				m_tcsmodsswdeffilebutton->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty() && !swtcmod_profile_current_temp->deffile.IsEmpty());

				m_tcsmodsswdeffilecheckBox->SetValue(!swtcmod_profile_current_temp->modfiles.IsEmpty() && !swtcmod_profile_current_temp->deffile.IsEmpty());
				m_tcsmodsswdeffilecheckBox->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty());
				m_tcsmodsswdeffiletextCtrl->ChangeValue(swtcmod_profile_current_temp->deffile);
				m_tcsmodsswdeffiletextCtrl->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty() && !swtcmod_profile_current_temp->deffile.IsEmpty());

				m_tcsmodsswmodurlcheckBox->SetValue(swtcmod_profile_current_temp->usemodurl);
				m_tcsmodsswmodurltextCtrl->ChangeValue(swtcmod_profile_current_temp->modurl);
				m_tcsmodsswmodurltextCtrl->Enable(swtcmod_profile_current_temp->usemodurl);
			}

			break;
		default: ;
	}
}

void TCsMODsDialog::OnUpdateTextInput(wxCommandEvent& event)
{
	long cursor;

	switch (event.GetId())
	{
		case ID_SELECTDN3DMOD:
			SelectModProfile(GAME_DN3D);

			cursor = m_tcsmodsdn3dprofilechoice->GetInsertionPoint();

			dn3dtcmod_profile_current_temp->profilename = m_tcsmodsdn3dprofilechoice->GetValue();

			m_tcsmodsdn3dprofilechoice->SetString(dn3dtcmod_profile_index, dn3dtcmod_profile_current_temp->profilename);

			m_tcsmodsdn3dprofilechoice->Select(dn3dtcmod_profile_index);

			m_tcsmodsdn3dprofilechoice->SetInsertionPoint(cursor);
			break;

		case ID_DN3DCONFILETEXT:
			dn3dtcmod_profile_current_temp->confile = m_tcsmodsdn3dconfiletextCtrl->GetValue();
			break;

		case ID_DN3DDEFFILETEXT:
			dn3dtcmod_profile_current_temp->deffile = m_tcsmodsdn3ddeffiletextCtrl->GetValue();
			break;

		case ID_DN3DMODURLTEXT:
			dn3dtcmod_profile_current_temp->modurl = m_tcsmodsdn3dmodurltextCtrl->GetValue();
			break;

		case ID_SELECTSWMOD:
			SelectModProfile(GAME_SW);

			cursor = m_tcsmodsswprofilechoice->GetInsertionPoint();

			swtcmod_profile_current_temp->profilename = m_tcsmodsswprofilechoice->GetValue();

			m_tcsmodsswprofilechoice->SetString(swtcmod_profile_index, swtcmod_profile_current_temp->profilename);

			m_tcsmodsswprofilechoice->Select(swtcmod_profile_index);

			m_tcsmodsswprofilechoice->SetInsertionPoint(cursor);
			break;

		case ID_SWDEFFILETEXT:
			swtcmod_profile_current_temp->deffile = m_tcsmodsswdeffiletextCtrl->GetValue();
			break;

		case ID_SWMODURLTEXT:
			swtcmod_profile_current_temp->modurl = m_tcsmodsswmodurltextCtrl->GetValue();
			break;
		default: ;
	}
}

void TCsMODsDialog::OnAddNewProfile(wxCommandEvent& event)
{
	switch (event.GetId())
	{
		case ID_ADDNEWDN3DMOD:
			if (!dn3dtcmod_profile_list_temp)
			{
				dn3dtcmod_profile_list_temp = new dn3dtcmod_profile_t;
				dn3dtcmod_profile_list_temp->next = NULL;

				m_tcsmodsdn3dprofilechoice->Enable();
				m_tcsmodsdn3ddeletebutton->Enable();
				m_tcsmodsdn3dmodfilesstaticText->Enable();
				m_tcsmodsdn3dmodfileslistBox->Enable();
				m_tcsmodsdn3daddmodfilebutton->Enable();
				m_tcsmodsdn3dmodurlcheckBox->Enable();
			}
			else
			{
				dn3dtcmod_profile_list_temp->previous = new dn3dtcmod_profile_t;
				dn3dtcmod_profile_list_temp->previous->next = dn3dtcmod_profile_list_temp;
				dn3dtcmod_profile_list_temp = dn3dtcmod_profile_list_temp->previous;

				m_tcsmodsdn3dmodfileslistBox->Clear();
				m_tcsmodsdn3dremmodfilebutton->Disable();
				m_tcsmodsdn3dconfilebutton->Disable();
				m_tcsmodsdn3ddeffilebutton->Disable();
				m_tcsmodsdn3dconfilecheckBox->SetValue(false);
				m_tcsmodsdn3dconfilecheckBox->Disable();
				m_tcsmodsdn3dconfiletextCtrl->ChangeValue(wxEmptyString);
				m_tcsmodsdn3dconfiletextCtrl->Disable();
				m_tcsmodsdn3ddeffilecheckBox->SetValue(false);
				m_tcsmodsdn3ddeffilecheckBox->Disable();
				m_tcsmodsdn3ddeffiletextCtrl->ChangeValue(wxEmptyString);
				m_tcsmodsdn3ddeffiletextCtrl->Disable();
				m_tcsmodsdn3dmodurlcheckBox->SetValue(false);
				m_tcsmodsdn3dmodurltextCtrl->ChangeValue(wxEmptyString);
				m_tcsmodsdn3dmodurltextCtrl->Disable();
			}

			dn3dtcmod_profile_list_temp->previous = NULL;

			m_tcsmodsdn3dprofilechoice->Insert(wxT("New profile (rename to the TC/MOD's name to add)"), 0);

			m_tcsmodsdn3dprofilechoice->Select(0);

			dn3dtcmod_profile_index = 0;

			dn3dtcmod_profile_current_temp = dn3dtcmod_profile_list_temp;

			dn3dtcmod_profile_current_temp->profilename = m_tcsmodsdn3dprofilechoice->GetValue();
			dn3dtcmod_profile_current_temp->usemodurl = false;

			m_tcsmodsdn3dprofilechoice->SetFocus();

			break;

		case ID_ADDNEWSWMOD:
			if (!swtcmod_profile_list_temp)
			{
				swtcmod_profile_list_temp = new swtcmod_profile_t;
				swtcmod_profile_list_temp->next = NULL;

				m_tcsmodsswprofilechoice->Enable();
				m_tcsmodsswdeletebutton->Enable();
				m_tcsmodsswmodfilesstaticText->Enable();
				m_tcsmodsswmodfileslistBox->Enable();
				m_tcsmodsswaddmodfilebutton->Enable();
				m_tcsmodsswmodurlcheckBox->Enable();
			}
			else
			{
				swtcmod_profile_list_temp->previous = new swtcmod_profile_t;
				swtcmod_profile_list_temp->previous->next = swtcmod_profile_list_temp;
				swtcmod_profile_list_temp = swtcmod_profile_list_temp->previous;

				m_tcsmodsswmodfileslistBox->Clear();
				m_tcsmodsswremmodfilebutton->Disable();
				m_tcsmodsswdeffilebutton->Disable();
				m_tcsmodsswdeffilecheckBox->SetValue(false);
				m_tcsmodsswdeffilecheckBox->Disable();
				m_tcsmodsswdeffiletextCtrl->ChangeValue(wxEmptyString);
				m_tcsmodsswdeffiletextCtrl->Disable();
				m_tcsmodsswmodurlcheckBox->SetValue(false);
				m_tcsmodsswmodurltextCtrl->ChangeValue(wxEmptyString);
				m_tcsmodsswmodurltextCtrl->Disable();
			}

			swtcmod_profile_list_temp->previous = NULL;

			m_tcsmodsswprofilechoice->Insert(wxT("New profile (rename to the TC/MOD's name to add)"), 0);

			m_tcsmodsswprofilechoice->Select(0);

			swtcmod_profile_index = 0;

			swtcmod_profile_current_temp = swtcmod_profile_list_temp;

			swtcmod_profile_current_temp->profilename = m_tcsmodsswprofilechoice->GetValue();
			swtcmod_profile_current_temp->usemodurl = false;

			m_tcsmodsswprofilechoice->SetFocus();

			break;
		default: ;
	}
}

void TCsMODsDialog::OnDeleteProfile(wxCommandEvent& event)
{
	switch (event.GetId())
	{
		case ID_DELETEDN3DMOD:
			if (wxMessageBox(wxT("Are you sure you want to delete the \"") + m_tcsmodsdn3dprofilechoice->GetValue() + wxT("\" TC/MOD profile ?\n\nNote that if you press the \"Cancel\" button at the bottom of this \"TCs and MODs\" configuration dialog,\nany change you may have done will be reverted/cancelled, including the added and deleted profiles."), wxT("Confirm TC/MOD profile deletion"), wxYES_NO|wxICON_INFORMATION) == wxYES)
			{
				m_tcsmodsdn3dprofilechoice->Delete(dn3dtcmod_profile_index);

				dn3dtcmod_profile_index = 0;

				if (dn3dtcmod_profile_current_temp->previous)
					dn3dtcmod_profile_current_temp->previous->next = dn3dtcmod_profile_current_temp->next;
				else
					dn3dtcmod_profile_list_temp = dn3dtcmod_profile_list_temp->next;

				if (dn3dtcmod_profile_current_temp->next)
					dn3dtcmod_profile_current_temp->next->previous = dn3dtcmod_profile_current_temp->previous;

				delete dn3dtcmod_profile_current_temp;

				dn3dtcmod_profile_current_temp = dn3dtcmod_profile_list_temp;

				if (!dn3dtcmod_profile_list_temp)
				{
					m_tcsmodsdn3dprofilechoice->Clear();
					m_tcsmodsdn3dprofilechoice->Disable();
					m_tcsmodsdn3ddeletebutton->Disable();
					m_tcsmodsdn3dmodfilesstaticText->Disable();
					m_tcsmodsdn3dmodfileslistBox->Clear();
					m_tcsmodsdn3dmodfileslistBox->Disable();
					m_tcsmodsdn3daddmodfilebutton->Disable();
					m_tcsmodsdn3dremmodfilebutton->Disable();
					m_tcsmodsdn3dconfilebutton->Disable();
					m_tcsmodsdn3ddeffilebutton->Disable();
					m_tcsmodsdn3dconfilecheckBox->SetValue(false);
					m_tcsmodsdn3dconfilecheckBox->Disable();
					m_tcsmodsdn3dconfiletextCtrl->ChangeValue(wxEmptyString);
					m_tcsmodsdn3dconfiletextCtrl->Disable();
					m_tcsmodsdn3ddeffilecheckBox->SetValue(false);
					m_tcsmodsdn3ddeffilecheckBox->Disable();
					m_tcsmodsdn3ddeffiletextCtrl->ChangeValue(wxEmptyString);
					m_tcsmodsdn3ddeffiletextCtrl->Disable();
					m_tcsmodsdn3dmodurlcheckBox->SetValue(false);
					m_tcsmodsdn3dmodurlcheckBox->Disable();
					m_tcsmodsdn3dmodurltextCtrl->ChangeValue(wxEmptyString);
					m_tcsmodsdn3dmodurltextCtrl->Disable();
				}
				else
				{
					m_tcsmodsdn3dprofilechoice->Select(0);

					m_tcsmodsdn3dmodfileslistBox->Set(dn3dtcmod_profile_current_temp->modfiles);

					m_tcsmodsdn3dremmodfilebutton->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty());
					m_tcsmodsdn3dconfilebutton->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->confile.IsEmpty());
					m_tcsmodsdn3ddeffilebutton->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->deffile.IsEmpty());

					m_tcsmodsdn3dconfilecheckBox->SetValue(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->confile.IsEmpty());
					m_tcsmodsdn3dconfilecheckBox->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty());
					m_tcsmodsdn3dconfiletextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->confile);
					m_tcsmodsdn3dconfiletextCtrl->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->confile.IsEmpty());

					m_tcsmodsdn3ddeffilecheckBox->SetValue(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->deffile.IsEmpty());
					m_tcsmodsdn3ddeffilecheckBox->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty());
					m_tcsmodsdn3ddeffiletextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->deffile);
					m_tcsmodsdn3ddeffiletextCtrl->Enable(!dn3dtcmod_profile_current_temp->modfiles.IsEmpty() && !dn3dtcmod_profile_current_temp->deffile.IsEmpty());

					m_tcsmodsdn3dmodurlcheckBox->SetValue(dn3dtcmod_profile_current_temp->usemodurl);
					m_tcsmodsdn3dmodurltextCtrl->ChangeValue(dn3dtcmod_profile_current_temp->modurl);
					m_tcsmodsdn3dmodurltextCtrl->Enable(dn3dtcmod_profile_current_temp->usemodurl);
				}
			}

			break;

		case ID_DELETESWMOD:
			if (wxMessageBox(wxT("Are you sure you want to delete the \"") + m_tcsmodsswprofilechoice->GetValue() + wxT("\" TC/MOD profile ?\n\nNote that if you press the \"Cancel\" button at the bottom of this \"TCs and MODs\" configuration dialog,\nany change you may have done will be reverted/cancelled, including the added and deleted profiles."), wxT("Confirm TC/MOD profile deletion"), wxYES_NO|wxICON_INFORMATION) == wxYES)
			{
				m_tcsmodsswprofilechoice->Delete(swtcmod_profile_index);

				swtcmod_profile_index = 0;

				if (swtcmod_profile_current_temp->previous)
					swtcmod_profile_current_temp->previous->next = swtcmod_profile_current_temp->next;
				else
					swtcmod_profile_list_temp = swtcmod_profile_list_temp->next;

				if (swtcmod_profile_current_temp->next)
					swtcmod_profile_current_temp->next->previous = swtcmod_profile_current_temp->previous;

				delete swtcmod_profile_current_temp;

				swtcmod_profile_current_temp = swtcmod_profile_list_temp;

				if (!swtcmod_profile_list_temp)
				{
					m_tcsmodsswprofilechoice->Clear();
					m_tcsmodsswprofilechoice->Disable();
					m_tcsmodsswdeletebutton->Disable();
					m_tcsmodsswmodfilesstaticText->Disable();
					m_tcsmodsswmodfileslistBox->Clear();
					m_tcsmodsswmodfileslistBox->Disable();
					m_tcsmodsswaddmodfilebutton->Disable();
					m_tcsmodsswremmodfilebutton->Disable();
					m_tcsmodsswdeffilebutton->Disable();
					m_tcsmodsswdeffilecheckBox->SetValue(false);
					m_tcsmodsswdeffilecheckBox->Disable();
					m_tcsmodsswdeffiletextCtrl->ChangeValue(wxEmptyString);
					m_tcsmodsswdeffiletextCtrl->Disable();
					m_tcsmodsswmodurlcheckBox->SetValue(false);
					m_tcsmodsswmodurlcheckBox->Disable();
					m_tcsmodsswmodurltextCtrl->ChangeValue(wxEmptyString);
					m_tcsmodsswmodurltextCtrl->Disable();
				}
				else
				{
					m_tcsmodsswprofilechoice->Select(0);

					m_tcsmodsswmodfileslistBox->Set(swtcmod_profile_current_temp->modfiles);

					m_tcsmodsswremmodfilebutton->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty());
					m_tcsmodsswdeffilebutton->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty() && !swtcmod_profile_current_temp->deffile.IsEmpty());

					m_tcsmodsswdeffilecheckBox->SetValue(!swtcmod_profile_current_temp->modfiles.IsEmpty() && !swtcmod_profile_current_temp->deffile.IsEmpty());
					m_tcsmodsswdeffilecheckBox->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty());
					m_tcsmodsswdeffiletextCtrl->ChangeValue(swtcmod_profile_current_temp->deffile);
					m_tcsmodsswdeffiletextCtrl->Enable(!swtcmod_profile_current_temp->modfiles.IsEmpty() && !swtcmod_profile_current_temp->deffile.IsEmpty());

					m_tcsmodsswmodurlcheckBox->SetValue(swtcmod_profile_current_temp->usemodurl);
					m_tcsmodsswmodurltextCtrl->ChangeValue(swtcmod_profile_current_temp->modurl);
					m_tcsmodsswmodurltextCtrl->Enable(swtcmod_profile_current_temp->usemodurl);
				}
			}

			break;
		default: ;
	}
}

void TCsMODsDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	if (ApplySettings())
		Destroy();
}

void TCsMODsDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	if (dn3dtcmod_profile_list_temp)
	{
		while (dn3dtcmod_profile_list_temp->next)
		{
			dn3dtcmod_profile_current_temp = dn3dtcmod_profile_list_temp->next;

			while (dn3dtcmod_profile_current_temp->next)
				dn3dtcmod_profile_current_temp = dn3dtcmod_profile_current_temp->next;

			dn3dtcmod_profile_current_temp->previous->next = NULL;

			delete dn3dtcmod_profile_current_temp;
		}

		delete dn3dtcmod_profile_list_temp;
	}

	if (swtcmod_profile_list_temp)
	{
		while (swtcmod_profile_list_temp->next)
		{
			swtcmod_profile_current_temp = swtcmod_profile_list_temp->next;

			while (swtcmod_profile_current_temp->next)
				swtcmod_profile_current_temp = swtcmod_profile_current_temp->next;

			swtcmod_profile_current_temp->previous->next = NULL;

			delete swtcmod_profile_current_temp;
		}

		delete swtcmod_profile_list_temp;
	}

	Destroy();
}

bool TCsMODsDialog::ApplySettings()
{
	dn3dtcmod_profile_t *dn3dtcmod_profile_list_comp, *dn3dtcmod_profile_current_comp;
	swtcmod_profile_t *swtcmod_profile_list_comp, *swtcmod_profile_current_comp;
	size_t l_loop_var;


	if (dn3dtcmod_profile_list_temp)
	{
		dn3dtcmod_profile_current_comp = dn3dtcmod_profile_list_temp;

		while (dn3dtcmod_profile_current_comp)
		{
			dn3dtcmod_profile_current_comp->profilename.Trim(false);

			if (dn3dtcmod_profile_current_comp->profilename.IsEmpty())
			{
				wxMessageBox(wxT("You have set one or many TC/MOD profile(s) to an empty name."), wxT("TC/MOD profile(s) with empty name"), wxOK|wxICON_EXCLAMATION);

				return false;
			}

			dn3dtcmod_profile_current_comp = dn3dtcmod_profile_current_comp->next;
		}
	}

	if (dn3dtcmod_profile_list_temp)
	{
		if (dn3dtcmod_profile_list_temp->next)
		{
			dn3dtcmod_profile_list_comp = dn3dtcmod_profile_list_temp;

			while (dn3dtcmod_profile_list_comp)
			{
				dn3dtcmod_profile_current_comp = dn3dtcmod_profile_list_comp->next;

				while (dn3dtcmod_profile_current_comp)
				{
					if (dn3dtcmod_profile_list_comp->profilename.IsSameAs(dn3dtcmod_profile_current_comp->profilename, false))
					{
						l_loop_var = 0;

						while (m_tcsmodsnotebook->GetPageText(l_loop_var) != GAMENAME_DN3D)
							l_loop_var++;

						m_tcsmodsnotebook->ChangeSelection(l_loop_var);

						wxMessageBox(wxT("You have named many different TC/MOD profiles to \"") + dn3dtcmod_profile_list_comp->profilename + wxT("\"\n\nPlease use an unique name for each of the different TC/MOD profiles."), wxT("Different TC/MOD profiles with same name"), wxOK|wxICON_EXCLAMATION);

						return false;
					}

					dn3dtcmod_profile_current_comp = dn3dtcmod_profile_current_comp->next;
				}

				dn3dtcmod_profile_list_comp = dn3dtcmod_profile_list_comp->next;
			}
		}
	}


	if (swtcmod_profile_list_temp)
	{
		swtcmod_profile_current_comp = swtcmod_profile_list_temp;

		while (swtcmod_profile_current_comp)
		{
			swtcmod_profile_current_comp->profilename.Trim(false);

			if (swtcmod_profile_current_comp->profilename.IsEmpty())
			{
				wxMessageBox(wxT("You have set one or many TC/MOD profile(s) to an empty name."), wxT("TC/MOD profile(s) with empty name"), wxOK|wxICON_EXCLAMATION);

				return false;
			}

			swtcmod_profile_current_comp = swtcmod_profile_current_comp->next;
		}
	}

	if (swtcmod_profile_list_temp)
	{
		if (swtcmod_profile_list_temp->next)
		{
			swtcmod_profile_list_comp = swtcmod_profile_list_temp;

			while (swtcmod_profile_list_comp)
			{
				swtcmod_profile_current_comp = swtcmod_profile_list_comp->next;

				while (swtcmod_profile_current_comp)
				{
					if (swtcmod_profile_list_comp->profilename.IsSameAs(swtcmod_profile_current_comp->profilename, false))
					{
						l_loop_var = 0;

						while (m_tcsmodsnotebook->GetPageText(l_loop_var) != GAMENAME_SW)
							l_loop_var++;

						m_tcsmodsnotebook->ChangeSelection(l_loop_var);

						wxMessageBox(wxT("You have named many different TC/MOD profiles to \"") + swtcmod_profile_list_comp->profilename + wxT("\"\n\nPlease use an unique name for each of the different TC/MOD profiles."), wxT("Different TC/MOD profiles with same name"), wxOK|wxICON_EXCLAMATION);

						return false;
					}

					swtcmod_profile_current_comp = swtcmod_profile_current_comp->next;
				}

				swtcmod_profile_list_comp = swtcmod_profile_list_comp->next;
			}
		}
	}


	if (g_configuration->dn3dtcmod_profile_list)
	{
		while (g_configuration->dn3dtcmod_profile_list->next)
		{
			g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list->next;

			while (g_configuration->dn3dtcmod_profile_current->next)
				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;

			g_configuration->dn3dtcmod_profile_current->previous->next = NULL;

			delete g_configuration->dn3dtcmod_profile_current;
		}

		delete g_configuration->dn3dtcmod_profile_list;
	}

	g_configuration->dn3dtcmod_profile_list = NULL;

	if (dn3dtcmod_profile_list_temp)
	{
		if (dn3dtcmod_profile_list_temp->next)
		{
			while (dn3dtcmod_profile_list_temp)
			{
				dn3dtcmod_profile_list_comp = dn3dtcmod_profile_list_temp;

				dn3dtcmod_profile_current_comp = dn3dtcmod_profile_list_temp->next;

				while (dn3dtcmod_profile_current_comp)
				{
					if (dn3dtcmod_profile_list_comp->profilename.CmpNoCase(dn3dtcmod_profile_current_comp->profilename) > 0)
						dn3dtcmod_profile_list_comp = dn3dtcmod_profile_current_comp;

					dn3dtcmod_profile_current_comp = dn3dtcmod_profile_current_comp->next;
				}

				if (dn3dtcmod_profile_list_comp->previous)
					dn3dtcmod_profile_list_comp->previous->next = dn3dtcmod_profile_list_comp->next;
				else
					dn3dtcmod_profile_list_temp = dn3dtcmod_profile_list_temp->next;

				if (dn3dtcmod_profile_list_comp->next)
					dn3dtcmod_profile_list_comp->next->previous = dn3dtcmod_profile_list_comp->previous;

				if (!g_configuration->dn3dtcmod_profile_list)
				{
					g_configuration->dn3dtcmod_profile_list = dn3dtcmod_profile_list_comp;
					g_configuration->dn3dtcmod_profile_list->previous = NULL;
				}
				else
				{
					g_configuration->dn3dtcmod_profile_list->next = dn3dtcmod_profile_list_comp;
					g_configuration->dn3dtcmod_profile_list->next->previous = g_configuration->dn3dtcmod_profile_list;
					g_configuration->dn3dtcmod_profile_list = g_configuration->dn3dtcmod_profile_list->next;
					g_configuration->dn3dtcmod_profile_list->next = NULL;
				}
			}

			while (g_configuration->dn3dtcmod_profile_list->previous)
				g_configuration->dn3dtcmod_profile_list = g_configuration->dn3dtcmod_profile_list->previous;
		}
		else
			g_configuration->dn3dtcmod_profile_list = dn3dtcmod_profile_list_temp;
	}


	if (g_configuration->swtcmod_profile_list)
	{
		while (g_configuration->swtcmod_profile_list->next)
		{
			g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list->next;

			while (g_configuration->swtcmod_profile_current->next)
				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;

			g_configuration->swtcmod_profile_current->previous->next = NULL;

			delete g_configuration->swtcmod_profile_current;
		}

		delete g_configuration->swtcmod_profile_list;
	}

	g_configuration->swtcmod_profile_list = NULL;

	if (swtcmod_profile_list_temp)
	{
		if (swtcmod_profile_list_temp->next)
		{
			while (swtcmod_profile_list_temp)
			{
				swtcmod_profile_list_comp = swtcmod_profile_list_temp;

				swtcmod_profile_current_comp = swtcmod_profile_list_temp->next;

				while (swtcmod_profile_current_comp)
				{
					if (swtcmod_profile_list_comp->profilename.CmpNoCase(swtcmod_profile_current_comp->profilename) > 0)
						swtcmod_profile_list_comp = swtcmod_profile_current_comp;

					swtcmod_profile_current_comp = swtcmod_profile_current_comp->next;
				}

				if (swtcmod_profile_list_comp->previous)
					swtcmod_profile_list_comp->previous->next = swtcmod_profile_list_comp->next;
				else
					swtcmod_profile_list_temp = swtcmod_profile_list_temp->next;

				if (swtcmod_profile_list_comp->next)
					swtcmod_profile_list_comp->next->previous = swtcmod_profile_list_comp->previous;

				if (!g_configuration->swtcmod_profile_list)
				{
					g_configuration->swtcmod_profile_list = swtcmod_profile_list_comp;
					g_configuration->swtcmod_profile_list->previous = NULL;
				}
				else
				{
					g_configuration->swtcmod_profile_list->next = swtcmod_profile_list_comp;
					g_configuration->swtcmod_profile_list->next->previous = g_configuration->swtcmod_profile_list;
					g_configuration->swtcmod_profile_list = g_configuration->swtcmod_profile_list->next;
					g_configuration->swtcmod_profile_list->next = NULL;
				}
			}

			while (g_configuration->swtcmod_profile_list->previous)
				g_configuration->swtcmod_profile_list = g_configuration->swtcmod_profile_list->previous;
		}
		else
			g_configuration->swtcmod_profile_list = swtcmod_profile_list_temp;
	}


	g_configuration->Save();

	return true;
}

