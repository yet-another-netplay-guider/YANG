/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/colordlg.h>
#include <wx/filename.h>
#include <wx/fontenum.h>
#include <wx/icon.h>
#include <wx/log.h>
#include <wx/msgdlg.h>
#include <wx/sizer.h>
#include <wx/socket.h>
#endif

#include "host_frame.h"
#include "client_frame.h"
#include "lookandfeel_dialog.h"
#include "config.h"
#include "yang.h"
#include "yang.xpm"

BEGIN_EVENT_TABLE(LookAndFeelDialog, YANGDialog)

EVT_CHECKBOX(ID_LOOKANDFEELUSECUSTOMFONT, LookAndFeelDialog::OnUseCustomStyleAnyCheck)
EVT_CHECKBOX(ID_LOOKANDFEELUSECUSTOMCOLORS, LookAndFeelDialog::OnUseCustomStyleAnyCheck)
EVT_CHOICE(ID_LOOKANDFEELFONTCHOICE, LookAndFeelDialog::OnFontChoice)
EVT_COMBOBOX(ID_LOOKANDFEELFONTSIZECOMBOBOX, LookAndFeelDialog::OnFontSizeUpdate)
EVT_TEXT_ENTER(ID_LOOKANDFEELFONTSIZECOMBOBOX, LookAndFeelDialog::OnFontSizeUpdate)
EVT_HYPERLINK(ID_LOOKANDFEELTEXTCOLORCHOICE, LookAndFeelDialog::OnTextColorSet)
EVT_HYPERLINK(ID_LOOKANDFEELBACKGROUNDCOLORCHOICE, LookAndFeelDialog::OnBackgroundColorSet)
EVT_BUTTON(wxID_OK, LookAndFeelDialog::OnOk)
//EVT_BUTTON(wxID_CANCEL, LookAndFeelDialog::OnCancel)

END_EVENT_TABLE()

LookAndFeelDialog::LookAndFeelDialog(wxWindow* parent, bool isHost) : YANGDialog(parent, wxID_ANY, wxT("Chat output style"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* LookAndFeelbSizer = new wxBoxSizer( wxVERTICAL );

	m_LookAndFeelpanel = new YANGPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* LookAndFeelpanelbSizer = new wxBoxSizer( wxVERTICAL );

	/*	m_darkthemecheckBox = new YANGCheckBox( m_LookAndFeelpanel, wxID_ANY, wxT("Use a dark theme."), wxDefaultPosition, wxDefaultSize, 0 );
	LookAndFeelpanelbSizer->Add( m_darkthemecheckBox, 0, wxALL, 5 );*/

	m_chatfontlayoutcheckBox = new YANGCheckBox( m_LookAndFeelpanel, ID_LOOKANDFEELUSECUSTOMFONT, wxT("Use customized font and size for chat."), wxDefaultPosition, wxDefaultSize, 0 );
	LookAndFeelpanelbSizer->Add( m_chatfontlayoutcheckBox, 0, wxALL, 5 );

	m_chatcolorlayoutcheckBox = new YANGCheckBox( m_LookAndFeelpanel, ID_LOOKANDFEELUSECUSTOMCOLORS, wxT("Use customized colors for chat."), wxDefaultPosition, wxDefaultSize, 0 );
	LookAndFeelpanelbSizer->Add( m_chatcolorlayoutcheckBox, 0, wxALL, 5 );

	wxBoxSizer* FontbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_fontstaticText = new wxStaticText( m_LookAndFeelpanel, wxID_ANY, wxT("Font:"), wxDefaultPosition, wxDefaultSize, 0 );
	FontbSizer->Add( m_fontstaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_fontchoice = new YANGChoice( m_LookAndFeelpanel, ID_LOOKANDFEELFONTCHOICE, wxDefaultPosition, wxDefaultSize );
	FontbSizer->Add( m_fontchoice, 1, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	LookAndFeelpanelbSizer->Add( FontbSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* FontSizerbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_fontsizestaticText = new wxStaticText( m_LookAndFeelpanel, wxID_ANY, wxT("Size:"), wxDefaultPosition, wxDefaultSize, 0 );
	FontSizerbSizer->Add( m_fontsizestaticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_fontsizecomboBox = new YANGComboBox( m_LookAndFeelpanel, ID_LOOKANDFEELFONTSIZECOMBOBOX, wxT("12"), wxDefaultPosition, wxDefaultSize, 0, NULL, 0 );
	m_fontsizecomboBox->Append( wxT("8") );
	m_fontsizecomboBox->Append( wxT("9") );
	m_fontsizecomboBox->Append( wxT("10") );
	m_fontsizecomboBox->Append( wxT("11") );
	m_fontsizecomboBox->Append( wxT("12") );
	m_fontsizecomboBox->Append( wxT("14") );
	m_fontsizecomboBox->Append( wxT("16") );
	m_fontsizecomboBox->Append( wxT("18") );
	m_fontsizecomboBox->Append( wxT("20") );
	m_fontsizecomboBox->Append( wxT("24") );
	m_fontsizecomboBox->Append( wxT("36") );
	m_fontsizecomboBox->Append( wxT("48") );
	m_fontsizecomboBox->Append( wxT("72") );
	FontSizerbSizer->Add( m_fontsizecomboBox, 0, wxALL, 5 );

	LookAndFeelpanelbSizer->Add( FontSizerbSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* ColorbSizer = new wxBoxSizer( wxHORIZONTAL );

	m_textcolorhyperlink = new YANGHyperlinkCtrl( m_LookAndFeelpanel, ID_LOOKANDFEELTEXTCOLORCHOICE, wxT("Text color"), wxEmptyString, wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE );
	ColorbSizer->Add( m_textcolorhyperlink, 0, wxALL, 5 );

	m_backcolorhyperlink = new YANGHyperlinkCtrl( m_LookAndFeelpanel, ID_LOOKANDFEELBACKGROUNDCOLORCHOICE, wxT("Background color"), wxEmptyString, wxDefaultPosition, wxDefaultSize, wxHL_DEFAULT_STYLE );
	ColorbSizer->Add( m_backcolorhyperlink, 0, wxALL, 5 );

	LookAndFeelpanelbSizer->Add( ColorbSizer, 0, wxEXPAND, 5 );

	m_chatsamplestaticText = new wxStaticText( m_LookAndFeelpanel, wxID_ANY, wxT("Customized layout test:"), wxDefaultPosition, wxDefaultSize, 0 );
	LookAndFeelpanelbSizer->Add( m_chatsamplestaticText, 0, wxALL, 5 );

	m_chatsampletextCtrl = new YANGTextCtrl( m_LookAndFeelpanel, wxID_ANY, wxT("This is a sample text."), wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY|wxTE_RICH );
	LookAndFeelpanelbSizer->Add( m_chatsampletextCtrl, 1, wxALL|wxEXPAND, 5 );

	m_LookAndFeelsdbSizer = new wxStdDialogButtonSizer();
	m_LookAndFeelsdbSizerOK = new YANGButton( m_LookAndFeelpanel, wxID_OK );
	m_LookAndFeelsdbSizer->AddButton( m_LookAndFeelsdbSizerOK );
	m_LookAndFeelsdbSizerCancel = new YANGButton( m_LookAndFeelpanel, wxID_CANCEL );
	m_LookAndFeelsdbSizer->AddButton( m_LookAndFeelsdbSizerCancel );
	m_LookAndFeelsdbSizer->Realize();
	LookAndFeelpanelbSizer->Add( m_LookAndFeelsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );

	m_LookAndFeelpanel->SetSizer( LookAndFeelpanelbSizer );
	m_LookAndFeelpanel->Layout();
	LookAndFeelpanelbSizer->Fit( m_LookAndFeelpanel );
	LookAndFeelbSizer->Add( m_LookAndFeelpanel, 1, wxEXPAND | wxALL, 5 );

	SetSizer( LookAndFeelbSizer );
	Layout();
	LookAndFeelbSizer->Fit( this );

	Centre();
	SetIcon(wxIcon(yang_xpm));

	wxFontEnumerator l_fontenum;
	l_fontenum.EnumerateFacenames();
	wxArrayString l_facenames = l_fontenum.GetFacenames();
	l_facenames.Sort(g_compareNoCase);
	m_fontchoice->Append(l_facenames);

	m_def_font = m_chatsampletextCtrl->GetFont();
	m_def_textcolor = m_chatsampletextCtrl->GetForegroundColour();
	m_def_backcolor = m_chatsampletextCtrl->GetBackgroundColour();

	m_parent = parent;
	m_is_host = isHost;
}
/*
LookAndFeelDialog::~LookAndFeelDialog()
{
if (g_main_frame) // We may want to refresh theme. Even if not, also do something else.
g_main_frame->Destroy();
g_main_frame = new MainFrame(NULL);
g_main_frame->ShowMainFrame();
}
*/
void LookAndFeelDialog::RefreshSampleText()
{
	wxFont l_font;
	wxColour l_textcolor, l_backcolor;

	if (m_chatfontlayoutcheckBox->GetValue())
		l_font = m_font;
	else
		l_font = m_def_font;

	if (m_chatcolorlayoutcheckBox->GetValue())
	{
		l_textcolor = m_textcolor;
		l_backcolor = m_backcolor;
	}
	else
	{
		l_textcolor = m_def_textcolor;
		l_backcolor = m_def_backcolor;
	}

	m_chatsampletextCtrl->SetForegroundColour(l_textcolor);
	m_chatsampletextCtrl->SetBackgroundColour(l_backcolor);
	m_chatsampletextCtrl->SetFont(l_font);
#ifdef __WXMSW__
	m_chatsampletextCtrl->SetDefaultStyle(wxTextAttr(l_textcolor, l_backcolor, l_font));
	m_chatsampletextCtrl->SetStyle(0, m_chatsampletextCtrl->GetLastPosition(), wxTextAttr(l_textcolor, l_backcolor, l_font));
#endif

	m_chatsampletextCtrl->SetValue(wxT("This is a sample text."));
}

void LookAndFeelDialog::ShowDialog()
{
	m_chatfontlayoutcheckBox->SetValue(g_configuration->use_custom_chat_text_layout);
	m_chatcolorlayoutcheckBox->SetValue(g_configuration->use_custom_chat_text_colors);

	m_fontchoice->SetStringSelection(g_configuration->chat_font);
	m_fontsizecomboBox->SetValue(wxString::Format(wxT("%d"), g_configuration->chat_font_size));

	m_textcolor = wxColour(g_configuration->chat_textcolor_red,
		g_configuration->chat_textcolor_green,
		g_configuration->chat_textcolor_blue);
	m_backcolor = wxColour(g_configuration->chat_backcolor_red,
		g_configuration->chat_backcolor_green,
		g_configuration->chat_backcolor_blue);
	m_font.SetFaceName(g_configuration->chat_font);
	m_font.SetPointSize(g_configuration->chat_font_size);
	RefreshSampleText();
	Show();
}


void LookAndFeelDialog::OnFontChoice(wxCommandEvent& WXUNUSED(event))
{
	m_font.SetFaceName(m_fontchoice->GetStringSelection());
	RefreshSampleText();
}

void LookAndFeelDialog::OnUseCustomStyleAnyCheck(wxCommandEvent& WXUNUSED(event))
{
	RefreshSampleText();
}

void LookAndFeelDialog::OnFontSizeUpdate(wxCommandEvent& WXUNUSED(event))
{
	unsigned long l_num_value;
	int l_num_actual_value;
	if (m_fontsizecomboBox->GetValue().ToULong(&l_num_value))
	{
		l_num_actual_value = (int)l_num_value;
		if (l_num_actual_value < 0)
			l_num_actual_value = CUSTOM_FONT_DEFAULT_SIZE;
	}
	else
		l_num_actual_value = CUSTOM_FONT_DEFAULT_SIZE;
	m_font.SetPointSize(l_num_actual_value);
	m_fontsizecomboBox->SetValue(wxString::Format(wxT("%d"), l_num_actual_value));

	RefreshSampleText();
}
/*
void LookAndFeelDialog::UpdateAndSaveCustomColors()
{
wxColour l_color;
for (size_t l_loop_var = 0; l_loop_var < COLORDIALOG_NUM_CUSTOM_COLORS; l_loop_var++)
{
l_color = m_colordata.GetCustomColour(l_loop_var);
g_configuration->custcolor_red[l_loop_var] = l_color.Red();
g_configuration->custcolor_green[l_loop_var] = l_color.Green();
g_configuration->custcolor_blue[l_loop_var] = l_color.Blue();
}
g_configuration->Save();
}
*/
void LookAndFeelDialog::OnTextColorSet(wxHyperlinkEvent& WXUNUSED(event))
{
	m_colordata.SetColour(m_textcolor);
	wxColourDialog* l_dialog = new wxColourDialog(this, &m_colordata);
	int l_result = l_dialog->ShowModal();
	m_colordata = l_dialog->GetColourData();
	if (l_result == wxID_OK)
	{
		m_textcolor = m_colordata.GetColour();
		l_dialog->Destroy(); 
		RefreshSampleText();
	}
	//UpdateAndSaveCustomColors();
}

void LookAndFeelDialog::OnBackgroundColorSet(wxHyperlinkEvent& WXUNUSED(event))
{
	m_colordata.SetColour(m_backcolor);
	wxColourDialog* l_dialog = new wxColourDialog(this, &m_colordata);
	int l_result = l_dialog->ShowModal();
	m_colordata = l_dialog->GetColourData();
	if (l_result == wxID_OK)
	{
		m_backcolor = m_colordata.GetColour();
		l_dialog->Destroy(); 
		RefreshSampleText();
	}
	//UpdateAndSaveCustomColors();
}

void LookAndFeelDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	wxColour l_color;
	/*
	for (size_t l_loop_var = 0; l_loop_var < COLORDIALOG_NUM_CUSTOM_COLORS; l_loop_var++)
	{
	l_color = m_colordata.SetCustomColour(l_loop_var);
	g_configuration->custcolor_red[l_loop_var] = l_color.Red();
	g_configuration->custcolor_green[l_loop_var] = l_color.Green();
	g_configuration->custcolor_blue[l_loop_var] = l_color.Blue();
	}
	*/
	//g_configuration->theme_is_dark = m_darkthemecheckBox->GetValue();
	g_configuration->use_custom_chat_text_layout = m_chatfontlayoutcheckBox->GetValue();
	g_configuration->use_custom_chat_text_colors = m_chatcolorlayoutcheckBox->GetValue();

	g_configuration->chat_textcolor_red = m_textcolor.Red();
	g_configuration->chat_textcolor_green = m_textcolor.Green();
	g_configuration->chat_textcolor_blue = m_textcolor.Blue();

	g_configuration->chat_backcolor_red = m_backcolor.Red();
	g_configuration->chat_backcolor_green = m_backcolor.Green();
	g_configuration->chat_backcolor_blue = m_backcolor.Blue();

	g_configuration->chat_font = m_font.GetFaceName();
	g_configuration->chat_font_size = m_font.GetPointSize();

	g_configuration->Save();

	wxCommandEvent* l_close_event;
	if (m_is_host)
		l_close_event = new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, ID_HOSTLOOKANDFEELCLOSE);
	else
		l_close_event = new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, ID_CLIENTLOOKANDFEELCLOSE);
#if wxCHECK_VERSION(2, 9, 0)
	m_parent->GetEventHandler()->QueueEvent(l_close_event);
#else
	m_parent->AddPendingEvent(*l_close_event);
	delete l_close_event;
#endif
	Hide();
}
/*
void LookAndFeelDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
Destroy();
}
*/
