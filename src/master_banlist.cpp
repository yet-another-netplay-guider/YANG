/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#include <wx/textfile.h>
#endif

#include "master_banlist.h"

MasterBanList::MasterBanList()
{
	m_num_of_clients = 0;
}

void MasterBanList::SetClients(MasterBanList_LinkedList clients_linked_list, long new_num_of_clients)
{
	int l_loop_var;
	wxCriticalSectionLocker l_locker(m_critical_section);
	if (m_num_of_clients)
		delete [] m_clients;
	if (new_num_of_clients)
	{
		m_clients = new MasterBanList_Client[new_num_of_clients];
		clients_linked_list = clients_linked_list->next;
		l_loop_var = 0;
		while (clients_linked_list != NULL)
		{
			m_clients[l_loop_var] = clients_linked_list->client;
			l_loop_var++;
			clients_linked_list = clients_linked_list->next;
		}
	}
	m_num_of_clients = new_num_of_clients;
}

void MasterBanList::Load(const wxArrayString& filenames)
{
	wxTextFile l_settingsfile;
	wxString l_filename, l_curr_hostname;
	wxIPV4address l_addr;
	int l_file_loop_var, l_line_loop_var, l_num_of_clients = 0,
		l_num_of_files = filenames.GetCount(), l_num_of_lines;
	MasterBanList_LinkedList l_clients_linked_list = new MasterBanList_Node,
		l_curr_node = l_clients_linked_list;
	l_curr_node->next = NULL;
	for (l_file_loop_var=0; l_file_loop_var<l_num_of_files; l_file_loop_var++)
	{
		l_filename = filenames[l_file_loop_var];
		if (wxFile::Access(l_filename, wxFile::read))
		{
			l_settingsfile.Open(l_filename);
			l_num_of_lines = l_settingsfile.GetLineCount();
			for (l_line_loop_var=0; l_line_loop_var<l_num_of_lines; l_line_loop_var++)
			{
				l_curr_node->next = new MasterBanList_Node;
				l_curr_node = l_curr_node->next;
				l_curr_hostname = l_settingsfile[l_line_loop_var];
				l_curr_node->client.hostname = l_curr_hostname;
				if (l_addr.Hostname(l_curr_hostname))
					l_curr_node->client.address = l_addr.IPAddress();
				else
					l_curr_node->client.address = wxEmptyString;
				l_curr_node->next = NULL;
				l_num_of_clients++;
			}
			l_settingsfile.Close();
		}
	}
	SetClients(l_clients_linked_list, l_num_of_clients);
	do // We must remove ALL nodes,
	{  // including the list anchor.
		l_curr_node = l_clients_linked_list->next;
		delete l_clients_linked_list;
		l_clients_linked_list = l_curr_node;
	}
	while (l_curr_node != NULL);
}

bool MasterBanList::IsBanned(const wxString& address)
{
	long l_loop_var;
	wxCriticalSectionLocker l_locker(m_critical_section);
	l_loop_var = 0;
	while (l_loop_var < m_num_of_clients)
	{
		if ((address == m_clients[l_loop_var].hostname) || (address == m_clients[l_loop_var].address))
			return true;
		l_loop_var++;
	}
	return false;
}
