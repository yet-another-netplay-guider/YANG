/**************************************************************************

Copyright 2009-2023 - NY00123
Copyright 2009-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_OUTDATEDEXE_DIALOG_H_
#define _YANG_OUTDATEDEXE_DIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/panel.h>
#include <wx/stattext.h>
#include <wx/string.h>
#endif

class OutdatedExeDialog : public wxDialog
{
public:
	OutdatedExeDialog(wxWindow* parent, const wxString& contents, const wxString& title,
		const wxString& patch, const wxString& extrapatch,
		const wxString& patch_filename, const wxString& extrapatch_filename,
		bool have_extrapatch);
	void OnDownload1(wxCommandEvent& event);
	void OnDownload2(wxCommandEvent& event);
	void OnClose(wxCommandEvent& event);
private:
	wxPanel* m_OutdatedExepanel;
	wxStaticText* m_outdatedexecontents;
	wxButton *m_download1button, *m_download2button, *m_closebutton;
	wxString m_patch, m_extrapatch;

	DECLARE_EVENT_TABLE()
};

#endif
