/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#include <wx/stdpaths.h>
#include <wx/dir.h>
#include <wx/filename.h>
#include <wx/image.h>
#include <wx/msgdlg.h>
#include <wx/textfile.h>
#include <wx/sound.h>
#endif

#include "yang.h"
#include "config.h"
#include "main_frame.h"
#include "host_frame.h"
#include "client_frame.h"
#include "sp_dialog.h"
#include "mp_dialog.h"
#include "manualjoin_dialog.h"
#include "srcports_dialog.h"
#include "tcsmods_dialog.h"
#include "network_dialog.h"
#include "lookandfeel_dialog.h"
#include "adv_dialog.h"
#include "outdatedexe_dialog.h"
#include "getosdesc.h"
#include "crc.h"

#ifdef ENABLE_UPNP
#include "upnp.h"
#endif

#ifdef __WXMSW__
#include <windows.h>
#include <wx/msw/winundef.h>
#endif

wxArrayString *g_blood_skill_list, *g_dn3d_skill_list, *g_sw_skill_list,
	*g_blood_level_list, *g_blood_cp_level_list, *g_descent_level_list, *g_descent2_level_list, *g_dn3d_level_list, *g_sw_level_list,
	*g_blood_gametype_list, *g_descent_gametype_list, *g_descent2_gametype_list, *g_dn3d_gametype_list, *g_sw_gametype_list,
	*g_blood_monstersettings_list, *g_blood_weaponsettings_list, *g_blood_itemsettings_list, *g_blood_respawnsettings_list,
	*g_dn3d_spawn_list, *g_blood_packetmode_list, *g_dn3d_connectiontype_list,
	*g_dn3d_playercol_list, *g_sw_playercol_list, *g_serverlists_addrs;
short l_duke3dw_player_col_map[] = {0, 9, 10, 11, 12, 13, 14, 15, 16, 23};
bool yang_portable;
wxArrayLong *g_serverlists_portnums;
LauncherConfig *g_configuration;
MainFrame *g_main_frame;
HostedRoomFrame *g_host_frame;
ClientRoomFrame *g_client_frame;
SRCPortsDialog *g_srcports_dialog;
TCsMODsDialog *g_tcsmods_dialog;
NetworkDialog *g_network_dialog;
LookAndFeelDialog *g_lookandfeel_dialog;
AdvDialog *g_adv_dialog;
SPDialog *g_sp_dialog;
MPDialog *g_mp_dialog;
ManualJoinDialog *g_manualjoin_dialog;
wxString *g_launcher_user_profile_path, *g_launcher_resources_path, *g_osdescription;
GeoIP *g_geoip_db;
//FlagsDB *g_countryflags;
#if (defined __WXMAC__) || (defined __WXCOCOA__)
wxMenuBar *g_empty_menubar;
#endif
#ifdef ENABLE_UPNP
UPNPData *g_upnpdata = NULL;
#endif

srcport_crc_t* g_srcport_crc[SOURCEPORT_NUMOFPORTS];
outdated_srcport_t* g_outdated_srcport[NUM_OF_OUTDATED_SRCPORT_EXES];

// Give wxWidgets the means to create a YANGApp object
IMPLEMENT_APP(YANGApp)

void g_CheckForUpdates()
{
	int l_pos1, l_pos2;
	wxSocketClient* m_client = new wxSocketClient();
	wxFrame* m_frame = new wxFrame(NULL, wxID_ANY, wxT("Checking for updates..."));
	m_frame->Centre();
	m_frame->Show(true);
	wxIPV4address l_addr;
	char l_buffer[2048];
	wxString l_temp_str;
	l_addr.Hostname(WEBSITE_HOSTNAME);
	l_addr.Service(80);
	m_client->SetNotify(false);
	m_client->SetTimeout(3);
	if (m_client->Connect(l_addr, true))
	{
//		g_SendCStringToSocket(m_client, (wxString(wxT("GET /yang/files/yang/yangver.txt HTTP/1.1\nHost: ")) + WEBSITE_HOSTNAME + wxT("\n\n")).mb_str(wxConvUTF8));
		g_SendCStringToSocket(m_client, (wxString(wxT("GET /files/yang/yangver.txt HTTP/1.1\nHost: ")) + WEBSITE_HOSTNAME + wxT("\n\n")).mb_str(wxConvUTF8));
		m_client->Read(l_buffer, 2048);
		if (m_client->LastCount())
		{
			l_temp_str = wxString(l_buffer, wxConvUTF8, m_client->LastCount());
			m_client->Destroy();
			l_pos1 = l_temp_str.Find(wxT("YANGVer:"));
			if (l_pos1 != wxNOT_FOUND)
			{
				l_pos1 += 8;
				if ((l_temp_str.Len()) > (unsigned)l_pos1)
				{
					l_pos2 = l_temp_str.Mid(l_pos1).Find(wxT(':'));
					if ((l_pos2 != wxNOT_FOUND) && (l_pos2 > 0))
					{
						l_temp_str = l_temp_str.Mid(l_pos1, l_pos2);
						if (l_temp_str != YANG_STR_VERSION)
						{
							m_frame->Hide();
							if (wxMessageBox(wxT("A new version of YANG is available. Would you like to go to the website?"),
								wxT("An update is available"), wxYES_NO|wxICON_INFORMATION) == wxYES)
								g_OpenURL(WEBSITE_ADDRESS);
						}
					}
				}
			}
		}
		else
			m_client->Destroy();
	}
	m_frame->Destroy();
}

bool g_CheckPortCRC32(SourcePortType src_port, const wxString& executable)
{
	wxFile file;
	char *fileptr;

	unsigned long filesize;
	DWORD crc32;
	//	int l_outdated_dialog_id;

	size_t l_loop_var = 0;
	srcport_crc_t* l_srcport_crc_ptr;

	if (file.Access(executable, wxFile::read) && file.Open(executable, wxFile::read))
	{
		// If the actual file size returned by GetSize is VERY large,
		// something is wrong.
		filesize = (size_t)wxFileName::GetSize(executable).ToULong();

		fileptr = new char[filesize];
		file.Read(fileptr, filesize);
		file.Close();

		crc32 = crc32buf(fileptr, filesize);

		delete[] fileptr;

		l_srcport_crc_ptr = g_srcport_crc[src_port];

		while (l_srcport_crc_ptr)
		{
			if (l_srcport_crc_ptr->filesize == filesize && l_srcport_crc_ptr->crc32 == crc32)
				return true;
			l_srcport_crc_ptr = l_srcport_crc_ptr->next;
		}

		while (l_loop_var < SOURCEPORT_NUMOFPORTS)
		{
			l_srcport_crc_ptr = g_srcport_crc[l_loop_var];
			while (l_srcport_crc_ptr)
			{
				if (l_srcport_crc_ptr->filesize == filesize && l_srcport_crc_ptr->crc32 == crc32)
					break;
				l_srcport_crc_ptr = l_srcport_crc_ptr->next;
			}
			if (l_srcport_crc_ptr)
				break;
			l_loop_var++;
		}

		if (l_loop_var < SOURCEPORT_NUMOFPORTS)
			wxMessageBox(wxT("You have set the executable of ") + l_srcport_crc_ptr->srcportname + wxT("\nin the text input box of ") + g_srcport_crc[src_port]->srcportname, wxT("Known executable set in wrong text input box"), wxOK|wxICON_EXCLAMATION);
		else
		{
			l_loop_var = 0;

			while ((l_loop_var < NUM_OF_OUTDATED_SRCPORT_EXES) && (g_outdated_srcport[l_loop_var]->filesize != filesize || g_outdated_srcport[l_loop_var]->crc32 != crc32))
				l_loop_var++;

			if (l_loop_var < NUM_OF_OUTDATED_SRCPORT_EXES)
			{
				OutdatedExeDialog *m_outdated_dialog = new OutdatedExeDialog(NULL, g_outdated_srcport[l_loop_var]->updateinfo, wxT("Outdated executable detected"), g_outdated_srcport[l_loop_var]->patch_address, g_outdated_srcport[l_loop_var]->extrapatch_address, g_outdated_srcport[l_loop_var]->patch_filename, g_outdated_srcport[l_loop_var]->extrapatch_filename, g_outdated_srcport[l_loop_var]->extrapatch);
				m_outdated_dialog->ShowModal();
				m_outdated_dialog->Destroy();
				/*				wxMessageDialog *m_outdated_dialog = new wxMessageDialog(NULL, g_outdated_srcport[l_loop_var]->updateinfo, wxT("Outdated executable detected"), !g_outdated_srcport[l_loop_var]->extrapatch ? wxOK|wxCANCEL|wxICON_EXCLAMATION : wxYES_NO|wxCANCEL|wxICON_EXCLAMATION);

				if (!g_outdated_srcport[l_loop_var]->extrapatch)
				m_outdated_dialog->SetOKCancelLabels(wxT("Download ") + g_outdated_srcport[l_loop_var]->patch_filename, wxT("Close"));
				else
				m_outdated_dialog->SetYesNoCancelLabels(wxT("Download ") + g_outdated_srcport[l_loop_var]->patch_filename, wxT("Download ") + g_outdated_srcport[l_loop_var]->extrapatch_filename, wxT("Close"));

				while ((l_outdated_dialog_id = m_outdated_dialog->ShowModal()) != wxID_CANCEL)
				{
				if (l_outdated_dialog_id == wxID_OK || l_outdated_dialog_id == wxID_YES)
				g_OpenURL(g_outdated_srcport[l_loop_var]->patch_address);
				else if (l_outdated_dialog_id == wxID_NO)
				g_OpenURL(g_outdated_srcport[l_loop_var]->extrapatch_address);
				}*/
			}
			else
				wxMessageBox(wxT("Unknown/unsupported executable set for ") + g_srcport_crc[src_port]->srcportname + wxT("\n\nPlease, reinstall the game from your original CD, and try again."), wxT("Unknown/unsupported executable detected"), wxOK|wxICON_EXCLAMATION);
		}
	}
	else
		wxMessageBox(wxT("Invalid pathname/filename set for ") + g_srcport_crc[src_port]->srcportname, wxT("Invalid pathname/filename detected"), wxOK|wxICON_ERROR);

	return false;
}

size_t g_GetBloodLevelSelectionByNum(long blood_level)
{
	if ((blood_level > 100) && (blood_level <= 108)) // Episode 1 level?
		return (blood_level % 100 + 1);
	if ((blood_level > 200) && (blood_level <= 209))
		return (blood_level % 200 + 10);
	if ((blood_level > 300) && (blood_level <= 308))
		return (blood_level % 300 + 20);
	if ((blood_level > 400) && (blood_level <= 409))
		return (blood_level % 400 + 29);
	if ((blood_level > 500) && (blood_level <= 511))
		return (blood_level % 500 + 39);
	if ((blood_level > 600) && (blood_level <= 609))
		return (blood_level % 600 + 51);
	return 0;
}

size_t g_GetBloodCPLevelSelectionByNum(long blood_level)
{
	if ((blood_level > 100) && (blood_level <= 110)) // Episode 1 level?
		return (blood_level % 100 + 1);
	if ((blood_level > 200) && (blood_level <= 204))
		return (blood_level % 200 + 12);
	return 0;
}

size_t g_GetDN3DLevelSelectionByNum(long dn3d_level)
{
	if ((dn3d_level > 100) && (dn3d_level <= 107)) // Episode 1 level?
		return (dn3d_level % 100 + 1);
	if ((dn3d_level > 200) && (dn3d_level <= 411)
		&& (dn3d_level % 100 > 0) && (dn3d_level % 100 <= 11))
		return ((dn3d_level / 100) * 12 + dn3d_level % 100 - 15);
	return 0;
}

size_t g_GetSWLevelSelectionByNum(long sw_level)
{
	if ((sw_level > 0) && (sw_level <= 4))
		return (sw_level+1);
	if ((sw_level > 4) && (sw_level <= 22))
		return (sw_level+2);
	if ((sw_level > 22) && (sw_level <= 26))
		return (sw_level+3);
	if ((sw_level > 26) && (sw_level <= 28))
		return (sw_level+4);
	return 0;
}

size_t g_GetBloodLevelNumBySelection(long blood_selection)
{
	if ((blood_selection >= 2) && (blood_selection <= 9))
		return (blood_selection+99);
	if ((blood_selection >= 11) && (blood_selection <= 19))
		return (blood_selection+190);
	if ((blood_selection >= 21) && (blood_selection <= 28))
		return (blood_selection+280);
	if ((blood_selection >= 30) && (blood_selection <= 38))
		return (blood_selection+371);
	if ((blood_selection >= 40) && (blood_selection <= 50))
		return (blood_selection+461);
	if ((blood_selection >= 52) && (blood_selection <= 60))
		return (blood_selection+549);
	return 0;
}

size_t g_GetBloodCPLevelNumBySelection(long blood_selection)
{
	if ((blood_selection >= 2) && (blood_selection <= 11))
		return (blood_selection+99);
	if ((blood_selection >= 13) && (blood_selection <= 16))
		return (blood_selection+188);
	return 0;
}

size_t g_GetDN3DLevelNumBySelection(long dn3d_selection)
{
	if ((dn3d_selection >= 2) && (dn3d_selection <= 8))
		return (dn3d_selection+99); // So E1L4 becomes 104, for instance.
	if ((dn3d_selection >= 10) && (dn3d_selection != 21) && (dn3d_selection != 33))
		return ((dn3d_selection - 9) % 12 + ((dn3d_selection + 15) / 12) * 100);
	return 0;
}

size_t g_GetSWLevelNumBySelection(long sw_selection)
{
	if ((sw_selection >= 2) && (sw_selection <= 5))
		return (sw_selection-1);
	if ((sw_selection >= 7) && (sw_selection <= 24))
		return (sw_selection-2);
	if ((sw_selection >= 26) && (sw_selection <= 29))
		return (sw_selection-3);
	if ((sw_selection >= 31) && (sw_selection <= 32))
		return (sw_selection-4);
	return 0;
}

void l_ShrinkFilesToNamesOnly(wxArrayString *file_list)
{
	size_t i, l_temp_num_strings = file_list->GetCount();
	for (i = 0; i < l_temp_num_strings; i++)
		(*file_list)[i] = wxFileName((*file_list)[i]).GetFullName();
}

int g_compareNoCase(const wxString& first, const wxString& second)
{
	return first.CmpNoCase(second);
}

void g_AddAllBloodMapFiles(wxArrayString *file_list)
{
	size_t l_loop_var;
	if (wxDir::Exists(g_configuration->blood_maps_dir))
	{
		wxDir::GetAllFiles(g_configuration->blood_maps_dir, file_list, wxEmptyString, wxDIR_FILES);
		l_loop_var = 0;
		while (l_loop_var < file_list->GetCount())
			if (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".map"), false))
				l_loop_var++;
			else
				file_list->RemoveAt(l_loop_var);
		l_ShrinkFilesToNamesOnly(file_list);
		file_list->Sort(g_compareNoCase);
	}
}

void g_AddAllDescentMapFiles(wxArrayString *file_list, bool msn)
{
	size_t l_loop_var = 0;
	if (wxDir::Exists(g_configuration->descent_maps_dir))
	{
		wxDir::GetAllFiles(g_configuration->descent_maps_dir, file_list, wxEmptyString, wxDIR_FILES);
		l_ShrinkFilesToNamesOnly(file_list);

		while (l_loop_var < file_list->GetCount())
			if ((((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".hog"), false)
				|| ((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".rdl"), false)
				|| (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".msn"), false) && msn))
				&& !((*file_list)[l_loop_var]).IsSameAs(wxT("DESCENT.HOG"), false)
				&& !((*file_list)[l_loop_var]).IsSameAs(wxT("CHAOS.HOG"), false)
				&& !((*file_list)[l_loop_var]).IsSameAs(wxT("CHAOS.MSN"), false))
				l_loop_var++;
			else
				file_list->RemoveAt(l_loop_var);
		file_list->Sort(g_compareNoCase);
	}
}

void g_AddAllDescent2MapFiles(wxArrayString *file_list, bool mn2)
{
	size_t l_loop_var = 0;
	if (wxDir::Exists(g_configuration->descent2_maps_dir))
	{
		wxDir::GetAllFiles(g_configuration->descent2_maps_dir, file_list, wxEmptyString, wxDIR_FILES);
		l_ShrinkFilesToNamesOnly(file_list);

		while (l_loop_var < file_list->GetCount())
			if ((((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".hog"), false)
				|| ((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".rl2"), false)
				|| (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".mn2"), false) && mn2))
				&& !((*file_list)[l_loop_var]).IsSameAs(wxT("DESCENT2.HOG"), false)
				&& !((*file_list)[l_loop_var]).IsSameAs(wxT("D2-2PLYR.HOG"), false)
				&& !((*file_list)[l_loop_var]).IsSameAs(wxT("D2-2PLYR.MN2"), false)
				&& !((*file_list)[l_loop_var]).IsSameAs(wxT("D2CHAOS.HOG"), false)
				&& !((*file_list)[l_loop_var]).IsSameAs(wxT("D2CHAOS.MN2"), false)
				&& !((*file_list)[l_loop_var]).IsSameAs(wxT("D2X.HOG"), false)
				&& !((*file_list)[l_loop_var]).IsSameAs(wxT("D2X.MN2"), false))
				l_loop_var++;
			else
				file_list->RemoveAt(l_loop_var);
		file_list->Sort(g_compareNoCase);
	}
}

void g_AddAllDN3DMapFiles(wxArrayString *file_list)
{
	// Although we could provide the file extension to GetAllFiles, we don't do this
	// because some operating systems are case-sensitive, and some aren't!
	size_t l_loop_var;
	if (wxDir::Exists(g_configuration->dn3d_maps_dir))
	{
		wxDir::GetAllFiles(g_configuration->dn3d_maps_dir, file_list, wxEmptyString, wxDIR_FILES);
		l_loop_var = 0;
		while (l_loop_var < file_list->GetCount())
			if (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".map"), false))
				l_loop_var++;
			else
				file_list->RemoveAt(l_loop_var);
		l_ShrinkFilesToNamesOnly(file_list);
		file_list->Sort(g_compareNoCase);
	}
}

void g_AddAllSWMapFiles(wxArrayString *file_list)
{
	size_t l_loop_var;
	if (wxDir::Exists(g_configuration->sw_maps_dir))
	{
		wxDir::GetAllFiles(g_configuration->sw_maps_dir, file_list, wxEmptyString, wxDIR_FILES);
		l_loop_var = 0;
		while (l_loop_var < file_list->GetCount())
			if (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".map"), false))
				l_loop_var++;
			else
				file_list->RemoveAt(l_loop_var);
		l_ShrinkFilesToNamesOnly(file_list);
		file_list->Sort(g_compareNoCase);
	}
}

void g_AddAllDemoFiles(wxArrayString *file_list, const wxString& demo_dir)
{
	size_t l_loop_var;
	if (wxDir::Exists(demo_dir))
	{
		wxDir::GetAllFiles(demo_dir, file_list, wxEmptyString, wxDIR_FILES);
		l_loop_var = 0;
		while (l_loop_var < file_list->GetCount())
			if (((*file_list)[l_loop_var].Right(4)).IsSameAs(wxT(".dmo"), false))
				l_loop_var++;
			else
				file_list->RemoveAt(l_loop_var);
		l_ShrinkFilesToNamesOnly(file_list);
		file_list->Sort(g_compareNoCase);
	}
}

bool g_FindDescentMSNfile(wxString *extra_usermap, const wxString& usermap)
{
	size_t l_loop_var = 0;
	wxArrayString file_list;

	if (wxDir::Exists(g_configuration->descent_maps_dir))
	{
		wxDir::GetAllFiles(g_configuration->descent_maps_dir, &file_list, wxEmptyString, wxDIR_FILES);
		l_ShrinkFilesToNamesOnly(&file_list);

		while (l_loop_var < file_list.GetCount())
		{
			if (file_list[l_loop_var].IsSameAs(usermap.BeforeLast('.') + wxT(".msn"), false))
			{
				*extra_usermap = file_list[l_loop_var];
				return true;
			}

			l_loop_var++;
		}
	}

	*extra_usermap = wxEmptyString;
	return false;
}

bool g_FindDescentMN2file(wxString *extra_usermap, const wxString& usermap)
{
	size_t l_loop_var = 0;
	wxArrayString file_list;

	if (wxDir::Exists(g_configuration->descent2_maps_dir))
	{
		wxDir::GetAllFiles(g_configuration->descent2_maps_dir, &file_list, wxEmptyString, wxDIR_FILES);
		l_ShrinkFilesToNamesOnly(&file_list);

		while (l_loop_var < file_list.GetCount())
		{
			if (file_list[l_loop_var].IsSameAs(usermap.BeforeLast('.') + wxT(".mn2"), false))
			{
				*extra_usermap = file_list[l_loop_var];
				return true;
			}

			l_loop_var++;
		}
	}

	*extra_usermap = wxEmptyString;
	return false;
}

void g_OpenURL(const wxString& url)
{
	if (g_configuration->override_browser)
		wxExecute(wxT('"') + g_configuration->browser_exec + wxT("\" \"") + url + wxT('"'));
	else
		wxLaunchDefaultBrowser(url);
}

void g_PlaySound(const wxString& filename)
{
	if (g_configuration->override_soundcmd)
		wxExecute(wxT('"') + g_configuration->playsound_cmd + wxT("\" \"") + filename + wxT('"'));
	else
		wxSound::Play(filename);
}
/*
#ifdef __WXMSW__
#define l_ConvToLocalExec(fullfilename) (((wxFileName)fullfilename).GetFullName())
#else
#define l_ConvToLocalExec(fullfilename) (wxT("./")+((wxFileName)fullfilename).GetFullName())
#endif
*/
wxString l_GetAppLocalCmd(const wxString& appname)
{
#ifdef __WXMSW__
	return wxFileName(appname).GetFullName();
#else
	wxString l_localname = wxFileName(appname).GetFullName();
#if (defined __WXMAC__) || (defined __WXCOCOA__)
	if (wxDir::Exists(appname) &&
		(l_localname.Len() >= 4) && l_localname.Right(4).IsSameAs(wxT(".app"), false))
	{
		wxString l_actual_exe = wxDir::FindFirst(appname + wxT("/Contents/MacOS"), wxEmptyString);
		if (!l_actual_exe.IsEmpty())
			return wxT("./") + l_localname + wxT("/Contents/MacOS/") + wxFileName(l_actual_exe).GetFullName();
	}
#endif
	return wxT("./") + l_localname;
#endif
}

wxString l_GetPathWithNoTrailingSlash(wxString path)
{
#ifdef __WXMSW__
	while (!path.IsEmpty() && (path.Last() == '\\' || path.Last() == '/'))
#else
	while (!path.IsEmpty() && path.Last() == '/')
#endif
		path.RemoveLast();
	return path;
}

void l_create_origfilespath(const wxString& game_origfilespath, bool& use_origfilespath, wxTextFile& settings_file)
{
	if (!use_origfilespath)
	{
		use_origfilespath = true;
#ifdef __WXMSW__
		settings_file.AddLine(wxT("md \"") + game_origfilespath + wxT('"'));
#else
		settings_file.AddLine(wxT("mkdir -p \"") + game_origfilespath + wxT('"'));
#endif
	}
}

wxString l_RelativeToAbsolutePath(wxString path)
{
	wxFileName temp_path;
	temp_path = wxFileName(l_GetPathWithNoTrailingSlash(path));

	if (temp_path.IsRelative())
	{
		temp_path.MakeAbsolute();
	}

	return temp_path.GetFullPath();
}

bool g_MakeLaunchScript(bool mp_game,
						const wxString& script_filename,
						RoomPlayersTable* players_table,
						size_t player_index,
						GameType game,
						SourcePortType src_port,
						bool use_crypticpassage,
						size_t episode_map,
						const wxString& user_map,
						bool launch_usermap,
						BloodMPGameT_Type bloodgametype,
						DN3DMPGameT_Type dn3dgametype,
						SWMPGameT_Type swgametype,
						size_t num_fake_players,
						bool have_bot_ai,
						size_t skill_num,
						BloodMPMonsterType blood_monster,
						BloodMPWeaponType blood_weapon,
						BloodMPItemType blood_item,
						BloodMPRespawnType blood_respawn,
						DN3DSpawnType spawn,
						bool play_demo,
						const wxString& playdemo_filename,
						bool record_demo,
						const wxString& recorddemo_filename,
						size_t connectiontype,
						size_t rxdelay,
						bool use_tcmod,
						const wxString& extra_args)
{
	size_t l_temp_num, l_loop_var;
	wxArrayString l_str_list;
	wxString extra_usermap;
	wxString l_game_fullcmd, l_game_userpath, l_temp_str, l_game_origfilespath,
		l_game_execpath = wxFileName(g_configuration->dosbox_exec).GetPath(); // Sort of a default.
	bool l_dos_game = false, l_add_searchpath = false, l_use_origfilespath = false, l_is_bat;
	wxTextFile l_settings_file;
	// EDuke32 now accepts only args like -l1 and -v1,
	// while other Duke3D variants (e.g. DOS Duke) may accept only /l1 and /v1.
	// Thus, store " /" for now and change later if needed. Space is always used.
	wxString l_arg_prefix_str = wxT(" /");

	// First of all, a room for some per-srcport settings...
	// Including the actual game execution, and setting
	// the path for temporary files.
	switch (src_port)
	{
		case SOURCEPORT_DOSBLOODSW:
			if (players_table)
				l_game_fullcmd = wxT("COMMIT.EXE");
			else
				l_game_fullcmd = ((wxFileName)g_configuration->dosbloodsw_exec).GetFullName();
			l_game_userpath = ((wxFileName)g_configuration->dosbloodsw_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_DOSBLOODRG:
			if (players_table)
				l_game_fullcmd = wxT("COMMIT.EXE");
			else
			{
				if (!use_crypticpassage)
					l_game_fullcmd = ((wxFileName)g_configuration->dosbloodrg_exec).GetFullName();
				else
					l_game_fullcmd = wxT("CRYPTIC.EXE");
			}
			l_game_userpath = ((wxFileName)g_configuration->dosbloodrg_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_DOSBLOODPP:
			if (players_table)
				l_game_fullcmd = wxT("COMMIT.EXE");
			else
			{
				if (!use_crypticpassage)
					l_game_fullcmd = ((wxFileName)g_configuration->dosbloodpp_exec).GetFullName();
				else
					l_game_fullcmd = wxT("CRYPTIC.EXE");
			}
			l_game_userpath = ((wxFileName)g_configuration->dosbloodpp_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_DOSBLOODOU:
			if (players_table)
				l_game_fullcmd = wxT("COMMIT.EXE");
			else
			{
				if (!use_crypticpassage)
					l_game_fullcmd = ((wxFileName)g_configuration->dosbloodou_exec).GetFullName();
				else
					l_game_fullcmd = wxT("CRYPTIC.EXE");
			}
			l_game_userpath = ((wxFileName)g_configuration->dosbloodou_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_DOSDESCENT:
			l_game_fullcmd = ((wxFileName)g_configuration->dosdescent_exec).GetFullName();
			l_game_userpath = ((wxFileName)g_configuration->dosdescent_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_D1XREBIRTH:
			l_game_fullcmd = g_configuration->d1xrebirth_exec;
			l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
			l_game_userpath = l_game_execpath;
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(l_game_fullcmd) + wxT('"');
			break;
		case SOURCEPORT_DOSDESCENT2:
			l_game_fullcmd = ((wxFileName)g_configuration->dosdescent2_exec).GetFullName();
			l_game_userpath = ((wxFileName)g_configuration->dosdescent2_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_D2XREBIRTH:
			l_game_fullcmd = g_configuration->d2xrebirth_exec;
			l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
			l_game_userpath = l_game_execpath;
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(l_game_fullcmd) + wxT('"');
			break;
		case SOURCEPORT_DOSDUKESW:
			if (players_table)
				l_game_fullcmd = wxT("COMMIT.EXE");
			else
				l_game_fullcmd = ((wxFileName)g_configuration->dosdukesw_exec).GetFullName();
			l_game_userpath = ((wxFileName)g_configuration->dosdukesw_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_DOSDUKERG:
			if (players_table)
				l_game_fullcmd = wxT("COMMIT.EXE");
			else
				l_game_fullcmd = ((wxFileName)g_configuration->dosdukerg_exec).GetFullName();
			l_game_userpath = ((wxFileName)g_configuration->dosdukerg_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_DOSDUKEAE:
			if (players_table)
				l_game_fullcmd = wxT("COMMIT.EXE");
			else
				l_game_fullcmd = ((wxFileName)g_configuration->dosdukeae_exec).GetFullName();
			l_game_userpath = ((wxFileName)g_configuration->dosdukeae_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_DUKE3DW:
			l_game_fullcmd = g_configuration->duke3dw_exec;
			l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
			if (g_configuration->duke3dw_userpath_use)
				l_game_userpath = g_configuration->duke3dw_userpath;
			else
				l_game_userpath = l_game_execpath;
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(l_game_fullcmd) + wxT('"');
			l_add_searchpath = true;
			break;
		case SOURCEPORT_EDUKE32:
			l_arg_prefix_str = wxT(" -");
			l_game_fullcmd = g_configuration->eduke32_exec;
			l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
			if (g_configuration->eduke32_userpath_use)
				l_game_userpath = g_configuration->eduke32_userpath;
			else
				l_game_userpath = l_game_execpath;
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(l_game_fullcmd) + wxT('"');
			l_add_searchpath = true;
			//                                l_game_fullcmd = wxT('"') + l_game_fullcmd + wxT('"');
			break;
		case SOURCEPORT_NDUKE:
			l_game_fullcmd = g_configuration->nduke_exec;
			l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
			if (g_configuration->nduke_userpath_use)
				l_game_userpath = g_configuration->nduke_userpath;
			else
				l_game_userpath = l_game_execpath;
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(l_game_fullcmd) + wxT('"');
			break;
		case SOURCEPORT_HDUKE:
			l_game_fullcmd = g_configuration->hduke_exec;
			l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
			if (g_configuration->hduke_userpath_use)
				l_game_userpath = g_configuration->hduke_userpath;
			else
				l_game_userpath = l_game_execpath;
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(l_game_fullcmd) + wxT('"');
			break;
		case SOURCEPORT_XDUKE:
			l_game_fullcmd = g_configuration->xduke_exec;
			l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
			l_game_userpath = l_game_execpath;
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(l_game_fullcmd) + wxT('"');
			break;
		case SOURCEPORT_DOSSWSW:
			if (players_table)
				l_game_fullcmd = wxT("COMMIT.EXE");
			else
				l_game_fullcmd = ((wxFileName)g_configuration->dosswsw_exec).GetFullName();
			l_game_userpath = ((wxFileName)g_configuration->dosswsw_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_DOSSWRG:
			if (players_table)
				l_game_fullcmd = wxT("COMMIT.EXE");
			else
				l_game_fullcmd = ((wxFileName)g_configuration->dosswrg_exec).GetFullName();
			l_game_userpath = ((wxFileName)g_configuration->dosswrg_exec).GetPath();
			l_dos_game = true;
			break;
		case SOURCEPORT_VOIDSW:
			l_game_fullcmd = g_configuration->voidsw_exec;
			l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
			if (g_configuration->voidsw_userpath_use)
				l_game_userpath = g_configuration->voidsw_userpath;
			else
				l_game_userpath = l_game_execpath;
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(l_game_fullcmd) + wxT('"');
			break;
		case SOURCEPORT_SWP:
			l_game_fullcmd = g_configuration->swp_exec;
			l_game_execpath = ((wxFileName)l_game_fullcmd).GetPath();
			l_game_userpath = l_game_execpath;
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(l_game_fullcmd) + wxT('"');
			l_add_searchpath = true;
			break;
		case SOURCEPORT_CUSTOM:
			g_configuration->custom_profile_current = g_configuration->custom_profile_list;

			if (!mp_game)
				while (g_configuration->custom_profile_current->profilename != g_configuration->gamelaunch_custom_profilename_in_sp)
					g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
			else if (!player_index)
				while (g_configuration->custom_profile_current->profilename != g_configuration->gamelaunch_custom_profilename)
					g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;
			else
				while (g_configuration->custom_profile_current->profilename != g_configuration->gamelaunch_customselect_profilename && g_configuration->custom_profile_current->next)
					g_configuration->custom_profile_current = g_configuration->custom_profile_current->next;

			if (players_table || (g_configuration->custom_profile_current->ingamesupport && mp_game))
			{
				l_game_fullcmd = ((wxFileName)g_configuration->custom_profile_current->mpexecutable).GetFullName();
				l_game_userpath = ((wxFileName)g_configuration->custom_profile_current->mpexecutable).GetPath();
			}
			else
			{
				l_game_fullcmd = ((wxFileName)g_configuration->custom_profile_current->spexecutable).GetFullName();
				l_game_userpath = ((wxFileName)g_configuration->custom_profile_current->spexecutable).GetPath();
			}

			l_dos_game = true;
			break;
		default: ;
	}
	// Remove trailing slash if any
	l_game_userpath = l_GetPathWithNoTrailingSlash(l_game_userpath);

	for (l_loop_var = 0; l_loop_var < 9; l_loop_var++)
		if (!wxDirExists(l_game_userpath + wxFILE_SEP_PATH + wxString::Format(wxT("tempyangdir%u"), (unsigned)l_loop_var)))
		{
			l_game_origfilespath = l_game_userpath + wxFILE_SEP_PATH + wxString::Format(wxT("tempyangdir%u"), (unsigned)l_loop_var);
			break;
		}

	if (l_game_fullcmd.Right(1).IsSameAs(wxT('"'), false))
		l_is_bat = l_game_fullcmd.Len() >= 5 && l_game_fullcmd.Right(5).IsSameAs(wxT(".bat\""), false);
	else
		l_is_bat = l_game_fullcmd.Len() >= 4 && l_game_fullcmd.Right(4).IsSameAs(wxT(".bat"), false);

#ifdef __WXMSW__
	if (!l_dos_game && l_is_bat)
		l_game_fullcmd.Prepend(wxT("call "));
#else
	// If the command ends with an EXE suffix and it's not a DOS game,
	// we should use Wine if it's set to be used.
	// It should then end with .exe" (including double-quotes);
	// DOS apps should not have the path and not need quotes anyway,
	// so we would "catch" them anyway.
	if (l_game_fullcmd.Len() >= 5 && l_game_fullcmd.Right(5).IsSameAs(wxT(".exe\""), false)
		&& g_configuration->have_wine)
		l_game_fullcmd = g_configuration->wine_exec + wxT(' ') + l_game_fullcmd;
#endif

	// Next step: Command line arguments.
	if (players_table || (src_port == SOURCEPORT_EDUKE32 && connectiontype == CONNECTIONTYPE_SERVERCLIENT && !player_index))
	{
		if (game == GAME_BLOOD || game == GAME_DN3D || game == GAME_SW)
			l_game_fullcmd << (game == GAME_BLOOD ? wxT(" -pname ") : wxT(" -name ")) << g_configuration->game_nickname;

		if (src_port == SOURCEPORT_DUKE3DW)
			l_game_fullcmd << wxT(" /col ") << l_duke3dw_player_col_map[g_configuration->gamelaunch_dn3d_playercolnum];
		else if (src_port == SOURCEPORT_SWP)
			l_game_fullcmd << wxT(" -col ") << g_configuration->gamelaunch_sw_playercolnum;
	}

	// Game-specific.
	switch (game)
	{
		case GAME_BLOOD:
			if (bloodgametype)
			{
				if (!player_index)
					l_game_fullcmd << wxT(" -getopt network.ini");
				else
					l_game_fullcmd << wxT(" -auto network.ini");

				if (connectiontype == PACKETMODE_BROADCAST)
					l_game_fullcmd << wxT(" -broadcast");
				else if (connectiontype == PACKETMODE_MASTERSLAVE)
					l_game_fullcmd << wxT(" -masterslave");
			}
			else if (launch_usermap)
				l_game_fullcmd << wxT(" -map ") << user_map;

			if (record_demo)
				l_game_fullcmd << wxT(" -record") << recorddemo_filename;

			break;

		case GAME_DN3D:
			if (use_tcmod)
			{
				g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_list;

				if (!player_index)
					while (g_configuration->dn3dtcmod_profile_current->profilename != g_configuration->gamelaunch_dn3dtcmod_profilename)
						g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;
				else
					while (g_configuration->dn3dtcmod_profile_current->profilename != g_configuration->gamelaunch_dn3dtcmodselect_profilename && g_configuration->dn3dtcmod_profile_current->next)
						g_configuration->dn3dtcmod_profile_current = g_configuration->dn3dtcmod_profile_current->next;
			}

			if (l_add_searchpath)
			{
				if (launch_usermap)
				{
					l_game_fullcmd << l_arg_prefix_str << wxT("j\"") << l_RelativeToAbsolutePath(g_configuration->dn3d_maps_dir) +wxT('"');
				}

				if (use_tcmod)
				{
					l_temp_num = g_configuration->dn3dtcmod_profile_current->modfiles.GetCount();

					for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
					{
						l_temp_str = ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetPath();

						if (l_str_list.Index(l_temp_str) == wxNOT_FOUND)
						{
							l_game_fullcmd << l_arg_prefix_str << wxT("j\"") << l_temp_str + wxT('"');

							l_str_list.Add(l_temp_str);
						}
					}
				}
			}

			// Play a demo?
			if (play_demo)
				l_game_fullcmd << l_arg_prefix_str << wxT('d') << playdemo_filename;
			else // Play some game.
			{
				// The first or zero-index selection is 1.
				// And the total amount of players is the number of bots plus 1.
				if (dn3dgametype && !players_table)
				{
					l_game_fullcmd << wxT(" -name ") << g_configuration->game_nickname
						<< l_arg_prefix_str << wxT('q') << num_fake_players+1;

					if (have_bot_ai)
						l_game_fullcmd << l_arg_prefix_str << wxT('a');
				}

				if (record_demo)
					l_game_fullcmd << l_arg_prefix_str << wxT('r');

				if (!(src_port == SOURCEPORT_EDUKE32 && connectiontype == CONNECTIONTYPE_SERVERCLIENT && player_index))
				{
					if (!skill_num)
						l_game_fullcmd << l_arg_prefix_str << wxT('m');
					else // For respawning enemies, use Spawn setting.
						l_game_fullcmd << l_arg_prefix_str << wxT('s') << skill_num;
				}

				if ((!launch_usermap) && episode_map)
					l_game_fullcmd << l_arg_prefix_str << wxT('l') << (episode_map % 100) << l_arg_prefix_str << wxT('v') << (episode_map / 100);

				if (launch_usermap)
					l_game_fullcmd << wxT(" -map ") << user_map;
			}

			// May be required for both a real game and a demo.
			if (!(src_port == SOURCEPORT_EDUKE32 && connectiontype == CONNECTIONTYPE_SERVERCLIENT && player_index))
			{
				if (players_table || dn3dgametype) // Multiplayer or fake multiplayer?
					l_game_fullcmd << l_arg_prefix_str << wxT('c') << (src_port == SOURCEPORT_DOSDUKESW || src_port == SOURCEPORT_DOSDUKERG ? dn3dgametype-1 : dn3dgametype);

				if (spawn == DN3DSPAWN_ALL)
					l_game_fullcmd << l_arg_prefix_str << wxT("tx");
				else if (spawn != DN3DSPAWN_NONE)
					l_game_fullcmd << l_arg_prefix_str << wxT('t') << spawn;
			}

			if (use_tcmod)
			{
				// CON and DEF files at first. DEF - In EDuke32 and Duke3dw only.
				if (!g_configuration->dn3dtcmod_profile_current->confile.IsEmpty())
					l_game_fullcmd << l_arg_prefix_str << wxT('x') << g_configuration->dn3dtcmod_profile_current->confile;

				if ((src_port == SOURCEPORT_EDUKE32 || src_port == SOURCEPORT_DUKE3DW) && !g_configuration->dn3dtcmod_profile_current->deffile.IsEmpty())
					l_game_fullcmd << l_arg_prefix_str << wxT('h') << g_configuration->dn3dtcmod_profile_current->deffile;

				// Afterwards, GRP, PK3 And ZIP files. PK3/ZIP - In EDuke32 and Duke3dw only.
				l_temp_num = g_configuration->dn3dtcmod_profile_current->modfiles.GetCount();

				for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
				{
					l_temp_str = ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName();

					if	(
						( (l_temp_str.Right(4)).IsSameAs(wxT(".grp"), false) ) ||
						( (src_port == SOURCEPORT_EDUKE32 || src_port == SOURCEPORT_DUKE3DW) && ((l_temp_str.Right(4)).IsSameAs(wxT(".zip"), false) || (l_temp_str.Right(4)).IsSameAs(wxT(".pk3"), false)) )
						)
					{
						l_game_fullcmd << l_arg_prefix_str << wxT('g') << l_temp_str;
					}
				}
			}

			break;

		case GAME_SW:
			if (use_tcmod)
			{
				g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_list;

				if (!player_index)
					while (g_configuration->swtcmod_profile_current->profilename != g_configuration->gamelaunch_swtcmod_profilename)
						g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;
				else
					while (g_configuration->swtcmod_profile_current->profilename != g_configuration->gamelaunch_swtcmodselect_profilename && g_configuration->swtcmod_profile_current->next)
						g_configuration->swtcmod_profile_current = g_configuration->swtcmod_profile_current->next;
			}

			if (l_add_searchpath)
			{
				if (launch_usermap)
					l_game_fullcmd << wxT(" -j\"") << l_RelativeToAbsolutePath(g_configuration->sw_maps_dir) + wxT('"');

				if (use_tcmod)
				{
					l_temp_num = g_configuration->swtcmod_profile_current->modfiles.GetCount();

					for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
					{
						l_temp_str = ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetPath();

						if (l_str_list.Index(l_temp_str) == wxNOT_FOUND)
						{
							l_game_fullcmd << wxT(" -j\"") << l_temp_str + wxT('"');

							l_str_list.Add(l_temp_str);
						}
					}
				}
			}

			// Play a demo?
			if (play_demo)
				l_game_fullcmd << wxT(" -dp") << playdemo_filename;
			else // Play some game.
			{
				if (record_demo)
					l_game_fullcmd << wxT(" -dr") << recorddemo_filename;

				// On SWP we have -s0. But no need to apply skill for a client in a multi-player game.
				if ((!skill_num) && (src_port != SOURCEPORT_SWP))
					l_game_fullcmd << wxT(" -monst");
				else if ((src_port != SOURCEPORT_SWP) || (!players_table) || (!player_index))
					l_game_fullcmd << wxT(" -s") << skill_num;
			}

			// On SWP, no need to apply in a multi-player game for a client.
			if ((!launch_usermap) && episode_map &&
				((src_port != SOURCEPORT_SWP) || (!players_table) || (!player_index)))
				l_game_fullcmd << wxT(" -level") << episode_map;

			// May be required for both a real game and a demo.
			// Also, again no need in SWP for a client in a multi-player game.
			if (launch_usermap &&
				((src_port != SOURCEPORT_SWP) || (!players_table) || (!player_index)))
				l_game_fullcmd << wxT(" -map ") << user_map;

			// Supply MP gametype with SWP only, and only for the host.
			if ((src_port == SOURCEPORT_SWP) && players_table && (!player_index))
				l_game_fullcmd << wxT(" -netgame ") << swgametype;

			if (use_tcmod)
			{
				if (src_port == SOURCEPORT_VOIDSW || src_port == SOURCEPORT_SWP)
				{
					// DEF files at first - In VoidSW and SWP only.
					if (!g_configuration->swtcmod_profile_current->deffile.IsEmpty())
						l_game_fullcmd << wxT(" -h") << g_configuration->swtcmod_profile_current->deffile;

					// Afterwards, GRP, PK3 And ZIP files - In VoidSW and SWP only.
					l_temp_num = g_configuration->swtcmod_profile_current->modfiles.GetCount();

					for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
					{
						l_temp_str = ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName();

						if (((l_temp_str.Right(4)).IsSameAs(wxT(".grp"), false))
							|| ((l_temp_str.Right(4)).IsSameAs(wxT(".zip"), false)) || ((l_temp_str.Right(4)).IsSameAs(wxT(".pk3"), false)))
							l_game_fullcmd << wxT(" -g") << l_temp_str;
					}
				}
			}

			break;
		default: ;
	}

	// Finally...
	if (extra_args != wxEmptyString)
		l_game_fullcmd << wxT(' ') << extra_args;

	// Source Port specific parameters for multiplayer.
	if (players_table || (((src_port == SOURCEPORT_EDUKE32 && connectiontype == CONNECTIONTYPE_SERVERCLIENT) || src_port == SOURCEPORT_D1XREBIRTH || src_port == SOURCEPORT_D2XREBIRTH) && !player_index && mp_game))
	{
		switch (src_port)
		{
			case SOURCEPORT_EDUKE32:
//				if (g_configuration->gamelaunch_enable_natfree)
//					l_game_fullcmd << wxT(" -stun");

			case SOURCEPORT_DUKE3DW:
			case SOURCEPORT_VOIDSW:
			case SOURCEPORT_SWP:

				switch (connectiontype)
				{
					case CONNECTIONTYPE_PEER2PEER:
/*						l_game_fullcmd << wxT(" /net /p") << players_table->players[player_index].game_port_number;
						for (l_loop_var = 0; l_loop_var < player_index; l_loop_var++)
							l_game_fullcmd << wxT(' ') << players_table->players[l_loop_var].ingameip
							<< wxT(':') << players_table->players[l_loop_var].game_port_number;
						l_game_fullcmd << wxT(" /n1");
						for (l_loop_var = player_index+1; l_loop_var < players_table->num_players; l_loop_var++)
							l_game_fullcmd << wxT(' ') << players_table->players[l_loop_var].ingameip
							<< wxT(':') << players_table->players[l_loop_var].game_port_number;
*/
						l_game_fullcmd << wxT(" /net");
						if (src_port != SOURCEPORT_SWP)
							l_game_fullcmd << wxT(" /p") << players_table->players[player_index].game_port_number;
						for (l_loop_var = 0; l_loop_var < player_index; l_loop_var++)
						{
							l_game_fullcmd << wxT(' ') << players_table->players[l_loop_var].ingameip;
							if (src_port != SOURCEPORT_VOIDSW && src_port != SOURCEPORT_SWP)
								l_game_fullcmd << wxT(':') << players_table->players[l_loop_var].game_port_number;
						}
						l_game_fullcmd << wxT(" /n1");
						for (l_loop_var = player_index+1; l_loop_var < players_table->num_players; l_loop_var++)
						{
							l_game_fullcmd << wxT(' ') << players_table->players[l_loop_var].ingameip;
							if (src_port != SOURCEPORT_VOIDSW && src_port != SOURCEPORT_SWP)
								l_game_fullcmd << wxT(':') << players_table->players[l_loop_var].game_port_number;
						}

						break;

					case CONNECTIONTYPE_MASTERSLAVE:
						if (player_index) // A client.
							l_game_fullcmd << wxT(" /net /n0 ") << players_table->players[0].ingameip
							<< wxT(':') << players_table->players[0].game_port_number
							<< wxT(" /p") << players_table->players[player_index].game_port_number;
						else // A server.
							l_game_fullcmd << wxT(" /net /n0:") << players_table->num_players
							<< wxT(" /p") << players_table->players[0].game_port_number;

						break;

					case CONNECTIONTYPE_SERVERCLIENT:
						if (player_index) // A client.
							l_game_fullcmd << wxT(" -connect ") << players_table->players[0].ingameip
							<< wxT(':') << players_table->players[0].game_port_number;
						else // A server.
							l_game_fullcmd << wxT(" -port ") << g_configuration->game_port_number << wxT(" -server");

						break;
					default: ;
				}

				break;

			case SOURCEPORT_XDUKE:
//				if (g_configuration->gamelaunch_enable_natfree)
//					l_game_fullcmd << wxT(" -stun");

			case SOURCEPORT_NDUKE:
			case SOURCEPORT_HDUKE:
				l_game_fullcmd << wxT(" -net netlist.txt");

				break;

			case SOURCEPORT_D1XREBIRTH:
			case SOURCEPORT_D2XREBIRTH:
				if (player_index)
					l_game_fullcmd << wxT(" -udp_hostaddr ") << players_table->players[0].ingameip
					<< wxT(" -udp_hostport ") << players_table->players[0].game_port_number;

				l_game_fullcmd << wxT(" -udp_myport ") << g_configuration->game_port_number;

				break;
			default: ;
		}
	}

	// Let's try creating the game user folder if it doesn't exist.
	// It's at least required for COMMIT.DAT and netlist.txt.
	
	if (!wxDirExists(l_game_userpath))
		wxFileName::Mkdir(l_game_userpath, 0777, wxPATH_MKDIR_FULL);

	// Now, for the case we'd want to use DOSBox, we'd later change l_game_fullcmd.
	if (l_dos_game)
	{
		if (game == GAME_BLOOD && bloodgametype)
		{
			// Let's create a NETWORK.INI file.
#ifdef __WXMSW__
			l_temp_str = l_game_userpath + wxT("\\NETWORK.INI");
#else
			l_temp_str = l_game_userpath + wxT("/NETWORK.INI");
#endif
			if (((!wxFileExists(l_temp_str)) || (wxRemoveFile(l_temp_str)))
				&& (l_settings_file.Create(l_temp_str)))
			{
				l_settings_file.AddLine(wxT("[options]"));
				l_settings_file.AddLine(wxT("Online = commit"));
				if (!player_index)
				{
					l_settings_file.AddLine(wxString::Format(wxT("GameType=%d"), (int)bloodgametype));
					if ((!launch_usermap) && episode_map)
					{
						l_settings_file.AddLine(wxString::Format(wxT("EpisodeID=%d"), (int)((episode_map / 100)-1)));
						l_settings_file.AddLine(wxString::Format(wxT("LevelID=%d"), (int)((episode_map % 100)-1)));
					}
					l_settings_file.AddLine(wxString::Format(wxT("GameDifficulty=%d"), (int)skill_num));
					l_settings_file.AddLine(wxString::Format(wxT("MonsterSettings=%d"), (int)blood_monster));
					l_settings_file.AddLine(wxString::Format(wxT("WeaponSettings=%d"), (int)blood_weapon));
					l_settings_file.AddLine(wxString::Format(wxT("ItemSettings=%d"), (int)blood_item));
					l_settings_file.AddLine(wxString::Format(wxT("RespawnSettings=%d"), (int)blood_respawn));
					if (launch_usermap)
					{
						l_settings_file.AddLine(wxT("UserMapName = ") + user_map);
						l_settings_file.AddLine(wxT("UserMap = 1"));
					}
				}
				l_settings_file.Write(wxTextFileType_Dos);
				l_settings_file.Close();
			}
		}

		if ((game == GAME_BLOOD || game == GAME_DN3D || game == GAME_SW) && players_table) // Multiplayer?
		{
			// Let's create a COMMIT.DAT file.
#ifdef __WXMSW__
			l_temp_str = l_game_userpath + wxT("\\COMMIT.DAT");
#else
			l_temp_str = l_game_userpath + wxT("/COMMIT.DAT");
#endif
			if (((!wxFileExists(l_temp_str)) || (wxRemoveFile(l_temp_str)))
				&& (l_settings_file.Create(l_temp_str)))
			{
				l_settings_file.AddLine(wxT("[Commit]"));
				l_settings_file.AddLine(wxT(";COMMIT data file"));
				l_settings_file.AddLine(wxT("; "));
				l_settings_file.AddLine(wxT(";This data file defines the configurable options of COMMIT.  Presumably,"));
				l_settings_file.AddLine(wxT(";the user never sees this file, but rather it is setup by another"));
				l_settings_file.AddLine(wxT(";program which launches COMMIT."));
				l_settings_file.AddLine(wxT("; "));
				l_settings_file.AddLine(wxT("; "));
				l_settings_file.AddLine(wxT(";COMMTYPE"));
				l_settings_file.AddLine(wxT("; - SERIAL - 1"));
				l_settings_file.AddLine(wxT("; - MODEM - 2"));
				l_settings_file.AddLine(wxT("; - NETWORK - 3"));
				l_settings_file.AddLine(wxT("; "));
				l_settings_file.AddLine(wxT("; NUMPLAYERS"));
				l_settings_file.AddLine(wxT("; - 0..MAXPLAYERS"));
				l_settings_file.AddLine(wxT("; "));
				l_settings_file.AddLine(wxT("; VECTOR"));
				l_settings_file.AddLine(wxT("; - ~ find an empty one"));
				l_settings_file.AddLine(wxT("; - 0x60..0x69 specify one"));
				l_settings_file.AddLine(wxT("; "));
				l_settings_file.AddLine(wxT("; CONNECTTYPE"));
				l_settings_file.AddLine(wxT("; - DIALMODE - 0"));
				l_settings_file.AddLine(wxT("; - ANSWERMODE - 1"));
				l_settings_file.AddLine(wxT("; - ALREADYCONNECTED - 2"));
				l_settings_file.AddLine(wxT("; "));
				l_settings_file.AddLine(wxT("COMMTYPE = 3"));
				l_settings_file.AddLine(wxString::Format(wxT("NUMPLAYERS = %d"), (int)players_table->num_players));
				l_settings_file.AddLine(wxT("PAUSE = 0"));
				l_settings_file.AddLine(wxT("VECTOR = ~"));
				l_settings_file.AddLine(wxT("SOCKETNUMBER = 34889"));
				l_settings_file.AddLine(wxT("INITSTRING = \"ATZ\""));
				l_settings_file.AddLine(wxT("HANGUPSTRING = \"ATH0=0\""));
				l_settings_file.AddLine(wxT("PHONENUMBER = \"\""));
				switch (src_port)
				{
					case SOURCEPORT_DOSBLOODSW:
						l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
						((wxFileName)g_configuration->dosbloodsw_exec).GetFullName()
						+ wxT('"'));
						break;
					case SOURCEPORT_DOSBLOODRG:
						if (!use_crypticpassage)
							l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
							((wxFileName)g_configuration->dosbloodrg_exec).GetFullName()
							+ wxT('"'));
						else
							l_settings_file.AddLine(wxT("LAUNCHNAME = \"CRYPTIC.EXE\""));
						break;
					case SOURCEPORT_DOSBLOODPP:
						if (!use_crypticpassage)
							l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
							((wxFileName)g_configuration->dosbloodpp_exec).GetFullName()
							+ wxT('"'));
						else
							l_settings_file.AddLine(wxT("LAUNCHNAME = \"CRYPTIC.EXE\""));
						break;
					case SOURCEPORT_DOSBLOODOU:
						if (!use_crypticpassage)
							l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
							((wxFileName)g_configuration->dosbloodou_exec).GetFullName()
							+ wxT('"'));
						else
							l_settings_file.AddLine(wxT("LAUNCHNAME = \"CRYPTIC.EXE\""));
						break;
					case SOURCEPORT_DOSDUKESW:
						l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
						((wxFileName)g_configuration->dosdukesw_exec).GetFullName()
						+ wxT('"'));
						break;
					case SOURCEPORT_DOSDUKERG:
						l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
						((wxFileName)g_configuration->dosdukerg_exec).GetFullName()
						+ wxT('"'));
						break;
					case SOURCEPORT_DOSDUKEAE:
						l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
						((wxFileName)g_configuration->dosdukeae_exec).GetFullName()
						+ wxT('"'));
						break;
					case SOURCEPORT_DOSSWSW:
						l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
						((wxFileName)g_configuration->dosswsw_exec).GetFullName()
						+ wxT('"'));
						break;
					case SOURCEPORT_DOSSWRG:
						l_settings_file.AddLine(wxT("LAUNCHNAME = \"") +
						((wxFileName)g_configuration->dosswrg_exec).GetFullName()
						+ wxT('"'));
						break;
					default: ;
				}
				l_settings_file.AddLine(wxT("CONNECTTYPE = 0"));
				l_settings_file.AddLine(wxT("USETONE = 1"));
				l_settings_file.AddLine(wxT("COMPORT = 2"));
				l_settings_file.AddLine(wxT("IRQNUMBER = 0"));
				l_settings_file.AddLine(wxT("UARTADDRESS = ~"));
				l_settings_file.AddLine(wxT("PORTSPEED = 9600"));
				l_settings_file.AddLine(wxT("SHOWSTATS = 0"));
				// COMMIT.DAT should be read in an emulated DOS environment.
				// Therefore it's important to use the CR-LF kind of newlines!
				l_settings_file.Write(wxTextFileType_Dos);
				l_settings_file.Close();
			}
		}
		l_temp_str = *g_launcher_user_profile_path + wxT("dosyang.conf");
		if (((!wxFileExists(l_temp_str)) || (wxRemoveFile(l_temp_str)))
			&& (l_settings_file.Create(l_temp_str)))
		{
			if (game != GAME_CUSTOM)
			{
				l_settings_file.AddLine(wxT("[dosbox]"));
				l_settings_file.AddLine(wxT("memsize=32"));
			}

			if (game == GAME_CUSTOM && connectiontype && g_configuration->dosbox_use_conf)
			{
				l_settings_file.AddLine(wxT("[serial]"));
				l_settings_file.AddLine(wxT("serial1=dummy"));
			}
			else if (players_table || ((src_port == SOURCEPORT_DOSDESCENT || src_port == SOURCEPORT_DOSDESCENT2 || (game == GAME_CUSTOM && g_configuration->custom_profile_current->ingamesupport)) && mp_game))
			{
				l_settings_file.AddLine(wxT("[ipx]"));
				l_settings_file.AddLine(wxT("ipx=true"));
			}

			l_settings_file.AddLine(wxT("[autoexec]"));
			// Now let's write the actual commands.
			if (players_table || ((src_port == SOURCEPORT_DOSDESCENT || src_port == SOURCEPORT_DOSDESCENT2 || (game == GAME_CUSTOM && g_configuration->custom_profile_current->ingamesupport)) && mp_game))
			{
				if (game == GAME_CUSTOM && connectiontype)
				{
					if (player_index) // A client.
						l_settings_file.AddLine(wxT("serial1 nullmodem server:") + players_table->players[0].ingameip +
						wxString::Format(wxT(" port:%d"), (int)players_table->players[0].game_port_number) +
						(rxdelay != DEFAULT_RXDELAY ? wxString::Format(wxT(" rxdelay:%d"), (int)rxdelay) : wxString(wxEmptyString)));
					else // A server.
						l_settings_file.AddLine(wxString::Format(wxT("serial1 nullmodem port:%d"), (int)g_configuration->game_port_number) +
						(rxdelay != DEFAULT_RXDELAY ? wxString::Format(wxT(" rxdelay:%d"), (int)rxdelay) : wxString(wxEmptyString)));
				}
				else
				{
					if (player_index) // A client.
						// Do this 4 times cause we could miss and just run the game while not being connected...
						// For now no convenient way found to just loop until it's connected.
						for (l_loop_var = 0; l_loop_var < 4; l_loop_var++)
							l_settings_file.AddLine(wxT("ipxnet connect ") + players_table->players[0].ingameip +
							wxString::Format(wxT(" %d"), (int)players_table->players[0].game_port_number));
					else // A server.
						l_settings_file.AddLine(wxString::Format(wxT("ipxnet startserver %d"), (int)g_configuration->game_port_number));
				}
			}

			if (g_configuration->dosbox_use_conf)
			{
				l_settings_file.AddLine(wxT("mount -u c"));

				if (g_configuration->dosbox_cdmount || (game == GAME_CUSTOM && g_configuration->custom_profile_current->cdmount))
					l_settings_file.AddLine(wxT("mount -u d"));

				if ((g_configuration->have_bmouse && (game == GAME_BLOOD || game == GAME_DN3D || game == GAME_SW)) || (game == GAME_CUSTOM && g_configuration->custom_profile_current->usenetbios))
					l_settings_file.AddLine(wxT("mount -u e"));
			}

			// Now let's mount the actual game dir.
			// No need to worry about trailing any slash here.
			l_settings_file.AddLine(wxT("mount c \"") + l_game_userpath + wxT('"'));

			if (game != GAME_CUSTOM)
			{
				if (g_configuration->dosbox_cdmount == CDROMMOUNT_DIR)
					// On Windows, the CD-ROM letter should already have a trailing slash
					// (and it is required by DOSBox, at least with v0.72).
					// On Linux there seems to be no need for that anyway.
					l_settings_file.AddLine(wxT("mount d \"") + g_configuration->dosbox_cdrom_location + wxT("\" -t cdrom"));
				else if (g_configuration->dosbox_cdmount == CDROMMOUNT_IMG)
					// At least on Linux, and with the real CD-ROM device /dev/scd0,
					// it seems like the mount command has to be executed twice.
				{
					l_settings_file.AddLine(wxT("imgmount d \"") + g_configuration->dosbox_cdrom_image + wxT("\" -t cdrom"));
#ifndef __WXMSW__
					l_settings_file.AddLine(wxT("imgmount d \"") + g_configuration->dosbox_cdrom_image + wxT("\" -t cdrom"));
#endif
				}
			}
			else
			{
				if (g_configuration->custom_profile_current->cdmount == CDROMMOUNT_DIR)
					l_settings_file.AddLine(wxT("mount d \"") + g_configuration->custom_profile_current->cdlocation + wxT("\" -t cdrom"));
				else if (g_configuration->custom_profile_current->cdmount == CDROMMOUNT_IMG)
				{
					l_settings_file.AddLine(wxT("imgmount d \"") + g_configuration->custom_profile_current->cdimage + wxT("\" -t cdrom"));
#ifndef __WXMSW__
					l_settings_file.AddLine(wxT("imgmount d \"") + g_configuration->custom_profile_current->cdimage + wxT("\" -t cdrom"));
#endif
				}
			}

			if (g_configuration->have_bmouse && (game == GAME_BLOOD || game == GAME_DN3D || game == GAME_SW))
			{
				l_settings_file.AddLine(wxT("mount e \"") + ((wxFileName)g_configuration->bmouse_exec).GetPath() + wxT('"'));
				l_game_fullcmd.Prepend(wxT("e:\\") + ((wxFileName)g_configuration->bmouse_exec).GetFullName() + wxT(" launch "));
			}
			else if (game == GAME_CUSTOM && g_configuration->custom_profile_current->usenetbios)
			{
				l_settings_file.AddLine(wxT("mount e \"") + ((wxFileName)g_configuration->custom_profile_current->netbiospath).GetPath() + wxT('"'));
				l_settings_file.AddLine(wxT("e:\\") + ((wxFileName)g_configuration->custom_profile_current->netbiospath).GetFullName());
			}

			l_settings_file.AddLine(wxT("c:"));
			// Launch the game!
			if (l_is_bat)
				l_settings_file.AddLine(wxT("call ") + l_game_fullcmd);
			else
				l_settings_file.AddLine(l_game_fullcmd);
			// Don't forget to exit!
			l_settings_file.AddLine(wxT("exit"));
			l_settings_file.Write();
			l_settings_file.Close();
		}
		// The real command would rather be... one of two:
		if (g_configuration->dosbox_use_conf)
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(g_configuration->dosbox_exec) + wxT("\" -conf \"")
			+ g_configuration->dosbox_conf + wxT("\" -conf \"") + l_RelativeToAbsolutePath(l_temp_str) + wxT('"');
		else
			l_game_fullcmd = wxT('"') + l_GetAppLocalCmd(g_configuration->dosbox_exec)
			+ wxT("\" -conf \"") + l_RelativeToAbsolutePath(l_temp_str) + wxT('"');
	}
	// In the case of multiplayer, we should maybe create a netlist.txt file otherwise.
	else if (players_table &&
		((src_port == SOURCEPORT_NDUKE) || (src_port == SOURCEPORT_HDUKE) || (src_port == SOURCEPORT_XDUKE)))
	{
#ifdef __WXMSW__
		l_temp_str = l_game_userpath + wxT("\\netlist.txt");
#else
		l_temp_str = l_game_userpath + wxT("/netlist.txt");
#endif
		if (((!wxFileExists(l_temp_str)) || (wxRemoveFile(l_temp_str)))
			&& (l_settings_file.Create(l_temp_str)))
		{
			l_settings_file.AddLine(wxT("interface ") +
				players_table->players[player_index].ingameip +
				wxString::Format(wxT(":%d"), (int)players_table->players[player_index].game_port_number));
			l_settings_file.AddLine(wxT("mode peer"));
			for (l_loop_var = 0; l_loop_var < player_index; l_loop_var++)
				l_settings_file.AddLine(wxT("allow ") +
				players_table->players[l_loop_var].ingameip +
				wxString::Format(wxT(":%d"), (int)players_table->players[l_loop_var].game_port_number));
			for (l_loop_var = player_index+1; l_loop_var < players_table->num_players; l_loop_var++)
				l_settings_file.AddLine(wxT("allow ") +
				players_table->players[l_loop_var].ingameip +
				wxString::Format(wxT(":%d"), (int)players_table->players[l_loop_var].game_port_number));
			l_settings_file.Write();
			l_settings_file.Close();
		}
	}
	// Now let's try creating the script!
	if (((!wxFileExists(script_filename)) || (wxRemoveFile(script_filename)))
		&& (l_settings_file.Create(script_filename)))
	{
#ifdef __WXMSW__
		l_settings_file.AddLine(wxT("md \"") + l_game_userpath + wxT('"'));
//		l_settings_file.AddLine(wxT("md \"") + l_game_origfilespath + wxT('"'));

		if (src_port == SOURCEPORT_DOSDESCENT2)
			l_settings_file.AddLine(wxT("md \"") + l_game_userpath + wxT("\\MISSIONS\""));
		else if (src_port == SOURCEPORT_D1XREBIRTH || src_port == SOURCEPORT_D2XREBIRTH)
			l_settings_file.AddLine(wxT("md \"") + l_game_userpath + wxT("\\missions\""));
#else
		l_settings_file.AddLine(wxT("#!/bin/sh"));
		l_settings_file.AddLine(wxT("mkdir -p \"") + l_game_userpath + wxT('"'));
//		l_settings_file.AddLine(wxT("mkdir -p \"") + l_game_origfilespath + wxT('"'));

		if (src_port == SOURCEPORT_DOSDESCENT2)
			l_settings_file.AddLine(wxT("mkdir -p \"") + l_game_userpath + wxT("/MISSIONS\""));
		else if (src_port == SOURCEPORT_D1XREBIRTH || src_port == SOURCEPORT_D2XREBIRTH)
			l_settings_file.AddLine(wxT("mkdir -p \"") + l_game_userpath + wxT("/missions\""));
#endif
		// Copy or symlink map file, if used and needed.
		if (launch_usermap)
		{
			if ((game == GAME_BLOOD) && (wxFileName(l_game_userpath) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->blood_maps_dir))))
			{
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + user_map))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->blood_maps_dir + wxT('\\') + user_map) + wxT("\" \"") + l_game_userpath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->blood_maps_dir + wxT('/') + user_map) + wxT("\" \"") + l_game_userpath + wxT('"'));
#endif
			}
			else if ((src_port == SOURCEPORT_DOSDESCENT) && (wxFileName(l_game_userpath) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->descent_maps_dir))))
			{
				g_FindDescentMSNfile(&extra_usermap, user_map);

				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + user_map))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->descent_maps_dir + wxT('\\') + user_map) + wxT("\" \"") + l_game_userpath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->descent_maps_dir + wxT('/') + user_map) + wxT("\" \"") + l_game_userpath + wxT('"'));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + extra_usermap))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + extra_usermap + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + extra_usermap + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->descent_maps_dir + wxT('\\') + extra_usermap) + wxT("\" \"") + l_game_userpath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->descent_maps_dir + wxT('/') + extra_usermap) + wxT("\" \"") + l_game_userpath + wxT('"'));
#endif
			}
			else if ((src_port == SOURCEPORT_D1XREBIRTH) && (wxFileName(l_game_userpath + wxFILE_SEP_PATH + wxT("missions")) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->descent_maps_dir))))
			{
				g_FindDescentMSNfile(&extra_usermap, user_map);

				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + wxT("missions") + wxFILE_SEP_PATH + user_map))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + wxT("missions") + wxFILE_SEP_PATH + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + wxT("missions") + wxFILE_SEP_PATH + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->descent_maps_dir + wxT('\\') + user_map) + wxT("\" \"") + l_game_userpath + wxT("\\missions\""));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->descent_maps_dir + wxT('/') + user_map) + wxT("\" \"") + l_game_userpath + wxT("/missions\""));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + wxT("missions") + wxFILE_SEP_PATH + extra_usermap))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + wxT("missions") + wxFILE_SEP_PATH + extra_usermap + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + wxT("missions") + wxFILE_SEP_PATH + extra_usermap + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->descent_maps_dir + wxT('\\') + extra_usermap) + wxT("\" \"") + l_game_userpath + wxT("\\missions\""));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->descent_maps_dir + wxT('/') + extra_usermap) + wxT("\" \"") + l_game_userpath + wxT("/missions\""));
#endif
			}
			else if ((src_port == SOURCEPORT_DOSDESCENT2) && (wxFileName(l_game_userpath + wxFILE_SEP_PATH + wxT("MISSIONS")) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->descent2_maps_dir))))
			{
				g_FindDescentMN2file(&extra_usermap, user_map);

				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + wxT("MISSIONS") + wxFILE_SEP_PATH + user_map))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + wxT("MISSIONS") + wxFILE_SEP_PATH + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + wxT("MISSIONS") + wxFILE_SEP_PATH + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->descent2_maps_dir + wxT('\\') + user_map) + wxT("\" \"") + l_game_userpath + wxT("\\MISSIONS\""));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->descent2_maps_dir + wxT('/') + user_map) + wxT("\" \"") + l_game_userpath + wxT("/MISSIONS\""));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + wxT("MISSIONS") + wxFILE_SEP_PATH + extra_usermap))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + wxT("MISSIONS") + wxFILE_SEP_PATH + extra_usermap + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + wxT("MISSIONS") + wxFILE_SEP_PATH + extra_usermap + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->descent2_maps_dir + wxT('\\') + extra_usermap) + wxT("\" \"") + l_game_userpath + wxT("\\MISSIONS\""));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->descent2_maps_dir + wxT('/') + extra_usermap) + wxT("\" \"") + l_game_userpath + wxT("/MISSIONS\""));
#endif
			}
			else if ((src_port == SOURCEPORT_D2XREBIRTH) && (wxFileName(l_game_userpath + wxFILE_SEP_PATH + wxT("missions")) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->descent2_maps_dir))))
			{
				g_FindDescentMN2file(&extra_usermap, user_map);

				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + wxT("missions") + wxFILE_SEP_PATH + user_map))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + wxT("missions") + wxFILE_SEP_PATH + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + wxT("missions") + wxFILE_SEP_PATH + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->descent2_maps_dir + wxT('\\') + user_map) + wxT("\" \"") + l_game_userpath + wxT("\\missions\""));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->descent2_maps_dir + wxT('/') + user_map) + wxT("\" \"") + l_game_userpath + wxT("/missions\""));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + wxT("missions") + wxFILE_SEP_PATH + extra_usermap))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + wxT("missions") + wxFILE_SEP_PATH + extra_usermap + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + wxT("missions") + wxFILE_SEP_PATH + extra_usermap + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->descent2_maps_dir + wxT('\\') + extra_usermap) + wxT("\" \"") + l_game_userpath + wxT("\\missions\""));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->descent2_maps_dir + wxT('/') + extra_usermap) + wxT("\" \"") + l_game_userpath + wxT("/missions\""));
#endif
			}
			else if ((game == GAME_DN3D) && (wxFileName(l_game_userpath) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->dn3d_maps_dir))))
			{
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + user_map))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->dn3d_maps_dir + wxT('\\') + user_map) + wxT("\" \"") + l_game_userpath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->dn3d_maps_dir + wxT('/') + user_map) + wxT("\" \"") + l_game_userpath + wxT('"'));
#endif
			}
			else if ((game == GAME_SW) && (wxFileName(l_game_userpath) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->sw_maps_dir))))
			{
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + user_map))
				{
					l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + user_map + wxT("\" \"") + l_game_origfilespath + wxT('"'));
#endif
				}
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("copy \"") + l_RelativeToAbsolutePath(g_configuration->sw_maps_dir + wxT('\\') + user_map) + wxT("\" \"") + l_game_userpath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("ln -s \"") + l_RelativeToAbsolutePath(g_configuration->sw_maps_dir + wxT('/') + user_map) + wxT("\" \"") + l_game_userpath + wxT('"'));
#endif
			}
		}

		if (use_tcmod)
		{
#ifdef __WXMSW__
			switch (game)
			{
				case GAME_DN3D:
					l_temp_num = g_configuration->dn3dtcmod_profile_current->modfiles.GetCount();

					for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
						if (wxFileName(((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetPath()) != wxFileName(l_game_userpath))
						{
							if (wxFileExists(l_game_userpath + wxT('\\') + ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName()))
							{
								l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
								l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT("\" \"") + l_game_origfilespath + wxT('"'));
							}
							if (!l_add_searchpath)
								l_settings_file.AddLine(wxT("copy \"") + g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var] + wxT("\" \"") + l_game_userpath + wxT('"'));
						}
					break;

				case GAME_SW:
					l_temp_num = g_configuration->swtcmod_profile_current->modfiles.GetCount();

					for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
						if (wxFileName(((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetPath()) != wxFileName(l_game_userpath))
						{
							if (wxFileExists(l_game_userpath + wxT('\\') + ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName()))
							{
								l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
								l_settings_file.AddLine(wxT("move \"") + l_game_userpath + wxT('\\') + ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT("\" \"") + l_game_origfilespath + wxT('"'));
							}
							if (!l_add_searchpath)
								l_settings_file.AddLine(wxT("copy \"") + g_configuration->swtcmod_profile_current->modfiles[l_loop_var] + wxT("\" \"") + l_game_userpath + wxT('"'));
						}
					break;
				default: ;
			}
#else
			l_temp_str = wxEmptyString;

			switch (game)
			{
				case GAME_DN3D:
					l_temp_num = g_configuration->dn3dtcmod_profile_current->modfiles.GetCount();

					for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
						if (wxFileName(((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetPath()) != wxFileName(l_game_userpath))
						{
							if (wxFileExists(l_game_userpath + wxT('/') + ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName()))
							{
								l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
								l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT("\" \"") + l_game_origfilespath + wxT('"'));
							}
							if (!l_add_searchpath)
								l_temp_str << wxT('"') << g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var] << wxT("\" ");
						}
					break;

				case GAME_SW:
					l_temp_num = g_configuration->swtcmod_profile_current->modfiles.GetCount();

					for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
						if (wxFileName(((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetPath()) != wxFileName(l_game_userpath))
						{
							if (wxFileExists(l_game_userpath + wxT('/') + ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName()))
							{
								l_create_origfilespath(l_game_origfilespath, l_use_origfilespath, l_settings_file);
								l_settings_file.AddLine(wxT("mv \"") + l_game_userpath + wxT('/') + ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT("\" \"") + l_game_origfilespath + wxT('"'));
							}
							if (!l_add_searchpath)
								l_temp_str << wxT('"') << g_configuration->swtcmod_profile_current->modfiles[l_loop_var] << wxT("\" ");
						}
					break;
				default: ;
			}

			if (l_temp_str != wxEmptyString)
				l_settings_file.AddLine(wxT("ln -s ") + l_temp_str + wxT('"') + l_game_userpath + wxT('"'));
#endif
		}

		// Now changing to the directory of the game, or of DOSBox.
#ifdef __WXMSW__
		l_settings_file.AddLine(((wxFileName)l_game_execpath).GetVolume() + wxT(':'));
#endif
		l_settings_file.AddLine(wxT("cd \"") + l_game_execpath + wxT('"'));
		// Finally, let's play!
		l_settings_file.AddLine(l_game_fullcmd);
		// But don't forget to remove temporary files! That is, if required.
		if (launch_usermap)
		{
			if ((src_port == SOURCEPORT_DOSDESCENT) && (wxFileName(l_game_userpath) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->descent_maps_dir))))
			{
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("del \"") + l_game_userpath + wxT('\\') + user_map + wxT('"'));
#else
					l_settings_file.AddLine(wxT("rm -f \"") + l_game_userpath + wxT('/') + user_map + wxT('"'));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + user_map))
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_origfilespath + wxT('\\') + user_map + wxT("\" \"") + l_game_userpath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_origfilespath + wxT('/') + user_map + wxT("\" \"") + l_game_userpath + wxT('"'));
#endif
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("del \"") + l_game_userpath + wxT('\\') + extra_usermap + wxT('"'));
#else
					l_settings_file.AddLine(wxT("rm -f \"") + l_game_userpath + wxT('/') + extra_usermap + wxT('"'));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + extra_usermap))
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_origfilespath + wxT('\\') + extra_usermap + wxT("\" \"") + l_game_userpath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_origfilespath + wxT('/') + extra_usermap + wxT("\" \"") + l_game_userpath + wxT('"'));
#endif
			}
			else if ((src_port == SOURCEPORT_DOSDESCENT2) && (wxFileName(l_game_userpath + wxFILE_SEP_PATH + wxT("MISSIONS")) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->descent2_maps_dir))))
			{
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("del \"") + l_game_userpath + wxT("\\MISSIONS\\") + user_map + wxT('"'));
#else
					l_settings_file.AddLine(wxT("rm -f \"") + l_game_userpath + wxT("/MISSIONS/") + user_map + wxT('"'));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + wxT("MISSIONS") + wxFILE_SEP_PATH + user_map))
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_origfilespath + wxT('\\') + user_map + wxT("\" \"") + l_game_userpath + wxFILE_SEP_PATH + wxT("MISSIONS") + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_origfilespath + wxT('/') + user_map + wxT("\" \"") + l_game_userpath + wxFILE_SEP_PATH + wxT("MISSIONS") + wxT('"'));
#endif
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("del \"") + l_game_userpath + wxT("\\MISSIONS\\") + extra_usermap + wxT('"'));
#else
					l_settings_file.AddLine(wxT("rm -f \"") + l_game_userpath + wxT("/MISSIONS/") + extra_usermap + wxT('"'));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + wxT("MISSIONS") + wxFILE_SEP_PATH + extra_usermap))
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_origfilespath + wxT('\\') + extra_usermap + wxT("\" \"") + l_game_userpath + wxFILE_SEP_PATH + wxT("MISSIONS") + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_origfilespath + wxT('/') + extra_usermap + wxT("\" \"") + l_game_userpath + wxFILE_SEP_PATH + wxT("MISSIONS") + wxT('"'));
#endif
			}
			else if (((src_port == SOURCEPORT_D1XREBIRTH) && (wxFileName(l_game_userpath + wxFILE_SEP_PATH + wxT("missions")) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->descent_maps_dir))))
					|| ((src_port == SOURCEPORT_D2XREBIRTH) && (wxFileName(l_game_userpath + wxFILE_SEP_PATH + wxT("missions")) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->descent2_maps_dir)))))
			{
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("del \"") + l_game_userpath + wxT("\\missions\\") + user_map + wxT('"'));
#else
					l_settings_file.AddLine(wxT("rm -f \"") + l_game_userpath + wxT("/missions/") + user_map + wxT('"'));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + wxT("missions") + wxFILE_SEP_PATH + user_map))
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_origfilespath + wxT('\\') + user_map + wxT("\" \"") + l_game_userpath + wxFILE_SEP_PATH + wxT("missions") + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_origfilespath + wxT('/') + user_map + wxT("\" \"") + l_game_userpath + wxFILE_SEP_PATH + wxT("missions") + wxT('"'));
#endif
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("del \"") + l_game_userpath + wxT("\\missions\\") + extra_usermap + wxT('"'));
#else
					l_settings_file.AddLine(wxT("rm -f \"") + l_game_userpath + wxT("/missions/") + extra_usermap + wxT('"'));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + wxT("missions") + wxFILE_SEP_PATH + extra_usermap))
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_origfilespath + wxT('\\') + extra_usermap + wxT("\" \"") + l_game_userpath + wxFILE_SEP_PATH + wxT("missions") + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_origfilespath + wxT('/') + extra_usermap + wxT("\" \"") + l_game_userpath + wxFILE_SEP_PATH + wxT("missions") + wxT('"'));
#endif
			}
			else if (((game == GAME_BLOOD) && (wxFileName(l_game_userpath) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->blood_maps_dir)))) ||
					((game == GAME_DN3D) && (wxFileName(l_game_userpath) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->dn3d_maps_dir)))) ||
					((game == GAME_SW) && (wxFileName(l_game_userpath) != wxFileName(l_GetPathWithNoTrailingSlash(g_configuration->sw_maps_dir)))))
			{
				if (!l_add_searchpath)
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("del \"") + l_game_userpath + wxT('\\') + user_map + wxT('"'));
#else
					l_settings_file.AddLine(wxT("rm -f \"") + l_game_userpath + wxT('/') + user_map + wxT('"'));
#endif
				if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + user_map))
#ifdef __WXMSW__
					l_settings_file.AddLine(wxT("move \"") + l_game_origfilespath + wxT('\\') + user_map + wxT("\" \"") + l_game_userpath + wxT('"'));
#else
					l_settings_file.AddLine(wxT("mv \"") + l_game_origfilespath + wxT('/') + user_map + wxT("\" \"") + l_game_userpath + wxT('"'));
#endif
			}
		}

		if (use_tcmod)
		{
			switch (game)
			{
				case GAME_DN3D:
					l_temp_num = g_configuration->dn3dtcmod_profile_current->modfiles.GetCount();

					for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
						if (wxFileName(((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetPath()) != wxFileName(l_game_userpath))
						{
							if (!l_add_searchpath)
#ifdef __WXMSW__
								l_settings_file.AddLine(wxT("del \"") + l_game_userpath + wxFILE_SEP_PATH + ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT('"'));
#else
								l_settings_file.AddLine(wxT("rm -f \"") + l_game_userpath + wxFILE_SEP_PATH + ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT('"'));
#endif
							if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName()))
#ifdef __WXMSW__
								l_settings_file.AddLine(wxT("move \"") + l_game_origfilespath + wxT('\\') + ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT("\" \"") + l_game_userpath + wxT('"'));
#else
								l_settings_file.AddLine(wxT("mv \"") + l_game_origfilespath + wxT('/') + ((wxFileName)g_configuration->dn3dtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT("\" \"") + l_game_userpath + wxT('"'));
#endif
						}
					break;

				case GAME_SW:
					l_temp_num = g_configuration->swtcmod_profile_current->modfiles.GetCount();

					for (l_loop_var = 0; l_loop_var < l_temp_num; l_loop_var++)
						if (wxFileName(((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetPath()) != wxFileName(l_game_userpath))
						{
							if (!l_add_searchpath)
#ifdef __WXMSW__
								l_settings_file.AddLine(wxT("del \"") + l_game_userpath + wxFILE_SEP_PATH + ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT('"'));
#else
								l_settings_file.AddLine(wxT("rm -f \"") + l_game_userpath + wxFILE_SEP_PATH + ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT('"'));
#endif
							if (wxFileExists(l_game_userpath + wxFILE_SEP_PATH + ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName()))
#ifdef __WXMSW__
								l_settings_file.AddLine(wxT("move \"") + l_game_origfilespath + wxT('\\') + ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT("\" \"") + l_game_userpath + wxT('"'));
#else
								l_settings_file.AddLine(wxT("mv \"") + l_game_origfilespath + wxT('/') + ((wxFileName)g_configuration->swtcmod_profile_current->modfiles[l_loop_var]).GetFullName() + wxT("\" \"") + l_game_userpath + wxT('"'));
#endif
						}
					break;
				default: ;
			}
		}
		// Also don't forget to remove temporary directory created!
		// And yes, this works on both Windows and Linux :)
		if (l_use_origfilespath)
			l_settings_file.AddLine(wxT("rmdir \"") + l_game_origfilespath + wxT('"'));

		l_settings_file.Write();
		l_settings_file.Close();
#ifndef __WXMSW__
		wxExecute(wxT("chmod +x \"") + script_filename + wxT('"'));
		// A trial to prevent an error like "execvp(/home/ny00123/.yang/yang.sh) failed with error 13!"
		wxMilliSleep(250);
#endif
		return true;
	}
	return false;
}

bool l_AddServerLists(wxString filename)
{
	wxTextFile l_settingsfile;
	wxString l_curr_text;
	size_t l_loop_var, l_num_of_lines;
	int l_sep_pos;
	long l_portnum;
	if (wxFile::Access(filename, wxFile::read))
	{
		l_settingsfile.Open(filename);
		l_num_of_lines = l_settingsfile.GetLineCount();
		for (l_loop_var = 0; l_loop_var < l_num_of_lines; l_loop_var++)
		{
			l_curr_text = l_settingsfile[l_loop_var];
			l_sep_pos = l_curr_text.Find(wxT(':'), true);
			if (l_sep_pos != wxNOT_FOUND)
			{
				g_serverlists_addrs->Add(l_curr_text.Left(l_sep_pos));
				if (!(l_curr_text.Mid(l_sep_pos+1)).ToLong(&l_portnum))
					l_portnum = 0;
				g_serverlists_portnums->Add(l_portnum);
			}
		}
		l_settingsfile.Close();
		return true;
	}
	return false;
}

void l_global_lists_init()
{
	g_blood_level_list = new wxArrayString;
	g_blood_level_list->Add(wxT("None"));
	g_blood_level_list->Add(wxT("--- Episode 1: The Way of All Flesh ---"));
	g_blood_level_list->Add(wxT("E1M1: Cradle to Grave"));
	g_blood_level_list->Add(wxT("E1M2: Wrong Side of the Tracks"));
	g_blood_level_list->Add(wxT("E1M3: Phantom Express"));
	g_blood_level_list->Add(wxT("E1M4: Dark Carnival"));
	g_blood_level_list->Add(wxT("E1M5: Hallowed Grounds"));
	g_blood_level_list->Add(wxT("E1M6: The Great Temple"));
	g_blood_level_list->Add(wxT("E1M7: Altar of Stone"));
	g_blood_level_list->Add(wxT("E1M8: House of Horrors"));
	g_blood_level_list->Add(wxT("--- Episode 2: Even Death May Die ---"));
	g_blood_level_list->Add(wxT("E2M1: Shipwrecked"));
	g_blood_level_list->Add(wxT("E2M2: The Lumber Mill"));
	g_blood_level_list->Add(wxT("E2M3: Rest for the Wicked"));
	g_blood_level_list->Add(wxT("E2M4: The Overlooked Hotel"));
	g_blood_level_list->Add(wxT("E2M5: The Haunting"));
	g_blood_level_list->Add(wxT("E2M6: The Cold Rush"));
	g_blood_level_list->Add(wxT("E2M7: Bowels of the Earth"));
	g_blood_level_list->Add(wxT("E2M8: The Lair of Shial"));
	g_blood_level_list->Add(wxT("E2M9: Thin Ice"));
	g_blood_level_list->Add(wxT("--- Episode 3: Farewell to Arms ---"));
	g_blood_level_list->Add(wxT("E3M1: Ghost Town"));
	g_blood_level_list->Add(wxT("E3M2: The Siege"));
	g_blood_level_list->Add(wxT("E3M3: Raw Sewage"));
	g_blood_level_list->Add(wxT("E3M4: The Sick Ward"));
	g_blood_level_list->Add(wxT("E3M5: Spare Parts"));
	g_blood_level_list->Add(wxT("E3M6: Monster Bait"));
	g_blood_level_list->Add(wxT("E3M7: The Pit of Cerberus"));
	g_blood_level_list->Add(wxT("E3M8: Catacombs"));
	g_blood_level_list->Add(wxT("--- Episode 4: Dead Reckoning ---"));
	g_blood_level_list->Add(wxT("E4M1: Butchery Loves Company"));
	g_blood_level_list->Add(wxT("E4M2: Breeding Grounds"));
	g_blood_level_list->Add(wxT("E4M3: Charnel House"));
	g_blood_level_list->Add(wxT("E4M4: Crystal Lake"));
	g_blood_level_list->Add(wxT("E4M5: Fire and Brimstone"));
	g_blood_level_list->Add(wxT("E4M6: The Ganglion Depths"));
	g_blood_level_list->Add(wxT("E4M7: In the Flesh"));
	g_blood_level_list->Add(wxT("E4M8: The Hall of the Epiphany"));
	g_blood_level_list->Add(wxT("E4M9: Mall of the Dead"));
	g_blood_level_list->Add(wxT("--- Episode 5: BloodBath ---"));
	g_blood_level_list->Add(wxT("BB1: The Stronghold"));
	g_blood_level_list->Add(wxT("BB2: Winter Wonderland"));
	g_blood_level_list->Add(wxT("BB3: Bodies"));
	g_blood_level_list->Add(wxT("BB4: The Tower"));
	g_blood_level_list->Add(wxT("BB5: Click!"));
	g_blood_level_list->Add(wxT("BB6: Twin Fortress"));
	g_blood_level_list->Add(wxT("BB7: Midgard"));
	g_blood_level_list->Add(wxT("BB8: Fun With Heads"));
	g_blood_level_list->Add(wxT("DM1: Monolith Building 11"));
	g_blood_level_list->Add(wxT("DM2: Power!"));
	g_blood_level_list->Add(wxT("DM3: Area 15"));
	g_blood_level_list->Add(wxT("--- Episode 6: Post Mortem ---"));
	g_blood_level_list->Add(wxT("E6M1: Welcome to Your Life"));
	g_blood_level_list->Add(wxT("E6M2: They Are Here"));
	g_blood_level_list->Add(wxT("E6M3: Public Storage"));
	g_blood_level_list->Add(wxT("E6M4: Aqueducts"));
	g_blood_level_list->Add(wxT("E6M5: The Ruined Temple"));
	g_blood_level_list->Add(wxT("E6M6: Forbidden Rituals"));
	g_blood_level_list->Add(wxT("E6M7: The Dungeon"));
	g_blood_level_list->Add(wxT("E6M8: Beauty and the Beast"));
	g_blood_level_list->Add(wxT("E6M9: Forgotten Catacombs"));

	g_blood_cp_level_list = new wxArrayString;
	g_blood_cp_level_list->Add(wxT("None"));
	g_blood_cp_level_list->Add(wxT("--- Episode 1: Cryptic Passage ---"));
	g_blood_cp_level_list->Add(wxT("CP01: Boat Docks"));
	g_blood_cp_level_list->Add(wxT("CP02: Old Opera House"));
	g_blood_cp_level_list->Add(wxT("CP03: Gothic Library"));
	g_blood_cp_level_list->Add(wxT("CP04: Lost Monastery"));
	g_blood_cp_level_list->Add(wxT("CP05: Steamboat"));
	g_blood_cp_level_list->Add(wxT("CP06: Graveyard"));
	g_blood_cp_level_list->Add(wxT("CP07: Mountain Pass"));
	g_blood_cp_level_list->Add(wxT("CP08: Abysmal Mine"));
	g_blood_cp_level_list->Add(wxT("CP09: The Castle"));
	g_blood_cp_level_list->Add(wxT("CPSL: Boggy Creek"));
	g_blood_cp_level_list->Add(wxT("--- Episode 2: Cryptic BloodBath ---"));
	g_blood_cp_level_list->Add(wxT("CPBB01: Crypt of Despair"));
	g_blood_cp_level_list->Add(wxT("CPBB02: Pits of Blood"));
	g_blood_cp_level_list->Add(wxT("CPBB03: Unholy Cathedral"));
	g_blood_cp_level_list->Add(wxT("CPBB04: Deadly Inspirations"));

	g_descent_level_list = new wxArrayString;
	g_descent_level_list->Add(wxT("Descent: First Strike"));
	g_descent_level_list->Add(wxT("Total Chaos (CHAOS.HOG)"));

	g_descent2_level_list = new wxArrayString;
	g_descent2_level_list->Add(wxT("Descent 2: Counterstrike!"));
	g_descent2_level_list->Add(wxT("Descent 2: Vertigo (D2X.HOG)"));
	g_descent2_level_list->Add(wxT("DogFight! (D2-2PLYR.HOG)"));
	g_descent2_level_list->Add(wxT("Pandemonium (D2CHAOS.HOG)"));

	g_dn3d_level_list = new wxArrayString;
	g_dn3d_level_list->Add(wxT("None"));
	g_dn3d_level_list->Add(wxT("--- Episode 1: L.A. Meltdown ---"));
	g_dn3d_level_list->Add(wxT("E1L1: Hollywood Holocaust"));
	g_dn3d_level_list->Add(wxT("E1L2: Red Light District"));
	g_dn3d_level_list->Add(wxT("E1L3: Death Row"));
	g_dn3d_level_list->Add(wxT("E1L4: Toxic Dump"));
	g_dn3d_level_list->Add(wxT("E1L5: The Abyss"));
	g_dn3d_level_list->Add(wxT("E1L6: Launch Facility"));
	g_dn3d_level_list->Add(wxT("E1L7: Faces Of Death"));
	g_dn3d_level_list->Add(wxT("--- Episode 2: Lunar Apocalypse ---"));
	g_dn3d_level_list->Add(wxT("E2L1: Spaceport"));
	g_dn3d_level_list->Add(wxT("E2L2: Incubator"));
	g_dn3d_level_list->Add(wxT("E2L3: Warp Factor"));
	g_dn3d_level_list->Add(wxT("E2L4: Fusion Station"));
	g_dn3d_level_list->Add(wxT("E2L5: Occupied Territory"));
	g_dn3d_level_list->Add(wxT("E2L6: Tiberius Station"));
	g_dn3d_level_list->Add(wxT("E2L7: Lunar Reactor"));
	g_dn3d_level_list->Add(wxT("E2L8: Dark Side"));
	g_dn3d_level_list->Add(wxT("E2L9: Overlord"));
	g_dn3d_level_list->Add(wxT("E2L10: Spin Cycle"));
	g_dn3d_level_list->Add(wxT("E2L11: Lunatic Fringe"));
	g_dn3d_level_list->Add(wxT("--- Episode 3: Shrapnel City ---"));
	g_dn3d_level_list->Add(wxT("E3L1: Raw Meat"));
	g_dn3d_level_list->Add(wxT("E3L2: Bank Roll"));
	g_dn3d_level_list->Add(wxT("E3L3: Flood Zone"));
	g_dn3d_level_list->Add(wxT("E3L4: L.A. Rumble"));
	g_dn3d_level_list->Add(wxT("E3L5: Movie Set"));
	g_dn3d_level_list->Add(wxT("E3L6: Rabid Transit"));
	g_dn3d_level_list->Add(wxT("E3L7: Fahrenheit"));
	g_dn3d_level_list->Add(wxT("E3L8: Hotel Hell"));
	g_dn3d_level_list->Add(wxT("E3L9: Stadium"));
	g_dn3d_level_list->Add(wxT("E3L10: Tier Drops"));
	g_dn3d_level_list->Add(wxT("E3L11: Freeway"));
	g_dn3d_level_list->Add(wxT("--- Episode 4: The Birth ---"));
	g_dn3d_level_list->Add(wxT("E4L1: It's Impossible"));
	g_dn3d_level_list->Add(wxT("E4L2: Duke-Burger"));
	g_dn3d_level_list->Add(wxT("E4L3: Shop-N-Bag"));
	g_dn3d_level_list->Add(wxT("E4L4: Babe Land"));
	g_dn3d_level_list->Add(wxT("E4L5: Pigsty"));
	g_dn3d_level_list->Add(wxT("E4L6: Going Postal"));
	g_dn3d_level_list->Add(wxT("E4L7: XXX-Stacy"));
	g_dn3d_level_list->Add(wxT("E4L8: Critical Mass"));
	g_dn3d_level_list->Add(wxT("E4L9: Derelict"));
	g_dn3d_level_list->Add(wxT("E4L10: The Queen"));
	g_dn3d_level_list->Add(wxT("E4L11: Area 51"));

	g_sw_level_list = new wxArrayString;
	g_sw_level_list->Add(wxT("None"));
	g_sw_level_list->Add(wxT("--- Enter The Wang ---"));
	g_sw_level_list->Add(wxT("L1: Seppuku Station"));
	g_sw_level_list->Add(wxT("L2: Zilla Construction"));
	g_sw_level_list->Add(wxT("L3: Master Leep's Temple"));
	g_sw_level_list->Add(wxT("L4: Dark Woods of the Serpent"));
	g_sw_level_list->Add(wxT("--- Code Of Honor ---"));
	g_sw_level_list->Add(wxT("L5: Rising Son"));
	g_sw_level_list->Add(wxT("L6: Killing Fields"));
	g_sw_level_list->Add(wxT("L7: Hara-Kiri Harbor"));
	g_sw_level_list->Add(wxT("L8: Zilla's Villa"));
	g_sw_level_list->Add(wxT("L9: Monastery"));
	g_sw_level_list->Add(wxT("L10: Raider of the Lost Wang"));
	g_sw_level_list->Add(wxT("L11: Sumo Sky Palace"));
	g_sw_level_list->Add(wxT("L12: Bath House"));
	g_sw_level_list->Add(wxT("L13: Unfriendly Skies"));
	g_sw_level_list->Add(wxT("L14: Crude Oil"));
	g_sw_level_list->Add(wxT("L15: Coolie Mines"));
	g_sw_level_list->Add(wxT("L16: Subpen 7"));
	g_sw_level_list->Add(wxT("L17: The Great Escape"));
	g_sw_level_list->Add(wxT("L18: Floating Fortress"));
	g_sw_level_list->Add(wxT("L19: Water Torture"));
	g_sw_level_list->Add(wxT("L20: Stone Rain"));
	g_sw_level_list->Add(wxT("L21: Shanghai Shipwreck - Secret Level"));
	g_sw_level_list->Add(wxT("L22: Auto Maul - Secret Level"));
	g_sw_level_list->Add(wxT("--- DM Levels ---"));
	g_sw_level_list->Add(wxT("L23: Heavy Metal [DM]"));
	g_sw_level_list->Add(wxT("L24: Ripper Valley [DM]"));
	g_sw_level_list->Add(wxT("L25: House of Wang [DM]"));
	g_sw_level_list->Add(wxT("L26: Lo Wang Rally [DM]"));
	g_sw_level_list->Add(wxT("--- CTF Levels ---"));
	g_sw_level_list->Add(wxT("L27: Ruins of the Ronin [CTF]"));
	g_sw_level_list->Add(wxT("L28: Killing Fields [CTF]"));

	g_blood_gametype_list = new wxArrayString;
	g_blood_gametype_list->Add(wxT("Cooperative"));
	g_blood_gametype_list->Add(wxT("Bloodbath"));
	g_blood_gametype_list->Add(wxT("Teams"));

	g_descent_gametype_list = new wxArrayString;
	g_descent_gametype_list->Add(wxT("Anarchy"));
	g_descent_gametype_list->Add(wxT("Team Anarchy"));
	g_descent_gametype_list->Add(wxT("Robo-Anarchy"));
	g_descent_gametype_list->Add(wxT("Cooperative"));

	g_descent2_gametype_list = new wxArrayString;
	g_descent2_gametype_list->Add(wxT("Anarchy"));
	g_descent2_gametype_list->Add(wxT("Team Anarchy"));
	g_descent2_gametype_list->Add(wxT("Robo-Anarchy"));
	g_descent2_gametype_list->Add(wxT("Cooperative"));
	g_descent2_gametype_list->Add(wxT("Capture The Flag"));
	g_descent2_gametype_list->Add(wxT("Hoard"));
	g_descent2_gametype_list->Add(wxT("Team Hoard"));

	g_dn3d_gametype_list = new wxArrayString;
	g_dn3d_gametype_list->Add(wxT("Dukematch (Spawn)"));
	g_dn3d_gametype_list->Add(wxT("Cooperative"));
	g_dn3d_gametype_list->Add(wxT("Dukematch (No Spawn)"));
	g_dn3d_gametype_list->Add(wxT("Team Dukematch (Spawn)"));
	g_dn3d_gametype_list->Add(wxT("Team Dukematch (No Spawn)"));

	g_sw_gametype_list = new wxArrayString;
	g_sw_gametype_list->Add(wxT("Wangbang (Spawn)"));
	g_sw_gametype_list->Add(wxT("Wangbang (No Spawn)"));
	g_sw_gametype_list->Add(wxT("Cooperative"));

	g_blood_skill_list = new wxArrayString;
	g_blood_skill_list->Add(wxT("Still Kicking"));
	g_blood_skill_list->Add(wxT("Pink On The Inside"));
	g_blood_skill_list->Add(wxT("Lightly Broiled"));
	g_blood_skill_list->Add(wxT("Well Done"));
	g_blood_skill_list->Add(wxT("Extra Crispy"));

	g_dn3d_skill_list = new wxArrayString;
	g_dn3d_skill_list->Add(wxT("No Monsters"));
	g_dn3d_skill_list->Add(wxT("Piece Of Cake"));
	g_dn3d_skill_list->Add(wxT("Let's Rock"));
	g_dn3d_skill_list->Add(wxT("Come Get Some"));
	g_dn3d_skill_list->Add(wxT("Damn I'm Good"));

	g_sw_skill_list = new wxArrayString;
	g_sw_skill_list->Add(wxT("No Monsters"));
	g_sw_skill_list->Add(wxT("Tiny Grasshopper"));
	g_sw_skill_list->Add(wxT("I Have No Fear"));
	g_sw_skill_list->Add(wxT("Who Wants Wang"));
	g_sw_skill_list->Add(wxT("No Pain, No Gain"));

	g_blood_monstersettings_list = new wxArrayString;
	g_blood_monstersettings_list->Add(wxT("None"));
	g_blood_monstersettings_list->Add(wxT("Bring 'em on"));
	g_blood_monstersettings_list->Add(wxT("Respawn"));

	g_blood_weaponsettings_list = new wxArrayString;
	g_blood_weaponsettings_list->Add(wxT("Do not Respawn"));
	g_blood_weaponsettings_list->Add(wxT("Are Permanent"));
	g_blood_weaponsettings_list->Add(wxT("Respawn"));
	g_blood_weaponsettings_list->Add(wxT("Respawn with Markers"));

	g_blood_itemsettings_list = new wxArrayString;
	g_blood_itemsettings_list->Add(wxT("Do not Respawn"));
	g_blood_itemsettings_list->Add(wxT("Respawn"));
	g_blood_itemsettings_list->Add(wxT("Respawn with Markers"));

	g_blood_respawnsettings_list = new wxArrayString;
	g_blood_respawnsettings_list->Add(wxT("At Random Locations"));
	g_blood_respawnsettings_list->Add(wxT("Close to Weapons"));
	g_blood_respawnsettings_list->Add(wxT("Away from Enemies"));

	g_dn3d_spawn_list = new wxArrayString;
	g_dn3d_spawn_list->Add(wxT("N/A"));
	g_dn3d_spawn_list->Add(wxT("Monsters"));
	g_dn3d_spawn_list->Add(wxT("Items"));
	g_dn3d_spawn_list->Add(wxT("Inventory"));
	g_dn3d_spawn_list->Add(wxT("All"));

	g_blood_packetmode_list = new wxArrayString;
	g_blood_packetmode_list->Add(wxT("Auto"));
	g_blood_packetmode_list->Add(wxT("Broadcast"));
	g_blood_packetmode_list->Add(wxT("Master/Slave"));

	g_dn3d_connectiontype_list = new wxArrayString;
	g_dn3d_connectiontype_list->Add(wxT("Peer-2-Peer"));
	g_dn3d_connectiontype_list->Add(wxT("Master/Slave"));
	g_dn3d_connectiontype_list->Add(wxT("Server/Client (BROKEN!)"));

	g_dn3d_playercol_list = new wxArrayString;
	g_dn3d_playercol_list->Add(wxT("Auto"));
	g_dn3d_playercol_list->Add(wxT("Blue"));
	g_dn3d_playercol_list->Add(wxT("Red"));
	g_dn3d_playercol_list->Add(wxT("Olive"));
	g_dn3d_playercol_list->Add(wxT("White"));
	g_dn3d_playercol_list->Add(wxT("Grey"));
	g_dn3d_playercol_list->Add(wxT("Green"));
	g_dn3d_playercol_list->Add(wxT("Brown"));
	g_dn3d_playercol_list->Add(wxT("Navy"));
	g_dn3d_playercol_list->Add(wxT("Yellow"));

	g_sw_playercol_list = new wxArrayString;
	g_sw_playercol_list->Add(wxT("Brown"));
	g_sw_playercol_list->Add(wxT("Gray"));
	g_sw_playercol_list->Add(wxT("Purple"));
	g_sw_playercol_list->Add(wxT("Red"));
	g_sw_playercol_list->Add(wxT("Yellow"));
	g_sw_playercol_list->Add(wxT("Olive"));
	g_sw_playercol_list->Add(wxT("Green"));
	g_sw_playercol_list->Add(wxT("Blue"));

	g_serverlists_addrs = new wxArrayString;
	g_serverlists_portnums = new wxArrayLong;
#ifdef __WXMSW__
	l_AddServerLists(wxT("deflists.cfg"));
#else
	if (!l_AddServerLists(wxT("deflists.cfg")))
		l_AddServerLists(*g_launcher_resources_path + wxT("deflists.cfg"));
#endif
}

void l_crc_init()
{
	size_t l_loop_var;

	for (l_loop_var = 0; l_loop_var < SOURCEPORT_NUMOFPORTS; l_loop_var++)
	{
		g_srcport_crc[l_loop_var] = new srcport_crc_t;
		g_srcport_crc[l_loop_var]->filesize = 0;
		g_srcport_crc[l_loop_var]->crc32 = 0;
		g_srcport_crc[l_loop_var]->next = NULL;
	}
	g_srcport_crc[SOURCEPORT_DOSBLOODSW]->srcportname = wxT("Blood DOS Shareware v1.11");
	g_srcport_crc[SOURCEPORT_DOSBLOODRG]->srcportname = wxT("Blood DOS Registered v1.11");
	g_srcport_crc[SOURCEPORT_DOSBLOODPP]->srcportname = wxT("Blood DOS Plasma Pak v1.11");
	g_srcport_crc[SOURCEPORT_DOSBLOODOU]->srcportname = wxT("Blood DOS One Whole Unit v1.21");
	g_srcport_crc[SOURCEPORT_DOSDUKESW]->srcportname = wxT("Duke Nukem 3D DOS Shareware v1.3D");
	g_srcport_crc[SOURCEPORT_DOSDUKERG]->srcportname = wxT("Duke Nukem 3D DOS Registered v1.3D");
	g_srcport_crc[SOURCEPORT_DOSDUKEAE]->srcportname = wxT("Duke Nukem 3D DOS Atomic Edition v1.5");
	g_srcport_crc[SOURCEPORT_DOSSWSW]->srcportname = wxT("Shadow Warrior DOS Shareware v1.2");
	g_srcport_crc[SOURCEPORT_DOSSWRG]->srcportname = wxT("Shadow Warrior DOS Registered v1.2");

	g_srcport_crc[SOURCEPORT_DOSBLOODSW]->filesize = 1437231;
	g_srcport_crc[SOURCEPORT_DOSBLOODRG]->filesize = 1437751;
	g_srcport_crc[SOURCEPORT_DOSBLOODPP]->filesize = 1442359;
	g_srcport_crc[SOURCEPORT_DOSBLOODOU]->filesize = 1442615;
	g_srcport_crc[SOURCEPORT_DOSDUKESW]->filesize = 1178963;
	g_srcport_crc[SOURCEPORT_DOSDUKERG]->filesize = 1179131;
	g_srcport_crc[SOURCEPORT_DOSDUKEAE]->filesize = 1246231;
	g_srcport_crc[SOURCEPORT_DOSSWSW]->filesize = 2324603;
	g_srcport_crc[SOURCEPORT_DOSSWRG]->filesize = 2323389;

	g_srcport_crc[SOURCEPORT_DOSBLOODSW]->crc32 = 0x9409618E;
	g_srcport_crc[SOURCEPORT_DOSBLOODRG]->crc32 = 0x1F9BF51C;
	g_srcport_crc[SOURCEPORT_DOSBLOODPP]->crc32 = 0x003D7B1E;
	g_srcport_crc[SOURCEPORT_DOSBLOODOU]->crc32 = 0x76B5C5C3;
	g_srcport_crc[SOURCEPORT_DOSDUKESW]->crc32 = 0xB1757729;
	g_srcport_crc[SOURCEPORT_DOSDUKERG]->crc32 = 0x1B50472D;
	g_srcport_crc[SOURCEPORT_DOSDUKEAE]->crc32 = 0x0167875D;
	g_srcport_crc[SOURCEPORT_DOSSWSW]->crc32 = 0x8D180F06;
	g_srcport_crc[SOURCEPORT_DOSSWRG]->crc32 = 0x01CAD068;
	// Add an alternative release of DN3D Atomic Edition, with no CD check (GOG.com).
	g_srcport_crc[SOURCEPORT_DOSDUKEAE]->next = new srcport_crc_t;
	g_srcport_crc[SOURCEPORT_DOSDUKEAE]->next->srcportname = wxT("Duke Nukem 3D DOS Atomic Edition v1.5");
	g_srcport_crc[SOURCEPORT_DOSDUKEAE]->next->filesize = 1246231;
	g_srcport_crc[SOURCEPORT_DOSDUKEAE]->next->crc32 = 0x0BDED204;
	g_srcport_crc[SOURCEPORT_DOSDUKEAE]->next->next = NULL;

	for (l_loop_var = 0; l_loop_var < NUM_OF_OUTDATED_SRCPORT_EXES; l_loop_var++)
	{
		g_outdated_srcport[l_loop_var] = new outdated_srcport_t;
		g_outdated_srcport[l_loop_var]->extrapatch = false;
	}

	g_outdated_srcport[0]->filesize = 1280559;
	g_outdated_srcport[0]->crc32 = 0x1E97A686;
	g_outdated_srcport[0]->updateinfo = wxT("Outdated \"Blood Registered v1.00\" has been detected :\n\nPlease, update it to \"Blood Registered v1.11\" using the following patch,\nfrom the official FTP site of \"Monolith Productions\" :\n\nftp://ftp.lith.com/pub/blood/patch/reg/r0011.exe");
	g_outdated_srcport[0]->patch_filename = wxT("r0011.exe");
	g_outdated_srcport[0]->patch_address = wxT("ftp://ftp.lith.com/pub/blood/patch/reg/r0011.exe");

	g_outdated_srcport[1]->filesize = 1416747;
	g_outdated_srcport[1]->crc32 = 0xE53E5EB1;
	g_outdated_srcport[1]->updateinfo = wxT("Outdated \"Blood Registered v1.02\" has been detected :\n\nPlease, update it first to \"Blood Registered v1.10\" using the following patch,\nfrom the official FTP site of \"Monolith Productions\" :\n\nftp://ftp.lith.com/pub/blood/patch/reg/r0210.exe\n\n\nThen finally, update this \"Blood Registered v1.10\" to \"Blood Registered v1.11\" using the following patch :\n\nftp://ftp.lith.com/pub/blood/patch/reg/r1011.exe");
	g_outdated_srcport[1]->extrapatch = true;
	g_outdated_srcport[1]->patch_filename = wxT("r0210.exe");
	g_outdated_srcport[1]->patch_address = wxT("ftp://ftp.lith.com/pub/blood/patch/reg/r0210.exe");
	g_outdated_srcport[1]->extrapatch_filename = wxT("r1011.exe");
	g_outdated_srcport[1]->extrapatch_address = wxT("ftp://ftp.lith.com/pub/blood/patch/reg/r1011.exe");

	g_outdated_srcport[2]->filesize = 1437239;
	g_outdated_srcport[2]->crc32 = 0x5C46F478;
	g_outdated_srcport[2]->updateinfo = wxT("Outdated \"Blood Registered v1.10\" has been detected :\n\nPlease, update it to \"Blood Registered v1.11\" using the following patch,\nfrom the official FTP site of \"Monolith Productions\" :\n\nftp://ftp.lith.com/pub/blood/patch/reg/r1011.exe");
	g_outdated_srcport[2]->patch_filename = wxT("r1011.exe");
	g_outdated_srcport[2]->patch_address = wxT("ftp://ftp.lith.com/pub/blood/patch/reg/r1011.exe");

	g_outdated_srcport[3]->filesize = 1442103;
	g_outdated_srcport[3]->crc32 = 0xD2898F07;
	g_outdated_srcport[3]->updateinfo = wxT("Outdated \"Blood Plasma Pak v1.11 (August 21st, 1997)\" has been detected :\n\nPlease, update it to \"Blood Plasma Pak v1.11 (September 10th, 1997)\" using the following patch,\nfrom the official FTP site of \"Monolith Productions\" :\n\nftp://ftp.lith.com/pub/blood/patch/plasma/pcd11.exe");
	g_outdated_srcport[3]->patch_filename = wxT("pcd11.exe");
	g_outdated_srcport[3]->patch_address = wxT("ftp://ftp.lith.com/pub/blood/patch/plasma/pcd11.exe");
}

bool l_configuration_initandload()
{
	g_configuration = new LauncherConfig;
	return g_configuration->Load();
}

#ifdef ENABLE_UPNP
class UPNPDetectThread : public wxThread
{
public:
	virtual void *Entry();
private:
};

class UPNPForwardThread : public wxThread
{
public:
	UPNPForwardThread(long port_num, bool is_tcp);
	virtual void *Entry();
private:
	long m_port_num;
	bool m_is_tcp;
};

class UPNPUnForwardThread : public wxThread
{
public:
	UPNPUnForwardThread(long port_num, bool is_tcp);
	virtual void *Entry();
private:
	long m_port_num;
	bool m_is_tcp;
};

void* UPNPDetectThread::Entry()
{
	g_upnpdata->Detect();
	return NULL;
};

UPNPForwardThread::UPNPForwardThread(long port_num, bool is_tcp)
{
	m_port_num = port_num;
	m_is_tcp = is_tcp;
}

void* UPNPForwardThread::Entry()
{
	g_upnpdata->Forward(m_port_num, m_is_tcp);
	return NULL;
}

UPNPUnForwardThread::UPNPUnForwardThread(long port_num, bool is_tcp)
{
	m_port_num = port_num;
	m_is_tcp = is_tcp;
}

void* UPNPUnForwardThread::Entry()
{
	g_upnpdata->UnForward(m_port_num, m_is_tcp);
	return NULL;
}

void g_Init_UPNP()
{
	if (!g_upnpdata)
		g_upnpdata = new UPNPData;
	UPNPDetectThread* l_thread = new UPNPDetectThread();
	if ( l_thread->Create() == wxTHREAD_NO_ERROR )
		l_thread->Run();
	else
		l_thread->Delete();
}

void g_UPNP_Forward(long port_num, bool is_tcp)
{
	UPNPForwardThread* l_thread = new UPNPForwardThread(port_num, is_tcp);
	if (l_thread->Create() == wxTHREAD_NO_ERROR )
		l_thread->Run();
	else
		l_thread->Delete();
}

void g_UPNP_UnForward(long port_num, bool is_tcp)
{
	UPNPUnForwardThread* l_thread = new UPNPUnForwardThread(port_num, is_tcp);
	if (l_thread->Create() == wxTHREAD_NO_ERROR )
		l_thread->Run();
	else
		l_thread->Delete();
}
#endif

#if ASK_TO_ACCEPT_LICENSE
bool g_DisplayLicenseAndGetResponse()
{
	if (wxYES == wxMessageBox(wxT("You are allowed to distribute YANG (i.e. Yet Another Netplay Guider) as-is. You may not make modifications of the YANG executable. This includes (but not limited to) disengineering, disassembling and decompiling.\n\nIN ADDITION, YANG IS PROVIDED WITHOUT ANY KIND OF WARRANTY, WHETHER EXPRESSED OR IMPLIED.\n\nDo you agree to these terms?"), wxT("The YANG license"), wxYES_NO|wxICON_INFORMATION))
		return (wxYES == wxMessageBox(wxT("By using YANG online, accesses to the YANG network are done. You agree to the terms posted on the following page, whenever you access the YANG network:\n") + WEBSITE_LICENSE_TERMS + wxT("\n\nAs a couple of examples, the YANG network is accessed whenever you advertise a room on the network, or join an advertised room. You also access it if you retrieve the list of advertised rooms.\n\nFurthermore, whenever you do access the YANG network, you agree that the terms mentioned above may change at any moment, while still being effective. That is, whenever you access the YANG network, you agree to the very current terms.\n\nDo you agree?"), wxT("YANG network access agreement"), wxYES_NO|wxICON_INFORMATION));
	return false;
}
#endif

bool YANGApp::OnInit()
{
#ifdef __WXMSW__
	timeBeginPeriod(1); // DON'T REMOVE THIS OR PINGS MAY BE LESS ACCURATE!
#endif
	SetAppName(wxT("yang"));
#if wxMAJOR_VERSION < 3
	wxStandardPathsBase& l_standard_paths_base = wxStandardPaths::Get();
#if ((defined __UNIX__) || (defined __UNIX_LIKE__) || (defined __LINUX__)) && (defined UNIX_INSTALL_PREFIX) && (!defined __WXMAC__) && (!defined __WXCOCOA__)
	wxStandardPaths l_standard_paths;
	l_standard_paths.SetInstallPrefix(wxT(UNIX_INSTALL_PREFIX));
	g_launcher_resources_path = new wxString(l_standard_paths.GetResourcesDir());
#else
	g_launcher_resources_path = new wxString(l_standard_paths_base.GetResourcesDir());
#endif
#else // wxMAJOR_VERSION > 3
	wxStandardPaths& l_standard_paths_base = wxStandardPaths::Get();
#if ((defined __UNIX__) || (defined __UNIX_LIKE__) || (defined __LINUX__)) && (defined UNIX_INSTALL_PREFIX) && (!defined __WXMAC__) && (!defined __WXCOCOA__)
	l_standard_paths_base.SetInstallPrefix(wxT(UNIX_INSTALL_PREFIX));
#endif
	g_launcher_resources_path = new wxString(l_standard_paths_base.GetResourcesDir());
#endif // wxMAJOR_VERSION
	*g_launcher_resources_path << wxFILE_SEP_PATH;

	// Used for storing user-profile.
	// - [JM] Or, current directory if in portable mode.
	if (wxFileExists(wxT("yang_portable")))
	{
		g_launcher_user_profile_path = new wxString(wxT("."));
		yang_portable = true;
	}
	else
	{
		g_launcher_user_profile_path = new wxString(l_standard_paths_base.GetUserDataDir());
	}

	*g_launcher_user_profile_path << wxFILE_SEP_PATH;
	g_osdescription = new wxString(g_GetOsDescription());

	g_geoip_db = NULL;
	if (wxFileExists(wxT("GeoIP.dat")))
		g_geoip_db = GeoIP_open("GeoIP.dat", GEOIP_STANDARD);
#ifndef __WXMSW__
	wxString l_file_path = *g_launcher_resources_path + wxT("GeoIP.dat");
	if ((g_geoip_db == NULL) && (wxFileExists(l_file_path)))
		g_geoip_db = GeoIP_open(l_file_path.mb_str(wxConvUTF8), GEOIP_STANDARD);
#endif
	if (g_geoip_db == NULL)
		wxMessageBox(wxT("WARNING: Couldn't load the Geo IP database. Country names and flags won't be shown."), wxT("Can't load GeoIP database"), wxOK|wxICON_EXCLAMATION);
	//else
	//  g_countryflags = new FlagsDB();
	/*
	#if (defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__)
	// Remove "\yang" or "/yang"
	g_user_global_profile_path = new wxString(g_launcher_user_profile_path->Mid(0, g_launcher_user_profile_path->Len()-5));
	#else
	// Remove "/.yang"
	g_user_global_profile_path = new wxString(g_launcher_user_profile_path->Mid(0, g_launcher_user_profile_path->Len()-6));
	#endif
	*/
	l_global_lists_init();
	l_crc_init();
	g_host_frame = NULL;
#if (defined __WXMAC__) || (defined __WXCOCOA__)
	g_empty_menubar = new wxMenuBar();
	wxMenuBar::MacSetCommonMenuBar(g_empty_menubar);
#endif
	wxSocketBase::Initialize();
	// VERY IMPORTANT FOR PNG ICONS TO WORK!
	wxImage::AddHandler(new wxPNGHandler);

	if (!l_configuration_initandload())
	{
#if ASK_TO_ACCEPT_LICENSE
		if (!g_DisplayLicenseAndGetResponse())
		{
			//g_configuration->Remove();
			return false;
		}
		g_configuration->is_license_accepted = true;
#endif
		wxMessageBox(wxT("Welcome to YANG, Yet Another Netplay Guider!\nThis launcher lets you create and join rooms so you can then play online.\nSingle player games are supported as well.\n\nBefore YANG can be used, a few games or their source ports have to be specified for YANG.\n\nNow you're going to get two dialogs for initial setup."), wxT("Welcome to YANG!"), wxOK|wxICON_INFORMATION);
		g_main_frame = NULL;
		g_srcports_dialog = new SRCPortsDialog;
		g_srcports_dialog->Show(true);
		g_srcports_dialog->SetFocus();
		//  g_srcports_dialog->Raise();
	}
	else
	{
#if ASK_TO_ACCEPT_LICENSE
		if (!g_configuration->is_license_accepted)
		{
			if (!g_DisplayLicenseAndGetResponse())
				return false;
			g_configuration->is_license_accepted = true;
			// Do check this NOW TOO, because, if there's a version
			// update and we save config, we automatically save the
			// new version value and won't detect it!!
			if (g_configuration->cfg_ver == CONFIG_DEFAULT_VERSION)
				g_configuration->Save();
		}
#endif
		if (g_configuration->cfg_ver != CONFIG_DEFAULT_VERSION)
		{ // NOTICE: This is a temporary migration for users of older versions of YANG.
			/*
			wxFrame* l_temp_win = new wxFrame();

			g_configuration->main_col_private_len = (wxDLG_UNIT(l_temp_win, wxSize(17, -1))).GetWidth();
			g_configuration->main_col_loc_len = (wxDLG_UNIT(l_temp_win, wxSize(15, -1))).GetWidth();
			g_configuration->main_col_ping_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
			g_configuration->main_col_flux_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
			g_configuration->main_col_players_len = (wxDLG_UNIT(l_temp_win, wxSize(34, -1))).GetWidth();
			g_configuration->main_col_tcmod_len = (wxDLG_UNIT(l_temp_win, wxSize(37, -1))).GetWidth();
			g_configuration->main_col_ingame_len = (wxDLG_UNIT(l_temp_win, wxSize(36, -1))).GetWidth();

			g_configuration->mainplayer_col_loc_len = (wxDLG_UNIT(l_temp_win, wxSize(15, -1))).GetWidth();

			g_configuration->host_col_ready_len = (wxDLG_UNIT(l_temp_win, wxSize(23, -1))).GetWidth();
			g_configuration->host_col_loc_len = (wxDLG_UNIT(l_temp_win, wxSize(15, -1))).GetWidth();
			g_configuration->host_col_ping_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
			g_configuration->host_col_flux_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
			g_configuration->host_col_filesizes_len = (wxDLG_UNIT(l_temp_win, wxSize(38, -1))).GetWidth();

			g_configuration->client_col_ready_len = (wxDLG_UNIT(l_temp_win, wxSize(23, -1))).GetWidth();
			g_configuration->client_col_loc_len = (wxDLG_UNIT(l_temp_win, wxSize(15, -1))).GetWidth();
			g_configuration->client_col_ping_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
			g_configuration->client_col_flux_len = (wxDLG_UNIT(l_temp_win, wxSize(24, -1))).GetWidth();
			*/

			g_configuration->Save();

			wxMessageBox(wxT("Thanks for updating YANG! It's recommended you take a short look at the following notes:\n\n"
			                 "* The option \"Extra args for everyone\" was removed. It exposed a security vulnerability, "
			                 "as the host of a room could send an arbitrary exe to the clients under the disguise of a map file, "
			                 "and then make the clients execute the code.\n\n"
			                 "* The wxWidgets 3.0 series is now used instead of 2.8.\n\n"
			                 "* An old and experimental UPnP toggle, depending on wxWidgets 3.0, is now available. However, it can lead to instabilities, like crashes, so it's disabled by default. Using an external UPnP port forwarding application may prove to work better as an alternative.\n\n"
			                 "* You can now use PK3 files for source ports that support them.\n\n"
			                 "* Support for Icculus' Duke3D port was removed. It was replaced with (limited) support for hDuke and nDuke.\n\n"
			                 "* Support for JFShadowWarrior was replaced with VoidSW support. \"Master/Slave\" connection type is the only one which may currently be used with VoidSW.\n\n"
			                 "* For a Master/Slave game, the slave players' configured UDP ports are now explicitly used; Mostly important when there's a mix of Internet and LAN players.\n\n"
			                 "* When a multiplayer hDuke game is started, the demo recording option is repurposed for a mode selection startup menu.\n\n"
			                 "* You may now create a file named yang_portable alongside the place where you run the exe, so YANG can look for configuration files and other files in the same directory."
			             ), wxT("Thanks for updating!"), wxOK|wxICON_INFORMATION);
		}
		if (g_configuration->check_for_updates_on_startup)
			g_CheckForUpdates();
		g_main_frame = new MainFrame(NULL);
		g_main_frame->ShowMainFrame();
	}
#ifdef ENABLE_UPNP
	if (g_configuration->enable_upnp)
		g_Init_UPNP();
#endif
	return true;
}

#ifdef __WXMSW__
int YANGApp::OnExit()
{
	// DON'T REMOVE THIS, ESPECIALLY WHEN timeBeginPeriod
	// FROM ABOVE IS IN USE.
	timeEndPeriod(1);
	return wxApp::OnExit();
}
#endif
