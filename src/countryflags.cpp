/**************************************************************************

Copyright 2011-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <ctype.h>
#include <string.h>
//#include "flags_hash.h"
#include "countryflags.h"

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/mstream.h>
#include <wx/image.h>
#endif

/*
const char** getFlagXPM(const char* code)
{
	int i, l_len = strlen(code);
	char* l_lowercode = new char[l_len+1];
	for (i = 0; i < l_len; i++)
		l_lowercode[i] = tolower(code[i]);
	l_lowercode[l_len] = '\0';
	return Perfect_Hash::in_word_set(l_lowercode, l_len);
}
*/

wxImage getFlag(const char *code)
{
	int i = 0;

	while (wxString(code, wxConvUTF8).MakeLower() != wxString(flagPNGCodeVector[i].code, wxConvUTF8) && i < FLAGS_PNG_SIZE-1)
		i++;

	wxMemoryInputStream istream_flag(flagPNGCodeVector[i].png, flagPNGCodeVector[i].size);

//	return wxImage(getFlagXPM(code)).Resize(wxSize(16, 16), wxPoint(!strcmp(code, "CH") ? 2 : !strcmp(code, "NP") ? 3 : 0, 3));
	return wxImage(istream_flag, wxBITMAP_TYPE_PNG).Resize(wxSize(16, 16), wxPoint(!strcmp(code, "CH") ? 2 : !strcmp(code, "NP") ? 3 : 0, 3));
}
