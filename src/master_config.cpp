/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/
/*
#ifdef wxUSE_GUI
#undef wxUSE_GUI
#define wxUSE_GUI 0
#endif
*/
#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/string.h>
//#include <wx/filename.h>
#include <wx/textfile.h>
#endif

#include "master_config.h"
#include "master.h"
#include "common.h"

MasterConfig::MasterConfig()
{
	port_num = MASTER_DEFAULT_PORT_NUM;
	max_clients = MASTER_DEFAULT_MAX_CLIENTS;
	max_connections_per_ip = MASTER_DEFAULT_MAX_CONNECTIONS_PER_IP;
	max_rooms = MASTER_DEFAULT_MAX_ROOMS;
	max_rooms_per_ip = MASTER_DEFAULT_MAX_ROOMS_PER_IP;
	send_msg_on_advertise = false;
	banlist_refresh_time = MASTER_DEFAULT_BANLIST_REFRESH_TIME;
	// wxString message should be empty by default.
}

wxString l_parseMessage(const wxString& message)
{
	wxString l_final_str;
	size_t l_length = message.Len();
	size_t l_loop_var = 0;
	// Future compatibility with wxWidgets v3.00.
#ifdef wxUSE_UNICODE_UTF8
	wxUniChar l_curr_char;
#else
	wxChar l_curr_char;
#endif
	while (l_loop_var < l_length)
	{
		l_curr_char = message[l_loop_var];
		if (l_curr_char == wxT('\\'))
		{
			l_loop_var++;
			if (l_loop_var < l_length)
			{
				l_curr_char = message[l_loop_var];
				if (l_curr_char == wxT('n'))
					l_final_str << wxT('\n');
				else if (l_curr_char == wxT('t'))
					l_final_str << wxT('\t');
				else if (l_curr_char == wxT('f'))
					l_final_str << wxT('\f');
				else // Includes the case of a backslash '\\'.
					l_final_str << l_curr_char;
			}
		}
		else
			l_final_str << l_curr_char;
		l_loop_var++;
	}
	return l_final_str;
}

void MasterConfig::Load(const wxString& filename)
{
	wxTextFile l_settingsfile;
	wxString temp_string;
	int l_loop_var, l_num_of_lines;
	//if (wxFile::Access(MASTER_CONFIG_FILENAME, wxFile::read))
	if (wxFile::Access(filename, wxFile::read))
	{
		//  l_settingsfile.Open(MASTER_CONFIG_FILENAME);
		l_settingsfile.Open(filename);
		l_loop_var = 0;
		l_num_of_lines = l_settingsfile.GetLineCount();
		while (l_loop_var < l_num_of_lines)
		{
			temp_string = l_settingsfile[l_loop_var];
			if (!temp_string.Find(wxT("port_num = ")))
			{
				if (!temp_string.Mid(11).ToLong(&port_num))
					port_num = MASTER_DEFAULT_PORT_NUM;
			}
			else if (!temp_string.Find(wxT("max_clients = ")))
			{
				if (!temp_string.Mid(14).ToLong(&max_clients))
					max_clients = MASTER_DEFAULT_MAX_CLIENTS;
			}
			else if (!temp_string.Find(wxT("max_connections_per_ip = ")))
			{
				if (!temp_string.Mid(25).ToLong(&max_connections_per_ip))
					max_connections_per_ip = MASTER_DEFAULT_MAX_CONNECTIONS_PER_IP;
			}
			else if (!temp_string.Find(wxT("max_rooms = ")))
			{
				if (!temp_string.Mid(12).ToLong(&max_rooms))
					max_rooms = MASTER_DEFAULT_MAX_ROOMS;
			}
			else if (!temp_string.Find(wxT("max_rooms_per_ip = ")))
			{
				if (!temp_string.Mid(19).ToLong(&max_rooms_per_ip))
					max_rooms_per_ip = MASTER_DEFAULT_MAX_ROOMS_PER_IP;
			}
			else if (!temp_string.Find(wxT("send_msg_on_advertise = 1")))
				send_msg_on_advertise = true;
			else if (!temp_string.Find(wxT("send_msg_on_advertise = 0")))
				send_msg_on_advertise = false;
			else if (!temp_string.Find(wxT("banlist_refresh_time = ")))
			{
				if (!temp_string.Mid(23).ToLong(&banlist_refresh_time))
					max_rooms = MASTER_DEFAULT_BANLIST_REFRESH_TIME;
			}
			else if (!temp_string.Find(wxT("message = ")))
				message = l_parseMessage(temp_string.Mid(10));
			l_loop_var++;
		}
		l_settingsfile.Close();
	}
}
