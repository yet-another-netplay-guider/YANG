/**************************************************************************

Copyright 2011-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/gbsizer.h>
#include <wx/listbox.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/dialog.h>
#include <wx/icon.h>
#endif

#include "banlist_dialog.h"
#include "host_frame.h"
#include "theme.h"
#include "yang.xpm"

BEGIN_EVENT_TABLE(BanListDialog, wxDialog)
EVT_BUTTON(ID_BANLISTADD, BanListDialog::OnBanListAdd)
EVT_TEXT_ENTER(ID_BANLISTENTER, BanListDialog::OnBanListAdd)
EVT_BUTTON(ID_BANLISTREM, BanListDialog::OnBanListRem)
EVT_BUTTON(wxID_OK, BanListDialog::OnOk)
END_EVENT_TABLE()

BanListDialog::BanListDialog( wxWindow* parent ) : YANGDialog( parent, wxID_ANY, wxT("Ban List"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE )
{
	wxBoxSizer* BanListbSizer = new wxBoxSizer( wxVERTICAL );

	m_BanListpanel = new YANGPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* BanListpanelbSizer = new wxBoxSizer( wxVERTICAL );

	wxGridBagSizer* BanListpanelgbSizer = new wxGridBagSizer( 0, 0 );
	BanListpanelgbSizer->SetFlexibleDirection( wxBOTH );
	BanListpanelgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_banliststaticText = new wxStaticText( m_BanListpanel, wxID_ANY, wxT("IP Address"), wxDefaultPosition, wxDefaultSize, 0 );
	BanListpanelgbSizer->Add( m_banliststaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	m_banlisttextCtrl = new YANGTextCtrl( m_BanListpanel, ID_BANLISTENTER, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, -1)), wxTE_PROCESS_ENTER );
	BanListpanelgbSizer->Add( m_banlisttextCtrl, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_banlistaddbutton = new YANGButton( m_BanListpanel, ID_BANLISTADD, wxT("Add"), wxDefaultPosition, wxDefaultSize, 0 );
	BanListpanelgbSizer->Add( m_banlistaddbutton, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );

	m_banlistBox = new YANGListBox( m_BanListpanel, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(80, 80)), 0, NULL, wxLB_MULTIPLE ); 
	BanListpanelgbSizer->Add( m_banlistBox, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5 );

	m_banlistrembutton = new YANGButton( m_BanListpanel, ID_BANLISTREM, wxT("Remove"), wxDefaultPosition, wxDefaultSize, 0 );
	BanListpanelgbSizer->Add( m_banlistrembutton, wxGBPosition( 2, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );

	BanListpanelbSizer->Add( BanListpanelgbSizer, 0, wxEXPAND, 5 );

	m_BanListsdbSizer = new wxStdDialogButtonSizer();
	m_BanListsdbSizerOK = new YANGButton( m_BanListpanel, wxID_OK );
	m_BanListsdbSizer->AddButton( m_BanListsdbSizerOK );
	m_BanListsdbSizerCancel = new YANGButton( m_BanListpanel, wxID_CANCEL );
	m_BanListsdbSizer->AddButton( m_BanListsdbSizerCancel );
	m_BanListsdbSizer->Realize();
	BanListpanelbSizer->Add( m_BanListsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );

	m_BanListpanel->SetSizer( BanListpanelbSizer );
	m_BanListpanel->Layout();
	//	BanListpanelbSizer->Fit( m_BanListpanel );
	BanListpanelbSizer->SetSizeHints( m_BanListpanel );
	BanListbSizer->Add( m_BanListpanel, 1, wxEXPAND | wxALL, 5 );

	SetSizer( BanListbSizer );
	Layout();
	//	BanListbSizer->Fit( this );
	BanListbSizer->SetSizeHints(this);

	Centre();
	SetIcon(wxIcon(yang_xpm));

	m_parent = parent;
}

void BanListDialog::ShowBanListDialog()
{
	m_BanListsdbSizerCancel->SetFocus();
	Show();
}

void BanListDialog::OnBanListAdd(wxCommandEvent& WXUNUSED(event))
{
	m_banlistBox->Append(m_banlisttextCtrl->GetValue());
}

void BanListDialog::OnBanListRem(wxCommandEvent& WXUNUSED(event))
{
	size_t l_loop_var = 0;
	while (l_loop_var < m_banlistBox->GetCount())
	{
		if (m_banlistBox->IsSelected(l_loop_var))
			m_banlistBox->Delete(l_loop_var); // Don't increase l_loop_var by 1!
		else                                // Following items "move" by 1 place.
			l_loop_var++;
	}
}

void BanListDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	wxCommandEvent* l_close_event = new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, ID_BANLISTCLOSE);
#if wxCHECK_VERSION(2, 9, 0)
	m_parent->GetEventHandler()->QueueEvent(l_close_event);
#else
	m_parent->AddPendingEvent(*l_close_event);
	delete l_close_event;
#endif
	Hide();
}
