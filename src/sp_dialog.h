/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_SPDIALOG_H_
#define _YANG_SPDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "config.h"
#include "theme.h"

#define ID_SPBLOODSELECTSRCPORT 1000
#define ID_SPDESCENTSELECTSRCPORT 1001
#define ID_SPDESCENT2SELECTSRCPORT 1002
#define ID_SPDN3DSELECTSRCPORT 1003
#define ID_SPSWSELECTSRCPORT 1004
#define ID_SPBLOODSELECTGAMETYPE 1005
#define ID_SPDN3DSELECTGAMETYPE 1006
#define ID_SPDN3DPLAYDEMO 1007
#define ID_SPSWPLAYDEMO 1008
#define ID_SPBLOODRECORDDEMO 1009
#define ID_SPDN3DRECORDDEMO 1010
#define ID_SPSWRECORDDEMO 1011
#define ID_SPBLOODUSECRYPTICPASSAGE 1012
#define ID_SPBLOODUSEEPIMAP 1013
#define ID_SPDESCENTUSEEPIMAP 1014
#define ID_SPDESCENT2USEEPIMAP 1015
#define ID_SPDN3DUSEEPIMAP 1016
#define ID_SPSWUSEEPIMAP 1017
#define ID_SPBLOODUSEUSERMAP 1018
#define ID_SPDESCENTUSEUSERMAP 1019
#define ID_SPDESCENT2USEUSERMAP 1020
#define ID_SPDN3DUSEUSERMAP 1021
#define ID_SPSWUSEUSERMAP 1022
#define ID_SPSELECTCUSTOMPROFILE 1023

class SPDialog : public YANGDialog
{
public:
	SPDialog();
	~SPDialog();

	void OnSrcPortSelect(wxCommandEvent& event);
	void OnGameTypeSelect(wxCommandEvent& event);
	void OnCheckBoxClick(wxCommandEvent& event);
	void OnRadioBtnClick(wxCommandEvent& event);
	void OnSelectCustomProfile(wxCommandEvent& event);

	void OnCancel(wxCommandEvent& event);
	void OnOk(wxCommandEvent& event);
	void ApplySettings();

private:
	void UpdateSummaryList();
	void AppendLevelsList(GameType game, int level_selection);
	void PrepareDemoFileControls(GameType game);

	YANGGeneralizedbook* m_spgeneralizedbook;

	YANGPanel* m_spbloodpanel;
	wxStaticText* m_spbloodsourceportstaticText;
	YANGChoice* m_spbloodsourceportchoice;
	YANGCheckBox* m_spbloodusecrypticpassagecheckBox;
	YANGRadioButton* m_spbloodepimapradioBtn;
	YANGChoice* m_spbloodepimapchoice;
	YANGRadioButton* m_spbloodusermapradioBtn;
	YANGChoice* m_spbloodusermapchoice;
	wxStaticText* m_spbloodgametypestaticText;
	YANGChoice* m_spbloodgametypechoice;
	YANGCheckBox* m_spbloodrecorddemocheckBox;
	YANGTextCtrl* m_spbloodrecorddemotextCtrl;
	wxStaticText* m_spbloodextraargsstaticText;
	YANGTextCtrl* m_spbloodextraargstextCtrl;

	YANGPanel* m_spdescentpanel;
	wxStaticText* m_spdescentsourceportstaticText;
	YANGChoice* m_spdescentsourceportchoice;
	YANGRadioButton* m_spdescentepimapradioBtn;
	YANGChoice* m_spdescentepimapchoice;
	YANGRadioButton* m_spdescentusermapradioBtn;
	YANGChoice* m_spdescentusermapchoice;
	wxStaticText* m_spdescentextraargsstaticText;
	YANGTextCtrl* m_spdescentextraargstextCtrl;

	YANGPanel* m_spdescent2panel;
	wxStaticText* m_spdescent2sourceportstaticText;
	YANGChoice* m_spdescent2sourceportchoice;
	YANGRadioButton* m_spdescent2epimapradioBtn;
	YANGChoice* m_spdescent2epimapchoice;
	YANGRadioButton* m_spdescent2usermapradioBtn;
	YANGChoice* m_spdescent2usermapchoice;
	wxStaticText* m_spdescent2extraargsstaticText;
	YANGTextCtrl* m_spdescent2extraargstextCtrl;

	YANGPanel* m_spdn3dpanel;
	wxStaticText* m_spdn3dsourceportstaticText;
	YANGChoice* m_spdn3dsourceportchoice;
	wxStaticText* m_spdn3dtcmodprofilestaticText;
	YANGChoice* m_spdn3dtcmodprofilechoice;
	YANGRadioButton* m_spdn3depimapradioBtn;
	YANGChoice* m_spdn3depimapchoice;
	YANGRadioButton* m_spdn3dusermapradioBtn;
	YANGChoice* m_spdn3dusermapchoice;
	wxStaticText* m_spdn3dgametypestaticText;
	YANGChoice* m_spdn3dgametypechoice;
	wxStaticText* m_spdn3dfakeplayersstaticText;
	YANGChoice* m_spdn3dfakeplayerschoice;
	YANGCheckBox* m_spdn3dbotscheckBox;
	wxStaticText* m_spdn3dskillstaticText;
	YANGChoice* m_spdn3dskillchoice;
	wxStaticText* m_spdn3dspawnstaticText;
	YANGChoice* m_spdn3dspawnchoice;
	YANGCheckBox* m_spdn3dplaydemocheckBox;
	YANGChoice* m_spdn3dplaydemochoice;
	YANGCheckBox* m_spdn3drecorddemocheckBox;
	wxStaticText* m_spdn3dextraargsstaticText;
	YANGTextCtrl* m_spdn3dextraargstextCtrl;

	YANGPanel* m_spswpanel;
	wxStaticText* m_spswsourceportstaticText;
	YANGChoice* m_spswsourceportchoice;
	wxStaticText* m_spswtcmodprofilestaticText;
	YANGChoice* m_spswtcmodprofilechoice;
	YANGRadioButton* m_spswepimapradioBtn;
	YANGChoice* m_spswepimapchoice;
	YANGRadioButton* m_spswusermapradioBtn;
	YANGChoice* m_spswusermapchoice;
	wxStaticText* m_spswskillstaticText;
	YANGChoice* m_spswskillchoice;
	YANGCheckBox* m_spswplaydemocheckBox;
	YANGChoice* m_spswplaydemochoice;
	YANGCheckBox* m_spswrecorddemocheckBox;
	YANGTextCtrl* m_spswrecorddemotextCtrl;
	wxStaticText* m_spswextraargsstaticText;
	YANGTextCtrl* m_spswextraargstextCtrl;

	YANGPanel* m_spcustompanel;
	wxStaticText* m_spcustomprofilestaticText;
	YANGChoice* m_spcustomprofilechoice;
	wxStaticText* m_spcustomsummarystaticText;
	YANGTextCtrl* m_spcustomsummarytextCtrl;
	wxStaticText* m_spcustomextraargsstaticText;
	YANGTextCtrl* m_spcustomextraargstextCtrl;

	wxStdDialogButtonSizer* m_spsdbSizer;
	YANGButton* m_spsdbSizerOK;
	YANGButton* m_spsdbSizerCancel;

	int m_temp_bloodepimap, m_temp_bloodepimap_cp;

	DECLARE_EVENT_TABLE()
};

#endif

