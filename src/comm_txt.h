/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_COMMTXT_H_
#define _YANG_COMMTXT_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/arrstr.h>
#include <wx/socket.h>
#include <wx/string.h>
#endif

#define MAX_COMM_TEXT_LENGTH 1024
#define MAX_COMM_READ_ATTEMPTS 8

#define COMM_KEY_IPLIST_FROM_MASTER_TO_MAINWIN "g5Tu8w@4fs"
#define COMM_KEY_IPLIST_FROM_HOST_TO_CLIENT "a7T!Gm$r15"
#define COMM_KEY_IPLIST_FROM_HOST_TO_MASTER "1Jf%^6h(2b"
#define COMM_KEY_IPLIST_FROM_CLIENT_TO_HOST "47Ed$3a7!@"
#define COMM_KEY_IPLIST_FROM_MASTER_TO_HOST "t9e$3Ge7Z1"
// NOTE: Currently unused. IP query may help with debugging, though?
#define COMM_KEY_IPQUERY_FROM_MASTER "6Y2gs3%5#M"

// The function converts a single byte value into a sequence of two hex chars.
// Their order is determined by endiannessID. Little endian if 0, big otherwise.
// For instance: If the value of chInput is 147 (or 93h in Hex),
// and endiannessID==0, then we fill bufOutput with the characters "39".
inline void l_ConvertCharToHexDigits(unsigned char ch_input, char *buf_output, int endianness_id)
{
	if ((ch_input & 15) < 10)
		buf_output[endianness_id] = (ch_input & 15) + '0';
	else
		buf_output[endianness_id] = (ch_input & 15) - 10 + 'a';
	if ((ch_input >> 4) < 10)
		buf_output[1-endianness_id] = (ch_input >> 4) + '0';
	else
		buf_output[1-endianness_id] = (ch_input >> 4) - 10 + 'a';
}

// Same as l_ConvertCharToHexDigits, only it does exactly the opposite.
inline unsigned char l_ConvertHexDigitsToChar(const char* buf_input, int endianness_id)
{
	unsigned char l_result = 0;
	if (buf_input[endianness_id] <= '9') // '9' comes BEFORE 'a'!!!
		l_result |= buf_input[endianness_id] - '0';
	else
		l_result |= buf_input[endianness_id] - 'a' + 10;
	if (buf_input[1-endianness_id] <= '9')
		l_result |= (buf_input[1-endianness_id] - '0') << 4;
	else
		l_result |= (buf_input[1-endianness_id] - 'a' + 10) << 4;
	return l_result;
}

inline wxString g_doEncryptForCommText(const char *input, const char *key)
{
	char l_buffer[2];
	wxString l_result;
	size_t l_key_pos = 0, l_key_len = strlen(key), l_input_len = strlen(input);
	for (size_t l_loop_var = 0; l_loop_var < l_input_len; l_loop_var++)
	{
		l_ConvertCharToHexDigits((unsigned)input[l_loop_var] ^ (unsigned)key[l_key_pos], l_buffer, key[l_key_pos] % 2);
		l_result << wxString(l_buffer, wxConvUTF8, 2);
		l_key_pos++;
		l_key_pos %= l_key_len;
	}
	return l_result;
}

inline wxString g_EncryptForCommText(const wxString& input, const char *key)
{
	return g_doEncryptForCommText(input.mb_str(wxConvUTF8), key);
}

inline wxString g_doDecryptFromCommText(const char *enc_input, const char *key)
{
	unsigned char l_converted_char;
	wxString l_result;
	size_t l_key_pos = 0, l_key_len = strlen(key), l_input_len = strlen(enc_input);
	for (size_t l_loop_var = 0; l_loop_var < l_input_len; l_loop_var += 2)
	{
		if (l_loop_var + 1 == l_input_len)
			break;
		l_converted_char = (unsigned)key[l_key_pos] ^ l_ConvertHexDigitsToChar(enc_input + l_loop_var, key[l_key_pos] % 2);
		l_result << wxString((char *)(&l_converted_char), wxConvUTF8, 1);
		l_key_pos++;
		l_key_pos %= l_key_len;
	}
	return l_result;
}

inline wxString g_DecryptFromCommText(const wxString& enc_input, const char *key)
{
	return g_doDecryptFromCommText(enc_input.mb_str(wxConvUTF8), key);
}

int g_ConvertStringListToCommText(const wxArrayString& str_list, char buffer[MAX_COMM_TEXT_LENGTH]);
//int g_TestStringListToCommTextConversion(const wxArrayString& str_list, size_t length);
//wxArrayString g_ConvertCommTextToStringList(char buffer[MAX_COMM_TEXT_LENGTH]);

#define g_SendCStringToSocket(socket, str) socket->Write(str, strlen(str))
//void g_SendCStringToSocket(wxSocketBase* socket, const char* str);
void g_SendStringListToSocket(wxSocketBase* socket, const wxArrayString& str_list);
//wxArrayString g_GetStringListFromSocket(wxSocketBase *socket);
size_t g_ConvertStringBufToStringList(wxString& str, wxArrayString& str_list);

#endif

