/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2009-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_H_
#define _YANG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/app.h>
#include <GeoIP.h>
#endif

#include "common.h"
#include "config.h"
#include "main_frame.h"
#include "host_frame.h"
#include "client_frame.h"
#include "sp_dialog.h"
#include "mp_dialog.h"
#include "mp_common.h"
#include "manualjoin_dialog.h"
#include "srcports_dialog.h"
#include "tcsmods_dialog.h"
#include "network_dialog.h"
#include "lookandfeel_dialog.h"
#include "adv_dialog.h"
#include "sniptype.h"
//#include "countryflags.h"

#define WEBSITE_HOSTNAME wxT("127.0.0.1")
#define WEBSITE_ADDRESS (wxString(wxT("http://")) + WEBSITE_HOSTNAME + wxT('/'))
#define WEBSITE_LICENSE_TERMS (WEBSITE_ADDRESS + wxT("yangnet_lic.html"))

#define BLOOD_MAX_SKILLNUM 4
#define DN3D_MAX_SKILLNUM 4
#define SW_MAX_SKILLNUM 4
#define BLOOD_DEFAULT_SKILL 2
#define DN3D_DEFAULT_SKILL 0
#define SW_DEFAULT_SKILL 0
#define DN3D_DEFAULT_SKILL_IN_SP 2
#define SW_DEFAULT_SKILL_IN_SP 2
#define BLOOD_DEFAULT_LEVEL 0
#define DESCENT_DEFAULT_LEVEL 0
#define DESCENT2_DEFAULT_LEVEL 0
#define DN3D_DEFAULT_LEVEL 0
#define SW_DEFAULT_LEVEL 0
#define DEFAULT_MAXPING 0
#define DEFAULT_RXDELAY 20
//#define MAX_NUM_OF_MOD_FILES 10
#define DEFAULT_GAME_PORT_NUM 23513
#define DEFAULT_SERVER_PORT_NUM 8501

// [JM] Get rid of these when our game struct is done.
#define GAMENAME_BLOOD wxT("Blood")
#define GAMENAME_DESCENT wxT("Descent")
#define GAMENAME_DESCENT2 wxT("Descent 2")
#define GAMENAME_DN3D wxT("Duke Nukem 3D")
#define GAMENAME_SW wxT("Shadow Warrior")
#define GAMENAME_CUSTOM wxT("Custom DOS game")

// [JM] Get rid of these when our port struct is done.
#define SRCPORTNAME_DOSBLOODSW wxT("DOS Shareware")
#define SRCPORTNAME_DOSBLOODRG wxT("DOS Registered")
#define SRCPORTNAME_DOSBLOODPP wxT("DOS Plasma Pak")
#define SRCPORTNAME_DOSBLOODOU wxT("DOS One Whole Unit")
#define SRCPORTNAME_DOSDESCENT wxT("DOS Descent")
#define SRCPORTNAME_D1XREBIRTH wxT("D1X-Rebirth")
#define SRCPORTNAME_DOSDESCENT2 wxT("DOS Descent 2")
#define SRCPORTNAME_D2XREBIRTH wxT("D2X-Rebirth")
#define SRCPORTNAME_DOSDUKESW wxT("DOS Shareware")
#define SRCPORTNAME_DOSDUKERG wxT("DOS Registered")
#define SRCPORTNAME_DOSDUKEAE wxT("DOS Atomic Edition")
#define SRCPORTNAME_DUKE3DW wxT("Duke3dw")
#define SRCPORTNAME_EDUKE32 wxT("EDuke32")
#define SRCPORTNAME_NDUKE wxT("nDuke")
#define SRCPORTNAME_HDUKE wxT("hDuke")
#define SRCPORTNAME_XDUKE wxT("xDuke")
#define SRCPORTNAME_DOSSWSW wxT("DOS Shareware")
#define SRCPORTNAME_DOSSWRG wxT("DOS Registered")
#define SRCPORTNAME_VOIDSW wxT("VoidSW")
#define SRCPORTNAME_SWP wxT("SWP")

#define NUM_OF_OUTDATED_SRCPORT_EXES 4

extern bool yang_portable;
extern wxArrayString *g_blood_skill_list, *g_dn3d_skill_list, *g_sw_skill_list,
*g_blood_level_list, *g_blood_cp_level_list, *g_descent_level_list, *g_descent2_level_list, *g_dn3d_level_list, *g_sw_level_list,
*g_blood_gametype_list, *g_descent_gametype_list, *g_descent2_gametype_list, *g_dn3d_gametype_list, *g_sw_gametype_list,
*g_blood_monstersettings_list, *g_blood_weaponsettings_list, *g_blood_itemsettings_list, *g_blood_respawnsettings_list,
*g_dn3d_spawn_list, *g_blood_packetmode_list, *g_dn3d_connectiontype_list,
*g_dn3d_playercol_list, *g_sw_playercol_list, *g_serverlists_addrs;
extern wxArrayLong *g_serverlists_portnums;
extern LauncherConfig *g_configuration;
extern MainFrame *g_main_frame;
extern HostedRoomFrame *g_host_frame;
extern ClientRoomFrame *g_client_frame;
extern SRCPortsDialog *g_srcports_dialog;
extern TCsMODsDialog *g_tcsmods_dialog;
extern NetworkDialog *g_network_dialog;
extern LookAndFeelDialog *g_lookandfeel_dialog;
extern AdvDialog *g_adv_dialog;
extern SPDialog *g_sp_dialog;
extern MPDialog *g_mp_dialog;
extern ManualJoinDialog *g_manualjoin_dialog;
extern wxString *g_launcher_user_profile_path, *g_launcher_resources_path, *g_osdescription;
extern GeoIP *g_geoip_db;
//extern FlagsDB *g_countryflags;
#if (defined __WXMAC__) || (defined __WXCOCOA__)
extern wxMenuBar *g_empty_menubar;
#endif
#if (defined ENABLE_UPNP) && !wxCHECK_VERSION(2, 9, 0)
#error "ERROR: UPnP support should be used when compiled with wxWidgets 2.9 or later!"
#endif

struct srcport_crc_t
{
	wxString srcportname;
	unsigned long filesize;
	DWORD crc32;
	srcport_crc_t* next;
};

extern srcport_crc_t* g_srcport_crc[SOURCEPORT_NUMOFPORTS];

typedef struct
{
	unsigned long filesize;
	DWORD crc32;
	wxString updateinfo;
	bool extrapatch;
	wxString patch_filename;
	wxString patch_address;
	wxString extrapatch_filename;
	wxString extrapatch_address;
} outdated_srcport_t;

extern outdated_srcport_t* g_outdated_srcport[NUM_OF_OUTDATED_SRCPORT_EXES];

#ifdef ENABLE_UPNP
void g_Init_UPNP();
void g_UPNP_Forward(long port_num, bool is_tcp);
void g_UPNP_UnForward(long port_num, bool is_tcp);
#endif

void g_CheckForUpdates();

bool g_CheckPortCRC32(SourcePortType src_port, const wxString& executable);

size_t g_GetBloodLevelSelectionByNum(long blood_level);
size_t g_GetBloodCPLevelSelectionByNum(long blood_level);
size_t g_GetDN3DLevelSelectionByNum(long dn3d_level);
size_t g_GetSWLevelSelectionByNum(long sw_level);
size_t g_GetBloodLevelNumBySelection(long blood_selection);
size_t g_GetBloodCPLevelNumBySelection(long blood_selection);
size_t g_GetDN3DLevelNumBySelection(long dn3d_selection);
size_t g_GetSWLevelNumBySelection(long sw_selection);
void g_OpenURL(const wxString& url);
void g_PlaySound(const wxString& filename);
void g_AddAllBloodMapFiles(wxArrayString *file_list);
void g_AddAllDN3DMapFiles(wxArrayString *file_list);
void g_AddAllSWMapFiles(wxArrayString *file_list);
void g_AddAllDescentMapFiles(wxArrayString *file_list, bool msn);
void g_AddAllDescent2MapFiles(wxArrayString *file_list, bool mn2);
void g_AddAllDemoFiles(wxArrayString *file_list, const wxString& demo_dir);
bool g_FindDescentMSNfile(wxString *extra_usermap, const wxString& usermap);
bool g_FindDescentMN2file(wxString *extra_usermap, const wxString& usermap);

int g_compareNoCase(const wxString& first, const wxString& second);

bool g_MakeLaunchScript(bool mp_game,
						const wxString& script_filename,
						RoomPlayersTable* players_table,
						size_t player_index,
						GameType game,
						SourcePortType src_port,
						bool use_crypticpassage,
						size_t episode_map,
						const wxString& user_map,
						bool launch_usermap,
						BloodMPGameT_Type bloodgametype,
						DN3DMPGameT_Type dn3dgametype,
						SWMPGameT_Type swgametype,
						size_t num_fake_players,
						bool have_bot_ai,
						size_t skill_num,
						BloodMPMonsterType blood_monster,
						BloodMPWeaponType blood_weapon,
						BloodMPItemType blood_item,
						BloodMPRespawnType blood_respawn,
						DN3DSpawnType spawn,
						bool play_demo,
						const wxString& playdemo_filename,
						bool record_demo,
						const wxString& recorddemo_filename,
						size_t connectiontype,
						size_t rxdelay,
						bool use_tcmod,
						const wxString& extra_args);

class YANGApp : public wxApp
{
public:
	// Called on application startup
	virtual bool OnInit();
#ifdef __WXMSW__
	virtual int OnExit();
#endif
	//private:
	//  DECLARE_EVENT_TABLE()
};

// Implements YANGApp& GetApp()
//DECLARE_APP(YANGApp)

#endif
