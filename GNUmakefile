#YANG Flags
ENABLE_UPNP=0
MORE_WARNINGS=1
DEBUG?=0
MACUNIVERSAL=0
BINPREFIX=
#WINDRESBIN=$(BINPREFIX)windres

#WxWidgets
WXCONFIG=wx-config
#WXPATH=$(PWD)/wxWidgets/wxMSW-static
#WXCONFIG=$(WXPATH)/bin/wx-config
WINDRESBIN=`$(WXCONFIG) --rescomp`

#GeoIP
GEOIPMACLIBFULLPATH=libGeoIP.1.dylib
#GEOIPMACLIB=libGeoIP.1.dylib
COPY_GEOIPLIB_TO_APP_BUNDLE=0

#Compiler Flags
CXX=$(BINPREFIX)g++
STRIPBIN=$(BINPREFIX)strip
INTCLIENTLDFLAGS=-lGeoIP `$(WXCONFIG) --libs`
INTMASTERLDFLAGS=`$(WXCONFIG) --libs base,net`
INTCOMMONLDFLAGS=-static-libgcc -static-libstdc++
LIBS=-lpthread
#LIBS=-lstdc++ -lpthread

#Paths
SRC=src
RSRC=rsrc
OBJ=obj
UNIX_INSTALL_PREFIX=/usr

OBJECTS=$(OBJ)/adv_dialog.o \
        $(OBJ)/banlist_dialog.o \
        $(OBJ)/client_frame.o \
        $(OBJ)/comm_txt.o \
        $(OBJ)/common.o \
        $(OBJ)/config.o \
        $(OBJ)/countryflags.o \
        $(OBJ)/crc_32.o \
        $(OBJ)/customselect_dialog.o \
        $(OBJ)/file_transfer.o \
        $(OBJ)/getosdesc.o \
        $(OBJ)/host_frame.o \
        $(OBJ)/lookandfeel_dialog.o \
        $(OBJ)/main_frame.o \
        $(OBJ)/manualjoin_dialog.o \
        $(OBJ)/mapselection_dialog.o \
        $(OBJ)/mp_common.o \
        $(OBJ)/mp_dialog.o \
        $(OBJ)/network_dialog.o \
        $(OBJ)/outdatedexe_dialog.o \
        $(OBJ)/pingstopwatch.o \
        $(OBJ)/sp_dialog.o \
        $(OBJ)/srcports_dialog.o \
        $(OBJ)/tcmodselect_dialog.o \
        $(OBJ)/tcsmods_dialog.o \
        $(OBJ)/theme.o \
        $(OBJ)/yang.o

MASTEROBJECTS=$(OBJ)/comm_txt.o \
              $(OBJ)/master.o \
              $(OBJ)/master_banlist.o \
              $(OBJ)/master_clientstable.o \
              $(OBJ)/master_config.o \
              $(OBJ)/master_roomstable.o
INTCXXFLAGS=

ifeq ($(MORE_WARNINGS), 1)
  INTCXXFLAGS+= -W -Wall -Wextra
endif
ifeq ($(DEBUG),1)
  INTCXXFLAGS+= -g
endif
INTCXXFLAGS+= -I$(SRC) `$(WXCONFIG) --cppflags`
ifeq ($(ENABLE_UPNP), 1)
  OBJECTS+= $(OBJ)/upnp.o
  INTCXXFLAGS+= -DENABLE_UPNP
endif
ifdef UNIX_INSTALL_PREFIX
  INTCXXFLAGS+= -DUNIX_INSTALL_PREFIX=\"$(UNIX_INSTALL_PREFIX)\"
endif

ifndef PLATFORM
  osname=$(strip $(shell uname -s))
  ifeq ($(findstring MINGW,$(osname)),MINGW)
    PLATFORM=WINDOWS
  else
    ifeq ($(findstring CYGWIN,$(osname)),CYGWIN)
      PLATFORM=WINDOWS
    else
      ifeq ($(findstring Darwin,$(osname)),Darwin)
        PLATFORM=DARWIN
      else
        PLATFORM=UNKNOWN
      endif
    endif
  endif
endif

ifeq ($(PLATFORM), WINDOWS)
  EXE_EXT=.exe
  OBJECTS+= $(OBJ)/yangrc.o
  INTCOMMONLDFLAGS+= -Wl,-Bstatic,--whole-archive $(LIBS) -Wl,--no-whole-archive
else
  INTCOMMONLDFLAGS+= $(LIBS)
  ifeq ($(PLATFORM), DARWIN)
    ifeq ($(MACUNIVERSAL), 1)
      INTCXXFLAGS+= -arch ppc -arch i386
      INTCOMMONLDFLAGS+= -arch ppc -arch i386
    endif
  endif
endif

FULL_EXE=yang$(EXE_EXT)
FULL_MASTER_EXE=yang_master$(EXE_EXT)

.PHONY: all client master clean clean_client clean_master

all: client master
master: $(FULL_MASTER_EXE)
ifeq ($(PLATFORM), DARWIN)
client: YANG.app
YANG.app: $(RSRC)/Info.plist $(FULL_EXE) $(RSRC)/YANG.icns
	-mkdir YANG.app
	-mkdir YANG.app/Contents
	-mkdir YANG.app/Contents/MacOS
	-mkdir YANG.app/Contents/Resources
	echo -n 'APPL????' > YANG.app/Contents/PkgInfo
	YANGVER=`grep YANG_STR_VERSION $(SRC)/common.h | cut -d'"' -f2`
	sed "s/PutVerHere/${YANGVER}/" $(RSRC)/Info.plist > YANG.app/Contents/Info.plist
	cp $(FULL_EXE) YANG.app/Contents/MacOS/
ifeq ($(COPY_GEOIPLIB_TO_APP_BUNDLE), 1)
	-mkdir YANG.app/Contents/Frameworks
	cp $(GEOIPMACLIBFULLPATH) YANG.app/Contents/Frameworks
#	install_name_tool -change /usr/local/lib/$(GEOIPMACLIB) @executable_path/../Frameworks/$(GEOIPMACLIB) YANG.app/Contents/MacOS/$(FULL_EXE)
endif
	cp -R $(RSRC)/YANG.icns deflists.cfg GeoIP.dat snddata YANG.app/Contents/Resources/
else
client: $(FULL_EXE)
endif

$(FULL_MASTER_EXE): $(MASTEROBJECTS)
	$(CXX) $(MASTEROBJECTS) $(LDFLAGS) $(INTMASTERLDFLAGS) $(INTCOMMONLDFLAGS) -o $@
ifeq ($(DEBUG),0)
	$(STRIPBIN) $(FULL_MASTER_EXE)
endif
$(FULL_EXE): $(OBJECTS)
	$(CXX) $(OBJECTS) $(LDFLAGS) $(INTCLIENTLDFLAGS) $(INTCOMMONLDFLAGS) -o $@
ifeq ($(DEBUG),0)
	$(STRIPBIN) $(FULL_EXE)
endif

$(OBJ)/yangrc.o: $(RSRC)/yang.rc | $(OBJ)
	$(WINDRESBIN) -o $(OBJ)/yangrc.o $(RSRC)/yang.rc
$(OBJ)/%.o: $(SRC)/%.cpp | $(OBJ)
	$(CXX) -c $(CXXFLAGS) $(INTCXXFLAGS) $< -o $@
$(OBJ):
	mkdir $(OBJ)

clean: clean_client clean_master
clean_client:
	-rm -f $(FULL_EXE) $(OBJECTS)
ifeq ($(PLATFORM), DARWIN)
	-rm -rf YANG.app
endif
clean_master:
	-rm -f $(FULL_MASTER_EXE) $(MASTEROBJECTS)

