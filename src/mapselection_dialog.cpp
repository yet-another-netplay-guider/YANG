/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/filedlg.h>
#include <wx/filename.h>
#include <wx/log.h>
#include <wx/msgdlg.h>
#include <wx/sizer.h>
#endif

#include "mapselection_dialog.h"
#include "config.h"
#include "yang.h"
#include "theme.h"
#include "yang.xpm"

BEGIN_EVENT_TABLE(MapSelectionDialog, YANGDialog)
EVT_BUTTON(ID_ADDNEWMAP, MapSelectionDialog::OnAddNewMap)
EVT_BUTTON(wxID_OK, MapSelectionDialog::OnOKOrCancel)
EVT_LISTBOX_DCLICK(wxID_OK, MapSelectionDialog::OnOKOrCancel)
EVT_BUTTON(wxID_CANCEL, MapSelectionDialog::OnOKOrCancel)
END_EVENT_TABLE()

MapSelectionDialog::MapSelectionDialog( wxWindow* parent, GameType game) : YANGDialog( parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE )
{
	wxBoxSizer* MapSelectionbSizer = new wxBoxSizer( wxVERTICAL );

	m_mapselectionpanel = new YANGPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );

	/****** WORKAROUND: Why should the button be created before, so with dark theme it isn't fully grey? ******/
	m_mapselectionsdbSizer = new wxStdDialogButtonSizer();
	m_mapselectionsdbSizerOK = new YANGButton( m_mapselectionpanel, wxID_OK );
	m_mapselectionsdbSizer->AddButton( m_mapselectionsdbSizerOK );
	m_mapselectionsdbSizerCancel = new YANGButton( m_mapselectionpanel, wxID_CANCEL );
	m_mapselectionsdbSizer->AddButton( m_mapselectionsdbSizerCancel );
	m_mapselectionsdbSizer->Realize();

	wxBoxSizer* mapselectionpanelbSizer = new wxBoxSizer( wxVERTICAL );

	if (game == GAME_DESCENT)
	{
		m_mapselectionstaticText = new wxStaticText( m_mapselectionpanel, wxID_ANY, wxT("Note : You need to upload both the \".MSN\" file,\n            and the associated \".HOG\" or \".RDL\" file."), wxDefaultPosition, wxDefaultSize, 0 );
		mapselectionpanelbSizer->Add( m_mapselectionstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	}
	else if (game == GAME_DESCENT2)
	{
		m_mapselectionstaticText = new wxStaticText( m_mapselectionpanel, wxID_ANY, wxT("Note : You need to upload both the \".MN2\" file,\n            and the associated \".HOG\" or \".RL2\" file."), wxDefaultPosition, wxDefaultSize, 0 );
		mapselectionpanelbSizer->Add( m_mapselectionstaticText, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	}

	m_mapselectionlistBox = new YANGListBox( m_mapselectionpanel, wxID_OK, wxDefaultPosition, wxDLG_UNIT(this, wxSize(150, 80)) ); 
	mapselectionpanelbSizer->Add( m_mapselectionlistBox, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxEXPAND, 5 );

	m_mapselectionaddbutton = new YANGButton( m_mapselectionpanel, ID_ADDNEWMAP, wxT("Add a new map file..."), wxDefaultPosition, wxDefaultSize, 0 );
	mapselectionpanelbSizer->Add( m_mapselectionaddbutton, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );

	/****** WORKAROUND: Why should the button be created before, so with dark theme it isn't fully grey? ******/
	/*	m_mapselectionsdbSizer = new wxStdDialogButtonSizer();
	m_mapselectionsdbSizerOK = new YANGButton( m_mapselectionpanel, wxID_OK );
	m_mapselectionsdbSizer->AddButton( m_mapselectionsdbSizerOK );
	m_mapselectionsdbSizerCancel = new YANGButton( m_mapselectionpanel, wxID_CANCEL );
	m_mapselectionsdbSizer->AddButton( m_mapselectionsdbSizerCancel );
	m_mapselectionsdbSizer->Realize();*/
	mapselectionpanelbSizer->Add( m_mapselectionsdbSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );

	m_mapselectionpanel->SetSizer( mapselectionpanelbSizer );
	m_mapselectionpanel->Layout();
	mapselectionpanelbSizer->Fit( m_mapselectionpanel );
	MapSelectionbSizer->Add( m_mapselectionpanel, 1, wxEXPAND | wxALL, 5 );

	SetSizer( MapSelectionbSizer );
	Layout();
	MapSelectionbSizer->Fit( this );

	Centre();
	SetIcon(wxIcon(yang_xpm));
	m_game = game;
	RefreshMapsList();
	m_parent = parent;
}

void MapSelectionDialog::ShowDialog()
{
	Show(true);
	m_mapselectionlistBox->SetFocus();
}

void MapSelectionDialog::RefreshMapsList()
{
	wxArrayString l_file_list;

	switch (m_game)
	{
		case GAME_BLOOD:
			g_AddAllBloodMapFiles(&l_file_list);
			SetTitle(wxT("Select a Blood map file"));
			break;
		case GAME_DESCENT:
			g_AddAllDescentMapFiles(&l_file_list, true);
			SetTitle(wxT("Select a Descent map file"));
			break;
		case GAME_DESCENT2:
			g_AddAllDescent2MapFiles(&l_file_list, true);
			SetTitle(wxT("Select a Descent 2 map file"));
			break;
		case GAME_DN3D:
			g_AddAllDN3DMapFiles(&l_file_list);
			SetTitle(wxT("Select a Duke Nukem 3D map file"));
			break;
		case GAME_SW:
			g_AddAllSWMapFiles(&l_file_list);
			SetTitle(wxT("Select a Shadow Warrior map file"));
			break;
		default: ;
	}

	m_mapselectionlistBox->Set(l_file_list);
}

bool MapSelectionDialog::GetMapSelection(wxString& map_name)
{
	int l_selection = m_mapselectionlistBox->GetSelection();

	if (l_selection == wxNOT_FOUND)
		return false;

	map_name = m_mapselectionlistBox->GetString(l_selection);

	return true;
}

void MapSelectionDialog::OnAddNewMap(wxCommandEvent& WXUNUSED(event))
{
	GameType l_last_game = m_game; // Might change while the dialog appears.
	bool l_do_copy = true, l_file_is_new = true;

	wxFileDialog l_dialog(NULL, m_game == GAME_DESCENT ? wxT("Select a Descent MAP file") : m_game == GAME_DESCENT2 ? wxT("Select a Descent 2 MAP file") : wxT("Select a Build MAP file"),
		m_game == GAME_BLOOD ? g_configuration->blood_maps_dir : m_game == GAME_DESCENT ? g_configuration->descent_maps_dir : m_game == GAME_DESCENT2 ? g_configuration->descent2_maps_dir : m_game == GAME_DN3D ? g_configuration->dn3d_maps_dir : g_configuration->sw_maps_dir, wxEmptyString,
		m_game == GAME_DESCENT ? wxT("Descent MAP files (*.msn;*.hog;*.rdl)|*.msn;*.hog;*.rdl|All files|*") : m_game == GAME_DESCENT2 ? wxT("Descent 2 MAP files (*.mn2;*.hog;*.rl2)|*.mn2;*.hog;*.rl2|All files|*") : wxT("Build MAP files (*.map)|*.map|All files|*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);

	if (l_dialog.ShowModal() == wxID_OK)
	{
		wxString l_fullpath = l_dialog.GetPath(), l_newpath;
		wxString l_filename = ((wxFileName)l_fullpath).GetFullName();
		wxString* l_maps_dir;

		switch (l_last_game)
		{
			case GAME_BLOOD:    l_maps_dir = &(g_configuration->blood_maps_dir); break;
			case GAME_DESCENT:  l_maps_dir = &(g_configuration->descent_maps_dir); break;
			case GAME_DESCENT2: l_maps_dir = &(g_configuration->descent2_maps_dir); break;
			case GAME_DN3D:     l_maps_dir = &(g_configuration->dn3d_maps_dir); break;
			case GAME_SW:       l_maps_dir = &(g_configuration->sw_maps_dir); break;
			default: ;
		}

		if (!wxDirExists(*l_maps_dir))
			wxFileName::Mkdir(*l_maps_dir, 0777, wxPATH_MKDIR_FULL);

		l_newpath = *l_maps_dir + wxFILE_SEP_PATH + l_filename;

		if (wxFileExists(l_newpath))
		{
			l_file_is_new = false;

			l_do_copy = (wxMessageBox(wxT("WARNING: A map file with an identical filename already exists in the configured maps directory for the game, ")
				+ *l_maps_dir + wxT("\nAre you sure you want to overwrite it?"),
				wxT("Map file exists"), wxYES_NO|wxICON_EXCLAMATION) == wxYES);
		}

		if (l_do_copy)
		{
			if (wxCopyFile(l_fullpath, l_newpath))
			{
				if (l_last_game == m_game)
				{
					if (l_file_is_new)
					{
						wxArrayString l_file_list = m_mapselectionlistBox->GetStrings();
						l_file_list.Add(l_filename);
						l_file_list.Sort();
						m_mapselectionlistBox->Set(l_file_list);
					}

					m_mapselectionlistBox->SetStringSelection(l_filename);
				}
			}
			else
				wxLogError(wxT("ERROR: Couldn't make a copy of the map file in the configured maps directory."));
		}
	}

	if (l_last_game != m_game)
		wxMessageBox(wxT("ATTENTION: The host has changed the game. The list of maps to select has been refreshed."),
		wxT("Host has changed game"), wxOK|wxICON_INFORMATION);
}

void MapSelectionDialog::OnOKOrCancel(wxCommandEvent& event)
{
	m_response = (event.GetId() == wxID_OK);

	if (m_response)
	{
		int l_selection = m_mapselectionlistBox->GetSelection();

		if (l_selection == wxNOT_FOUND)
		{
			wxMessageBox(wxT("Please select a map before confirming."), wxT("No map is selected"), wxOK|wxICON_INFORMATION);
			return;
		}

		m_response_filename = m_mapselectionlistBox->GetString(l_selection);
	}

	wxCommandEvent* l_close_event = new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, ID_MAPSELECTIONCLOSE);

#if wxCHECK_VERSION(2, 9, 0)
	m_parent->GetEventHandler()->QueueEvent(l_close_event);
#else
	m_parent->AddPendingEvent(*l_close_event);
	delete l_close_event;
#endif

	Hide();
}
