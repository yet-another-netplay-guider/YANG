/*
**  SNIPTYPE.H - Include file for SNIPPETS data types and commonly used macros
**  A shrunk variation.
*/

#ifndef SNIPTYPE__H
#define SNIPTYPE__H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/defs.h>
#endif

typedef enum {Error_ = -1, Success_, False_ = 0, True_} Boolean_T;

#ifndef BYTE
#define BYTE wxUint8
#endif
#ifndef DWORD
#define DWORD wxUint32
#endif
#ifndef WORD
#define WORD wxUint16
#endif

#endif /* SNIPTYPE__H */
