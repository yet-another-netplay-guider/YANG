/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_MANUALJOINDIALOG_H_
#define _YANG_MANUALJOINDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/gbsizer.h>
#endif

#include "theme.h"

#define ID_MANUALJOINENTER 1000
#define ID_MANUALJOINADD 1001
//#define ID_MANUALJOINNAMELIST 1002
//#define ID_MANUALJOINHOSTADDRLIST 1003
//#define ID_MANUALJOINPORTNUMLIST 1004
//#define ID_MANUALJOINREM 1005
#define ID_MANUALJOINLIST 1002
#define ID_MANUALJOINREM 1003

class ManualJoinDialog : public YANGDialog 
{
public:
	ManualJoinDialog();
	//~ManualJoinDialog();
	//void RefreshListCtrlSize();
	void OnOk(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);
	void OnAddHost(wxCommandEvent& event);
	void OnRemHost(wxCommandEvent& event);
	//void OnHostSelect(wxCommandEvent& event);
	void OnHostSelect(wxListEvent& event);
	void OnHostActivate(wxListEvent& event);
private:
	void Connect();
	YANGPanel* m_ManualJoinpanel;
	wxStaticText* m_manualjoinnamestaticText;
	wxStaticText* m_manualjoinhostaddrstaticText;
	wxStaticText* m_manualjoinportnumstaticText;
	YANGTextCtrl* m_manualjoinnametextCtrl;
	YANGTextCtrl* m_manualjoinhostaddrtextCtrl;
	wxStaticText* m_manualjoinseparatorstaticText;
	YANGTextCtrl* m_manualjoinportnumtextCtrl;
	YANGButton* m_manualjoinaddbutton;
	YANGListCtrl* m_manualjoinlistCtrl;
	YANGButton* m_manualjoinrembutton;
	wxStdDialogButtonSizer* m_ManualJoinsdbSizer;
	YANGButton* m_ManualJoinsdbSizerOK;
	YANGButton* m_ManualJoinsdbSizerCancel;

	DECLARE_EVENT_TABLE()
};

#endif

