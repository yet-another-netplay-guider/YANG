#!/bin/sh
if [ -z $JOBS ]; then
  JOBS=4
fi
DIR="$( cd "$(dirname "$0")" ; pwd -P )"
WXVER=3.0.2
echo Script Directory: $DIR
if [ ! -d "$DIR/wxWidgets/wxWidgets-$WXVER" ]; then
  echo "You should have a $DIR/wxWidgets/wxWidgets-$WXVER folder ready"
  echo "for compilation of wxWidgets-$WXVER"
  exit 0
fi
cd $DIR/wxWidgets/wxWidgets-$WXVER
mkdir mingw
cd mingw
../configure --prefix=$DIR/wxWidgets/wxMSW-static --with-msw --disable-shared --enable-unicode --without-subdirs --without-libtiff --without-libjpeg --with-libpng=builtin --with-regex=builtin --with-zlib=builtin --with-expat=builtin --disable-joystick --disable-svg "$@"
make -j$JOBS
make install

