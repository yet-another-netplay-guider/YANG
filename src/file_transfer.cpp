/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#include <wx/sckstrm.h>
#include <wx/wfstream.h>
#include <wx/window.h>
#include <wx/zstream.h>
#endif

#include "file_transfer.h"

#define FILE_TRANSFER_BUFFER_SIZE 16384

FileSendThread::FileSendThread(wxSocketBase* sock, wxWindow* window,
							   wxFile* fp, TransferStateType* transfer_state_ptr)
{
	m_sock = sock;
	m_window = window;
	m_fp = fp;
	m_transfer_state_ptr = transfer_state_ptr;
}

void* FileSendThread::Entry()
{
	char l_buffer[FILE_TRANSFER_BUFFER_SIZE];
	wxInt32 l_num_of_bytes, l_num_of_bytes_left;
	//m_sock->SetFlags(wxSOCKET_NOWAIT);
	m_sock->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
	m_sock->SetTimeout(1);

	while ((!m_fp->Eof()) && (m_sock->IsConnected()) && (*m_transfer_state_ptr == TRANSFERSTATE_BUSY))
	{
		l_num_of_bytes = m_fp->Read(l_buffer, FILE_TRANSFER_BUFFER_SIZE);
		l_num_of_bytes_left = l_num_of_bytes;
		while (l_num_of_bytes_left && (m_sock->IsConnected()) && (*m_transfer_state_ptr == TRANSFERSTATE_BUSY))
		{
			m_sock->Write(l_buffer+l_num_of_bytes-l_num_of_bytes_left, l_num_of_bytes_left);
			/*    if ((m_sock->Error()) && (m_sock->LastError() != wxSOCKET_NOERROR)
			&& (m_sock->LastError() != wxSOCKET_WOULDBLOCK) && (m_sock->LastError() != wxSOCKET_TIMEDOUT))
			{
			*m_transfer_state_ptr = TRANSFERSTATE_ABORTED;
			wxLogError(wxString::Format(wxT("File transfer error: %d"), m_sock->LastError()));
			}
			else*/
			l_num_of_bytes_left -= m_sock->LastCount();
		}
	}

	// Don't close and delete here - May cause a race condition regarding UI updates.
	//delete m_fp;
	m_sock->Destroy();

	wxCommandEvent* l_end_event = new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, ID_TRANSFERTHREADEND);
#if wxCHECK_VERSION(2, 9, 0)
	m_window->GetEventHandler()->QueueEvent(l_end_event);
#else
	m_window->AddPendingEvent(*l_end_event);
	delete l_end_event;
#endif
	return NULL;
}

FileReceiveThread::FileReceiveThread(wxSocketBase* sock, wxWindow* window,
									 wxFile* fp, wxULongLong filesize,
									 TransferStateType* transfer_state_ptr)
{
	m_sock = sock;
	m_window = window;
	m_fp = fp;
	m_filesize = filesize;
	m_transfer_state_ptr = transfer_state_ptr;
}

void* FileReceiveThread::Entry()
{
	char l_buffer[FILE_TRANSFER_BUFFER_SIZE];
	size_t l_total_read = 0, l_curr_read;
	//m_sock->SetFlags(wxSOCKET_NOWAIT);
	m_sock->SetFlags(wxSOCKET_NONE|wxSOCKET_BLOCK);
	m_sock->SetTimeout(1);

	while ((l_total_read < m_filesize) && (*m_transfer_state_ptr == TRANSFERSTATE_BUSY))
	{
		m_sock->Read(l_buffer, FILE_TRANSFER_BUFFER_SIZE);
		/*  if ((m_sock->Error()) && (m_sock->LastError() != wxSOCKET_NOERROR)
		&& (m_sock->LastError() != wxSOCKET_WOULDBLOCK) && (m_sock->LastError() != wxSOCKET_TIMEDOUT))
		{
		*m_transfer_state_ptr = TRANSFERSTATE_ABORTED;
		wxLogError(wxString::Format(wxT("File transfer error: %d"), m_sock->LastError()));
		}
		else*/
		l_curr_read = m_sock->LastCount();
		l_total_read += l_curr_read;
		if (l_curr_read)
			m_fp->Write(l_buffer, l_curr_read);
	}

	// Don't close and delete here - May cause a race condition regarding UI updates.
	//delete fp;
	m_sock->Destroy();

	wxCommandEvent* l_end_event = new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, ID_TRANSFERTHREADEND);
#if wxCHECK_VERSION(2, 9, 0)
	m_window->GetEventHandler()->QueueEvent(l_end_event);
#else
	m_window->AddPendingEvent(*l_end_event);
	delete l_end_event;
#endif

	return NULL;
}
