/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/
#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/socket.h>
#endif

#include <string.h>
#include "comm_txt.h"

// Routines to convert between a local wxArrayString list of strings
// and a string received/sent via a TCP/IP socket.

int l_FillBuffer(const char* input_str, char output_buffer[MAX_COMM_TEXT_LENGTH])
{
	size_t l_buff_length = strlen(input_str);
	if (l_buff_length <= MAX_COMM_TEXT_LENGTH)
	{
		memcpy(output_buffer, input_str, l_buff_length);
		return l_buff_length;
	}
	else
	{
		memcpy(output_buffer, input_str, MAX_COMM_TEXT_LENGTH);
		return 0;
	}
}

int g_ConvertStringListToCommText(const wxArrayString& str_list, char buffer[MAX_COMM_TEXT_LENGTH])
{
	wxString l_curr_str;
	// Future compatibility with wxWidgets v3.00.
#ifdef wxUSE_UNICODE_UTF8
	wxUniChar l_curr_char;
#else
	wxChar l_curr_char;
#endif
	size_t l_str_loop_var, l_num_of_strings = str_list.GetCount();
	wxString l_final_str = wxString::Format(wxT("%llu:"), (unsigned long long)l_num_of_strings);
	wxString::const_iterator l_char_iterator, l_string_end;
	for (l_str_loop_var = 0; l_str_loop_var < l_num_of_strings; l_str_loop_var++)
	{
		l_curr_str = str_list[l_str_loop_var];
		l_string_end = l_curr_str.end();
		for (l_char_iterator = l_curr_str.begin(); l_char_iterator != l_string_end; l_char_iterator++)
		{
			l_curr_char = *l_char_iterator;
			if (l_curr_char == wxT('\\'))
				l_final_str << wxT("\\\\");
			else if (l_curr_char == wxT('\n'))
				l_final_str << wxT("\\n");
			else if (l_curr_char == wxT('\t'))
				l_final_str << wxT("\\t");
			else if (l_curr_char == wxT('\f'))
				l_final_str << wxT("\\f");
			else if (l_curr_char == wxT(':'))
				l_final_str << wxT("\\:");
			else
				l_final_str << l_curr_char;
		}
		l_final_str << wxT(':');
	}
	return (l_FillBuffer(l_final_str.mb_str(wxConvUTF8), buffer));
}
/*
int g_TestStringListToCommTextConversion(const wxArrayString& str_list)
{
char buffer[MAX_COMM_TEXT_LENGTH];
return g_ConvertStringListToCommText(str_list, buffer);
}

wxArrayString g_ConvertCommTextToStringList(char buffer[MAX_COMM_TEXT_LENGTH], size_t length)
{
wxString l_initial_str = wxString(buffer, wxConvUTF8, length), l_num_items_str, l_curr_str;
#ifdef wxUSE_UNICODE_UTF8
wxUniChar l_curr_char = wxT('A');
#else
wxChar l_curr_char = wxT('A');
#endif
wxArrayString l_str_list;
//size_t l_last_pos = MAX_COMM_TEXT_LENGTH-1;
long l_num_of_strings_left = 0;
// In order to avoid an "endless" loop!
//char l_buffer[MAX_COMM_TEXT_LENGTH];
//for (l_char_loop_var = 0; l_char_loop_var < l_last_pos; l_char_loop_var++)
//  l_buffer[l_char_loop_var] = buffer[l_char_loop_var];
//l_buffer[l_last_pos] = '\0';
//l_initial_str = wxString(l_buffer, wxConvUTF8);
wxString::const_iterator l_char_iterator = l_initial_str.begin(),
l_string_end = l_initial_str.end();
//wxMessageBox(l_initial_str);
while ((l_char_iterator != l_string_end) && (l_curr_char != wxT(':')))
{
l_curr_char = *l_char_iterator;
if (l_curr_char == wxT(':'))
{
if (!l_num_items_str.ToLong(&l_num_of_strings_left))
return wxArrayString();
if (l_num_of_strings_left <= 0)
return wxArrayString();
}
else
l_num_items_str << l_curr_char;
l_char_iterator++;
}
if (!l_num_of_strings_left)
return wxArrayString();
while ((l_char_iterator != l_string_end) && (l_num_of_strings_left > 0))
{
l_curr_char = *l_char_iterator;
if (l_curr_char == wxT('\\'))
{
l_char_iterator++;
if (l_char_iterator != l_string_end)
{
l_curr_char = *l_char_iterator;
if (l_curr_char == wxT('n'))
l_curr_str << wxT('\n');
else
if (l_curr_char == wxT('t'))
l_curr_str << wxT('\t');
else
if (l_curr_char == wxT('f'))
l_curr_str << wxT('\f');
else
if (l_curr_char == wxT(':'))
l_curr_str << wxT(':');
else // Includes the case of a backslash.
l_curr_str << l_curr_char;
l_char_iterator++;
}
}
else
{
if (l_curr_char == wxT(':'))
{
l_str_list.Add(l_curr_str);
l_curr_str = wxEmptyString;
l_num_of_strings_left--;
}
else
l_curr_str << l_curr_char;
l_char_iterator++;
}
}
if (!l_curr_str.IsEmpty())
l_str_list.Add(l_curr_str);
return l_str_list;
}
*/
void g_SendStringListToSocket(wxSocketBase* socket, const wxArrayString& str_list)
{
	char l_buffer[MAX_COMM_TEXT_LENGTH];
	int l_length = g_ConvertStringListToCommText(str_list, l_buffer);
	if (l_length)
		socket->Write(l_buffer, l_length);
	else
		socket->Write(l_buffer, MAX_COMM_TEXT_LENGTH);
}

/* *** A DEFINITION IS NOW USED IN THE HEADER FILE. ***
void g_SendCStringToSocket(wxSocketBase* socket, const char* str)
{
socket->Write(str, strlen(str));
}

wxArrayString g_GetStringListFromSocket(wxSocketBase *socket)
{
char l_buffer[MAX_COMM_TEXT_LENGTH];
socket->Read(l_buffer, MAX_COMM_TEXT_LENGTH);
return g_ConvertCommTextToStringList(l_buffer, socket->LastCount());
}
*/
size_t g_ConvertStringBufToStringList(wxString& str, wxArrayString& str_list)
{
	wxString l_num_items_str, l_curr_str;
#ifdef wxUSE_UNICODE_UTF8
	wxUniChar l_curr_char = wxT('A');
#else
	wxChar l_curr_char = wxT('A');
#endif
	wxArrayString l_str_list;
	size_t /*l_char_loop_var = 0, l_str_length, */l_final_num_of_strings;
	wxString::const_iterator l_char_iterator = str.begin(), l_string_end = str.end();
	long l_num_of_strings_left = 0;
	//str = wxString(buffer, wxConvUTF8, length);
	//l_str_length = str.Len();
	while ((l_char_iterator != l_string_end) && (l_curr_char != wxT(':')))
	{
		l_curr_char = *l_char_iterator;
		if (l_curr_char == wxT(':'))
		{
			if (!l_num_items_str.ToLong(&l_num_of_strings_left))
			{
				str = wxEmptyString;
				return 0;
			}
			if (l_num_of_strings_left <= 0)
			{
				str = wxEmptyString;
				return 0;
			}
		}
		else
			l_num_items_str << l_curr_char;
		l_char_iterator++;
	}
	if (!l_num_of_strings_left)
	{
		str = wxEmptyString;
		return 0;
	}
	l_final_num_of_strings = l_num_of_strings_left;
	while ((l_char_iterator != l_string_end) && (l_num_of_strings_left > 0))
	{
		l_curr_char = *l_char_iterator;
		if (l_curr_char == wxT('\\'))
		{
			l_char_iterator++;
			if (l_char_iterator != l_string_end)
			{
				l_curr_char = *l_char_iterator;
				if (l_curr_char == wxT('n'))
					l_curr_str << wxT('\n');
				else if (l_curr_char == wxT('t'))
					l_curr_str << wxT('\t');
				else if (l_curr_char == wxT('f'))
					l_curr_str << wxT('\f');
				else // Includes the cases of a backslash '\\' and a colon ':'.
					l_curr_str << l_curr_char;
				l_char_iterator++;
			}
		}
		else
		{
			if (l_curr_char == wxT(':'))
			{
				l_str_list.Add(l_curr_str);
				l_curr_str = wxEmptyString;
				l_num_of_strings_left--;
			}
			else
				l_curr_str << l_curr_char;
			l_char_iterator++;
		}
	}
	if (l_num_of_strings_left > 0)
		return 0;
	str_list = l_str_list;
	l_curr_str = wxEmptyString;
	while (l_char_iterator != l_string_end)
	{
		l_curr_str << *l_char_iterator;
		l_char_iterator++;
	}
	str = l_curr_str;
	return l_final_num_of_strings;
}
