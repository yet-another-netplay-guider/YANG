/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#ifndef _YANG_NETWORKDIALOG_H_
#define _YANG_NETWORKDIALOG_H_

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#include "theme.h"

#define ID_GAMENICKNAMETEXT 1000
#define ID_SNDJOINCHECKBOX 1001
#define ID_SNDJOINTEST 1002
#define ID_SNDJOINLOCATE 1003
#define ID_SNDLEAVECHECKBOX 1004
#define ID_SNDLEAVETEST 1005
#define ID_SNDLEAVELOCATE 1006
#define ID_SNDSENDCHECKBOX 1007
#define ID_SNDSENDTEST 1008
#define ID_SNDSENDLOCATE 1009
#define ID_SNDRECEIVECHECKBOX 1010
#define ID_SNDRECEIVETEST 1011
#define ID_SNDRECEIVELOCATE 1012
#define ID_SNDERRORCHECKBOX 1013
#define ID_SNDERRORTEST 1014
#define ID_SNDERRORLOCATE 1015
#define ID_SNDRESTORE 1016

class NetworkDialog : public YANGDialog
{
public:
	NetworkDialog();
	~NetworkDialog();
	void OnLocateSndFile(wxCommandEvent& event);
	void OnTestSound(wxCommandEvent& event);
	void OnSndRestore(wxCommandEvent& event);
	void OnCheckBoxClick(wxCommandEvent& event);
	void OnUpdateGameNickName(wxCommandEvent& event);

	//void OnApply(wxCommandEvent& event);
	void OnCancel(wxCommandEvent& event);
	void OnOk(wxCommandEvent& event);
	bool ApplySettings();

private:
	YANGNotebook* m_networknotebook;
	YANGPanel* m_networkprofilepanel;
	wxStaticText* m_nicknamestaticText;
	YANGTextCtrl* m_nicknametextCtrl;
	wxStaticText* m_gamenicknamestaticText;
	wxStaticText* m_nicknamenotestaticText;
	YANGTextCtrl* m_gamenicknametextCtrl;
	YANGPanel* m_networksndnotipanel;
	YANGCheckBox* m_soundjoincheckBox;
	YANGButton* m_soundjointestbutton;
	YANGButton* m_soundjoinlocatebutton;
	YANGTextCtrl* m_soundjointextCtrl;
	YANGCheckBox* m_soundleavecheckBox;
	YANGButton* m_soundleavetestbutton;
	YANGButton* m_soundleavelocatebutton;
	YANGTextCtrl* m_soundleavetextCtrl;
	YANGCheckBox* m_soundsendcheckBox;
	YANGButton* m_soundsendtestbutton;
	YANGButton* m_soundsendlocatebutton;
	YANGTextCtrl* m_soundsendtextCtrl;
	YANGCheckBox* m_soundreceivecheckBox;
	YANGButton* m_soundreceivetestbutton;
	YANGButton* m_soundreceivelocatebutton;
	YANGTextCtrl* m_soundreceivetextCtrl;
	YANGCheckBox* m_sounderrorcheckBox;
	YANGButton* m_sounderrortestbutton;
	YANGButton* m_sounderrorlocatebutton;
	YANGTextCtrl* m_sounderrortextCtrl;
	YANGCheckBox* m_soundmutewhileingamecheckBox;
	YANGButton* m_soundrestorebutton;
	YANGPanel* m_networknetpanel;
	wxStaticText* m_gameportstaticText;
	YANGTextCtrl* m_gameporttextCtrl;
	wxStaticText* m_serverportstaticText;
	YANGTextCtrl* m_serverporttextCtrl;
#ifdef ENABLE_UPNP
	YANGCheckBox* m_enableupnpcheckBox;
#endif
	wxStaticText* m_netnotesstaticText;
	wxStdDialogButtonSizer* m_networksdbSizer;
	YANGButton* m_networksdbSizerOK;
	YANGButton* m_networksdbSizerCancel;

	DECLARE_EVENT_TABLE()
};

#endif
