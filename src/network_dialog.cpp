/**************************************************************************

Copyright 2008-2023 - NY00123
Copyright 2011-2023 - Turrican
Copyright 2019-2023 - Jordon Moss

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**************************************************************************/

#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/filedlg.h>
#include <wx/gbsizer.h>
#include <wx/msgdlg.h>
#endif

#include "network_dialog.h"
#include "adv_dialog.h"
#include "main_frame.h"
#include "config.h"
#include "yang.h"
#include "theme.h"
#include "yang.xpm"

BEGIN_EVENT_TABLE(NetworkDialog, YANGDialog)

EVT_BUTTON(wxID_OK, NetworkDialog::OnOk)
//EVT_BUTTON(wxID_APPLY, NetworkDialog::OnApply)
EVT_BUTTON(wxID_CANCEL, NetworkDialog::OnCancel)

EVT_BUTTON(ID_SNDJOINTEST, NetworkDialog::OnTestSound)
EVT_BUTTON(ID_SNDLEAVETEST, NetworkDialog::OnTestSound)
EVT_BUTTON(ID_SNDSENDTEST, NetworkDialog::OnTestSound)
EVT_BUTTON(ID_SNDRECEIVETEST, NetworkDialog::OnTestSound)
EVT_BUTTON(ID_SNDERRORTEST, NetworkDialog::OnTestSound)

EVT_BUTTON(ID_SNDJOINLOCATE, NetworkDialog::OnLocateSndFile)
EVT_BUTTON(ID_SNDLEAVELOCATE, NetworkDialog::OnLocateSndFile)
EVT_BUTTON(ID_SNDSENDLOCATE, NetworkDialog::OnLocateSndFile)
EVT_BUTTON(ID_SNDRECEIVELOCATE, NetworkDialog::OnLocateSndFile)
EVT_BUTTON(ID_SNDERRORLOCATE, NetworkDialog::OnLocateSndFile)
EVT_BUTTON(ID_SNDRESTORE, NetworkDialog::OnSndRestore)

EVT_CHECKBOX(ID_SNDJOINCHECKBOX, NetworkDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SNDLEAVECHECKBOX, NetworkDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SNDSENDCHECKBOX, NetworkDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SNDRECEIVECHECKBOX, NetworkDialog::OnCheckBoxClick)
EVT_CHECKBOX(ID_SNDERRORCHECKBOX, NetworkDialog::OnCheckBoxClick)

EVT_TEXT(ID_GAMENICKNAMETEXT, NetworkDialog::OnUpdateGameNickName)

END_EVENT_TABLE()

NetworkDialog::NetworkDialog() : YANGDialog(NULL, wxID_ANY, wxT("Multiplayer and networking"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE|wxDIALOG_NO_PARENT, wxEmptyString)
{
	wxBoxSizer* NetworkDialogbSizer = new wxBoxSizer( wxVERTICAL );

	m_networknotebook = new YANGNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	//	m_networklistbook = new wxListbook( this, wxID_ANY, wxDefaultPosition, wxDLG_UNIT(this, wxSize(88, -1)), wxLB_DEFAULT|wxTAB_TRAVERSAL );
	m_networkprofilepanel = new YANGPanel( m_networknotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* profilepanelbSizer = new wxBoxSizer( wxVERTICAL );

	wxGridBagSizer* profilegbSizer = new wxGridBagSizer( 0, 0 );
	profilegbSizer->SetFlexibleDirection( wxBOTH );
	profilegbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_nicknamestaticText = new wxStaticText( m_networkprofilepanel, wxID_ANY, wxT("Nickname:"), wxDefaultPosition, wxDefaultSize, 0 );
	profilegbSizer->Add( m_nicknamestaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_nicknametextCtrl = new YANGTextCtrl( m_networkprofilepanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(128, -1)) );
	profilegbSizer->Add( m_nicknametextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_gamenicknamestaticText = new wxStaticText( m_networkprofilepanel, wxID_ANY, wxT("In-game nickname*:"), wxDefaultPosition, wxDefaultSize, 0 );
	profilegbSizer->Add( m_gamenicknamestaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_nicknamenotestaticText = new wxStaticText( m_networkprofilepanel, wxID_ANY, wxT("* For the in-game nickname,\nonly the following characters are allowed:\n\n# @ _ - +\nand all digits and English letters."), wxDefaultPosition, wxDefaultSize, 0 );
	profilegbSizer->Add( m_nicknamenotestaticText, wxGBPosition( 2, 0 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_gamenicknametextCtrl = new YANGTextCtrl( m_networkprofilepanel, ID_GAMENICKNAMETEXT, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(48, -1)) );
	profilegbSizer->Add( m_gamenicknametextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	profilegbSizer->AddGrowableCol( 1 );

	profilepanelbSizer->Add( profilegbSizer, 1, wxEXPAND, 0 );

	m_networkprofilepanel->SetSizer( profilepanelbSizer );
	m_networkprofilepanel->Layout();
	profilepanelbSizer->Fit( m_networkprofilepanel );
	m_networknotebook->AddPage( m_networkprofilepanel, wxT("Player profile"), true );
	m_networksndnotipanel = new YANGPanel( m_networknotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* sndnotibSizer = new wxBoxSizer( wxVERTICAL );

	wxGridBagSizer* sndnotigbSizer = new wxGridBagSizer( 0, 0 );
	sndnotigbSizer->SetFlexibleDirection( wxBOTH );
	sndnotigbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_soundjoincheckBox = new YANGCheckBox( m_networksndnotipanel, ID_SNDJOINCHECKBOX, wxT("Play a sound when a player joins a room."), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundjoincheckBox, wxGBPosition( 0, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundjointestbutton = new YANGButton( m_networksndnotipanel, ID_SNDJOINTEST, wxT("Test"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundjointestbutton, wxGBPosition( 0, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );

	m_soundjoinlocatebutton = new YANGButton( m_networksndnotipanel, ID_SNDJOINLOCATE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundjoinlocatebutton, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundjointextCtrl = new YANGTextCtrl( m_networksndnotipanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	sndnotigbSizer->Add( m_soundjointextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundleavecheckBox = new YANGCheckBox( m_networksndnotipanel, ID_SNDLEAVECHECKBOX, wxT("Play a sound when a player leaves a room."), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundleavecheckBox, wxGBPosition( 2, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundleavetestbutton = new YANGButton( m_networksndnotipanel, ID_SNDLEAVETEST, wxT("Test"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundleavetestbutton, wxGBPosition( 2, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );

	m_soundleavelocatebutton = new YANGButton( m_networksndnotipanel, ID_SNDLEAVELOCATE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundleavelocatebutton, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundleavetextCtrl = new YANGTextCtrl( m_networksndnotipanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	sndnotigbSizer->Add( m_soundleavetextCtrl, wxGBPosition( 3, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundsendcheckBox = new YANGCheckBox( m_networksndnotipanel, ID_SNDSENDCHECKBOX, wxT("Play a sound when you send a message."), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundsendcheckBox, wxGBPosition( 4, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundsendtestbutton = new YANGButton( m_networksndnotipanel, ID_SNDSENDTEST, wxT("Test"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundsendtestbutton, wxGBPosition( 4, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );

	m_soundsendlocatebutton = new YANGButton( m_networksndnotipanel, ID_SNDSENDLOCATE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundsendlocatebutton, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundsendtextCtrl = new YANGTextCtrl( m_networksndnotipanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	sndnotigbSizer->Add( m_soundsendtextCtrl, wxGBPosition( 5, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundreceivecheckBox = new YANGCheckBox( m_networksndnotipanel, ID_SNDRECEIVECHECKBOX, wxT("Play a sound when you receive a message."), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundreceivecheckBox, wxGBPosition( 6, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundreceivetestbutton = new YANGButton( m_networksndnotipanel, ID_SNDRECEIVETEST, wxT("Test"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundreceivetestbutton, wxGBPosition( 6, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );

	m_soundreceivelocatebutton = new YANGButton( m_networksndnotipanel, ID_SNDRECEIVELOCATE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundreceivelocatebutton, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundreceivetextCtrl = new YANGTextCtrl( m_networksndnotipanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	sndnotigbSizer->Add( m_soundreceivetextCtrl, wxGBPosition( 7, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_sounderrorcheckBox = new YANGCheckBox( m_networksndnotipanel, ID_SNDERRORCHECKBOX, wxT("Play a sound when something wrong occurs."), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_sounderrorcheckBox, wxGBPosition( 8, 0 ), wxGBSpan( 1, 3 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_sounderrortestbutton = new YANGButton( m_networksndnotipanel, ID_SNDERRORTEST, wxT("Test"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_sounderrortestbutton, wxGBPosition( 8, 3 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT, 5 );

	m_sounderrorlocatebutton = new YANGButton( m_networksndnotipanel, ID_SNDERRORLOCATE, wxT("Locate"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_sounderrorlocatebutton, wxGBPosition( 9, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_sounderrortextCtrl = new YANGTextCtrl( m_networksndnotipanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(120, -1)) );
	sndnotigbSizer->Add( m_sounderrortextCtrl, wxGBPosition( 9, 1 ), wxGBSpan( 1, 3 ), wxALL|wxEXPAND|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundmutewhileingamecheckBox = new YANGCheckBox( m_networksndnotipanel, wxID_ANY, wxT("Mute all sounds while in game and until you do some action."), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundmutewhileingamecheckBox, wxGBPosition( 10, 0 ), wxGBSpan( 1, 4 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_soundrestorebutton = new YANGButton( m_networksndnotipanel, ID_SNDRESTORE, wxT("Restore defaults"), wxDefaultPosition, wxDefaultSize, 0 );
	sndnotigbSizer->Add( m_soundrestorebutton, wxGBPosition( 11, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	sndnotibSizer->Add( sndnotigbSizer, 1, wxEXPAND, 5 );

	m_networksndnotipanel->SetSizer( sndnotibSizer );
	m_networksndnotipanel->Layout();
	sndnotibSizer->Fit( m_networksndnotipanel );
	m_networknotebook->AddPage( m_networksndnotipanel, wxT("Sound notifications"), false );
	m_networknetpanel = new YANGPanel( m_networknotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* netbSizer = new wxBoxSizer( wxVERTICAL );

	wxGridBagSizer* netgbSizer = new wxGridBagSizer( 0, 0 );
	netgbSizer->SetFlexibleDirection( wxBOTH );
	netgbSizer->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_gameportstaticText = new wxStaticText( m_networknetpanel, wxID_ANY, wxT("Game port number (UDP):\nNull-Modem port number (TCP):"), wxDefaultPosition, wxDefaultSize, 0 );
	netgbSizer->Add( m_gameportstaticText, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_gameporttextCtrl = new YANGTextCtrl( m_networknetpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
	netgbSizer->Add( m_gameporttextCtrl, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_serverportstaticText = new wxStaticText( m_networknetpanel, wxID_ANY, wxT("YANG server port number (TCP):"), wxDefaultPosition, wxDefaultSize, 0 );
	netgbSizer->Add( m_serverportstaticText, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_serverporttextCtrl = new YANGTextCtrl( m_networknetpanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDLG_UNIT(this, wxSize(32, -1)) );
	netgbSizer->Add( m_serverporttextCtrl, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

#ifdef ENABLE_UPNP
	m_enableupnpcheckBox = new YANGCheckBox( m_networknetpanel, wxID_ANY, wxT("Enable automatic port forwarding, using UPnP (experimental, potentially unstable)."), wxDefaultPosition, wxDefaultSize, 0 );
	netgbSizer->Add( m_enableupnpcheckBox, wxGBPosition( 2, 0 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );
#endif

	m_netnotesstaticText = new wxStaticText( m_networknetpanel, wxID_ANY, wxT("PLEASE READ THE FOLLOWING:\n\n* D1X-Rebirth and D2X-Rebirth use the UDP port specified above,\ninstead of using their default UDP port number 42424.\n\n* SWP ignores the UDP port specified above,\nsince it supports multiplayer using port number 23513 ONLY.\n\n* If you want two or more players on the same\nlocal network to join INTERNET rooms, you should\nset DIFFERENT game port numbers. Otherwise\nthere won't be a way for the outside (Internet) players\nto tell the difference between you two or more.\n\n* If you want to host two or more game rooms\non the same local network, apart from stealing a\nnot so-tiny amount of bandwidth, that'd also require\nyou to use DIFFERENT server port numbers\nfor the rooms."), wxDefaultPosition, wxDefaultSize, 0 );
	netgbSizer->Add( m_netnotesstaticText, wxGBPosition( 3, 0 ), wxGBSpan( 1, 2 ), wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	netbSizer->Add( netgbSizer, 1, wxEXPAND, 0 );

	m_networknetpanel->SetSizer( netbSizer );
	m_networknetpanel->Layout();
	netbSizer->Fit( m_networknetpanel );
	m_networknotebook->AddPage( m_networknetpanel, wxT("Networking"), false );

	NetworkDialogbSizer->Add( m_networknotebook, 1, wxEXPAND | wxALL, 5 );

	m_networksdbSizer = new wxStdDialogButtonSizer();
	m_networksdbSizerOK = new YANGButton( this, wxID_OK );
	m_networksdbSizer->AddButton( m_networksdbSizerOK );
	m_networksdbSizerCancel = new YANGButton( this, wxID_CANCEL );
	m_networksdbSizer->AddButton( m_networksdbSizerCancel );
	m_networksdbSizer->Realize();
	NetworkDialogbSizer->Add( m_networksdbSizer, 0, wxALIGN_CENTER_HORIZONTAL | wxBOTTOM, 5 );

	//	m_networknotebook->Layout();
	SetSizer( NetworkDialogbSizer );
	Layout();
	NetworkDialogbSizer->Fit( this );

	Centre();
	SetIcon(wxIcon(yang_xpm));

	m_nicknametextCtrl->SetMaxLength(NICK_MAX_LENGTH);
	m_gamenicknametextCtrl->SetMaxLength(GAMENICK_MAX_LENGTH);

	m_nicknametextCtrl->SetValue(g_configuration->nickname);
	m_gamenicknametextCtrl->SetValue(g_configuration->game_nickname);

	m_soundjoincheckBox->SetValue(g_configuration->play_snd_join);
	m_soundjointextCtrl->SetValue(g_configuration->snd_join_file);
	m_soundjointestbutton->Enable(g_configuration->play_snd_join);
	m_soundjoinlocatebutton->Enable(g_configuration->play_snd_join);
	m_soundjointextCtrl->Enable(g_configuration->play_snd_join);

	m_soundleavecheckBox->SetValue(g_configuration->play_snd_leave);
	m_soundleavetextCtrl->SetValue(g_configuration->snd_leave_file);
	m_soundleavetestbutton->Enable(g_configuration->play_snd_leave);
	m_soundleavelocatebutton->Enable(g_configuration->play_snd_leave);
	m_soundleavetextCtrl->Enable(g_configuration->play_snd_leave);

	m_soundsendcheckBox->SetValue(g_configuration->play_snd_send);
	m_soundsendtextCtrl->SetValue(g_configuration->snd_send_file);
	m_soundsendtestbutton->Enable(g_configuration->play_snd_send);
	m_soundsendlocatebutton->Enable(g_configuration->play_snd_send);
	m_soundsendtextCtrl->Enable(g_configuration->play_snd_send);

	m_soundreceivecheckBox->SetValue(g_configuration->play_snd_receive);
	m_soundreceivetextCtrl->SetValue(g_configuration->snd_receive_file);
	m_soundreceivetestbutton->Enable(g_configuration->play_snd_receive);
	m_soundreceivelocatebutton->Enable(g_configuration->play_snd_receive);
	m_soundreceivetextCtrl->Enable(g_configuration->play_snd_receive);

	m_sounderrorcheckBox->SetValue(g_configuration->play_snd_error);
	m_sounderrortextCtrl->SetValue(g_configuration->snd_error_file);
	m_sounderrortestbutton->Enable(g_configuration->play_snd_error);
	m_sounderrorlocatebutton->Enable(g_configuration->play_snd_error);
	m_sounderrortextCtrl->Enable(g_configuration->play_snd_error);

	m_soundmutewhileingamecheckBox->SetValue(g_configuration->mute_sounds_while_ingame);

	m_gameporttextCtrl->SetValue(wxString::Format(wxT("%ld"), g_configuration->game_port_number));
	m_serverporttextCtrl->SetValue(wxString::Format(wxT("%ld"), g_configuration->server_port_number));

#ifdef ENABLE_UPNP
	m_enableupnpcheckBox->SetValue(g_configuration->enable_upnp);
#endif
}

NetworkDialog::~NetworkDialog()
{
#if ((defined __WXMSW__) || (defined __WXMAC__) || (defined __WXCOCOA__))
	if (!g_main_frame)
		g_main_frame = new MainFrame(NULL);
	g_main_frame->ShowMainFrame();
#else
	if (g_main_frame)
		g_main_frame->ShowMainFrame();
	else
	{
		g_configuration->DetectTerminal();
		if (g_configuration->terminal_fullcmd.IsEmpty())
		{
			wxMessageBox(wxT("WARNING: No graphical terminal emulator has been detected.\nIt's HIGHLY RECOMMENDED (and sometimes, even a must!) to execute a game from within a terminal emulator.\nAs an example, an EDuke32 multiplayer game has got stuck at the GTK startup because of that."), wxT("No terminal emulator detected"), wxOK|wxICON_EXCLAMATION);
			g_adv_dialog = new AdvDialog;
			g_adv_dialog->Show(true);
			g_adv_dialog->SetFocus();
		}
		else
		{
			g_configuration->Save();
			g_main_frame = new MainFrame(NULL);
			g_main_frame->ShowMainFrame();
		}
	}
#endif
	//g_network_dialog = NULL;
}

void NetworkDialog::OnLocateSndFile(wxCommandEvent& event)
{
	wxFileDialog l_dialog(NULL, wxT("Select a sound file"), wxEmptyString, wxEmptyString,
		wxT("RIFF wave files (*.wav)|*.wav|All files|*"), wxFD_OPEN|wxFD_FILE_MUST_EXIST);
	if (l_dialog.ShowModal() == wxID_OK)
	{
		wxString l_fullpath = l_dialog.GetPath();
		switch (event.GetId())
		{
			case ID_SNDJOINLOCATE:    m_soundjointextCtrl->SetValue(l_fullpath); break;
			case ID_SNDLEAVELOCATE:   m_soundleavetextCtrl->SetValue(l_fullpath); break;
			case ID_SNDSENDLOCATE:    m_soundsendtextCtrl->SetValue(l_fullpath); break;
			case ID_SNDRECEIVELOCATE: m_soundreceivetextCtrl->SetValue(l_fullpath); break;
			case ID_SNDERRORLOCATE:   m_sounderrortextCtrl->SetValue(l_fullpath); break;
			default: ;
		}
	}
}

void NetworkDialog::OnTestSound(wxCommandEvent& event)
{
	switch (event.GetId())
	{
		case ID_SNDJOINTEST:    g_PlaySound(m_soundjointextCtrl->GetValue()); break;
		case ID_SNDLEAVETEST:   g_PlaySound(m_soundleavetextCtrl->GetValue()); break;
		case ID_SNDSENDTEST:    g_PlaySound(m_soundsendtextCtrl->GetValue()); break;
		case ID_SNDRECEIVETEST: g_PlaySound(m_soundreceivetextCtrl->GetValue()); break;
		case ID_SNDERRORTEST:   g_PlaySound(m_sounderrortextCtrl->GetValue()); break;
		default: ;
	}
}

void NetworkDialog::OnSndRestore(wxCommandEvent& WXUNUSED(event))
{
	wxString l_snd_join, l_snd_leave, l_snd_send, l_snd_receive, l_snd_error;
	RestoreDefaultSounds(l_snd_join, l_snd_leave, l_snd_send, l_snd_receive, l_snd_error);
	m_soundjointextCtrl->SetValue(l_snd_join);
	m_soundleavetextCtrl->SetValue(l_snd_leave);
	m_soundsendtextCtrl->SetValue(l_snd_send);
	m_soundreceivetextCtrl->SetValue(l_snd_receive);
	m_sounderrortextCtrl->SetValue(l_snd_error);
	m_soundjointextCtrl->Disable();
	m_soundleavetextCtrl->Disable();
	m_soundsendtextCtrl->Disable();
	m_soundreceivetextCtrl->Disable();
	m_sounderrortextCtrl->Disable();
	m_soundjoincheckBox->SetValue(false);
	m_soundleavecheckBox->SetValue(false);
	m_soundsendcheckBox->SetValue(false);
	m_soundreceivecheckBox->SetValue(false);
	m_sounderrorcheckBox->SetValue(false);
	m_soundjointestbutton->Disable();
	m_soundleavetestbutton->Disable();
	m_soundsendtestbutton->Disable();
	m_soundreceivetestbutton->Disable();
	m_sounderrortestbutton->Disable();
	m_soundjoinlocatebutton->Disable();
	m_soundleavelocatebutton->Disable();
	m_soundsendlocatebutton->Disable();
	m_soundreceivelocatebutton->Disable();
	m_sounderrorlocatebutton->Disable();
	/*m_soundjointextCtrl->Enable(l_snd_join);
	m_soundleavetextCtrl->Enable(l_snd_leave);
	m_soundsendtextCtrl->Enable(l_snd_send);
	m_soundreceivetextCtrl->Enable(l_snd_receive);
	m_sounderrortextCtrl->Enable(l_snd_error);
	m_soundjoincheckBox->SetValue(l_snd_join);
	m_soundleavecheckBox->SetValue(l_snd_leave);
	m_soundsendcheckBox->SetValue(l_snd_send);
	m_soundreceivecheckBox->SetValue(l_snd_receive);
	m_sounderrorcheckBox->SetValue(l_snd_error);
	m_soundjointestbutton->Enable(l_snd_join);
	m_soundleavetestbutton->Enable(l_snd_leave);
	m_soundsendtestbutton->Enable(l_snd_send);
	m_soundreceivetestbutton->Enable(l_snd_receive);
	m_sounderrortestbutton->Enable(l_snd_error);
	m_soundjoinlocatebutton->Enable(l_snd_join);
	m_soundleavelocatebutton->Enable(l_snd_leave);
	m_soundsendlocatebutton->Enable(l_snd_send);
	m_soundreceivelocatebutton->Enable(l_snd_receive);
	m_sounderrorlocatebutton->Enable(l_snd_error);*/
	m_soundmutewhileingamecheckBox->SetValue(true);
}

void NetworkDialog::OnCheckBoxClick(wxCommandEvent& event)
{
	bool l_selection = event.IsChecked();

	switch (event.GetId())
	{
		case ID_SNDJOINCHECKBOX:
			m_soundjointestbutton->Enable(l_selection);
			m_soundjoinlocatebutton->Enable(l_selection);
			m_soundjointextCtrl->Enable(l_selection);
			break;
		case ID_SNDLEAVECHECKBOX:
			m_soundleavetestbutton->Enable(l_selection);
			m_soundleavelocatebutton->Enable(l_selection);
			m_soundleavetextCtrl->Enable(l_selection);
			break;
		case ID_SNDSENDCHECKBOX:
			m_soundsendtestbutton->Enable(l_selection);
			m_soundsendlocatebutton->Enable(l_selection);
			m_soundsendtextCtrl->Enable(l_selection);
			break;
		case ID_SNDRECEIVECHECKBOX:
			m_soundreceivetestbutton->Enable(l_selection);
			m_soundreceivelocatebutton->Enable(l_selection);
			m_soundreceivetextCtrl->Enable(l_selection);
			break;
		case ID_SNDERRORCHECKBOX:
			m_sounderrortestbutton->Enable(l_selection);
			m_sounderrorlocatebutton->Enable(l_selection);
			m_sounderrortextCtrl->Enable(l_selection);
			break;
		default: ;
	}
}

void NetworkDialog::OnUpdateGameNickName(wxCommandEvent& WXUNUSED(event))
{
	wxString l_nickname = m_gamenicknametextCtrl->GetValue(),
		l_allowedchars = wxT("#+-0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz");
	int l_loop_var = 0, l_str_length = l_nickname.Len();
	while (l_loop_var < l_str_length)
		if (l_allowedchars.Find(l_nickname[l_loop_var]) >= 0)
			l_loop_var++;
		else
		{
			l_nickname.Remove(l_loop_var, 1);
			m_gamenicknametextCtrl->SetValue(l_nickname);
			l_str_length--;
		}
}
/*
void NetworkDialog::OnApply(wxCommandEvent& WXUNUSED(event))
{
ApplySettings();
}
*/
void NetworkDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	if (ApplySettings())
		Destroy();
}

void NetworkDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	Destroy();
}

bool NetworkDialog::ApplySettings()
{
	long l_server_port_num;
	if (!((m_serverporttextCtrl->GetValue()).ToLong(&l_server_port_num)))
		g_configuration->server_port_number = DEFAULT_SERVER_PORT_NUM;
	else if (l_server_port_num == 8000)
	{
		m_networknotebook->SetSelection(2);
		wxMessageBox(wxT("You can't set server TCP port number to 8000.\nThis is because, according to a few tests,\ncertain players have had issues when they have tried\nto join a room hosted on port 8000."), wxT("Port number can't be set"));
		return false;
	}
	else
		g_configuration->server_port_number = l_server_port_num;

	if (!((m_gameporttextCtrl->GetValue()).ToLong(&(g_configuration->game_port_number))))
		g_configuration->game_port_number = DEFAULT_GAME_PORT_NUM;

	g_configuration->nickname = m_nicknametextCtrl->GetValue();
	g_configuration->game_nickname = m_gamenicknametextCtrl->GetValue();

	g_configuration->play_snd_join = m_soundjoincheckBox->GetValue();
	g_configuration->snd_join_file = m_soundjointextCtrl->GetValue();

	g_configuration->play_snd_leave = m_soundleavecheckBox->GetValue();
	g_configuration->snd_leave_file = m_soundleavetextCtrl->GetValue();

	g_configuration->play_snd_send = m_soundsendcheckBox->GetValue();
	g_configuration->snd_send_file = m_soundsendtextCtrl->GetValue();

	g_configuration->play_snd_receive = m_soundreceivecheckBox->GetValue();
	g_configuration->snd_receive_file = m_soundreceivetextCtrl->GetValue();

	g_configuration->play_snd_error = m_sounderrorcheckBox->GetValue();
	g_configuration->snd_error_file = m_sounderrortextCtrl->GetValue();

	g_configuration->mute_sounds_while_ingame = m_soundmutewhileingamecheckBox->GetValue();
#ifdef ENABLE_UPNP
	if (m_enableupnpcheckBox->GetValue() && !g_configuration->enable_upnp)
		g_Init_UPNP();
	g_configuration->enable_upnp = m_enableupnpcheckBox->GetValue();
#endif
	g_configuration->Save();
	return true;
}
